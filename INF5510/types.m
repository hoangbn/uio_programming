%Exercise 1 Types

const Interface <- typeobject Interface
   function TestMe -> [s:String]
   operation TestOperation
end Interface

const TypeTest <- object TypeTest
%export function TestMe -> [s:String]
%end TestMe
export operation TestOperation
   stdout.putstring["Test Operation\n"]
end TestOperation
end TypeTest

const Hello <- object Hello
var number: Integer
var num: Integer <- 1
var name: String <- "Test"


export function TestMe -> [s:String]
   s <- "Function"
end TestMe

export operation TestOperation
  stdout.putstring["Test operation\n"]
end TestOperation

export function Multiply[a:Integer, b:Integer] -> [res:Integer]
res <- a*b
end Multiply 

initially
 stdout.putstring["I want initial block\n"]
 number <- 0
 name <- "Testing"

if typeof(TypeTest) *> Interface then
      stdout.putstring["OKEY \n"]
  else 
    stdout.putstring["NOT OKEY \n"]
end if
end initially

process
if number = 0 then
 for i: Integer <- 0 while i < 10 by i <- i+1 
   number <- number + 1
 num <- Hello.Multiply[number, num]
 stdout.putstring["Num: "||num.asstring||"\n"]
   stdout.putstring["WORK: " ||name||" Number: " || number.asstring||"\n"]
   %stdout.putstring["I WANT TO PRINT MORE!\n"]
 end for
     else
 var s: String <- Hello.TestMe
 stdout.putstring["String s is " ||s||"\n"]
 Hello.TestOperation
end if
end process



end Hello
