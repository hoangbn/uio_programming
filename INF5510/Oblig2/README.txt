Exercise 1 - Breakeven Results

[hoangbn@lily Oblig2]$ emx -Rdiamant:16854 Exercise1.x
Emerald listening on port 16854 41d6, epoch 57236 df94
Starting on lily.ifi.uio.no
2 nodes active
The times it took to move 100, 500, 1000 and 2000 bytes: 
call-by-visit time: 0:000721
call-by-move time: 0:000438
call-by-visit time: 0:000757
call-by-move time: 0:000378
call-by-visit time: 0:000796
call-by-move time: 0:000413
call-by-visit time: 0:000810
call-by-move time: 0:000411

Summary
If we look at the results, we can see that with call-by-visit, the time will only rise
proportionaly with the size of the data used in transfer. But when using call-by-move there 
is only a slight difference in time whatever size of data is being transferred. And it looks like
call-by-move is faster in transferring all sizes of data. 

Exercise 2 - TimeCollector Results
----------------------  Shell 1  ----------------------
[hoangbn@lily Oblig2]$ emx -Rdiamant:16854 Exercise2.x
Emerald listening on port 18902 49d6, epoch 45725 b29d
Starting on lily.ifi.uio.no
3 nodes active.
Time collected are: 
0: Sat Mar  8 15:24:59 2014
1: Sat Mar  8 15:24:59 2014
2: Sat Mar  8 15:24:59 2014
Back home.
----------------------  Shell 2  ----------------------
hoangbn@diamant ~/INF5510/Oblig2 $ emx -R
Emerald listening on port 16854 41d6, epoch 29474 7322
Timecollector was here
----------------------  Shell 3  ----------------------
[hoangbn@lily Oblig2]$ emx -Rdiamant:16854
Emerald listening on port 17878 45d6, epoch 15422 3c3e
Timecollector was here
-------------------------------------------------------

Summary
Well since the network we're using is extremely fast it would take very little time to
move an object from a node to another node, that's why all the time is the same.


Exercise 3 - Time Synchronization
Maybe I should comment more in my code..
Well I put in a delay so we can simulate a delay in the network so we can find the median
to use in time synchronization.

----------------------  Shell 1  ----------------------
[hoangbn@lily Oblig2]$ emx -Rdiamant:17878 Exercise3.x
Emerald listening on port 16854 41d6, epoch 43975 abc7
Starting on lily.ifi.uio.no
3 nodes active.
Moved to diamant.ifi.uio.no
Moved to lily.ifi.uio.no
Time collected are: 
0: Sat Mar  8 15:30:38 2014
1: Sat Mar  8 15:30:39 2014
2: Sat Mar  8 15:30:40 2014
The median/average time is 1.00394
Back home.
-------------------------------------------------------

The output of other shell is basically the same as in Exercise 2. It just says someone has 
been in this node.

Exercise 4 - Planetlab
Well, I got to log in exactly 6 nodes/machines from planetlab, which are:
planetlab1lannion.elibel.tm.fr
planetlab1.science.unitn.it
planetlab1.s3.kth.se
planetlab1.research.nicta.com.au      ----really slow, gives other nodes segmentation fault!?
planetlab1.unineuchatel.ch
planetlab2.urv.cat

Tried to log in other nodes that was specified from "http://www.uio.no/studier/emner/matnat/ifi/INF5510/v14/planetlab.html"
But the other nodes just give us an error message after a long wait.

But as you can see some writing I did there, that node made it so everything went slow and sometimes I get segmentation fault in other nodes... don't know the reason.
So I got to run the program on 5 nodes.

Results - The main node executing the program
---------------------------------------------------------------------------------------------
[diku_INF5510v13@planetlab1lannion ~]$ emx -U -Rplanetlab1.science.unitn.it:24691 Exercise3.x
Emerald listening on port 25158 6246, epoch 52757 ce15
Starting on planetlab1lannion.elibel.tm.fr
5 nodes active.
Moved to planetlab1.science.unitn.it
Moved to planetlab1.unineuchatel.ch
Moved to planetlab2.urv.cat
Moved to planetlab1.s3.kth.se
Time collected are: 
0: Sat Mar  8 15:29:48 2014
1: Sat Mar  8 15:29:49 2014
2: Sat Mar  8 15:29:59 2014
3: Sat Mar  8 15:29:49 2014
4: Sat Mar  8 15:29:49 2014
The median/average time is 2.38734
Back home.
---------------------------------------------------------------------------------------------
All the other nodes prints out that TimeCollector was here.
Kjørte fra UiO sine pcer.

[hoangbn@lily Oblig2]$ emx -U -Rplanetlab1.science.unitn.it:24691 Exercise3.x
Emerald listening on port 16854 41d6, epoch 29834 748a
Starting on lily.ifi.uio.no
6 nodes active.
Moved to planetlab1.science.unitn.it
Moved to planetlab1.unineuchatel.ch
Moved to planetlab2.urv.cat
Moved to planetlab1.s3.kth.se
Moved to planetlab1lannion.elibel.tm.fr
Time collected are: 
0: Sat Mar  8 16:59:15 2014
1: Sat Mar  8 16:59:16 2014
2: Sat Mar  8 16:59:28 2014
3: Sat Mar  8 16:59:19 2014
4: Sat Mar  8 16:59:20 2014
5: Sat Mar  8 16:59:21 2014
The median/average time is 4.79415
Back home.



Exercise 5 - Emerald-Lite
Ran the programs in planetlab and uio machines, but don't know how to put the result here.
But in the first exercise. The time was much higher(in milliseconds) than when ran in the computers.
And some reason I got segmentation fault when I tried to run the 3. exercise.
It got the time of the 5 first nodes, but when it came to the last node, segmentation fault.

Well I could show you the programs running on the Emerald lite in the next group lecture.
