%Exercise 3 - Time Synchronization

const Timecollector <- object Timecollector
	const NodeHome <- locate self
	var NodeAway: Node
	var home: Node
	var timeArray: Array.of[Time] <- Array.of[Time].empty
	var localTime: Time
	var AllNodes: NodeList
	var nodeElem: NodeListElement
	var sec: Integer
	var ms: Integer
	var median: Real
	var one: Time


	initially
	  sec <- 0
	  ms <- 0
	  median <- 0.0
	  one <-  Time.create[0 , 0]
	end initially

	process
		NodeHome$stdout.PutString["Starting on " || NodeHome$name || "\n"]
		AllNodes <- NodeHome.getActiveNodes
		NodeHome$stdout.PutString[(AllNodes.upperbound + 1).asString || " nodes active.\n"]
		localTime <- NodeHome.getTimeOfDay
		%timeArray.addUpper[localTime]
		timeArray.addUpper[NodeHome.getTimeOfDay]
		for i: Integer <- 1 while i <= AllNodes.upperbound by i <- i + 1
			NodeAway <- AllNodes[i]$theNode
			move Timecollector to NodeAway
			NodeHome$stdout.PutString["Moved to " || NodeAway$name || "\n"] 
			NodeAway.delay[one]
			NodeAway$stdout.PutString[NodeHome$name || " was here\n"]
			timeArray.addUpper[NodeAway.getTimeOfDay]
		end for
		move Timecollector to NodeHome
		stdout.putstring["Time collected are: \n"]
		for a: Integer <- 0 while a <= timeArray.upperbound by a <- a + 1
		   stdout.putstring[a.asstring || ": " || timeArray[a].asDate.asstring || "\n"]
		   
		   sec <- sec + (timeArray[a].getSeconds - localTime.getSeconds)
		   ms <- ms + (timeArray[a].getMicroSeconds - localTime.getMicroSeconds)
		 %  stdout.putstring[(timeArray[a].getSeconds-localTime.getSeconds).asReal.asstring||"\n"]
		 %  stdout.putstring[(timeArray[a].getMicroSeconds-localTime.getMicroSeconds).asReal.asstring||"\n"]
		end for
		
	%	stdout.putstring["The median/average time of number of nodes: " || timeArray.upperbound || " is \n"]
		
	%	stdout.putstring["Seconds "||sec.asstring||"\n"]
	%	stdout.putstring["Micro Seconds "|| ms.asstring||"\n"]
	%	stdout.putstring["Micro Seconds to seconds "|| (ms.asReal/1000000.0).asstring || "\n"]
		
		median <- median + ((sec.asReal + (ms.asReal/1000000.0))/(timeArray.upperbound+1).asReal)
		stdout.putstring["The median/average time is " || median.asString || "\n"]
		NodeHome$stdout.PutString["Back home.\n"]
	end process
end Timecollector
