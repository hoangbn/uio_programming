%Exercise 2 - Timecollector

const Timecollector <- object Timecollector
	const NodeHome <- locate self
	var NodeAway: Node
	var timeArray: Array.of[Time] <- Array.of[Time].empty
	var localTime: Time
	var AllNodes: NodeList
	var nodeElem: NodeListElement
	
	process
		NodeHome$stdout.PutString["Starting on " || NodeHome$name || "\n"]
		AllNodes <- NodeHome.getActiveNodes
		NodeHome$stdout.PutString[(AllNodes.upperbound + 1).asString || " nodes active.\n"]
		%localTime <- NodeHome.getTimeOfDay
		%timeArray.addUpper[localTime]
		timeArray.addUpper[NodeHome.getTimeOfDay]
		for i: Integer <- 1 while i <= AllNodes.upperbound by i <- i + 1
			NodeAway <- AllNodes[i]$theNode
			move Timecollector to NodeAway
			
			NodeAway$stdout.PutString["Timecollector was here\n"]
			timeArray.addUpper[NodeAway.getTimeOfDay]
		end for
		move Timecollector to NodeHome
		stdout.putstring["Time collected are: \n"]
		for a: Integer <- 0 while a <= timeArray.upperbound by a <- a + 1
		  stdout.putstring[a.asstring || ": " || timeArray[a].asDate.asstring || "\n"]
		end for
		NodeHome$stdout.PutString["Back home.\n"]
	end process
end Timecollector
