%Exercise 1 - Break-even by visit
% 100 b, 500 b, 1000 b, 2000 b

const Breakeven <- object Breakeven
	const home <- locate self
	var away: Node
	var sTime: Time
	var eTime: Time
	var all: NodeList
	var elem: NodeListElement
	var TimeCollection: Array.of[Time] <- Array.of[Time].empty
	var TimeCollection2: Array.of[Time] <- Array.of[Time].empty
	
	operation getSize[num:Integer] -> [res:Integer]
		if num = 0 then
			res <- 100
		elseif num = 1 then
			res <- 500 
		elseif num = 2 then 
			res <- 1000 
		else 
			res <- 2000 
		end if
	end getSize
	
	process
		home$stdout.putstring["Starting on " || home$name || "\n"]
		all <- home.getActiveNodes
		home$stdout.putstring[(all.upperbound + 1).asstring || " nodes active\n"]
		
		for i: Integer <- 1 while i <= all.upperbound by i <- i + 1 
			away <- all[i]$theNode
		        %It is not only Nodes that is movable, we can use "move" to move parameters too
			for a:Integer <- 0 while a < 4 by a <- a + 1
			  sTime <- home.getTimeOfDay
			  const size <- BitChunk.create[self.getSize[a]]
			  move size to away
			  TimeCollection2.addUpper[home.getTimeOfDay-sTime]
			  move size to home
			  eTime <- home.getTimeOfDay
			  TimeCollection.addUpper[eTime-sTime]
			end for
		end for
		
		%Print out the results
		stdout.putstring["The times it took to move 100, 500, 1000 and 2000 bytes: \n"]
		for x: Integer <- 0 while x <= TimeCollection.upperbound by x <- x + 1
		  stdout.putstring["call-by-visit time: " || TimeCollection[x].asstring||"\n"]
		  stdout.putstring["call-by-move time: " || TimeCollection2[x].asstring||"\n"]
		end for
	end process
	
end Breakeven
		
