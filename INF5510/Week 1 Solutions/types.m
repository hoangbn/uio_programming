%Testing emerald

const SimpleCollection <- typeObject SimpleCollection
	operation add[navn:String] ->[res: Boolean]
	operation contains[navn:String] ->[res: Boolean]
	operation remove[navn: String] ->[res: Boolean]
end SimpleCollection

const Test <- class Test 
%create arrays of
	export operation add[navn:String] ->[res: Boolean]
	end add
	export operation contains[navn:String] ->[res: Boolean]
	end contains
	export operation remove[navn: String] ->[res: Boolean]
	end remove
end Test


const Main <- object Main
var testObject : Test <- Test.create[]
	process
		if (typeof testObject) *> SimpleCollection then
			stdout.putstring["YES\n"]
		end if

	end process

end Main
