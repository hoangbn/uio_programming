%simplestack

const SimpleCollection <- typeObject SimpleCollection
	operation add[navn:String] ->[res: Boolean]
	operation contains[navn:String] ->[res: Boolean]
	operation remove[navn: String] ->[res: Boolean]
end SimpleCollection

const SimpleStack <- class SimpleStack 
%create arrays of names, 
 var ai: Array.of[String] <- Array.of[String].empty[]
 
 export operation add[navn:String]->[res: Boolean]
 	for i : Integer <- 0 while i < ai.upperbound+1 by i <- i + 1
 		if ai.getElement[i] = navn then
 			res <- false
 			return
 		end if
 	end for
 	
 	ai.addUpper[navn]	
 	res <- true
 end add
  	export operation contains[navn:String] ->[res: Boolean]

  	 	for i : Integer <- 0 while i < ai.upperbound+1 by i <- i + 1
 		if ai.getElement[i] = navn then
 			res <- true
 			return
 		end if
 	end for
 	res <- false
 	end contains
 	
 	
	export operation remove[navn: String] ->[res: Boolean]
	  	for i : Integer <- 0 while i < ai.upperbound+1 by i <- i + 1
 		if ai.getElement[i] = navn then
 				ai.setElement[i, nil]
 		 	res <- true
 			return
 		end if
 	end for
 	res <- false
	end remove
	
	export operation getElem[index: Integer] -> [res: String]
		res <- ai.getElement[index]
	end getElem

end SimpleStack



const Main <- object Main 
var X : SimpleStack <- SimpleStack.create[]

	initially	 
		if X.add["Eric Jul"] then
			stdout.putstring["Success adding\n"]
		else 
			stdout.putstring["Failed adding person\n"]		
		end if
	
		if X.contains["Eric Jul"] then
			stdout.putstring["Success finding person\n"]
		else 
			stdout.putstring["Failed finding person\n"]		
		end if
	
		if X.remove["Eric Jul"] then 
			stdout.putstring["Removed person\n"]
		else
			stdout.putstring["Failed to remove person\n"]
		end if
		if X.contains["Eric Jul"] then
			stdout.putstring["Success finding person\n"]
		else 
			stdout.putstring["Failed finding person\n"]		
		end if	


	end initially

end Main
