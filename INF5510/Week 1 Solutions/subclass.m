
	const person <- class person [navn:String, alder:Integer] 
		export operation insertName[nynavn: String]
			navn <-nynavn
		end insertName
		export operation hentNavn-> [res:String]
			res <- navn
		end hentNavn
	end person

	const child <- class child (person) [stil: String]
		
		
	end child

	if self.containsFileName[stil] then
				(locate self)$stdout.putstring["I already have the file, don't need to download\n"]
				return
			end if
			self.srandom		
			var listPeers : Array.of[Peer] <- self.listPeersWithGivenFile[s]
			
			if listPeers == nil then
				(locate self)$stdout.putstring["File not found in the server\n"]
				return
			end if
			
			
			var randomPeer : Peer
			var randomIndex : Integer <- 0
			
			if (listPeers.upperbound+1) > 1 then
				%if there are many peers that has a given file, else just download from 1 available peer
				randomIndex <- self.random.abs # (listPeers.upperbound+1)
			end if		
				
			randomPeer <- listPeers.getElement[randomIndex]
			if self.checkAvailable[randomPeer,s] then
				(locate self)$stdout.putstring["Begin to download from random peer\n"]			
				var fileget : File <- randomPeer.searchFileName[s]
				self.addFile[fileget]	
				
			else
				(locate self)$stdout.putstring["Failed to download from random peer, it has gone down\n"]
				if serverName.containsPeer[randomPeer] then
					serverName.removePeerFromEverything[randomPeer]
				end if
							
			end if

	const Main <- object Main

	process
	var teacher: child <- child.create["navn",24,"hei"]
	stdout.putString[teacher.hentNavn[]||"\n"]
	teacher.insertName["kHIEM"]
	stdout.putstring["New name: " ||teacher.hentNavn||"\n"]

	end process

	end Main
