%sieve program


const pType <- typeobject pType
operation pass[prime: Integer]
end pType


const Prime <- class Prime 
var primeInput : Integer <- 0
var next : pType <- nil

	export operation pass[primeNew: Integer] 
		if primeInput == 0 then
			primeInput <- primeNew
				if primeInput == 2 then 
					stdout.putstring["Sieve call: "]
					stdout.putint[primeNew,12]
					stdout.putstring["\n"]	
				end if		
		else
				if next == nil then
					next <- Prime.create[]
					
					stdout.putstring["Sieve call: "]
					stdout.putint[primeNew,12]
					stdout.putstring["\n"]
					
				end if
				
				if (primeNew # primeInput) !== 0 then
					next.pass[primeNew]
				end if	
		end if
	end pass
end Prime



const Sieve <- object Sieve

var firstInput : Integer <- 2
var primeCall : pType <- Prime.create[]

initially
	loop
		primeCall.pass[firstInput]
		firstInput <- firstInput + 1
		exit when firstInput == 100
	
	end loop
end initially

end Sieve