%moving man


const theHandler <- object theHandler

	export operation nodeUp[n: Node, t: Time]
		(locate self)$stdout.putString["NodeUp!\n"]
	end nodeUp

	export operation nodeDown[n: Node, t: Time]
		(locate self)$stdout.putString["NodeDown !\n"]		
		
	end nodeDown

end theHandler

const MoveManType <- typeobject MoveManType
	operation getOnline[] -> [list: NodeList]
	operation collectMovedIn[n:Node]
	operation printOut[]
	operation checkUnavailable[place: Node] -> [r:Boolean]
	operation setOnline[list : NodeList]
	
end MoveManType


const MoveMan <- object MoveMan



	const home <- locate self
	
	var there : Node
	var activeNodes: NodeList 
	var collect_id : Array.of[Integer] <- Array.of[Integer].create[0]
    const one <- Time.create[3, 0]
%    var watch : Watcher <- Watcher.create[activeNodes, home]
    
    
    export operation print
      (locate self)$stdout.putstring["Hello\n"]
    end print
  
    
    export operation getOnline[] -> [list: NodeList]
    	list <- activeNodes
    end getOnline
    
    export operation setOnline[list : NodeList]
    	activeNodes <- list
    end setOnline
	
	export operation collectMovedId[n: Node]
		collect_id.addUpper[n.getLNN]
	end collectMovedId
	
	export operation printOut[]
		stdout.putstring["Moving Man has moved to this location: \n"]
		for i : Integer <- 0 while i <= collect_id.upperbound by i <- i + 1
			var id : Integer <- collect_id.getElement[i] / 65536
			stdout.putstring["Node: "||id.asstring||"\n"]
		end for
	end printOut
	
	
	export operation checkUnavailable[place: Node] -> [r:Boolean]
	    move self to place
	    const here <- locate self
	    r <- false
	     unavailable
     		(locate self)$stdout.putstring["Node unavailable\n"]
		self.print
     		r <- true
		end unavailable 
	end checkUnavailable
	
	export operation clearArray[]
		for i : Integer <- 0 while i <= collect_id.upperbound by i <- i + 1
			var removeThis : Integer <- collect_id.removeUpper[]
		end for
	end clearArray


	
	process
		home.setNodeEventHandler[theHandler]
		activeNodes <- home$ActiveNodes
		home$stdout.PutString[(activeNodes.upperbound + 1).asString || " nodes active.\n"]		

			loop
		
				activeNodes <- home$ActiveNodes
				for i : Integer <- activeNodes.upperbound while i >= 1 by i <- i - 1
     			 there <- activeNodes[i]$theNode    
     		  	if self.checkUnavailable[there] then 
     		  		  stdout.putstring["IN HERE\n"]
     		  		  activeNodes <- home.getActiveNodes
     		  		  i <- i - 1
     		  	else
     		 		MoveMan.collectMovedId[there]
     		 		there$stdout.PutString["Move Man moved in here, but it seemed that the place hated him\n"]
     		 	end if
     		 	end for
    	
    		move self to home
    		MoveMan.printOut	
			MoveMan.clearArray
    		home.delay[one]
    		end loop

    
	
	
	end process



end MoveMan



