const pType <- typeobject pType
  operation pass[prime:Integer]
end pType

const Prime <- class Prime
  var primeInput: Integer <- 0
  var next: pType <- nil

  export operation pass[newPrime: Integer]
    if primeInput == 0 then
      primeInput <- newPrime
      if primeInput == 2 then
	stdout.putstring[newPrime.asstring || "\n"]
      end if
    else
      if next == nil then
	next <- Prime.create[]
	stdout.putstring[newPrime.asstring || "\n"]
      end if

      if (newPrime # primeInput) !== 0 then
	next.pass[newPrime]
      end if
    end if
