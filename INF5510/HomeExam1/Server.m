%Server that indexes all files the nodes connected to the network are sharing
export Server 

const Server <- class Server
    var names:String <- "Server"
var pe: syntactictypeof Peer
    export operation addPeers[p:Any]
        
    end addPeers

    export operation printout
	
    end printout

    export operation listOfPeers[s:String] -> [l:Array.of[Any]]
%stdout.putstring[s || "\n"]
	l <- nil

    end listOfPeers

  	export operation getNames[] ->[s: String]
		s <- names
	end getNames

end Server
