%a Peer
export Peer

const Peer <- class Peer[central:Server]
  const home <- locate self
  var PeersWithTheFile: Array.of[Peer] <- Array.of[Peer].empty
 
  var names:String <- "Peer"
	
	export operation getNames[] ->[s: String]
		s <- names
	end getNames

	export operation hello
	    (locate self)$stdout.putstring["Called by server\n"]
	end hello
      
	export operation getList
	  PeersWithTheFile <- central.listOfPeers["YOLO"]
	  for i:Integer <- 0 while i <= PeersWithTheFile.upperbound by i <- i + 1
	      
	  end for
	end getList

  initially
 
  end initially

  process
     (locate central)$stdout.putstring["Hello server from peer \n"]
  end process
end Peer
