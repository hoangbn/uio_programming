const Peer <- class Peer[name:Any]
      export operation getName -> [s:String]
	s <- (view name as String)
      end getName
end Peer

const Server <- class Server
  var list: Array.of[Peer] <- Array.of[Peer].empty
  
  export operation getlist -> [l:Array.of[Peer]]
    l <- list
  end getlist
  
end Server


const Test <- object Test
    var ser:Server <- Server.create
    var per: Peer <- Peer.create["YOLO"]
	var p: Peer
    var list2: Array.of[Any] <- Array.of[Any].empty
    var list: Array.of[Peer] <- Array.of[Peer].create[1]
    var a: Array.of[None] <- Array.of[None].create[1]
    var an: Any
    var c: String <- "Presentation of fundamental concepts of distributed objects including local and distributed objects, remote object invocation, communication, parameter passing principles, garbage collection, performance issues and typical applications. We also cover advanced type systems including concepts such as immutability and its application to distributed programming and access control. We also discuss the design of language constructs for parallelism. Throughout we use the programming language Emerald as a base. We dig into the implementation of distributed objects including virtual machines, compilation techniques, run-time typing, dynamic program loading, and how to do distributed garbage collection."
    var b: String <- "Give a man some fire, and he'll be warm for a day. \n Set a man on file, and he'll be warm for the rest of his life"
	var m: Any <- Array.of[Peer].empty
	
  operation hash[s:String, len:Integer] -> [res:Integer]
    var hash: Integer <- 0
    var str:String <- s
     
   for i:Integer <- 0 while i < len by i <- i + 1
      hash <- (33 * hash + s.getElement[i].ord) # 65075022
   end for
   res <- hash
  end hash
	
%	m_w = <choose-initializer>;    /* must not be zero, nor 0x464fffff */
%m_z = <choose-initializer>;    /* must not be zero, nor 0x9068ffff */
 
%uint get_random()
%{
   % m_z = 36969 * (m_z & 65535) + (m_z >> 16);
  %  m_w = 18000 * (m_w & 65535) + (m_w >> 16);
 %   return (m_z << 16) + m_w;  /* 32-bit result */
%}
	%static unsigned int seed = 1;
%void srand (int newseed) {
 %   seed = (unsigned)newseed & 0x7fffffffU;
%}
%int rand (void) {
%    seed = (seed * 1103515245U + 12345U) & 0x7fffffffU;
%    return (int)seed;
%}
	
	export operation getRandom[modulo:Integer] -> [res:Integer]
		var seed:Integer <- (locate self).getTimeOfDay.getMicroSeconds% # 2147483647
		
		seed <- (seed * 48271 + 0) & 2147483647
		res <- seed # (modulo + 1)
	end getRandom
  
	export operation getTwoValues -> [res:Integer, res2:Integer]
		res <- 10
		res2 <- 20
	end getTwoValues
  
    process
	
	
	%stdout.putstring[(view list2[0] as Peer).getName || "11\n"]
	(view m as Array.of[Peer]).addupper[per]
	for i:Integer <- 1 while i < 5 by i <- i + 1
	    (view m as Array.of[Peer]).addUpper[nil]
	end for
	for i:Integer <- 0 while i < 5 by i <- i + 1
	    if (view m as Array.of[Peer])[i] == nil then 
		(view m as Array.of[Peer]).setElement[i, Peer.create[i.asstring]]
	    end if
	    
	%	stdout.putstring[(view m as Array.of[Peer])[i].getName || "\n"]
	end for
	
	for i:Integer <- 0 while i < 10 by i <- i + 1
		stdout.putstring[self.getRandom[1].asString || "\n"]
	end for
	%for i:Integer <- 0 while i < 5 by i <- i + 1
	    
	%end for
	var k: Any <- per
	an <- Peer.create["Halla"]
	list2.addUpper[per]
	p <- (view an as Peer)
%	stdout.putstring[(view an as Peer).getName || "\n"]
	
	var num:Integer <- 2147483647

	stdout.putstring[('N').ord.asString || "\n"]
	stdout.putstring[('n').ord.asString || "\n"]
	stdout.putstring[self.hash[c, c.length].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Hello", 5].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Bassa", 5].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Ogilk", 5].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Hello", 5].asString || "  <-----------\n"]
  	stdout.putstring[num.asString || "  <-----------\n"]
	
	stdout.putstring[self.getTwoValues.asString || "\n"]
	
	if list2[0] == per then
%		stdout.putstring[c.hash.asString || "\n"]
	else
%		stdout.putstring[('N').ord.asString || "\n"]
	end if
%	var count:Integer <- 2147483647
%	loop
%	  count <- count + 1
%	  stdout.putstring[count.asString || "\n"]
%	  if count < 0 then 
%	      exit
%	  end if
 %    
%	end loop



    end process


end Test
