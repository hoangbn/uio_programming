export File
export Message
export HashFunction
export Random

%Random generator based on Linear congruential generator
const Random <- class Random
	export operation getRandom[modulo:Integer] -> [res:Integer]
		var seed:Integer <- (locate self).getTimeOfDay.getMicroSeconds # 2147483647
		
		seed <- (seed * 1103515245 + 12345) & 2147483647
		res <- seed # (modulo + 1)
	end getRandom
end Random

%Implemented as the typeobject specified in the The Emerald Programming Language Report
%const Handler <- object Handler
%	export operation nodeUp[n:Node, t:Time]
%		(locate self)$stdout.putstring["a Node went up\n"]
%	end nodeUp
%	
%	export operation nodeDown[n:Node, t:Time]
%		(locate self)$stdout.putstring["a Node went down\n"]
%	end nodeDown
%end Handler

const File <- immutable class File[filename:String, content:String]
	export operation getName -> [name:String]
		name <- filename
	end getName
	
	export operation getContent -> [str:String]
		str <- content
	end getcontent
end File

%Reason for that particular number for modulo was to limit the overflow
%In the report document page 25, there is an example program for typeobject and object that solves our requirement for a hash function
const HashFunction <- immutable object HashFunction
	const SecureDigest <- typeObject SecureDigest
		operation hash[str:String] -> [value:Integer]
	end SecureDigest
	
	%Without this operation other objects won't recognize its type, and we'll get error messages
	%Operation getSignature
	%Return its type SecureDigest
	export operation getSignature -> [sig:Signature]
		sig <- SecureDigest
	end getSignature
	
	%create an object with a hash function that conforms to the typeobject SecureDigest
	export operation createHashFunction[] -> [ret: SecureDigest]
		%Bernstein hash function used, therefore the name
		ret <- object BernsteinHash
			%operation hash
			%input: a string, in our case the content of a file
			%return: a hash value of the content
			%Reason for using modulo will be explained in the report
			export operation hash[str:String] -> [value:Integer]			
				var hash:Integer <- 0
				
				for i:Integer <- 0 while i < str.length by i <- i + 1
					hash <- (hash * 33 + str.getElement[i].ord) # 65075254
				end for
				value <- hash			
			end hash
		end BernsteinHash
	end createHashFunction
end HashFunction

%Used to send more than one result from server to a peer 
const Message <- class Message[aPeer:Any, data:Array.of[Any], hash:Integer]
	export operation getFrom -> [p:Any]
		p <- aPeer
	end getFrom
	
	export operation getData -> [d:Array.of[Any]]
		d <- data
	end getData

	export operation getHash -> [content:Integer]
		content <- hash
	end getHash		
end Message
