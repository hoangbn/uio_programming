Just a reminder.
in Main.m you can control the speed of execution. 
t - delays to test unavailable
tr - delays to see what happens between nodes, if is recommended to increase it if you see duplications of operation, since the random generator use the clock to make a random number.

actions - I have it on 50 now since the Nodes from planetlab was so slow that It would take forever to finish.

Have a good day
