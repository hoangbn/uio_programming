IFI-machines

Terminal 1 Server
-------------------------------------------------------------------------------
[hoangbn@lily HomeExam1x]$ emx -Rlily:16854 Helper.x Data.x Server.x Peer.x Main.x 
Emerald listening on port 21974 55d6, epoch 44187 ac9b
6 Nodes active
New file added to table
peer added to the new file
New file added to table
peer added to the new file
New file added to table
peer added to the new file
New file added to table
peer added to the new file
File exists, Add a peer to the list
File exists, Add a peer to the list
New file added to table
peer added to the new file
File exists, Add a peer to the list
File exists, Add a peer to the list
File exists, Add a peer to the list
New file added to table
peer added to the new file
New file added to table
peer added to the new file
New file added to table
peer added to the new file
File exists, Add a peer to the list
File exists, Add a peer to the list
File exists, Add a peer to the list
File exists, Add a peer to the list
New file added to table
peer added to the new file
Lord of the Rings was requested
There is no file with name: 'Lord of the Rings' in our database
Peer Four was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, Add a peer to the list
Lord of the Rings was requested
There is no file with name: 'Lord of the Rings' in our database
INF5510 - Distributed objects was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, Add a peer to the list
File exists, Add a peer to the list
File exists, Add a peer to the list
File exists, The list already contains the peer
Peer is already in the list
Peer One was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, Add a peer to the list
Now is the time to kill a node if you want
Peer Two was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, The list already contains the peer
Peer is already in the list
Lord of the Rings was requested
There is no file with name: 'Lord of the Rings' in our database
Peer Four was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, Add a peer to the list
New file added to table
peer added to the new file
Peer Four was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, Add a peer to the list
File exists, Add a peer to the list
File exists, Add a peer to the list
File exists, Add a peer to the list
Confucius says: Volume 1 was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, Add a peer to the list
File exists, Add a peer to the list
Confucius says: Volume 1 was requested
Server has the list of peers who has file
Found list of peers who has the file, sending it back to the one requested it
File exists, The list already contains the peer
Peer is already in the list
File exists, Add a peer to the list
Status of peers, their content will be displayed in their respective nodes
Peer 1 in Node lily.ifi.uio.no is alive 
Peer 2 in Node lily.ifi.uio.no is alive 
Peer 3 in Node lily.ifi.uio.no is alive 
Peer 4 in Node lily.ifi.uio.no is alive 
Peer 5 in Node lily.ifi.uio.no is alive 
The server has indexed these files:
0. INF5510 - Distributed objects with Secure Digest 59178112
1. The Hobbit - Desolation of Smaug with Secure Digest 49426460
2. Peer One with Secure Digest 28326917
3. Hello with Secure Digest 22871574
4. Peer Two with Secure Digest 49013106
5. Peer Four with Secure Digest 50794697
6. Peer Three with Secure Digest 47229839
7. Confucius says: Volume 1 with Secure Digest 55953789
8. Confucius says: Volume 2 with Secure Digest 17529281
9. Lord of the Rings with Secure Digest 27169944
-------------------------------------------------------------------------------
Terminal 2 Peer
-------------------------------------------------------------------------------
[hoangbn@lily HomeExam1x]$ emx -R
Emerald listening on port 16854 41d6, epoch 6648 19f8
File: INF5510 - Distributed objects added to peer
File: The Hobbit - Desolation of Smaug added to peer
The file: The Hobbit - Desolation of Smaug already exist in our storage device
File: Peer One added to peer
The file: INF5510 - Distributed objects already exist in our storage device
File: Hello added to peer
The file: Hello already exist in our storage device
We already have the file, no need to download it again
File 'INF5510 - Distributed objects' was requested
Sending the requested file back
File 'Peer One' was requested
Sending the requested file back
We already have the file, no need to download it again
We already have the file, no need to download it again
We already have the file, no need to download it again
File is unavailable/doesn't exist
File: Peer Four added to peer
Got File
File downloaded successfully
The file: The Hobbit - Desolation of Smaug already exist in our storage device
The file: Hello already exist in our storage device
We already have the file, no need to download it again
File is unavailable/doesn't exist
File: Confucius says: Volume 1 added to peer
Got File
File downloaded successfully
We already have the file, no need to download it again
File: Confucius says: Volume 2 added to peer
The number of files this peer has is 7
1 Filename: INF5510 - Distributed objects
2 Filename: The Hobbit - Desolation of Smaug
3 Filename: Peer One
4 Filename: Hello
5 Filename: Peer Four
6 Filename: Confucius says: Volume 1
7 Filename: Confucius says: Volume 2
-------------------------------------------------------------------------------
Terminal 3 Peer
-------------------------------------------------------------------------------
[hoangbn@lily HomeExam1x]$ emx -Rlily:16854
Emerald listening on port 17878 45d6, epoch 45060 b004
File: Peer One added to peer
The file: Peer One already exist in our storage device
File: INF5510 - Distributed objects added to peer
File: Peer Two added to peer
File is unavailable/doesn't exist
File: Peer Four added to peer
Got File
File downloaded successfully
The file: Peer One already exist in our storage device
File is unavailable/doesn't exist
There is no one sharing that file
Downloading failed
File: Lord of the Rings added to peer
We already have the file, no need to download it again
File: Confucius says: Volume 1 added to peer
The file: INF5510 - Distributed objects already exist in our storage device
Removed successful
We already have the file, no need to download it again
The file: Peer One already exist in our storage device
The number of files this peer has is 5
1 Filename: Peer One
2 Filename: INF5510 - Distributed objects
3 Filename: Peer Two
4 Filename: Lord of the Rings
5 Filename: Confucius says: Volume 1
-------------------------------------------------------------------------------
Terminal 4 Peer
-------------------------------------------------------------------------------
[hoangbn@lily HomeExam1x]$ emx -Rlily:16854
Emerald listening on port 20950 51d6, epoch 52047 cb4f
File: Peer One added to peer
The file: Peer One already exist in our storage device
File: Hello added to peer
File: Confucius says: Volume 1 added to peer
File: The Hobbit - Desolation of Smaug added to peer
The file: Hello already exist in our storage device
The file: Hello already exist in our storage device
The file: Confucius says: Volume 1 already exist in our storage device
File: Confucius says: Volume 2 added to peer
File: Peer Two added to peer
File 'Peer Two' was requested
Sending the requested file back
File is unavailable/doesn't exist
File: Peer Four added to peer
Got File
File downloaded successfully
File 'Peer Four' was requested
Sending the requested file back
We already have the file, no need to download it again
We already have the file, no need to download it again
File 'Confucius says: Volume 1' was requested
Sending the requested file back
File: Lord of the Rings added to peer
The file: Confucius says: Volume 2 already exist in our storage device
File 'Confucius says: Volume 1' was requested
Sending the requested file back
The number of files this peer has is 8
1 Filename: Peer One
2 Filename: Hello
3 Filename: Confucius says: Volume 1
4 Filename: The Hobbit - Desolation of Smaug
5 Filename: Confucius says: Volume 2
6 Filename: Peer Two
7 Filename: Peer Four
8 Filename: Lord of the Rings
-------------------------------------------------------------------------------
Terminal 5 Peer
-------------------------------------------------------------------------------
[hoangbn@lily HomeExam1x]$ emx -Rlily:16854
Emerald listening on port 19926 4dd6, epoch 45060 b004
File: Peer Three added to peer
File: Confucius says: Volume 1 added to peer
File is unavailable/doesn't exist
There is no one sharing that file
Downloading failed
File is unavailable/doesn't exist
There is no one sharing that file
Downloading failed
File: Peer Two added to peer
Removed successful
The file: Peer Three already exist in our storage device
File: Confucius says: Volume 1 added to peer
File is unavailable/doesn't exist
File: Peer One added to peer
Got File
File downloaded successfully
The file: Confucius says: Volume 1 already exist in our storage device
We already have the file, no need to download it again
We already have the file, no need to download it again
Removed successful
The file: Peer Three already exist in our storage device
File: The Hobbit - Desolation of Smaug added to peer
File is unavailable/doesn't exist
File: Confucius says: Volume 1 added to peer
Got File
File downloaded successfully
We already have the file, no need to download it again
The number of files this peer has is 5
1 Filename: Peer Three
2 Filename: Peer Two
3 Filename: Peer One
4 Filename: The Hobbit - Desolation of Smaug
5 Filename: Confucius says: Volume 1
-------------------------------------------------------------------------------
Terminal 6 Peer
-------------------------------------------------------------------------------
[hoangbn@lily HomeExam1x]$ emx -Rlily:16854
Emerald listening on port 18902 49d6, epoch 45060 b004
File: Peer One added to peer
The file: Peer One already exist in our storage device
File: Peer Two added to peer
File: Hello added to peer
File: Peer Four added to peer
File 'Peer Four' was requested
Sending the requested file back
File is unavailable/doesn't exist
File: INF5510 - Distributed objects added to peer
Got File
File downloaded successfully
Removed successful
File is unavailable/doesn't exist
File: Peer Two added to peer
Got File
File downloaded successfully
File 'Peer Four' was requested
Sending the requested file back
Removed successful
File: The Hobbit - Desolation of Smaug added to peer
We already have the file, no need to download it again
The number of files this peer has is 5
1 Filename: Peer One
2 Filename: Hello
3 Filename: INF5510 - Distributed objects
4 Filename: Peer Two
5 Filename: The Hobbit - Desolation of Smaug
-------------------------------------------------------------------------------
