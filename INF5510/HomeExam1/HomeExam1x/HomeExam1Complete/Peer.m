export Peer

const Peer <- class Peer[central:Server, name:String]
	var list: Array.of[Peer]
	var storage: Array.of[File] <- Array.of[File].empty	
	var names:String <- "Peer"
	var HashFunc:HashFunction
	var Rand:Random
	
	export operation checkAvailable -> [bool:Boolean]
		%This peer responded with a nil, We've to check whether this peer is alive or dead
		bool <- true
	end checkAvailable

	export operation getName -> [str:String]
		str <- name
	end getName
	
	%Remove a random file, simulate a peer unsharing a file, but since files are immutable, this is one way to do that
	export operation removeRandomFile -> [res:Boolean]
		var index: Integer <- 0
		var temp:Array.of[File] <- Array.of[File].empty
		res <- false
		
		%If the storage is empty
		if storage == nil | storage.upperbound = -1 then
			res <- false
		else
			index <- Rand.getRandom[storage.upperbound]
			%Else add everything but the content located with index
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				if i != index then
					temp.addUpper[storage[i]]
					res <- true
					
				end if
			end for
			(locate self)$stdout.putstring["Removed successful\n"]
			storage <- temp
		end if
		
		failure
			(locate self)$stdout.putstring["Failed in Remove\n"]
		end failure
	end removeRandomFile
	
	%Check if the Peer has the file
	operation containsFile[f:File] -> [bool:Boolean]
		bool <- false
		if storage.upperbound = -1 then
			bool <- false
		else
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				if storage[i] == f then
					bool <- true
					exit
				end if
			end for
		end if
		failure
			(locate self)$stdout.putstring["Failed in contains\n"]
		end failure
	end containsFile
	
	%Add a file to the peer and tell the server that it is sharing the file too
	export operation addFile[f:File] -> [bool:Boolean]
		if self.containsFile[f] then
			(locate self)$stdout.putstring["The file: " || f.getName || " already exist in our storage device\n"]
			bool <- false
		else 
			storage.addUpper[f]
			(locate self)$stdout.putstring["File: " || f.getName || " added to peer\n"]
			
			%Send information about the file to the server
			central.addFile[f.getName, HashFunc.hash[f.getContent], self]
			bool <- true
		end if
		
		failure
			(locate self)$stdout.putstring["Failed in add\n"]
		end failure
		%Even though it calls an operation inside itself, it is possible to die
		%unavailable
		%	bool <- false
		%end unavailable
	end addFile
	
	%debugging purpose
	export operation checkStorage[bool:Boolean]
		(locate self)$stdout.putstring["The number of files this peer has is " || (storage.upperbound + 1).asString || "\n"]
		if bool then
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				(locate self)$stdout.putstring[(i+1).asString || " Filename: " || storage[i].getName || "\nContent: " || storage[i].getContent || "\n"]
			end for
		else
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				(locate self)$stdout.putstring[(i+1).asString || " Filename: " || storage[i].getName || "\n"]
			end for
		end if
		
		%I commented this out since we dont need it, since the only time we call this operation is when we're done with the program, it shouldn't be unavailable
		%Peers that died cant come here since I removed them from the list of peers that is alive.
		%unavailable
		%	(locate self)$stdout.putstring[self.getName || " is Unavailable\n"]
		%end unavailable
	end checkStorage
	
	
	operation getFile[filename:String] -> [f:File]
		if storage == nil | storage.upperbound = -1 then
			(locate self)$stdout.putstring["The storage is empty\n"]
			f <- nil
		  else
		    %Use containsFile MAYBE!!?
			f <- nil
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				if storage[i].getName = filename then
					f <- storage[i]
					exit
				end if
			end for
			
			if f == nil then
				(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
			end if
		end if
		
		failure
			(locate self)$stdout.putstring["Failed in get\n"]
		end failure
		%unavailable
		%	f <- nil
		%end unavailable
	end getFile
	
	export operation completeRequest[s:String] -> [f:File]
		(locate self)$stdout.putstring["File '" || s || "' was requested\n"]
		f <- self.getFile[s]
		if f == nil then
			(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
			f <- nil
		else 
			(locate self)$stdout.putstring["Sending the requested file back\n"]
		end if
		
		failure
			(locate self)$stdout.putstring["Failed in complete\n"]
		end failure
		%navailable
			%f <- nil
		%end unavailable
	end completeRequest
	
	%Request a file from a peer, check the secure digest of the file, if it is different then return false
	export operation requestFileFromPeer[p:Peer, fname:String, hash:Integer] -> [bool:Boolean]
		bool <- false
		var tmp: File <- p.completeRequest[fname]
  
		if tmp == nil then
			(locate self)$stdout.putstring["File is unavailable or removed from sharing\n"]
		elseif HashFunc.hash[tmp.getContent] != hash then
			(locate self)$stdout.putstring["File is corrupt or wrong file, different hash value\n"]
		else
			bool <- true
			
			if self.addFile[tmp] then
				(locate self)$stdout.putstring["Got File\n"]
			else
				(locate self)$stdout.putstring["Something went wrong, couldn't save the file, probably we have the file already\n"]
				bool <- false
			end if
		end if

		unavailable
			bool <- false
		end unavailable
		
		failure
			(locate self)$stdout.putstring["Failed in request\n"]
		end failure
	end requestFileFromPeer
	
	%Make a new array from an existing array, but leave the content from the specified index out of the new one
	operation updateList[arr:Array.of[Any], index:Integer] -> [res:Array.of[Any]]
		var newArray: Array.of[Any] <- Array.of[Any].empty
		if arr == nil then
			res <- nil
		else
			for i:Integer <- 0 while i <= arr.upperbound by i <- i + 1
				if i != index then
					newArray.addUpper[arr[i]]
				end if
			end for
			
			%The array is empty
			if newArray.upperbound = -1 then
				res <- nil
			else
				res <- newArray
			end if
		end if
		
		%unavailable
			%res <- nil
		%end unavailable
		failure
			(locate self)$stdout.putstring["Failed in update List\n"]
		end failure
	end updateList
	
	%Maybe check if our local storage has a file with the specified name and just end it there if we have it
	%Download a file from a random Peer
	export operation downloadFile[filename:String] -> [bool:Boolean]
		%Check if we already have the file, if not then download
		if self.getFile[filename] !== nil then
			(locate self)$stdout.putstring["We already have the file, no need to download it again\n"]
		else 
			%Request a list of Peers who has the file from the Server
			var msg:Message <- central.getListOfPeers[filename]
			var rNum: Integer
			bool <- false	      
			
			%Test if the Message is empty
			if msg == nil or msg.getData == nil then
				(locate self)$stdout.putstring["There is no one sharing that file\n"]
			else
				%Message was not empty
				var pList:Array.of[Any] <- msg.getData
				%Pick a random Peer from the list
				loop
					%The only time the array should be nil is when the peer is unavailable or there are no peers sharing this particular file
					exit when bool = true
					rNum <- Rand.getRandom[pList.upperbound]
					bool <- self.requestFileFromPeer[(view pList[rNum] as Peer), filename, msg.getHash]

					%Update the pList if the peer didnt give us the file for whatever reason.
					if bool != true then
						pList <- self.updateList[pList, rNum]
						exit when pList == nil
					end if
				end loop
			end if
			
			%See whether we downloaded the file or not
			if bool then
				(locate self)$stdout.putstring["File downloaded successfully\n"]
			else
				(locate self)$stdout.putstring["Downloading failed\n"]
				%If the Message was not empty and the list of Peers is not empty then it means no Peers from the list we got from Server has the file/they are dead, so we should
				%notify the server about that
				if msg !== nil AND msg.getData !== nil AND msg.getData.upperbound > -1 then
					(locate self)$stdout.putstring["No peers in the list had the file or they went down\n"]
					central.fileNoLongerExist[filename]
				end if
			end if
		end if
		
		unavailable
			bool <- false
		end unavailable		
		
		failure
			(locate self)$stdout.putstring["Failed in download\n"]
		end failure
	end downloadFile
				
	initially
		HashFunc <- HashFunction.createHashFunction
		Rand <- Random.create
	end initially
end Peer
