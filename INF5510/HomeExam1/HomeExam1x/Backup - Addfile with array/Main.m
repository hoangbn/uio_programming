%How to run!!! remember to run all x files !!

const Handler <- object Handler
  export operation nodeUp[n:Node, t:Time]
    
  end nodeUp

  export operation nodeDown[n:Node, t:Time]

  end nodeDown

end Handler

%Outstream? log activities in a file?
const Main <- object Main
	%Variables
	var aServer: Server <- Server.create	
	const home <- locate aServer
	var aPeer: Peer
	var list: Array.of[Peer] <- Array.of[Peer].empty
	var all: NodeList
	var element: NodeListElement
	var away:Node
	var files: Array.of[File] <- Array.of[File].empty
	var Rand:Random <- Random.create 
	var t:Time <- Time.create[1,0]
	%Increase or decrease value to test
	var actions:Integer <- 100
	var number:Integer
	
	%Update the list of peers by ignoring the peer that was unavailable
	export operation renewList[arr:Array.of[Peer], index:Integer] -> [res:Array.of[Peer]]
		var newArray: Array.of[Peer] <- Array.of[Peer].empty
		if arr == nil then
			(locate self)$stdout.putstring["Seems like every peer in the system is dead\n"]
			res <- nil
		else
			for i:Integer <- 0 while i <= arr.upperbound by i <- i + 1
				if i != index then
					newArray.addUpper[arr[i]]
				end if
			end for
			
			res <- newArray
		end if
	end renewList

	process
		all <- home.getActiveNodes
		home$stdout.putString[(all.upperbound + 1).asstring || " Nodes active\n"]
		home.setNodeEventHandler[Handler]
		if all.upperbound = 5 then
			%creates peers and add information about the peer and its files
			%if there are 6 nodes available, then 1 node for server and a peer on each node
			%Move the server to a node, should be a dedicated machine
			move home to all[0]$theNode
				
			%create peers and move them to each node, which is 5 peers in 5 different nodes
			for i:Integer <- 1 while i <= all.upperbound by i <- i + 1	
				away <- all[i]$theNode
				aPeer <- Peer.create[aServer]
				list.addUpper[aPeer]					
				move aPeer to away
				aPeer.setName
			end for
			
			%Make files
			files.addUpper[File.create["Hello", "Hello World"]]
			files.addUpper[File.create["Lord of the Rings", "One Ring To Rule Them All"]]
			files.addUpper[File.create["The Hobbit - Desolation of Smaug", "I am Fire, I am Death"]]
			files.addUpper[File.create["Confucius says: Volume 1", "Give a man some fire, and he'll be warm for a day.\nSet a man on fire, and he'll be warm for the rest of his life"]]
			files.addUpper[File.create["Confucius says: Volume 2", "It is only when a mosquito lands on your testicles that you realize\nThere is always a way to solve problems without using violence"]]
			files.addUpper[File.create["INF5510 - Distributed objects", "Presentation of fundamental concepts of distributed objects including local and distributed objects, remote object invocation, communication, parameter passing principles, garbage collection, performance issues and typical applications. We also cover advanced type systems including concepts such as immutability and its application to distributed programming and access control. We also discuss the design of language constructs for parallelism. Throughout we use the programming language Emerald as a base. We dig into the implementation of distributed objects including virtual machines, compilation techniques, run-time typing, dynamic program loading, and how to do distributed garbage collection."]]
			files.addUpper[File.create["Peer One", "Each peer should be able to insert new ﬁles into the system. It shall do so by registering that it has a given ﬁle by sending name and the secure digest to the central server which then makes sure to insert data into a table (unless the ﬁle already exists) Moreover, the peer is inserted in the list of peers that has the speciﬁc ﬁle."]]
			files.addUpper[File.create["Peer Two", "A peer should be able to ask to be given the list of the peers that has the ﬁle with a given name. Moreover, peers may deliver the ﬁle on request."]]
			files.addUpper[File.create["Peer Three", "A peer will also be able to tell the central server that a given ﬁle no longer exists among peers."]]
			files.addUpper[File.create["Peer Four", " It is not necessary to store the ﬁles on disk, it is sufﬁcient that the ﬁles are objects that lies in the memory"]]
			
			%Randomly adding the files we have made to different peers
			%Add files to each peer
			for i:Integer <- 0 while i <= list.upperbound by i <- i + 1
				%Random number of files to be added to a peer, with <=, each peer will have atleast 1 file
				for j:Integer <- 0 while j <= Rand.getRandom[files.upperbound] by j <- j + 1
					%Add a random file picked from the list of files we have made
					var bool:Boolean <- false 
					loop
						exit when bool = true
						bool <- list[i].addFile[files[Rand.getRandom[files.upperbound]]]
					end loop
				end for
			end for
			
			%if list[2].addFile[files[1]] then
				%synd
			%end if
			
			%Every peer tells the server which file or files they are sharing
			for i:Integer <- 0 while i <= list.upperbound by i <- i + 1
				list[i].updateSharing
			end for
			
			%How many times to execute different operations
			for i:Integer <- 0 while i < actions by i <- i + 1
				number <- Rand.getRandom[2]
				%A random Peer tries to get or download a file
				if number = 0 then
					if list[Rand.getRandom[list.upperbound]].downloadFile[files[Rand.getRandom[files.upperbound]].getName] then
						%The file is unavailable
						%Check if any peer is down
					else

					end if
				%A random peer tries to add a new file from our repository of files to its storage
				elseif number = 1 then
				        if list[Rand.getRandom[list.upperbound]].addFile[files[Rand.getRandom[files.upperbound]]] then
						%Added a file
					else
						%File already exist or peer is unavailable
					end if
				elseif number = 2 then
					
				else
				end if
			end for

			%list[1].downloadFile["Lord of the Rings"]	
			%list[2].downloadFile["Confucius says: Volume 1"]
			%list[2].downloadFile["Lord of the Rings"]
			%list[0].checkStorage
			for i:Integer <- 0 while i <= list.upperbound by i <- i + 1
				list[i].checkStorage
			%list[1].requestFileFromPeer[ServerNode.getPeer[0], "Lord of the Rings"]
			end for
		else
			stdout.putString["Make 6 nodes, 1 for server and 5 others for peers, make sure the server is a dedicated machine, and it shouldn't hurt to do the same for peers also\n"]
		end if
		
		%ServerNode.printout
	
		%if list[0] == list[1] then
		%	(locate self)$stdout.putstring["It works?\n"]
		%else
		%       	(locate self)$stdout.putstring["Nope\n"]
		%end if
	end process
end Main
