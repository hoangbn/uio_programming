const Peer <- class Peer[name:Any]
      export operation getName -> [s:String]
	s <- (view name as String)
      end getName
end Peer

const Hash <- immutable object Hash
	const SecureDigest <- typeobject SecureDigest
		operation getHash[str:String] -> [ret:Integer]
	end SecureDigest
	
	export operation getSignature -> [s:Signature]
		s <- SecureDigest
	end getSignature
	
	export operation createHashFunction[] -> [res:SecureDigest]
		res <- 
			object aHash
				export operation getHash[str:String] -> [ret:Integer]
				
				end getHash
			end aHash
			
	end createHashFunction
	
end Hash

const Server <- class Server
  var list: Array.of[Peer] <- Array.of[Peer].empty
  
  export operation getlist -> [l:Array.of[Peer]]
    l <- list
  end getlist
  
end Server

const Handler <- object Handler
  export operation nodeUp[n:Node, t:Time]
    
  end nodeUp

  export operation nodeDown[n:Node, t:Time]
    
  end nodeDown
end Handler

const Test <- object Test
    var ser:Server <- Server.create
    var per: Peer <- Peer.create["YOLO"]
	var p: Peer
    var list2: Array.of[Any] <- Array.of[Any].empty
    var list: Array.of[Peer] <- Array.of[Peer].create[1]
    var a: Array.of[None] <- Array.of[None].create[1]
    var an: Any
    var c: String <- "Presentation of fundamental concepts of distributed objects including local and distributed objects, remote object invocation, communication, parameter passing principles, garbage collection, performance issues and typical applications. We also cover advanced type systems including concepts such as immutability and its application to distributed programming and access control. We also discuss the design of language constructs for parallelism. Throughout we use the programming language Emerald as a base. We dig into the implementation of distributed objects including virtual machines, compilation techniques, run-time typing, dynamic program loading, and how to do distributed garbage collection."
    var b: String <- "Give a man some fire, and he'll be warm for a day. \n Set a man on file, and he'll be warm for the rest of his life"
	var m: Any <- Array.of[Peer].empty
	const myHash: Hash <- Hash.createHashFunction
	
  operation hash[s:String, len:Integer] -> [res:Integer]
    var hash: Integer <- 0
    var str:String <- s
     
   for i:Integer <- 0 while i < len by i <- i + 1
      hash <- (33 * hash + s.getElement[i].ord) # 65075022
   end for
   res <- hash
  end hash
	
       
  
    process
	
    



	
	%stdout.putstring[(view list2[0] as Peer).getName || "11\n"]
	(view m as Array.of[Peer]).addupper[per]
	for i:Integer <- 1 while i < 5 by i <- i + 1
	    (view m as Array.of[Peer]).addUpper[nil]
	end for
	for i:Integer <- 0 while i < 5 by i <- i + 1
	    if (view m as Array.of[Peer])[i] == nil then 
		(view m as Array.of[Peer]).setElement[i, Peer.create[i.asstring]]
	    end if
	    
	%	stdout.putstring[(view m as Array.of[Peer])[i].getName || "\n"]
	end for

	%for i:Integer <- 0 while i < 5 by i <- i + 1
	    
	%end for
	var k: Any <- per
	an <- Peer.create["Halla"]
	list2.addUpper[per]
	p <- (view an as Peer)
%	stdout.putstring[(view an as Peer).getName || "\n"]
	
	var num:Integer <- 2147483647

	stdout.putstring[('N').ord.asString || "\n"]
	stdout.putstring[('n').ord.asString || "\n"]
	stdout.putstring[self.hash[c, c.length].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Hello", 5].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Bassa", 5].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Ogilk", 5].asString || "  <-----------\n"]
	stdout.putstring[self.hash["Hello", 5].asString || "  <-----------\n"]
  	stdout.putstring[num.asString || "  <-----------\n"]
	if list2[0] == per then
%		stdout.putstring[c.hash.asString || "\n"]
	else
%		stdout.putstring[('N').ord.asString || "\n"]
	end if
%	var count:Integer <- 2147483647
%	loop
%	  count <- count + 1
%	  stdout.putstring[count.asString || "\n"]
%	  if count < 0 then 
%	      exit
%	  end if
 %    
%	end loop



    end process


end Test
