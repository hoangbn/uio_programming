export File

%I will reuse the array since remove operation cant remove peers in the middle
%So a nil will be used to tell us that a space is free, and a counter(free_space)
%is used to check if there are any free spaces in the array(any nil)
const File <- class File[name:String, holder:Any]
	var listOfPeers: Array.of[Any] <- Array.of[Any].empty
	var free_space: Integer <- 0

	%When adding a peer there are 2 possibilities
	%1. Peer isnt in the list: Add the peer to the list
	%2. Peer is already there: No need to add it if it already contains the peer
	export operation addPeer[p:Any] -> [b:Boolean]
		b <- false
		
		%if there are free spaces in the array
		if free_space < listOfPeers.upperbound then
			for i:Integer <- 0 while i <= listOfPeers.upperbound by i <- i + 1
			    %Ignore everything else, find an index with an element nil, 
			    %then replace it with the new peer, substract free spaces
			    if listOfPeers[i] == nil then
				    listOfPeers.setElement[i, p]
				    free_space <- free_space - 1
			    end if
			end for
		%If there are no free spaces in the array
		else
			listOfPeers.addUpper[p]
		end if
	end addPeer
	
	%When removing a peer there are 3 possibilities
	%1. We encounter a nil: Just ignore it
	%2. We found the peer in the list: Remove it by setting the data in the index to nil
	%3. We didnt find the peer in the list: Something went wrong or the peer was already remove 
	export operation removePeer[p:Any]
		for i:Integer <- 0 while i <= listOfPeers.upperbound by i <- i + 1
			%have to handle nil before doing anything else
			if listOfPeers[i] == nil then
			%Ignore placeholders of nil
			%(locate self)$stdout.putstring["Nil ignored\n"]
			elseif listOfPeers[i] == p then
				listOfPeers.setElement[i, nil]
				free_space <- free_space - 1
			else
				(locate self)$stdout.putstring["The peer is already removed from list of peers who has this file\n"]
			end if
		end for
	end removePeer

end File
