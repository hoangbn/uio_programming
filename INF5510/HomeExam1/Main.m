%How to run!!! remember to run all x files !!

const Main <- object Main
	%Variables
	var aServer: Server <- Server.create	
	const home <- locate aServer
	var aPeer: Peer
	var list: Array.of[Peer] <- Array.of[Peer].empty
	var all: NodeList
	var element: NodeListElement
	var away:Node
    var files: Array.of[File] <- Array.of[File].empty
	var Rand:Random <- Random.create
	
	process
		all <- home.getActiveNodes
		home$stdout.putString[(all.upperbound + 1).asstring || " Nodes active\n"]
		move home to all[0]$theNode
		
		%Make files
		files.addUpper[File.create["Hello", "Hello World", false]]
		files.addUpper[File.create["Lord of the Rings", "One Ring To Rule Them All", false]]
		files.addUpper[File.create["The Hobbit - Desolation of Smaug", "I am Fire, I am Death", false]]
		files.addUpper[File.create["Confucius says: Volume 1", "Give a man some fire, and he'll be warm for a day.\nSet a man on fire, and he'll be warm for the rest of his life", false]]
		files.addUpper[File.create["Confucius says: Volume 2", "It is only when a mosquito lands on your testicles that you realize\nThere is always a way to solve problems without using violence", false]]
		files.addUpper[File.create["INF5510 - Distributed objects", "Presentation of fundamental concepts of distributed objects including local and distributed objects, remote object invocation, communication, parameter passing principles, garbage collection, performance issues and typical applications. We also cover advanced type systems including concepts such as immutability and its application to distributed programming and access control. We also discuss the design of language constructs for parallelism. Throughout we use the programming language Emerald as a base. We dig into the implementation of distributed objects including virtual machines, compilation techniques, run-time typing, dynamic program loading, and how to do distributed garbage collection.", false]]
		files.addUpper[File.create["Peer One", "Each peer should be able to insert new ﬁles into the system. It shall do so by registering that it has a given ﬁle by sending name and the secure digest to the central server which then makes sure to insert data into a table (unless the ﬁle already exists) Moreover, the peer is inserted in the list of peers that has the speciﬁc ﬁle.", false]]
		files.addUpper[File.create["Peer Two", "A peer should be able to ask to be given the list of the peers that has the ﬁle with a given name. Moreover, peers may deliver the ﬁle on request.", false]]
		files.addUpper[File.create["Peer Three", "A peer will also be able to tell the central server that a given ﬁle no longer exists among peers.", false]]
		files.addUpper[File.create["Peer Four", " It is not necessary to store the ﬁles on disk, it is sufﬁcient that the ﬁles are objects that lies in the memory", false]]
		
		%creates peers and add information about the peer and its files
		%if there are 6 nodes available, then 1 node for server and a peer on each node
		if all.upperbound = 5 then
			for i:Integer <- 1 while i < all.upperbound by i <- i + 1	
				away <- all[Rand.getRandom[all.upperbound]]$theNode
				aPeer <- Peer.create[aServer]
			  
				%aServer.addPeers[peers]
				list.addUpper[aPeer]
				move aPeer to away
				list[0].addFile[files[0]]
			end for
		end if
		list[2].addFile[files[1]]
		
		list[0].shareAllFiles
		list[0].updateSharing
		
		list[2].shareAllFiles
		list[2].updateSharing
		
		list[1].downloadFile["Lord of the Rings"]
		list[2].downloadFile["Confucius says: Volume 1"]
		list[2].downloadFile["Lord of the Rings"]
		%list[0].checkStorage
		for i:Integer <- 0 while i <= list.upperbound by i <- i + 1
			%list[i].checkStorage
		%list[1].requestFileFromPeer[aServer.getPeer[0], "Lord of the Rings"]
		end for
		
		%Make a loop and a random function
		%aServer.printout
	
		%if list[0] == list[1] then
		%	(locate self)$stdout.putstring["It works?\n"]
		%else
		%       	(locate self)$stdout.putstring["Nope\n"]
		%end if
		else
			stdout.putString["Make a total of 6 nodes, 1 for the server and it should be on a dedicated machine,\n and 5 others for the peers, it doesnt hurt to place them on different machines too\n"]
		end if
	end process
end Main
