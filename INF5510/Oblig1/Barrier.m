%Mandatory Assignment 1
%Exercise 1 - Barrier

const Barrier <- object Barrier
  const limit <- 4
  const run <- 3
  var count: Integer <- 0  

  const monObj <- monitor object monObj
    const stop: Condition <- Condition.create
    const open: Condition <- Condition.create
    
    export operation gate[num:Integer]   
      count <- count + 1
      stdout.putstring["Process "||num.asstring||" is waiting at the barrier \n"]
      if count < limit then
	wait stop
      else
	signal open
      end if
      stdout.putstring["Process "||num.asstring||" is went through the barrier \n"]
    end gate

    export operation openGate
      if count < limit then
	stdout.putstring["The barrier is still closed\n"]
	wait open
      end if
      stdout.putstring["The barrier is now open\n"]

      loop
	count <- count -1
	signal stop
	exit when count = 0
      end loop

    end openGate


    initially
      stdout.putstring["Starting Barrier Program\n"]
    end initially

  end monObj

  const p1 <- object p1
  process
      var i: Integer <- 0
      loop
	exit when i = run
	monObj.gate[1]
	i <- i + 1
      end loop
    end process
  end p1

  const p2 <- object p2
   process
      var i: Integer <- 0
      loop
	exit when i = run
	monObj.openGate
	i <- i + 1
      end loop
    end process
  end p2

  const p3 <- object p3
    process
      var i: Integer <- 0
      loop
	exit when i = run
	monObj.gate[2]
	i <- i + 1
      end loop
    end process
  end p3

  const p4 <- object p4
    process
      var i: Integer <- 0
      loop
	exit when i = run
	monObj.gate[3]
	i <- i + 1
      end loop
    end process
  end p4

  process
     var i: Integer <- 0
      loop
	exit when i = run
	monObj.gate[4]
	i <- i + 1
      end loop
  end process	


end Barrier
