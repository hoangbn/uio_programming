%Mandatory Assignment 1
%Exercise 2 - Producer/Consumer

const Buffer <- object Buffer
  const limit <- 30
  var count: Integer <- 0
  var buffer_size: Integer <- 2
  var buf: Array.of[Integer] <- Array.of[Integer].create[2]
  
  const monObj <- monitor object monObj
    const empty: Condition <- Condition.create
    const full: Condition <- Condition.create
    
%add a number from the producer to the buffer
    export operation AddToBuffer[number: Integer]
      loop
	if count = buffer_size then
	  wait full
	else
	  exit
	end if
      end loop
      
      count <- count + 1
      buf.addUpper[number]
      stdout.putstring["Producer produce "||number.asstring||"\n"]
      if count > 0 then
	signal empty
      end if

    end AddToBuffer

%remove a number from the buffer and return it to the consumer
    export operation RmFromBuffer -> [number: Integer]
      loop
	if count = 0 then
	  stdout.putstring["Waiting\n"]
	  wait empty
	else
	  exit
	end if
      end loop

      count <- count - 1
      number <- buf.removeUpper
      stdout.putstring["Consumer consumes "||number.asstring||"\n"]
      if count < 2 then
	signal full
      end if

  
    end RmFromBuffer


    initially
      stdout.putstring["Starting Producer/Consumer\n"]
    end initially

   end monObj

  const Consumer <- object Consumer
    var num:Integer <- 0
    process
      const here <- locate self
      const one <- Time.create[0, 100]

      for i:Integer <- 0 while i < limit by i <- i + 1
	num <- monObj.RmFromBuffer
       	
	if i # 3 = 0 then
	 %stdout.putstring["Consumer "||i.asstring||"\n"]
	  here.delay[one]
	end if
      end for
    end process 
  end Consumer
    
  process
      const here <- locate self
      const one <- Time.create[0, 100]
      for i:Integer <- 0 while i < limit by i <- i + 1
	monObj.AddToBuffer[i]
	if i # 5 = 0 then
	  %stdout.putstring["Producer "||i.asstring||"\n"]
	  here.delay[one]
	end if
      end for
  end process

end Buffer
