
const Times <- object Times
	const NodeHome <- locate self
	var NodeAway: Node
	%var timeArray: Array.of[Time] <- Array.of[Time].empty
	%var localTime: Time
	var AllNodes: NodeList
	var nodeElem: NodeListElement


	process
		NodeHome$stdout.PutString["Starting on " || NodeHome$name || "\n"]
		AllNodes <- NodeHome.getActiveNodes
		NodeHome$stdout.PutString[(AllNodes.upperbound + 1).asString || " nodes active.\n"]
		
		NodeHome$stdout.putstring[NodeHome.getTimeOfDay.asDate || "\n"]
		for i: Integer <- 1 while i <= AllNodes.upperbound by i <- i + 1
			NodeAway <- AllNodes[i]$theNode
			move Times to NodeAway
			
		    NodeAway.delay[Time.create[10, 0]]
			NodeAway$stdout.PutString["Timecollector was here\n"]
			%NodeAway.delay[Time.create[1, 0]]
			NodeAway$stdout.putstring[NodeAway.getTimeOfDay.asDate || "\n"]
		end for
		move Times to NodeHome
		NodeHome$stdout.putstring[NodeHome.getTimeOfDay.asDate || "\n"]
		NodeHome$stdout.PutString["Back home.\n"]
	end process
end Times
