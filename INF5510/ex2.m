%Exercise 2

const Person <- class Person[name:String]
  %When setting variables do it in the initially
  var nm: String
			       
  initially
    nm <- name
  end initially
  
  %Process is used for operations it seems
  process
    stdout.putstring[name ||"Name: " ||nm||"\n"]
  end process

end Person

const Teacher <- class Teacher(Person)[pos:String] 
  %Of some reason class export has to be the first function in a subclass
  class export function getSuperClass -> [r:Any]
    r <- Person
  end getSuperClass
    
  var position: String

  initially
    position <- pos
  end initially

   process
    stdout.putstring["Name: "||name|| " and position: "||position||"\n"]
  end process
end Teacher

const Test <- object Test
  %NB when asking for classes remember to use uppercase!!
   var Pers: Any
  var Teac: Any
  var name: String
  var position: String
 

  initially
    name <- "Bao"
    position <- "Super"
    Pers <- Person.create[name]
    Teac <- Teacher.create[name, position]
  end initially
end Test
