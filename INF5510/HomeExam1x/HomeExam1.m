export Peer
export Server
export Data
export File


%I will reuse the array instead of making a new one since remove operation cant remove peers in the middle
%So a nil will be used to tell us that a space is free, and a counter(free_space)
%is used to check if there are any free spaces in the array(any nil)
const Data <- class Data[name:String]
	var listOfPeers: Array.of[Any] <- Array.of[Any].empty
	var free_space: Integer <- 0
	var tmp: Array.of[Any]
	
	%Get the string
	export operation getFileName -> [s:String]
		s <- name
	end getFileName
	
	export operation getList -> [aList:Array.of[Any]]
		%make sure to empty it before adding
		if listOfPeers.upperbound == -1 then
			tmp <- nil
		else
			tmp <- Array.of[Any].empty
		end if
				
		for i:Integer <- 0 while i <= listOfPeers.upperbound by i <- i + 1
			if listOfPeers[i] == nil then
				%Its a nil so do nothing, just skip it
			else
				tmp.addUpper[listOfPeers[i]]
			end if
		end for
		%return the newly made array of peers
		aList <- tmp
	end getList
	
	%Check if a peer is already in the list
	export operation containsPeer[p:Any] -> [bool:Boolean]
	       bool <- false
	       for i:Integer <- 0 while i <= listOfPeers.upperbound by i <- i + 1
			if listOfPeers[i] == p then
				%stdout.putstring["Contained Peer\n"]
				bool <- true
				exit
			end if
	       end for
	end containsPeer
	
	%When adding a peer there are 2 possibilities
	%1. Peer isnt in the list: Add the peer to the list
	%2. Peer is already there: No need to add it if it already contains the peer
	export operation addPeer[p:Any] -> [bool:Boolean]
		bool <- false
		if self.containsPeer[p] then
			(locate self)$stdout.putstring["The list already contains the peer\n"]
		else
			bool <- true
			%if there are free spaces in the array
			if free_space < listOfPeers.upperbound then
				for i:Integer <- 0 while i <= listOfPeers.upperbound by i <- i + 1
					%Ignore everything else, find an index with an element nil, 
					%then replace it with the new peer, substract free spaces
					if listOfPeers[i] == nil then
						listOfPeers.setElement[i, p]
						free_space <- free_space - 1
						exit
					end if
				end for
			%If there are no free spaces in the array
			else
				listOfPeers.addUpper[p]
			end if
		end if
	end addPeer
					
	%When removing a peer there are 3 possibilities
	%1. We encounter a nil: Just ignore it
	%2. We found the peer in the list: Remove it by setting the data in the index to nil
	%3. We didnt find the peer in the list: Something went wrong or the peer was already remove 
	export operation removePeer[p:Any]
		for i:Integer <- 0 while i <= listOfPeers.upperbound by i <- i + 1
			%have to handle nil before doing anything else
			if listOfPeers[i] == nil then
			%Ignore placeholders of nil
			%(locate self)$stdout.putstring["Nil ignored\n"]
			elseif listOfPeers[i] == p then
				listOfPeers.setElement[i, nil]
				free_space <- free_space + 1
			else
				(locate self)$stdout.putstring["The peer is already removed from list of peers who has this file\n"]
			end if
		end for
	end removePeer
end Data

const File <- class File[filename:String, content:String, sharing:Boolean]
	export operation getName -> [name:String]
		name <- filename
	end getName
	
	export operation getSharing -> [sh:Boolean]
		sh <- sharing
	end getSharing
	
	export operation changeSharing[bool:Boolean]
		sharing <- bool
	end changeSharing
	
	export operation getContent -> [str:String]
		str <- content
	end getcontent
end File


const SecureDigest <- typeObject SecureDigest
	operation hash[str:String] -> [hash:BitChunk]
end SecureDigest

const FNVHash <- class FNVHash
	export operation hash[str:String] -> [hash:BitChunk]

	end hash
end FNVHash

const Metadata <- class Metadata[filename:String, hash:Any]
	export operation getName -> [name:String]
		name <- filename
	end getName
	
	export operation getHash -> [ret:Any]
		ret <- hash
	end getHash
end Metadata

const Message <- class Message[who:Any, type:Character, content:Any]
	export operation getFrom -> [p:Any]
		p <- who
	end getFrom
	
	export operation getType -> [c:Character]
		c <- type
	end getType

	export operation getContent -> [cont:Any]
		cont <- content
	end getContent		
end Message

const Peer <- class Peer[central:Server]
	const home <- locate self
	var list: Array.of[Peer]
	var storage: Array.of[File] <- Array.of[File].empty
	var tmp: File
	var names:String <- "Peer"
	
	operation containsFile[f:File] -> [bool:Boolean]
		bool <- false
		if storage.upperbound = 0 then
			bool <- false
		else
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				if storage[i] == f then
					bool <- true
					exit
				end if
			end for
		end if
	end containsFile
	
	%Used for adding files to a Peer
	export operation addFile[f:File] 
		if self.containsFile[f] then
			(locate self)$stdout.putstring["The file already exist in our storage device\n"]
		else 
			storage.addUpper[f]
			(locate self)$stdout.putstring["File added to peer\n"]
		end if
	end addFile
	
	%debugging purpose
	export operation checkStorage
		(locate self)$stdout.putstring["The number of files this peer has is " || (storage.upperbound + 1).asString || "\n"]
		for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
			(locate self)$stdout.putstring[(i+1).asString || " Filename: " || storage[i].getName || "\nContent: " || storage[i].getContent || "\n"]
		end for
	end checkStorage
	
	operation getFile[filename:String] -> [f:File]
		if storage.upperbound = 0 then
			(locate self)$stdout.putstring["The storage is empty\n"]
			f <- nil
		else
			var count:Integer <- 0
			loop
				%Test if it is the right file, return it if true
				if storage[count].getName = filename then
					f <- storage[count]
					exit
				end if
				count <- count + 1
				
				%We've tried to get the file, but it seems the file is unavailable/doesn't exist
				if count > storage.upperbound then
					(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
					f <- nil
				end if
			end loop
		end if
	end getFile
	
	export operation completeRequest[s:String] -> [f:File]
		(locate self)$stdout.putstring["File '" || s || "' was requested\n"]
		f <- self.getFile[s]
		if f == nil then
			(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
			f <- nil
		else 
			(locate self)$stdout.putstring["Sending the requested file back\n"]
		end if
	end completeRequest
	
	export operation requestFileFromPeer[p:Peer, fname:String] -> [bool:Boolean]
		bool <- false
		
		tmp <- p.completeRequest[fname]
		if tmp == nil then
			%Maybe check if the peer is unavailable?
			(locate self)$stdout.putstring["its nil\n"]
		else
			bool <- true
			%Run the string through hash function and compare it to servers hash value?
			self.addFile[tmp]
			(locate self)$stdout.putstring["Got File\n"]
		end if
	end requestFileFromPeer
	
	%only request list or download too, make a local Array list in this operation?
	export operation requestListOfPeers[filename:String] -> [pList:Array.of[Any]]
		%make sure the list is empty, necessary?
		pList <- central.getListOfPeers[filename]
		if pList == nil then
			(locate self)$stdout.putstring["There is no one sharing that file\n"]
		else
		        (locate self)$stdout.putstring["Got list of peers for that file\n"]	  
		end if
	end requestListOfPeers

	export operation downloadFile[filename:String]
		var pList: Array.of[Any] <- self.requestListOfPeers[filename]
		var bool:Boolean <- false
		%list <- (view pList as Array.of[Peer])
	      
		if pList == nil then
			%(locate self)$stdout.putstring["There is no one sharing that file\n"]
		else
			
		       	for i:Integer <- 0 while i <= pList.upperbound by i <- i + 1
				if self.requestFileFromPeer[(view pList[i] as Peer), filename] then
					bool <- true
					exit
				end if
			end for
		end if

		if bool then
			(locate self)$stdout.putstring["File downloaded successfully\n"]
		else
			(locate self)$stdout.putstring["Downloading failed, tell server\n"]
		end if

	end downloadFile

	export operation unshareAFile
	
	end unshareAFile
	
	export operation unshareAllFiles
	
	end unshareAllFiles
	
	export operation shareAFile
	
	end shareAFile
	
	export operation shareAllFiles
		if storage.upperbound = -1 then
			(locate self)$stdout.putstring["No file on this peer\n"]
		else
			(locate self)$stdout.putstring["Make every file in the storage sharable\n"]
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				storage[i].changeSharing[true]
			end for
		end if
	end shareAllFiles
	
	%Make a list of files to share? msg class? use the sharing status
	export operation updateSharing
		(locate self)$stdout.putstring["send info of files to Server\n"]
		for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
		        (locate self)$stdout.putstring[storage[i].getName || " \n"]
			central.addFile[storage[i].getName, self]
		end for
	end updateSharing
	
 

	export operation hello
	    (locate self)$stdout.putstring["Called by server\n"]
	end hello
end Peer

const Server <- class Server
	var list: Array.of[Peer] <- Array.of[Peer].empty
	var listOfFiles: Array.of[Data] <- Array.of[Data].empty
	var check: Integer
	
	var names:String <- "Server"

	export operation addPeers[p:Peer]
		list.addUpper[p]
	end addPeers
	
	%make a compare on hash value, if we have 2 files with the same name
	%If the file exist, then return the index of where the file is, if not then return -1
	operation containsFile[filename:String] -> [index:Integer]
		index <- -1
		for i:Integer <- 0 while i <= listOfFiles.upperbound by i <- i + 1
			if filename = listOfFiles[i].getFileName then
				index <- i
			end if
		end for
	end containsFile
	
	%TODO add a hash value
	export operation addFile[filename:String, aPeer:Any]
		check <- self.containsFile[filename]
	       
		%The file already exist, just add the peer in the list to those who has the file
		if check >= 0 then
			(locate self)$stdout.putstring["File exists, "]
			if listOfFiles[check].addPeer[aPeer] then
				(locate self)$stdout.putstring["Add a peer to the list\n"]
			else
				(locate self)$stdout.putstring["Peer is already in the list\n"]
			end if
		else
			(locate self)$stdout.putstring["New file added to table\n"]
			listOfFiles.addUpper[Data.create[filename]]
			check <- self.containsFile[filename]
			if !listOfFiles[check].addPeer[aPeer] then
				(locate self)$stdout.putstring["This should never happen!!!\n"]
			end if
			(locate self)$stdout.putstring["peer added to the new file\n"]
		end if
	end addFile
	
	export operation getListOfPeers[filename:String] -> [aList:Array.of[Any]]
		check <- self.containsFile[filename]
       		(locate self)$stdout.putstring[filename || " was requested\n"]
		if check >= 0 then
			(locate self)$stdout.putstring["Server has the list of peers who has the file\n"]
			aList <- listOfFiles[check].getList
			if aList == nil then
				(locate self)$stdout.putstring["There is no one who share this particular file\n"]
			else
				(locate self)$stdout.putstring["Found list of peers who has the file, sending it back to the one requested it\n"]	 
			end if
		else
			aList <- nil
			(locate self)$stdout.putstring["There is no file with name: '" || filename || "' in our database\n" ]
		end if
	end getListOfPeers
	
	export operation getPeer[index:Integer] -> [p:Peer]
		p <- list[index]
	end getPeer
	
	export operation printout
		for i:Integer <- 0 while i <= list.upperbound by i <- i + 1
			list[i].hello
		end for
	end printout

end Server
