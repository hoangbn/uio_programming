export Server

%Server doesnt need unavailable since we assume it can not crash
const Server <- class Server
	%var list: Array.of[Peer] <- Array.of[Peer].empty
	var listOfFiles: Array.of[Data] <- Array.of[Data].empty
	var check: Integer
	
	var names:String <- "Server"

%	export operation addPeers[p:Peer]
%		list.addUpper[p]
%	end addPeers
	
	%make a compare on hash value, if we have 2 files with the same name
	%If the file exist, then return the index of where the file is, if not then return -1
	operation containsFile[filename:String] -> [index:Integer]
		index <- -1
		for i:Integer <- 0 while i <= listOfFiles.upperbound by i <- i + 1
			if filename = listOfFiles[i].getFileName then
				index <- i
			end if
		end for
	end containsFile
	
    export operation addFile[fNameArray:Array.of[String], hashArray:Array.of[Integer], aPeer:Any]
	    var filename:String
	    var value:Integer
	    for i:Integer <- 0 while i <= fNameArray.upperbound by i <- i + 1
			filename <- fNameArray[i]
			value <- hashArray[i]
			check <- self.containsFile[filename]
	       
			%The file already exist, just add the peer in the list to those who has the file
			if check >= 0 then
			        (locate self)$stdout.putstring["File exists, "]
			        if listOfFiles[check].addPeer[aPeer] then
			                (locate self)$stdout.putstring["Add a peer to the list\n"]
			        else
				        (locate self)$stdout.putstring["Peer is already in the list\n"]
			        end if
			else
			        (locate self)$stdout.putstring["New file added to table\n"]
			        listOfFiles.addUpper[Data.create[filename, value]]
			        check <- self.containsFile[filename]
			        if !listOfFiles[check].addPeer[aPeer] then
				        (locate self)$stdout.putstring["This should never happen!!!\n"]
			        end if
			        (locate self)$stdout.putstring["peer added to the new file\n"]
			end if
	    end for
	end addFile
	
	%Send a datapack with an array and the hash value for the file ?
	export operation getListOfPeers[filename:String] -> [msg:Message]
		check <- self.containsFile[filename]
	       	var aList: Array.of[Any]
		var hash:Integer

       		(locate self)$stdout.putstring[filename || " was requested\n"]
		if check >= 0 then
			(locate self)$stdout.putstring["Server has the list of peers who has the file\n"]
			aList <- listOfFiles[check].getList
			
			if aList == nil then
				(locate self)$stdout.putstring["There is no one who share this particular file\n"]
			        hash <- nil
			else
				(locate self)$stdout.putstring["Found list of peers who has the file, sending it back to the one requested it\n"]
				hash <- listOfFiles[check].getHash
			end if
		else
			aList <- nil
			hash <- nil
			(locate self)$stdout.putstring["There is no file with name: '" || filename || "' in our database\n" ]
		end if
		
		msg <- Message.create[self, aList, hash]

		%Necessary? we can just assume that the server is available at all times.
		unavailable
			%Were pretty screwed if the server is unavailable
			(locate self)$stdout.putstring["Currently unavailable\n"]
			msg <- nil
	        end unavailable
	end getListOfPeers
	
	export operation fileNoLongerExist[filename:String]
		check <- self.containsFile[filename]
		if check >= 0 then
			listOfFiles[check].reset
			(locate self)$stdout.putstring["Removed the list of peers who had this file " || filename || "\n"]
		else
		  (locate self)$stdout.putstring["The file '" || filename || "' didn't exist in the first place !!\n"]
		end if
	end fileNoLongerExist


%	export operation getPeer[index:Integer] -> [p:Peer]
%		p <- list[index]
%	end getPeer
	
%	export operation printout
%		for i:Integer <- 0 while i <= list.upperbound by i <- i + 1
%			list[i].hello
%		end for
%	end printout

end Server
