export Peer

%add unavailable everywhere since it can crash at any time
const Peer <- class Peer[central:Server]
    var name:String
	var list: Array.of[Peer]
	var storage: Array.of[File] <- Array.of[File].empty	
	var names:String <- "Peer"
	var HashFunc:HashFunction
	var Rand:Random
	
	%This operation will always respond with a true boolean if it is alive, else a false
	export operation checkAvailable -> [bool:Boolean]
		%This peer responded with a nil, We've to check whether this peer is alive or dead
		bool <- true
		
		unavailable
			bool <- false
		end unavailable
	end checkAvailable
	
	export operation setName
		name <- (locate self)$name
	end setName

	export operation getName -> [str:String]
		str <- name

		unavailable
			str <- nil
		end unavailable
	end getName

	%Check if the Peer has the file
	operation containsFile[f:File] -> [bool:Boolean]
		bool <- false
		if storage.upperbound = 0 then
			bool <- false
		else
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				if storage[i] == f then
					bool <- true
					exit
				end if
			end for
		end if
		
	    unavailable
			bool <- false		 	
	    end unavailable
	end containsFile
	
	%Add a file to the peer and tell the server that it is sharing the file too
	export operation addFile[f:File] -> [bool:Boolean]
		if self.containsFile[f] then
			(locate self)$stdout.putstring["The file: " || f.getName || " already exist in our storage device\n"]
			bool <- false
		else 
			storage.addUpper[f]
			(locate self)$stdout.putstring["File: " || f.getName || " added to peer\n"]
			bool <- true
		end if

		unavailable
			bool <- false
		end unavailable
	end addFile
	
	%debugging purpose
	export operation checkStorage
		(locate self)$stdout.putstring["The number of files this peer has is " || (storage.upperbound + 1).asString || "\n"]
		for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
			(locate self)$stdout.putstring[(i+1).asString || " Filename: " || storage[i].getName || "\nContent: " || storage[i].getContent || "\n"]
		end for
	end checkStorage
	
	operation getFile[filename:String] -> [f:File]
		if storage.upperbound = 0 then
			(locate self)$stdout.putstring["The storage is empty\n"]
			f <- nil
		  else
		    %Use containsFile MAYBE!!?
			var count:Integer <- 0
			loop
				%Test if it is the right file, return it if true
				if storage[count].getName = filename then
					f <- storage[count]
					exit
				end if
				count <- count + 1
				
				%We've tried to get the file, but it seems the file is unavailable/doesn't exist
				if count > storage.upperbound then
					(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
					f <- nil
				end if
			end loop
		end if

		unavailable
			f <- nil
		end unavailable
	end getFile
	
	export operation completeRequest[s:String] -> [f:File]
		(locate self)$stdout.putstring["File '" || s || "' was requested\n"]
		f <- self.getFile[s]
		if f == nil then
			(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
			f <- nil
		else 
			(locate self)$stdout.putstring["Sending the requested file back\n"]
		end if
		unavailable
			f <- nil
		end unavailable
	end completeRequest
	
	export operation requestFileFromPeer[p:Peer, fname:String, hash:Integer] -> [bool:Boolean]
		bool <- false
		var tmp: File <- p.completeRequest[fname]
  
		if tmp == nil then
			%Maybe check if the peer is unavailable?
			(locate self)$stdout.putstring["File is unavailable or removed from sharing\n"]
		elseif HashFunc.hash[tmp.getContent] != hash then
			(locate self)$stdout.putstring["File is corrupt or wrong file, different hash value\n"]
		else
			bool <- true
			%Run the string through hash function and compare it to servers hash value?
			if self.addFile[tmp] then
				(locate self)$stdout.putstring["Got File\n"]
			else
				(locate self)$stdout.putstring["Something went wrong, couldn't save the file\n"]
				bool <- false
			end if
		end if

		unavailable
			bool <- false
		end unavailable
	end requestFileFromPeer
	
	%only request list or download too, make a local Array list in this operation?
%	export operation requestListOfPeers[filename:String] -> [pList:Array.of[Any]]
		%make sure the list is empty, necessary?
		
	%	pList <- (view msg.getData as Array.of[Any])
	%	if pList == nil then
	%		(locate self)$stdout.putstring["There is no one sharing that file\n"]
	%	else
	%	     (locate self)$stdout.putstring["Got list of peers for that file\n"]	  
	%	end if

	%	unavailable
	%		pList <- nil
	%	end unavailable
%	end requestListOfPeers
	
	%Make a new array from an existing array, but leave the content from the specified index out of the new one
	operation updateList[arr:Array.of[Any], index:Integer] -> [res:Array.of[Any]]
		var newArray: Array.of[Any] <- Array.of[Any].empty
		if arr == nil then
			res <- nil
		else
			for i:Integer <- 0 while i <= arr.upperbound by i <- i + 1
				if i != index then
					newArray.addUpper[arr[i]]
				end if
			end for
			
			%The array is empty
			if newArray.upperbound = -1 then
				res <- nil
			else
				res <- newArray
			end if
		end if
		
		unavailable
			res <- nil
		end unavailable
	end updateList
	
	%Maybe check if our local storage has a file with the specified name and just end it there if we have it
	export operation downloadFile[filename:String] -> [bool:Boolean]
		var msg:Message <- central.getListOfPeers[filename]
		var rNum: Integer
		bool <- false	      
		if msg == nil or msg.getData == nil then
			(locate self)$stdout.putstring["There is no one sharing that file\n"]
		else
			var pList:Array.of[Any] <- msg.getData
				%Pick a random Peer from the list
			loop
				%The only time the array should be nil is when the peer is unavailable or there are no peers sharing this particular file
				exit when bool = true
				rNum <- Rand.getRandom[pList.upperbound]
				bool <- self.requestFileFromPeer[(view pList[rNum] as Peer), filename, msg.getHash]
				%Update the pList if the peer didnt give us the file for whatever reason.
				if bool != true then
					pList <- self.updateList[pList, rNum]
					exit when pList == nil
				end if
			end loop
				
		    %for i:Integer <- 0 while i <= pList.upperbound by i <- i + 1
				%TODO should we remove the peer from server when we find it unavailable
			%	if self.requestFileFromPeer[(view pList[i] as Peer), filename, msg.getHash] then	    
			%		bool <- true
			%		exit
			%	end if
			%end for
		end if

		if bool then
			(locate self)$stdout.putstring["File downloaded successfully\n"]
		else
			(locate self)$stdout.putstring["Downloading failed\n"]
			if msg !== nil AND msg.getData !== nil AND msg.getData.upperbound > -1 then
				(locate self)$stdout.putstring["No peers in the list had the file or they went down\n"]
				central.fileNoLongerExist[filename]
			end if
		end if
		
		unavailable
			bool <- false
		end unavailable		
	end downloadFile
				
	%Make a list of files to share? msg class? use the sharing status, or just send an array and the server can add each of them, but we will have to make a hash array too.
	export operation updateSharing
	       	var fNameArray:	Array.of[String] <- Array.of[String].empty			
	       	var hashArray: Array.of[Integer] <- Array.of[Integer].empty
		(locate self)$stdout.putstring["Send info of files to Server\n"]
		for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
		       % (locate self)$stdout.putstring[storage[i].getName || " \n"]
			%add a filename and its hash to the server
			%modify the operations so we can send an array of filenames and an array of hashes corresponding to the content of the files
			fNameArray.addUpper[storage[i].getName]
			hashArray.addUpper[HashFunc.hash[storage[i].getContent]]
		end for

		central.addFile[fNameArray, hashArray, self]
	end updateSharing
	
 

	export operation hello
	    (locate self)$stdout.putstring["Called by server\n"]
	end hello
    
	initially
		HashFunc <- HashFunction.createHashFunction
		Rand <- Random.create
	end initially
end Peer
