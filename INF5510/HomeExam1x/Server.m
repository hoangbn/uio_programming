export Server

%Server doesnt need unavailable since we assume it can not crash
%And since it just returns call it doesnt need unavailable body in its operations
const Server <- class Server
	var listOfFiles: Array.of[Data] <- Array.of[Data].empty
	var check: Integer
	var names:String <- "Server"
	
	%make a compare on hash value, if we have 2 files with the same name
	%If the file exist, then return the index of where the file is, if not then return -1
	operation containsFile[filename:String] -> [index:Integer]
		index <- -1
		for i:Integer <- 0 while i <= listOfFiles.upperbound by i <- i + 1
			if filename = listOfFiles[i].getFileName then
				index <- i
			end if
		end for
	end containsFile
	
	%Add a file to the server
    export operation addFile[filename:String, hash:Integer, aPeer:Any]
			check <- self.containsFile[filename]
	       
			%The file already exist, just add the peer in the list to those who has the file
			if check >= 0 then
			        (locate self)$stdout.putstring["File exists, "]
			        if listOfFiles[check].addPeer[aPeer] then
			                (locate self)$stdout.putstring["Add a peer to the list\n"]
			        else
				        (locate self)$stdout.putstring["Peer is already in the list\n"]
			        end if
			else
				%If it is a new file, we will come here
			        (locate self)$stdout.putstring["New file added to table\n"]
			        listOfFiles.addUpper[Data.create[filename, hash]]
					
					%find the location of the file
					check <- self.containsFile[filename]
					
					%add the peer to the list
			        if !listOfFiles[check].addPeer[aPeer] then
				        (locate self)$stdout.putstring["This should never happen!!!\n"]
			        end if
			        (locate self)$stdout.putstring["peer added to the new file\n"]
			end if
	end addFile
	
	%Send a datapack with an array and the hash value for the file ?
	export operation getListOfPeers[filename:String] -> [msg:Message]
		check <- self.containsFile[filename]
	    var aList: Array.of[Any]
		var hash:Integer

       	(locate self)$stdout.putstring[filename || " was requested\n"]
		%of check is 0 or higher, it means we got the file indexed
		if check >= 0 then
			(locate self)$stdout.putstring["Server has the list of peers who has file\n"]
			aList <- listOfFiles[check].getList
			
			if aList == nil then
				(locate self)$stdout.putstring["There is no one who share this particular file\n"]
			        hash <- nil
			else
				(locate self)$stdout.putstring["Found list of peers who has the file, sending it back to the one requested it\n"]
				hash <- listOfFiles[check].getHash
			end if
		else
			%There is no file with the filename
			aList <- nil
			hash <- nil
			(locate self)$stdout.putstring["There is no file with name: '" || filename || "' in our database\n" ]
		end if
		
		%Message is used for only respond with more values, a list of peers and the hash value
		msg <- Message.create[self, aList, hash]
	end getListOfPeers
	
	%When no peer has a file, then the list of peers will be removed
	export operation fileNoLongerExist[filename:String]
		check <- self.containsFile[filename]
		if check >= 0 then
			listOfFiles[check].reset
			(locate self)$stdout.putstring["Removed the list of peers who had this file " || filename || "\n"]
		else
		  (locate self)$stdout.putstring["The file '" || filename || "' didn't exist in the first place !!\n"]
		end if
	end fileNoLongerExist
	
	%Write out the contents in the Server
	export operation printTable
		(locate self)$stdout.putstring["The server has indexed these files:\n"]
		for i:Integer <- 0 while i <= listOfFiles.upperbound by i <- i + 1
			(locate self)$stdout.putstring[i.asString || ". " || listOfFiles[i].getFileName || " with Secure Digest " || listOfFiles[i].getHash.asString || "\n"]
		end for
	end printTable
end Server
