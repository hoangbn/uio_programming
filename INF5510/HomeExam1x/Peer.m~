export Peer

const Peer <- class Peer[central:Server]
	const home <- locate self
	var list: Array.of[Peer]
	var storage: Array.of[File] <- Array.of[File].empty
	var tmp: File
	var names:String <- "Peer"
	var HashFunc:BernsteinHash
	var Rand:Random
	
	%Check if the Peer has the file
	operation containsFile[f:File] -> [bool:Boolean]
		bool <- false
		if storage.upperbound = 0 then
			bool <- false
		else
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				if storage[i] == f then
					bool <- true
					exit
				end if
			end for
		end if
	end containsFile
	
	%Add a file to the peer
	export operation addFile[f:File] -> [bool:Boolean]
		if self.containsFile[f] then
			(locate self)$stdout.putstring["The file already exist in our storage device\n"]
			bool <- false
		else 
			storage.addUpper[f]
			(locate self)$stdout.putstring["File added to peer\n"]
			bool <- true
		end if
	end addFile
	
	%debugging purpose
	export operation checkStorage
		(locate self)$stdout.putstring["The number of files this peer has is " || (storage.upperbound + 1).asString || "\n"]
		for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
			(locate self)$stdout.putstring[(i+1).asString || " Filename: " || storage[i].getName || "\nContent: " || storage[i].getContent || "\n"]
		end for
	end checkStorage
	
	operation getFile[filename:String] -> [f:File]
		if storage.upperbound = 0 then
			(locate self)$stdout.putstring["The storage is empty\n"]
			f <- nil
		else
			var count:Integer <- 0
			loop
				%Test if it is the right file, return it if true
				if storage[count].getName = filename then
					f <- storage[count]
					exit
				end if
				count <- count + 1
				
				%We've tried to get the file, but it seems the file is unavailable/doesn't exist
				if count > storage.upperbound then
					(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
					f <- nil
				end if
			end loop
		end if
	end getFile
	
	export operation completeRequest[s:String] -> [f:File]
		(locate self)$stdout.putstring["File '" || s || "' was requested\n"]
		f <- self.getFile[s]
		if f == nil then
			(locate self)$stdout.putstring["File is unavailable/doesn't exist\n"]
			f <- nil
		else 
			(locate self)$stdout.putstring["Sending the requested file back\n"]
		end if
		unavailable
			f <- nil
		end unavailable
	end completeRequest
	
	export operation requestFileFromPeer[p:Peer, fname:String] -> [bool:Boolean]
		bool <- false
		
		tmp <- p.completeRequest[fname]
		if tmp == nil then
			%Maybe check if the peer is unavailable?
			(locate self)$stdout.putstring["File is unavailable or removed from sharing\n"]
		else
			bool <- true
			%Run the string through hash function and compare it to servers hash value?
			if self.addFile[tmp] then
				(locate self)$stdout.putstring["Got File\n"]
			else
				(locate self)$stdout.putstring["Something went wrong, couldn't save the file\n"]
				bool <- false
			end if
		end if

		unavailable
			bool <- false
		end unavailable
	end requestFileFromPeer
	
	%only request list or download too, make a local Array list in this operation?
	export operation requestListOfPeers[filename:String] -> [pList:Array.of[Any]]
		%make sure the list is empty, necessary?
		pList <- central.getListOfPeers[filename]
		if pList == nil then
			(locate self)$stdout.putstring["There is no one sharing that file\n"]
		else
		     (locate self)$stdout.putstring["Got list of peers for that file\n"]	  
		end if
	end requestListOfPeers
	
	%Maybe check if our local storage has a file with the specified name and just end it there if we have it
	export operation downloadFile[filename:String]
		var pList: Array.of[Any] <- self.requestListOfPeers[filename]
		var bool:Boolean <- false
		%list <- (view pList as Array.of[Peer])
	      
		if pList == nil then
			%(locate self)$stdout.putstring["There is no one sharing that file\n"]
		else
			
		       	for i:Integer <- 0 while i <= pList.upperbound by i <- i + 1
				if self.requestFileFromPeer[(view pList[i] as Peer), filename] then
					bool <- true
					exit
				end if
			end for
		end if

		if bool then
			(locate self)$stdout.putstring["File downloaded successfully\n"]
		else
			(locate self)$stdout.putstring["Downloading failed, tell server\n"]
			central.fileNoLongerExist[filename]
		end if
				%Shouldnt need a unavailable, since this operation simulates a person currently using Nopester		
	end downloadFile
				


	%Operations that help us to set the status of the files, necessary?
        export operation unshareAFile[filename:String]
	
	end unshareAFile
	
	export operation unshareAllFiles[filename:String]
	
	end unshareAllFiles
	
	export operation shareAFile
	
	end shareAFile
	
	export operation shareAllFiles
		if storage.upperbound = -1 then
			(locate self)$stdout.putstring["No file on this peer\n"]
		else
			(locate self)$stdout.putstring["Make every file in the storage sharable\n"]
			for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
				storage[i].changeSharing[true]
			end for
		end if
	end shareAllFiles
	

	%Make a list of files to share? msg class? use the sharing status, or just send an array and the server can add each of them, but we'll have to make a hash array too.
	export operation updateSharing
		(locate self)$stdout.putstring["send info of files to Server\n"]
		for i:Integer <- 0 while i <= storage.upperbound by i <- i + 1
		        (locate self)$stdout.putstring[storage[i].getName || " \n"]
			%add a filename and its hash to the server
			%modify the operations so we can send an array of filenames and an array of hashes corresponding to the content of the files
			central.addFile[storage[i].getName, HashFunc.hash[storage[i].getContent], self]
		end for
	end updateSharing
	
 

	export operation hello
	    (locate self)$stdout.putstring["Called by server\n"]
	end hello
    
	initially
		HashFunc <- BernsteinHash.create
		Rand <- Random.create
	end initially
end Peer
