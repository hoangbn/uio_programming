%Handler, tells us when a node is up or down, preferable down since we dont do up
const Handler <- object Handler
	export operation nodeUp[n:Node, t:Time]
		(locate self)$stdout.putString["Node went up, which shouldnt happen since we don't do that \n"]
	end nodeUp

	export operation nodeDown[n:Node, t:Time]
		(locate self)$stdout.putString["Node went down, peers assigned to that Node is probably gone too \n"]
	end nodeDown
end Handler

const Info <- class Info[name:String, host:String]
	export operation getName -> [n:String]
		n <- name
	end getName
	
	export operation getHost -> [h:String]
		h <- host
	end getHost
end Info

%Outstream? log activities in a file?
const Main <- object Main
	%Variables
	var aServer: Server <- Server.create	
	var aPeer: Peer
	
	const home <- locate self
	
	var list: Array.of[Peer] <- Array.of[Peer].empty
	var files: Array.of[File] <- Array.of[File].empty
	
	%Store information about peers and nodes so we can write it out
	var listNames: Array.of[String] <- Array.of[String].empty
	
	%Used to print out information easier
	var listInfo: Array.of[Info] <- Array.of[Info].empty
	
	%References of Nodes
	var all: NodeList
	var element: NodeListElement
	var away:Node
	
	%Rand, used to make random numbers
	var Rand:Random <- Random.create 
	
	%Include delays so the user can decide whether to kill a node or let it live for another day... or seconds
	var t:Time <- Time.create[0,5000]
	%Increase this to see better what is going on between the peers
	var tr:Time <- Time.create[0,5000]
	
	
	%Increase or decrease value to decide how many random operation our system should execute
	var actions:Integer <- 1000
	
	%Update the list of peers by ignoring the peer that was unavailable
	export operation renewList[arr:Array.of[Peer], index:Integer] -> [res:Array.of[Peer]]
		var newArray: Array.of[Peer] <- Array.of[Peer].empty
		if arr == nil then
			(locate self)$stdout.putstring["Seems like every peer in the system is dead\n"]
			res <- nil
		else
			for i:Integer <- 0 while i <= arr.upperbound by i <- i + 1
				if i != index then
					newArray.addUpper[arr[i]]
				end if
			end for
			
			%NewArray didnt add anything
			if newArray.upperbound = -1 then
				res <- nil
			else
				res <- newArray
			end if
		end if
	end renewList
	
	%An Get operation to use another operation in another object, if the operation we called is unavailable, then our unavailable will execute
	operation getFile -> [bool:Boolean]
		bool <- list[Rand.getRandom[list.upperbound]].downloadFile[files[Rand.getRandom[files.upperbound]].getName] 
		
		unavailable
			(locate self)$stdout.putString["a Peer went down\n"]
			bool <- false
		end unavailable
	end getFile
	
	%An Add operation to use another operation in another object, if the operation we called is unavailable, then our unavailable will execute
	operation addFile -> [bool:Boolean]
		bool <- list[Rand.getRandom[list.upperbound]].addFile[files[Rand.getRandom[files.upperbound]]] 	
		
		unavailable
			(locate self)$stdout.putString["a Peer went down\n"]
			bool <- false
		end unavailable
	end addFile
	
	operation removeFile -> [bool:Boolean]
		bool <- list[Rand.getRandom[list.upperbound]].removeRandomFile
		
		unavailable
			(locate self)$stdout.putString["a Peer went down\n"]
			bool <- false
		end unavailable
	end removeFile
	
	%Writes out to screen the status of nodes and peers and their contents
	operation dumpSystem
		(locate self)$stdout.putString["Status of peers, their content will be displayed in their respective nodes\n"]
		var bool:Boolean <- false
		if list == nil then
			(locate self)$stdout.putString["Every node is dead\n"]
		else
			for i:Integer <- 0 while i <= listInfo.upperbound by i <- i + 1	
				(locate self)$stdout.putString["Peer " || listInfo[i].getName || " in Node " || listInfo[i].getHost || " is "]
				for j:Integer <- 0 while j <= list.upperbound by j <- j + 1 	
					if listInfo[i].getName = list[j].getName then
						bool <- true
						list[j].checkStorage[false]
					end if
				end for
				if bool then
					(locate self)$stdout.putString["alive \n"]
				else
					(locate self)$stdout.putString["dead\n"]
				end if
				bool <- false
			end for
			aServer.printTable
		end if	
		
		unavailable
			(locate self)$stdout.putString["We shouldn't be able to come here, if it does then it means we've tried to access operation of peers that was unavailable\n"]
		end unavailable
	end dumpSystem
	
	%An operation to check whether a peer is unavailable
	operation checkUnavailable[p:Peer] -> [bool:Boolean]
		bool <- p.checkAvailable
		
		unavailable
			bool <- false
		end unavailable
	end checkUnavailable
		
	process
		all <- home.getActiveNodes
		home$stdout.putString[(all.upperbound + 1).asstring || " Nodes active\n"]
		home.setNodeEventHandler[Handler]
		
		%I've made it so the program fullfills the requirement of 10 files and 5 peers, so t is a requirement that we have to make 6 nodes, preferable on different machines
		if all.upperbound = 5 then
			%creates peers and add information about the peer and its files
			%if there are 6 nodes available, then 1 node for server and a peer on each node
			%Move the server to a node, should be a dedicated machine
			
			%This is actually not necessary since every object created will reside in all[0]$theNode
			move home to all[0]$theNode
			move aServer to all[0]$theNode
			
			%create peers and move them to each node, which is 5 peers in 5 different nodes
			for i:Integer <- 1 while i <= all.upperbound by i <- i + 1	
				away <- all[i]$theNode
				aPeer <- Peer.create[aServer, i.asString]
				list.addUpper[aPeer]					
				move aPeer to away
				listInfo.addUpper[Info.create[aPeer.getName, away$name]]
				listNames.addUpper[aPeer.getName]
			end for
			
			%Make files
			files.addUpper[File.create["Hello", "Hello World"]]
			files.addUpper[File.create["Lord of the Rings", "One Ring To Rule Them All"]]
			files.addUpper[File.create["The Hobbit - Desolation of Smaug", "I am Fire, I am Death"]]
			files.addUpper[File.create["Confucius says: Volume 1", "Give a man some fire, and he'll be warm for a day.\nSet a man on fire, and he'll be warm for the rest of his life"]]
			files.addUpper[File.create["Confucius says: Volume 2", "It is only when a mosquito lands on your testicles that you realize\nThere is always a way to solve problems without using violence"]]
			files.addUpper[File.create["INF5510 - Distributed objects", "Presentation of fundamental concepts of distributed objects including local and distributed objects, remote object invocation, communication, parameter passing principles, garbage collection, performance issues and typical applications. We also cover advanced type systems including concepts such as immutability and its application to distributed programming and access control. We also discuss the design of language constructs for parallelism. Throughout we use the programming language Emerald as a base. We dig into the implementation of distributed objects including virtual machines, compilation techniques, run-time typing, dynamic program loading, and how to do distributed garbage collection."]]
			files.addUpper[File.create["Peer One", "Each peer should be able to insert new ﬁles into the system. It shall do so by registering that it has a given ﬁle by sending name and the secure digest to the central server which then makes sure to insert data into a table (unless the ﬁle already exists) Moreover, the peer is inserted in the list of peers that has the speciﬁc ﬁle."]]
			files.addUpper[File.create["Peer Two", "A peer should be able to ask to be given the list of the peers that has the ﬁle with a given name. Moreover, peers may deliver the ﬁle on request."]]
			files.addUpper[File.create["Peer Three", "A peer will also be able to tell the central server that a given ﬁle no longer exists among peers."]]
			files.addUpper[File.create["Peer Four", " It is not necessary to store the ﬁles on disk, it is sufﬁcient that the ﬁles are objects that lies in the memory"]]
			
			%Randomly adding the files we have made to different peers
			%Add files to each peer
			for i:Integer <- 0 while i <= list.upperbound by i <- i + 1
				%Random number of files to be added to a peer, with <=, each peer will have atleast 1 file
				for j:Integer <- 0 while j <= Rand.getRandom[files.upperbound] by j <- j + 1
					%Add a random file picked from the list of files we have made
					var bool:Boolean <- false 
					loop
						exit when bool = true
						bool <- list[i].addFile[files[Rand.getRandom[files.upperbound]]]
					end loop
				end for
			end for
			
			%Variables used in this for loop
			var check:Boolean <- false
			var number:Integer
			
			%How many times to execute different operations
			for i:Integer <- 0 while i < actions by i <- i + 1
				if list == nil | list.upperbound = 0 then
					(locate self)$stdout.putString["Since every node is dead or there is only one peer, its time to exit the program\n"]
					exit
				end if
				number <- Rand.getRandom[16]
				%A random Peer tries to get or download a file
				if number <= 7 then
					check <- self.getFile
				%A random peer tries to add a new file from our repository of files to its storage and share it
				elseif number <= 14  then
				    check <- self.addFile	
				%
				elseif number = 15 then
					check <- self.removeFile
				else
					%We set it to false so we can check whether the user killed any node
					check <- false
					(locate self)$stdout.putString["Now is the time to kill a node if you want\n"]
					home.delay[t]
				end if
				if !check then 
					for j:Integer <- 0 while j <= list.upperbound by j <- j + 1
						if !self.checkUnavailable[list[j]] then
							list <- self.renewList[list, j]
							%We have to reset it to 0 since we have changed the list so the upperbound has decreased
							i <- 0
						end if
						
						%Exit if the list is nil, empty
						if list == nil then
							exit
						end if
					end for
				end if
				%Had a delay here so if the user wants to see what is going on, the person can increase the delay
				home.delay[tr]
			end for
			
			self.dumpSystem
		else
			stdout.putString["Make 6 nodes, 1 for server and 5 others for peers, make sure the server is a dedicated machine, and it shouldn't hurt to do the same for peers also\n"]
		end if
	end process
end Main
