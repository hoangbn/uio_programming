%The Primary Copy Replication should send updates to all replicas
%Find out how to have one-to-many references, an array?

export PrimaryCopyReplication
const PrimaryCopyReplication <- class PrimaryCopyReplication[ID:Integer, data:Array.of[PersonInfo]]
	var objectID: Integer
	var cond: Condition
	var localData: Array.of[PersonInfo]
	%var data: Array.of[PersonInfo]
	%var tServer: TimeServer
	
	%Need reference to framework to ask about the Primary
	%var refFrame:Framework
	var aReplica: PrimaryCopyReplication
	
	
		
	export operation updateData[input:Any] -> [res:Integer]
		res <- 1
		
		failure
			(locate self)$stdout.putstring["updateData"]
		end failure
	end updateData
	
	export operation getUpdate -> [res:Integer]
		res <- 0
	end getUpdate
	
	%create a new object and copy everything from the primary to the new object
	%No need to inherit
	export operation cloneMe -> [res: PrimaryCopyReplication]
		res <- PrimaryCopyReplication.create[ID, localData]
		
		
		%If the Node is unavaiable or the object is dead
		unavailable
			res <- nil
		end unavailable
		
		%failure has to be last and not before unavaiable
		failure
			(locate self)$stdout.putstring["cloneMe"]
		end failure
	end cloneMe
	
	initially
		cond <- Condition.create
		data <- Array.of[Any].empty
	end initially
end PrimaryCopyReplication
--------------------------------------------------------
%The Primary Copy Replication should send updates to all replicas
%Find out how to have one-to-many references, an array?

export PrimaryCopyReplication
const PrimaryCopyReplication <- monitor class PrimaryCopyReplication[ID:Integer, data:Array.of[PersonInfo], ref:Framework]
	var objectID: Integer
	var cond: Condition <- Condition.create
	var localData: Array.of[PersonInfo]
	
	var lastUpdate:Time
	%var data: Array.of[PersonInfo]
	%var tServer: TimeServer
	
	%Need reference to framework to ask about the Primary
	var refFrame:Framework
	

	
	
	export operation updateData[input:Any] -> [res:Integer]
		res <- 1
		
		failure
			(locate self)$stdout.putstring["updateData\n"]
		end failure
	end updateData
	
	export operation getUpdate -> [res:Integer]
		res <- 0
		
	end getUpdate
	
	%create a new object and copy everything from the primary to the new object
	%No need to inherit
	export operation cloneMe -> [res: PrimaryCopyReplication]
		res <- PrimaryCopyReplication.create[ID, data, ref]
		
		
		%If the Node is unavaiable or the object is dead
		unavailable
			res <- nil
		end unavailable
		
		%failure has to be last and not before unavaiable
		failure
			(locate self)$stdout.putstring["cloneMe\n"]
		end failure
	end cloneMe
	
	initially
		cond <- Condition.create
		localData <- data
		refFrame <- ref
	end initially
end PrimaryCopyReplication
