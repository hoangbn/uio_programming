export Client

const Client <- class Client[name:String, ID:Integer, ref:Framework]
	var read: String
	var tmpPerson: PersonInfo
	var name: String
	var age: String
	var job: String
	
	var ns:NameServer
	var ts:TimeServer
	const home <- locate self
	var all: NodeList
	%var frame: Framework <- Framework.create
	
	export operation updateName
		(locate self)$stdout.putString["Name: \n"]
		name <- (locate self)$stdin.getString
	
		(locate self)$stdout.putString["age: \n"]
		age <- (locate self)$stdin.getString
	
		(locate self)$stdout.putString["Job: \n"]
		job <- (locate self)$stdin.getString
		tmpPerson <- PersonInfo.create[name, age, job]
		
		ref.update[tmpPerson, 0]
	end updateName
	
	export operation getName 
		(locate self)$stdout.putString["Look up Name: \n"]
		name <- (locate self)$stdin.getString
		tmpPerson <- view ref.getData[name, 0] as PersonInfo
		
		if tmpPerson !== nil then
			(locate self)$stdout.putString["Result: \n"]
			(locate self)$stdout.putString["Name: " || tmpPerson.getName]
			(locate self)$stdout.putString["Age: " || tmpPerson.getAge]
			(locate self)$stdout.putString["Job: " || tmpPerson.getJob]
		else
			(locate self)$stdout.putString[name || " didn't exist \n"]
		end if
		%ref.hello
	end getName
	
	export operation updateTime
		(locate self)$stdout.putString["Sending Request to update time Synchroization\n"]
		ref.update[nil, 1]
	end updateTime
	
	export operation getTime
		(locate self)$stdout.putString["The time Synchroization is " || (view ref.getData[nil, 1] as Real).asString ||"\n"]
	end getTime
	
	operation replicateName
		(locate self)$stdout.putString["How many replicates? \n"]
		read <- (locate self)$stdin.getString
		var num: Integer <- read.getElement[0].ord
		
		ns <- view ref.getPrimary[0] as NameServer
		ref.replicateMe[ns, num, 0]
		ref.distributeObjects[0]
		(locate self)$stdout.putString["Made " || num.asString || " replicas of Name Server\n"]
	end replicateName
	
	operation replicateTime
		%(locate self)$stdout.putString["Replicating the Primary Copy of Time Server\n"]
		
		(locate self)$stdout.putString["How many replicates? \n"]
		read <- (locate self)$stdin.getString
		var num: Integer <- read.getElement[0].ord
		ts <- view ref.getPrimary[1] as TimeServer
		
		ref.replicateMe[ts, num, 1]
		ref.distributeObjects[0]
		(locate self)$stdout.putString["Made " || num.asString || " replicas of Time Server\n"]
	end replicateTime
	
	operation printInfo
		ref.printInfo
	end printInfo	
		
	process
		%file <- OutStream.create[0]
		loop
			(locate self)$stdout.putString["***************Client menu*************** \n"]
			(locate self)$stdout.putString["1. Update Name\n"]
			(locate self)$stdout.putString["2. Get Name\n"]
			(locate self)$stdout.putString["3. Update Time\n"]
			(locate self)$stdout.putString["4. Get Time\n"]
			(locate self)$stdout.putString["5. Replicate Name Servers\n"]
			(locate self)$stdout.putString["6. Replicate Time Servers\n"]
			(locate self)$stdout.putString["7. Status of the System\n"]
			(locate self)$stdout.putString["8. Add/remove delay, testing purposes\n"]
			%(locate self)$stdout.putString["?. Identify server type and whether it is a replica or primary\n"]
			read <- (locate self)$stdin.getString
			%ref.hello
			if read.getElement[0] = '1' then
				self.updateName	
			elseif read.getElement[0] = '2' then
				self.getName
			elseif read.getElement[0] = '3' then
				self.updateTime
			elseif read.getElement[0] = '4' then
				self.getTime
			elseif read.getElement[0] = '5' then
				self.replicateName
			elseif read.getElement[0] = '6' then
				self.replicateTime
			elseif read.getElement[0] = '7' then
				self.printInfo
			elseif read.getElement[0] = '8' then
				ref.modifyTest
			else
				ref.hello
			end if
			
			%ref.hello
			%read <- str.getString
			%(locate self)$stdout.putString[read || " \n"]
		end loop
	end process

end Client