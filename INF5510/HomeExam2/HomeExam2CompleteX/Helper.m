export Random
export updatePack
export PersonInfo
%Random generator based on Linear congruential generator
const Random <- class Random
	export operation getRandom[modulo:Integer] -> [res:Integer]
		var seed:Integer <- (locate self).getTimeOfDay.getMicroSeconds # 2147483647
		
		seed <- (seed * 1103515245 + 12345) & 2147483647
		res <- seed # (modulo + 1)
	end getRandom
end Random

const updatePack <- class UpdatePack[t:Time, data:Array.of[PersonInfo]]
	export operation getTimeStamp -> [res:Time]
		res <- t
	end getTimeStamp

	export operation getData -> [res:Array.of[PersonInfo]]
		res <- data
	end getData
end updatePack

export PersonInfo
const PersonInfo <- class PersonInfo[name:String, age:String, job:String]
	export operation getName -> [n:String]
		n <- name
	end getName
	
	export operation getAge -> [a:String]
		a <- age
	end getAge
	
	export operation getJob -> [j:String]
		j <- job
	end getJob
end PersonInfo