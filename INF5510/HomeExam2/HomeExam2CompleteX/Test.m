const test <- object test
	%References of Nodes
	const home <- locate self
	var all: NodeList <- home.getActiveNodes
	var element: NodeListElement
	var away:Node
	%var arr:Array.of[TimeServer] <- Array.of[TimeServer].empty
	var frame:Framework <- Framework.create
	%var tem:TimeServer
	var file: OutStream
	var str: InStream
	var read: String
	
	var cli: Array.of[Client] <- Array.of[Client].empty
	
	var timeKey: Boolean <- false
	var Key:Boolean <- false
	
	const syncObject <- monitor class innerObject
		const n: Condition <- Condition.create
		const t: Condition <- Condition.create
		
		export operation nameOperations[nm:Any, type:Integer, which:Integer] -> [res:Any]
			loop
				if Key then
					wait n
				else
					Key <- true
				end if
			end loop
			
			res <- nil
			%Name Server Operations
			if type = 0 then
				%Update
				if which = 0 then
					frame.update[view nm as PersonInfo, type]
				else
					%Get
					res <- frame.getData[view nm as String, type]
				end if
			else
				%Time Server Operations
				if which = 0 then
					%Update
					frame.update[nm, type]
				else
					%Get
					res <- frame.getData[nil, type]
				end if
			end if
		end nameOperations
		
		export operation signalN
			signal n
		end signalN
		
		export operation signalT
			signal t
		end signalT
		
		export operation timeOperations[type:Integer] -> [res:Any]
			res <- nil
		
		end timeOperations
	
	end innerObject	
	
	operation testConcurrency
		var limit:Integer <- 100
		const obj1 <- object obj1
			
			var count: Integer <- 0
			process
				loop
					exit when count = limit
					count <- count + 1
				end loop
			end process
		end obj1
		
		const obj2 <- object obj2
			var count: Integer <- 0
			process
				loop
					exit when count = limit
					count <- count + 1
				end loop
			end process
		end obj2
	
	end testConcurrency
	
	
	process
			move self to all[1]$theNode
			cli.addUpper[Client.create["Bao", 1, frame]]
			frame.addNode[all[1]$theNode]
			%move self to all[2]$theNode
			%cli.addUpper[Client.create["Dao", 2, frame]]
			%frame.addNode[all[2]$theNode]
			
			move frame to all[0]$theNode
			move self to all[0]$theNode
			frame.addNode[all[0]$theNode]
			
			var n: NameServer <- NameServer.create["H", Time.create[0,0], Array.of[PersonInfo].empty, frame, true]
			move n to all[3]$theNode
			frame.addNode[all[3]$theNode]
			frame.addObject[n, 0]
			frame.replicateMe[n, 1, 0]
			frame.addNode[all[4]$theNode]
			frame.distributeObjects[0]
			
			var t: TimeServer <- TimeServer.create["H", Time.create[0,0], 0.0, frame, true]
			move t to all[4]$theNode
			
			frame.addObject[t, 1]
            frame.replicateMe[t, 1, 1]
			frame.distributeObjects[0]
			%(locate self)$stdout.putString["Client menu \n"]
			%read <- (locate self)$stdin.getString
			%if read.getElement[0] == 'c' then
			%	self.close
			%end if
			%(locate self)$stdout.putString[read || " \n"]
			%cli[0].start
			%cli[1].start
	end process
	
	%process
	
	%	all <- home.getActiveNodes
	%	home$stdout.putString[(all.upperbound + 1).asstring || " Nodes active\n"]
		
		%for i:Integer <- 1 while i <= all.upperbound by i <- i + 1
		%	away <- all[i]$theNode
		%	tem <- TimeServer.create
		%	arr.addUpper[tem]
		%	move tem to away
		%end for
		%s
		%var c:Integer <- 0
		%loop
		%	if c = 0 then
		%		arr[c].getT
		%		c <- c + 1
		%	else
		%		arr[c].getT
		%		c <- c - 1
		%	end if
			
			
		%end loop
	%end process
end test
