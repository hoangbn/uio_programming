%The Framework should offer functionalities to replicate a given object to a given number of machines.
export Framework
export NameServer
export TimeServer



const Framework <- class Framework
	%Array storing the references to each object
	var nameArray: Array.of[NameServer] <- Array.of[NameServer].empty
	var timeArray: Array.of[TimeServer] <- Array.of[TimeServer].empty
	
	%Array storing the references to each object which has been assigned to a node/machine
	var nameArrayDist: Array.of[NameServer] <- Array.of[NameServer].empty
	var timeArrayDist: Array.of[TimeServer] <- Array.of[TimeServer].empty
	
	%This is used to unite the NameServer and TimeServer to make it easier to distribute
	var distArray: Array.of[Any] <- Array.of[Any].empty
	
	%Adds delay on various places for testing
	var testUnvailable: Boolean <- false
	
	%Used to check whether a Node has an object or not
	var NodeInfo: Array.of[Integer] <- Array.of[Integer].empty
	
	%Variables
	var nameObject: NameServer
	var timeObject: TimeServer
	
	%References to the Primary objects
	var namePrimary: NameServer
	var timePrimary: TimeServer
	
	%The Index in their respective array where we can find the Primary
	var nameIndex: Integer
	var timeIndex: Integer
	
	var temp: Integer
	var freeNodes: Integer <- 0
	
	%Random number
	var rand: Random <- Random.create
	
	%References to Nodes
	var all: NodeList
	var element: NodeListElement
	var away: Node
	
	%Check Node available

	%When a client is calling for get or update, pick a random object and check if it will delegate to the primary
	%Make nodes x
	%Move objects to nodes x
	%Failure Model
	%Primary died
	%backup died
	
	%Added so the user can decide whether to add delays to test the program by killing off nodes during delays
	export operation modifyTest
		if testUnvailable then
			testUnvailable <- false
		else
			testUnvailable <- true
		end if
	end modifyTest
	
	export operation testing[n:Any, opr:Integer]
		if testUnvailable then
			(locate n)$stdout.putString["Kill this node now, unless this is the FrameWork then DONT!!\n"]
			if opr = 0 then
				(locate self)$stdout.putstring["FrameWork: Testing Update\n"]
			elseif opr = 1 then
				(locate self)$stdout.putstring["FrameWork: Testing Get\n"]
			elseif opr = 2 then
				(locate self)$stdout.putstring["FrameWork: Testing notifyUpdate, kill one Random Node\n"]		
			else
			
			end if
			(locate self).delay[Time.create[5 , 0]]
		end if
	end testing
	
	%Check if the Node has an object from before
	operation NodeContainsObject[n:Node] -> [res:Boolean]
		res <- false
		temp <- n.getLNN
		%(locate self)$stdout.putstring[temp.asString || "\n"]
		for i:Integer <- 0 while i <= NodeInfo.upperbound by i <- i + 1
			
			if temp = NodeInfo[i] then
				%(locate self)$stdout.putstring[NodeInfo[i].asString || "\n"]
				res <- true
			end if
		end for
		
		failure
			(locate self)$stdout.putstring["NodeContainsObject\n"]
		end failure
	end NodeContainsObject
	
	%export operation checkAvailable[]
	
	%if an object died,remove the reference to the object from the array 
	export operation renewParameters[type:Integer] %-> [res:Integer]
		%all <- (locate self).getActiveNodes
		if type = 0 then
			var tmpArray: Array.of[NameServer] <- Array.of[NameServer].empty
			for i:Integer <- 0 while i <= nameArrayDist.upperbound by i <- i + 1
				if nameArrayDist[i] !== nil then
					tmpArray.addUpper[nameArrayDist[i]]
				end if
			end for
			nameArrayDist <- tmpArray
		else
			var tmpArray: Array.of[TimeServer] <- Array.of[TimeServer].empty
			for i:Integer <- 0 while i <= timeArrayDist.upperbound by i <- i + 1
				if timeArrayDist[i] !== nil then
					tmpArray.addUpper[timeArrayDist[i]]
				end if
			end for
			timeArrayDist <- tmpArray
		end if
	end renewParameters
	
	
	
	
	%Add a Node which has been assigned with an object
	export operation addNode[n:Node]
		NodeInfo.addUpper[n.getLNN]
	end addNode
	
	%This operation is called by the Test object, which is used only when adding the primary object
	export operation addObject[obj:Any, type:Integer]
		if type = 0 then
			nameObject <- (view obj as NameServer)
			nameArrayDist.addUpper[nameObject]
			nameIndex <- nameArrayDist.upperbound
			namePrimary <- nameObject
		else
			timeObject <- (view obj as TimeServer)
			timeArrayDist.addUpper[timeObject]
			timeIndex <- timeArrayDist.upperbound
			timePrimary <- timeObject
		end if
	end addObject
	
	%Union of two arrays
	export operation mash
			for i:Integer <- 0 while i <= nameArray.upperbound by i <- i + 1
				distArray.addUpper[nameArray[i]]
			end for

			for i:Integer <- 0 while i <= timeArray.upperbound by i <- i + 1
				distArray.addUpper[timeArray[i]]
			end for
	end mash
		
	%Distribute replicas to various nodes and machines
	export operation distributeObjects[type:Integer]% -> [res:Integer]
		%Refresh the NodeList, if the user has connected new nodes to the network
		all <- (locate self).getActiveNodes
		
		%Union between two arrays
		self.mash
			(locate self)$stdout.putstring["Distributing\n"]
			%if NodeInfo.upperbound = -1 then
			if type = 0 then
				var counter: Integer <- 0
				%There is no need to check whether we've enough nodes, since we will try to distribute replicas to Nodes that haven't been assigned with an object
				%Even if we have 9 object and only 8 nodes, We will only maintain 
				for i:Integer <- 0 while i <= all.upperbound by i <- i + 1
					away <- all[i]$theNode
					exit when counter > distArray.upperbound
					%Find out which object is already distributed
					if !self.NodeContainsObject[away] then	
						%If there are fewer objects than Nodesm then 
						if counter <= distArray.upperbound then   %nameArray.upperbound then
							move distArray[counter] to away
							%move nameArray[counter] to away
							%nameArrayDist.addUpper[nameArray[counter]]
							self.addNode[away]
							if counter <= nameArray.upperbound then
								nameArrayDist.addUpper[view distArray[counter] as NameServer]
							else 
								timeArrayDist.addUpper[view distArray[counter] as TimeServer]
							end if
							%nameArray[count1].hello
							counter <- counter + 1
							
						end if
					end if
				end for
			end if	
		
			if type = 0 then
				nameArray <- Array.of[NameServer].empty
				timeArray <- Array.of[TimeServer].empty
				distArray <- Array.of[Any].empty
			end if
		%(locate self)$stdout.putstring[nameArrayDist.upperbound.asstring || "\n"]
		
		unavailable
			(locate self)$stdout.putstring["distribute!!!!!\n"]
		end unavailable
		failure 
			
		end failure
	end distributeObjects
	
	export operation replicateMe[pcr:Any, number:Integer, type:Integer]% -> [res:Array.of[Any]]
		%Check number of machines/nodes, if it is lower than number then we will only make so many as we can
		
		%If number is higher than available machines, then we only create replicas to the available nodes
		(locate self)$stdout.putstring["Replicating "]
		
		if type = 0 then
			%A NameServer
			(locate self)$stdout.putstring["name servers\n"]
			nameObject <- (view pcr as NameServer)
			namePrimary <- nameObject
			for i:Integer <- 0 while i < number by i <- i + 1
				nameArray.addUpper[nameObject.cloneMe]
			end for
		else
			%A TimeServer
			(locate self)$stdout.putstring["time servers\n"]
			timeObject <- (view pcr as TimeServer)
			timePrimary <- timeObject
			for i:Integer <- 0 while i < number by i <- i + 1
				timeArray.addUpper[timeObject.cloneMe]
			end for
		end if
		
		unavailable
			(locate self)$stdout.putstring["replicate!!!!!\n"]
		end unavailable
		
		failure
			if pcr == nil then
				(locate self)$stdout.putstring["NIL!!!!!\n"]
			end if
			(locate self)$stdout.putstring[number.asString || "\n"]
		end failure
	end replicateMe
	
	%Return the Primary Copy to the Replica
	export operation getPrimary[type:Integer] -> [res:Any]
		if type = 0 then
			res <- namePrimary
		else
			res <- timePrimary
		end if
	end getPrimary
	
	%Only used by TimeServer to calculate the time between TimeServers
	export operation getReplicas -> [res:Array.of[TimeServer]]
		res <- timeArrayDist
		
		failure
			(locate self)$stdout.putstring["getReplicas\n"]
		end failure
	end getReplicas
	
	%Randomly pick a server to update to simulate different people accesing different servers
	export operation update[input:Any, type:Integer] %-> [res:Integer]
		(locate self)$stdout.putstring["FrameWork:update in Framwork\n"]
		var index: Integer
		if type = 0 then
			
			%Update NameServer
			%var index: Integer <- rand.getRandom[nameArrayDist]
			var tmp: PersonInfo <- view input as PersonInfo
			index <- rand.getRandom[nameArrayDist.upperbound]
			var tmpServer: NameServer <- nameArrayDist[index]
			self.testing[locate tmpServer, 0]
			%var tmpServer: NameServer <- view self.pickServer[type] as NameServer
			if tmpServer.updateData[tmp] = 1 then
				
				%res <- 1
			end if
		else
			(locate self)$stdout.putstring["update Time Server in Framwork\n"]
			index <- rand.getRandom[timeArrayDist.upperbound]
			%var tmp: Any <- 
			%var tmpServer: TimeServer <- view self.pickServer[type] as TimeServer
			var tmpServer: TimeServer <- timeArrayDist[index]
			self.testing[locate tmpServer, 0]
			tmpServer.updateTime
		end if
		
		unavailable
			(locate self)$stdout.putstring["FrameWork: Unavailable during update call\n"]
			%nameIndex and timeIndex is the index where the Primary reside in the Array
			if type = 0 then
				if nameIndex = index then
					self.fixNetwork[type, true, index, 1]
				else	
					self.fixNetwork[type, false, index, 1]
				end if
			else
				if timeIndex = index then
					self.fixNetwork[type, true, index, 1]
				else	
					self.fixNetwork[type, false, index, 1]
				end if
			end if
			self.update[input, type]
		end unavailable
			
		failure
			(locate self)$stdout.putstring[Index.asString || "\n"]
		end failure
	end update
	
	export operation getData[input:Any, type:Integer] -> [res:Any]
		(locate self)$stdout.putstring["Get data in Framwork\n"]
		var index: Integer
		if type = 0 then
			%Update NameServer
			%var index: Integer <- rand.getRandom[nameArrayDist]
			var tmp: String <- view input as String
			index <- rand.getRandom[nameArrayDist.upperbound]
			var tmpServer: NameServer <- nameArrayDist[index]
			self.testing[locate tmpServer, 1]
			%var tmpServer: NameServer <- view self.pickServer[type] as NameServer
			res <- tmpServer.getData[tmp]
		else
			%var tmp: Any <- 
			%var tmpServer: TimeServer <- view self.pickServer[type] as TimeServer
			index <- rand.getRandom[timeArrayDist.upperbound]
			%var tmp: Any <- 
			%var tmpServer: TimeServer <- view self.pickServer[type] as TimeServer
			var tmpServer: TimeServer <- timeArrayDist[index]
			self.testing[locate tmpServer, 1]
			res <- tmpServer.getTime
			(locate self)$stdout.putstring["update Time Server in Framwork\n"]
		end if
		
		unavailable
			(locate self)$stdout.putstring["FrameWork: Unavailable during get Request\n"]
			if type = 0 then
				if nameIndex = index then
					self.fixNetwork[type, true, index, 1]
				else	
					self.fixNetwork[type, false, index, 1]
				end if
			else
				if timeIndex = index then
					self.fixNetwork[type, true, index, 1]
				else	
					self.fixNetwork[type, false, index, 1]
				end if
			end if
			res <- self.getData[input, type]
		end unavailable
	end getData
	
	export operation votePrimary[type:Integer]
		var index: Integer
		var crash: Integer
		var timeTmp: Time
		var timeRes: Time <- Time.create[0, 0]
		
		if testUnvailable then
			(locate self)$stdout.putstring["Killing a Node when voting ain't funny\n"]
		end if
		
		if type = 0 then
			for i:Integer <- 0 while i <= nameArrayDist.upperbound by i <- i + 1
				crash <- i
				timeTmp <- nameArrayDist[i].getLastUpdate
				self.testing[locate nameArrayDist[i], 0]
				if timeRes <= timeTmp then
					timeRes <- timeTmp
					index <- i
					exit
				end if
			end for
			nameArrayDist[index].promotePrimary
			namePrimary <- nameArrayDist[index]
			nameIndex <- index
		else
			for i:Integer <- 0 while i <= timeArrayDist.upperbound by i <- i + 1
				crash <- i
				timeTmp <- timeArrayDist[i].getLastUpdate
				self.testing[locate timeArrayDist[i], 1]
					if timeRes <= timeTmp then
						timeRes <- timeTmp
						index <- i
						exit
					end if
			end for
			timeArrayDist[index].promotePrimary
			timePrimary <- timeArrayDist[index]
			timeIndex <- index
		end if
		
		unavailable
			(locate self)$stdout.putstring["Node died when Framwork trying to promote new Primary\n"]
			self.fixNetwork[type, true, crash, 0]
			
		end unavailable
	end votePrimary
	
	export operation fixNetwork[type:Integer, aPrimary: Boolean, index:Integer, error:Integer]
		
		if aPrimary then
			(locate self)$stdout.putstring["Primary went down\n"]
			if type = 0 then
				%NameServer Primary
				if !(nameIndex > nameArrayDist.upperbound) then
					nameArrayDist[nameIndex] <- nil
				end if
			else
				%TimeServer Primary
				if !(timeIndex > timeArrayDist.upperbound) then
					timeArrayDist[timeIndex] <- nil
				end if
			end if
			self.renewParameters[type]
			self.votePrimary[type]
		else
			(locate self)$stdout.putstring["Replicate went down\n"]
			if type = 0 then
				%set it to nil so we can remove the element from array
				nameArrayDist[index] <- nil
				self.replicateMe[namePrimary, 1, 0]
			else
				%set it to nil so we can remove the element from array
				timeArrayDist[index] <- nil
				self.replicateMe[timePrimary, 1, 1]
			end if
			self.renewParameters[type]
			self.distributeObjects[0]
			%Try and continue the update
			%self.notifyUpdate[type]
		end if
		
		failure
			(locate self)$stdout.putstring[type.asString||"\n"]
			(locate self)$stdout.putstring[aPrimary.asString||"\n"]
			(locate self)$stdout.putstring[index.asString||"\n"]
		end failure
	end fixNetwork
	
	%It identifies object and check if something went down during pause
	export operation hello
		var index: Integer <- 0
		var type:Integer <- 0
		for i:Integer <- 0 while i <= nameArrayDist.upperbound by i <- i + 1
			index <- i
			nameArrayDist[i].hello
		end for
		type <- 1
		for i:Integer <- 0 while i <= timeArrayDist.upperbound by i <- i + 1
			index <- i
			timeArrayDist[i].hello
		end for
		%(locate self)$stdout.putstring["Hello from Framework\n"]
		
		%unavailable
		%	if type = 0 then
		%		if nameIndex = index then
		%			self.fixNetwork[type, true, index, 1]
		%		else	
		%			self.fixNetwork[type, false, index, 1]
		%		end if
		%	else
		%		if timeIndex = index then
		%			self.fixNetwork[type, true, index, 1]
		%		else	
		%			self.fixNetwork[type, false, index, 1]
		%		end if
		%	end if
		%end unavailable
	end hello
	
	export operation printInfo
		(locate self)$stdout.putstring["Framework: Print out the System Info\n"]
		(locate self)$stdout.putstring["Framework: NameServers: \n"]
		for i:Integer <- 0 while i <= nameArrayDist.upperbound by i <- i + 1
			nameArrayDist[i].printInfo
			(locate self)$stdout.putstring[i.asString || " got assigned to host: " || nameArrayDist[i].getHost || " and is a "]
			if nameArrayDist[i].checkPrimary then
				(locate self)$stdout.putstring[" Primary Copy \n"]
			else
				(locate self)$stdout.putstring[" Replica\n"]
			end if
		end for
		(locate self)$stdout.putstring["Framework: TimeServers: \n"]
		for i:Integer <- 0 while i <= timeArrayDist.upperbound by i <- i + 1
			timeArrayDist[i].printInfo
			(locate self)$stdout.putstring[i.asString || " got assigned to host: " || timeArrayDist[i].getHost || " and is a "]
			if timeArrayDist[i].checkPrimary then
				(locate self)$stdout.putstring[" Primary Copy \n"]
			else
				(locate self)$stdout.putstring[" Replica\n"]
			end if
		end for
		
		
	end printInfo
	
	%During updating check if nodes dies, then fix it
	export operation notifyUpdate[type:Integer]
		
		self.testing[locate self, 2]
		var index: Integer <- 0
		if type = 0 then
			if nameArrayDist.upperbound = -1 then
				(locate self)$stdout.putstring["There are no Name Replicas to update\n"]
			else
				loop
					exit when index > nameArrayDist.upperbound
					nameArrayDist[index].notify
					index <- index + 1
				end loop
			end if
		else
			if timeArrayDist.upperbound = -1 then
				(locate self)$stdout.putstring["There are no Time Replicas to update\n"]
			else
				loop
					exit when index > timeArrayDist.upperbound
					timeArrayDist[index].notify
					index <- index + 1
				end loop
			end if
		end if
		
		unavailable
			(locate self)$stdout.putstring["Object died, fix the it then rerun this operation again\n"]
			self.fixNetwork[type, false, index, 2]	
			self.notifyUpdate[type]
		end unavailable
	end notifyUpdate
	
end Framework

const NameServer <- class NameServer[name:String, lt:Time, data:Array.of[PersonInfo], ref:Framework, primary:Boolean]
	%Variables
	var lastUpdate: Time
	var tmp: PersonInfo
	
	%Get hostname
	export operation getHost ->[res:String]
		res <- name
	end getHost
	
	%Get the date for the last update
	export operation getLastUpdate -> [res:Time]
		res <- lastUpdate
	end getLastUpdate
	
	%Promote this replica to be the new Primary
	export operation promotePrimary
		(locate self)$stdout.putstring["NameServer: Promoted to Primary Copy\n"]
		primary <- true
	end promotePrimary
	
	%Primary has been updated, fetch data from Primary
	export operation notify
		%The primary doesn't need to update itself
		if !primary then
			%Get the primary
			var prime: NameServer <- view ref.getPrimary[0] as NameServer
			
			var tempPack: UpdatePack <- prime.getUpdate
			lastUpdate <- tempPack.getTimeStamp
			data <- tempPack.getData
			%Ask for update
			(locate self)$stdout.putstring["NameServer: Got update from Primary Copy\n"]
		end if
		
		unavailable
			ref.fixNetwork[0, true, 0, -1]
			%ref.notifyUpdate[0]
			self.notify
			(locate self)$stdout.putstring["NameServer: Primary dead, call FrameWork\n"]
		end unavailable
	end notify
	
	%Check whether this is the Primary or not
	export operation checkPrimary -> [res:Boolean]
		res <- primary
	end checkPrimary
	
	%This operation is called by replicas that wnats the latest update
	export operation getUpdate -> [res:UpdatePack]
		var pack: UpdatePack <- UpdatePack.create[lastUpdate, data]
		res <- pack
		(locate self)$stdout.putstring["NameServer: Request for update from Primary Copy\n"]
	end getUpdate
	
	%Modify or add new data
	export operation updateData[input:PersonInfo] -> [res:Integer]
		(locate self)$stdout.putstring["NameServer: Update has been called\n"]
		%If is is the primary, then update it
		if primary then
			(locate self)$stdout.putstring["NameServer: This object is the Primary Copy, Updating\n"]
		
			%Check whether the object is already stored
			var index:Integer <- -1
			for i:Integer <- 0 while i <= data.upperbound by i <- i + 1
					if data[i].getName = input.getName then
						index <- i
					end if
			end for
		
			res <- 0
			%If the server contains the data, update it
			if index != -1 then
				data[index] <- input
				res <- 1
			else
				%New data to be stored
				data.addUpper[input]	
				res <- 1
			end if
			%Timestamp the latest update
			lastUpdate <- (locate self)$TimeOfDay
			(locate self)$stdout.putstring["NameServer: Primary Updated tell FrameWork to notify replicas\n"]
			lastupdate <- (locate self)$TimeOfDay
			ref.notifyUpdate[0]
		else
			(locate self)$stdout.putstring["NameServer: This object is not the Primary Copy, delegate the request to the Primary Copy\n"]
			%Delegate the update to the primary
			var prime: NameServer <- view ref.getPrimary[0] as NameServer
			res <- prime.updateData[input]
		end if
		
		unavailable
			(locate self)$stdout.putstring["NameServer: The Primary Copy is Unavailable, notify FrameWork\n"]
			ref.fixNetwork[0, true, 0, -1]
			res <- self.updateData[input]
		end unavailable
		
		failure
			(locate self)$stdout.putstring["NameServer UpdateData failed\n"]
		end failure
	end updateData
	
	%Get data from NameServer
	export operation getData[s:String] -> [res:PersonInfo]
		(locate self)$stdout.putString["NameServer: Request for data\n"]
		res <- nil		
		%Check whether the object is already stored
		var index:Integer <- -1
		for i:Integer <- 0 while i <= data.upperbound by i <- i + 1
				if data[i].getName = s then
					index <- i
				end if
					
				exit when index != -1
		end for
		
		if  index != -1 then
			res <- data[index]	
		else
			(locate self)$stdout.putstring["No data or the data doesn't exist in the server\n"]
		end if
	end getData
	
	%Whenever an object is cloned, the cloned object is automatically not the Primary 	
	export operation cloneMe -> [res:NameServer]
		res <- NameServer.create[name, lastUpdate, data, ref, false]
	end cloneMe
	
	export operation hello
		%Update the name to the host
		name <- (locate self)$name
		(locate self)$stdout.putstring["Hello From NameServer, Host: " || name || "\n"]
		if primary then
			(locate self)$stdout.putstring["Primary\n"]
		else
			(locate self)$stdout.putstring["Replicate\n"]
		end if
	end hello
	
	%Print out all info of the server
	export operation printInfo
		(locate self)$stdout.putstring["NameServer: Info\n"]
		if lastUpdate == nil then
			(locate self)$stdout.putstring["NameServer: There hasn't been any update\n"]
		else
			(locate self)$stdout.putstring["NameServer: Last Update was on " || lastUpdate.asDate.asString ||"\n"]
			for i:Integer <- 0 while i <= data.upperbound by i <- i + 1
				(locate self)$stdout.putstring[i.asString || ". PersonInfo\n"]
				(locate self)$stdout.putstring["Name: " || data[i].getName]
				(locate self)$stdout.putstring["Name: " || data[i].getAge]
				(locate self)$stdout.putstring["Name: " || data[i].getJob]
			end for
		end if
	end printInfo
	
	initially
		lastUpdate <- lt
	end initially
end NameServer

%Modify the input
export TimeServer
const TimeServer <- class TimeServer[name:String, tl:Time, median:Real, ref:Framework, primary:Boolean]
	var localTime: Time
	var timeArray: Array.of[Time] 
	
	var lastUpdate: Time %<- Time.create[0,0]
	
	var sec: Integer
	var ms: Integer
	%This is the time the TimeServer has to maintain, the Time Synchronization
	var timeSync: Real
	
	var one: Time <- Time.create[1 , 0]
	
	%Get Host
	export operation getHost -> [res:String]
		res <- name
	end getHost
	
	%Get timestamp of the last update
	export operation getLastUpdate -> [res:Time]
		res <- lastUpdate
	end getLastUpdate
	
	export operation promotePrimary
		(locate self)$stdout.putstring["TimeServer: Promoted to Primary Copy\n"]
		primary <- true
	end promotePrimary
	
	%Check whether this is the primary or not
	export operation checkPrimary -> [res:Boolean]
		res <- primary
	end checkPrimary
	
	%Get the latest update
	export operation getUpdate -> [res:Real]
		res <- timeSync
	end getUpdate
	
	%The callee of this operation is the FrameWork notifying replicas that the Primary Copy just updated
	export operation notify
		%The primary doesn't need to update itself
		if !primary then
			%Get the primary
			var prime: TimeServer <- view ref.getPrimary[1] as TimeServer
			%Ask for update	
			
			timeSync <- prime.getUpdate
			lastupdate <- prime.getLastUpdate
			(locate self)$stdout.putstring["TimeServer: Got update from Primary Copy\n"]
		end if
		
		unavailable		
			(locate self)$stdout.putstring["TimeServer: Primary dead, call FrameWork\n"]
			ref.fixNetwork[1, true, -1, 0]
			self.notify
		end unavailable
	end notify
	
	%Update the Time Synchronization
	%In the assignment I used call-by-move, but here I used call-by-visit, since it is not logical that a server can move around
	export operation updateTime
		(locate self)$stdout.putstring["TimeServer: Request for update\n"]
		if primary then
			var tArray: Array.of[TimeServer] <- ref.getReplicas
			%Make sure to reset it
			timeArray <- Array.of[Time].empty
			localTime <- (locate self)$TimeOfDay	
			timeArray.addUpper[localTime]
			for i:Integer <- 0 while i <= tArray.upperbound by i <- i + 1
				%Ignore the primary
				if !tArray[i].checkPrimary then
					%(locate self).delay[Time.create[1,0]]
					timeArray.addUpper[(locate tArray[i])$TimeOfDay]
				end if
			end for
			
			%Calculate the Time Synchronization
			var tmp:Time <- Time.create[0, 0]
			for a: Integer <- 0 while a <= timeArray.upperbound by a <- a + 1
				tmp <- tmp + (timeArray[a] - localTime)
			end for
			%(locate self)$stdout.putstring["TimeServer: " || tmp.asString || "\n"]
			sec <- tmp.getSeconds
			ms <- tmp.getMicroSeconds
			timeSync <- (sec.asReal + (ms.asReal/1000000.0))/(timeArray.upperbound+1).asReal
			(locate self)$stdout.putstring["TimeServer: " || timeSync.asString || "\n"]
			(locate self)$stdout.putstring["TimeServer: Primary Updated tell FrameWork to notify replicas\n"]
			lastupdate <- (locate self)$TimeOfDay
			ref.notifyUpdate[1]
		else
			(locate self)$stdout.putstring["TimeServer: This object is not the Primary, delegate the request to the Primary Copy\n"]
			var prime: timeServer <- view ref.getPrimary[1] as timeServer
			prime.updateTime
		end if
		
		unavailable
			(locate self)$stdout.putstring["TimeServer: The Primary Copy is Unavailable, notify FrameWork\n"]
			ref.fixNetwork[1, true, -1, 0]
			self.updateTime
		end unavailable
	end updateTime
	
	%Fetch nnew updated data
	export operation getTime -> [res:Real]
		(locate self)$stdout.putstring["TimeServer: Request for time\n"]
		res <- timeSync
	end getTime
	
	export operation cloneMe -> [res:TimeServer]
		res <- TimeServer.create[name, lastUpdate, timeSync, ref, false]
	end cloneMe
	
	export operation hello
		%Update the name to the host
		name <- (locate self)$name
		(locate self)$stdout.putstring["Hello From TimeServer, Host: " || name || "\n"]
		if primary then
			(locate self)$stdout.putstring["Primary\n"]
		else
			(locate self)$stdout.putstring["Replicate\n"]
		end if
	end hello
	
	%Print out all info of the server
	export operation printInfo
		(locate self)$stdout.putstring["TimeServer: Info\n"]
		if lastUpdate == nil then
			(locate self)$stdout.putstring["TimeServer: There hasn't been any update\n"]
		else
		(locate self)$stdout.putstring["TimeServer: Last Update was on " || lastUpdate.asDate.asString ||"\n"]
		(locate self)$stdout.putstring["The Time Synchronization between the time servers are " ||timeSync.asString|| "\n"]
		end if
	end printInfo
	
	initially
		lastUpdate <- tl
		timeSync <- median
	end initially
end TimeServer

