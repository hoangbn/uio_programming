%The Framework should offer functionalities to replicate a given object to a given number of machines.
export Framework

const Framework <- object Framework
	%Array storing the references to each object
	
	export operation replicateMe[pcr:PrimaryCopyReplication, number:Integer] -> [res:Array.of[PrimaryCopyReplication]]
		%Check number of machines/nodes, if it is lower than number then we will only make so many as we can
		
		
		for i:Integer <- 0 while i < number by i <- i + 1
			res.addUpper[pcr.cloneMe]
		end for
		
		failure
		
		end failure
		
	end replicateMe

end Framework