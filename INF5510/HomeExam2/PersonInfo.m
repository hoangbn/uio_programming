export PersonInfo
const PersonInfo <- class PersonInfo[name:String, age:String, job:String]
	export operation getName -> [n:String]
		n <- name
	end getName
	
	export operation getAge -> [a:String]
		a <- age
	end getAge
	
	export operation getJob -> [j:String]
		j <- job
	end getJob
	
	export operation updateName[n:String]
		name <- n
	end updateName
	
	export operation updateAge[a:String]
		age <- a
	end updateAge
	
	export operation updateJob[input:String]
		job <- input
	end updateJob
end PersonInfo