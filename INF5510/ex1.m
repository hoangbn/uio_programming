%Exercise 1 - Types
%Since the only requirement is to conform  the SimpleCollection operationsignatures

const SimpleCollection <- typeobject SimpleCollection
  operation add[name:String] -> [res:Boolean]
  operation contains[name:String] -> [res:Boolean]
  operation remove[name:String] -> [res:Boolean]
end SimpleCollection

const Collection <- object Collection
  var name: String 
  var size: Integer
  var myArray: Array.of[String]
  
  initially
    name <- ""
    size <- 0
    myArray <- Array.of[String].empty
  end initially

  export operation add[nm:String] -> [res:Boolean]
    if false = Collection.contains[nm] then
      name <- nm
      res <- true
    else	
      res <- false
    end if
  end add

  export operation contains[nm:String] -> [res:Boolean]
     if name = nm then
      res <- true
    else
      res <- false
    end if
  end contains


  export operation remove[nm:String] -> [res:Boolean]
    if name.length > 0 then
      res <- false
    else 
      name <- ""
      res <- true
    end if
  end remove

  process
    if true = Collection.add["Bao"] then
      stdout.putstring["name: " ||name|| "\n"]
      for i: Integer <- 0 while i < size by i<-i+1
	stdout.putstring[""||i.asstring||". "||myArray.getElement[i]||"\n"]
      end for
    else
      stdout.putstring["Couldnt add!!\n"]
    end if
   % stdout.putstring["Length of string: " ||name.length.asstring||"\n"]
   %  stdout.putstring["Size: "||myArray.lowerbound.asstring||"\n"]
    myArray.addUpper["Bao"]
  %   stdout.putstring["Size: "||myArray.lowerbound.asstring||"\n"]

  end process

end Collection



%Test whether it conforms to the type or not
const TestCollection <- object TestCollection
  initially
    if typeof(Collection) *> SimpleCollection then
      stdout.putstring["It conforms\n"]
    else 
      stdout.putstring["It doesn't Conform\n"]
    end if
  end initially
end TestCollection
