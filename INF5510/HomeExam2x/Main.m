export Framework
export NameServer
export TimeServer

const Framework <- class Framework

	export operation hello
		(locate self)$stdout.putstring["Hello Framework\n"]
	end hello

end Framework

const NameServer <- class NameServer

	export operation hello
		(locate self)$stdout.putstring["Hello NameServer\n"]
	end hello
end NameServer

const TimeServer <- class TimeServer

	export operation hello
		(locate self)$stdout.putstring["Hello TimeServer\n"]
	end hello
end TimeServer