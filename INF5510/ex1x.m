const SimpleCollection <- typeobject SimpleCollection
  operation add[name:String] -> [res:Boolean]
  operation contains[name:String] -> [res:Boolean]
  operation remove[name:String] -> [res:Boolean]
end SimpleCollection

const Collection <- object Collection
  export operation add[name:String] -> [res:Boolean]
    res <- true
  end add

  export operation contains[name:String] -> [res:Boolean]
    res <- true
  end contains

  export operation remove[name:String] -> [res:Boolean]
    res <- true
  end remove

end Collection

const Run
