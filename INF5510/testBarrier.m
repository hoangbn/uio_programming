const initialObject <- object initialObject
  const limit <- 4
  var count:Integer <- 0
  const newobj <- monitor object innerObject
    const c: Condition <- Condition.create
    const g: Condition <- Condition.create
    
    export operation gate
      count <- count + 1
      stdout.putstring["Waiting at the barrier\n"]
      if count < limit then
	wait c
      else
	signal g
      end if
      stdout.putstring["went through the barrier "||count.asstring||"\n"]
    end gate

    export operation openGate
      if count < limit then
	stdout.putstring["Waiting for barrier to fill up "||count.asstring||" \n"]
	wait g
      end if

      stdout.putstring["Out of loop "||count.asstring||" \n"]

      loop
	count <- count - 1
	signal c
	exit when count = 0
      end loop

    end openGate

    initially
      stdout.putstring["Starting Barrier Program\n"]
    end initially
  end innerObject
  
  const hoer <- object hoer
    process
      var i: Integer <- 0
      loop
	exit when i = limit
	newobj.gate
	i <- i + 1
      end loop
    end process
  end hoer
  
  const p1 <- object p1
  process
      var i: Integer <- 0
      loop
	exit when i = limit
	newobj.gate
	i <- i + 1
      end loop
    end process
  end p1

  const p2 <- object p2
   process
      var i: Integer <- 0
      loop
	exit when i = limit
	newobj.openGate
	i <- i + 1
      end loop
    end process
  end p2

  const p3 <- object p3
    process
      var i: Integer <- 0
      loop
	exit when i = limit
	newobj.gate
	i <- i + 1
      end loop
    end process
  end p3

  process
     var i: Integer <- 0
      loop
	exit when i = limit
	newobj.gate
	i <- i + 1
      end loop
  end process	
end initialObject
