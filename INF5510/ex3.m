%Exercise 3 - Sieve of Eratoshenes
%I will be using 3 chars which is
%U - Unmarked
%M - Marked
%P - Prime number
 
const Sieve <- object Sieve
  var prime: Integer
  var count: Integer
  var size: Integer
  var temp: Integer
  var myArray: Array.of[String]

  initially

    prime <- 2
    count <- 0
    temp <- 0
    %With size 100 then we get only from 0-99
    size <- 101
    myArray <- Array.of[String].create[size]
    for i: Integer <- 0 while i < size by i <- i+1
      myArray.setElement[count, "U"]
      count <- count + 1
    end for 
    count <- prime
  end initially

  process
    loop
      myArray.setElement[prime, "P"]
     %Uncomment this to get primtall 
     % stdout.putstring[prime.asstring||"\n"]
     %count <- count + prime
      loop
	count <- count + prime
	
	if count >= size then
	  count <- 0
	  exit
	else
	  if count != prime then
	    myArray.setElement[count, "M"]
	  end if
	end if
      end loop

      for a: Integer <- prime while a < size by a <- a + 1
	if myArray.getElement[a] = "U" then
	 % stdout.putstring["Hello\n"]
	  prime <- a
	  exit
	else 
	  %If we have come to this place then that means that its done
	  if a = size-1 then
	    stdout.putstring["Number: "||count.asstring||"\n"]
	    prime <- -1
	  end if
	end if
      end for

      if prime = -1 then
	exit
      end if

     % stdout.putstring["Infinite\n"]
%      if 1 = 1 then 
%	exit
%      else 
%	stdout.putstring["Infinite\n"]
%      end if
    end loop

    count <- 0
    for i: Integer <- 0 while i < size by i <- i+1
      if myArray.getElement[i] = "P" then
	 stdout.putstring[count.asstring||" "]
      end if
      
      %It was supposed to print 10 numbers then a newline but not so sure how modolus works in emerald
      if i # 10 = 0 then
	  stdout.putstring["\n"]
      end if
      count <- count + 1
    end for
  end process
end Sieve
