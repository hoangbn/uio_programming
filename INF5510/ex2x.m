const Person <- class Person[navn:String, alder:String]
  export operation insertName[nynavn:String] -> [res:Boolean]
    navn <- nynavn
  end insertName

  export operation getName -> [res:String]
    res <- navn
  end getName

end Person

const child <- class child(Person) [position:String]
  export operation getPosition -> [res:String]
    res <- position
  end getPosition
end child

const Main <- object Main
  var p: Person <- Person.create["Bao", 25]
  var p1: Child <- Child.create["Bao", 25, "Da DA"]

   

end Main
