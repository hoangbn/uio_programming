const message <- class message[tom: String, fromm : String, content : String]

	export operation getFrom[] ->[s: String]
		s <- fromm
	end getFrom
	
	export operation getTo[] -> [s : String]
		s <- tom
	end getTo

	export operation getContent[] -> [s : String]
		s <- content
	end getContent

end message

const Client <- class Client[addr: String, central: CentralServer]
	attached var msg : message
	

	export operation sendMessage[tom: String, content: String]
		msg <- message.create[tom,addr,content]
		central.distributeContent[msg]
		(locate self)$stdout.putstring["Message successfully sent\n"]

	end sendMessage


	export operation recvMessage[mess: message]
		
		(locate self)$stdout.putstring["Message received\n"]
		(locate self)$stdout.putstring["Sender: "||mess.getTo||"\n"]
		(locate self)$stdout.putstring["Content: \n"||mess.getContent||"\n"]
	end recvMessage

	export operation getAddr[] ->[s: String]
		s <- addr
	end getAddr
	


end Client


const CentralServer <- class CentralServer
	var tracker: Array.of[Client] <- Array.of[Client].empty
	
	export operation addClients[c: Client]
		tracker.addupper[c]
	end addClients

	export operation distributeContent[msg : message]
		for i : Integer <- 0 while i < tracker.upperbound + 1 by i <- i + 1
			if tracker.getElement[i].getAddr = msg.getTo then
				tracker.getElement[i].recvMessage[msg]
			end if
		end for
	end distributeContent
	

end CentralServer


const Main <- object Main
	var central: CentralServer <- CentralServer.create
	const home <- locate central
	var all : NodeList
	var there : Node
	var cli1 : Client <- Client.create["kkho@ifi.uio.no",central]
	var cli2 : Client <- Client.create["ericbj@ifi.uio.no",central]



	process
		central.addClients[cli1]
		central.addClients[cli2]
	
		all <- home$activeNodes
		move central to all[0].getTheNode
		move cli1 to all[1].getTheNode
		move cli2 to all[2].getTheNode

		cli1.sendMessage[cli2.getAddr,"Correct MY ASSIGNMENTS!"]


	end process


end Main
