const message <- class message[tom: String, fromm : String, content : String]

	export operation getFrom[] ->[s: String]
		s <- fromm
	end getFrom
	
	export operation getTo[] -> [s : String]
		s <- tom
	end getTo

	export operation getContent[] -> [s : String]
		s <- content
	end getContent

end message
const Client <- class Client[addr: String, central: CentralServer]
	attached var msg : message
	


	export operation sendMessage[tom: Array.of[String], content: String]
		for i : Integer <- 0 while i < tom.upperbound + 1 by i <- i + 1
			msg <- message.create[tom.getElement[i],addr,content]
			central.distributeContent[msg]
			(locate self)$stdout.putstring["Message successfully sent\n"]
		end for
	end sendMessage


	export operation recvMessage[mess: message]
		
		(locate self)$stdout.putstring["Message received\n"]
		(locate self)$stdout.putstring["Sender: "||mess.getFrom||"\n"]
		(locate self)$stdout.putstring["Content: \n"||mess.getContent||"\n"]
	end recvMessage

	export operation getAddr[] ->[s: String]
		s <- addr
	end getAddr
	


end Client


const CentralServer <- class CentralServer
	var tracker: Array.of[Client] <- Array.of[Client].empty
	
	export operation addClients[c: Client]
		tracker.addupper[c]
	end addClients

	export operation distributeContent[msg : message]
		for i : Integer <- 0 while i < tracker.upperbound + 1 by i <- i + 1
			if tracker.getElement[i].getAddr = msg.getTo then
				tracker.getElement[i].recvMessage[msg]
			end if
		end for
	end distributeContentx
	

end CentralServer


const Main <- object Main
	var central: CentralServer <- CentralServer.create
	const home <- locate central
	var all : NodeList
	var there : Node
	var cli1 : Client <- Client.create["kkho@ifi.uio.no",central]
	var cli2 : Client <- Client.create["ericbj@ifi.uio.no",central]
	var cli3 : Client <- Client.create["wwalmnes@ifi.uio.no",central]
	var listClients : Array.of[String] <- Array.of[String].create[2]

	process
		central.addClients[cli1]
		central.addClients[cli2]
		central.addClients[cli3]
	

		listClients.setElement[0,cli2.getAddr]
		listClients.setElement[1,cli3.getAddr]

		all <- home$activeNodes
		move central to all[0].getTheNode
		move cli1 to all[1].getTheNode
		move cli2 to all[2].getTheNode
		move cli3 to all[3].getTheNode
		

		(locate cli1)$stdout.putstring["I am: "||cli1.getaddr||"\n"]
		(locate cli2)$stdout.putstring["I am: "||cli2.getaddr||"\n"]
		(locate cli3)$stdout.putstring["I am: "||cli3.getaddr||"\n"]
		cli1.sendMessage[listClients,"Correct MY ASSIGNMENTS!"]


	end process


end Main
