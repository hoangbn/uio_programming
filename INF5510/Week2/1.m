const Client <- class Client

	export operation ping[]
	end ping

end Client



const Main <- object Main
const home <- locate self
var all : NodeList
var there : Node
var trackClients : Array.of[Client] <- Array.of[Client].empty
var timer : Time <- Time.create[2,0]

	export operation checkAvailable
		for j : Integer <- 0 while j < trackClients.upperbound+1 by j <- j + 1 
			trackClients.getElement[j].ping
		end for

		unavailable
			(locate self)$stdout.putstring["Node has crashed\n"]
		end unavailable

	end checkAvailable


	process
		all <- home$ActiveNodes
		
		for i : Integer <- 0 while i < all.upperbound + 1 by i <- i + 1 
			there <- all[i]$theNode
			var createClient : Client <- Client.create
			trackClients.addupper[createClient]
			move createClient to there
		end for	

		loop
			all <- home$ActiveNodes
			(locate self).delay[timer]
			self.checkAvailable

		end loop



	end process



end Main

