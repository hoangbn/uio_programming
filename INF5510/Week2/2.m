const TravelMan <- class TravelMan[]

	export operation statusAlive[]
		(locate self)$stdout.putstring["No war in here\n"]
	end statusAlive
end TravelMan







const Main <- object Main
const home <- locate self
var all : NodeList
var there : Node
var stays : Time <- Time.create[10,0]
var timer : Time <- Time.create[5,0]
var travelingMan : TravelMan <- TravelMan.create
	export operation checkUnavailable[t : TravelMan] ->[b:Boolean]
		b <- true
		t.statusAlive


		unavailable
			b <- false
			(locate self)$stdout.putstring["I guess I died\n"]
			all <- home$activeNodes
		end unavailable

	end checkUnavailable

	export operation rebirth[] ->[t:TravelMan]
		var tmp: TravelMan <- TravelMan.create[]
		t <- tmp		
	end rebirth
	
	

	process
		
		loop
			all <- home$activeNodes
			exit when all.upperbound +1 == 0
			
			(locate self).delay[timer]
			stdout.putstring["Initialize\n"]
			
			for i : Integer<- 1 while i < all.upperbound + 1 by i <- i + 1 
				there <- all[i]$theNode
				move travelingMan to there
				(locate travelingMan)$stdout.putstring["I arrived, I will take a break\n"]
				(locate self).delay[stays]
				if self.checkUnavailable[travelingMan] then
					
				else
					stdout.putstring["Rebirth\n"]
					travelingMan <- self.rebirth
				end if
			end for
		end loop

	end process



end Main
