const Person <- class Person[navn:String]
        
        var n: String
        
        initially
                n <- navn
        end initially
        
        process
                stdout.putstring[n||"\n"]
        end process
        
end Person


const Teacher <- class Teacher(Person)[skole:String]
        
        class export function getSuperClass -> [r : Any]
                r <- Person
        end getSuperClass
        
        var s: String
        
        initially
                s <- skole
        end initially
        
        
        process
                stdout.putstring[s||" "||n||"\n"]
        end process

end Teacher


const Classes <- object Classes
        
        var navn: String
        var skole: String
        var Sup: Any
        var Sub: Any

        initially
                navn <- "Jesper"
                skole <- "HVGS"
                Sup <- Person.create[navn]
                Sub <- Teacher.create[navn, skole]
                
        end initially

end Classes
