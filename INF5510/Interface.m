const Interface <- typeobject Interface
	var sNode: Server
	
	var list: Array.of[Peer] <- Array.of[Peer].empty

	export operation getList[s:String] -> [l.Array.Of[Peer]]
		l <- list
	end getList
end Interface
