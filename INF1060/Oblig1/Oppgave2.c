#include <stdio.h>
#include <stdlib.h>
#include <string.h> 

/*Guessing that since we doesn't have much memory, then we're gonna just read char by char straight form file and work with that, by printing or write etc.*/
FILE *file, *output;
char c;
/*Didn't really see this till I looked at the ASCI-II tabell, if I use bit operator | 0,1, 2, 3 to set/copy a bit(00,01,10,11) on existing*/




/*reads straight from file and takes char by char and print it out on screen, runs till the file reach the end*/
void printOutFile()
{
  c = getc(file);  /*reads the first char from file*/
  while(c != EOF){
    putchar(c);
    c = getc(file);  /*reads the next char in file*/
  }
  printf("\n");
}
  
/*pretty much the same as printOutFile metod, the differents is that when it reads char by char it will convert from ASCII-representation to bit and compress then write it in a new file.
Remember that 0, 1, 2 ,3 is the same as 00, 01, 10, 11 from ASCI-II*/
void encodeOrDecompress()
{
  int count = 0;     /*Make sure that it gets filled up exactly right, or else we will erase the old bits*/
  char encode = 0;
  char test = 0;
  c = getc(file);  /*get a char from file, prolly the first one...:P*/
  
  while(c != EOF){ /* runs til we get to the end of file*/
    /*     00000000         */
    /*           II those place will be overwritten each time the loop goes, but since we bitshift it to the left each time, our info for which char was saved won't be overwritten*/
    if(c == ' '){
      encode = encode | 0; 
    } else  if(c == ':'){
      encode = encode | 1; 
    } else  if(c == '@'){
      encode = encode | 2; 
    } else  if(c == '\n'){
      encode = encode | 3;
    }
    if(count == 3){
      fprintf(output, "%c", encode);     
      encode = 0;               /* have to refresh so that we can start all over again*/
      count = -1;
    }
    count++;
    encode = encode << 2; /*bitshift to the left 2 times, so it will not get overwritten next time we try to set*/
    
   
    c = getc(file);
  }
  
  /*if there is still some char to write*/ 
  fprintf(output, "%c", encode);
  fclose(output);
  
}


/*take a char from file and bitshift it 8 times per char to get the corresponding bit-representation of the char we are after, then compare it int*/
void decodeOrUncompress()
{
  unsigned char decode;  
 
  c = getc(file);   /*Get the first char from file*/

  while(c != EOF){    /*runs till the end of file*/
   
    int i;
    for(i = 0; i < 4; i++){
      decode = c << 2*i;       /*bitshift to the left 2*i times, and then bitshift to the right so we will get the bit representation*/
      decode = decode >> 6;      /*we were looking for, and it will mean we get a nummer in range 0-3*/
   
      if(decode == 0){
	printf("%c", ' ');
      } else  if(decode == 1){
	printf("%c", ':');
      } else  if(decode == 2){
	printf("%c", '@');
      } else  if(decode == 3){
	printf("%c", '\n');
      }
    }
    
    c = getc(file);      /*get next char from file*/
  }
  printf("\n");
  fclose(file);      /*close file*/
}

/*print out a list of commands*/
void commandList(){
  printf("USAGE: ./a.out command input_file output_file\n\nUse the following command\n   p    print input_file\n        (output_file is ignored if specified)\n   e    encode/compress input_file to output_file\n   d    decode/uncompress and print input_file\n        (output_file is ignored if specified)\n");
}
  
/*reads in file and check if it's there, tries to open file to overwrite and if there isn't a file already, it will create a new one to write.*/
int main(int argc, char *argv[])
{
    
  /*At least 3 arguments*/
  if(argc < 3 || argc > 4){
    printf("There are to few/many arguments, please try again\n");
    commandList();
  } else {
    file = fopen(argv[2], "r");
    if(file == NULL){
      printf("Couldn't open the file: %s\nPlease make sure it's the right file\n\n", argv[2]);
      commandList();
      return;
    } else {
      if(strcmp(argv[1], "p") == 0){
	printOutFile();
      } else if(strcmp(argv[1], "e") == 0){
	output = fopen(argv[3], "w+"); 
	if(output == NULL){
	  printf("Couldn't open/create the file: %s\n\n", argv[3]);
	}
	encodeOrDecompress();
      } else if(strcmp(argv[1], "d") == 0){
	decodeOrUncompress();
      } else {
	printf("Something was wrong with the arguments, please try again\n");
	commandList();
      }
    }
  }
}
    
  






