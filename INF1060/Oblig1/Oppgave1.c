#include <stdio.h>
#include <stdlib.h>
#include <string.h> 


FILE *file;
char data[1000];
char vowels[] = "aeiouy���";

/*struct is like classes in Java, but with all fields public and no methods,lie in java with nodes */

struct node {
  char *data;
  struct node *next;
};

struct node *first = NULL;

/*print a random line from list, we'll use srand() to get the time as a modulus operator so we can get more randomness since the time is always changing, but you have to wait till the time have changed to get differents randomNr*/
/*traverse through the list till the size is lower than randomNr, and jumps to the next node each time i is smaller than randomNr*/

void randomLine(int size)
{
  srand(time(NULL));  
  int randomNr = (rand() % size);
  struct node *curNode = first;
  
  int i;
  for(i = 0; i < size; i++){
    if(i < randomNr){
      curNode = curNode->next;
    }
  }
  printf("Print a random line:\n   %s\n", curNode->data);   
}

/*Finds all Norwegian vocals and replace it with a different vocal, weakness in this is it can't see the difference from upper- to lower-case chars, but we could extend the method to use lower case in all char*/

void replaceChar()
{
  char *tempLine;
  int toPrint = strlen(vowels);
    
  printf("Replace vowels ...\n");
  int i;
  for(i = 0; i < toPrint; i++) {
    printf("... with vowel '%c'\n", vowels[i]);
    
    struct node *temp = first;
 
    while(temp != NULL){
      printf("   ");
      tempLine = temp->data;
      int j;
      for(j = 0; j < strlen(temp->data);j++) {
	if(tempLine[j] == 'a') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == 'e') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == 'i') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == 'o') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == 'u') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == 'y') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == '�') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == '�') {
	  printf("%c", vowels[i]);
	} else if(tempLine[j] == '�') {
	  printf("%c", vowels[i]); 
	} else {
	  printf("%c", tempLine[j]);
	}
      }  
      temp = temp->next;      
    }
    printf("\n\n");
  }
}

/*It wasn't necessary to remove the char from list, so I just skipped all vocals, pretty much identical to replace method. Same weakness as the replace method, since it can't differentiate upper- and lower case*/
void removeChar()
{
  char *tempLine;
  int toPrint = strlen(vowels);
    
  printf("remove vowels:\n");
  int i;
  struct node *temp = first;
  
  while(temp != NULL){
    printf("   ");
    tempLine = temp->data;
    int j;
    for(j = 0; j < strlen(temp->data);j++) {
      if(tempLine[j] == 'a') {
	printf("");
      } else if(tempLine[j] == 'e') {
	printf("");
      } else if(tempLine[j] == 'i') {
	printf("");
      } else if(tempLine[j] == 'o') {
	printf("");
      } else if(tempLine[j] == 'u') {
	printf("");
      } else if(tempLine[j] == 'y') {
	printf("");
      } else if(tempLine[j] == '�') {
	printf("");
      } else if(tempLine[j] == '�') {
	printf("");
      } else if(tempLine[j] == '�') {
	printf(""); 
      } else {
	printf("%c", tempLine[j]);
      }
    }  
    temp = temp->next;      
  }
  printf("\n");
}



/*running through the list and instead of replacing or remove, just count, it gets the node and finds its data and check how long the line is, for-loop will just run as many times as there is chars*/
void countChar()
{
  char *tempLine;
  int count = 0;
  struct node *temp = first;
  
  while(temp != NULL){
    tempLine = temp->data;
    int j;
    for(j = 0; j < strlen(temp->data);j++) {
      count++;
    }
    
    temp = temp->next;      
  }
  printf("The text is %d character long\n", count);
}


/*traverse the list and print out all lines*/
void printOut() 
{
  struct node *curNode;
  
  curNode = first;
  
  printf("Print text:\n");
  while(curNode != NULL){
    printf("   %s", curNode->data);
    curNode = curNode->next;
  }
  printf("\n");

}

/*Adds a line in the linked list*/
void addLine(char *data) 
{
  struct node *tempNode;
  struct node *curNode;
  
  tempNode = malloc(sizeof(struct node));   /*makes an allocation in memory*/
  tempNode->data = malloc(strlen(data));     
  strcpy(tempNode->data, data);
 
  /*if the list is empty*/
  if(first == NULL) {
    first = tempNode;
    first->next = NULL;
    return;
  }
 
  curNode = first;      
  
  while(curNode->next != NULL){                /*traverse the list till we get to NULL*/
    curNode = curNode->next;
  }
  /* sets temp next to null so when we traverse the list, we'll know when we get to the end*/
  curNode->next = tempNode;                    /*Since we have traversed the list to the last node, we know that curNode->next is a null*/
  tempNode->next = NULL;
}


/*runs through the list and use the free() method to free all memory that was used by this program*/
void freeResources()
{
  struct node *freeRes = first;

  while(freeRes != NULL){
    first = freeRes->next;
    free(freeRes->data);
    free(freeRes);
    freeRes = first;
  }  
}


/* argc counts our arguments from command-line, and argv saves it in an array */
/*Reads from txt file line by line and adds it in the linked list*/
int main(int argc, char *argv[])
{
  int count;
 
 /* if everythin is going according to plan, then we will get 3 arguments, the first is "a.out" in this case, and the 2 others are our command + filename.*/
  if(argc < 3 || argc > 3){
    printf("There are too few/many arguments, please try again\n");
    return 0;
  } else {
    file = fopen(argv[2], "r");     
    if(file == NULL){
      printf("Filen fins ikke\n,");
      return 0;
    } 
    int countline = 0;
   
    /*reads line by line to it gets to the end of file*/
    while(fgets(data, 1000, file) != NULL) { 
      addLine(data);
      countline++;
    }
    fclose(file);
 
    if(strcmp(argv[1], "print") == 0){
      printOut();
    } else if(strcmp(argv[1], "random") == 0){
      randomLine(countline);
    } else if(strcmp(argv[1], "replace") == 0){
      replaceChar();
    } else if(strcmp(argv[1], "remove") == 0){
      removeChar();
    } else if(strcmp(argv[1], "len") == 0){
      countChar();
    } else {
      printf("Something wrong with arguments, make sure you use these commands:\nprint        print input_file\nrandom       print a random line\nreplace      replace the vowels with all the other vowels\nremove       remove vowels\nlen          print the number of characters in the input_file\n");
      
    }
  }
  freeResources();
}
	
