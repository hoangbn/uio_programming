#include "safefork.h"
#include "commands.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>  
#include <sys/wait.h>  /*Wait for termination of a processs*/
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

char line[120];
char newLine[120];   
int len;            /*line's length*/
char **param;      /*A 2D-array with the pointers  to an array of chars*/
char **path;       /*Same*/
char *username;     
char *prog;        /*extreme makeover of command*/ 
char *bitmap;     
char *datablocks;
int bool = 0;

extern pid_t safefork(void);
typedef void (*sighandler_t)(int);

/*If user used CTRL-C, this will run. and instead of process being killed, program will exit normally*/
void forceQuit(int signal)
{
  free(username);
  free(param);
  freeHistory();
  free(path);
  free(bitmap);
  free(datablocks);
  exit(0);
}

void freeOld()
{
  int i;
  for(i = 0; i < 21; i++) {
    if(param[i] == 0) break;
    free(param[i]);
  } 
}

/*frees all the memory we've allocated in the array og chars*/
void freeMemory()
{ 
  free(prog);
  int i;
  for(i = 0; i < 21; i++) {
    if(param[i] == 0) {
      free(param[i]);
      for(i = 0; i < 21; i++) {
	if(path[i] == 0) {
	  free(path[i]);
	  return;
	} else {
	  free(path[i]);
	}
      }
    } else {
      free(param[i]);
    }
  } 
}

void callChild()
{
  int status;
  pid_t pid;
  pid = safefork();      /*create a new child process*/
 /*  if(pid >= 0){        /\*testing if safefork() succeded in creating a new process*\/ */
  if(pid == 0){                    /*in the child process*/
    int err = execve(prog, param, path);
    if(err == -1){
      
      printf("inf1060-sh: %s: command not found\n", param[0]);
    }
    freeMemory();
    free(bitmap);
    freeHistory();
    free(datablocks);
    free(username);
    free(param);
    free(path);
    exit(0);
  } else if(pid < 0){                  /*if pid is < 0 (in this case -1), then it failed to create a child process*/
    printf("failed to create a child process\n");
  
  } else {
    if(strchr(line, '&')){
      printf("Barne PID: %d\n", pid);
      
    } else {
    pid = wait(&status);    /*wait for the child process to end*/
    }
  }
}

void split_line()
{
  int i = 0; 
           
  char *temp = NULL;
  temp = strtok(line, " \t");
  param[i] = malloc(120 * sizeof(char));
  strcpy(param[i], temp);
  while(temp != NULL)
    {
      i++;
      temp = strtok(NULL, " \t");
      if(temp == NULL){
	break;
      }
	  param[i] = malloc(120 * sizeof(char));
	  strcpy(param[i], temp);
	  if(strchr(param[i], '\n')){
	    *(strchr(param[i], '\n')) = '\0';
	  }
    }
  param[i] = '\0';                          /*we set the next place to the last inputtet to 0*/
}

void split_path()
{
  int i = 0;
  char *temp = NULL;
  char *pathname = getenv("PATH");
  temp = strtok(pathname, ":");
  path[i] = malloc(120 * sizeof(char));  /*bot sure the mazx length of a path, so chose the same length as a line*/
      
  strcpy(path[i], temp);                 /*copies the word from line and copies it to the allocated memory*/
  strcat(path[i], "/");                  /*adds chars/strings at the end of source string*/
  strcat(path[i], param[0]);
      
  while(temp != NULL){
    i++;
    temp = strtok(NULL, ":");
    if(temp == NULL){
      break;
    }
    path[i] = malloc(120 * sizeof(char));
    strcpy(path[i], temp);               /*copies the word from line and copies it to the allocated memory*/
    strcat(path[i], "/");          /*adds chars/strings at the end of source string*/
    if(strchr(path[i], '\n')){           /*check if there is a newline*/
      *(strchr(path[i], '\n')) = 0;      /*there is a newline, overwrite it's place with 0 or \0*/
    }

  }
  path[i] = '\0';      /*n+1 position in array sets to \0, n is all the place that's taken*/ 
}

void make_commando()
{
  char *tempProg = "/bin/\0";
  
  prog = malloc(120 * sizeof(char));
  strcpy(prog, tempProg);
  strcat(prog, param[0]);
      
  if(strchr(prog, '\n')){
    *(strchr(prog, '\n')) = 0;
  }
}


int main()
{
  int counter = 1;
  
  signal(SIGINT, forceQuit);                        /*this catches a signal that comes from CTRL-C(SIGINT) and use the forceQuit() method*/

  username = malloc(sizeof(username));              /*allocate memory*/
  strcpy(username, getenv("USER"));                 /*Copies the string to the allocated memory place*/
  
  path = malloc(21 * sizeof(char*));                  /*allocates memory for a 2D-array*/
  param = malloc(21 * sizeof(char*));                 /*it's the same as *param[21], allocates memory*/
  bitmap = malloc(8 * sizeof(char));
  memset(bitmap, '\0', 8);
  datablocks = malloc(64 * 8 * sizeof(char));
  memset(datablocks, '\0', 512);
  
  
  printf("%s@inf1060-sh %d> ", username, counter);
  
  while(fgets(line, 120, stdin) != NULL) {
#ifdef DEBUG
    printf("\nthe line that was inputted: %s\n", line);
#endif
    if(strncmp(line, "exit", 4) == 0 || strncmp(line, "quit", 4) == 0){ /* check if user gave quit or exit command, then free the memory we've allocated*/
    /*   freeMemory(); */
      free(username);
      free(param);
      free(path);
      free(bitmap);
      free(datablocks);
      freeHistory();
      exit(0);
    } else {
      len = 0;
      int i;      
      for(i = 0; i < 120;i++){
	if(line[0] == '\n' || line[i] == '\0') break;
	newLine[i] = line[i];
	len++;
      }
      counter++;                  /*counter to include in prefix*/      
   
      split_line();               /*call on method to split lines*/
       
      if(strncmp(param[0], "h", 1)){
	  bool = 1;
      }
#ifdef DEBUG
      printf("Words that were stored in param-array:\n");
      
      i = 0;
      while(param[i] != NULL){
	printf("%s\n", param[i]);
	i++;
      }
#endif
      
      if(strncmp(param[0], "h", 1) == 0 && param[1] != NULL && isdigit(param[1][0])){
	runStory(atoi(param[1]));
	fprintf(stderr, "HER\n");
	freeOld();
	split_line();
      }

      split_path();               /*call on method to split paths*/
      make_commando();            /*process the commando to use in execve in callChild()*/
      
      callChild();                /*makes a child process*/
      
      if(strncmp(param[0], "h", 1) == 0 && param[1] != NULL && strncmp(param[1], "-d", 2) == 0 && isdigit(param[2][0])){
	rmPart(atoi(param[2]));
      }

      save();
           
      if(strncmp(line, "h\n", 2) == 0){
	history();
      }
      
#ifdef DEBUG
      printBit();
#endif
      
#ifdef DEBUG
      printf("DEBUG - BITMAP:\n");
      
      for(i = 0; i < 64; i++){
	if(i % 4 == 0) printf("\n");
	printf("#");
	printDatablock(i);
	printf("#");
      }
      printf("\n");
#endif
         
      printf("%s@inf1060-sh %d> ", username, counter);
      freeMemory();
    }
  }
  free(path);
  free(username);
  free(param);
  free(bitmap);
  free(datablocks);
  freeHistory();
  exit(0);
}
