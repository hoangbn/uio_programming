#ifndef SAFEFORK
#define SAFEFORK
#include <sys/types.h>

pid_t safefork(void);

#endif
