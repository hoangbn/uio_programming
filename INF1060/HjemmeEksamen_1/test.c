/* I'm trying to learn some C++ and thus want to make a small test program in the form of a command-line intrepeter. However, I'm getting a segmentation fault in the childprocess and a guy I talked to who knows programming a lot better than me said there was two faults in line 82, and that I should also clean up the amounts of zombie processes by adding the following line */
/* while(waitpid(0,&status,WNOHANG|WUNTRACED)>0); */

/* What he forgot to tell me is where to add that line though. Although I had a bunch of other errors, I managed to fix most of them (I think), but those two still remain a small mystery to me. Problem is he's in Australia on vacation for the next month, so I have a hard time getting a hold of him. Thus I ask the question here. */

/* I removed the comments as they're in norwegian in any case (would have been very confusing :P). I'll mark "line 82" with a big comment for you :) */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "safefork.c"

#define MAX 250

extern char **environ;
char **path;
char *pathstart;
int ant_path;

int main(void) {
  char cmd[MAX], prev[MAX];
  char *param[30];
  char *pathen;
  char *arg;
  int ant_param, tom, ws, amp, i, status;
  pid_t pid, n;
 
  get_path();

  while(1){
    ant_param = 0;
    amp = 0;
    ws = 0;
    tom = 0;

    printf("ifish>");
   
    if(fgets(cmd, MAX, stdin) == NULL) exit(1);

    if(strcmp(cmd, "exit\n") == 0) exit(1);
   
    if(strcmp(cmd, "\n") == 0) tom = 1;
 
    if(tom == 0) {
      cmd[strlen(cmd)-1] = '\0';
     
      if(strcmp(cmd, "!!") == 0) strcpy(cmd, prev);
     
      strcpy(prev, cmd);
     
      amp = sjekk_og(cmd);
      arg=strtok(cmd, " ");
     
     
      param[0] = arg;
     
      do{
      
      param[++ant_param] = strtok(NULL, " ");
      }
      while(param[ant_param] != NULL);
     
      pid = safefork();
     
      if(pid == 0){
      for(i=0; i<ant_path;i++) {
        pathen = malloc(strlen(path[i]+1)); //*******LINE 82********
        strcpy(pathen, path[i]);
        strcat(strcat(pathen, "/"), arg);
        execve(pathen,param,environ);
        
        free(pathen);
      }
      printf("ifish: %s: Command not found\n", param[0]);
      exit(1);
      }
      else {
      if(amp == 0){
        do{         
          n = wait(&status);
        }
        while(n != pid);
      }
      else {
        printf("Pid: %d\n", pid);
      }
      }
    }
  }
}//slutt main

int sjekk_og(char *cmd) {
  char *og;
  og = strrchr(cmd, '&');
  if(og != NULL)
    if(*++og == '\0' || *og == ' ') {
      *--og= '\0';
      return 1;
    }
  return 0;
}

int get_path() {
  int i;
  char *pathcpy;
  pathstart = getenv("PATH");
  pathcpy = strdup(pathstart);
  pathstart = pathcpy;
 
  while(1) {
    if(*pathcpy == '\0') {
      i = 0;
      break;
    }
    if(*pathcpy == ':') ant_path++;
    pathcpy++;
  }
  ant_path++;
  path = malloc(sizeof(char*)*ant_path);
  path[0] = strtok(pathstart, ":");
 
  for(i=1; i<ant_path; i++) path[i]=strtok(NULL, ":");
 
  return 1;
  free(path);
}




and safefork.c looks like this:

#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/errno.h>

extern int errno;

#define MAX_PROCESSES 6

static int n_processes(void)
{
  return system("exit `/local/bin/ps | /local/bin/wc -l`")/256;
}


pid_t safefork(void)
{
  static int n_initial = -1;
 
  if (n_initial == -1)
    n_initial = n_processes();
  else if (n_processes() >= n_initial+MAX_PROCESSES) {
    sleep(2);
    return -EAGAIN;
  }
 
  return fork();
}

/* PS: the safefork.c was something my friend gave me. Said it was safer to start with than the normal fork operations. */

/* Anyways, any help is appreciated. */

/* Thanks, */
/* ZombieBobie */
