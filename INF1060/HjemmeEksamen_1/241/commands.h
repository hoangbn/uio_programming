#ifndef COMMANDS /*sikring for at det ikke blir brukt duplikater*/
#define COMMANDS

void history();
void save();
void printDatablock(int pos);
void freeHistory();
void rmPart(int i);
void printBit();
void runStory(int i);
#endif
