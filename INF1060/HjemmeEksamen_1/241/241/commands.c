#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

extern char *bitmap;
extern char *datablocks;
extern char line[120];
extern char newLine[120];
extern int len;
extern char **path;

char tempLine[120];
char **command;

int lineCount;
int histCount = 0;
int bitFree = 64;
int bitNeeded;

/*acting like a node where we link it together   */
/*length of the string saved                     */
/*how many bits we used to save the string       */
/*data[15] is a buffer for 1-15 bits that we may */
/*have used in saveing the string                */
struct metablock {
  int length;                    
  int bitused;                    
  int data[15];                  
  struct metablock *next;
  struct metablock *prev;         
};
struct metablock *first = NULL;           /*list starts here*/

/*clear the bytes starting at pos*/
void clearDatablock(int pos)
{
  int start = pos * 8;
  bzero(&datablocks[start], 8);  /*sets 8 bytes to zero, starting from 'start'*/ 
}

/*copies the chars in the allocated 512 bytes in primary memory*/



void saveDatablock(int b)
{
  int start = b * 8;
  int end = start + 8;
  
  int i;
  for(i = start; i < end; i++){
    strncpy(&datablocks[i], &newLine[lineCount], 1);    /*copies string to memory*/                       
    lineCount++;                                        /*counts, so that we know which chars from newLine we've copied so far*/
    len--;                                              /*counts how many chars we have left*/
    if(len == 0) break;                                 /*no more place break*/
  }
}

/*save bitmaps, datablocks and bits used information in a FIFO linked list*/ 
void saveMetablock(int leng, int *array, int used)
{
  struct metablock *tempBlock;                     /*temporary block*/
  struct metablock *curBlock;                      /*block we are on*/
  
  tempBlock = malloc(sizeof(struct metablock));    /*allocate memory*/
  tempBlock->length = leng;                        /*length of string */
  tempBlock->bitused = used;                       /*bits used to save the string*/
  

  /*copies the *array over to block*/
  int i;
  for(i = 0; i < 15; i++){
    if(array[i] == -1){
      break;
    } else {
      tempBlock->data[i] = array[i];
    }
  }
  
  if(i < 14){
    tempBlock->data[i] = '\0';                    /*if array isn't filled use null byte to determine when it should stop*/
  }
  
  /*if there is no list*/
  if(first == NULL){
    first = tempBlock;                            /*sets to first*/
    first->next = NULL;                           /*next to null, then we know where the end is*/
    first->prev = NULL;                           /*so we know our way back*/
    return;
  }
   
  /*if there is a list*/
  curBlock = tempBlock;                                    
  first->prev = curBlock;                          /*makes a link between them*/
  curBlock->next = first;                          
  first = curBlock;                                /*takes first position*/
  first->prev = NULL;
  return;
}


/*returns true if the bit at pos in char (c) is set. */
/*Teacher said it was okay to implement methods from exercise he's given*/ 
int checkBit(char c, int pos)
{
  char mask;
  mask = 1 << (7-pos);                       /*1 -> 00000001 in binary, so we're bitshifting left*/
  return c & mask;                           /*returns an int so we can check if the bits is taken or not*/
}

/*We make a bit int byte to 0*/
void clearBit(int byte, int pos, int data)
{
  char mask;
  mask = ~(1 << (7-pos));
  bitmap[byte] = bitmap[byte] & mask;          /*& bitoperater makes it so that both bits have to be the same to be true, else 0*/
  clearDatablock(data);                        /*bits is clear so we want to clear the corresponding byte, just multiply with 8 and we find the spot*/
}

/*sets a bit to 1 to say that this bit is taken*/
void setBit(int byte, int pos)
{
  char mask;
  mask = 1 << (7-pos);
  bitmap[byte] = bitmap[byte] | mask;         /*| bitoperator makes it so that if either of the bits are 1, then it's true, meaning set the bit to 1*/ 
  bitFree--;
} 

/* prints a char bit by bit, this will only run in debug mode*/
/*from exercise we got in week 37, it's identical*/
/*if it's true then 1 will print, otherwise 0*/
void printBit()
{
  printf("DEBUG - BITMAP:\n");
  int i;
  for(i = 0; i < 8;i++){
    if(i == 4) putchar('\n');  
    int j;
    for(j = 0; j < 8; j++){
      putchar(checkBit(bitmap[i], j) ? '1' : '0');
    }
  }
  putchar('\n');
}

/*run through the bitmap, and look for free bits to save the commando user typed*/
int checkMap()
{
  int i;
  for(i = 0; i < 64;i++){
    if(!checkBit(bitmap[i/8], i%8)){             /*use of i/8 and i%8 will always give us the right byte and bit position*/
      setBit(i/8, i%8);                          /*set this bit to 1*/
      saveDatablock(i);                          /*done stting the bit, then save info to datablock*/
      return i;
    }
  } 
  return 0;
}

/*run through the list and free all allocated memory we've used*/
void freeHistory()
{
  struct metablock *temp = first;
  
  while(temp != NULL){
    first = temp->next;
    free(temp);                    
    temp = first;
  }
  histCount = 0;                /*resets the count for history*/
}

/*When there is no space left in datablocks to save, then we try to make more space*/
/*by deleting the oldest command                                                   */
void freeSpace()
{
  struct metablock *temp = first;
  struct metablock *previous;
  
  while(temp->next != NULL){
    previous = temp;
    temp = temp->next;
  }

  int bits = temp->length / 8;              /*find out how many bits we used*/
  if((temp->length % 8) > 0) bits++;       
  
  int i;
  for(i = 0; i < bits;i++){
    clearBit(temp->data[i] / 8, temp->data[i] % 8, temp->data[i]);        /*clear the bits*/
    bitFree++;                                                            /*updates how many bites we got left*/
  }
  free(temp);                                                             /*free it*/
  previous->next = NULL;                                                  /*tell it's previous that it's the last in list*/
}

/*remove a commando line from history given a position*/
int rmPart(int i)
{
  if(i > histCount){                                                         /*test if user typed a position out of reach*/
    fprintf(stderr, "Please try with a number lower than %d\n", histCount);
    return 0;
  } 

  struct metablock *temp = first;
  struct metablock *previous;
  
  /*loop to the metablock which store this commando line*/
  int a;  
  for(a = 1; a < i;a++){
    temp = temp->next;
  }
  
  /*find how many bits we used*/
  int bits = temp->length / 8;
  if((temp->length % 8) > 0) bits++;

  if(temp == first){                                              /*if temp is first in list*/
    for(a = 0; a < bits; a++){
      clearBit(temp->data[a] / 8, temp->data[a] % 8, temp->data[a]);
      bitFree++;
    }
    first = temp->next;
    temp->next->prev = first;
    free(temp);
    histCount--;
    return 1;
  } else {
    for(a = 0; a < bits; a++){                                         /*we know temp is not first*/
      clearBit(temp->data[a] / 8, temp->data[a] % 8, temp->data[a]);
      bitFree++;
    }
    previous = temp->prev;
    if(temp->next == NULL){                            /*check if this is last in list*/
      previous->next = NULL;
      free(temp);
      histCount--;
      return 1;
    } else {                                         /*it's in the middle of two block*/
      previous->next = temp->next;
      temp->next->prev = previous;
      free(temp);
      histCount--;                                   /*update count for history*/
      return 1;
    }
  }
}

/*loop through list and find the corresponding history command */
/*and change the commando line user inputted to the history one*/
int runStory(int i)
{
  int counts = 0;
  int bit = 0;

  if(i > histCount-1){                                                           /*if user typed a position out of reach*/
    fprintf(stderr, "That command %d isn't saved in buffer yet\n", histCount);
    return 0;
  }

  memset(&line, '\0', 120);        /*resets line */
    
  struct metablock *temp = first;
 
  /*loop to the history command we're looking for*/
  int a;
  for(a = 1; a < i;a++){
    temp = temp->next;
  }
  
  /*update the resetted line with the one in history*/
  for(a = 0;a < temp->bitused;a++){
    if(temp->data[a] == '\0'){
      break;
    }
    bit = temp->data[a];
    
    int start = bit * 8;
    int end = start + 8;
    
    int b;
    for(b = start;b < end;b++){
      strcpy(&line[counts], &datablocks[b]);
      counts++;
   
    }
  }
  return 1;
}

/*the saving process starts here, and it controls all saving to history*/
void save()
{
  if(len == 0){
    return;
  }
  len = len - 1;               /*length of string -1*/
  lineCount = 0;

  bitNeeded = 0;               /*resets bit*/
  bitNeeded = len / 8;
  if(len % 8 > 0) bitNeeded++;
  
  int which = 0;
  int array[15];
  int i;
  
  for(i = 0; i < 15; i++) array[i] = -1;
  
  int save = len;
  while(1){                                     /*loop forever til all the data we inputted is saved*/
    if(bitNeeded <= bitFree){    
      for(i = 0; i < bitNeeded; i++){
	which = checkMap();
	array[i] = which;
      }

      saveMetablock(save, array, bitNeeded);
      histCount++;
    }  
    if(len == 0){
      break;
    } else {
      freeSpace();                             /*need more space to save, so free some*/
      histCount--;
    }
  } 
}

/*prints out what's in datablocks*/
void printDatablock(int pos)
{
  int start = pos * 8;
  int end = start + 8;
  int i;
  for(i = start; i < end;i++){
    if(datablocks[i] == '\0'){  
      putchar(' ');
    } else {
      putchar(datablocks[i]);
    }
  } 
}

/*print out all history that's saved in primary memory*/
void history()
{
  printf("\nHistory list of the last %d commands:\n", histCount);

  int count = histCount;

  struct metablock *temp = first;
  int bit;

  /*loops to last in list*/
  while(temp->next != NULL){
    temp = temp->next;
  }
  
  /*print out all history starting from tail and going up to head*/
  while(temp != NULL){
    int i;
    printf("%d: ", count);
    for(i = 0;i < temp->bitused ;i++){    /*loops to how many bits we used to save it in history*/
      bit = temp->data[i];
      printDatablock(bit);
    }
    count--;
    printf("\n");
    temp = temp->prev;    /*update temp*/
  }
} 



    
 
  
  

