#ifndef SAFEFORK              /*sikring for at det ikke blir brukt duplikater*/
#define SAFEFORK
#include <sys/types.h>

pid_t safefork(void);

#endif
