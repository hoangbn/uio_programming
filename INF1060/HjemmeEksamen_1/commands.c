#include "shell.h"

extern char *bitmap;
extern char *datablocks;
extern char line[120];
extern char newLine[120];
extern int len;
extern char **path;

char tempLine[120];
char **command;

int lineCount;
int histCount = 0;
int bitFree = 64;
int bitNeeded;

struct metablock {
  int length;
  int bitused;
  int data[15];
  struct metablock *next;
  struct metablock *prev;
};
struct metablock *first = NULL;



void clearDatablock(int pos)
{
  int start = pos * 8;
  bzero(&datablocks[start], 8);
}

void saveDatablock(int b)
{
 /*  char *p = datablocks; */
  int start = b * 8;
  /*   printf("%d\n", start); */
  int end = start + 8;
  
  int i;
  for(i = start; i < end; i++){
    strncpy(&datablocks[i], &newLine[lineCount], 1);
    printf("%s", &datablocks[i]);
    lineCount++;
    len--;
    if(len == 0) break;
  }
}

void saveMetablock(int leng, int *array, int used)
{
  struct metablock *tempBlock;
  struct metablock *curBlock;
  
  tempBlock = malloc(sizeof(struct metablock));
  tempBlock->length = leng;
  tempBlock->bitused = used;
  
  int i;
  for(i = 0; i < 15; i++){
    if(array[i] == -1){
      break;
    } else {
      tempBlock->data[i] = array[i];
      
    }
  }
  
  if(i < 14){
    tempBlock->data[i] = '\0';
  }
  
  if(first == NULL){
    first = tempBlock;
    first->next = NULL;
    first->prev = NULL;
    printf("%d, %d\n",first->length, first->data[0]);
    return;
  }

  curBlock = tempBlock;
  first->prev = curBlock;
  curBlock->next = first;
  first = curBlock;
  first->prev = NULL;
  printf("%d, %d\n",first->length, first->data[0]);
  return;
}


/*returns true if the bit at pos in char (c) is set. */
/*Teacher said it was okay to implement methods from exercise he's given*/ 
int checkBit(char c, int pos)
{
  char mask;
  mask = 1 << (7-pos);
  return c & mask;
}

void clearBit(int byte, int pos, int data)
{
  char mask;
  mask = ~(1 << (7-pos));
  bitmap[byte] = bitmap[byte] & mask;
  clearDatablock(data);
}

void setBit(int byte, int pos)
{
  char mask;
  mask = 1 << (7-pos);
  bitmap[byte] = bitmap[byte] | mask;
  bitFree--;
} 

/* prints a char bit by bit*/
/*from exercise we got in week 37, it's identical*/
/*if it's true then 1 will print, otherwise 0*/
void printBit()
{
  printf("DEBUG - BITMAP:\n\n");
  int i;
  for(i = 0; i < 8;i++){

    if(i == 4) putchar('\n');
    
    int j;
    for(j = 0; j < 8; j++){
      putchar(checkBit(bitmap[i], j) ? '1' : '0');
    }
  }
  putchar('\n');
}


int checkMap()
{
  int i;
  for(i = 0; i < 64;i++){
    if(!checkBit(bitmap[i/8], i%8)){
      setBit(i/8, i%8);
      saveDatablock(i);
    /*   bitNeeded--; */
     /*  if(bitNeeded == 0) break; */
      return i;
    }
  } 
  return 0;
}

void freeHistory()
{
  struct metablock *temp = first;
  
  while(temp != NULL){
    first = temp->next;
  /*   free(temp->data); */
    free(temp);
    temp = first;
  }
  histCount = 0;
}

void freeSpace()
{
  struct metablock *temp = first;
  struct metablock *previous;
  
  while(temp->next != NULL){
    previous = temp;
    temp = temp->next;
  }
  int bits = temp->length / 8;
  
  if((temp->length % 8) > 0) bits++;
  
  int i;
  for(i = 0; i < bits;i++){
    clearBit(temp->data[i] / 8, temp->data[i] % 8, temp->data[i]);
    bitFree++;
  }
  free(temp);
  previous->next = NULL;
}

int rmPart(int i)
{
  if(i > histCount){
    fprintf(stderr, "Please try with a number lower than %d\n", histCount);
    return 0;
  } 
  struct metablock *temp = first;
  struct metablock *previous;

  int a;  
  for(a = 1; a < i;a++){
    temp = temp->next;
  }
  
  int bits = temp->length / 8;
  if((temp->length % 8) > 0) bits++;

  if(temp == first){
    for(a = 0; a < bits; a++){
      clearBit(temp->data[a] / 8, temp->data[a] % 8, temp->data[a]);
      bitFree++;
    }
    first = temp->next;
    temp->next->prev = first;
    free(temp);
    histCount--;
    return 1;
  } else {
    for(a = 0; a < bits; a++){
      clearBit(temp->data[a] / 8, temp->data[a] % 8, temp->data[a]);
      bitFree++;
    }
    previous = temp->prev;
    if(temp->next == NULL){
      previous->next = NULL;
      free(temp);
      histCount--;
      return 1;
    } else {
      previous->next = temp->next;
      temp->next->prev = previous;
      free(temp);
      histCount--;
      return 1;
    }
  }
}


int runStory(int i)
{
  int counts = 0;
  int bit = 0;
  if(i > histCount-1){
    fprintf(stderr, "That command %d isn't saved in buffer yet\n", histCount);
    return 0;
  }

  memset(&line, '\0', 120);
    
  struct metablock *temp = first;

  int a;
  for(a = 1; a < i;a++){
    temp = temp->next;
  }
  
  for(a = 0;a < temp->bitused;a++){
    if(temp->data[a] == '\0'){
      break;
    }
    bit = temp->data[a];
    
    int start = bit * 8;
    int end = start + 8;
    
    int b;
    for(b = start;b < end;b++){
      strcpy(&line[counts], &datablocks[b]);
      counts++;
   
    }
  }
  return 1;
/*   splitAndRun(); */
}


void save()
{
  if(len == 0){
    return;
  }
  len = len - 1;
  lineCount = 0;
  bitNeeded = 0; 
  bitNeeded = len / 8;
  
  if(len % 8 > 0) bitNeeded++;
  
 /*  printf("%d \n", len); */
/*   printf("%d \n", bitNeeded); */
  
  int which = 0;
  int array[15];
  int i;
  
  for(i = 0; i < 15; i++) array[i] = -1;
  
  int save = len;
  while(1){
    if(bitNeeded <= bitFree){
      
      for(i = 0; i < bitNeeded; i++){
	which = checkMap();
	array[i] = which;

      }
  /*     printf("%d, %d\n", save, array[0]); */
      saveMetablock(save, array, bitNeeded);
      histCount++;
    }  
    if(len == 0){
      break;
    } else {
      freeSpace();
      histCount--;
    }
    /*   freeHistory();  */
  } 
}


void printDatablock(int pos)
{
  int start = pos * 8;
  int end = start + 8;
  int i;
  for(i = start; i < end;i++){
    if(datablocks[i] == '\0'){
      putchar(' ');
    } else {
    putchar(datablocks[i]);
    }
  }

}

void history()
{
  printf("\nHistory list of the last %d commands:\n", histCount);

  int count = histCount;

  struct metablock *temp;
  
  temp = first;
  int bit;

  while(temp->next != NULL){
    temp = temp->next;
  }

 while(temp != NULL){
    int i;
    printf("%d: ", count);
    for(i = 0;i < temp->bitused ;i++){     
      bit = temp->data[i];
      printDatablock(bit);
    }
    count--;
    printf("\n");
    temp = temp->prev;
  }
} 



    
 
  
  

