/*
INF1060 oblig 1 oppgave 2
Skrevet av: �yvind Kolbu (oyvink)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void decode(char *);
static void encode(char *, char *);
static void printFile(char *);
static void usage(void);

/* Uses the indexes as bit values for the characters.
 * 0 = 00, 1 = 01, 2 = 10 and 3 = 11 */
const char convert[4] = {' ', ':', '@', '\n'};

int
main(int argc, char *argv[]) {
	char *command, *inputfile;

	/* Require either two or three arguments. */
	if (argc < 3 || argc > 4) {
		usage();
		return 1;
	}
	
	command = argv[1];
	inputfile = argv[2];

	if (command[0] == 'p')
		printFile(inputfile);
	else if (command[0] ==  'e') {
		if (argc != 4) {
			usage();
			return 1;
		} else {
			encode(inputfile, argv[3]);
		}
	} else if (command[0] == 'd')
		decode(inputfile);
	else {
		usage();
		return 1;
	}

	return 0;
}

/* Reverses the encode algoritm. Converts 1 read character into 4 characters.
 * The read input character is on the form 01001110, from where two and two
 * bits are decoded, starting from right. */
static void
decode(char *filename) {
	FILE *infile;
	char inchar;
	unsigned char twobitchar;

	if ((infile = fopen(filename, "r")) == NULL) {
		printf("ERROR: Could not open %s for reading. Aborting.\n", filename);
		exit(1);
	}

	/* Read first char */
	inchar = fgetc(infile);

	/* Make sure the file has contents. */
	if (inchar == EOF)
		printf("File is empty!\n");
	else {

		printf("Decoding file %s ... \n", filename);

		/* Then loop until EOF */
		do {
			for (int i = 0; i < 4; i++) {
				/* Bitshift to the next two bits. */
				twobitchar = inchar << 2*i;
				/* Make the 6 first bits zeroes, such that only the two
				 * rightmost bits can vary and thus create a number between
				 * 0 and 3. */
				twobitchar = twobitchar >> 6;
				/* lookup and print the decoded character! */
				printf("%c", convert[twobitchar]);
			}	
		} while ((inchar = fgetc(infile)) != EOF);
	}

	fclose(infile);
	printf("   ... done!\n");
}


/* encode file into a 2 bit system. Limiting to 4 different characeters 
 * will only require 2 bits for each character, thus we are able to bitshift
 * 4 chars into 1 byte. */
static void
encode(char *input, char *output) {
	FILE *infile, *outfile;
	char inchar;
	/* 8 bits available. Each char will consume two of the bits. */
	char encbyte;

	int chars = 0; /* Number of chars present in the byte. */

	if ((infile = fopen(input, "r")) == NULL) {
		printf("ERROR: Could not open %s for reading. Aborting.\n", input);
		exit(1);
	}

	if ((outfile = fopen(output, "w")) == NULL) {
		printf("ERROR: Could not open %s for writing. Aborting.\n", output);
		exit(1);
	}

	/* Read first char */
	inchar = fgetc(infile);

	/* Make sure the file has contents. */
	if (inchar == EOF)
		printf("File is empty!\n");
	else {

		printf("Encoding file %s ... \n", input);
		encbyte = 0;
		/* Then loop until EOF */
		do {
			for (int i = 0; i < 4; i++) {
				if (convert[i] == inchar) {
					// OR in the i value.
					encbyte = encbyte | i;
					chars++;
				}
			}
			if (chars == 4) {
				/* We have used up all the space in the byte. Write it out
				 * and start over again. */
				fputc(encbyte, outfile);
				encbyte = 0;
				chars = 0;
			} else {
				/* Shift two positions to the left, to make room
				 * for next two bits. */
				encbyte = encbyte << 2;
			}
		} while ((inchar = fgetc(infile)) != EOF);
	}

	/* Remember to write the possibly last byte, even though not full. */
	if (chars > 0)
		fputc(encbyte, outfile);

	fclose(infile);
	fclose(outfile);
	printf("   ... done!\n");

}

/* Prints file which is specified in argument. */
void printFile(char *filename) {
	FILE *file;
	char c;

	if ((file = fopen(filename, "r")) == NULL) {
		printf("ERROR: Could not open %s for reading. Aborting.\n", filename);
		exit(1);
	}

	/* Read first char */
	c = fgetc(file);

	/* Make sure the file has contents. */
	if (c == EOF)
		printf("File is empty!\n");
	else {

		printf("Printing file %s ... \n\n", filename);

		/* Then loop until EOF */
		while (c != EOF) {
			printf("%c", c);
			c = fgetc(file);
		}
	}
}

void
usage(void)
{
	fprintf(stderr,"%s\n\n%s\n\n%s\n%s\n%s\n%s\n%s\n",
	    "Usage: oppgave2 command input_file output_file",
		"where \"command\" is one of the following:",
		"\tp\tprint input_file",
		"\t\t(output_file is ignored if specified)",
		"\te\tencode/compress input_file to output_file",
		"\td\tdecode/uncompress and print input_file",
		"\t\t(output_file is ignored if specified)");
}
