/*
INF1060 oblig 1 oppgave 1
Skrevet av: �yvind Kolbu (oyvink)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Could not find out how the get variable line length input...
#define MAXLINELENGTH 200

static int fileChars(char *);
static void freeList(void);
static void insertLine(char *);
static void printLines(void);
static void printRandom(void);
static void readFile(char *);
static void removeVowels(void);
static void replaceVowels(void);
static void usage(void);

struct list
{
	struct list *next;
	char *line;
};

typedef struct list listitem;

listitem *head = NULL;
listitem *last = NULL;

const char *vowels = "aeiou���";
	
// Number of lines in the list
int numLines = 0;

int
main(int argc, char *argv[]) {
	/* Require 2 arguments. */
	if (argc < 3 || argc > 3) {
		usage();
		exit(1);
	}
	
	readFile(argv[2]);

	/* OK, could read the file, then procede to actually do something useful.
	 * Do Solaris style password matching.. only check if the n first characters
	 * are matching */
	if (strncmp(argv[1], "print", 5) == 0)
		printLines();
	else if (strncmp(argv[1], "random", 6) == 0)
		printRandom();
	else if (strncmp(argv[1], "replace", 6) == 0)
		replaceVowels();
	else if (strncmp(argv[1], "remove", 6) == 0)
		removeVowels();
	else if (strncmp(argv[1], "len", 3) == 0)
		printf("The text is %d characters long.\n", fileChars(argv[2]));
	else {
		printf("Wrong argument: %s\n", argv[1]);
		usage();
		exit(1);
	}
	freeList();	
	return 0;
}

/* Count and return the number of characters in a given file. */
static int
fileChars(char *filename) {
	FILE *file;

	int chars = 0;

	if ((file = fopen(filename, "r")) == NULL) {
		printf("Can not open %s.\n", filename);
		exit(1);
	}

	while (fgetc(file) != EOF)
		chars++;

	fclose(file);

	return chars;

}

/* free list contents and the list  */
static void
freeList(void) {
	listitem *freeme = head;

	while (freeme) {
		head = freeme->next;
		free(freeme->line);
		free(freeme);
		freeme = head;
	}
}

/* Adds the new line at the end of the list. */
static void
insertLine(char *line) {
	listitem *new = malloc(sizeof(listitem));
	new->line = malloc(strlen(line));
	strcpy(new->line, line);

	if (head == NULL)
		head = last = new;
	else {
		last->next = new;
		last = new;
	}

	numLines++;
}

static void
printLines(void) {
	listitem *print = head;

	if (print == NULL)
		printf("No lines to print..\n");
	else {
		printf("Print text:\n");
		while(print) {
			printf("\t%s", print->line);
			print = print->next;
		}
	}
}

/* Prints a random line. Using the remainer of the mod operatior to
 * determine which line to print. */
static void
printRandom(void) {
	// Need to initialize rand(), and seconds since epoch is suitable.
	srand(time(NULL));
	int randomLine = rand() % numLines;
	listitem *print = head;
	
	// Skip until 'print' is the randomline.
	for (int i = 0; i < randomLine; i++)
		print = print->next;

	printf("\nPrint a random line\n");
	printf("\t%s", print->line);
}

static void
readFile(char *filename) {
	FILE *file;

	char line[MAXLINELENGTH];

	if ((file = fopen(filename, "r")) == NULL) {
		printf("Can not open %s.\n", filename);
		exit(1);
	}

	while (fgets(line, MAXLINELENGTH, file) != NULL)
		insertLine(line);
	
	fclose(file);

}

static void
replaceVowels(void) {
	printf("Replace vowels ...\n\n");

	for (size_t i = 0; i < strlen(vowels); i++) {
		listitem *list = head;

		printf("... with vowel '%c' \n", vowels[i]);

		while (list) {
			// Need to preserve the original line.
			char *line = malloc(strlen(list->line) + 1);
			strcpy(line, list->line);

			for (size_t j = 0; j < strlen(line); j++) {
				// Check if current char is a vowel.
				if (strchr(vowels, line[j]))
					line[j] = vowels[i];
			}
			printf("\t%s", line);
			free(line);
			list = list->next;
		}
		printf("\n");
	}
}

/* Removes all vowels and prints out the new string. */
static void
removeVowels(void) {
	listitem *list = head;

	printf("Remove vowels: \n");

	while (list) {
		char *line = malloc(strlen(list->line) + 1);

		// Current position in the line array;
		int linePosition = 0;

		for (size_t i = 0; i < strlen(list->line); i++) {
			// Skip vowels
			if (strchr(vowels,list->line[i]) == NULL)
				line[linePosition++] = list->line[i];
		}

		line[linePosition] = '\0';
		printf("\t%s", line);
		free(line);
		list = list->next;
	}
}

static void
usage(void) {
	fprintf(stderr,"%s\n\n%s\n\n%s\n%s\n%s\n%s\n%s\n",
	    "Usage: oppgave1 command input_file",
		"where \"command\" is one of the following:",
		"\tprint\tprint input_file",
		"\trandom\tprint a random line",
		"\treplace\treplace the vowels with all the other vowels",
		"\tremove\tremove vowels",
		"\tlen\tprint the number of characters in the input file");
}
