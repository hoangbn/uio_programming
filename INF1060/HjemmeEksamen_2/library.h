#ifndef LIBRARY
#define LIBRARY

#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>


#define SIZE_MSG 229            //219 + 4 for int *\/ */
int socket_fd;
struct Metablock *findMsg(int id);
int sends(int id, char *ph, char *from, int n);
void lastFree();
/* int delMessage(int id); */
void shell();
void addMsg(struct Metablock *in);
struct Metablock *findPhone(char *ph);
void delPhone(struct Metablock *del);

struct Metablock
{
  int message_id;    /*4 numbers*/
  char creation_time[18];
  char send_time[18];                //can be empty
  char recv_time[18];                //can be empty
  char to_phone_number[9];
  char from_phone_number[9];
  int size;
  char flags;               /*I  = innbox, O = outbox,  D = draft*/
  char checked;             /*R for read and U for UNREAD */
  char reference[201];           /*I for innbox, O for outbox and D for drafts*/
};

struct Linkedlist
{
  struct Metablock *data;
  struct Linkedlist *next;
};





#endif
