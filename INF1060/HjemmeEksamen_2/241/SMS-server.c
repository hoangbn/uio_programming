#include "library.h"


int status, socket_fd_new, binded, listening, sel, sending, maxfd, numSock;    
char *port_number;

struct addrinfo address_infos;
struct addrinfo *server_info;    //pointer to result
struct sockaddr_storage incoming_ips;           //ipaddress to client
struct timeval times;

socklen_t address_size;
fd_set fdset, temp_set;                      //fdset is a file descrptor list, temp_set a temporary file descriptor list
char msg_buf[201];
int bytesSend = 0, bytesHSend = 0, master[FD_SETSIZE], totalSend = 0, id = 999, count = 0;
extern struct Linkedlist *first;

/*duplicate fdset since there is something wrong with FD-method in school*/
void duplicate()
{
  FD_ZERO(&fdset);
  
  int i;
  for(i = 0;i <= maxfd;i++){
    if(master[i] != -1) FD_SET(master[i], &fdset);
    if(maxfd < master[i]) maxfd = master[i];
  }
}

/*send all messages to client*/
void sendt(int fd)
{
 
  struct Metablock *temp;
  
  while((temp = findPhone(msg_buf)) != NULL){
      write(fd, "Go", 2);
      
      write(fd, temp->creation_time, 18);
      
      write(fd, temp->from_phone_number, 9);
      
      write(fd, temp->to_phone_number, 9);
      
      write(fd, temp->send_time, 18);
     
      write(fd, temp->reference, temp->size);
      delPhone(temp);
      count--;
    }
  write(fd, "No", 2);
}


/*shows stats of server, debug info*/
void stats()
{
  printf("Total Msg sent: %d\n", count);
  printf("Total bytes including header sent: %d\n", bytesHSend);
  printf("Total bytes excluding header sent: %d\n", bytesSend);
}

/*debug info after 3 messages*/
void show(){
  struct Linkedlist *temp = first;
  struct Metablock *out;
  
  while(temp != NULL){
    
    out = temp->data;
    printf("Message ID: %d\n", out->message_id);
    printf("Creation Time: %s\n", out->creation_time);
    printf("Phone number: %s\n", out->from_phone_number);
 
    printf("Message: %s\n", out->reference);
    temp = temp->next;
  }
}


/*if server got ack - Sending then client wants check if there's any mail for client */
int reads(int sd)
{
  bzero(msg_buf, 201);
  read(sd, msg_buf, 8);
  bytesHSend += 8;
  if(strcmp("Sendings", msg_buf) == 0){
    struct Metablock *data = malloc(sizeof(struct Metablock));
    
    bzero(msg_buf, 201);
    
   
    int end = 0;
    
    while(end < 18)  end += read(sd, msg_buf, 18);          //runs till we've readed all bytes
    strcpy(data->creation_time, msg_buf);
    
    bzero(msg_buf, 201);    
    end = 0;
    while(end < 9)  end += read(sd, msg_buf, 9);
    strcpy(data->from_phone_number, msg_buf);
    
    bzero(msg_buf, 201);
    end = 0;
    while(end < 9) end += read(sd, msg_buf, 9);
    strcpy(data->to_phone_number, msg_buf);
    
    end = 0;
    bzero(msg_buf, 201);
    while(end < 18)  end += read(sd, msg_buf, 18);
    strcpy(data->send_time, msg_buf);
    
    end = 0;
    bzero(msg_buf, 201);
  
    read(sd, msg_buf, 201);
    strcpy(data->reference, msg_buf);
    
    data->size = strlen(data->reference);
    data->reference[data->size] = '\0';
    id += 1;
    data->message_id = id; 
    count++;
    bytesSend += (54 + data->size);
    bytesHSend += (54 + data->size);

    addMsg(data);
#ifdef DEBUG
    if(count % 3 == 0) stats();
#endif

  } else {
#ifdef DEBUG
  printf("Phone: %s is connected, and port it's using is %s\n", msg_buf, port_number);
#endif
    sendt(sd);
    return 0;
  } 
    
  return 0;
}

/*if user pushed CTRL-C, meaning to kill process */
void forceQuit(int signal)
{
  struct Metablock *pOut; 
  struct Linkedlist *temp = first;
  printf("***Messages left in server***\n");
  while(temp != NULL){
    pOut = temp->data;
    printf("Message ID: %d\n", pOut->message_id);
    printf("Creation Time: %s\n", pOut->creation_time);
    printf("Phone number: %s\n", pOut->from_phone_number);
    printf("Phone number: %s\n", pOut->to_phone_number);
    printf("Message: %s\n", pOut->reference);
    temp = temp->next;
  }
  lastFree();
  exit(0);
}


/*accept a new connection*/
int newConnection()
{
  int accepted;
  accepted = accept(socket_fd, (struct sockaddr *)&incoming_ips, &address_size);

  return accepted;
}

/*starting server*/
void startServer()
{
  memset(master, -1, sizeof(master));
  memset(&address_infos, 0, sizeof(address_infos));    // make that the struct is casted and empty
  address_infos.ai_family = AF_INET;                  //using only IPv4, it's possibly to use AF_UNSPEC to make it accept IPv6 too
  address_infos.ai_socktype = SOCK_STREAM;            //using TCP sockets
  address_infos.ai_flags = AI_PASSIVE;                //fills in IP automatically(local host) if none specified, used on servers which are supposed to accept any connection from any network address
  
  if((status = getaddrinfo(NULL, port_number, &address_infos, &server_info)) == -1){      //if we specify an ip address it would stand where NULL is
    perror("getaddrinfo error\n"); 
    exit(1);
  }
  
  if((socket_fd = socket(server_info->ai_family, server_info->ai_socktype, server_info->ai_protocol)) == -1){    //In this case, ip-type IPv4, type stream, type TCP, return a socket descriptor for later use
    perror("socket error");
    exit(1);
  }
  
  if((binded = bind(socket_fd, server_info->ai_addr, server_info->ai_addrlen)) == -1){  /*tries to bind with incoming ip if any*/
    perror("bind error");
    exit(1);
  }
  
  freeaddrinfo(server_info);   //free memory from linked list
  
  if((listening = listen(socket_fd, 20)) == -1){
    perror("listen error");
    exit(1);
  }
  
  FD_SET(socket_fd, &fdset);                 //set fd in list
  master[socket_fd] = socket_fd;
  maxfd = socket_fd;                      //first fd is always the highest fd
  numSock = 0;
   
  while(1){
    
    duplicate();               //duplicate original set
    times.tv_sec = 2;         //reset timer
    times.tv_usec = 0;
    
    if((sel = select(maxfd + 1, &fdset, NULL, NULL, &times) == -1)){                    //timeout is 2 sec, can work with many connections at the same time
      perror("Select: ");
      exit(1);
    }
    printf("Running...\n");

    int i, newfd;
    for(i = 0; i <= maxfd;i++){
      if(FD_ISSET(i, &fdset)){
	if(i == socket_fd){                      //if fd is the same as the one we are listening then handle new connection
	  address_size = sizeof(incoming_ips);
	  newfd = newConnection();
	  if(newfd == -1){
	    perror("accept: ");
	  } else {
	    master[newfd] = newfd;              //put in new socket fd
	    if(newfd > maxfd){
	      maxfd = newfd;                    
	    }
	    printf("server: new connection from phone: \n"); 
	  }		  
	} else {   
	  int bytes;	  
	  if((bytes = reads(i)) <= 0){         
	    if(bytes == 0){
	      printf("server: socket %d closed\n", i);
	    } else {
	      perror("recieve: ");	    
	    }
	 
	    close(i);
	    master[i] = -1;
	  }		  
	}    	
      }   
    }
  }
}

int main(int argc, char *argv[])
{
  if(argc < 2 || argc > 2){
    printf("Usage: <port-number>\n");
  }
  port_number = argv[1];
  
  signal(SIGINT, forceQuit);
  
  FD_ZERO(&fdset);
  FD_ZERO(&temp_set);                //clear sets

  /*  int checked = 1; */                          //for setsockopt()
  
  startServer();
 
  return 0;
}
