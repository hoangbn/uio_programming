#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "shell.h"

struct hostent *host;


int main(int argc, char *argv[])
{
 

  if(argc != 4){
    printf("Usage: %s <phonenumber> <hostname> <port>\n", argv[0]);
    exit(1);
  }

  if((host = gethostbyname(argv[2])) == NULL){
    printf("gethostbyname() error\n");
    exit(1);
  }
  
  /*
Telefonnummeret skal brukes som avsender i SMSene som sendes fra telefonen, og er adressen som andre mobiler bruker for � sende SMSer til denne telefonen. Hvordan denne informasjonen lagres i programmet er opp til deg.

Videre skal programmet bruke adresseinformasjonen til SMS-serveren til � koble seg opp og eventuelt hente ned meldinger til telefonen som ligger p� serveren. Hvis man IKKE f�r kontakt med SMS-serveren skal likevel mobiltelefonen fungere ved at man for eksempel kan opprette og slette meldinger p� telefonen, men man kan da alts� ikke sende og motta meldinger (kommunisere med SMS-serveren).

DEBUG: Som debug-informasjon skal programmet skrive ut det innleste mobiltelefonnummeret samt IP-adressen og PORT-nummeret til SMS-serveren. Det skal ogs� skrives ut statusinformasjon om oppkoblingen til serveren var vellykket eller ikke og hvor mange meldinger som lastes ned. 

   */

  shell(argv[1]);

  #ifdef DEBUG
  fprintf(stderr, "Phone Number: %s\n", argv[1]); 
  fprintf(stderr, "Hostname : %s\n", host->h_name);
  fprintf(stderr, "IP Adress: %s\n", inet_ntoa(*((struct in_addr *)host->h_addr)));
  fprintf(stderr, "Port number: %s\n", argv[3]);
  #endif
  

  return 0;
}
  
