
#include <stdio.h>
 
#include <time.h>
       
   
const char *suffix ( int thing )
 
{
  switch ( thing ) {
  case 1: return "st";
  case 2: return "nd";
  case 3: return "rd";
  default: return "th";
  } 
}
  
       
  
int main ( void )
  
{
  char buffer[20];
  time_t now = time ( NULL );
  srand(time(NULL));
  struct tm *date = localtime ( &now );
  
  char b = 'b';
  int c = (int)b;
  printf("||%d||\n", c);
  char d = (char)c;
  printf("||%c||\n", d);

  printf ( "Today is the %d%s day of the %d%s month in the year %d\n",
  
	   date->tm_mday, suffix ( date->tm_mday ),
  
	   date->tm_mon + 1, suffix ( date->tm_mon + 1 ),
  
	   date->tm_year + 1900 );

  printf("minutes: %d, hour: %d\n", date->tm_min, date->tm_hour);

  int a = sprintf(buffer, "%d/%d-%d, %d:%d", date->tm_mday, date->tm_mon + 1, date->tm_year + 1900 , date->tm_hour, date->tm_min);
 
  int i = rand() % 8999 + 1000;
  
  printf("%d\n", i);     
  
  printf("Line: %s\n", buffer);
  
  return 0;
  
}
