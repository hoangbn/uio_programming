#include "library.h"

struct Linkedlist *first = NULL;


struct Metablock *findMsg(int id)
{
  struct Metablock *out;
  struct Linkedlist *temp = first;
  
  while(temp != NULL){
    out = temp->data;
    if(out->message_id == id){
      return out;
    }  
    temp = temp->next;
  }
  return NULL;
}

struct Metablock *findPhone(char *ph)
{
 
  struct Metablock *out;
  struct Linkedlist *temp = first;
  
  while(temp != NULL){
    out = temp->data;

    if(strcmp(out->to_phone_number, ph) == 0){
      return out;
    }  
    temp = temp->next;
  }
 
  return NULL;
}

void delPhone(struct Metablock *del)
{
  struct Metablock *temp;
 
  struct Linkedlist *previous;
  struct Linkedlist *curPointer = first;
 
  while(curPointer != NULL){
    temp = curPointer->data;
    if(temp->message_id == del->message_id){
      break;
    }
    previous = curPointer;
    curPointer = curPointer->next;
  }
 
  free(temp);
  if(curPointer == first){              /*first node*/
    first = curPointer->next;
    free(curPointer);
  } else {
    if(curPointer->next == NULL){       /*last node*/
      previous->next = NULL;
      free(curPointer);
    } else {
      previous->next = curPointer->next;
      free(curPointer);
    }
  } 
}


void addMsg(struct Metablock *inn)
{
  struct Metablock *temp = inn;
  struct Linkedlist *tpoint;
  struct Linkedlist *cur;
  tpoint = malloc(sizeof(struct Linkedlist));
  tpoint->data = temp; 

  if(first == NULL){
    first = tpoint;
    first->next = NULL;
    
    return;
  }
  
  cur = tpoint;
  cur->next = first;
  first = cur;
  return;
}


void lastFree()
{
  struct Linkedlist *temp = first;

  while(temp != NULL){
    first = temp->next;
    free(temp->data);
    free(temp);
    temp = first;
  }
}



