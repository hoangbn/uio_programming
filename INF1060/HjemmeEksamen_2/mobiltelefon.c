#include "library.h"

int status, bytes_recv,connected, socket_fd, port;
struct addrinfo address_infos;
struct addrinfo *server_info/* , *p */;
struct hostent *host;
struct Metablock *data;
char *phone;
char msg_buffer[239];
int connected = 0;
char *ports, *ips;

int connectToServer()
{
  memset(&address_infos, 0, sizeof(address_infos));     //make sure ut's casted and empty

  address_infos.ai_family = AF_INET;                      //Only IPv4 addresses
  address_infos.ai_socktype = SOCK_STREAM;                //TCP connection
  port = htons(atoi(ports));                             

  if((status = getaddrinfo(ips, ports, &address_infos, &server_info)) == -1){
    perror("getaddrinfo error");
    freeaddrinfo(server_info);
    return -1;
  } 

  if((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1){   //hard-coded IPv4 as ip address, type sockstream and lastly 0 so to tell method that we're gonna use the protocol used in sock_stream(TCP)  
    perror("socket error");
    freeaddrinfo(server_info);
    return -1;
  }

  if((connected = connect(socket_fd, server_info->ai_addr, server_info->ai_addrlen)) == -1){  //tries to connect, if return -1 then just continue
    close(socket_fd);
    perror("connect error");
      freeaddrinfo(server_info);
    return -1;
  }
  
  freeaddrinfo(server_info);
  connected = 0;
  return 0;
}


int main(int argc, char *argv[])
{
  if(argc != 4){
    printf("Usage: %s <phonenumber> <hostname> <port>\n", argv[0]);
    exit(1);
  }

  if((host = gethostbyname(argv[2])) == NULL){
    printf("gethostbyname() error\n");
    exit(1);
  }
  ports = malloc(sizeof(argv[3]));
  ips = malloc(sizeof(argv[2]));
  strcpy(ports, argv[3]);
  strcpy(ips, argv[2]);

  #ifdef DEBUG
  fprintf(stderr, "Phone Number: %s\n", argv[1]);
  fprintf(stderr, "Hostname : %s\n", host->h_name);
  fprintf(stderr, "IP Adress: %s\n", inet_ntoa(*((struct in_addr *)host->h_addr)));
  fprintf(stderr, "Port number: %s\n", argv[3]);
  #endif

  int ok = connectToServer();
  if(ok == -1){
    printf("Couldn't connect to server\n");
  }
  
  
 
  phone = argv[1];
  shell();

  
  return 0;
}

