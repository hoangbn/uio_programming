#include "library.h"
 
int status, socket_fd, socket_fd_new, binded, listening, sel, sending, maxfd;    
char *port_number;
struct addrinfo address_infos;
struct addrinfo *server_info;    //pointer to result
struct sockaddr_storage incoming_ips;           //ipaddress to client
/* struct msg message; */
socklen_t address_size;
fd_set fdset, temp_set;                      //fdset is a file descrptor list, temp_set a temporary file descriptor list
char msg_buf[9];
int bytesSend, bytesRecv;

struct socket_list
{
  int sock;
  struct socket_list *next;
};
struct socket_list *first = NULL;



void addSocket(int sock_fd)
{
  printf("added!\n");
  struct socket_list *temp;
  struct socket_list *list;
  temp = malloc(sizeof(struct socket_list));
  temp->sock = sock_fd;
  
  if(first == NULL){ 
    first = temp;
    first->next = NULL;
  } 
  
  list = temp;
  list->next = first;
  first = list;
}

void rmSocket(int sock_fd){
  printf("Remove!");
  
  if(first == NULL) return;
  struct socket_list *list = first;
  struct socket_list *previous;
  
  
  while(list != NULL){
    if(list->sock == sock_fd) break;
    previous = list;
    list = list->next;
  }
  
  if(list == first){
    first = list->next;
    free(list);    
  } else if(list == first) {
    first = list->next;
    free(list);
  } else {
    if(list->next == NULL){
      previous->next = NULL;
      free(list);
    } else {
      previous->next = list->next;
      free(list);
    }  
  }    
}

void new_Set()
{
  FD_ZERO(&fdset);
  FD_SET(socket_fd, &fdset);
  
  struct socket_list *temp = first;
  while(temp != NULL){
    FD_SET(temp->sock, &fdset);
    if(maxfd > temp->sock){
      maxfd = temp->sock;
    }
    temp = temp->next;
  }
}


void forceQuit(int signal)
{
  struct socket_list *temp = first;
  struct socket_list *prev;
  
  while(temp != NULL){
    prev = temp;
    temp = temp->next;
    free(prev);
  }
  
  
  exit(0);
}



int main(int argc, char *argv[])
{
  if(argc < 2 || argc > 2){
    printf("Usage: <port-number>\n");
  }
  port_number = argv[1];
  
  signal(SIGINT, forceQuit);
  
  struct timeval time;

 
  FD_ZERO(&fdset);
  FD_ZERO(&temp_set);                //clear sets

  /*  int checked = 1; */                          //for setsockopt()
  


  memset(&address_infos, 0, sizeof(address_infos));    // make that the struct is casted and empty
  address_infos.ai_family = AF_INET;                  //using only IPv4, it's possibly to use AF_UNSPEC to make it accept IPv6 too
  address_infos.ai_socktype = SOCK_STREAM;            //using TCP sockets
  address_infos.ai_flags = AI_PASSIVE;                //fills in IP automatically(local host) if none specified, used on servers which are supposed to accept any connection from any network address
  
  if((status = getaddrinfo(NULL, port_number, &address_infos, &server_info)) == -1){      //if we specify an ip address it would stand where NULL is
    perror("getaddrinfo error\n"); 
    exit(1);
  }
  
  if((socket_fd = socket(server_info->ai_family, server_info->ai_socktype, server_info->ai_protocol)) == -1){    //In this case, ip-type IPv4, type stream, type TCP, return a socket descriptor for later use
    perror("socket error");
    exit(1);
  }
  
  /*   if(setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &checked, sizeof(int)) == -1){ */
  /*     perror("setsockopt error:"); */
  /*     exit(1); */
  /*   } */
  
  if((binded = bind(socket_fd, server_info->ai_addr, server_info->ai_addrlen)) == -1){
    perror("bind error");
    exit(1);
  }
  
  
  
  freeaddrinfo(server_info);   //free memory from linked list
  
  
  if((listening = listen(socket_fd, 20)) == -1){
    perror("listen error");
    exit(1);
  }
  
  
  addSocket(socket_fd);             //set fd in list
  maxfd = socket_fd;                      //first fd is always the highest fd
  
  
  while(1){
    time.tv_sec = 3;
    time.tv_usec = 0;
    new_Set();
  /*   temp_set = fdset;   */                        //duplicate original set
    if((sel = select(maxfd + 1, &fdset, NULL, NULL, &time) == -1)){
      perror("Select: ");
      exit(1);
    }
    printf("Running...\n");
    int i, newfd;
    for(i = 0; i < maxfd+1;i++){
      if(FD_ISSET(i, &fdset)){
	if(i == socket_fd){                      //if fd is the same as the one we are listening then handle new connection
	  address_size = sizeof(incoming_ips);
	  if((newfd = accept(socket_fd, (struct sockaddr *)&incoming_ips, &address_size)) == -1){
	    perror("accept: ");
	  } else {
	    addSocket(socket_fd);//add to fdset list
	    if(newfd > maxfd){
	      maxfd = newfd; 
	    }
	    printf("server: new connection\n"); 
	  }	
	}
      } else {   
	int bytes;
	if((bytes = recv(i, &msg_buf, 18, 0)) <= 0){         ;
	  if(bytes == 0){
	    printf("server: socket %d closed\n", i);
	  } else {
	    perror("recieve: ");	    
	  }
	  printf("%s\n", msg_buf);
	  close(i);
	  rmSocket(i);
	} else {
	  int j;
	  for(j = 0;j < maxfd+1;j++){
	    if(FD_ISSET(j, &fdset)){
	      if(j != socket_fd && j != i) {
		if((sending = send(j, &msg_buf, 239, 0)) == -1){
		  perror("send: ");
		}
	      }	      	      
	    }	  
	  } 
    	}      	
      }         
    } 
  }
  return 0;
}
