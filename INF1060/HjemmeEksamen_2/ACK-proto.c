#include "library.h"


/*ACK-protokol, it's main task is to tell the server if the client want to receive or send messages*/
void ACK(char what, char *phone, int size)
{

  if(size == 0){
    if(what == 'R'){                       //receive from server
      write(socket_fd, phone, 8);          //datamessage and phone in one 
    } 
    if(what == 'S'){
      write(socket_fd, "Sendings", 8);    //send from client
    }                                     
  }
}

