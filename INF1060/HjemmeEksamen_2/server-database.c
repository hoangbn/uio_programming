#include "library.h"

struct Message
{
  char message[239];
  
  struct Message *next;
}; 


struct Message *first = NULL;

struct Message *findID(char *buf)
{
  struct Message *find = first;
  
  while(find != NULL){
    if(strncmp(buf, find->message, 8) == 0){
      return find;
    }
    find = find->next;
  }
  return NULL;
}
