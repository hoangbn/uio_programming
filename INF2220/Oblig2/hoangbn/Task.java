import java.util.LinkedList;

class Task {
    boolean visit = false;
    int id, time, staff;
    int length;
    String name;
    int eStart, lStart, eFinish, lFinish, slack;
    LinkedList<Integer> innEdges;
    LinkedList<Integer> outEdges;
    int cntPredecessors;
    int cntSuccesors;

    Task(int n, String nm, int t, int man){
	id = n;
	name = nm;
	time = t;
	staff = man;
	innEdges = new LinkedList<Integer>();
	outEdges = new LinkedList<Integer>();
	length = 0;
	eStart = 0;
	lStart = 0;
	eFinish = 0;
	lFinish = 0;
	slack = 0;
	cntSuccesors = 0;
    }
    
    /**
     **returns number of successors
     */
    int getCountOut(){
	return cntSuccesors;
    }
    
    /**
     **Since we know that if a task is called for the first time, then that means
     **it has to be starting, when it's called a second time, then it's finished 
     */
    public int printProcess(){
	if(visit){                                   //second time visiting
	    System.out.printf("Finished: %d\n", id);
	    return -staff;                          //substract the manpower this task needed
	} else {                                    //first time visiting
	    System.out.printf("Starting: %d\n", id);	 
	    visit = true;
	    return staff;                          // adds the manpower this task needed
	}
    }
    /**
     **adds succesors
     */
    void addCount(){
	cntSuccesors++;
    }
    
    /**
     **substract succesors
     */
    void minSCount(){
	if(cntSuccesors != 0)
	    cntSuccesors--;
    }
    
    /**
     **set predecessors
     */
    void setCnt(int cnt){
	cntPredecessors = cnt;
    }   
    
    /**
     **return predecessors
     */
    int getCountInn(){
	return cntPredecessors;
    }
    
    /**
     **substract 1 predecessor
     */
    void minCount(){
	if(cntPredecessors != 0){
	    cntPredecessors--;
	}
    }
    
    /**
     **prints out info about this task
     */
    void printInfo(){
	System.out.printf("ID: %d\nName: %s\nTime Estimate: %d\nManpower: %d\nSlack: %d\nLatest starting time: %d\nTask which depends on this task:\n", id, name, time, staff, slack, lStart);   
	for(int i = 0; i < innEdges.size();i++){
	    System.out.println(innEdges.get(i));
	}
	System.out.println();
    }
}