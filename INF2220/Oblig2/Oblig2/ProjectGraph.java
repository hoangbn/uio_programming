import java.util.ArrayList;
import java.lang.System;
import java.util.Stack;
import java.util.HashMap;

public class ProjectGraph{
    ArrayList<Task> graph;
    ArrayList<Edge> edges;
    int size;
    
    /*constructor*/
    ProjectGraph(int s){
	graph = new ArrayList<Task>();
	size = s;
    }
    
    /**
     **Adds a Task in Graph
     */
    public void addTask(Task t){
	if(graph.contains(t) == false){
	    graph.add(t);
	} else {
	    System.out.print("Something went wrong with adding");
	} 
    }
      
    /**
     **traverse through the list and find all innedges
     **and set this task as an outedge in them(innedges)
     */
    public void sortOutEdges(){
	Task t, sort;
	
	for(int i = 0;i < graph.size();i++){
	    t = graph.get(i);	    
	    for(int a = 0;a < t.getCountInn();a++){
		sort = graph.get(t.innEdges.get(a)-1);   
		sort.addCount();
		sort.outEdges.add(t.id);
		
	    }
	}
    }
    
    
    /**calculates early start and early finish time for each
     **task, and save the sorted tasks in a list
     */
    public void topSort(){
	ArrayList<Task> list = new ArrayList<Task>();
	ArrayList<Task> sorted = new ArrayList<Task>();
	
	Task t, pre;
	int counter = 0;
	int time = 0;

	/*find tasks with no dependencies*/
	for(int i = 0; i < graph.size();i++){
	    t = graph.get(i);
	    if(t.getCountInn() == 0){
		t.eStart = 0;
		t.eFinish = t.eStart + t.time;
		time = t.eFinish;
		list.add(t);
		sorted.add(t);
	
	    }
	}

	while(!list.isEmpty()) {
	    t = list.get(0);
	    list.remove(0);
	 
	    counter++;
	    int which;
	  
	    for(int a = 0;a < t.outEdges.size();a++){
		which = t.outEdges.get(a);                 
		pre = graph.get(which-1);	     //substract 1 to get the corresponding task, since an array start at 0
		pre.minCount();                            //substract 1 predecessor
		if(pre.eStart < t.eFinish){                //update
		    pre.eStart = t.eFinish;
		    pre.eFinish = pre.eStart + pre.time;
		  
		}
		if(pre.eFinish > time){                  //first time in ths task
		    time = pre.eFinish;
		}
		if(pre.getCountInn() == 0){             //when there is no predecessors anymore, save it in list
       	 
		    sorted.add(pre);
		    list.add(pre);
		   
		} 
	    }
	    
	}
	if(counter < graph.size()){
	    System.out.println("Cycle found!");
	    System.exit(1);
	} else {
	    printTasks(sorted, time);
	}
    }
    
  
    /**
     **calculates slack time by going backwards
     **We can find slack by taking last finish
     **substract with time for task to finish
     */
    public void findSlack(){
	ArrayList<Task> list = new ArrayList<Task>();
	ArrayList<Task> sorted = new ArrayList<Task>();
	
	Task t, pre;	
	int time = 0;
	
	/*find those task with no outEdges, last task to process and save them and find the highest earliest time to finish*/
	for(int i = 0; i < graph.size();i++){
	    t = graph.get(i);
	    if(t.getCountOut() == 0){	
		if(time < t.eFinish){
		    time = t.eFinish;	
		}
		list.add(t);	 		    
	    }
	}
	
	/*Since we've found the highest end time, we'll set it to all last task in graph*/
	for(int a = 0; a < list.size();a++){
	    t = list.get(a);
	    t.lFinish = time;
	    t.lStart = t.lFinish - t.time;
	    t.slack = t.lStart - t.eStart;	    //slack
	    sorted.add(t);                         //need it to print out info later
	}
		
	while(!list.isEmpty()) {
	    t = list.get(0);
	    list.remove(0);
	 	 
	    int which;
	  
	    for(int a = 0;a < t.innEdges.size();a++){
		which = t.innEdges.get(a);
		pre = graph.get(which-1);	
		pre.minSCount();
		
		if(pre.lFinish == 0){		          //first time coming to this task
		    pre.lFinish = t.lStart;	
		    pre.lStart = pre.lFinish - pre.time;
		    pre.slack = pre.lStart - pre.eStart;
		} else if(pre.lFinish > t.lStart){    //update if this last finish is higher than it's successors last start
		    pre.lFinish = t.lStart;	
		    pre.lStart = pre.lFinish - pre.time;
		    pre.slack = pre.lStart - pre.eStart;
		}
		if(pre.getCountOut() == 0){		   
		    sorted.add(pre);
		    list.add(pre);	   
		} 
	    } 
	}
	printSlack();     //print out info
    }

    /**
     **prints out all info in order from graph
     */
    public void printSlack(){
	System.out.println("---------------------------------Exercise 3---------------------------------");
	Task t;
	for(int i = 0;i < graph.size();i++){
	    t = graph.get(i);
	    t.printInfo();
	}
    }

    /**prints out all task with time, which task is starting and 
     **which is finished
     **this is probably far over O(n^2)
     **Some thoughts about how to make it better/faster?
     **thought about using a hashmap with list using integer as key
     **and Task have a boolean value to check if visited
     **then using earliest start and latest start as keys, and print out all list from key
     */
    public void printTasks(ArrayList<Task> list, int end){
	Task t;
	boolean check = false;
	boolean check1 = true;
	int man = 0;
	int last = end + 1;
	System.out.println("---------------------------------Exercise 2---------------------------------");
		
	for(int i = 0;i < last;i++){
	    for(int j = 0; j < list.size();j++){
		t = list.get(j);
		if(i == t.eFinish){
		    man = man - t.staff;
		    if(check1){
			System.out.printf("Time: %d\n", i);
			check1 = false;
		    }
		    System.out.printf("Finished: %d\n", t.id);
		 
		    check = true;
		}
		
		if(i == t.eStart){
		    if(check1){
			System.out.printf("Time: %d\n", i);
			check1 = false;
		    }
		    man = man + t.staff;
		    System.out.printf("Starting: %d\n", t.id);
		    check = true;
		} 	
	    }
	    if(check){
		System.out.printf("Current staff: %d\n\n", man); 
		check = false;
		check1 = true;
	    }
	}
    }
}