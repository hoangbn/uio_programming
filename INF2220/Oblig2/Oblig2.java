import java.io.File;
import java.util.Scanner;
import java.io.IOException;


public class Oblig2 {
    public static void main(String[] args){
	Task t;
	ProjectGraph g;
// 	Edge ed;
	if(args.length == 2){
	    try{
		int manpower = Integer.parseInt(args[1]);
		File file = new File(args[0]);
		Scanner input = new Scanner(file);
		String line;
		String[] words;
		g = new ProjectGraph(input.nextInt());
		
		while(input.hasNext()){
		    line = input.nextLine();
		    words = line.split("\\s+");
		    if(words.length == 1){
			continue;
		    }
		    t = new Task(Integer.parseInt(words[0]), words[1],Integer.parseInt(words[2]), Integer.parseInt(words[3]));
 
		    int i = 4;
		    int check = Integer.parseInt(words[i]);
		    if(words.length > 4){		
			int counter = 0;			
			
			while(check != 0){	
			  //   ed = new Edge(t.id, check);
// 			    t.inn.add(ed);
			    t.innEdges.add(check);
			    counter++;
			    i++;
			    check = Integer.parseInt(words[i]);		    
			}
			t.setCnt(counter);
		    } 
		   
		  //   t.innEdges.add(check);
// 		    ed = new Edge(t.id, check);
		 //    t.inn.add(ed);
		    g.addTask(t);
		   
		}
		g.sortOutEdges();
	// 	g.shortestPath(g.graph.get(7));
	// 	Task d;
// 		for(int a = 0;a < g.graph.size();a++){
// 		    d = g.graph.get(a); 
// 		    d.printInfo();
// 		    System.out.println();
		    
// 		    if(d.getCountOut() == 0) continue;
		    
// 		    for(int b = 0; b < d.getCountOut();b++){
// 			System.out.printf("%d ,", d.outEdges.get(b));
// 		    }
// 		    System.out.println();
// 		}
		g.topSort();
		input.close();
	    } catch(IOException e){
		e.printStackTrace();
	    }
	} else{
	    System.out.println("Too many/few arguments from user, make sure to input exactly 2 arguments:\njava prgramname 'argument1'.txt 'Integer argument'");
	    
	}
	
	
	
	
	    
    }
}
    