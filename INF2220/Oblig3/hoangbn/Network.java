import java.util.HashMap;
import java.util.ArrayList;
import java.util.Stack;

public class Network {
    HashMap<Integer, Machine> machines;
    ArrayList<Edge> edges;

    Network(){
	machines = new HashMap<Integer, Machine>();
	edges = new ArrayList<Edge>(); 
    }
    
    public void addEdge(Edge ed){
	edges.add(ed);
    }
    
    public void addMachine(Machine m){
	if(machines.containsKey(m.id)) return;
	machines.put(m.id, m);
    }

    public Machine getMachine(int m){
	return machines.get(m);
    }
    
    public void sortEdges(){
	Machine m, sort;
	Edge e;
	for(int i = 0; i < edges.size();i++){
	    e = edges.get(i);
	    m = machines.get(e.fromID);
	    m.out.add(e.toID);
	    m.addEdge(e);
	}
    }
   
    public void shortestPath(int machineID, String type, String typeData, int data){
// 	Stack<Machine> prev = new Stack<Machine>();
	ArrayList<Machine> sort = new ArrayList<Machine>();
	Stack<Machine> temp = new Stack<Machine>();
	    
	Machine m, source, neighbor;
	Edge e;
	int time = 0, cost = 0, total = 0;
	
	for(int i = 1; i < machines.size();i++){
	    m = machines.get(i);
	    m.value = Integer.MAX_VALUE;     //-1 is for infinity
	    m.previous = null;
	}
	    
	source = machines.get(machineID);
	source.value = 0;                        //value from source to source

	temp.push(source);
	    
	while(!temp.isEmpty()){
	    m = temp.pop();
	   
	    if(m.value == Integer.MAX_VALUE) break;
		
	    if(m.hasData(data, typeData)){
		if(!sort.isEmpty()) sort.clear();  
		while(m.previous != null){
		    sort.add(m);
		    m = m.previous;
		}		
	    } else {
		for(int a = 0; a < m.out.size(); a++){
		    neighbor = machines.get(m.out.get(a));
		    e = whichEdge(m.id, neighbor.id);
		    if(type.equals("C") || type.equals("B")){   //<------------------------
			total = m.value + e.cost;
		    } else if(type.equals("T")){
			total = m.value + e.time;
		    }
			
		    if(total < neighbor.value){
			neighbor.value = total;
			neighbor.previous = m;
			temp.push(neighbor);
		    }
		}
	    }	    
	} 	 	    
	System.out.printf("%d: ", data);
	boolean check = true;
	for(int i = 0;i < sort.size();i++){
	    check = false;
	    m = sort.get(i);
	    if(m.previous != m){
		e = whichEdge(m.previous.id, m.id);
		if(e == null) break;
		time = time + e.time;
		cost = cost + e.cost;
	    }   
	   
	    System.out.printf("%d -> ", m.id);
	}
	if(check && !source.hasData(data, typeData)){
	    System.out.printf("Data not found in network\n");
	} else {	    
	    source.addData(data);
	    System.out.printf("%d (t=%d, c=%d)", machineID, time, cost);
	    System.out.println();	
	}
    }
    
    public Edge whichEdge(int from, int to){
	Edge e;
	for(int i = 0; i < edges.size();i++){
	    if(edges.get(i).fromID == from && edges.get(i).toID == to){
		e = edges.get(i);
		return e;
	    }
	}
	return null;
    }

   

}
