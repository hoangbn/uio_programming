import java.io.File;
import java.util.Scanner;
import java.io.IOException;

public class Oblig3 {
    public static void main(String[] args){
	Network n;
	Machine m;
// 	Data d;
	Edge ed;
	try{
	    File file = new File("config.txt");
	    Scanner input = new Scanner(file);
	    String line;
	    String[] words;
	    n = new Network();

	    while(input.hasNext()){
		line = input.nextLine();
		words = line.split("\\s+");
		
		ed = new Edge(Integer.parseInt(words[0]), Integer.parseInt(words[1]), Integer.parseInt(words[2]), Integer.parseInt(words[3]));
		n.addEdge(ed);
	    }
	    file = new File("data.txt");
	    input = new Scanner(file);
	    while(input.hasNext()){
		line = input.nextLine();
		words = line.split("\\s+");
		m = new Machine(Integer.parseInt(words[0]), Integer.parseInt(words[1]));
		n.addMachine(m);
		
		for(int i = 2; i < words.length;i++){
		    n.getMachine(Integer.parseInt(words[0])).addData(Integer.parseInt(words[i]));
		}
		n.getMachine(Integer.parseInt(words[0])).check = true;	
	    }
	    n.sortEdges();
	    input = new Scanner(System.in);
	    String[] data;
	    System.out.printf("Shell > ");
	    while((line = input.nextLine()) != null) {
		if(line.equals("quit") || line.equals("exit")) System.exit(0);
		if(line.isEmpty()){
		    System.out.printf("Shell > ");
		    continue;
		}
		words = line.split(":");
	
		data = words[3].split(" ");
	
		
		for(int i = 0;i < data.length;i++){
		    n.shortestPath(Integer.parseInt(words[0]), words[1], words[2], Integer.parseInt(data[i]));
		}
		
		
		
		System.out.println();
		System.out.printf("Shell > ");
	    }
	    
	    
	} catch(IOException e){
	    e.printStackTrace();
	}

    }
}