import java.util.ArrayList;
import java.util.LinkedList;

public class Machine {
    int id, maxCap, time, cost, value;
    Machine previous;
    boolean check = false;
    ArrayList<Edge> edge;
    ArrayList<Integer> original;
    LinkedList<Integer> copies;
    ArrayList<Integer> out;
    
    Machine(int a, int size){
	id = a;
	maxCap = size;
	time = Integer.MAX_VALUE;
	cost = Integer.MAX_VALUE;
	out = new ArrayList<Integer>();
	original = new ArrayList<Integer>();
	copies = new LinkedList<Integer>();
	edge = new ArrayList<Edge>();
    }
    
    public void addData(int data){
	if(!check) {  
	    original.add(data);
	} else {
	    if(copies.size() + original.size() == maxCap){
		copies.removeLast();
		copies.addFirst(data);
	    } else {
		copies.addFirst(data);
	    }
	}
    }

    public void addEdge(Edge d){
	edge.add(d);
    }

    public boolean hasData(int data, String which){
	if(which.equals("O")){
	    for(int i = 0;i < original.size();i++){
		if(original.get(i) == data){
		    return true;
		}
	    }
	} else {
	    for(int i = 0;i < original.size();i++){
		if(original.get(i) == data){
		    return true;
		}
	    }
	    for(int a = 0; a < copies.size(); a++){
		if(copies.get(a) == data){
		    return true;
		}
	    }
	}
	return false;
    }

}
