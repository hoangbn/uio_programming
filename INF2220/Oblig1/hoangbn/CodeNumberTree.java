import java.util.NoSuchElementException;
import java.util.HashMap;
import java.util.ArrayList;

/**Didn't finish remove method yet, it couldn't remove the node from list, the 
 **could be because I have to call remove on the node that have that list, but
 **I thought it wasn't neccesary..., well tried many ways, but this remove method
 **tries to remove and adds new node succesfully
 */

public class CodeNumberTree {
    Node n;
    public Node root;
    int counter = 0;    
    public HashMap<Integer, Node> buffer = new HashMap<Integer, Node>();                //buffer
    public ArrayList<CodeNumberNode> listToChange = new ArrayList<CodeNumberNode>();
    
    public boolean add(CodeNumberNode cnn){	
	n = new Node(cnn.cCode);
	if(cnn == null){
	    throw new NullPointerException();
	}

	
	if(root == null){	   
	    root = n;
	    counter++;
	    return true;
	} else {
	    Node temp = root;
	
	    while(temp != null){                                
		if(temp.cCode == cnn.cCode){	             //make a new tree in the corresponding node
		    return temp.add(cnn);
		} else if(temp.cCode > cnn.cCode){           //usual algorithm for BST insertion
		    if(temp.left == null){
			n.parent = temp;
			temp.left = n;
			n.add(cnn);
			counter++;
			return true;
		    } else {
			temp = temp.left;
		    }
		} else if(temp.cCode < cnn.cCode){
		    if(temp.right == null){
			n.parent = temp;
			temp.right = n;	
			n.add(cnn);
			counter++;	
			return true;
		    } else {
			temp = temp.right;
		    }
		}			
	    }

	}
	return false;
    }

   
    /**
     **Traverse the Country code tree in preorder while traversing phone tree in inorder
     **
     **/
    public void traversePreorder(Node n){
	if(n == null){
	    throw new NullPointerException();
	}
	System.out.printf("Country Code:%d  has these phone numbers in tree: \n", n.cCode);
	if(n.root == null){
	    return;
	}
	n.traverseInorder(n.root);                //begin traversing phone tree
	System.out.printf("\n\n");
	if(n.left != null){
	    traversePreorder(n.left);
	}
	if(n.right != null){
	    traversePreorder(n.right);
	}		
    }
    
    /**
     **traverse the tree and add all country codes node in buffer
     */
    public void fillHash(Node cnn, int code){
	if(cnn == null){
	    throw new NullPointerException();
	}
	if(cnn.left != null){
	    fillHash(cnn.left, code);
	}
	buffer.put(cnn.cCode, cnn);
	if(cnn.right != null){
	    fillHash(cnn.right, code);
	}
    }
    
    /**
     **Get 2 string from user-input and search from buffer with code as key
     **
     */
    public void findThenSend(String code, String phone){
	
	int size, count = 0; 
	int country = Integer.parseInt(code);                       //convert string to int
	int newCountry = Integer.parseInt(new String(code + phone)); // same
	
	fillHash(root, country);    /* fills treeNode hashmap */
// 	System.out.println(newCountry);
	Node temp = buffer.get(country);
	
	makeList(temp.root, phone);                      //call method to search and save all corresponding prefix in buffer
	size = listToChange.size();
	for(int i = 0; i < size; i++){
	    removeAndAdd(listToChange.get(i), newCountry);    //get all node in buffer and tries to remove and add a new node
	   
	}
	
	
    }     
    
    /**
     **Tries to remove the node directly from CodeNumberTree didn't work out so well.
     **it did create a new node with a new country code, but could delete the old node
     */
    public void removeAndAdd(CodeNumberNode cnn, int newCode){
	CodeNumberNode temp, tempParent, newNode;

	
	if(cnn == null){
	    throw new NullPointerException();
	}

	if(cnn.left == null && cnn.right == null){  //If the node has no children
	    if(cnn.first){
	 	newNode = new CodeNumberNode(Integer.toString(newCode), cnn.pNumber) ;	
		cnn = null;
		
	 	add(newNode);
	    } else {
	 	newNode = new CodeNumberNode(Integer.toString(newCode), cnn.pNumber) ;	
		System.out.println("her");
 
		cnn.parent = null;
		cnn = null;
	 	add(newNode);
	    }
	    
	} else if(cnn.left == null && cnn.right != null){ //Node have only a right child
	    if(cnn.first){                             //if bn is the root
	 	newNode = new CodeNumberNode(Integer.toString(newCode), cnn.pNumber) ;	
	
		temp = cnn.right;
		cnn = temp;
		temp = null;
	 	add(newNode);
	    } else {
	 	newNode = new CodeNumberNode(Integer.toString(newCode), cnn.pNumber) ;	
	
		temp = cnn.right;
		tempParent = cnn.parent;
		tempParent.right = temp;
		temp.parent = tempParent;   
		cnn = null;		    
	 	add(newNode);
	    }
	} else if(cnn.left != null && cnn.right == null) { //Node have only a left child
	    if(cnn.first){                              //Node is a root
	
	 	newNode = new CodeNumberNode(Integer.toString(newCode), cnn.pNumber) ;	
		temp = cnn.left;
		cnn = temp;
		temp = null;
	 	add(newNode);
	    } else {
	
	 	newNode = new CodeNumberNode(Integer.toString(newCode), cnn.pNumber) ;	
		temp = cnn.left;
		tempParent = cnn.parent;
		tempParent.left = temp;
		temp.parent = tempParent;
		cnn = null;
	 	add(newNode);
	    }
	} else {   //Node have both left and right children, find the node with smallest value in it's right subtree and let it take over this node place and delete the node, 
	    
	    
	    temp = findMin(cnn.right);                       //it doesn't matter if cnn is root here since we are not remove the root node, just replacing all it's data with last node, and remove last 
	    newNode = new CodeNumberNode(Integer.toString(newCode), cnn.pNumber) ;
	    cnn.cCode = temp.cCode;
	    cnn.pNumber = temp.pNumber;		    
	    temp.parent = null;
	    temp = null;
	    add(newNode);
	 }
    }

    /**
     **Finds the node in the right subtree with smallest value and return it
     */
    public CodeNumberNode findMin(CodeNumberNode cnn) {
	if(cnn == null){
	    return null;
	}
	
	if(cnn.left == null){
	    return cnn;
	} else {
	    return findMin(cnn.left);
	}
	
	
    }

    /**
     **makes a list with all nodes with the same prefix in phone
     */
    public void makeList(CodeNumberNode nn, String phone){
	if(nn == null){
	    return;
	}
	if(nn.left != null){
	    makeList(nn.left, phone);
	}
	if(nn.pNumber.startsWith(phone)){
	    listToChange.add(nn);
	}
	if(nn.right != null){
	    makeList(nn.right, phone);
	}
    }

    
    
    /**
     **another attempt with only CodeNumberNode without Node, only one node was used here
     **then later i tried with two nodes to see if remove was easier to implement.. it was actually
     **but it didn't remove even though I change the node to null.
     */
  //   public void removeThenAdd(CodeNumberNode cnn, int newCode){
// 	CodeNumberNode temp, tempParent;

// 	if(cnn.treeNode){
// 	    if(cnn.root == null){
// 		if(cnn.left == null && cnn.right == null){
// 		    if(cnn.parent == null){
// 			cnn = null;
// 			System.out.println("List is empty");
// 		    } else {
// 			cnn.parent = null;
// 			cnn = null;
// 		    }
// 		} else if(cnn.left == null && cnn.right != null){
// 		    if(cnn == root){
// 			temp = cnn.right;
// 			root = temp;
// 			cnn = null;
// 		    } else {
// 			temp = cnn.right;
// 			tempParent = cnn.parent;
// 			tempParent.right = temp;
// 			temp.parent = tempParent;
// 			cnn = null;
// 		    }
// 		} else if(cnn.left != null && cnn.right == null){
// 		    if(cnn == root){
// 			temp = cnn.left;
// 			root = temp;
// 			cnn = null;
// 		    } else {
// 			temp = cnn.left;
// 			tempParent = cnn.parent;
// 			tempParent.left = temp;
// 		    temp.parent = tempParent;
// 		    cnn = null;
// 		    }
		    
		    
// 		} else {
// 		    temp = findMin(cnn.right);
// 		    cnn.cCode = temp.cCode;
// 		    cnn.pNumber = temp.pNumber;
// 		    if(temp.root != null){
// 			cnn.root = temp.root;
// 			temp.root.rootParent = cnn;
// 		    }
// 		    temp.parent = null;
// 		    temp = null;
// 		    return true;
// 		}
// 	    } else {
// 		tempParent = cnn.root;
// 		cnn.cCode = tempParent.cCode;
// 		cnn.pNumber = tempParent.pNumber;
// 		cnn.removeAndAdd(cnn, newCode);
		
// 	    }
	    

// 	}

//     }
    
    
//     public CodeNumberNode findMin(CodeNumberNode cnn){
// 	if(cnn.left == null){
// 	    return cnn;
// 	} else {
// 	    return findMin(cnn.left);
// 	}
//     }
    
}