import java.util.NoSuchElementException;
import java.util.HashMap;
import java.util.Arrays;

public class CodeNumberTree {
  
    public CodeNumberNode root;
    int counter = 0;    
    public HashMap<String, CodeNumberNode> buffer = new HashMap<String, CodeNumberNode>(); 

    public boolean add(CodeNumberNode cnn){	
	if(cnn == null){
	    throw new NullPointerException();
	}
	
	if(root == null){
	    root = cnn;
	    counter++;
	    return true;
	} else {
	    CodeNumberNode temp = root;
	
	    while(temp != null){
		if(temp.cCode == cnn.cCode){	    
		    return temp.add(cnn);
		} else if(temp.cCode > cnn.cCode){
		    if(temp.left == null){
			cnn.parent = temp;
			temp.left = cnn;
			cnn.treeNode = true;
			counter++;
		
			return true;
		    } else {
			temp = temp.left;
		    }
		} else if(temp.cCode < cnn.cCode){
		    if(temp.right == null){
			cnn.parent = temp;
			temp.right = cnn;
			cnn.treeNode = true;
			counter++;
		
			return true;
		    } else {
			temp = temp.right;
		    }
		}			
	    }

	}
	return false;
    }

    public int size(){
	return counter;
    }
    
    public void traversePreorder(CodeNumberNode cnn){
	if(cnn == null){
	    throw new NullPointerException();
	}
	System.out.printf("Country Code:%d  has these phone numbers in tree: \n", cnn.cCode);
	if(cnn.root == null){
	    return;
	}
	cnn.traverseInorder(cnn.root);
	System.out.printf("\n\n");
	if(cnn.left != null){
	    traversePreorder(cnn.left);
	}
	if(cnn.right != null){
	    traversePreorder(cnn.right);
	}		
    }
    
    public void fillHash(CodeNumberNode cnn, int code){
	if(cnn == null){
	    throw new NullPointerException();
	}
	if(cnn.left != null){
	    fillHash(cnn.left, code);
	}
	if(cnn.cCode == code){
	    buffer.put(cnn.pNumber, cnn);
	}
	if(cnn.right != null){
	    fillHash(cnn.right, code);
	}
    }
    
    public void findThenSend(String code, String phone){
	CodeNumberNode temp; int size, count = 0; CodeNumberNode[] toSend;
	int country = Integer.parseInt(code);
	int newCountry = Integer.parseInt(new String(code + phone));
	
	fillHash(root, country);    /* fills treeNode hashmap */
	size = buffer.size();
	toSend = new CodeNumberNode[size];
	
	for(String key: buffer.keySet()){
	    if(key.startsWith(phone)){
		toSend[count] = buffer.get(key);
		count++;
	    }	
	}
	
// 	for(int i = 0; i < count; i++){
// 	    removeThenAdd(toSend[i], newCountry);
// 	}

    } 
    
     
    
  //   public void removeThenAdd(CodeNumberNode cnn, int newCode){
// 	CodeNumberNode temp, tempParent;

// 	if(cnn.treeNode){
// 	    if(cnn.root == null){
// 		if(cnn.left == null && cnn.right == null){
// 		    if(cnn.parent == null){
// 			cnn = null;
// 			System.out.println("List is empty");
// 		    } else {
// 			cnn.parent = null;
// 			cnn = null;
// 		    }
// 		} else if(cnn.left == null && cnn.right != null){
// 		    if(cnn == root){
// 			temp = cnn.right;
// 			root = temp;
// 			cnn = null;
// 		    } else {
// 			temp = cnn.right;
// 			tempParent = cnn.parent;
// 			tempParent.right = temp;
// 			temp.parent = tempParent;
// 			cnn = null;
// 		    }
// 		} else if(cnn.left != null && cnn.right == null){
// 		    if(cnn == root){
// 			temp = cnn.left;
// 			root = temp;
// 			cnn = null;
// 		    } else {
// 			temp = cnn.left;
// 			tempParent = cnn.parent;
// 			tempParent.left = temp;
// 		    temp.parent = tempParent;
// 		    cnn = null;
// 		    }
		    
		    
// 		} else {
// 		    temp = findMin(cnn.right);
// 		    cnn.cCode = temp.cCode;
// 		    cnn.pNumber = temp.pNumber;
// 		    if(temp.root != null){
// 			cnn.root = temp.root;
// 			temp.root.rootParent = cnn;
// 		    }
// 		    temp.parent = null;
// 		    temp = null;
// 		    return true;
// 		}
// 	    } else {
// 		tempParent = cnn.root;
// 		cnn.cCode = tempParent.cCode;
// 		cnn.pNumber = tempParent.pNumber;
// 		cnn.removeAndAdd(cnn, newCode);
		
// 	    }
	    

// 	}

//     }
    
    
//     public CodeNumberNode findMin(CodeNumberNode cnn){
// 	if(cnn.left == null){
// 	    return cnn;
// 	} else {
// 	    return findMin(cnn.left);
// 	}
//     }
    
}