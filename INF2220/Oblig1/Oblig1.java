import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Oblig1 {
    public static void main(String[] args) {
	BinaryNode bn;
	CodeNumberNode cnn;
	CodeNumberTree cnt = new CodeNumberTree();
	Node n;
	BinarySearchTree bst = new BinarySearchTree();
	File file = new File(System.getProperty("user.dir") + "//contact.txt");
	int i = -1;
	Scanner input = new Scanner(System.in);
	
	try {
	    FileInputStream a = new FileInputStream(file);	    
	    BufferedInputStream b = new BufferedInputStream(a);
	    BufferedReader c  = new BufferedReader(new InputStreamReader(b));
	    
	    //kj�rer for alltid helt til den finner 3 tabs + enter. 
	  
	    String s;	  
	    while((s = c.readLine().toLowerCase()) != null) {      //reads line by line from file and converted to lowercase
		String[] ord = s.split("\\s+");		
		if(ord.length < 3){
		    break;
		}

		bn = new BinaryNode(ord[0], ord[1], ord[2], ord[3]);
		bst.add(bn);                                              /*make BinarySearchTree*/
		cnn = new CodeNumberNode(ord[2], ord[3]);
		cnt.add(cnn);                                             /*make countrycode tree*/
		
	    }
	  
	

//  	    cnt.traversePreorder(cnt.root);
	
	    while(i == -1){                            /*Makes it loop till it gets a -1*/
		bst.common.clear();                    /*updates it if other methods have been used... like remove*/
		bst.sortNames(bst.root);               /*make hashmap with all unique names*/
	
		System.out.printf("Use the following commands: \n   unique                             -prints out the number of unique last names\n   common                             -find the most common last name and list all the first name\n   nonunique                          -print all non-unique full name with different country code\n   prefix input_here                  -print all names with phone number which starts  prefix\n   change fullname_here changeto      -change a person last name\n   quit                               -terminate process\n");
		System.out.printf("extra Commands for Country Code and Phone tree: \n   traverse                           -Traverse a tree in preorder and inner tree inorder, prints out data\n   newcountry countrycode_here prefix_here          -Make a new country from a city with all phone with same prefix\n");
		System.out.print("Please input your command: ");
		s = input.next();
	
		if(s.equals("unique")){
		    System.out.println(bst.common.size());
		} else if(s.equals("common")){
		    bst.printCommon();		    
		} else if(s.equals("nonunique")){
		    bst.printNonUnique();
		}
		else if(s.equals("prefix")){
		    bst.printPrefix(input.next());
		}
		else if(s.equals("change")){
    		    bst.remove(input.next(), input.next(), input.next());
		    
		} else if(s.equals("quit")){
		    i = 0;
		} else if(s.equals("traverse")){
		    cnt.traversePreorder(cnt.root);
		} else if(s.equals("newcountry")){
		    cnt.findThenSend(input.next(), input.next());
		}
		System.out.println();
	    }
	    
	    // release/close unnecessary memory-usage
	    a.close();
	    b.close();
	    c.close();

	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

       
    }
}