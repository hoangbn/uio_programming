package inf1010.assignment;

import java.util.Iterator;
import inf1010.lib.two.IfiCollection;
import java.util.NoSuchElementException;

public class SinglyLinkedList<E extends Comparable<E>> implements IfiCollection<E> {
    
    public Node first;
    int counter = 0;
  
    public boolean add(E e) {
	if(e == null){
	    throw new NullPointerException();
	}
	if(contains(e)) {
	    return false;
	}	
	
	Node n = new Node(e); 
	Node temp = first; 
       
	if(first == null) { //Spesielltilfelle
	    //  n.object = e;
	    first = n;
	    counter++;
	    return true;
	} else if(e.compareTo(first.object) < 0) {//Spesialltilfelle hvis input er mindre enn den f�rste objektet
	    //   n.object = e;
	    n.next = first;
	    first = n;
	    counter++;
	    return true;
	} else {
	    Node previous = temp;  // bruker det for � klare � sette objektet 'mellom' to objekter som fins fra f�r av
	  	
	    while(previous.next != null){
		if(e.compareTo(previous.next.object) < 0){
		    //    n.object = e;
		    Node temp2 = previous.next;
		    previous.next = n;
		    n.next = temp2;	
		    counter++;
		    return true;	   
		}
		previous = previous.next;
	    } 	    
	    //  n.object = e;
	    previous.next = n;
	    counter++;
	    return true;	 	       	    
	}
    }
    
    //sjekker om listen inneholder objektet, kunne bruke get-metoden for � sjekke om personen fins i listen istedenfor
    public boolean contains(E e) {
	if(get(e) == null){
	    return false;
	} else {
	    return true;
	}
    }
    //henter objektet fra listen
    public E get(E e) {
	for(Node n = first; n!= null; n = n.next) {  
	    if(n.object.compareTo(e) == 0) {
	
		return n.object;
	    }	  
	}

	return null;
    }
    //fjerner objektet fra listen
    public boolean remove(E e) {
	if(e == null) {
	    throw new NullPointerException();
	}
       	
	Node temp = first;
	
	while(temp != null) {
	    if(first.object.compareTo(e) == 0){  //Spesialtilfelle hvis det fins bare et object i listen
		if(first.next == null) {
		    first = null;
		    counter--;
		    return true;
		} else {                    // Hvis det er flere objekter i listen vil den f�rste sin neste bli den f�rste objektet 
		    first = first.next;
		    counter--;
		    return true;
		}
	    } else if(temp.next.object.compareTo(e) == 0){ // Siden den f�rste if testen har sjekket om den f�rste objektet er den som skal fjernes
		temp.next = temp.next.next;
		counter--;
		return true;
	    }
	    temp = temp.next;
	}	
	return false;
    }
    
    //returnerer st�rrelsen p� listen.
    public int size() {
	/*Node n = first;
	int sum = 0;
	while(n != null) {
	    sum++;
	    n = n.next;
	}
	return sum;	*/
	return counter;
    }
    
    //sjekker om listen er tom
    public boolean isEmpty() {
	if(first == null) {
	    return true;
	} else {
	    return false;
	}
    }
    
    //  fjerner alle objeckter i listen
    public void clear() {
	first = null;
	counter = 0;
    }
    
    // setter in listen over til en array, kunne brukt iterator til � l�se problemet, men lettere p� denne m�ten og bra m�te � l�se p� f�r man gjort Iterator.
    public E[] toArray(E[] a) {
	if(a == null) {
	    throw new NullPointerException();
	}	
	if(a.length < size()){
	    return null;
	} else {	
	    Node temp = first;
	    int i = 0;
	    
	    while(temp != null) {
		a[i++] = temp.object; 
		temp = temp.next;
	    }
	    if(a.length > size()) {
		a[size()] = null;
	    }
	}
	return a;
	
    }
    
    public void toList(E e) {
	Node n = new Node(e);
	n.next = first;
	first = n;
    }
    
    public Iterator<E> iterator() {
	return new NewIterator();
    }
    // Sender den f�rste objektet i listen, og this refererer til DENNE listen, siden vi lager mange lister sender vi bare den det gjelder for
    
    public class Node {
	public Node next;
	public E object;
    
	// forandret p� new Node<E>() -> new Node<E>(e) og slapp � skrive n.objekt. 
 	Node(E e) {       
	    this.object = e;
	    this.next = null;
	}
    }
    
    
    public class NewIterator implements Iterator<E> {
	Node currentNode, nextNode;
	boolean calledNext;

	NewIterator() {
	    nextNode = first;
	}

	// Siden det er bare to alternativer, false eller true, enten eller, sjekker om det fins neste objekt.
	public boolean hasNext() {
	    if(nextNode == null) {
		return false;
	    } else {
		return true;
	    }
	}

	// returnerer objektet samt oppdatere nextNode slik at vi kan sjekke om det fins en neste objekt.
	public E next() {
	    if(!hasNext()) {
		throw new NoSuchElementException();
	    } else {
		currentNode = nextNode;
		nextNode = nextNode.next;
		calledNext = true;
		return currentNode.object;
	    }
	}
	// peker bare til den neste sin neste objekt, og reduserer st�rrelsen
	public void remove() {
	    if(currentNode == null) {
		throw new IllegalStateException();
	    } 
	    if (calledNext) {
		if (nextNode != null) {
		    currentNode.next = nextNode.next;
		}
		calledNext = false;
		counter--;
	    }
	    
	}
    }
}