package inf1010.assignment;
import java.util.Iterator;

public interface Creature extends Comparable<Creature>, Iterable<Creature> {
    /**
     *brukt e f�rst void, men er bedre � bruke boolean slik at man vet fra shellet om det ble kj�rt eller ikke
     */
   public String getBirth();
   public boolean setBirth(String a);
   public String getPhone();
   public boolean setPhone(String b);
   public String getLocation();
   public boolean setLocation(String c);
   public String getName();
   public String getHomeplanet();
   public boolean addFriends(Creature a);
   public boolean rmFriends(Creature a);
   public boolean isFriend(Creature a);
   public Iterator<Creature> iterator();
    
   // public int comepareTo(Creature a);
	
   
}
