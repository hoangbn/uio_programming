package inf1010.assignment;

import java.util.Iterator;
import inf1010.lib.two.IfiCollection;
import java.util.NoSuchElementException;
import java.util.Stack;


public class BinarySearchTree<E extends Comparable<E>> implements IfiCollection<E> {
    
    public Node root, first;
    int counter = 0;
    
    /**
     *adder objekter inn, objekter som er mindre enn roten g�r til venstre og
     * st�rre p� h�yre siden
     * compareTo(), er innebygd i javabiblioteket
     *@return true if added
     */
    public boolean add(E e) {
	Node n = new Node(e);

	if(e == null) {
	    throw new NullPointerException();
	}
 	if(contains(e)){
	    return false;
	}
	
	if(root == null) {
	    root = n;
	    counter++;
	    return true;
	} else {
	    Node temp = root;
	
	    while(temp != null) {
		if(e.compareTo(temp.object) < 0) {
		    if(temp.left == null) {
			temp.left = n;
			counter++;
			return true;
		    } else {
			temp = temp.left;
		    }
		}else if(e.compareTo(temp.object) > 0) {
		    if(temp.right == null) {
			temp.right = n;
			counter++;
			return true;
		    } else {
			temp = temp.right;
		    }
		}
	    }
	}
	return false;
    }
    
    /**
     *istedenfor � skrive metoden 2 ganger, bruker jeg get-metoden for � sjekke om treet har objektet far f�r av
     *@return true if contains
     */
    public boolean contains(E e) {
	if(get(e) == null) {	    	 	    
	    return false;
	} else {
	    return true;
	}
    }
    
    /**
     *Ikke pensum, siden det er mye mer avansert � fjerne objekter fra et tre
     */
    public boolean remove(E e) {
	throw new UnsupportedOperationException();
    }
    /**
     *@return counter, forteller hvor mange objekter det fins i treet
     */
    public int size() {
	return counter;
    }
    /**
     *@return true hvis listen er tomt
     */
    public boolean isEmpty() {
	if(root == null){
	    return true;
	} else {
	    return false;
	}
    }
    /**
     * fjerner listen
     */
    public void clear() {
	root = null;
	counter = 0;
    }
    /**
     *@return objektet som vi leter etter, objektet som returneres m� tas fra listen og ikke objektet som vi brukte som input
     */
    public E get(E e) {
	if(e == null) {
	    throw new NullPointerException();
	}
	Node temp = root;
	
	while(temp != null) {
	    if(e.compareTo(temp.object) == 0) {
		return temp.object;
	    } else if(e.compareTo(temp.object) < 0) {
		temp = temp.left;
	    } else {
		temp = temp.right;
	    }   
	}
	return null;
    }
    /**
     *Bruker iterator er den letteste, siden treet blir konvertet til en stack(som er en liste), og derfra kan man p� en m�te lage metoden basert p� SinglyLinkedList
     *iterator()-metoden kj�res, blir returnert stacken, o it er pekeren til det
     */
    public E[] toArray(E[] a) {
	if(a == null) {
	    throw new NullPointerException();
	}
	if(a.length < size()) {
	    return null;
	} else {
	    Iterator<E> it = iterator();  
	    int i = 0;
	    
	    while(it.hasNext()){
		E e = it.next();
		a[i++] = e;
	    }
	    if(a.length > size()) {
		a[size()] = null;
	    }
	}
	return a;
	
    }
    
    /**
     *@return iterator, s� man kan bruke de hjelpe-metodene
     */
    public Iterator<E> iterator() {
	return new BinaryIterator();
    }
   
    public class Node {
	public Node left, right;
	public E object;
	
	Node(E e) {
	    this.object = e;
	    this.left = null;
	    this.right = null;	
	}
    }
  
    /**
     *En klasse som inneholde metoder, og Stack er hentet fra java biblioteket
     *konstrukt�ren kj�res med en gang, og lager listen(stacken)
     */
    public class BinaryIterator implements Iterator<E> {
	
	Stack<E> stack = new Stack<E>();
	
	BinaryIterator() {
	    setStack(root);
	}
	
	/**
	 * @inheritdoc
	 */
	public boolean hasNext() {
	    return !stack.empty();
	    /*	    if(stack.empty()) {
		return true;
	    }else {
		return false;
		}*/
	}
	/**
	 * @inheritdoc
	 */
	public E next() {
	    if(!hasNext()){
		throw new NoSuchElementException();
	    }
	    return stack.pop();
	}
	

	/** Ikke n�dvendig � implementere, Stack har metoder for � ta seg av det
	 *
	 */	
	public void remove() {
	    throw new UnsupportedOperationException();
	}
	/**
	 *lager listen av treet, den starter ved � sjekke venstre noden helt til den treffer null, og da er noden vi er p� den minste objekter, for � si det kort er dette nesten som en l�kke, litt vanskelig � beskrive det med ord.
	 *
	 */
	void setStack(Node n){
	    if(n == null){ //Spesialtilfelle hvis roten er null
		return;
	    }
	    if(n.right != null){
		setStack(n.right);
	    }
	    stack.push(n.object);
	    if(n.left != null) {
		setStack(n.left);
	    }
	}
    }
}