package inf1010.assignment;

import java.util.Iterator;
import inf1010.lib.two.IfiCollection;
import java.util.NoSuchElementException;
import java.util.Stack;


public class BinarySearchTree<E extends Comparable<E>> implements IfiCollection<E> {
    
    public Node root, first;
    int counter = 0;
    
    /**
     *adder objekter inn, objekter som er mindre enn roten g�r til venstre og
     * st�rre p� h�yre siden
     * compareTo(), er innebygd i javabiblioteket
     *@return true if added
     */
    public boolean add(E e) {
	Node n = new Node(e);

	if(e == null) {
	    throw new NullPointerException();
	}
 	if(contains(e)){
	    return false;
	}
	
	if(root == null) {
	    root = n;
	    counter++;
	    return true;
	} else {
	    Node temp = root;
	
	    while(temp != null) {
		if(e.compareTo(temp.object) < 0) {
		    if(temp.left == null) {
			temp.left = n;
			counter++;
			return true;
		    } else {
			temp = temp.left;
		    }
		}else if(e.compareTo(temp.object) > 0) {
		    if(temp.right == null) {
			temp.right = n;
			counter++;
			return true;
		    } else {
			temp = temp.right;
		    }
		}
	    }
	}
	return false;
    }
    
    /**
     *istedenfor � skrive metoden 2 ganger, bruker jeg get-metoden for � sjekke om treet har objektet far f�r av
     *@return true if contains
     */
    public boolean contains(E e) {
	if(get(e) == null) {	    	 	    
	    return false;
	} else {
	    return true;
	}
    }
    
    /**
     *Ikke pensum, siden det er mye mer avansert � fjerne objekter fra et tre
     */
    public boolean remove(E e) {
	throw new UnsupportedOperationException();
    }
    /**
     *@return counter, forteller hvor mange objekter det fins i treet
     */
    public int size() {
	return counter;
    }
    /**
     *@return true hvis listen er tomt
     */
    public boolean isEmpty() {
	if(root == null){
	    return true;
	} else {
	    return false;
	}
    }
    /**
     * fjerner listen
     */
    public void clear() {
	root = null;
	counter = 0;
    }
    /**
     *@return objektet som vi leter etter, objektet som returneres m� tas fra listen og ikke objektet som vi brukte som input
     */
    public E get(E e) {
	if(e == null) {
	    throw new NullPointerException();
	}
	Node temp = root;
	
	while(temp != null) {
	    if(e.compareTo(temp.object) == 0) {
		return temp.object;
	    } else if(e.compareTo(temp.object) < 0) {
		temp = temp.left;
	    } else {
		temp = temp.right;
	    }   
	}
	return null;
    }
    /**
     *Bruker iterator er den letteste, siden treet blir konvertet til en stack(som er en liste), og derfra kan man p� en m�te lage metoden basert p� SinglyLinkedList
     *iterator()-metoden kj�res, blir returnert stacken, o it er pekeren til det
     */
    public E[] toArray(E[] a) {
	if(a == null) {
	    throw new NullPointerException();
	}
	if(a.length < size()) {
	    return null;
	} else {
	    Iterator<E> it = iterator();  
	    int i = 0;
	    
	    while(it.hasNext()){
		E e = it.next();
		a[i++] = e;
	    }
	    if(a.length > size()) {
		a[size()] = null;
	    }
	}
	return a;
	
    }
    
    /**
     *@return iterator, s� man kan bruke de hjelpe-metodene
     */
    public Iterator<E> iterator() {
	return new BinaryIterator();
    }
   
    public class Node {
	public Node left, right;
	public E object;
	
	Node(E e) {
	    this.object = e;
	    this.left = null;
	    this.right = null;	
	}
    }
  
    /**
     *En klasse som inneholde metoder, og Stack er hentet fra java biblioteket
     *konstrukt�ren kj�res med en gang, og lager listen(stacken)
     */
    public class BinaryIterator implements Iterator<E> {
	
	Stack<E> stack = new Stack<E>();
	
	BinaryIterator() {
	    setStack(root);
	}
	
	/**
	 * @inheritdoc
	 */
	public boolean hasNext() {
	    return !stack.empty();
	    /*	    if(stack.empty()) {
		return true;
	    }else {
		return false;
		}*/
	}
	/**
	 * @inheritdoc
	 */
	public E next() {
	    if(!hasNext()){
		throw new NoSuchElementException();
	    }
	    return stack.pop();
	}
	

	/** Ikke n�dvendig � implementere, Stack har metoder for � ta seg av det
	 *
	 */	
	public void remove() {
	    throw new UnsupportedOperationException();
	}
	/**
	 *lager listen av treet, den starter ved � sjekke venstre noden helt til den treffer null, og da er noden vi er p� den minste objekter, for � si det kort er dette nesten som en l�kke, litt vanskelig � beskrive det med ord.
	 *
	 */
	void setStack(Node n){
	    if(n == null){ //Spesialtilfelle hvis roten er null
		return;
	    }
	    if(n.right != null){
		setStack(n.right);
	    }
	    stack.push(n.object);
	    if(n.left != null) {
		setStack(n.left);
	    }
	}
    }
}package inf1010.assignment;
import java.util.Iterator;

public interface Creature extends Comparable<Creature>, Iterable<Creature> {
    /**
     *brukt e f�rst void, men er bedre � bruke boolean slik at man vet fra shellet om det ble kj�rt eller ikke
     */
   public String getBirth();
   public boolean setBirth(String a);
   public String getPhone();
   public boolean setPhone(String b);
   public String getLocation();
   public boolean setLocation(String c);
   public String getName();
   public String getHomeplanet();
   public boolean addFriends(Creature a);
   public boolean rmFriends(Creature a);
   public boolean isFriend(Creature a);
   public Iterator<Creature> iterator();
    
   // public int comepareTo(Creature a);
	
   
}
package inf1010.assignment;

import java.util.Iterator;
import inf1010.lib.two.IfiCollection;
import java.util.NoSuchElementException;

public class SinglyLinkedList<E extends Comparable<E>> implements IfiCollection<E> {
    
    public Node first;
    int counter = 0;
  
    public boolean add(E e) {
	if(e == null){
	    throw new NullPointerException();
	}
	if(contains(e)) {
	    return false;
	}	
	
	Node n = new Node(e); 
	Node temp = first; 
       
	if(first == null) { //Spesielltilfelle
	    //  n.object = e;
	    first = n;
	    counter++;
	    return true;
	} else if(e.compareTo(first.object) < 0) {//Spesialltilfelle hvis input er mindre enn den f�rste objektet
	    //   n.object = e;
	    n.next = first;
	    first = n;
	    counter++;
	    return true;
	} else {
	    Node previous = temp;  // bruker det for � klare � sette objektet 'mellom' to objekter som fins fra f�r av
	  	
	    while(previous.next != null){
		if(e.compareTo(previous.next.object) < 0){
		    //    n.object = e;
		    Node temp2 = previous.next;
		    previous.next = n;
		    n.next = temp2;	
		    counter++;
		    return true;	   
		}
		previous = previous.next;
	    } 	    
	    //  n.object = e;
	    previous.next = n;
	    counter++;
	    return true;	 	       	    
	}
    }
    
    //sjekker om listen inneholder objektet, kunne bruke get-metoden for � sjekke om personen fins i listen istedenfor
    public boolean contains(E e) {
	if(get(e) == null){
	    return false;
	} else {
	    return true;
	}
    }
    //henter objektet fra listen
    public E get(E e) {
	for(Node n = first; n!= null; n = n.next) {  
	    if(n.object.compareTo(e) == 0) {
	
		return n.object;
	    }	  
	}

	return null;
    }
    //fjerner objektet fra listen
    public boolean remove(E e) {
	if(e == null) {
	    throw new NullPointerException();
	}
       	
	Node temp = first;
	
	while(temp != null) {
	    if(first.object.compareTo(e) == 0){  //Spesialtilfelle hvis det fins bare et object i listen
		if(first.next == null) {
		    first = null;
		    counter--;
		    return true;
		} else {                    // Hvis det er flere objekter i listen vil den f�rste sin neste bli den f�rste objektet 
		    first = first.next;
		    counter--;
		    return true;
		}
	    } else if(temp.next.object.compareTo(e) == 0){ // Siden den f�rste if testen har sjekket om den f�rste objektet er den som skal fjernes
		temp.next = temp.next.next;
		counter--;
		return true;
	    }
	    temp = temp.next;
	}	
	return false;
    }
    
    //returnerer st�rrelsen p� listen.
    public int size() {
	/*Node n = first;
	int sum = 0;
	while(n != null) {
	    sum++;
	    n = n.next;
	}
	return sum;	*/
	return counter;
    }
    
    //sjekker om listen er tom
    public boolean isEmpty() {
	if(first == null) {
	    return true;
	} else {
	    return false;
	}
    }
    
    //  fjerner alle objeckter i listen
    public void clear() {
	first = null;
	counter = 0;
    }
    
    // setter in listen over til en array, kunne brukt iterator til � l�se problemet, men lettere p� denne m�ten og bra m�te � l�se p� f�r man gjort Iterator.
    public E[] toArray(E[] a) {
	if(a == null) {
	    throw new NullPointerException();
	}	
	if(a.length < size()){
	    return null;
	} else {	
	    Node temp = first;
	    int i = 0;
	    
	    while(temp != null) {
		a[i++] = temp.object; 
		temp = temp.next;
	    }
	    if(a.length > size()) {
		a[size()] = null;
	    }
	}
	return a;
	
    }
    
    public void toList(E e) {
	Node n = new Node(e);
	n.next = first;
	first = n;
    }
    
    public Iterator<E> iterator() {
	return new NewIterator();
    }
    // Sender den f�rste objektet i listen, og this refererer til DENNE listen, siden vi lager mange lister sender vi bare den det gjelder for
    
    public class Node {
	public Node next;
	public E object;
    
	// forandret p� new Node<E>() -> new Node<E>(e) og slapp � skrive n.objekt. 
 	Node(E e) {       
	    this.object = e;
	    this.next = null;
	}
    }
    
    
    public class NewIterator implements Iterator<E> {
	Node currentNode, nextNode;
	boolean calledNext;

	NewIterator() {
	    nextNode = first;
	}

	// Siden det er bare to alternativer, false eller true, enten eller, sjekker om det fins neste objekt.
	public boolean hasNext() {
	    if(nextNode == null) {
		return false;
	    } else {
		return true;
	    }
	}

	// returnerer objektet samt oppdatere nextNode slik at vi kan sjekke om det fins en neste objekt.
	public E next() {
	    if(!hasNext()) {
		throw new NoSuchElementException();
	    } else {
		currentNode = nextNode;
		nextNode = nextNode.next;
		calledNext = true;
		return currentNode.object;
	    }
	}
	// peker bare til den neste sin neste objekt, og reduserer st�rrelsen
	public void remove() {
	    if(currentNode == null) {
		throw new IllegalStateException();
	    } 
	    if (calledNext) {
		if (nextNode != null) {
		    currentNode.next = nextNode.next;
		}
		calledNext = false;
		counter--;
	    }
	    
	}
    }
}package inf1010.assignment;

import java.util.Iterator;

abstract class CreatureAbstract implements Creature {
    /**
     *Hver creature har sin egen liste, og de listene er vennelisten dems
     *
     **/
    public String name,birthday, phone, location;
    public final String homeplanet;
    public SinglyLinkedList<Creature> list;

    CreatureAbstract(String a, String b, String c, String d, String e) {
	this.name = a;
	this.birthday = b;
	this.phone = c;
	this.location = d;
	this.homeplanet = e;
	this.list = new SinglyLinkedList<Creature>();
    }
    /**
     *@return navnet til objektet, som er en creature.
     */
    public String getName(){
	return name;
    }
    /**
     *@return bursdag/f�dselsdato
     */
    public String getBirth(){
	return birthday;
    }
    /**
     *@return posisjonen til creature
     */
    public String getLocation(){
	return location;
    }  
    /**
     *@return homeplanet
     */
    public String getHomeplanet(){
	return homeplanet;
    }
    /**
     *@return phone
     */
    public String getPhone(){
	return phone;
    }
    /**
     *kj�rer metodene i SinglyLinkedList klassen, gjelder for alle nede.Man gjenbruker bare alt man har gjort.
     */
    public boolean addFriends(Creature a){
	return list.add(a);
    }
    
    public boolean rmFriends(Creature a){
	return list.remove(a);	
    }

    public boolean isFriend(Creature a){
	return list.contains(a);
    }
    
    /**
     *returnerer en iterator over vennelist
     */
    public Iterator<Creature> iterator() {
	return list.iterator();
    }
    
    public boolean setBirth(String g){
	birthday = g;
	return true;
	
    }

    public boolean setPhone(String h){
	phone = h;
	return true;
    }
    
    public boolean setLocation(String i){
	location = i;
	return true;
    }
    
    public int compareTo(Creature j){
	return getName().compareTo(j.getName());
    }
}package inf1010.assignment;

public class EvilAlien extends CreatureAbstract {
    EvilAlien(String name, String birthday, String phone, String location){
	super(name, birthday, phone, location, "Ruritania");
    }
}
package inf1010.assignment;

public class Human extends CreatureAbstract {
    Human(String name, String birthday, String phone, String location){
	super(name, birthday, phone, location, "Earth");
    }
}package inf1010.assignment;
import inf1010.lib.two.ShellBase;
import java.util.Iterator;
/**
 *Ser ut som inputLoop metoden gj�r alt den harde jobben ved � dele opp og sammeligne input for oss,if�lge javadoc da.
 *
 */

public class Shell extends ShellBase {
    BinarySearchTree<Creature> creatures = new BinarySearchTree<Creature>();
    
    Shell() {
	inputLoop();
    }
    
    public static void main(String[] args) {
	Shell ss = new Shell();
    }
    /**
     *@inherit
     */
    protected void addHuman(String name, String birthday, String phone, String location){
	Human a = new Human(name, birthday, phone, location);
	if(creatures.add(a)){
	    System.out.println(name + " was added");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void addAlien(String name, String birthday, String phone, String location) {
	EvilAlien a = new EvilAlien(name, birthday, phone, location);
	if(creatures.add(a)){
	    System.out.println(name + " was added");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void update(String name, String birthday, String phone, String location) {
	Creature one = getCreature(name);
	//Creature one = new Human(creatures.get(name)));
	if(one != null){
	    one.setBirth(birthday);
	    one.setPhone(phone);
	    one.setLocation(location);
	    System.out.println(name + "s information has been updated");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void registerFriend(String name, String friendname){
	Creature one = getCreature(name);
	Creature two = getCreature(friendname);
	
	if(one != null && two != null && one.addFriends(two) && two.addFriends(one)){
	    System.out.println(name + " and " + friendname + " are now friends");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void unregisterFriend(String name, String friendname){
	Creature one = getCreature(name);
	Creature two = getCreature(friendname);
	
	if(one != null && two != null && one.rmFriends(two) && two.rmFriends(one)) {
	    System.out.println(name + " and " + friendname + " aren't friends anymore");
	} else {
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void show(String name){
	Creature one = getCreature(name);
	
	if(one != null){
	    System.out.printf("Name: %s\nBirthday: %s\nPhone: %s\nLocation: %s\nHomeplanet: %s\nFriends:\n", one.getName(), one.getBirth(), one.getPhone(), one.getLocation(), one.getHomeplanet());
	    Iterator<Creature> it = one.iterator();
	    while(it.hasNext()) {
		System.out.println(it.next().getName());
	    }
	} else {
	    System.out.println("There seems to be some issues, please try again");
	}
	
    }
    /**
     *@inherit
     */
    protected void list(){
	Iterator<Creature> it = creatures.iterator();
	if(it.hasNext()){
	    while(it.hasNext()){
		System.out.println(it.next().getName());
	    }
	} else {
	    System.out.println("There is no one in the list to show");
	}
    }
    
    protected void help() {
	System.out.printf("addHuman     Add a human to the database.\naddAlien     Add a evil alien to the database.\nupdate       Update an existing human or alien in the database.\nregfriend    Register a friend.\nunregfriend  Unregister a friend.\nshow         Show all information about a human or an alien, including the names of their friends.\nlist         List all people in the database.\nexit         Exit the shell.\n");
    }
    
    /**
     *Hjelpe-klasse siden kan ikke bruke get-metoden fra Bin�re tree ved bruk av String, s� jeg kj�rer gjennom det bin�re treet og sammeligner alle objekters navn med .. navnet eller inputten, og 
     *returnerer objektet
     */
    public Creature getCreature(String name){
	Iterator<Creature> it = creatures.iterator();
	if(it.hasNext()){
	    while(it.hasNext()){
		Creature temp = it.next();
		if(temp.getName().equals(name)){
		    return temp;
		}
	    }
	} 
	return null;
    }	

}