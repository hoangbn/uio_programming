package inf1010.assignment;
import inf1010.lib.two.ShellBase;
import java.util.Iterator;
/**
 *Ser ut som inputLoop metoden gj�r alt den harde jobben ved � dele opp og sammeligne input for oss,if�lge javadoc da.
 *
 */

public class Shell extends ShellBase {
    BinarySearchTree<Creature> creatures = new BinarySearchTree<Creature>();
    
    Shell() {
	inputLoop();
    }
    
    public static void main(String[] args) {
	Shell ss = new Shell();
    }
    /**
     *@inherit
     */
    protected void addHuman(String name, String birthday, String phone, String location){
	Human a = new Human(name, birthday, phone, location);
	if(creatures.add(a)){
	    System.out.println(name + " was added");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void addAlien(String name, String birthday, String phone, String location) {
	EvilAlien a = new EvilAlien(name, birthday, phone, location);
	if(creatures.add(a)){
	    System.out.println(name + " was added");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void update(String name, String birthday, String phone, String location) {
	Creature one = getCreature(name);
	//Creature one = new Human(creatures.get(name)));
	if(one != null){
	    one.setBirth(birthday);
	    one.setPhone(phone);
	    one.setLocation(location);
	    System.out.println(name + "s information has been updated");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void registerFriend(String name, String friendname){
	Creature one = getCreature(name);
	Creature two = getCreature(friendname);
	
	if(one != null && two != null && one.addFriends(two) && two.addFriends(one)){
	    System.out.println(name + " and " + friendname + " are now friends");
	}else{
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void unregisterFriend(String name, String friendname){
	Creature one = getCreature(name);
	Creature two = getCreature(friendname);
	
	if(one != null && two != null && one.rmFriends(two) && two.rmFriends(one)) {
	    System.out.println(name + " and " + friendname + " aren't friends anymore");
	} else {
	    System.out.println("There seems to be some issues, please try again");
	}
    }
    /**
     *@inherit
     */
    protected void show(String name){
	Creature one = getCreature(name);
	
	if(one != null){
	    System.out.printf("Name: %s\nBirthday: %s\nPhone: %s\nLocation: %s\nHomeplanet: %s\nFriends:\n", one.getName(), one.getBirth(), one.getPhone(), one.getLocation(), one.getHomeplanet());
	    Iterator<Creature> it = one.iterator();
	    while(it.hasNext()) {
		System.out.println(it.next().getName());
	    }
	} else {
	    System.out.println("There seems to be some issues, please try again");
	}
	
    }
    /**
     *@inherit
     */
    protected void list(){
	Iterator<Creature> it = creatures.iterator();
	if(it.hasNext()){
	    while(it.hasNext()){
		System.out.println(it.next().getName());
	    }
	} else {
	    System.out.println("There is no one in the list to show");
	}
    }
    
    protected void help() {
	System.out.printf("addHuman     Add a human to the database.\naddAlien     Add a evil alien to the database.\nupdate       Update an existing human or alien in the database.\nregfriend    Register a friend.\nunregfriend  Unregister a friend.\nshow         Show all information about a human or an alien, including the names of their friends.\nlist         List all people in the database.\nexit         Exit the shell.\n");
    }
    
    /**
     *Hjelpe-klasse siden kan ikke bruke get-metoden fra Bin�re tree ved bruk av String, s� jeg kj�rer gjennom det bin�re treet og sammeligner alle objekters navn med .. navnet eller inputten, og 
     *returnerer objektet
     */
    public Creature getCreature(String name){
	Iterator<Creature> it = creatures.iterator();
	if(it.hasNext()){
	    while(it.hasNext()){
		Creature temp = it.next();
		if(temp.getName().equals(name)){
		    return temp;
		}
	    }
	} 
	return null;
    }	

}