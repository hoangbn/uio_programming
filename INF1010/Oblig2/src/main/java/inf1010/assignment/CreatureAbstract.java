package inf1010.assignment;

import java.util.Iterator;

abstract class CreatureAbstract implements Creature {
    /**
     *Hver creature har sin egen liste, og de listene er vennelisten dems
     *
     **/
    public String name,birthday, phone, location;
    public final String homeplanet;
    public SinglyLinkedList<Creature> list;

    CreatureAbstract(String a, String b, String c, String d, String e) {
	this.name = a;
	this.birthday = b;
	this.phone = c;
	this.location = d;
	this.homeplanet = e;
	this.list = new SinglyLinkedList<Creature>();
    }
    /**
     *@return navnet til objektet, som er en creature.
     */
    public String getName(){
	return name;
    }
    /**
     *@return bursdag/f�dselsdato
     */
    public String getBirth(){
	return birthday;
    }
    /**
     *@return posisjonen til creature
     */
    public String getLocation(){
	return location;
    }  
    /**
     *@return homeplanet
     */
    public String getHomeplanet(){
	return homeplanet;
    }
    /**
     *@return phone
     */
    public String getPhone(){
	return phone;
    }
    /**
     *kj�rer metodene i SinglyLinkedList klassen, gjelder for alle nede.Man gjenbruker bare alt man har gjort.
     */
    public boolean addFriends(Creature a){
	return list.add(a);
    }
    
    public boolean rmFriends(Creature a){
	return list.remove(a);	
    }

    public boolean isFriend(Creature a){
	return list.contains(a);
    }
    
    /**
     *returnerer en iterator over vennelist
     */
    public Iterator<Creature> iterator() {
	return list.iterator();
    }
    
    public boolean setBirth(String g){
	birthday = g;
	return true;
	
    }

    public boolean setPhone(String h){
	phone = h;
	return true;
    }
    
    public boolean setLocation(String i){
	location = i;
	return true;
    }
    
    public int compareTo(Creature j){
	return getName().compareTo(j.getName());
    }
}