import java.util.ArrayList;

public class Box{

    public ArrayList<Square> position;
    
    Box(int size){
	position = new ArrayList<Square>(size);
    }
    /**
     *sjekker om tallet finnes i boksen
     */
    public boolean valid(int values){
	for(int i = 0; i < position.size(); i++){
	    if(position.get(i).value == values) {
		return false;
	    }
	}
	return true;
    }
    /**
     *legget til tallet i boksen
     */
    public boolean insert(Square s){
	return position.add(s);
    }
    
}