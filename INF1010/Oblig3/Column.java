import java.util.ArrayList;

public class Column{
    public ArrayList<Square> position;
    
    Column(int size) {
	position = new ArrayList<Square>(size);
    }
    /**
     *sjekker om tallet finnes i kolonnen
     */
    public boolean valid(int values){
	for(int i = 0; i < position.size(); i++){
	    if(position.get(i).value == values) {
		return false;
	    }
	}
	return true;
    }
    /**
     *legger til tallet i kolonnen
     */
    public boolean insert(Square s){
	return position.add(s);
    }


}
