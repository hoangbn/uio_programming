import java.awt.BorderLayout;			
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JFileChooser;
import javax.swing.filechooser.*;

import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

/** 
* Tegner ut et Sudoku-brett.
* @author Christian Tryti
* @author Stein Gjessing                         
*/
public class SudokuGUI extends JFrame implements ActionListener, Runnable {

    private final int RUTE_STRELSE = 50;  /* St�rrelsen p� hver rute */
    private final int PLASS_TOPP = 50;  /* Plass p� toppen av brettet */

    private JTextField[][] brett;   /* for � tegne ut alle rutene */
    private int dimensjon;      /* st�rrelsen p� brettet (n) */
    private int vertikalAntall; /* antall ruter i en boks loddrett */
    private int horisontalAntall;   /* antall ruter i en boks vannrett */
    public Board board;
    public SudokuBuffer buffer;
    public JButton finnSvarKnapp, nesteKnapp;
    public JPanel brettPanel, knappePanel;
    public ArrayList<int[][]> solutions;
    public int counter = 0;
    
    /**
     *konstrukt�r, hvis man bare leser fil dukker denne opp, hvis ike betyr det at man skal lese og skrive til fil
     *, da vil det ikke dukke opp noe GUI
     */
    public SudokuGUI() {	
	if(!onlyRead) {
	    dimensjon = 9;
	    vertikalAntall = 3;
	    horisontalAntall = 3;
	    	    
	    brett = new JTextField[dimensjon][dimensjon];
	    
	    setPreferredSize(new Dimension(dimensjon * RUTE_STRELSE, dimensjon * RUTE_STRELSE + PLASS_TOPP));
	    setTitle("Sudoku " + dimensjon + " x " + dimensjon );
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setLayout(new BorderLayout());
	    
	    JPanel knappePanel = lagKnapper();
	    //   getContentPane().add(brettPanel, BorderLayout.CENTER);
	    getContentPane().add(knappePanel, BorderLayout.NORTH);
	    pack();
	    setVisible(true);
	}
    }
    /**
     *alternativ konstrukt�r for ingen input p� terminal
     */
    public SudokuGUI(int a) {
	dimensjon = 9;
	vertikalAntall = 3;
	horisontalAntall = 3;
		
	brett = new JTextField[dimensjon][dimensjon];
	
	setPreferredSize(new Dimension(dimensjon * RUTE_STRELSE, dimensjon * RUTE_STRELSE + PLASS_TOPP));
	setTitle("Sudoku " + dimensjon + " x " + dimensjon );
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setLayout(new BorderLayout());
	
	JPanel knappePanel = lagKnapper();
	//JPanel brettPanel = lagBrettet();
	
	getContentPane().add(knappePanel, BorderLayout.NORTH);
	//   getContentPane().add(brettPanel, BorderLayout.CENTER);
	pack();
	setVisible(true);
    }
    
	
	
    /**
     *N�dvendig for Runnable, brukes for � starte en eget tr�d slik at man ikke f�r feilmeldig p� grunn av at programmet b�de 
     *setter inn objekter i iterator og pr�ver samtidig � modifisere det
     */
    public void run() {
	board.beginSolving();
	solutions = board.gets();
	System.out.println(solutions.size());
	if(solutions.size() == 0) return;
	
	JPanel panel = lagBrettet(solutions.get(0));
	counter = 1;
	this.getContentPane().remove(panel);
	this.getContentPane().add(panel, BorderLayout.CENTER);
	this.pack();
	this.setVisible(true);
    }

    /**
     *knapper som trykkes p�. n�r ma taster p� find solution skaper man en ny tr�d slik at man unng�r eventuele
     *modifiserings feilmeldinger p� iteratoren
     */
    public void actionPerformed(ActionEvent a) {
	String s = a.getActionCommand();
	JPanel make;
	if(s.equals("Open File")){
	    fileChooser();

	} else if(s.equals("Find Solution(s)")){	     
	    //   nesteKnapp.setEnabled(true);
	    // 	      finnSvarKnapp.setEnabled(false);
	    new Thread(this).start();
	} else if(s.equals("Next Solution")){
	    if(solutions.size() == counter){
		 //   nesteKnapp.setEnabled(false);
		   System.out.println("Ingen flere l�sninger");
	    } else {
		make = lagBrettet(solutions.get(counter));
		this.getContentPane().remove(make);
		this.getContentPane().add(make, BorderLayout.CENTER);
		this.pack();
		this.setVisible(true);
		counter++;
	    }
	}
    }
    
    /** Lager et brett med knapper langs toppen. */
    public SudokuGUI(int dim, int hd, int br, int[][] solution) {
        dimensjon = dim;
        vertikalAntall = hd;
        horisontalAntall = br;

        brett = new JTextField[dimensjon][dimensjon];

        setPreferredSize(new Dimension(dimensjon * RUTE_STRELSE, dimensjon * RUTE_STRELSE + PLASS_TOPP));
        setTitle("Sudoku " + dimensjon + " x " + dimensjon );
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JPanel knappePanel = lagKnapper();
	JPanel brettPanel = lagBrettet(solution);

        getContentPane().add(knappePanel, BorderLayout.NORTH);
        getContentPane().add(brettPanel, BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    /** 
    * Lager et panel med alle rutene. 
    * @return en peker til panelet som er laget.
    */
    private JPanel lagBrettet(int[][] solution) {
        int topp, venstre;
	int newSize = solution.length;
        JPanel brettPanel = new JPanel();
        brettPanel.setLayout(new GridLayout(newSize, newSize));
        brettPanel.setAlignmentX(CENTER_ALIGNMENT);
        brettPanel.setAlignmentY(CENTER_ALIGNMENT);
        setPreferredSize(new Dimension(new Dimension(newSize * RUTE_STRELSE, newSize * RUTE_STRELSE)));
        for(int i = 0; i < solution.length; i++) {
            /* finn ut om denne raden trenger en tykker linje p� toppen: */
            topp = (i % vertikalAntall == 0 && i != 0) ? 4 : 1;
            for(int j = 0; j < solution[i].length; j++) {
                /* finn ut om denne ruten er en del av en kolonne 
                   som skal ha en tykkere linje til venstre:       */
                venstre = (j % horisontalAntall == 0 && j != 0) ? 4 : 1;

                JTextField ruten = new JTextField();
                ruten.setBorder(BorderFactory.createMatteBorder(topp, venstre, 1, 1, Color.black));
                ruten.setHorizontalAlignment(SwingConstants.CENTER);
                ruten.setPreferredSize(new Dimension(RUTE_STRELSE, RUTE_STRELSE));
                if(solution[i][j] == -1){
		    ruten.setText(" ");
		    brett[i][j] = ruten;
		    brettPanel.add(ruten);
		} else {
		    ruten.setText("" + Character.toUpperCase(Character.forDigit(solution[i][j], 36)));
		    brett[i][j] = ruten;
		    brettPanel.add(ruten);
		}
            }
        }
        return brettPanel;
    }
    
    /** 
     * Lager et panel med noen knapper. 
     * @return en peker til panelet som er laget.
     */
 
    private JPanel lagKnapper() {
	
	JPanel knappPanel = new JPanel();
	knappPanel.setLayout(new BoxLayout(knappPanel, BoxLayout.X_AXIS));
	JButton finnSvarKnapp = new JButton("Find Solution(s)");
	finnSvarKnapp.setEnabled(true);
	finnSvarKnapp.addActionListener(this);
	JButton nesteKnapp = new JButton("Next Solution");
	nesteKnapp.setEnabled(true);
	nesteKnapp.addActionListener(this);
	JButton knapp = new JButton("Open File");
	knapp.addActionListener(this);
	knappPanel.add(finnSvarKnapp);
	knappPanel.add(nesteKnapp);
	knappPanel.add(knapp);
	return knappPanel;
	
    }
    /**
     *holder orden p� konstrukt�ren
     */
    public boolean onlyRead = true;
    /**
     *kj�res hvis brukeren tastet inn filnavn som skal leses
     */
    public void command(String read){	
	File input = new File(read);
	readFile(input);
    }

    /**
     *filnavn leses og lagres til den oppgitte filnavnet
     */
    public void command(String read, String write){
	onlyRead = false;
	File input = new File(read);
	File output = new File(write);
	readFile(input);
	board.beginSolving();
	writeFile(output);
    } 

	    
    /**
     *leser fra fil og filer konverterer char til int og lagres p� brettet
     */
    public void readFile(File file) {
	int size = 0;
	int height = 0;
	int width = 0;
	char[] singleArray;
	int[][] doubleArray;
	String read = "";

	try {
	    Scanner sc = new Scanner(file);
	    size = Integer.parseInt(sc.next());
	    width = Integer.parseInt(sc.next());
	    height = Integer.parseInt(sc.next());
	    dimensjon = size;
	    vertikalAntall = width;
	    horisontalAntall = height;
	    sc.nextLine();
	    doubleArray = new int[size][size];
	    for(int i = 0; i < size; i++) {
		singleArray = sc.nextLine().toCharArray();
		for(int j = 0; j < size; j++) {	    
		    doubleArray[i][j] = Character.digit(singleArray[j], 36);
		}
	    }
	    board = new Board(size, height, width, doubleArray);
	    if(onlyRead) {	
		brett = new JTextField[size][size];
		setPreferredSize(new Dimension(size * RUTE_STRELSE, size * RUTE_STRELSE + PLASS_TOPP));
		setTitle("Sudoku " + size + " x " + size );
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		JPanel knappePanel = lagKnapper();
		JPanel brettPanel = lagBrettet(doubleArray);
		
		getContentPane().add(knappePanel, BorderLayout.NORTH);
		getContentPane().add(brettPanel, BorderLayout.CENTER);
		pack();
		setVisible(true);
	    }
	} catch (FileNotFoundException e){
	    System.err.println("An error occured when trying to open the file ");
	    
	    
	}
    }

    /**
     *konverterer tall til char og skriver til den oppgitte fil-navnet
     *
     */
    public void writeFile(File file){
	try {
	    FileWriter fileOut = new FileWriter(file);
	    ArrayList<int[][]> list = board.gets();
	    for(int i = 0; i < list.size(); i++){
		fileOut.write((i+1) + ": ");
		for(int j = 0; j < dimensjon; j++) {		    
		    for(int k = 0; k < dimensjon; k++) {
			fileOut.write(Character.toUpperCase(Character.forDigit(list.get(i)[j][k], 36)));
		    }
		    fileOut.write("// ");
		}
		fileOut.write(System.getProperty("line.separator"));
	    }
	    fileOut.close();
	    System.out.println("L�sningene er lagret i filen");
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}
    }
    

    /**
     *�pner og leser txt-filene. bruker hjelpeklassene for � filtrere filene osv.
     *
     */
    public void fileChooser(){
	int size = 0;
	int height = 0;
	int width = 0;
	char[] singleArray;
	int[][] doubleArray;
	String read = "";
	JFileChooser file = new JFileChooser();
	FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT files", "txt");
	file.setFileFilter(filter);

	int reValue = file.showOpenDialog(this);
	if(reValue == JFileChooser.CANCEL_OPTION) {
	    return;
	} else if(reValue == JFileChooser.APPROVE_OPTION) {
	    System.out.println("You chose to open this file: " + file.getSelectedFile().getName());
	    try {
		Scanner sc = new Scanner(file.getSelectedFile());
		size = Integer.parseInt(sc.next());
		width = Integer.parseInt(sc.next());
		height = Integer.parseInt(sc.next());
		dimensjon = size;
		vertikalAntall = width;
		horisontalAntall = height;
		sc.nextLine();
		doubleArray = new int[size][size];
		for(int i = 0; i < size; i++) {
		    singleArray = sc.nextLine().toCharArray();
		    for(int j = 0; j < size; j++) {	       
			    doubleArray[i][j] = Character.digit(singleArray[j], 36);		       
		    }
		}
		brett = new JTextField[size][size];
		setPreferredSize(new Dimension(size * RUTE_STRELSE, size * RUTE_STRELSE + PLASS_TOPP));
		setTitle("Sudoku " + size + " x " + size );
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		board = new Board(size, height, width, doubleArray);

		JPanel knappePanel = lagKnapper();
		JPanel brettPanel = lagBrettet(doubleArray);
	       
	// 	finnSvarKnapp.setEnabled(true); 
		getContentPane().remove(brettPanel);
		getContentPane().add(knappePanel, BorderLayout.NORTH);
		getContentPane().add(brettPanel, BorderLayout.CENTER);		
		pack();
		setVisible(true);	
	    } catch (FileNotFoundException e){
		System.err.println("An error occured when trying to open the file " + file.getSelectedFile().getName());
		
		
	    }
	    
	
	}
    }
}
