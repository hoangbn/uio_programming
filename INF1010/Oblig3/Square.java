import java.util.ArrayList;

public class Square{
    public int value;
    public Column columnObject;
    public Row rowObject;
    public Box boxObject;
    public Board boardObject;
    public boolean occupied;
    public Square nextSquare;
    public int dimension;

    /**
     *Konstrukt�r
     */
    Square(int boxSize, int a, Column b, Row c, Box d, Board e){
    	this.value = a;
	this.columnObject = b;
	this.rowObject = c;
	this.boxObject = d;
	this.boardObject = e;
	this.occupied = (value != -1);
	this.dimension = boxSize;

	}	
    
    /**
     *setter neste peker
     */
    public void setNextSquare(Square s){
	nextSquare = s;
    }

    /**
     *setter et hvilken som helst nummer i ruten, og n�r den klarer � sette et
     * nummer kaller den p� seg selv igjen. n�r den nest epeker er null vil det
     * si at da befinner vi i den siste ruten og da lagres brettet til bufferet
     */
    public void setNumber(){
	if (occupied) {
	    if (nextSquare == null) {
		boardObject.addSolutions();
	    } else {
		nextSquare.setNumber();
	    }
	    return;
	}

	for(int i = 1; i <= dimension; i++){	 
	    if(valid(i)) {
		value = i;
		if (nextSquare == null) {
		    boardObject.addSolutions();
		} else {
		    nextSquare.setNumber();
		}
		value = -1;
	    }
	}
    }

    /**
     *sjekker om verdien finnes allerede i brettet
     */
    private boolean valid(int i) {
	return rowObject.valid(i) && columnObject.valid(i) && boxObject.valid(i);
    }
    
}	

    
