import java.util.ArrayList;

public class Board{
    public Square[][] table;
    public Row[] row;
    public Box[] box;
    public Column[] column;
    public Square tempNextSquare;
    public SudokuGUI gui;
    public ArrayList<int[][]> answers;
    public SudokuBuffer buffer = new SudokuBuffer();
    public int dimension;
    
    Board(int boxSize, int boxWidth, int boxHeight , int[][] tableContent) {	
	table = new Square[boxSize][boxSize];
	row = new Row[boxSize];
	column = new Column[boxSize];
	box = new Box[boxSize];
	answers = new ArrayList<int[][]>();
// 	this.gui = new SudokuGUI(boxSize, boxHeight, boxWidth);
	createObjects(boxSize, boxWidth, boxHeight, tableContent);
    }

    /**
     *lager rader for enhver i, og kolonner for enhver j, boxene blir opprettet i starten av boksene, man kan finne dens posisjon ved � ta b�de i modulus av bredden av boksen og j modulus med 
     *hoyden og resten m� v�re lik 0. testet det p� et ark med 3x3 og 4x4 og 5x5,og det ser ut til � funke.  lager nestepekere for alle rutene med en gang f�r man pr�ver � l�se brettet.
     **/
    public void createObjects(int size, int width, int height, int[][] position){
	int temp = 0;
	
	for(int i = 0; i < table.length; i++){
	    row[i] = new Row(size);                 
	    for(int j = 0; j < table[i].length; j++) {
		if(i == 0) {
		    column[j] = new Column(size); 
		}		
		//System.out.printf("%d %% %d = %d, %d %% %d = %d%n", i, height, i % height, j, width, j % width);
		if((i % height == 0) && (j % width == 0 )) {
		    box[temp++] = new Box(size);
		    //System.out.println(box[temp - 1]);
		}
		//System.out.println(whichBox(i,j,width,height));
	
		Square s = new Square(size, position[i][j], column[j], row[i], box[whichBox(i, j, width, height)], this);
		if(!(i == 0 && j == 0)){ //kj�res etter at den f�rste neste peker har blitt fullf�rt
		    tempNextSquare.setNextSquare(s);
		}
		tempNextSquare = s;
		table[i][j] = s;
		row[i].insert(s);
		column[j].insert(s);
		box[whichBox(i, j, width, height)].insert(s);		    
	    }
	}
    }
	
	
    /**
     *Fant lignende algoritmer p� nettet som skal finne ut hvilken boks man befinner seg i, ganske stygg. 
     *returnerer eksakt boks man befinner seg i.
     */
    public int whichBox(int row, int column, int width, int height){
	return (row / height) * height + column / width;
    }

    /**
     *lager et nytt 2D brett og lagrer det i bufferet siden det er ikke et krav 
     * � huske rader, kolonner og bokser rutenene var i.
     */
    public void addSolutions() {
	int[][] temp = new int[table.length][table.length];
	for(int i = 0; i < table.length; i++){
	    for(int j = 0; j < table[i].length; j++) {
		temp[i][j] = table[i][j].value;
	    }
	}
	buffer.add(temp);
	

    }
    /**
     *starter � l�se brettet fra den f�rste ruten,
     */
    public void beginSolving() {
	System.out.println("Solving....");
	table[0][0].setNumber();
    }

    /**
     * henter alle l�sningene som er lagret i arrayen i bufferet
     */
    public ArrayList<int[][]> gets(){
	return buffer.get();
    }
}