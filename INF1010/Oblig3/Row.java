import java.util.ArrayList;

public class Row{
    public ArrayList<Square> position;

    Row(int size){
	position = new ArrayList<Square>(size);
    }
    /**
     *sjekker om talle fins allerede i raden
     */
    public boolean valid(int values){
	for(int i = 0; i < position.size(); i++){
	    if(position.get(i).value == values) {
		return false;
	    }
	}
	return true;
    }
    /**
     *legger til tallet i raden
     */
    public boolean insert(Square s){
	return position.add(s);
    }
}