import java.awt.BorderLayout;			
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JFileChooser;
import javax.swing.filechooser.*;

import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

/** 
* Tegner ut et Sudoku-brett.
* @author Christian Tryti
* @author Stein Gjessing                         
*/
public class SudokuGUI extends JFrame implements ActionListener, Runnable {

    private final int RUTE_STRELSE = 50;  /* St�rrelsen p� hver rute */
    private final int PLASS_TOPP = 50;  /* Plass p� toppen av brettet */

    private JTextField[][] brett;   /* for � tegne ut alle rutene */
    private int dimensjon;      /* st�rrelsen p� brettet (n) */
    private int vertikalAntall; /* antall ruter i en boks loddrett */
    private int horisontalAntall;   /* antall ruter i en boks vannrett */
    public Board board;
    public SudokuBuffer buffer;
    public JButton finnSvarKnapp, nesteKnapp;
    public JPanel brettPanel, knappePanel;
    public ArrayList<int[][]> solutions;
    public int counter = 0;
    
    /**
     *konstrukt�r, hvis man bare leser fil dukker denne opp, hvis ike betyr det at man skal lese og skrive til fil
     *, da vil det ikke dukke opp noe GUI
     */
    public SudokuGUI() {	
	if(!onlyRead) {
	    dimensjon = 9;
	    vertikalAntall = 3;
	    horisontalAntall = 3;
	    	    
	    brett = new JTextField[dimensjon][dimensjon];
	    
	    setPreferredSize(new Dimension(dimensjon * RUTE_STRELSE, dimensjon * RUTE_STRELSE + PLASS_TOPP));
	    setTitle("Sudoku " + dimensjon + " x " + dimensjon );
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setLayout(new BorderLayout());
	    
	    JPanel knappePanel = lagKnapper();
	    //   getContentPane().add(brettPanel, BorderLayout.CENTER);
	    getContentPane().add(knappePanel, BorderLayout.NORTH);
	    pack();
	    setVisible(true);
	}
    }
    /**
     *alternativ konstrukt�r for ingen input p� terminal
     */
    public SudokuGUI(int a) {
	dimensjon = 9;
	vertikalAntall = 3;
	horisontalAntall = 3;
		
	brett = new JTextField[dimensjon][dimensjon];
	
	setPreferredSize(new Dimension(dimensjon * RUTE_STRELSE, dimensjon * RUTE_STRELSE + PLASS_TOPP));
	setTitle("Sudoku " + dimensjon + " x " + dimensjon );
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setLayout(new BorderLayout());
	
	JPanel knappePanel = lagKnapper();
	//JPanel brettPanel = lagBrettet();
	
	getContentPane().add(knappePanel, BorderLayout.NORTH);
	//   getContentPane().add(brettPanel, BorderLayout.CENTER);
	pack();
	setVisible(true);
    }
    
	
	
    /**
     *N�dvendig for Runnable, brukes for � starte en eget tr�d slik at man ikke f�r feilmeldig p� grunn av at programmet b�de 
     *setter inn objekter i iterator og pr�ver samtidig � modifisere det
     */
    public void run() {
	board.beginSolving();
	solutions = board.gets();
	System.out.println(solutions.size());
	if(solutions.size() == 0) return;
	
	JPanel panel = lagBrettet(solutions.get(0));
	counter = 1;
	this.getContentPane().remove(panel);
	this.getContentPane().add(panel, BorderLayout.CENTER);
	this.pack();
	this.setVisible(true);
    }

    /**
     *knapper som trykkes p�. n�r ma taster p� find solution skaper man en ny tr�d slik at man unng�r eventuele
     *modifiserings feilmeldinger p� iteratoren
     */
    public void actionPerformed(ActionEvent a) {
	String s = a.getActionCommand();
	JPanel make;
	if(s.equals("Open File")){
	    fileChooser();

	} else if(s.equals("Find Solution(s)")){	     
	    //   nesteKnapp.setEnabled(true);
	    // 	      finnSvarKnapp.setEnabled(false);
	    new Thread(this).start();
	} else if(s.equals("Next Solution")){
	    if(solutions.size() == counter){
		 //   nesteKnapp.setEnabled(false);
		   System.out.println("Ingen flere l�sninger");
	    } else {
		make = lagBrettet(solutions.get(counter));
		this.getContentPane().remove(make);
		this.getContentPane().add(make, BorderLayout.CENTER);
		this.pack();
		this.setVisible(true);
		counter++;
	    }
	}
    }
    
    /** Lager et brett med knapper langs toppen. */
    public SudokuGUI(int dim, int hd, int br, int[][] solution) {
        dimensjon = dim;
        vertikalAntall = hd;
        horisontalAntall = br;

        brett = new JTextField[dimensjon][dimensjon];

        setPreferredSize(new Dimension(dimensjon * RUTE_STRELSE, dimensjon * RUTE_STRELSE + PLASS_TOPP));
        setTitle("Sudoku " + dimensjon + " x " + dimensjon );
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JPanel knappePanel = lagKnapper();
	JPanel brettPanel = lagBrettet(solution);

        getContentPane().add(knappePanel, BorderLayout.NORTH);
        getContentPane().add(brettPanel, BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    /** 
    * Lager et panel med alle rutene. 
    * @return en peker til panelet som er laget.
    */
    private JPanel lagBrettet(int[][] solution) {
        int topp, venstre;
	int newSize = solution.length;
        JPanel brettPanel = new JPanel();
        brettPanel.setLayout(new GridLayout(newSize, newSize));
        brettPanel.setAlignmentX(CENTER_ALIGNMENT);
        brettPanel.setAlignmentY(CENTER_ALIGNMENT);
        setPreferredSize(new Dimension(new Dimension(newSize * RUTE_STRELSE, newSize * RUTE_STRELSE)));
        for(int i = 0; i < solution.length; i++) {
            /* finn ut om denne raden trenger en tykker linje p� toppen: */
            topp = (i % vertikalAntall == 0 && i != 0) ? 4 : 1;
            for(int j = 0; j < solution[i].length; j++) {
                /* finn ut om denne ruten er en del av en kolonne 
                   som skal ha en tykkere linje til venstre:       */
                venstre = (j % horisontalAntall == 0 && j != 0) ? 4 : 1;

                JTextField ruten = new JTextField();
                ruten.setBorder(BorderFactory.createMatteBorder(topp, venstre, 1, 1, Color.black));
                ruten.setHorizontalAlignment(SwingConstants.CENTER);
                ruten.setPreferredSize(new Dimension(RUTE_STRELSE, RUTE_STRELSE));
                if(solution[i][j] == -1){
		    ruten.setText(" ");
		    brett[i][j] = ruten;
		    brettPanel.add(ruten);
		} else {
		    ruten.setText("" + Character.toUpperCase(Character.forDigit(solution[i][j], 36)));
		    brett[i][j] = ruten;
		    brettPanel.add(ruten);
		}
            }
        }
        return brettPanel;
    }
    
    /** 
     * Lager et panel med noen knapper. 
     * @return en peker til panelet som er laget.
     */
 
    private JPanel lagKnapper() {
	
	JPanel knappPanel = new JPanel();
	knappPanel.setLayout(new BoxLayout(knappPanel, BoxLayout.X_AXIS));
	JButton finnSvarKnapp = new JButton("Find Solution(s)");
	finnSvarKnapp.setEnabled(true);
	finnSvarKnapp.addActionListener(this);
	JButton nesteKnapp = new JButton("Next Solution");
	nesteKnapp.setEnabled(true);
	nesteKnapp.addActionListener(this);
	JButton knapp = new JButton("Open File");
	knapp.addActionListener(this);
	knappPanel.add(finnSvarKnapp);
	knappPanel.add(nesteKnapp);
	knappPanel.add(knapp);
	return knappPanel;
	
    }
    /**
     *holder orden p� konstrukt�ren
     */
    public boolean onlyRead = true;
    /**
     *kj�res hvis brukeren tastet inn filnavn som skal leses
     */
    public void command(String read){	
	File input = new File(read);
	readFile(input);
    }

    /**
     *filnavn leses og lagres til den oppgitte filnavnet
     */
    public void command(String read, String write){
	onlyRead = false;
	File input = new File(read);
	File output = new File(write);
	readFile(input);
	board.beginSolving();
	writeFile(output);
    } 

	    
    /**
     *leser fra fil og filer konverterer char til int og lagres p� brettet
     */
    public void readFile(File file) {
	int size = 0;
	int height = 0;
	int width = 0;
	char[] singleArray;
	int[][] doubleArray;
	String read = "";

	try {
	    Scanner sc = new Scanner(file);
	    size = Integer.parseInt(sc.next());
	    width = Integer.parseInt(sc.next());
	    height = Integer.parseInt(sc.next());
	    dimensjon = size;
	    vertikalAntall = width;
	    horisontalAntall = height;
	    sc.nextLine();
	    doubleArray = new int[size][size];
	    for(int i = 0; i < size; i++) {
		singleArray = sc.nextLine().toCharArray();
		for(int j = 0; j < size; j++) {	    
		    doubleArray[i][j] = Character.digit(singleArray[j], 36);
		}
	    }
	    board = new Board(size, height, width, doubleArray);
	    if(onlyRead) {	
		brett = new JTextField[size][size];
		setPreferredSize(new Dimension(size * RUTE_STRELSE, size * RUTE_STRELSE + PLASS_TOPP));
		setTitle("Sudoku " + size + " x " + size );
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		JPanel knappePanel = lagKnapper();
		JPanel brettPanel = lagBrettet(doubleArray);
		
		getContentPane().add(knappePanel, BorderLayout.NORTH);
		getContentPane().add(brettPanel, BorderLayout.CENTER);
		pack();
		setVisible(true);
	    }
	} catch (FileNotFoundException e){
	    System.err.println("An error occured when trying to open the file ");
	    
	    
	}
    }

    /**
     *konverterer tall til char og skriver til den oppgitte fil-navnet
     *
     */
    public void writeFile(File file){
	try {
	    FileWriter fileOut = new FileWriter(file);
	    ArrayList<int[][]> list = board.gets();
	    for(int i = 0; i < list.size(); i++){
		fileOut.write((i+1) + ": ");
		for(int j = 0; j < dimensjon; j++) {		    
		    for(int k = 0; k < dimensjon; k++) {
			fileOut.write(Character.toUpperCase(Character.forDigit(list.get(i)[j][k], 36)));
		    }
		    fileOut.write("// ");
		}
		fileOut.write(System.getProperty("line.separator"));
	    }
	    fileOut.close();
	    System.out.println("L�sningene er lagret i filen");
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}
    }
    

    /**
     *�pner og leser txt-filene. bruker hjelpeklassene for � filtrere filene osv.
     *
     */
    public void fileChooser(){
	int size = 0;
	int height = 0;
	int width = 0;
	char[] singleArray;
	int[][] doubleArray;
	String read = "";
	JFileChooser file = new JFileChooser();
	FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT files", "txt");
	file.setFileFilter(filter);

	int reValue = file.showOpenDialog(this);
	if(reValue == JFileChooser.CANCEL_OPTION) {
	    return;
	} else if(reValue == JFileChooser.APPROVE_OPTION) {
	    System.out.println("You chose to open this file: " + file.getSelectedFile().getName());
	    try {
		Scanner sc = new Scanner(file.getSelectedFile());
		size = Integer.parseInt(sc.next());
		width = Integer.parseInt(sc.next());
		height = Integer.parseInt(sc.next());
		dimensjon = size;
		vertikalAntall = width;
		horisontalAntall = height;
		sc.nextLine();
		doubleArray = new int[size][size];
		for(int i = 0; i < size; i++) {
		    singleArray = sc.nextLine().toCharArray();
		    for(int j = 0; j < size; j++) {	       
			    doubleArray[i][j] = Character.digit(singleArray[j], 36);		       
		    }
		}
		brett = new JTextField[size][size];
		setPreferredSize(new Dimension(size * RUTE_STRELSE, size * RUTE_STRELSE + PLASS_TOPP));
		setTitle("Sudoku " + size + " x " + size );
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		board = new Board(size, height, width, doubleArray);

		JPanel knappePanel = lagKnapper();
		JPanel brettPanel = lagBrettet(doubleArray);
	       
	// 	finnSvarKnapp.setEnabled(true); 
		getContentPane().remove(brettPanel);
		getContentPane().add(knappePanel, BorderLayout.NORTH);
		getContentPane().add(brettPanel, BorderLayout.CENTER);		
		pack();
		setVisible(true);	
	    } catch (FileNotFoundException e){
		System.err.println("An error occured when trying to open the file " + file.getSelectedFile().getName());
		
		
	    }
	    
	
	}
    }
}
import java.util.ArrayList;

public class Square{
    public int value;
    public Column columnObject;
    public Row rowObject;
    public Box boxObject;
    public Board boardObject;
    public boolean occupied;
    public Square nextSquare;
    public int dimension;

    /**
     *Konstrukt�r
     */
    Square(int boxSize, int a, Column b, Row c, Box d, Board e){
    	this.value = a;
	this.columnObject = b;
	this.rowObject = c;
	this.boxObject = d;
	this.boardObject = e;
	this.occupied = (value != -1);
	this.dimension = boxSize;

	}	
    
    /**
     *setter neste peker
     */
    public void setNextSquare(Square s){
	nextSquare = s;
    }

    /**
     *setter et hvilken som helst nummer i ruten, og n�r den klarer � sette et
     * nummer kaller den p� seg selv igjen. n�r den nest epeker er null vil det
     * si at da befinner vi i den siste ruten og da lagres brettet til bufferet
     */
    public void setNumber(){
	if (occupied) {
	    if (nextSquare == null) {
		boardObject.addSolutions();
	    } else {
		nextSquare.setNumber();
	    }
	    return;
	}

	for(int i = 1; i <= dimension; i++){	 
	    if(valid(i)) {
		value = i;
		if (nextSquare == null) {
		    boardObject.addSolutions();
		} else {
		    nextSquare.setNumber();
		}
		value = -1;
	    }
	}
    }

    /**
     *sjekker om verdien finnes allerede i brettet
     */
    private boolean valid(int i) {
	return rowObject.valid(i) && columnObject.valid(i) && boxObject.valid(i);
    }
    
}	

    
import java.util.ArrayList;

public class Board{
    public Square[][] table;
    public Row[] row;
    public Box[] box;
    public Column[] column;
    public Square tempNextSquare;
    public SudokuGUI gui;
    public ArrayList<int[][]> answers;
    public SudokuBuffer buffer = new SudokuBuffer();
    public int dimension;
    
    Board(int boxSize, int boxWidth, int boxHeight , int[][] tableContent) {	
	table = new Square[boxSize][boxSize];
	row = new Row[boxSize];
	column = new Column[boxSize];
	box = new Box[boxSize];
	answers = new ArrayList<int[][]>();
// 	this.gui = new SudokuGUI(boxSize, boxHeight, boxWidth);
	createObjects(boxSize, boxWidth, boxHeight, tableContent);
    }

    /**
     *lager rader for enhver i, og kolonner for enhver j, boxene blir opprettet i starten av boksene, man kan finne dens posisjon ved � ta b�de i modulus av bredden av boksen og j modulus med 
     *hoyden og resten m� v�re lik 0. testet det p� et ark med 3x3 og 4x4 og 5x5,og det ser ut til � funke.  lager nestepekere for alle rutene med en gang f�r man pr�ver � l�se brettet.
     **/
    public void createObjects(int size, int width, int height, int[][] position){
	int temp = 0;
	
	for(int i = 0; i < table.length; i++){
	    row[i] = new Row(size);                 
	    for(int j = 0; j < table[i].length; j++) {
		if(i == 0) {
		    column[j] = new Column(size); 
		}		
		//System.out.printf("%d %% %d = %d, %d %% %d = %d%n", i, height, i % height, j, width, j % width);
		if((i % height == 0) && (j % width == 0 )) {
		    box[temp++] = new Box(size);
		    //System.out.println(box[temp - 1]);
		}
		//System.out.println(whichBox(i,j,width,height));
	
		Square s = new Square(size, position[i][j], column[j], row[i], box[whichBox(i, j, width, height)], this);
		if(!(i == 0 && j == 0)){ //kj�res etter at den f�rste neste peker har blitt fullf�rt
		    tempNextSquare.setNextSquare(s);
		}
		tempNextSquare = s;
		table[i][j] = s;
		row[i].insert(s);
		column[j].insert(s);
		box[whichBox(i, j, width, height)].insert(s);		    
	    }
	}
    }
	
	
    /**
     *Fant lignende algoritmer p� nettet som skal finne ut hvilken boks man befinner seg i, ganske stygg. 
     *returnerer eksakt boks man befinner seg i.
     */
    public int whichBox(int row, int column, int width, int height){
	return (row / height) * height + column / width;
    }

    /**
     *lager et nytt 2D brett og lagrer det i bufferet siden det er ikke et krav 
     * � huske rader, kolonner og bokser rutenene var i.
     */
    public void addSolutions() {
	int[][] temp = new int[table.length][table.length];
	for(int i = 0; i < table.length; i++){
	    for(int j = 0; j < table[i].length; j++) {
		temp[i][j] = table[i][j].value;
	    }
	}
	buffer.add(temp);
	

    }
    /**
     *starter � l�se brettet fra den f�rste ruten,
     */
    public void beginSolving() {
	System.out.println("Solving....");
	table[0][0].setNumber();
    }

    /**
     * henter alle l�sningene som er lagret i arrayen i bufferet
     */
    public ArrayList<int[][]> gets(){
	return buffer.get();
    }
}import java.util.ArrayList;

public class Box{

    public ArrayList<Square> position;
    
    Box(int size){
	position = new ArrayList<Square>(size);
    }
    /**
     *sjekker om tallet finnes i boksen
     */
    public boolean valid(int values){
	for(int i = 0; i < position.size(); i++){
	    if(position.get(i).value == values) {
		return false;
	    }
	}
	return true;
    }
    /**
     *legget til tallet i boksen
     */
    public boolean insert(Square s){
	return position.add(s);
    }
    
}import java.util.ArrayList;

public class Column{
    public ArrayList<Square> position;
    
    Column(int size) {
	position = new ArrayList<Square>(size);
    }
    /**
     *sjekker om tallet finnes i kolonnen
     */
    public boolean valid(int values){
	for(int i = 0; i < position.size(); i++){
	    if(position.get(i).value == values) {
		return false;
	    }
	}
	return true;
    }
    /**
     *legger til tallet i kolonnen
     */
    public boolean insert(Square s){
	return position.add(s);
    }


}
import java.util.ArrayList;

public class Row{
    public ArrayList<Square> position;

    Row(int size){
	position = new ArrayList<Square>(size);
    }
    /**
     *sjekker om talle fins allerede i raden
     */
    public boolean valid(int values){
	for(int i = 0; i < position.size(); i++){
	    if(position.get(i).value == values) {
		return false;
	    }
	}
	return true;
    }
    /**
     *legger til tallet i raden
     */
    public boolean insert(Square s){
	return position.add(s);
    }
}import java.lang.UnsupportedOperationException;
import java.util.ArrayList;
import java.util.Iterator;

public class SudokuBuffer {
    public ArrayList<int[][]> solutions = new ArrayList<int[][]>(500);
    public int positionInArray = 0;
    public int answers = 0;
    public int counter = 0;
 //    public Iterator<int[][]> it = solutions.iterator();
    
    /**
     *Lagrer den ferdigl�ste brettet i en array p� 500
     *Variablene er ganske selvforklart
     *
     */
    public void add(int[][] inn){
	
	if(inn == null) {
	    throw new NullPointerException();
	} else if(solutions.size() < 500){
	    solutions.add(inn);
	    answers++;
	} else {
	    answers++;	  
	}
    }
    
    /**
     *henter solutions-arrayen
     */
    public ArrayList<int[][]> get() {
	return solutions;
    }
   //  /**
//      *henter ut den neste l�sningen
//      */
//     public int[][] nextSolution() {
// 	int[][] temp = null;
// 	if(it.hasNext()){
// 	    temp = it.next();
// 	    return temp;
// 	} else {
// 	    return temp;
// 	}		
//     }
	
    /**
     *returnerer antall l�sninger som er l�st og ikke selve l�sningen, 
    
     */
    public int getTotalSolutions(){
	return answers;
    }


}



        

    
 
//import java.lang.UnsupportedOperationException;

public class Oblig3 {
    public static void main(String[] args) {
	String read, write = "";
	int words = args.length;
	SudokuGUI s = new SudokuGUI();
	
	if(words == 0) {
	    new SudokuGUI(words);
	} else if(words <= 1 || words < 3){
	    if(words == 1){
		read = args[0];
		new SudokuGUI().command(read);
	    } else {
		read = args[0];
		write = args[1];
		new SudokuGUI().command(read, write);
	    }
	} else {
	    System.out.println("Something went wrong, please make sure to have the right input(s)");
	}
    }
}


// 	int[][] a = new int[6][6];
// 	for(int i = 0; i < a.length; i++){
// 	    for(int j = 0 ; j < a[i].length; j++){
// 		a[i][j] = 0;
// 	    }
// 	}
// 	a[0][2] = 1;
// 	a[0][5] = 3;
// 	a[2][4] = 2;
// 	a[3][0] = 2;
// 	a[3][1] = 6;
// 	a[4][3] = 3;
// 	a[5][0] = 3;
// 	a[5][3] = 1;
// 	a[5][5] = 2;
// 	Board b = new Board(6, 3, 2, a);
// 	b.beginSolving();
// 	System.out.println(b.get() + " Way to solve this board");
// 	for(int i = 0; i < a.length; i++){
// 	    for(int j = 0 ; j < a[i].length; j++){
// 		System.out.print(a[i][j]);
		
// 	    }
// 	    System.out.println();
// 	}
//     }
// }
