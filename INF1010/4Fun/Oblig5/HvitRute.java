/*
 HvitRute og SortRute er subklasser av Rute. Disse må implementere char tilTegn() som deklareres i Rute.
 */

class HvitRute extends Rute {

    char tilTegn(){
	return ' ';
    }
}
