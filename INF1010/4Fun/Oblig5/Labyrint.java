/*
Labyrint har et 2-dimensjonalt Rute-array med referanser til alle rutene i labyrinten og instansvariable som lagrer antall rader og kolonner i labyrinten.
I tillegg skal metoden String toString() fra Object overstyres (override) slik at labyrinten kan skrives ut i terminalen. 
(Dette er nyttig for debugging av programmet underveis.) Du kan bruke den samme representasjonen som i filformatet, men det kan være greit å representere hvite ruter ved et mellomrom istedenfor et punktum.
 */
import java.util.*;
import java.io.*;





class Labyrint {
    private static final boolean debug = true;
    Rute[][] labyrint;
    static int rader = 0;
    static int kolonner = 0;
    // int id = 666;

    private Rute getRute(int x, int y){
	if(x >= 0 && x < rader && y >= 0 && y < kolonner) return labyrint[x][y];
	else return null;
    }

    /*
      Skaper referanser mellom nabo ruter
     */
    private void kobleLabyrinten(){
	Rute temp;
	System.out.println("Tilkobling av ruter i Labyrint Strukturen");
	for(int x = 0; x < rader; x++){
	    for(int y = 0; y < kolonner; y++){
		temp = labyrint[x][y];
		temp.lab = this;
		temp.rad = x;
		temp.kolonne = y;
		temp.syd = getRute(x-1, y);
		temp.nord = getRute(x+1, y);
		temp.vest = getRute(x, y-1);
		temp.oest = getRute(x, y+1);
	    }
	    // System.out.println();
	}
	
	//	if(debug){
	    System.out.println("Testing av tilkoblingene");
	    // System.out.println(labyrint[0][0].lab.id);
	    
	    //	}
    }
    

    private Labyrint(String[] lab){
	if(debug) System.out.println("We are inside the constructor");
	Rute r;
	labyrint = new Rute[rader][kolonner];
	char rute;	
	for(int i = 0; i < rader; i++){
	    
	    
	    for(int y = 0; y < kolonner; y++){
		rute = lab[i].charAt(y);
	        if(rute == '#'){
		    if(debug) System.out.print("#");
		    r = new SortRute();
		    labyrint[i][y] = r;
		    
		} else {
		    if(debug){ 
			if(i == 0 || i == rader-1 || y == 0 || y == kolonner-1) System.out.print("A");
			else System.out.print(" ");
		    }
		    //Hvite ruter som ligger på kanter er åpninger
		    if(i == 0 || i == rader-1 || y == 0 || y == kolonner-1){
			r = new Aapning();
			labyrint[i][y] = r;
		    } else {
			r = new HvitRute();
			labyrint[i][y] = r;
		    }
		    
		    //Sjekk om denne hvite ruten ligger i kanten, da er den en åpning
		    
		}
	    
	    }
	    if(debug) System.out.println();
	}

	if(debug){
	    System.out.println("Testing Labyrint Strukturen");
	    for(int i = 0; i < rader; i++){
		for(int y = 0; y < kolonner; y++){
		    System.out.print(labyrint[i][y].tilTegn());
		}
		System.out.println();
	    }
	}
	
	//lager referanser for ruter til deres nabo ruter
	kobleLabyrinten();
    }
    

    static Labyrint lesFraFil(File fil) throws FileNotFoundException, IOException{
	Scanner s = new Scanner(new BufferedReader(new FileReader(fil)));
	String str = "";
	int counter = 0;
	
	rader = Integer.parseInt(s.next());
	kolonner = Integer.parseInt(s.next());
	// // // int i = Integer.parseInt(s.next());
	// int y = Integer.parseInt(s.next());

	String[] innhold = new String[rader];
	
	while(s.hasNext()){
	    str = s.next();
	    innhold[counter] = str;
	    counter++;
	    
	    // System.out.println(str);
	}
	
	if(debug){
	    System.out.println("Testing");
	    for(int i = 0; i < rader; i++){
		System.out.println(innhold[i]);	    
	    }
	}
	
	
	Labyrint lab = new Labyrint(innhold);
	

	return lab;	
    }
    

    
    public static void main(String[] args) throws IOException{
	if(debug) System.out.println("Starting in Labyrint");
	File f = new File(args[0]);
	Labyrint lab = lesFraFil(f);
	
	
    }
}
