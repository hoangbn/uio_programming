
// Rute skal være abstrakt. Klassen skal ha instansvariable som lagrer koordinatene (rad og kolonne), 
//samt en referanse til labyrinten den er en del av. I tillegg skal klassen ha instansvariable for 
// nord/syd/vest/øst som skal peke på eventuelle naboruter i de retningene.
// Rute skal ha en abstakt metode char tilTegn() som returnerer rutens tegnrepresentasjon i en fil som beskrevet i Filformat.



abstract class Rute {
    Labyrint lab;
    int rad, kolonne;
    Rute nord, syd, oest, vest;
    
    abstract char tilTegn();

 
    
}
