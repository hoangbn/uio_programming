public class Oblig1x {
    
    public static void main(String[] args) throws Exception {

	if (args.length == 0) {
	    System.out.println("INF1010 - Oblig 1");
			System.out.println("Usage:");
			System.out.println("Run the tests: java Oblig1 test");
			System.out.println("Run the shell: java Oblig1 shell");
	}
	else if (args[0].equals("test")) {

	    System.out.println("Comment in the test code and compile with the test file to run the tests.");
	    
	    PersonList personlist = new PersonList();			
	    Oblig1Tests tester = new Oblig1Tests(personlist);
	    tester.runTests();
	    
	}
	else if (args[0].equals("shell")) {
	    System.out.print("");
	    
	    
	}
    }
}

class Person {
    private String name, phone;
    Person next, previous;
    PersonList friends;
    
    Person(){
	System.out.println("No input given. Try again");
    }
    
    Person(String name){
	this.name = name;
    }
    
    Person(String name, String phone){
	this.name = name;
	this.phone = phone;
    }
        /**
         * Get the name of the person
         * @return   name of the person
         */
    public String getName(){
	return name;
    }
    
    /**
     * Get the phone number of the person
     * @return    phone number to the person
     */
    public String getPhone(){
	if (phone == null) phone = "none";
	return phone;
    }
    
    /**
     * Get all the persons in the persons friend list.
     * @return    friends of this person
     */
    public Person [] getFriends(){
	Friend f = friends.firstFriend;
	int counter = 0;
	while (f != null){
	    counter++;
	    f = f.next;
	}
	Person[] friendArray = new Person[counter];
	counter = 0;
	while (f != null){
	    friendArray[counter] = f.person;
	    counter++;
	    f = f.next;
	}
	System.out.println(counter);
	return friendArray;
    }
    
}

class PersonList {
    Person first, last;
    Friend firstFriend, lastFriend;
    
    PersonList(){
    }
    
    /**
         * Add a new person to the list
         * @param name   name of the person
         * @param phone  phone number of the person
         * @return       true if person was added successfully.
         *               false if the person is already in the list
         */
    public boolean addPerson(String name, String phone){
	if (personExists(name)){
	    System.out.println("Name exists");
	    return false;
	}
	Person p = new Person(name, phone);
	if (first == null){
	    first = p;
	    last = p;
	    System.out.println("Person added");
	    return true;
	}
	last.next = p;
	p.previous = last;
	last = p;
	System.out.println("Person added");
	return true;
		}
    
    /**
     * Remove a person from the list.
     * @param name   name of the person
     * @return       true if person was successfully removed
     *               false if the person doesn't exist.
         */
    public boolean removePerson(String name){
	if (!personExists(name)) return false;
	Person remove = getPerson(name);
	if (name.equals(first.getName()) && name.equals(last.getName())) {
	    first = null;
	    last = null;
	    return true;
	}
	if (name.equals(first.getName())){
		first = first.next;
		first.previous = null;
	}
	else (remove.previous).next = remove.next;
	
	if (name.equals(last.getName())){
	    last = last.previous;
	    last.next = null;
	}
	else (remove.next).previous = remove.previous;
	
	removeFromFriends(name);
	return true;
    }
    
        /**
         * Add a person as a friend to another person
         * @param name        name of the person to gain a friend
         * @param friendName  name of the new friend
         * @return            true if successfully added person as friend
         *                    false if friendName is already a friend
         */
    public boolean addFriend(String name, String friendName){
	if (!personExists(name) || !personExists(friendName)) return false; //MAYBE REMOVE (SEE WHAT TESTER SAYS)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	PersonList l = getPerson(name).friends;
	Friend f = l.firstFriend;
	while (f != null){
	    if (f.name.equals(friendName)) return false;
	    f = f.next;
	}
	Friend n = new Friend(getPerson(friendName));
	
	if (l.firstFriend == null){
	    l.firstFriend = n;
			l.lastFriend = n;
			return true;
	}
	
	l.lastFriend.next = n;
	n.previous = l.lastFriend;
	l.lastFriend = n;
	return true;
    }
    
    /**
     * Remove a friend from a person
;     * @param name         name of the person to lose a friend
     * @param friendName   name of the friend to be removed
         * @return             true if successfully removd person as friend
         *                     false if friendName isn't a friend
         */
    public boolean removeFriend(String name, String friendName){
	if (!personExists(name) || !personExists(friendName)) return false; //  !!!!!!!!!!!!!!!!!!!!!!!!!!
	PersonList l = getPerson(name).friends;
	Friend f = l.firstFriend;
	while (f != null) {
	    if (f.name.equals(friendName)){			
		
		
		if (friendName.equals(l.firstFriend.name) && friendName.equals(l.lastFriend.name)) {
		    l.firstFriend = null;
		    l.lastFriend = null;
		    return true;
		}
		if (f.name.equals(l.firstFriend.name)) {
		    l.firstFriend = l.firstFriend.next;
		    l.firstFriend.previous = null;
		}
		else (f.previous).next = f.next;
		
		if (name.equals(lastFriend.name)) {
		    l.lastFriend = l.lastFriend.previous;
		    l.lastFriend.next = null;
		}
		else (f.next).previous = f.previous;
		
		return true;
	    }
	    f = f.next;
	}
	return false;
		}
    
    
    /**
     * Get the current number of persons in the list.
     * @return     number of persons
     */
    public int getSize(){
	Person check = first;
	int counter = 0;
	while (check != null){
	    counter++;
	    check = check.next;
	}
	return counter;
    }
    
        /**
         * Get an array of persons currently in the list.
         * @return     array of persons
         */
    public Person[] getPersons(){
	Person check = first;
	int counter = 0;
	Person persons[] = new Person[getSize()];
	while (check != null){
	    persons[counter] = check;
	    counter++;
	    check = check.next;
	}
	return persons;
	
    }
    
    /**
     * Get a person.
         * @param name   name of the person to get
         * @return       person that was requested
         */
    public Person getPerson(String name){
	Person check = first;
	while (check != null){
	    if (check.getName().equals(name)) return check;
	    check = check.next;
	}
	return null;
    }
    
    private boolean personExists (String name) {
	if (first == null) return false;
	Person check = first;
	while (check != null){
	    if (check.getName().equals(name)) return true;
	    check = check.next;
	}
	return false;
		}
    
    private void removeFromFriends(String name){
	if (first == null) return;
	Person check = first;
	while (check != null){
	    PersonList l = check.friends;
	    Friend friend = l.firstFriend;
	    while (friend != null){
		if (friend.name.equals(name)) {
		    if (name.equals(l.firstFriend.name) && name.equals(l.lastFriend.name)) {
			l.firstFriend = null;
			l.lastFriend = null;
			return;
		    }
		    if (name.equals(l.firstFriend.name)){
			l.firstFriend = l.firstFriend.next;
			l.firstFriend.previous = null;							
							}
		    else friend.previous.next = friend.next;
		    
		    if (name.equals(l.lastFriend.name)) {
			l.lastFriend = l.lastFriend.previous;
			l.lastFriend.next = null;
		    }
		    else friend.next.previous = friend.previous;
		    
		    
		}
		friend = friend.next;
	    }
	    check = check.next;	
	}
    }
}

class Friend {
    String name; // I use name since they are unique and can be used as param to the most methods. No private requested by assignment text
    Friend next, previous;
    Person person;
    
    Friend(Person p){
	this.name = p.getName();
	this.person = p;
    }
}
