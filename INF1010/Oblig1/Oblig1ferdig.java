import java.io.File;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;
 
/**
 * This class contains a few methods you can use to 
 * read from file and terminal, and write to file.
 * Instead of throwing exceptions, the methods will 
 * print an error message and return null.
 */
class Shell {
 	
	PersonList personlist = new PersonList();
	
	/**
	 * Create a java.util.Scanner that reads from the keyboard.
	 */
	public static Scanner terminalReader() {
		return new Scanner(System.in);
	}
 
 
	/**
	 * Create a java.util.Scanner that reads from the given file.
	 * @param filename	 file to read from
	 * @return	scanner object if file was found
	 *			null if file wasn't found
	 */
	public static Scanner fileReader(String filOblig1) {
		try {
			return new Scanner(new File(filOblig1));
		}
		catch (FileNotFoundException e) {
			System.err.println(
			"An error occured when trying to open the file "
			+ new File(filOblig1).getAbsolutePath());
			e.printStackTrace();
		}
		return null;
	}

	void help() {
		System.out.println("\n\tWelcome to Oblig 1!\n");
		System.out.println("\tYou are now in Shell mode, to run Test mode");
		System.out.println("\texit the program and run \"java Oblig1 test\"");
		System.out.println("\tThese are the valid commands for Oblig1:\n");
		System.out.println("\tadd <name> <number>");
		System.out.println("\tremove <name>");
		System.out.println("\tfriends <name> <name-friend> ");
		System.out.println("\tunfriends <name> <non-friend> ");
		System.out.println("\tshow <name> ");
		System.out.println("\tlistall ");
		System.out.println("\ttofile <filename> ");
		System.out.println("\tfromfile <filename> ");
		System.out.println("\thelp");
		System.out.println("\texit\n\n");
	}
	
	void kommandoLøkke () {
	
		Scanner scan = new Scanner(System.in);
		boolean run = true;

		help();
		
		while (run != false) {

			System.out.print("shell> ");
			String line = scan.nextLine();
			String[] words = line.split(" ");
			if (words[0].equals("hello")) {
				System.out.println("world!");
			}
			if (words[0].equals("add")) {
				if (words.length <= 2) {
					System.out.println("\tADD <NAME> <NUMBER>");
				}
				else {
					personlist.addPerson(words[1], words[2]);
				}
			}
			if (words[0].equals("remove")) {
				if (words.length <= 1) {
					System.out.println("\tREMOVE <NAME>");
				}
				else {
					personlist.removePerson(words[1]);
				}
			}
			if (words[0].equals("friends")) {
				if (words.length <= 1) {
					System.out.println("\tFRIENDS <NAME> <NAME>");
				}
				if (words.length <= 2) {
					personlist.addFriend(words[1], "no number");
				}
				else {
					personlist.addFriend(words[1], words[2]);
				}
			}
			if (words[0].equals("unfriends")) {
				if (words.length <= 2) {
					System.out.println("\tUNFRIENDS <NAME> <NAME>");
				}
				else {
					personlist.removeFriend(words[1], words[2]);
				}
			}
			if (words[0].equals("show")) {
				if (words.length <= 1) {
					System.out.println("\tSHOW <NAME>");
				}
				else {
					Person p = personlist.getPerson(words[1]);
					if (p == null) {
						System.out.println("\n\tERROR "+words[1]+" does not exist\n");
					}
					else {
						System.out.println("\n\tName: "+p.getName());
						System.out.println("\tPhone: "+p.getPhone());
						System.out.print("\tFriends: ");
						Person[] friends = p.getFriends();
						for(Person friend: friends) {
							int hit = 0;
							if (hit != 0) {
								System.out.print(", ");
							}
							System.out.print(friend.getName());
							hit++;
						}
						System.out.println("\n");
					}
				}
			}
			if (words[0].equals("listall")) {
				System.out.println("\tName\t\tPhone");
				Person[] all = personlist.getPersons();
				for(Person p : all) { 
					System.out.println("\t" + p.getName() + "\t" + p.getPhone());
				}
			}
			if (words[0].equals("tofile")) {
				if (words.length <= 1) {
					System.out.println("TOFILE <FILENAME>");
				}
				else {
					String fname;
					String output;
					boolean first = true;
					String filename = words[1]+".txt";
					PrintWriter fout = fileWriter(filename);
					Person[] all = personlist.getPersons();
					for(Person p : all) {
						output = "add "+ p.getName() + " " + p.getPhone();
						fout.println(output);
					}
					for(Person p : all) {
						Person[] friends = p.getFriends();
						for(Person friend: friends) {
							fname = friend.getName();
							output = "friends " + p.getName() + " " + fname;
							fout.println(output);
						}
					}
					fout.close();
					System.out.println("\tSAVED: "+words[1]+".txt");
				}
			}
			if (words[0].equals("fromfile")) {
				if (words.length <= 1) {
					System.out.println("FROMFILE <FILENAME>");
				}
				else {
					try {
						String fromfile = words[1]+".txt";
						FileReader reader = new FileReader(fromfile);
						String sline;
						while (reader.hasNextLine()) { 
						    String scannedline = reader.NextLine(); 
							String[] read = scannedline.split(" ");
							if (read[0].equals("add")) {
								personlist.addPerson(read[1], read[2]);
								System.out.println(read[1]+" added");
							}
							if (read[0].equals("friends")) {
								personlist.addFriend(read[1], read[2]);
								System.out.println(read[1]+" friends with "+read[2]);
							}
						}
						reader.close();
					}
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			if (words[0].equals("help")) {
				help();
			}
			if ( words[0].equals("exit") ) {
				run = false;			
			}
		}
	}
	
	/**
	 * Create a java.io.PrintWriter that writes to the given file.
	 * @param filename	 the file to write to
	 * @return	PrintWriter that writes to the file
	 *			null if an error occurred
	 */
	public static PrintWriter fileWriter(String filOblig1) {
		try {
			return new PrintWriter(filOblig1);
		} catch (FileNotFoundException e) {
			System.err.println(
			"An error occured when trying to open the file "
			+ new File(filOblig1).getAbsolutePath());
			e.printStackTrace();
		}
		return null;
	}
}



class Person {
	private String name;
	private String phone;	 
	PersonList friends;

	Person (String name, String phone){
		this.name = name;
		this.phone = phone;
		
		if(!name.equals("HEAD") && !name.equals("TAIL")) {
			friends = new PersonList ();
		}
	}
	public String getName (){
		return name;
	}
	public String getPhone (){
		return phone;
	}
	public Person [] getFriends (){
		return friends.getPersons();
	}
	public boolean isFriends (Person friend){
		return friend == friends.getPerson(friend.getName()) && friend != null;
	}
}



class PersonList	{
	class Element {
		Person me;
		Element next;
	
		Element (Person me){
			this.me = me;
		}
		 
	}
		
	Element first;
	Element head;
	Element tail;
	int size = 0;
			
	PersonList() { // Konstruktør for at person listen aldri skal bli tom
		head = new Element(new Person("HEAD", null)); 
		tail = new Element(new Person("TAIL", null));
		head.next = tail;
		first = head;
	}
		
	boolean addPerson (Person pe) {
		Element p = new Element(pe);
		if (first == head) { // Spesialtilfelle, kun når listen er tom vil det bli lagt inn en ny person her
			first = p;
			head.next = p;
			p.next = tail;
			size++;
			System.out.println("\t"+p+" Added");
			return true;
		}
		else{
			Element temp = first;
			while (temp != tail) { // Går gjennom hele listen
				if (temp.me == pe) { // dersom navnet er likt som et annet navn i listen, vil den returnere false
					return false;
					System.out.println("\t"+p+" Not added");
				}
				if (temp.next == tail){ // dersom den kommer på slutten og den har ikke navnet fra før, kan det her legges inn en ny person
					temp.next = p;
					p.next = tail;
					size++;
					System.out.println("\t"+p+" Added");
					return true;
				}
				temp = temp.next;
			}
		}
		return true;
	}
		
	public boolean removePerson (Person pe){
		Element p = first;
		Element forrige = head;
		if (p == head) { // Dersom listen er tom, vil det her ikke bli fjernet noen
			return false;
			System.out.println("\t"+p+" Not Removed");
		}
		else {
			while (p != tail) { // Går gjennom hele listen	
			if (p.me == pe) { // dersom navnet er likt som et annet navn i listen, vil den returnere true og fjerne denne persone
				forrige.next = p.next;			
				if (p == first){ 
					first = p.next;// First vil stå på utsiden og ikke forsvinne, her lar jeg den være inne i listen slik at jeg kan bruke den			
					if (tail == first){ // dersom first peker på den siste må ikke first forsvinne
						first = head;
					}
				}
				size--;
				System.out.println("\t"+p+" Removed");
				return true;
			}
			forrige = forrige.next;
			p = p.next; // her beveger p seg til neste
			}
			System.out.println("\t"+p+" Not Removed");
			return false; 
		}
	}
		
	// Sette inn en person
	public boolean addPerson (String name, String phone){
		Person pe = new Person (name, phone);
		Element p = new Element(pe);
		if (first == head) { // Spesialtilfelle, kun når listen er tom vil det bli lagt inn en ny person her
			first = p;
			head.next = p;
			p.next = tail;
			size++;
			System.out.println("\t"+pe+" Added");
			return true;
		}
		else {
		    Element temp = first;
		    while (temp != tail) { // Går gjennom hele listen
			if (name.equals (temp.me.getName())) { // dersom navnet er likt som et annet navn i listen, vil den returnere false
			    System.out.println("\t"+pe+" Not Added");
			    return false;
			}
			if (temp.next == tail){ // dersom den kommer på slutten og den har ikke navnet fra før, kan det her legges inn en ny person
			    temp.next = p;
			    p.next = tail;
			    size++;
			    System.out.println("\t"+pe+" Added");
			    return true;
			}
			temp = temp.next;
			}		 
		}
		return true;
	}
		
	public void removePersonF (Person pe){
		Element p = first;
		while (p != tail){
			removeFriend(p.me.getName(), pe.getName());
			p = p.next;
		}
	}
		
	// Fjerne en person
	public boolean removePerson (String name){
		Element p = first;
		Element forrige = head;
		if (p == head) { // Dersom listen er tom, vil det her ikke bli fjernet noen
			System.out.println("\t"+name+" Not Removed");
			return false;
		} 
		else {
			while (p != tail) { // Går gjennom hele listen
			if (name.equals (p.me.getName())) { // dersom navnet er likt som et annet navn i listen, vil den returnere true og fjerne denne persone
				Person fr = p.me;
				removePersonF(fr);
				forrige.next = p.next;
				if (p == first){ 
					first = p.next;// First vil stå på utsiden og ikke forsvinne, her lar jeg den være inne i listen slik at jeg kan bruke den
					if (tail == first){ // dersom first peker på den siste må ikke first forsvinne
						first = head;
					}
				}
				size--;	
				System.out.println("\t"+name+" Removed");
				return true;
			}	
			forrige = forrige.next;
			p = p.next; // her beveger p seg til neste
		}
		System.out.println("\t"+name+" Not Removed");
		return false;
		}
	}


	// Sette inn et objekt i vennelisten til en person
	public boolean addFriend (String name, String friendName) {
		Person p = getPerson (name);
		Person f = getPerson (friendName);
		if (f == null || p == null){
			System.out.println("\t"+name+" "+friendName+" Failed");
			return false;
		}
		else if(p.isFriends(f)) {
			System.out.println("\t"+name+" "+friendName+" Failed");
			return false;
		}
		p.friends.addPerson (f);			
		System.out.println("\t"+name+" "+friendName+" Friended");
		return true; 
	}

	public boolean removeFriend (String name, String friendName) {
		Person p = getPerson (name);
		Person f = getPerson (friendName);
		if (f == null || p == null){
			System.out.println("\t"+name+" "+friendName+" Failed");
			return false;
		}
		else if(p.isFriends(f)){
			p.friends.removePerson (f);
			System.out.println("\t"+name+" "+friendName+" Unfriended");
			return true;
		} 
		else {
			System.out.println("\t"+name+" "+friendName+" Failed");
			return false;
		}		
	}
			
	// Finne riktig antall personer i listen
	public int getSize() {
		return size;
	}
		
	// lage et array for antall personer i listen
	public Person [] getPersons() {
		int count =	0;
		Element p = first;
		if(first == head) {
			return new Person[0];
		}
		Person persons [] = new Person[getSize()];
		while (p != tail){
			persons[count++] = p.me;
			p = p.next;
		}
		return persons;
	}
		
	/* Vise informasjon om en person navn, telefonnummer, antall venner, samt en oversikt over alle vennene*/
	public Person getPerson (String name) {
		Element p = first;
		boolean found = false;
		while (p != tail) { // Går gjennom hele listen
			if (p.me.getName().equals (name)){
				return p.me;
			}
			p = p.next; // her beveger p seg til neste
			if (found){
				return null;
			}
		}
		return null;
	}
}
		
public class Oblig1ferdig {
	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("INF1010 - Oblig 1");
			System.out.println("Usage:");
			System.out.println("Run the tests: java Oblig1 test");
			System.out.println("Run the shell: java Oblig1 shell");
		}
		else if (args[0].equals("test")) {
			System.out.println("Comment in the test code and compile with the test file to run the tests.");
			PersonList personlist = new PersonList();			
			Oblig1Tests tester = new Oblig1Tests(personlist);
			tester.runTests();
		}
		else if (args[0].equals("shell")) {
			Shell ss = new Shell();
			ss.kommandoLøkke();
		}
	}
}