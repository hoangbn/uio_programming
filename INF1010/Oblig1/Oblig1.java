/**
 * Precode for Oblig1 - INF1010 v2010.
 */

// Litt dumt av meg � skrive p� to spr�k men programmet fungerer som den skal forh�pentligvis. Alle variablene og metodene er egentlig ganske selv forklart av de gj�r.
// Skriver litt kommentarer her og der. Programet er ganske spesielt fordi jeg har nesten helt like metoder i person og personliste klassene, men det var mye lettere � l�se slik fikk jeg vite av gruppel�rerne.
// det vaskeligste med oppgaven ville v�re � lete alle nullpointere, og m�tte egentli fikse ganske mye med obligen siden jeg testet ikke med Oblig1Test, slik at jeg m�tte forandre litt m� metodene her og der.
// men klarte det til slutt.


import java.util.Scanner;
//import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

public class Oblig1 {
    public static void main(String[] args) throws Exception {
	if (args.length == 0) {
	    System.out.println("INF1010 - Oblig 1");
	    System.out.println("Usage:");
	    System.out.println("Run the tests: java Oblig1 test");
	    System.out.println("Run the shell: java Oblig1 shell");
	}
	else if (args[0].equals("test")) {
	    System.out.println("Testing....");  
	    PersonList personlist = new PersonList();			
 	    Oblig1Tests tester = new Oblig1Tests(personlist);
 	    tester.runTests();
	}
	else if (args[0].equals("shell")) {
	    Shell ss = new Shell();
	    ss.kommandolokke();
	    // kj�rer kommandol�kke
	}
    }
}

class PersonList {
    Person forsteperson;
    
    public boolean addPerson(String navn, String tlfnummer) {
	Person p = new Person(navn, tlfnummer);
	if(finsPersonen(navn)) {  
	    System.out.printf("%s er allerede f�rt inn \n", navn);
	    return false;
	}
	if(forsteperson == null) {                                                      // spesialtilfelle: hvis det er en tom liste
	    forsteperson = p;
	    forsteperson.nestePerson = null;
	    System.out.printf("%s er f�rt inn med tlfnummer %s\n", navn, tlfnummer);    
	    return true;
	} else {
	    Person sjekk = forsteperson;
	    while(sjekk != null) {
		if(sjekk.nestePerson == null) {
		    sjekk.nestePerson = p;                 //en ny personobjekt p� nullpointer
		    break;
		}
		sjekk = sjekk.nestePerson;
	    }
	    System.out.printf("%s er f�rt inn med %s\n", navn, tlfnummer);
	  
	    return true;
	}
    }

    public boolean removePerson(String navn) {
	Person fjern = getPerson(navn);
	if(fjern != null) {
	    Person sjekk = forsteperson;
	  
	    // sender bare pekeren til den neste i raden, f.eks. fjerne nummer 2 da peker 1 til 3 istedenfor 2.
	    if(sjekk.nestePerson == null) {
		forsteperson = null;
		System.out.printf("%s er fjernet fra personlisten\n", navn);
		return true;
	    }
	    
	    while(sjekk != null) {
 		sjekk.rmFriends(fjern);
		sjekk = sjekk.nestePerson;
	    }

	    sjekk = forsteperson;	    
	    while(sjekk != null) {
		if(sjekk.nestePerson.getName().equals(fjern.getName())) {   
		    sjekk.nestePerson = sjekk.nestePerson.nestePerson;
		    System.out.printf("%s er fjernet fra personlisten\n", navn);
		    return true;
		}
		sjekk = sjekk.nestePerson;
	    }
	
	}
	System.out.printf("%s fins ikke i listen\n", navn);
	return false;
    }
    
    public boolean addFriend (String navn, String venn) {
	Person a = getPerson(navn);
	Person b = getPerson(venn);
	//samme tankegang som add Person, legger alltid til sist, og for at det er forskjell p� venneliste og personliste, ligger vennliste i Person.klassen.
	if(a == null || b == null) {
	    System.out.println("En eller flere av personene fins ikke i listen");
	    return false;
	} 	
      	return a.addFriend(b);
	     	
    }
	      
    public boolean removeFriend(String navn, String venn) {
	
	Person a = getPerson(navn);
	Person b = getPerson(venn);
	
	if(a == null || b == null) {
	    System.out.println("En eller flere av personene fins ikke i listen");
	    return false;
	} 	
	return a.rmFriends(b);
	 
    }
    // kj�rer en l�kke som sjekker fra f�rsteperson til den siste og teller antall pers. mens den gj�r det.
    public int getSize() {
	Person sjekk  = forsteperson;
	int sum = 0;
	while (sjekk != null) {
	    sum++;
	    sjekk = sjekk.nestePerson;
	}
	return sum;
    }
    // henter personen og designerer dem til arrayen
    public Person [] getPersons() {
	Person sjekk = forsteperson;
	int sum = 0;
	Person personer[] = new Person[getSize()];
	while(sjekk != null) {
	    personer[sum] = sjekk;
	    sjekk = sjekk.nestePerson;
	    sum++;
	}
	return personer;	
      }
    // metoden er for � hjelpe meg i begynnelsen av oblige, men etterhvert fant jeg ut at man kunne bare sjekke om det er en null-peker for � avgj�re om personen fins eller ikke
    public boolean finsPersonen(String navn) {
	Person soker = forsteperson;
	while (soker != null) {
	    if(soker.getName().equals(navn)) {
		return true;
	    }
	    soker = soker.nestePerson;
	}
	return false;
    }
    //henter personen i lista
    public Person getPerson(String navn) {
	Person sjekk = forsteperson;
	while(sjekk != null) {
	    if(sjekk.getName().equals(navn)){
		return sjekk;
	    }
	    sjekk = sjekk.nestePerson;
	}
	return null;
    }
}

class Person {
    String navn;
    String tlfnummer;
    Person nestePerson;
    Venn forsteVenn;

    Person (String navn) {
	this.navn = navn;
    }

    Person (String navn, String tlfnummer) {
	this.navn = navn;
	this.tlfnummer = tlfnummer;
    }
    
    public String getName() {
	return navn;
    }

    public String getPhone() {
	if(tlfnummer == null) {
	    tlfnummer = "Ingen tlfnummer";
	}
	return tlfnummer;	
    }
    
    // samme oppsett som getPersons
    public Person [] getFriends(){

	Venn sjekk = forsteVenn;
	int sum = 0;
	int sum1 = 0;
	while(sjekk != null) {
	    sum++;
	    sjekk = sjekk.nesteVenn;
	} 
	sjekk = forsteVenn;
	Person [] venneliste = new Person[sum];
	while(sjekk != null) {
	    venneliste[sum1] = sjekk.person;
	    sjekk = sjekk.nesteVenn;
	    sum1++;
	}
	return venneliste;	
    }
    
    // hjelpemetode i begynnelsen
    public boolean finsVennen(Person p) {

	Venn v = forsteVenn;
	while (v != null) {
	    if(v.person == p) {
		
		return true;
	    }
	    v = v.nesteVenn;
	}
	return false;
    }
    //samme oppsett som addperson
    public boolean addFriend(Person p){
	Venn v = new Venn(p);
	if(forsteVenn == null) {
	    forsteVenn = v;
	    System.out.printf("%s og %s er venner n�\n", navn, v.person.getName());
	    return true;
	}
	if(finsVennen(p)) {
	    System.out.printf("%s og %s er allerede venner\n", navn, v.person.getName());
	    return false;
	}
	else {
	    Venn sjekk = forsteVenn;
	    while(sjekk != null) {
		if(sjekk.nesteVenn == null) {
		    sjekk.nesteVenn = v;
		    break;
		}
		sjekk = sjekk.nesteVenn;
	    }
	    System.out.printf("%s og %s er venner n�\n", navn, v.person.getName());
	    return true;	    
	}
    }
    //nesten det samme oppsettet som removePerson
    public boolean rmFriends(Person fjern){
	    Venn sjekk = forsteVenn;

	    if (sjekk == null) {
		return false;
	    }
	    if(sjekk.person == fjern) {
		forsteVenn = forsteVenn.nesteVenn;
		System.out.printf("%s og %s er ikke venner lenger\n", navn, fjern.navn);
		return true;
	    }
	    while(sjekk.nesteVenn != null) {
		if(sjekk.nesteVenn.person.navn.equals(fjern.navn)){
		    sjekk.nesteVenn = sjekk.nesteVenn.nesteVenn;
		    System.out.printf("%s og %s er ikke venner lenger\n", navn, fjern.navn);
		    return true;
		}
		sjekk = sjekk.nesteVenn;
	    }
	    return false;	    
    }

    public int getSize() {
	Venn sjekk  = forsteVenn;
	int sum = 0;
	while (sjekk != null) {
	    sum++;
	    sjekk = sjekk.nesteVenn;
	}
	return sum;
    }
}
// hjelpe klasse
class Venn {
    Venn nesteVenn;
    Person person;
    
    Venn (Person a) {
	this.person = a;
    }
}
// misforsto helt hvordan shellet skulle bli i begynnelsen, men det var egentli bare � lage en kommandol�kke som i Oblig 4 i INF1000, i de fleste "metoden" under henviser jeg dem til metoden i personliste og lar dem ta seg av jobben(egentli alt av dem)
// bestemete meg � konvertere alle innput til lowercase slik at navn eller ord med blanding av store bokstaver ikke blir en problem.
class Shell {
    PersonList personlist = new PersonList();
    // fikk hjelp med scanner    
    public static Scanner lesfraTerminal() {
	return new Scanner(System.in);
    }
    
    void kommandolokke() {
	int masterkey = 1;
	Scanner input = new Scanner(System.in);
	// velger bare et valgfri tall som gj�r at while-l�kka kj�rer for alltid
	while(masterkey != 0) {
	    System.out.print("shell> ");
	    String setning = input.nextLine().toLowerCase();
	    String [] ord = setning.split(" ");
	  
	    if(ord[0].equals("add")) {
		if(ord.length <= 2) {
		    
		    System.out.println("Vennligst tast inn <name> og <nummer>");
		} else {
		    personlist.addPerson(ord[1], ord[2]);
		}
	    }
	    if(ord[0].equals("exit")){
		
		System.out.println("Sayonara!");
		break;
	    }
	    if(ord[0].equals("remove")){
		if(ord.length <= 1) {
		    System.out.println("Du har ikke skrevet navnet til personen du �nsker � fjerne");
		} else {
		    personlist.removePerson(ord[1]);
		}
	    }
	    if(ord[0].equals("friends")) {
		if(ord.length <= 2) {
		    System.out.println("Vennligst tast inn <name> og <name-friend>");
		} else {
		    personlist.addFriend(ord[1], ord[2]);
		}
	    }
	    if(ord[0].equals("unfriends")) {
		if(ord.length <= 2) {
		    System.out.println("Vennligst tast inn <name> og <name-unfriend>");
		} else {
		    personlist.removeFriend(ord[1], ord[2]);
		}
	    }
	    if(ord[0].equals("show")) {
		if(ord.length <= 1) {
		    System.out.println("Vennligst tast inn navn");	   
		} else {
		    Person p = personlist.getPerson(ord[1]);
		    if(p == null) {
			System.out.printf("%s fins ikke i listen\n", ord[1]);
		    } else {
			System.out.printf("Navn: %s\nTlfnummer: %s\nVenner: ", p.getName(), p.getPhone());
			Person [] venner = p.getFriends();
			System.out.println(venner.length);
			for(int telling = 0; telling < venner.length; telling++) {
			    System.out.println(venner[telling].getName());
			}
		    }	
		}
	    }
	    if(ord[0].equals("listall")) {
		Person [] folk = personlist.getPersons();
		int tall = 0;      
		if(folk.length >0) {
		    System.out.println("Alle registrete personer: ");
		    while(tall < personlist.getSize()) {
			System.out.println(folk[tall].getName());
			tall++;
		    } 
		}else { 
		    System.out.println("Det er forel�pig ingen personer registrert i listen");
		}
	    }
	    if(ord[0].equals("phone")) {
		if(ord.length <= 2) {
		    System.out.println("Vennligst tast inn <name> og <nummer>");
		} else {
		    Person p = personlist.getPerson(ord[1]);
		    if(p == null) {
			System.out.printf("%s fins ikke i listen\n", ord[1]);
		    } else {
			p.tlfnummer = ord[2];
			System.out.printf("%s's sitt nye telefonnummer er %s\n", p.getName(), p.getPhone());
		    }
		}
	    }
	    if(ord[0].equals("help")) {
		System.out.println("Tilgjengelige kommandoer: ");
		System.out.println("  add   <name> <number>           - add a person");
		System.out.println("  phone <name> <number>           - change phone number of a person");
		System.out.println("  remove <name>                   - remove a person");
		System.out.println("  friends <name> <name-friend>    - add a friendship between two persons");
		System.out.println("  unfriends <name> <non-friend>   - remove a friendship between two persons");
		System.out.println("  show <name>                     - show info about a person");
		System.out.println("  listall                         - list all registered persons");
		System.out.println("  tofile <filename>               - write data to file");
		System.out.println("  fromfile <filename>             - read data from file");
		System.out.println("  help                            - print this help");
		System.out.println("  exit                            - exit this program");
	    } 
	    if(ord[0].equals("tofile")){
		    if(ord.length <= 1) {
			System.out.println("Skriv riktig kommando");
		    } else {
			String datanavn, utnavn;
 	
			try {
			    FileWriter dataut = new FileWriter(ord[1] + ".txt");
			    Person[] alle = personlist.getPersons();
			    for(Person p: alle){	
				dataut.write("add " + p.getName() + " " + p.getPhone() + System.getProperty("line.separator"));
			    }
			    for(Person p: alle){
				Person[] venner = p.getFriends();
				for(Person venn: venner) {
				    dataut.write("friends " + p.getName() + " " + venn.getName() + System.getProperty("line.separator"));
				}
			    }
			    dataut.close();
			    System.out.println("Data er lagret i fil " + ord[1] + ".txt");
			}
			catch (Exception e) {
			    System.out.println(e.getMessage());
			}
		    }
	    }
	    if(ord[0].equals("fromfile")) {
		if(ord.length <= 1) {
		     System.out.println("Skriv riktig kommando");
		} else {
		    try {
			BufferedReader datain = new BufferedReader(new FileReader(ord[1] + ".txt"));
			String linje;
			
			while((linje = datain.readLine()) != null){
			    String[] delt = linje.split(" ");
			    if(delt[0].equals("add")) {
				personlist.addPerson(delt[1], delt[2]);
			    }
			    if(delt[0].equals("friends")){
				personlist.addFriend(delt[1], delt[2]);
			    }
			}
			datain.close();
		    }
		    catch (IOException e) {	
			System.out.println(e.getMessage());
		    }
		}
	    }
	} 
	
    }   
}