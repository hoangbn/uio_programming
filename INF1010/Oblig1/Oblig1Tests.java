import java.util.ArrayList;
import java.util.Arrays;


/**
 * Class used for testing Oblig 1 - INF1010 v2010
 * Updated 04.21.2010 - removePersonTest now checks friends list for all registered persons.
 */
class Oblig1Tests {

	
	PersonList personlist;
	
	Oblig1Tests(PersonList personlist) {
		this.personlist = personlist;
	}
	
	
	/**
	 * Tests the addPerson method in class PersonList
	 * @param name             name of the person to add
	 * @param phone            phone number of the person
	 * @param expectedResult   expected return value from addPerson
	 * @throws Exception       if the return value from addPerson does not 
	 *                         equal the expected.
	 */
	void addPersonTest(String name, String phone, boolean expectedResult) 
	throws Exception {

		if (personlist.addPerson(name, phone) != expectedResult) {

			if (expectedResult)
				throw new Exception("addPerson returned false when adding person \"" 
						+ name + "\" even though the person doesn't exist in the list");
			else
				throw new Exception("addPerson returned true when adding person \"" 
						+ name + "\" even though the person exists in the list");
		}
	}

	/**
	 * Tests the removePerson method in class PersonList
	 * @param name              name of the person to be removed
	 * @param expectedResult    expected return value from removePerson
	 * @throws Exception        if the return value from removePerson does not
	 *                          equal the expected
	 */
	void removePersonTest(String name, boolean expectedResult) throws Exception {

		if (personlist.removePerson(name) != expectedResult) {

			if (expectedResult)
				throw new Exception("removePerson returned false when removing person \"" 
						+ name + "\" even though the person exist in the list");
			else
				throw new Exception("removePerson returned true when removing person \"" 
						+ name + "\" even though the person shouldn't exists in the list");
		}
		
		
		// Checking that the removed person is no longer referenced in any persons friend list
		Person [] all = personlist.getPersons();
				
		for (Person p : all) {
			Person [] friends = p.getFriends();

			for (Person f : friends) {
				if (name.equals(f.getName())) {
					throw new Exception("The removed person "+name+" is still referenced in person " + p.getName() + "s friend list.");
				}
			}
		}			
	}

	/**
	 * Tests the addFriend method in class PersonList
	 * @param name             name of the person to gain a friend
	 * @param nameFriend       name of the friend to add
	 * @param expectedResult   expected return value from addFriend
	 * @throws Exception       if the return value from addFriend does not 
	 *                         equal the expected
	 */
	void addFriendTest(String name, String nameFriend, boolean expectedResult) throws Exception {

		if (personlist.addFriend(name, nameFriend) != expectedResult) {

			if (expectedResult)
				throw new Exception("addFriend returned false when adding friend \""
						+nameFriend+"\" to person \"" + name + 
				"\" even though they aren't friends");
			else
				throw new Exception("addFriend returned true when adding friend \""
						+nameFriend+"\" to person \"" + name + 
				"\" even though they are already friends");
		}
	}


	/**
	 * Tests the removeFriend method in class PersonList.
	 * @param name             name of the person to lose a friend
	 * @param friendName       name of the friend to remove
	 * @param expectedResult   expected return value from removeFriend
	 * @throws Exception       if the return value does not equal the expected.
	 */
	void removeFriendTest(String name, String friendName, boolean expectedResult) throws Exception {

		if (personlist.removeFriend(name, friendName) != expectedResult) {

			if (expectedResult)
				throw new Exception("removeFriend returned false when removing \""
						+friendName+"\" as friend form person \"" + name +
				"\" even though they are friends");
			else
				throw new Exception("removeFriend returned true when removing \""
						+friendName+"\" as friend from person \"" + name + 
				"\" even though they aren't friends");
		}
	}


	/**
	 * Tests the getSize in class PersonList
	 * @param extectedSize   the expected size of the list
	 * @throws Exception     if the size of the list is not equal the expected.
	 */
	void sizeTest(int extectedSize) throws Exception {

		if (personlist.getSize() != extectedSize) {
			throw new Exception("Expected size " + extectedSize + " but was "
					+ personlist.getSize());
		}
	}

	/**
	 * Tests that the persons returned by getPersons() in PersonList correspond
	 * to the expected names in the variable sized array taken as argument. 
	 * @param names        the expected names in the list
	 * @throws Exception   if a person in the expected array is not found in 
	 *                     the returned array, or vice versa.
	 */
	void getPersonsTest(String ... names) throws Exception {

		Person[] persons = personlist.getPersons();

		if (persons == null) {
			throw new NullPointerException("getPersons() should never return null. Return an empty array instead.");
		}
		
		ArrayList<Person> personList = new ArrayList<Person>(Arrays.asList(persons));

		for (String name : names) {

			boolean found = false;

			for (Person p : persons) {
				if (name.equals(p.getName())) {
					personList.remove(p);
					found = true;
					break;
				}
			}

			if (!found) {
				throw new Exception("Person \"" + name + 
				"\" was not found in the list of persons.");
			}
		}

		// Test if there are names in the list that does not exist in the expected
		if (personList.size() > 0) {

			String nameList = "";

			for (Person p : personList) {
				nameList += "\"" + p.getName() + "\" ";
			}

			throw new Exception("The following persons should not exist in the person list:" + nameList);
		}
	}

	/**
	 * Tests that the persons returned by getPersons() in PersonList correspond
	 * to the expected names in the variable sized array taken as argument. 
	 * @param names        the expected names in the list
	 * @throws Exception   if a person in the expected array is not found in 
	 *                     the returned array, or vice versa.
	 */
	void getFriendsTest(String name, String ... friendNames) throws Exception {

		Person person = personlist.getPerson(name);
		
		if (person == null)
			throw new Exception("Person " + name + " should exist, but was not found in the list");
		
		Person [] friendslist = person.getFriends();
		
		if (friendslist == null) {
			throw new NullPointerException("getFriends() should never return null. Return an empty array instead.");
		}
		
		ArrayList<Person> friendList = new ArrayList<Person>(Arrays.asList(friendslist));

		for (String friendName : friendNames) {

			boolean found = false;

			for (Person p : friendList) {
				if (friendName.equals(p.getName())) {
					friendList.remove(p);
					found = true;
					break;
				}
			}

			if (!found) {
				throw new Exception("Person \"" + friendName + 
				"\" was not found in the list of friends for person " + name);
			}
		}

		// Test if there are names in the list that does not exist in the expected
		if (friendList.size() > 0) {

			String nameList = "";

			for (Person p : friendList) {
				nameList += "\"" + p.getName() + "\" ";
			}

			throw new Exception("The following persons should not exist in the friend list:" + nameList);
		}
	}
	

	
	/**
	 * Runs some tests on the PersonList class
	 * If the PersonList does not behave the expected way, an exception is thrown
	 * The boolean value sent to the methods are the expected return values.
	 * @throws Exception   if any of the tests fail
	 */
	void runTests() throws Exception {
		
		addPersonTest("Are",        "9293847", true);
		addPersonTest("Christian",  "3547257", true);
		addPersonTest("Erlend",     "4568382", true);
		addPersonTest("Espen",      "7567575", true);
		addPersonTest("Jose",       "8765343", true);
		addPersonTest("Simen",      "2562344", true);
		addPersonTest("Stian",      "4563613", true);
				
		// These are the expected persons in the person list
		getPersonsTest("Are", "Christian", "Erlend", "Espen", "Jose", "Simen", "Stian");

		// Here size should be 7
		sizeTest(7);

		// Test already added
		addPersonTest("Are",        "9293847", false);
		addPersonTest("Espen",      "7567575", false);

		// Here size should be 7
		sizeTest(7);

		// Test removing non-existing person
		removePersonTest("nonexistingperson", false);

		// Test removing person
		removePersonTest("Espen", true);

		// Test removing a person that has already been removed
		removePersonTest("Espen", false);

		// Here size should be 6
		sizeTest(6);

		// Add three friends
		addFriendTest("Erlend", "Stian", true);
		addFriendTest("Erlend", "Christian", true);
		addFriendTest("Erlend", "Simen", true);


		// Already friends
		addFriendTest("Erlend", "Simen", false);

		// Tests that the person (first argument) has the friends he should have
		getFriendsTest("Erlend", new String[] {"Stian", "Christian", "Simen"});
	
		// Remove two friends
		removeFriendTest("Erlend", "Simen", true);
	
		removeFriendTest("Erlend", "Christian", true);
	
		// They are no longer friends, so should return false
		removeFriendTest("Erlend", "Simen", false);
		
		// Should work to add as friend again
		addFriendTest("Erlend", "Simen", true);
	
		// Never were friends in the first place
		removeFriendTest("Espen", "Stian", false);

		// Tests that the person (first argument) has the friends he should have
		getFriendsTest("Erlend", new String[] {"Stian", "Simen"});
	
		// Remove person Stian, he should no longer be in Erlends friend list
		removePersonTest("Stian", true);
	;
		// Adding more persons
		addPersonTest("Ruben",      "4563613", true);
		addPersonTest("Lars",      "4563613", true);
		
		// These are the expected persons in the person list
		getPersonsTest("Are", "Christian", "Erlend", "Jose", "Simen", "Ruben", "Lars");

		System.err.println("All tests run with no errors.");
	}
}
