/**
 * Precode for Oblig1 - INF1010 v2010.
 * 
 * @author inf1010
 *
 */
public class Oblig1 {

	public static void main(String[] args) throws Exception {

		if (args.length == 0) {
			System.out.println("INF1010 - Oblig 1");
			System.out.println("Usage:");
			System.out.println("Run the tests: java Oblig1 test");
			System.out.println("Run the shell: java Oblig1 shell");
		}
		else if (args[0].equals("test")) {
			
			System.out.println("Comment in the test code and compile with the test file to run the tests.");
			
			// Remove the comment on the following:
		
			PersonList personlist = new PersonList();			
			Oblig1Tests tester = new Oblig1Tests(personlist);
			tester.runTests();
			
		}
		else if (args[0].equals("shell")) {
			System.out.println("Create code to start your shell here.");
		}
	}
}
