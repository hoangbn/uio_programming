/**
 * Precode for Oblig1 - INF1010 v2010.
 * 
 * @author inf1010
 *
 */

import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;

public class Oblig1 {
    public static void main(String[] args) throws Exception {
	if (args.length == 0) {
	    System.out.println("INF1010 - Oblig 1");
	    System.out.println("Usage:");
	    System.out.println("Run the tests: java Oblig1 test");
	    System.out.println("Run the shell: java Oblig1 shell");
	}
	else if (args[0].equals("test")) {
	    
	    System.out.println("Comment in the test code and compile with the test file to run the tests.");
	    
	    PersonList personlist = new PersonList();			
 	    Oblig1Tests tester = new Oblig1Tests(personlist);
 	    tester.runTests();
	    
	 //    PersonList b = new PersonList();
// 	    b.addPerson("Alibaba", "65423821");
// 	    b.addPerson("Are", "12345831");
//  	    b.addPerson("Barte", "21345678");
//  	    b.addPerson("Cali", "98765432");
//  	    b.addPerson("Dalia", "58643215");
//  	    b.addPerson("Erik", "78964521");
// 	    System.out.println(b.getSize());
 	 
// 	    b.addFriend("Alibaba", "Dalia");
// 	    b.addFriend("Dalia", "Cali");
// 	    b.addFriend("Dalia", "Erik");
// 	    b.addFriend("Dalia", "Alibaba");
// 	    b.addFriend("Arthur", "Salamander");

// 	    b.getFriends("Dalia");
	    
// 	       b.getPersons();
// 	    System.out.println(b.forsteperson.getName());
// 	    System.out.println(b.forsteperson.nestePerson.getName());
	}
	else if (args[0].equals("shell")) {
	    Shell ss = new Shell();
	    ss.help();
	    
	    System.out.println("Create code to start your shell here.");
	}
    }
}

class PersonList {
    Person forsteperson;
    
    public boolean addPerson(String navn, String tlfnummer) {
	Person p = new Person(navn, tlfnummer);
	if(finsPersonen(navn)) {
	    System.out.println("Personen fins fra f�r av");
	    return false;
	}
	if(forsteperson == null) {
	    forsteperson = p;
	    forsteperson.nestePerson = null;
	    System.out.println("Personen er f�rt inn");
	    return true;
	} else {
	    Person sjekk = forsteperson;
	    while(sjekk != null) {
		if(sjekk.nestePerson == null) {
		    sjekk.nestePerson = p;
		    break;
		}
		sjekk = sjekk.nestePerson;
	    }
	    System.out.println("Personen er f�rt inn");
	    return true;
	}
    }

    public boolean removePerson(String navn) {
	if(finsPersonen(navn)) {
	    Person sjekk = forsteperson;
	    Person fjern = getPerson(navn);
	  
	    
	    if(sjekk.nestePerson == null) {
		forsteperson = null;
		return true;
	    }

	    while(sjekk != null) {
		sjekk.rmFriends(fjern);
		sjekk.nestePerson;
	    }
	    
	    while(sjekk != null) {
		if(sjekk.nestePerson.getName().equals(fjern.getName())) {   
		    sjekk.nestePerson = sjekk.nestePerson.nestePerson;
		    return true;
		}
		sjekk = sjekk.nestePerson;
	    }
	}
	System.out.printf("%s fins ikke i listen\n", navn);
	return false;
    }
    
    public boolean addFriend (String navn, String venn) {
	Person a = getPerson(navn);
	Person b = getPerson(venn);
	
	if(a == null || b == null) {
	    System.out.println("En eller flere av personene fins ikke i listen");
	    return false;
	} 	
      	return a.addFriend(b);
	     	
    }
	      
    public boolean removeFriend(String navn, String venn) {

	Person a = getPerson(navn);
	Person b = getPerson(venn);
	
	if(a == null || b == null) {
	    System.out.println("En eller flere av personene fins ikke i listen");
	    return false;
	} 	
	return a.rmFriends(b);
	 
    }

    public int getSize() {
	Person sjekk  = forsteperson;
	int sum = 0;
	while (sjekk != null) {
	    sum++;
	    sjekk = sjekk.nestePerson;
	}
	return sum;
    }
    
    public Person [] getPersons() {
	Person sjekk = forsteperson;
	int sum = 0;
	Person personer[] = new Person[getSize()];
	while(sjekk != null) {
	    personer[sum] = sjekk;
	   
	    sjekk = sjekk.nestePerson;
	    //  System.out.println(personer[sum].getName());
	    sum++;
	}
	return personer;	
      }
    
    public boolean finsPersonen(String navn) {
	Person soker = forsteperson;
	while (soker != null) {
	    if(soker.getName().equals(navn)) {
		return true;
	    }
	    soker = soker.nestePerson;
	}
	return false;
    }
    
    public Person getPerson(String navn) {
	Person sjekk = forsteperson;
	while(sjekk != null) {
	    if(sjekk.getName().equals(navn)){
		return sjekk;
	    }
	    sjekk = sjekk.nestePerson;
	}
	return null;
    }

}

class Person {
    String navn;
    String tlfnummer;
    Person nestePerson;
    Venn forsteVenn;

    Person (String navn) {
	this.navn = navn;
    }

    Person (String navn, String tlfnummer) {
	this.navn = navn;
	this.tlfnummer = tlfnummer;
    }
    
    public String getName() {
	return navn;
    }

    public String getPhone() {
	if(tlfnummer == null) {
	    tlfnummer = "Ingen tlfnummer";
	}
	return tlfnummer;	
    }
    
    
    public Person [] getFriends(){

	Venn sjekk = forsteVenn;
	int sum = 0;
	int sum1 = 0;
	while(sjekk != null) {
	    sum++;
	    sjekk = sjekk.nesteVenn;
	} 
	sjekk = forsteVenn;
	Person [] venneliste = new Person[sum];
	while(sjekk != null) {
	    venneliste[sum1] = sjekk.person;
	    sjekk = sjekk.nesteVenn;
	    sum1++;
	}
	return venneliste;	
     }
       
    /*   public boolean addPhone(String tlfnummer){
	 }*/
    
    public boolean finsVennen(Person p) {

	Venn v = forsteVenn;
	while (v != null) {
	    if(v.person == p) {
		
		return true;
	    }
	    
	    v = v.nesteVenn;
	}
	return false;
    }
    
    public boolean addFriend(Person p){
	Venn v = new Venn(p);
	if(forsteVenn == null) {
	    forsteVenn = v;
	    System.out.println("De er venner n�");
	    return true;
	}
	if(finsVennen(p)) {
	    System.out.println("Vennen fins fra f�r av");
	    return false;
	}
	else {
	    Venn sjekk = forsteVenn;
	    while(sjekk != null) {
		if(sjekk.nesteVenn == null) {
		    sjekk.nesteVenn = v;
		    break;
		}
		sjekk = sjekk.nesteVenn;
	    }
	    System.out.println("De er venner n�");
	    return true;
	    
	}
    }
    
    public boolean rmFriends(Person p){
	if(finsVennen(p)) {

	    Venn sjekk = forsteVenn;
	    Person fjern = p;

// 	    if(sjekk.nesteVenn == null) {
// 		forsteVenn = null;
// 		System.out.println("Vennen er fjernet fra vennelisten");
// 		return true;
// 	    }
// 	    if(sjekk.equals(fjern)) {
// 	      sjekk.nesteVenn = forsteVenn;
// 		return true;
// 	    }
	    while(sjekk != null) {
		if(sjekk.nesteVenn.person.navn.equals(fjern.navn)){
		    sjekk.nesteVenn = sjekk.nesteVenn.nesteVenn;
		    
		}
		sjekk = sjekk.nesteVenn;
	    }
	    System.out.printf("%s er fjernet fra vennelisten til %s\n", p.navn, navn);
	    return true;
	}
	System.out.printf("%s fins ikke i listen\n", p.navn);
	return false;
    }

    public int getSize() {
	Venn sjekk  = forsteVenn;
	int sum = 0;
	while (sjekk != null) {
	    sum++;
	    sjekk = sjekk.nesteVenn;
	}
	return sum;
    }
}

class Venn {
    Venn nesteVenn;
    Person person;
    
    Venn (Person a) {
	this.person = a;
    }
}

// class Shell {
//     PersonList personlist = new PersonList();
    
//     public static Scanner lesfraTerminal() {
// 	return new Scanner(System.in);
//     }
    
//     public static Scanner lesfraFil(String Oblig1fil) {
// 	try {
// 	    return new Scanner(new File(Oblig1fil));
// 	}
// 	catch(finnerikkefil e) {
// 	    System.err.println("Det var en feil ved lesing av fil" + new File(Oblig1fil).getAbsolutePath());
// 	    e.printStackTrace();
// 	}
// 	return null;
//     }
    
//     void kommandolokke() {
// 	int masterkey = 1;
// 	Scanner input = new Scanner(System.in);
	
	
// 	while(masterkey != 0) {
// 	    System.out.print("shell> ");
// 	    String setning = input.nextLine();
// 	    String [] ord = line.split(" ");
// 	    if(ord.equals("add")) {
// 		if(ord.length <= 2) {
// 		    System.out.println("
// 		}
// 		    personlist.addPerson(ord[1]);

	


// 	}
//     }
    
//     void add() {
	
//     }

//     void phone() {
//     }

//     void remove() {}

//     void friends() {}
    
//     void unfriends(){}

//     void show(){}

//     void listtall(){}

//     void writetofile(){}

//     void readfromfile(){}

//     void help() {
// 	System.out.println("add   <name> <number>           - add a person");
// 	System.out.println("phone <name> <number>           - change phone number of a person");
// 	System.out.println("remove <name>                   - remove a person");
// 	System.out.println("friends <name> <name-friend>    - add a friendship between two persons");
// 	System.out.println("unfriends <name> <non-friend>   - remove a friendship between two persons");
// 	System.out.println("show <name>                     - show info about a person");
// 	System.out.println("listall                         - list all registered persons");
// 	System.out.println("tofile <filename>               - write data to file");
// 	System.out.println("fromfile <filename>             - read data from file");
// 	System.out.println("help                            - print this help");
// 	System.out.println("exit                            - exit this program");
	

//     }
//     void exit(){}

//     void kommadolokke() {
//     }
// }