import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.io.FileNotFoundException;


public class Sorting {  

    Sorting(int core, String read, String write){
	int numCores = core;
	File input = new File(read);
	File output = new File(write);
	String[] arrayRead = readFile(input);
	Comparable[] arrayWrite = parallel_sort(arrayRead, numCores);
	writeFile(output, arrayWrite);
    }
    
    
    public String[] readFile(File file) {
	int size = 0;

	String line = "";
	try {
	    Scanner sc = new Scanner(file);
	    size = Integer.parseInt(sc.next());
	    String[] ar = new String[size];
	    for(int i = 0; i < size; i++) {
		ar[i] = sc.next();		
	    }
	    sc.close();
	    return ar;
	    
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void writeFile(File file, Comparable[] sorted) {
	
	try {
	    FileWriter out = new FileWriter(file);
	    Comparable[] toFile = sorted;
	    int words = toFile.length;
	    out.write(words);
	    out.write(System.getProperty("line.separator"));
	    for(int i = 0; i < toFile.length; i++) {
		out.write(toFile[i].toString());
		out.write(System.getProperty("line.separator"));
	    }
	    out.close();
	} catch(Exception e) {
	    System.out.println(e.getMessage());
	}
    }
    
//     public Comparable[] multipleThreadsSort(Comparable[] wholeArray, int numberOfCores){
	
// 	int threads = numberOfCores;
// 	final ArrayList<Comparable[]> arrayParts = new ArrayList<Comparable[]>();
// 	int partSize = wholeArray.lenght/threads;
	
	
    
  
    

    public Comparable[] parallel_sort(Comparable[] arr, int cores) {
        /* split the array in chunks to be processed */
	int numCores = cores;
        final ArrayList<Comparable[]> chunks = new ArrayList<Comparable[]>();
        int chunkSize = arr.length / numCores;

        for(int tid=0; tid<numCores; tid++) {
            /* calc begining and end of piece */
            int begin = tid * chunkSize;
            int end = begin + chunkSize;
            if (tid == numCores - 1) {
                /* if we are doing the last piece, go to the end of the array */
                end = arr.length;
            }
            Comparable[] chunk = new Comparable[end - begin];
            /* copy the elements into the chunk */
            for(int j=0,i=begin; i<end; i++) {
                chunk[j++] = arr[i];
            }
            chunks.add(chunk);
        }

	
	
        class Sorter implements Runnable {
            private int tid;

            public Sorter(int id) {
                tid = id;
            }

            public void run() {
                /* sort our piece */
                Comparable[] chunk = chunks.get(tid);
                Arrays.sort(chunk);
            }
        }

        Thread[] helpers = new Thread[numCores];
        for(int id=0; id<numCores; id++) {
            Thread t = new Thread(new Sorter(id));
            t.start();
            helpers[id] = t;
        }

        /* Wait for all helpers to sort their chunks */
        for(Thread t : helpers) {
            try {
                t.join();
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }

        /* Merge */
        /* 
         * In the case when there is more than 4 chunks we can execute
         * this step in parellel too but is it worth it since it is only
         * a linear time operation anyway?
         */
        Comparable[] result = chunks.get(0);
        for(int i=1; i<numCores; i++) {
            result = merge(result, chunks.get(i));
        }

        for(int i=0; i<arr.length; i++) {
            arr[i] = result[i];
        }
	result = Arrays.sort(result);
	return result;
    }

//     public static Comparable[] merge(Comparable[] resultArray, Comparable[] mergeArray) {
// 	Comparable[] result = new Comparable[resultArray.length + mergeArray.length];
// 	int index = 0;
// 	int countFirst = 0;
// 	int countSecond = 0;
	
// 	while(resultArray.length > 0 || mergeArray.length > 0) {
// 	    if(resultArray.length > 0 && mergeArray.length > 0) {
// 		if(
// 		result[index] = resultArray[countFirst++];
    
	    
    
    public static Comparable[] merge(Comparable[] arr1, Comparable[] arr2) {
        Comparable[] result = new Comparable [arr1.length + arr2.length];

        int j1 = 0; /* index into arr1 */
        int j2 = 0; /* index into arr2 */
        for(int i=0; i< result.length; i++) {
            if (j1 < arr1.length) {
                if (j2 < arr2.length) {
                    if (arr1[j1].compareTo(arr2[j2]) == -1) {
                        result[i] = arr1[j1++];
                    } else {
                        result[i] = arr2[j2++];
                    }
                } else {
                    /* reached end of second chunk, copy first*/
                    result[i] = arr1[j1++];
                }
            } else {
                /* reached end of 1st chunk, copy 2nd*/
                result[i] = arr2[j2++];
            }
        }

        return result;
    }
}
