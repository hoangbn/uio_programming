/**
 *Bruker main-etoden til � hente inn argumenter fra kommandolinjen og sende dem videre til sortering, sjekker at det m� v�re minst 3 argumenter og den f�rste m� v�re et integer
 */

public class Oblig4 {
    public static void main(String[] args) {
	int threads;
	if(args.length > 2){
	    try {
		threads = Integer.parseInt(args[0]);
		String readFile = args[1];
		String writeFile = args[2];
		new Mergesort(threads, readFile, writeFile);
	    } catch(NumberFormatException e) {
		System.err.println("Argumentet er ikke en Integer");
		System.exit(1);
	    }
	} else {
	    System.out.println("Ser ut som det mangler noe argumenter i kommandolinjen...");
	    System.out.println("Det m� v�re 3 argumenter: nummer, txt-fil, txt-fil");
	}	
    }
}