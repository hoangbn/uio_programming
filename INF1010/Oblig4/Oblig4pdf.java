/**
 *Bruker main-etoden til � hente inn argumenter fra kommandolinjen og sende dem videre til sortering, sjekker at det m� v�re minst 3 argumenter og den f�rste m� v�re et integer
 */

public class Oblig4 {
    public static void main(String[] args) {
	int threads;
	if(args.length > 2){
	    try {
		threads = Integer.parseInt(args[0]);
		String readFile = args[1];
		String writeFile = args[2];
		new Mergesort(threads, readFile, writeFile);
	    } catch(NumberFormatException e) {
		System.err.println("Argumentet er ikke en Integer");
		System.exit(1);
	    }
	} else {
	    System.out.println("Ser ut som det mangler noe argumenter i kommandolinjen...");
	    System.out.println("Det m� v�re 3 argumenter: nummer, txt-fil, txt-fil");
	}	
    }
}import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.io.FileNotFoundException;

/**
 *NB, jeg forvekslet vanlig oppgave med ekstraoppgave slik at jeg leste alle ord og lagret dem i et array istedenfor � lese inn et viss antall ord ogs� kj�re sorteringen.
 */

public class Mergesort {
    /**
     *konstrukt�r
     */
    Mergesort(int threads, String read, String write){
	long start = System.currentTimeMillis();
	File input = new File(read);
	File output = new File(write);
	String[] arrayRead = readFile(input);
	Comparable[] arrayWrite = sort(arrayRead, threads);
	mergeSort(arrayWrite);
	writeFile(output, arrayWrite);
	long end = System.currentTimeMillis();
	System.out.println("Mergesort: " + (end-start) + " ms");
    }

    /**
     *leser alle strenger i filen og lagrer det i en array
     */
    public String[] readFile(File file) {
	int size = 0;
	String line = "";
	try {
	    Scanner sc = new Scanner(file);
	    size = Integer.parseInt(sc.next());
	    String[] ar = new String[size];
	    for(int i = 0; i < size; i++) {
		ar[i] = sc.next();		
	    }
	    sc.close();
	    return ar;
	    
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     *skriver den sorterte listen inn i et nytt fil
     */
    public void writeFile(File file, Comparable[] sorted) {
	try {
	    FileWriter out = new FileWriter(file);
	    Comparable[] toFile = sorted;
	 //    int words = toFile.length;
// 	    out.write("" + words);
	  //   out.write(System.getProperty("line.separator"));
	    for(int i = 0; i < toFile.length; i++) {
		out.write(toFile[i].toString());
		out.write(System.getProperty("line.separator"));
	    }
	    out.close();
	} catch(Exception e) {
	    System.out.println(e.getMessage());
	}
    }
    
    /**
     *F�rste biten av metoden(dobbel forl�kka) fordeler alle ordene til de riktige arrayene, f.eks 100 or fordelt p� 10 tr�der, dvs partSize =10, derfor i = 0 start = 0 og end = 0 + 10; den neste array starter med objektene fra 11 - 20 da; blir riktig fordelt p� ethvert array. og lagres de i en arraylist
     *har en for-l�kka som skaper nye tr�der og begynner med � kj�re run() i Sort-klassen. venter med at alle tr�dene har kj�rt f�r programmet starter med � sette sammen de sorterte arrayene i arraylisten
     *
     */
    public Comparable[] sort(Comparable[] array, int threads){
	if(array.length == 1) {
	    return array;
	}
	int nmbThreads = threads;
	final ArrayList<Comparable[]> collection = new ArrayList<Comparable[]>();
	int partSize = array.length/nmbThreads;

	for(int i = 0; i<nmbThreads;i++){
	    int start = i * partSize;         
	    int end = start + partSize;
	    if(i == nmbThreads - 1){
		end = array.length;
	    }
	    
	    Comparable[] part = new Comparable[end - start];
	    int b = 0;
	    for(int a = start; a < end; a++){ 
		part[b++] = array[a];
	    }
	    collection.add(part);
	}
	
	Thread[] threadsArray = new Thread[nmbThreads];
	for(int i = 0; i < nmbThreads; i++) {
	    Thread thread = new Thread(new Sort(i, collection));
	    thread.start();                              
	    threadsArray[i] = thread;
	}
	
	for(Thread thread: threadsArray) {
	    try {
		thread.join();
	    } catch (InterruptedException e) {
		System.err.println(e);
	    }
	}
	
	Comparable[] result = collection.get(0);
	for(int i = 1; i < nmbThreads; i++) {
	    result = mergeTwoArray(result, collection.get(i));
	}
	return result;
    }
    
    /**
     *Metode for � sette sammen 2 nesten sorterte lister, og samtidig forst�rrer result arrayen slik at den kan lagre alle ordene som ble lest inn
     */
    public static Comparable[] mergeTwoArray(Comparable[] first, Comparable[] second) {
	Comparable[] result = new Comparable [first.length + second.length];
	
        int index1 = 0; 
        int index2 = 0; 
      
	for(int i=0; i< result.length; i++) {
            if (index1 < first.length) {
		if(index2 < second.length) {
                    if (first[index1].compareTo(second[index2]) < 0) {
                        result[i] = first[index1++];
                    } else {
                        result[i] = second[index2++];
                    }
                } else {              
                    result[i] = first[index1++];
                }
            } else {
                result[i] = second[index2++];
            }
        }
        return result;
    }
    
    /**
     *Mergesort algoritme
     *parameter er en array av Comparable objekter, i dette tilfellet String
     */
    public static void mergeSort(Comparable[] a) {
	if(a.length == 1) {
	    return;
	}
	Comparable[] temp = new Comparable[a.length];
	mergeSort(a, temp, 0, a.length - 1);
    }
    
    /**
     *metode som kaller p� funksjoner rekursivt
     *a er en array av Comparable objekter, og b er en temp array som lagrer den sammensatte resultatet
     *left er begynnelsen av den f�rste delarrayen og right er plassen p� den siste objektet i arrayen 
     *meningen er at man splitter opp arrayene slik at det ligger igjen bare et ord, da er det sortert, s� begynner man � kalle p� merge() slik at man setter sammen de sorterte arrayene
     */
    public static void mergeSort(Comparable[] a, Comparable[] b, int left, int right) {
	if(left < right) {
	    int mid = (left + right)/2;
	    mergeSort(a, b, left, mid);
	    mergeSort(a, b, mid+1, right);
	    merge(a, b, left, mid+1, right);
	}
    }
    
    /**
     *Denne metoden setter sammen to sorterte delarrayer
     *left viser til hvor den f�rste arrayen starter mid er hvor den andre starter og right er hvor den slutter
     */
    public static void merge(Comparable[] a, Comparable[] temp, int left, int mid, int right){
	int startRight = mid;
	int startLeft = left;
	int middle = mid - 1;
	
	for(int i = left; i <= right; i++) {
	    if(((startLeft <= middle) && ((startRight > right) || a[startLeft].compareTo(a[startRight]) < 0))) {
		temp[i] = a[startLeft++];
	    } else {
		temp[i] = a[startRight++];
	    }
	}
	for(int i = left; i <= right; i++) {
	    a[i] = temp[i];
	}
    }
    /**
     *Klasse som starter tr�dene for � sortere arrayene
     */
    class Sort implements Runnable  {
	public int counter;
	ArrayList<Comparable[]> lists;
	Mergesort m;
	
	Sort(int count, ArrayList<Comparable[]> list) {
	    this.counter = count;
	    this.lists = list;
	}

	public void run(){
	    Comparable[] part = lists.get(counter);
	    m.mergeSort(part);
	}
    }
}import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.io.FileNotFoundException;


public class Sorting {  

    Sorting(int core, String read, String write){
	int numCores = core;
	File input = new File(read);
	File output = new File(write);
	String[] arrayRead = readFile(input);
	Comparable[] arrayWrite = parallel_sort(arrayRead, numCores);
	writeFile(output, arrayWrite);
    }
    
    
    public String[] readFile(File file) {
	int size = 0;

	String line = "";
	try {
	    Scanner sc = new Scanner(file);
	    size = Integer.parseInt(sc.next());
	    String[] ar = new String[size];
	    for(int i = 0; i < size; i++) {
		ar[i] = sc.next();		
	    }
	    sc.close();
	    return ar;
	    
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public void writeFile(File file, Comparable[] sorted) {
	
	try {
	    FileWriter out = new FileWriter(file);
	    Comparable[] toFile = sorted;
	    int words = toFile.length;
	    out.write(words);
	    out.write(System.getProperty("line.separator"));
	    for(int i = 0; i < toFile.length; i++) {
		out.write(toFile[i].toString());
		out.write(System.getProperty("line.separator"));
	    }
	    out.close();
	} catch(Exception e) {
	    System.out.println(e.getMessage());
	}
    }
    
//     public Comparable[] multipleThreadsSort(Comparable[] wholeArray, int numberOfCores){
	
// 	int threads = numberOfCores;
// 	final ArrayList<Comparable[]> arrayParts = new ArrayList<Comparable[]>();
// 	int partSize = wholeArray.lenght/threads;
	
	
    
  
    

    public Comparable[] parallel_sort(Comparable[] arr, int cores) {
        /* split the array in chunks to be processed */
	int numCores = cores;
        final ArrayList<Comparable[]> chunks = new ArrayList<Comparable[]>();
        int chunkSize = arr.length / numCores;

        for(int tid=0; tid<numCores; tid++) {
            /* calc begining and end of piece */
            int begin = tid * chunkSize;
            int end = begin + chunkSize;
            if (tid == numCores - 1) {
                /* if we are doing the last piece, go to the end of the array */
                end = arr.length;
            }
            Comparable[] chunk = new Comparable[end - begin];
            /* copy the elements into the chunk */
            for(int j=0,i=begin; i<end; i++) {
                chunk[j++] = arr[i];
            }
            chunks.add(chunk);
        }

	
	
        class Sorter implements Runnable {
            private int tid;

            public Sorter(int id) {
                tid = id;
            }

            public void run() {
                /* sort our piece */
                Comparable[] chunk = chunks.get(tid);
                Arrays.sort(chunk);
            }
        }

        Thread[] helpers = new Thread[numCores];
        for(int id=0; id<numCores; id++) {
            Thread t = new Thread(new Sorter(id));
            t.start();
            helpers[id] = t;
        }

        /* Wait for all helpers to sort their chunks */
        for(Thread t : helpers) {
            try {
                t.join();
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }

        /* Merge */
        /* 
         * In the case when there is more than 4 chunks we can execute
         * this step in parellel too but is it worth it since it is only
         * a linear time operation anyway?
         */
        Comparable[] result = chunks.get(0);
        for(int i=1; i<numCores; i++) {
            result = merge(result, chunks.get(i));
        }

        for(int i=0; i<arr.length; i++) {
            arr[i] = result[i];
        }
	result = Arrays.sort(result);
	return result;
    }

//     public static Comparable[] merge(Comparable[] resultArray, Comparable[] mergeArray) {
// 	Comparable[] result = new Comparable[resultArray.length + mergeArray.length];
// 	int index = 0;
// 	int countFirst = 0;
// 	int countSecond = 0;
	
// 	while(resultArray.length > 0 || mergeArray.length > 0) {
// 	    if(resultArray.length > 0 && mergeArray.length > 0) {
// 		if(
// 		result[index] = resultArray[countFirst++];
    
	    
    
    public static Comparable[] merge(Comparable[] arr1, Comparable[] arr2) {
        Comparable[] result = new Comparable [arr1.length + arr2.length];

        int j1 = 0; /* index into arr1 */
        int j2 = 0; /* index into arr2 */
        for(int i=0; i< result.length; i++) {
            if (j1 < arr1.length) {
                if (j2 < arr2.length) {
                    if (arr1[j1].compareTo(arr2[j2]) == -1) {
                        result[i] = arr1[j1++];
                    } else {
                        result[i] = arr2[j2++];
                    }
                } else {
                    /* reached end of second chunk, copy first*/
                    result[i] = arr1[j1++];
                }
            } else {
                /* reached end of 1st chunk, copy 2nd*/
                result[i] = arr2[j2++];
            }
        }

        return result;
    }
}
