
--/*Oppgave 3.d*/ 

SELECT DIRECTOR.lastname, DIRECTOR.firstname, DIRECTOR.Number FROM
(SELECT lastname, firstname, count(p.personid) as Number 
FROM person p, filmparticipation fp, filmparticipation fd 
WHERE p.personid = fp.personid AND fp.parttype = 'director' 
							   AND fd.personid = p.personid 	
                              AND fd.filmid = fp.filmid AND fd.parttype != 'cast'
Group by p.lastname, p.firstname HAVING count(p.personid) > 10) as DIRECTOR
INNER JOIN
(SELECT lastname, firstname, count(ps.personid) as Number
FROM person ps, filmparticipation fps, filmparticipation fds
WHERE ps.personid = fps.personid /*AND fds.personid = fps.personid*/ AND fps.parttype = 'cast'
							AND fds.filmid = fps.filmid AND fds.parttype != 'director'						
Group by ps.lastname, ps.firstname HAVING count(ps.personid) > 10) as ACT
ON 
DIRECTOR.Number = ACT.Number
ORDER BY DIRECTOR.lastname; 


SELECT p.lastname, p.firstname, count(fi.filmid) 
FROM Person p, FilmItem fi, FilmParticipation fp
Where p.lastname = (SELECT  x.lastname FROM Person x, FilmParticipation fx
		    WHERE fx.filmid = fi.filmid AND x.personid = fx.personid
		    ORDER BY x.lastname DESC limit 1)
AND fp.personid = p.personid AND fp.parttype = 'cast'
AND fi.filmid = fp.filmid AND fi.filmtype = 'C'	    
GROUP BY p.lastname, p.firstname 
HAVING count(fp.filmid) > 50 
ORDER BY lastname ASC;

SELECT p.lastname, p.firstname, count(fi.filmid) 
FROM Person p, FilmItem fi, FilmParticipation fp
WHERE fp.personid = p.personid AND fp.parttype = 'cast'
		       	       AND fi.filmid = fp.filmid
			       AND fi.filmtype = 'C'
			       AND fp.personid = (SELECT  x.personid FROM Person x, FilmParticipation fx
						  WHERE fx.filmid = fi.filmid AND x.personid = fx.personid
					          ORDER BY x.lastname DESC limit 1)
GROUP BY p.lastname, p.firstname 
HAVING count(fp.filmid) > 50 
ORDER BY lastname ASC;


/*Oppg 3. d*/

CREATE VIEW directornum as
SELECT p.personid, count(filmid) as num
FROM person p, filmparticipation fp
WHERE p.personid = fp.personid AND fp.parttype = 'director'
GROUP BY p.personid HAVING count(fp.filmid) > 10;

CREATE VIEW castnum as
SELECT p.personid, count(filmid) as num
FROM person p, filmparticipation fp
WHERE p.personid = fp.personid AND fp.parttype = 'cast'
GROUP BY p.personid HAVING count(fp.filmid) > 10;

CREATE VIEW dircastnum as
SELECT p.personid, count(DISTINCT filmid) as num
FROM Person p, filmparticipation fp
WHERE p.personid = fp.personid AND (fp.parttype = 'cast' OR fp.parttype = 'director')
GROUP BY p.personid HAVING count(fp.filmid) > 10;

SELECT p.lastname, p.firstname, dc.num
FROM directornum dn, castnum cn, dircastnum dc, Person p
WHERE dn.personid = cn.personid  AND cn.personid = dc.personid 
AND dn.num = cn.num AND dc.num = dn.num + cn.num
AND dc.personid = p.personid; 


DROP VIEW directornum;
DROP VIEW castnum;
DROP VIEW dircastnum;
DROP VIEW dircastsum;

/*Oppg. 3 c*/
CREATE VIEW CINEMA as
SELECT fi.filmid, (SELECT p.lastname
		   FROM filmparticipation fp
		   INNER JOIN Person p ON 
		   p.personid = fp.personid
		   WHERE fp.filmid = fi.filmid
	           ORDER BY p.lastname DESC LIMIT 1) as lastname
FROM  FilmItem fi
WHERE fi.filmtype = 'C';




 (SELECT x.personid FROM Person x, FilmParticipation fx, FilmItem fi
	           WHERE fx.filmid = fi.filmid AND x.personid = fx.personid AND fx.parttype = 'cast'
	           ORDER BY x.lastname DESC limit 1) as personid

CREATE VIEW CINEMA2 as
SELECT * FROM CINEMA
WHERE personid IS NOT NULL;


CREATE VIEW CASTINCINEMA as
SELECT p.personid, count(DISTINCT fp.filmid) as num
FROM Person p, FilmParticipation fp, FilmItem fi
WHERE p.personid = fp.personid AND fp.parttype = 'cast' AND fp.filmid = fi.filmid AND fi.filmtype = 'C'
GROUP BY p.personid HAVING count(fp.filmid) > 50;



SELECT p.lastname, p.firstname
FROM Person p, CASTINCINEMA cc, FilmParticipation fp, FilmItem fi
WHERE  cc.num = (SELECT count(c.filmid) FROM CINEMA2 c WHERE c.personid = cc.personid) AND p.personid = cc.personid
GROUP BY p.lastname, p.firstname ORDER BY p.lastname;


SELECT p.lastname, p.firstname
FROM CASTINCINEMA cc
WHERE cc = (SELECT COUNT(*) FROM CINEMA c
      	    WHERE c.personid = cc.personid
GROUP BY p.lastname, p.firstname;




FROM Person p
WHERE cc.num = (SELECT count(c.filmid) FROM CINEMA c WHERE c.personid = cc.personid)
      	       	       		       AND p.personid = cc.personid
ORDER BY lastname ASC;



SELECT p.lastname, p.firstname, count(c.filmid) FROM CINEMA c, Person p WHERE c.personid = p.personid
GROUP BY p.lastname, p.firstname limit 10;


DROP VIEW CINEMA2;
DROP VIEW CINEMA;
DROP VIEW CASTINCINEMA;


/*Oppg.3 E*/

CREATE VIEW DIRECTOR50 as
SELECT p.personid, count(fp.filmid) as num
FROM Person p, FilmParticipation fp
WHERE p.personid = fp.personid AND p.gender = 'M' AND fp.parttype = 'director'
GROUP BY p.personid HAVING count(fp.filmid) > 50;

CREATE VIEW CAST50 as
SELECT p.personid, count(fp.filmid) as num
FROM Person p, FilmParticipation fp
WHERE p.personid = fp.personid AND fp.parttype = 'cast'
GROUP BY p.personid HAVING count(fp.filmid) > 50;

SELECT p.lastname, p.firstname 
FROM Person p, DIRECTOR50 d, Filmparticipation fp
WHERE p.personid = d.personid AND d.num = (SELECT count(*) FROM CAST50 c, Filmparticipation fd
      		   	      	  	  WHERE fd.filmid = fp.filmid AND c.personid = 

      
) 






--------------------------------------------------------------------------------
CREATE VIEW DIRECTOR50 as
SELECT DISTINCT fp.personid, count(distinct fp.filmid) as Directed 
FROM Person p, Filmparticipation fp
WHERE p.personid = fp.personid
AND p.gender = 'M'
AND fp.parttype = 'director'
group by fp.personid
HAVING count(*) > 50;

CREATE VIEW CAST50 as
SELECT DISTINCT fp.personid, count(DISTINCT fp.filmid) as Movies, s.personid as dirID
FROM DIRECTOR50 s, filmparticipation fp, filmparticipation fd
WHERE fp.parttype = 'cast'
AND fd.personid = s.personid AND fp.filmid = fd.filmid
group by fp.personid, s.personid;


CREATE view EXTRA as
SELECT DISTINCT p.personid
FROM CAST50 c, DIRECTOR50 d, person p
WHERE c.dirID = d.personid AND d.personid = p.personid
AND c.Movies = d.Directed
ORDER BY p.personid;

SELECT p.firstname, p.lastname, e.personid 
FROM Extra e, person p
WHERE e.personid = p.personid
GROUP BY p.firstname, p.lastname, e.personid;

DROP VIEW EXTRA;
DROP VIEW CAST50;
DROP VIEW DIRECTOR50;

--------------------------------------------------------------------------------


CREATE TEMPORARY TABLE overfemti AS
SELECT fp.personid
FROM filmparticipation fp
INNER JOIN filmitem f ON f.filmid = fp.filmid
WHERE f.filmtype = 'C' AND fp.parttype = 'cast'
GROUP BY fp.personid
HAVING COUNT(fp.filmid) > 50;




CREATE VIEW CASTINCINEMA as
SELECT p.personid, count(DISTINCT fp.filmid) as num
FROM Person p, FilmParticipation fp, FilmItem fi
WHERE p.personid = fp.personid AND fp.parttype = 'cast' AND fp.filmid = fi.filmid AND fi.filmtype = 'C'
GROUP BY p.personid HAVING count(fp.filmid) > 50;



SELECT x.firstname, x.lastname
FROM CASTINCINEMA o
INNER JOIN person x ON 
x.personid = o.personid
WHERE NOT EXISTS (SELECT c.filmid
      	  	 FROM filmparticipation c
		 WHERE c.personid = o.personid AND
		 x.lastname < (SELECT p.lastname
		 	      FROM filmparticipation fp
			      INNER JOIN person p ON p.personid = fp.personid
			      WHERE fp.filmid = c.filmid
			      ORDER BY p.lastname DESC
			      LIMIT 1));

