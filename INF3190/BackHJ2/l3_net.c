#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "l2_link.h"
#include "l3_net.h"
#include "l4_trans.h"
#include "l5_app.h"
#include "irq.h"

#define MAX_ADDRESSES 1024  /*maximum number of nodes*/
/*taken from book about dijkstra algorithm*/
#define INFINITY 1000000000   /*a number larger than every maximum path*/

struct EDGE distance[MAX_ADDRESSES][MAX_ADDRESSES]; /*information about the edge from source node to destination node*/ 

static int routing_table[MAX_ADDRESSES];  /*Routing table decide which neighboor node to use for sending*/
static int LSP_generation = 0;  /*which LSR generation we have, so we can determine if we're gonna update or not*/
static int LSP_gen_other[MAX_ADDRESSES];          /*this array is used to look up if it's an old Link State packet or new*/


/**link list as queue, a queue is necessary since our window size is only 10, and with all the 
 *info we have to send we need a queue
 */
struct LIST *start = NULL;

int counter = 0;

/*
 * This header is included in every network layer packet.
 */
struct L3Header
{
    int src_address;
    int dst_address;
    int real_address;
    char flag;
};

/*Getting the size of L3 header*/
int l3_header_size()
{
  return sizeof(struct L3Header) + l2_header_size();
}


/*
 * This static variable contains the host name of this machine.
 * It must be initialized at startup time.
 */
static int own_host_address = -1;

/*
 * The network layer needs to maintain private information about
 * the link that leads to a host address. Filling this map is
 * part of the routing process.
 * The map is static because it is inappropriate for other layers
 * to see or change it. Network layer functions should be written
 * for all necessary changes to the map.
 */
static int host_to_mac_map[MAX_ADDRESSES];
static int host_id[MAX_ADDRESSES];

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time. Set the own
 * host address.
 *
 * ONLY FOR ASSIGNMENT 1:
 * Host address and mac address are simply the same, allowing
 * a one-to-one mapping. Routing doesn't happen yet.
 * extended it with timer for various methods and initialize cost between nodes
 */
void l3_init( int self )
{
  int i, j;

    own_host_address = self;

    for( i = 0; i < MAX_ADDRESSES; i++){
      host_to_mac_map[i] = -1;  /*-1 meaning that this address isn't defined yet*/
      LSP_gen_other[i] = 0;     /*used to check generation of LSP*/
      for( j = 0; j < MAX_ADDRESSES; j++){
	distance[i][j].cost = 0;   /*if it doesn't cost anything from i to j then it's not defined*/
      }
    }
    
    /*timeout on when they're gonna run*/
    register_timeout_cb(get_time(10), &l3_send_echo, NULL);
    register_timeout_cb(get_time(10), &l3_send_list, NULL);
    register_timeout_cb(get_time(10), &l3_send_lsp, NULL);

}

/*
 * We have received a MAC address and should lookup or create a host
 * address for it (we fake the address assignment, we don't want to
 * implement ARP (address resolution protocol) in this course!).
 *
 * We have chosen MAC address = host address
 */
void l3_linkup( const char* other_hostname, int other_port, int other_mac_address )
{
    int other_host_address = other_mac_address;
 
    host_to_mac_map[other_host_address] = other_host_address;    /*adding address*/
   
    host_id[other_mac_address] = local_address_id[counter++];
    l4_linkup( other_host_address, other_hostname, other_port );
}

/*
 * Called by layer 4, transport, when it wants to send data to the
 * host identified by host_address.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 * Routing tabel is used here to determine which node it's gonna send
 *  to next
 */
int l3_send( int dest_address, const char* buf, int length )
{
    int              mac_address;
    char*            l2buf;
    struct L3Header* hdr_pointer;
    int              retval;
    
    /*Use the routing tabel to route the data through the network*/
    mac_address = routing_table[dest_address];
    
    /*used to get the first mac address of the node that everyone knows about*/
    if(host_to_mac_map[mac_address] == -1){
      mac_address = host_id[mac_address];
    }
    
    l2buf = (char*)malloc( length+sizeof(struct L3Header) );
    if( l2buf == 0 )
    {
        fprintf( stderr, "Not enough memory in l3_send\n" );
        return -1;
    }
    
    memcpy( &l2buf[sizeof(struct L3Header)], buf, length );

    hdr_pointer = (struct L3Header*)l2buf;
    hdr_pointer->dst_address = htonl(dest_address);
    hdr_pointer->src_address = htonl(own_host_address);
    hdr_pointer->flag = 'D';

    retval = l2_send( mac_address, l2buf, length+sizeof(struct L3Header) );
 
    free(l2buf);
    if( retval < 0 )
    {
        return retval;
    }
    else
    {
        return retval-sizeof(struct L3Header);
    }
}

/*Only difference is this is used to send LSA and ECHO packet to it's directly connected neighbours*/
int l3_send_info(int dest_address, const char* buf, int length, char what)
{
    int              mac_address;
    char*            l2buf;
    struct L3Header* hdr_pointer;
    int              retval;
    
    mac_address = l3_get_mac(dest_address);
    
    if(mac_address == -1){
      return -2;
    }

    l2buf = (char*)malloc( length+sizeof(struct L3Header) );
    if( l2buf == 0 )
    {
        fprintf( stderr, "Not enough memory in l3_send\n" );
        return -1;
    }

    memcpy( &l2buf[sizeof(struct L3Header)], buf, length );

    hdr_pointer = (struct L3Header*)l2buf;
    hdr_pointer->dst_address = htonl(dest_address);
    hdr_pointer->real_address = htonl(own_host_address);
    hdr_pointer->src_address = htonl(host_id[dest_address]);
    hdr_pointer->flag = what;

    retval = l2_send( mac_address, l2buf, length+sizeof(struct L3Header) );
 
    free(l2buf);
    if( retval < 0 )
    {
        return retval;
    }
    else
    {
        return retval-sizeof(struct L3Header);
    }
}


/*
 * Called by layer 2, link, when it has received data and wants to
 * deliver it.
 * A positive return value means that all data has been delivered.
 * A zero return value means that the receiver can not receive the
 * data right now.
 * A negative return value means that an error has occured and
 * receiving failed.
 *
 * NOTE:
 * Packet forwarding should be added here when it becomes relevant.
 * Routing is usually not included in layer 3 code, but sits beside
 * it. But routing can call functions of layer 3 to update
 * forwarding information.
 */
int l3_recv( int mac_address, const char* buf, int length )
{
    const struct L3Header* hdr_pointer;
    const char*            l4buf;
    int                    dest_address;
    int                    src_address;
    int                    real_mac;
    char                    flags;

    hdr_pointer   = (const struct L3Header*)buf;
    src_address   = ntohl(hdr_pointer->src_address);
    dest_address  = ntohl(hdr_pointer->dst_address);
    real_mac      = ntohl(hdr_pointer->real_address);

    flags         = hdr_pointer->flag; 
    
    l4buf = buf + sizeof(struct L3Header);
    
    fprintf(stderr, "source %d flags %c\n", src_address, flags); 
    
    if(flags == 'D'){
      if( dest_address != own_host_address ) {
	fprintf(stderr, "forward from %d to dest %d", src_address, dest_address); 
	l3_send(dest_address, l4buf, length-sizeof(struct L3Header));
	
	/*routing here*/
       	// return 1; 
      } 

     
      return l4_recv( src_address, l4buf, length-sizeof(struct L3Header) );
      /*LSR packet*/
    } else if(flags == 'L'){
      l3_recv_lsp( src_address, l4buf, length-sizeof(struct L3Header));
      //remember to send the src address but don't shift it with your own address.
      //return 1;

    } else {
      if(flags == 'R'){
	/*got a reply from an ECHO we sended*/
	l3_recv_echo(real_mac, l4buf, length-sizeof(struct L3Header));
      } else{

      /*got an Echo packet so we've to reply the ECHO Pack*/
	
	l3_reply_echo(src_address, l4buf, length-sizeof(struct L3Header));
	//	return 1;
      }    
    }
    return 1; /*All other packs should be accepted and return a positive number so the flow controll doesn't ask the package again*/
}

int l3_get_mac(int address)
{
    int mac;
    
    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        if( host_to_mac_map[mac] == address )
            break;
    }
    
    /*Didn't find and mac*/
    if( mac == MAX_ADDRESSES )
    {
      return -1;
    }

    return mac;
}



/**
 *sending echo packs to all neighbour
 *check map for neighbours and make sure we doesn't send the echo pack to ourself
 */
void l3_send_echo(void)
{
  int i, retval, destination, length;
  struct ECHO *pointer;
  struct timeval time;
  // fprintf(stderr, "trying to send echo\n own_add:%d\n", own_host_address);
  
  for(i = 0; i < MAX_ADDRESSES; i++){
    destination = l3_get_mac(i);  
    
    if(destination != own_host_address && destination != -1){
      
      /* fprintf(stderr, "Sending echo from %d to %d\n", own_host_address, destination); */
      pointer =  malloc(sizeof(struct ECHO));
      time = get_time(0);
      pointer->round_trip_time = htonl(time.tv_sec);      
      length = sizeof(struct ECHO);

      retval = l3_send_info(destination, (const char*)pointer, length, 'E');
      
      if(retval < length){
	fprintf(stderr, "adding to list\n");
	l3_add_to_list(destination, (char *)pointer, length, 'E');
      } else {
	//	printf("sended\n");
	free(pointer);
      }
    }
  }

   /*send echo to all neighboor each 10 sec*/
  register_timeout_cb(get_time(10), &l3_send_echo, NULL);
}

/**
 *When we get an ECHO packet, send it back immediately so we can get an accurate measurement of the delay   
 *between the nodes
 */
void l3_reply_echo(int destination, const char* buf, int length)
{
  // fprintf(stderr, "Replying an ECHO packet to %d\n", destination);
  
  int mac = l3_get_mac(destination);
  /* fprintf(stderr, "Replying an ECHO packet to %d\n", mac); */

  char *buffer = malloc(length);
  memcpy(buffer, buf, length);

  int retval = l3_send_info(mac, buffer, length, 'R');
  if(retval < length){
    l3_add_to_list(mac, buffer, length, 'R');
  } else {
    free(buffer);
  }
}

/**
 *Since we'll be exchanging a large number of information and our window size is 10,
 *we'll need somewhere to save the packages to send it at a later time
 */
void l3_add_to_list(int dest, const char* buf, int len, char type)
{
  //   printf("adding to queue\n");
  struct LIST *temp, *pointer;
  
  temp = malloc(sizeof(struct LIST));  
  temp->destination = dest;
  temp->buffer = (char *)buf;
  temp->length = len;
  temp->flag = type;

  /*if the list is empty*/
  if(start == NULL){
    start = temp;
    start->next = NULL;
  } else {
    /*Go to the last place in queue and add it there*/
    pointer = start->next;
    while(pointer != NULL){
      pointer = pointer->next;
    }
    pointer = temp;
    pointer->next = NULL;
  }
}

/*try and send packet and if it's a ECHO pack, then update its timer*/
void l3_send_list(void)
{
  printf("sending from queue\n"); 
  int retval;
  struct LIST *temp, *temp_next;
  struct ECHO *echo;
  struct timeval time;
  
  temp = start;
  temp_next = start;
  
  while(temp != NULL){
    echo = malloc(sizeof(struct ECHO));
    memcpy(echo, temp->buffer, sizeof(struct ECHO));
    time = get_time(0);
    echo->round_trip_time = htonl(time.tv_sec);
    if(temp->flag == 'E'){
      retval = l3_send_info(temp->destination, (const char*)echo, temp->length, temp->flag);       /*update timer so we can get an accurate measurement of the delay*/
    } else {
      /*either a reply to ECHO packet or LSP packet, we shouldn't modify*/
      retval = l3_send_info(temp->destination, temp->buffer, temp->length, temp->flag);      
    }
    /*just skip it if it failed when sending*/
    if(retval < temp->length){
      free(echo);
      temp = temp->next;
    } else {
      free(echo);
      temp_next = temp->next;
      l3_remove_from_list(temp);
      temp = temp_next;
    }
  }
  
  register_timeout_cb(get_time(10), &l3_send_list, NULL);
}

/**
 *time in echo pack can be used as a identificator to know which element in list
 *is to be removed
 */
void l3_remove_from_list(struct LIST *rm)
{
  printf("remove from list\n");
  struct LIST *previous, *remove;
  struct ECHO *a, *b;
  
  remove = start->next;
  previous = remove;

  a = (struct ECHO*) rm->buffer;
  b = (struct ECHO*) start->buffer;
  
  if(a->round_trip_time == b->round_trip_time){
    remove = start;
    start = previous;
    free(remove);
  } else {
    while(remove != NULL){
      b = (struct ECHO*) remove->buffer;
      if(a->round_trip_time == b->round_trip_time){
	previous->next = remove->next;
	free(remove);
	break;
      }
      previous = remove;
      remove = remove->next;
    }
  }
}

/**
 *We can get a reasonable estimate of the delay betwwen two nodes
 *by measuring the round trip time and dividing it by two
*/
void l3_recv_echo(int destination, const char* buffer, int length)
{
  
  
  struct ECHO *echo = (struct ECHO*) buffer;        /*cast buffer to a struct ECHO*/
  struct timeval time = get_time(0);                /*getting time now*/
  int round_trip;

  /*there is a catch if the round trip time is exactly 1, then we would get 1 / 2 = 0 in integer divisjon */
  int t = ntohl(echo->round_trip_time);
  if(t == 1){
    round_trip = 1;
  } else {
    round_trip = (int) (time.tv_sec - t) / 2;
  }
  distance[own_host_address][destination].cost = round_trip;
  
  /* if(distance[own_host_address][destination].cost != 0 && distance[destination][own_host_address].cost != 0){ */
  /*   l3_update_routing_table(); */
  /* } */
  // fprintf(stderr, "Received echo and the delay from %d to %d is %d\n", own_host_address, destination, round_trip);
}

  
/**
 *prepare the Link State Package, save all necessary info about our 
 *network topologi and the flood it to all routers/node
 *Parameters: none/void
 *return: none
 */
void l3_send_lsp(void)
{
  int count = 0;                    /*counting number of nodes we've and used by other routers to loop through our LSP*/
  int own = own_host_address;
  
  char *LSP_buffer/* , *LSP_copy */;

  struct NODE *node;
  struct LSP *lsp;

  int i, retval, host, length;
  /*count all directly connected nodes*/
 
  for(i = 0; i < MAX_ADDRESSES; i++){
    /*If there is a delay from this node to another then that means there is a connection between them*/
    if(distance[own][i].cost > 0){
      count++;
    }
  }
  
  /*If a router doesn't have any directly connected nodes, then there is no need to send a LSP about our network topologi*/
  if(count > 0){
    /*Make enough memory for the Link State Packet to store info about this network*/
    
    LSP_buffer = malloc((sizeof(struct LSP) + (sizeof(struct NODE) * count))); 
    
    length = sizeof(struct LSP) + sizeof(struct NODE) * count;

    
    /* fprintf(stderr, "size of LSP is %d\n", length); */
    


    lsp = (struct LSP*) LSP_buffer;                                        /*cast it to struct LSP and write in the necessary info about this LSP*/
    
    lsp->source = htonl(own);
    lsp->seq = htonl(LSP_generation);                                    /*set which generation this LSP is, and increment it*/
    lsp->nodes = htonl(count);
    
    // memcpy(LSP_buffer, lsp, sizeof(struct LSP));

    node = (struct NODE *) (LSP_buffer + sizeof(struct LSP));                /*cast LSP_buffer to struct EDGE and move the pointer forward so we don't overwrite any data*/
   

    /* fprintf(stderr, "SENDING LSP count: %d\n", ntohl(lsp->nodes)); */

    //   fprintf(stderr, "Own address: %d LSP generation is %d\n", own , LSP_generation);
    

    /*store info about the edges around this router*/
    for(i = 0; i < MAX_ADDRESSES; i++){     
      if(distance[own][i].cost > 0){
	node->address = htonl(i);
	node->cost = htonl(distance[own][i].cost);
	node = node + 1;                                /*by adding 1 it will move the pointer forward by sizeof(struct NODE)*/
      }
    }
    
  
    /*flood to everyone*/
    for(i = 0;i < MAX_ADDRESSES; i++){
      /*send it to all neighbour and not to ourself*/
      host = host_to_mac_map[i];
      if(host != own && host != -1){
	retval = l3_send_info(host, (const char *)LSP_buffer, length, 'L');
	if(retval < length){
	  l3_add_to_list(host, LSP_buffer, length, 'L');
	}
      }
    }
    
    free(LSP_buffer);             /*free memory after we've sended it to all neighbours*/
    LSP_generation++;
  } else {
    /* fprintf(stderr, "No nodes connected to this node yet\n"); */
  }
  /*Try and flood our LSP to others at a later time when we hopefully have updated our network */
  register_timeout_cb(get_time(10), &l3_send_lsp, NULL);

}

/**
 *receive Link state Package
 *
 */
void l3_recv_lsp(int source_address, const char *buf, int length)
{

  int i, own, cost, nodes, generation, lsp_source, mac, retval;
  
  struct LSP *lsp = (struct LSP *) buf;
  struct NODE *node = (struct NODE *) (buf + sizeof(struct LSP));
  char *LSP_buffer = malloc(length);
  
  lsp_source = ntohl(lsp->source);
  generation = ntohl(lsp->seq);
  nodes = ntohl(lsp->nodes);
  
  own = own_host_address;
  
  /*We ignore all old Link State Pakcets and LSP that was made from this router*/
  if(lsp_source != own && LSP_gen_other[lsp_source] <= generation){
    /* fprintf(stderr, "LSP Generation %d is accepted from source %d\n", generation, lsp_source); */
    /* fprintf(stderr, "count: %d\n", nodes); */
    i = 0;
    while(i < nodes){
      mac = ntohl(node->address);
      cost = ntohl(node->cost);
      distance[lsp_source][mac].cost = cost;
      node = node + 1;
      i++; 
      if(LSP_gen_other[lsp_source] > 0){
      	shortest_path(own, mac);
      }
      //  fprintf(stderr, "mac: %d cost: %d, distance from %d to %d is %d\n", mac, cost, lsp_source, mac, distance[lsp_source][mac].cost);
      
    }
    
    memcpy(LSP_buffer, buf, length);
    
    /*transmit LSP to all directly connected nodes except this router and the one who sended this package to us*/
    for(i = 0;i < MAX_ADDRESSES; i++){
      mac = host_to_mac_map[i];
      if(mac != own && mac != -1 && mac != source_address){
	retval = l3_send_info(mac, LSP_buffer, length, 'L');
	if(retval < length){
	  l3_add_to_list(mac, LSP_buffer, length, 'L');
	}
      }
    }
    
    LSP_gen_other[lsp_source]++;
    //   free(LSP_buffer);
    //   return;
  }
   l3_update_routing_table();
  free(LSP_buffer);
  /* if(own == lsp_source){ */
  /*    fprintf(stderr, "LSP Generation %d is ignored\n", generation); */
  /* } */
  //fprintf(stderr, "LSP Generation %d is ignored\n", generation);
}


int forward_packet(int dest_address, const char* buf, int length)
{
  
  return 0;

}

int dara = 0;
/**
 *Update routing table by running dijkstra/shortest path algorithm 
 *between two nodes
 *
 */
void l3_update_routing_table(void)
{
  fprintf(stderr, "Batch: %d\n", dara++);
  int i, k;
  for(i = 0; i < MAX_ADDRESSES; i++){
    for(k = 0; k < MAX_ADDRESSES;k++){
       if(distance[i][k].cost != 0 && distance[k][i].cost != 0){
  	fprintf(stderr, "from %d to %d cost %d\n", i, k, distance[i][k].cost);
	//	fprintf(stderr, "from %d to %d cost %d\n", k, i, distance[i][k].cost);
	
  	//shortest_path(i, k);
       }
    }
  }

  //local_address_id[MAX_ADDRESSES];
}


/*taken from book with minimal changes*/
/*It's somewhat special type of calculating the shortest path by going backward*/
void shortest_path(int s, int t)
{ 
  fprintf(stderr, "calculating Shortest path from %d to %d\n", s, t);
  int co = 0;
  if(s == t){
    fprintf(stderr, "Shortest path from %d to %d is through %d\n", s, t, s);
    return;
  }

  fprintf(stderr, "calculating Shortest path from %d to %d\n", s, t);
  struct state {                      /*the path being worked on*/
    int predecessor;                  /*previous node*/
    int length;                       /*length from source to this node*/
    enum{permanent, tentative} label; /*label state*/
  } state[MAX_ADDRESSES];
  
  int i, k, min, n = MAX_ADDRESSES;
  struct state *p;

  for(p = &state[0]; p < &state[n]; p++){ /*initialize state*/
    p->predecessor = -1;
    p->length = INFINITY;
    p->label = tentative;
  }

  state[t].length = 0;
  state[t].label = permanent;
  k = t;

  do{
    for(i = 0; i < n; i++){
      if(distance[k][i].cost != 0 && state[i].label == tentative){
	if(state[k].length + distance[k][i].cost < state[i].length){
	  state[i].predecessor = k;
	  state[i].length = state[k].length + distance[k][i].cost;
	}
      }
    }
    
    /*Find the tentatively labeled node with the smallest label*/
    k = 0;
    min = INFINITY;
    for(i = 0; i < n; i++){
      if(state[i].label == tentative && state[i].length < min){
	min = state[i].length;
	k = i;
      }
      state[k].label = permanent;
    }
    if(co == 10000) return;
    co++;
  } while(k != s);
    
  /*copy the oath onto the output array*/
  i = 0; 
  k = s;
 
  fprintf(stderr, "Shortest path from %d to %d is through %d\n", s, t, state[k].predecessor );

  routing_table[k] = state[k].predecessor;
}





