#ifndef L2_FLOW_H
#define L2_FLOW_H
/*
 * The MAC header. It is included in every frame.
 * You must extend it for your needs.
 */

struct L2Header
{
    int src_mac_address;
    int dst_mac_address;

    int seq;
    int ack;
    int size;
};

/* Makes sure the timers for each flow are started  */
void l2_flow_linkup( int device, const char* other_hostname, int other_port, int other_mac_address );
/* Initializes the flows */
void l2_flow_init( link_entry_t *map);
/* Se .c file for comments */
int flow_add_l2q( int device, const char *frame, size_t framelen );
int l2_flow_send( int dest_mac_addr, int via_mac_addr, const char* buf, int length );
void l2_flow_recv( int device, const char* buf, int length );
/* Handles timeout for each flow */
void flow_handle_timeout( );
#endif
