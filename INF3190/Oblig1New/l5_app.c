#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


#include "slow_receiver.h"
#include "l5_app.h"
#include "l1_phys.h"
#include "l4_trans.h"

/*
 * Initialize however you want.
 */

int  remote_address, remote_port, this_port;
char filename[50];

/* void sendFile(char *name); */
void sendFile2(char *name);

void l5_init(int local)
{
  remote_port = -1;
  remote_address = -1;
  this_port = local;
}

/*
 * Report success for a physical link establishment.
 */
void l5_linkup( int other_address, const char* other_hostname, int other_port )
{
    fprintf( stderr, "Successfully established a physical link (plugged in a cable)\n"
                     "with host:port %s:%d.\n"
                     "We can use the address >>%d<< for that machine.\n"
                     "\n",
                     other_hostname,
                     other_port,
                     other_address );
    remote_address = other_address;
    remote_port = other_port;
}

void l5_handle_keyboard( )
{
    char  buffer[1024];
    char *retval;
  

    retval = fgets( buffer, sizeof(buffer), stdin );
    if( retval != 0 )
    {
        buffer[strlen(buffer)-1] = 0;

        /* debug */
        fprintf( stderr, "The buffer contains: >>%s<<\n", buffer );

        if( strstr( buffer, "CONNECT" ) != NULL )
        {
            char hostname[1024];
            int  port;
            int  device;

            /*
             * sscanf tries to find the pattern in the buffer, and extracts
             * the parts into the given variables.
             */
            int ret = sscanf( buffer, "CONNECT %s %d", hostname, &port );
            if( ret == 2 )
            {
                /* two matches, we got our hostname and port */
                device = l1_connect( hostname, port );
				
                fprintf( stderr,
                         "Physical connection to host:port %s:%d has device number %d\n",
                         hostname, port, device );
		
		remote_port = port;
		remote_address = device;
            }
        }

        /* Your keyboard processing here */
	if(strstr(buffer, "QUIT") != NULL)
	  {
	    fprintf(stderr, "Exiting...\n");
	    FreeMemory();
	    exit(0);
	  }
	if(strstr(buffer, "SEND") != NULL)
	  {
	    if(sscanf(buffer, "SEND %s", filename) == 1) {
	      sendFile2(filename);
	   	      
	    }
	  }
    }
}

int getL5HeaderSize()
{
  return getL4HeaderSize();
}



int l5_recv( int dest_pid, int src_address, int src_port, const char* l5buf, int sz )
{
  return slow_receiver( l5buf, sz );
}

/*******************************************************************************
* Function name:    void sendFile(char *name)
* Parameters:       char *name
*   
* error:            return on errors                                        
* Description:      send data to a direct neighbour identified by the MAC address.
*******************************************************************************/

int read_pos, bytes_sent, frame_size, reads, total_sent, header_size, prev_sent, done;
char *buffer;
 FILE *file;

/* void sendFile(char *name) */
/* { */
/*   FILE *file; */
/*   char *buffer; */
 
  
/*   if(remote_port == -1) { */
/*     fprintf(stderr, "Not connected yet\n"); */
/*     return; */
/*   } */
  
/*   frame_size = 100; */
/*   bytes_sent = 0; */
/*   read_pos = 0; */
/*   total_sent = 0; */

/*   if((file = fopen(name, "r")) == NULL){ */
/*     fprintf(stderr, "Could not open file %s\n", name); */
/*   } */
  
/*   header_size = getL5HeaderSize(); */
  
/*   while(1){ */
/*     if(feof(file)){ */
/*       printf("File sended succesfully\n"); */
/*       bytes_sent = 0; */
/*       fclose(file); */
/*       return; */
/*     } */
/*     prev_sent = total_sent; */
/*     fseek(file, total_sent, SEEK_SET); */
/*     buffer = malloc(frame_size); */
/*     bzero(buffer, frame_size); */
      
/*     /\*read and stores to buffer*\/ */
/*     read = fread(buffer, 1, frame_size, file); */
/*     fprintf(stderr, "%s\n", buffer); */
/*     bytes_sent = l4_send(remote_address,0,0, buffer, read); */
      
/*     if(ferror(file)){ */
/*       fclose(file); */
/*       fprintf(stderr, "Reading error\n"); */
/*       return; */
/*     } */
      
/*     if(bytes_sent <= 0){ */
/*       total_sent = prev_sent; */
/*       /\*didn't receive ACK, or something went wrong*\/ */
/*       break; */
/*     } else { */
/*       total_sent = bytes_sent + total_sent; */
/*     } */

/*     free(buffer); */
    
    
    
/*    }  */
  
/* } */

void sendFile2(char *name)
{
 

  done = 0;
  
  if(remote_port == -1) {
    fprintf(stderr, "Not connected yet\n");
    return;
  }
  
  frame_size = 100;
  bytes_sent = 0;
  read_pos = 0;
  printf("201 resetting totalsent\n");
  total_sent = 0;
  header_size = getL5HeaderSize();

  if((file = fopen(name, "r")) == NULL){
    fprintf(stderr, "Could not open file %s\n", name);
  }
  if(ferror(file)){
    fclose(file);
    fprintf(stderr, "Reading error\n");
    return;
  }

  fseek(file, total_sent, SEEK_SET);
  buffer = malloc(frame_size);
  bzero(buffer, frame_size);
      
  /*read and stores to buffer*/
  reads = fread(buffer, 1, frame_size, file);

  bytes_sent = l4_send(remote_address,0,0, buffer, reads);
  while(bytes_sent <= 0) bytes_sent = l4_send(remote_address,0,0, buffer, reads);
 
  total_sent = bytes_sent + total_sent - header_size;
/*   printf("********total sent%d**********\n", total_sent); */
/*   printf("********bytes sent%d**********\n", bytes_sent); */
  
} 


//timeout went so we are gonna resend
void sendPrev()
{
  printf("Sending previous\n");
  while(((bytes_sent = l4_send(remote_address,0,0, buffer, reads)) <= 0));
/*   printf("********!!!!!!!!total sent%d**********\n", total_sent); */
/*   printf("********!!!!!!!!bytes sent%d**********\n", bytes_sent); */
}

/*Sending the next package*/
void sendNext()
{
  fseek(file, total_sent, SEEK_SET);    //start from total_sent bytes we sent so far
  buffer = malloc(frame_size);
  bzero(buffer, frame_size);
  
  reads = fread(buffer, 1, frame_size, file);
  printf("Sending next\n");
  
  while(((bytes_sent = l4_send(remote_address,0,0, buffer, reads)) <= 0));
  total_sent = bytes_sent + total_sent  - header_size;
/*   printf("********!!!!!!!!total sent%d**********\n", total_sent); */
/*   printf("********!!!!!!!!bytes sent%d**********\n", bytes_sent); */
  
  if(feof(file)){
   /*  printf("File sended succesfully\n"); */
    bytes_sent = 0;
    done = 1;
    fclose(file);
    bzero(filename, sizeof(filename));
    return;
  }
}

/*package we sended got acked, so to the next pack*/
void gotAck(){
  free(buffer);
  sendNext();
}


//Clean memory
int count = 0;
void doneSending()
{
  if(count == 0){
    free(buffer);
    count++;
  }
}

//more like got timeout
void gotNack(){
  sendPrev();
}
