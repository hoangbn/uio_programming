CONNECT and linkup now works flawlessly.
We can now send using delayed_dropping_sendto and still receive all
the packages on the receiving end.
I implemented with stop and wait so it's alot easier than the rest
protocols. Gotta extend it to sliding window.

got some errors from delayed_dropping_sendto when using valgrind, but
that's not my fault... or is it?:P

Design document hasn't been updated, under are the most important
updates in my program.


I didn't save the address in my_conns, I just used it in connect
method, and that's why one had the means to send over files and the
other couldn't.

Implmented acks and timeouts using the timeouts methods in
irq.c. 
The sender will ignore all but the ack it's waiting for, and the
receiver will only send a ack again if the sender sends a package the
receiver have already gotten, and it clear the issue of acks dropping
during transmission.
Sender's packages can drop midway too, so each package get a timeout
using register_timeout_cb from irq. what it does is to set a time in the
future and when it gets there it will call the address of the function
with whatever parameter you want to put in. It's like a ticking bomb
that's set to go off in 10 seconds. 
The point is when the timeout hits, we assume that the package were
lost on it's journey so we call a function to resend the same package
that was lost.

We assume acks are lost when receiver get a package
already gotten before, that's when we resend ack.


