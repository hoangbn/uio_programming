#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "delayed_sendto.h"
#include "slow_receiver.h"
#include "irq.h"
#include "l1_phys.h"
#include "l5_app.h"

/* two local functions, defined below */
static struct timeval* set_timeout_time( struct timeval* tv );
static void check_timeout_expired(void);

/*
 * The list of pending timeouts. Initially the list is empty.
 */
static timeout_cb_t* timeouts = 0;

/*
 * This is the main loop of this program.
 * It is meant to emulate a very basic interrupt dispatcher and
 * scheduler of an operating system.
 */
void handle_events( )
{
    int max_fd = my_udp_socket + 1;

    while( 1 )
    {
        fd_set          read_set;
        struct timeval  tv;
        struct timeval* tv_ptr = NULL;
        int             retval;

        /* Allow the timeout mechanisms to set a time when select
         * must wake up at the latest.
         */
        tv_ptr = set_timeout_time( &tv );

        /* The fd_set must be cleared and refilled every time before
         * you call select.
         */
        FD_ZERO( &read_set );
        FD_SET( 0, &read_set );             /* add keyboard input */
        FD_SET( my_udp_socket, &read_set ); /* add UDP socket */

        /* Now wait until something happens.
         */
        retval = select( max_fd, &read_set, 0, 0, tv_ptr );

        switch( retval )
        {
            case -1 :
                perror( "Error in select" );
                break;

            case 0 :
                /* Nothing happened on the socket or the keyboard. But the
                 * timeout has expired.
                 */
                check_timeout_expired();
                break;

            default :
                /* Something happened on the socket or keyboard. But the
                 * timeout may have expired as well. Check that first.
                 */
                check_timeout_expired();

                /* note: when you check several sockets and file descriptors
                 *       in an fd_set, always check all of them. If you do an
                 *       if-else, one of them may starve.
                 */

                /* If file descriptor 0 is set, something has happened on
                 * the keyboard. That's most likely user input. Call
                 * the application layer directly.
                 */
                if ( FD_ISSET( 0, &read_set ) )
                    l5_handle_keyboard( );

                /* If this file descriptor is set, something has happened on
                 * the UDP socket. Probably data has arrived. Call the event
                 * handler of the physical layer.
                 */
                if ( FD_ISSET( my_udp_socket, &read_set ) )
                    l1_handle_event( );
                break;
        }
    }
}

/*
 * First, we check whether timers have already expired. For all those,
 * we call all callback functions.
 * Second, we check whether there are still timeouts left. If not, we
 * can stop processing. We return 0, and select can wait infinitely
 * or until something else happens.
 * Third, we compute the timeout time for the waiting timeout structure
 * with the shortest waiting time. We substract the current time from
 * it and fill the timeval structure for select with the rest. We
 * return a pointer to that struct.
 */
struct timeval* set_timeout_time( struct timeval* tv )
{
    check_timeout_expired();

    if( timeouts == 0 )
    {
        /* nothing to do */
        return 0;
    }

    struct timeval now;
    gettimeofday( &now, 0 );

    timersub( &timeouts->calltime, &now, tv );
    return tv;
}

/*
 * Check whether any timeout callbacks should be triggered because
 * enough time has passed.
 * If yes, call the callback function, then delete the structure.
 */
static void check_timeout_expired( )
{
    if( timeouts == 0 )
    {
        /* nothing to do */
        return;
    }

    struct timeval now;
    gettimeofday( &now, 0 );

    while( timeouts && timercmp( &timeouts->calltime, &now, < ) )
    {
        timeout_cb_t* temp = timeouts;
        timeouts = temp->next;
        temp->next = 0;
        (*temp->callback)();
        free(temp);
    }
}

/*
 * Add a timeout to the timeout list.
 * The timeout time is an absolute time, as in "today, 16:34".
 * It is NOT a time relative from now, as in "in two minutes".
 * The first timeout that will expire is first in the queue.
 */
void register_timeout_cb( struct timeval tv, TimeoutCallFunc cb )
{
    timeout_cb_t* t;
    t = (timeout_cb_t*)malloc(sizeof(timeout_cb_t));
    t->calltime.tv_sec  = tv.tv_sec;
    t->calltime.tv_usec = tv.tv_usec;
    t->callback         = cb;
    t->next             = 0;

    if( timeouts == 0 )
    {
        timeouts = t;
    }
    else if( timercmp( &t->calltime, &timeouts->calltime, < ) )
    {
        t->next = timeouts;
        timeouts = t;
    }
    else
    {
        timeout_cb_t* finder = timeouts;

        while( finder->next )
        {
            if( timercmp( &t->calltime, &finder->next->calltime, < ) )
            {
                t->next = finder->next;
                finder->next = t;
                return;
            }
            finder = finder->next;
        }
        finder->next = t;
    }
}

