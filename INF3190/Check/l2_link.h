#ifndef L2_LINK_H
#define L2_LINK_H

#include <sys/types.h>

#define ACK     'A'
#define DATA    'D'
#define NACK    'N'

#define E_FULL	-1

#define MAX_ADDRESSES 1024
#define MAX_WINDOW_SIZE 10
#define SL_BUFFER_SIZE MAX_WINDOW_SIZE * 2

#define BUF_INC(id) if (1) { id++; if (id == SL_BUFFER_SIZE) id = 0; }

/*
 * This struct is meant to keep information about the local
 * physical layer device that must be used to reach a device
 * with a given remote MAC address.
 * The local device does have an own MAC address as well, of
 * course, even though we don't ever check whether a frame
 * has actually been sent to it or to another MAC address.
 */
struct LinkEntry
{
    int remote_mac_address;
    int phys_device;
};
typedef struct LinkEntry link_entry_t;

/*
 * Struct to hold the content and length of a buffer in the window.
 */
struct WindowEntry
{
    char* buf;
    int	len;
    int device;
};
typedef struct WindowEntry window_entry_t;

/*
 * The MAC header. It is included in every frame.
 */
struct L2Header
{
        int src_mac_address;
        int dst_mac_address;
        int id;
        char type;
};  

int dest_to_src_lookup(int dest_mac_addr);
int get_new_buffer_position(void);
int num_windows_in_buffer(void);
void receive_ack(int id);
int valid_id(int id);
int l2_connect( const char* hostname, int port );
int l2_get_header_size(void);
int l2_get_local_mac(void);
void l2_init( int local_mac_address, int device );
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address );
int  l2_send( int mac_address, const char* buf, int length );
void l2_recv(const char* buf, int length );

#endif /* L2_LINK_H */
