#ifndef L1_PHYS_H
#define L1_PHYS_H

#define MAX_CONNS 5

struct PhysicalConnection
{
    char* remote_hostname;
    int   remote_port;
};

typedef struct PhysicalConnection phys_conn_t;

/*
 * This is the one UDP socket that is used for all sending
 * and receiving. The select loop must know it.
 */
extern int my_udp_socket;

/* see more comments in the c file */

int  l1_connect( const char* hostname, int port);
int l1_get_header_size(void);
void l1_handle_event(void);
void l1_init( int local_port );
void l1_linkup(const char*, int, int);
void l1_req_physical_connection( const char* hostname, int port );
int  l1_send( int device, const char* buf, int length );

#endif /* L1_PHYS_H */

