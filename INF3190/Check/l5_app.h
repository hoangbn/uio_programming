#ifndef L5_APP_H
#define L5_APP_H

/* see comments in the c file */

int l5_get_header_size(void);
void l5_init(int);
void l5_linkup( int other_address, const char* other_hostname, int other_port );

void l5_handle_keyboard(void);
int l5_recv(const char* l5buf, int sz );
void l5_send(void);
#endif /* L5_APP_H */

