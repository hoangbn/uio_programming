/*
 * Handy debugging macro to get e.g.
 * [debug main.c:main:70] message..
 * Wrapped inside an if (1) {} to be able to do
 * if (condition)
 *      DEBUG("message");
 * without needing { } around the DEBUG macro.
*/
#ifdef DEBUGGING
#define DEBUG(msg, ...) if (1) {                                \
        fprintf(stderr,                                         \
        "[debug, %s:%s:%d] " msg,                               \
        __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ );      \
}
#else
#define DEBUG(msg, ...)
#endif

#define MAX_FRAME_LENGTH    100

#define SUCCESS		    1
#define E_TRYAGAIN	    -2

#define DEFAULT_TIMEOUT	    5
