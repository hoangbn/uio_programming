#include <sys/time.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "general.h"
#include "irq.h"
#include "l1_phys.h"
#include "l4_trans.h"
#include "l5_app.h"
#include "slow_receiver.h"

char filename[30];
int remote_addr, remote_port, my_port;
static int total_bytes_sent;

/*
 * Initialize however you want.
 */
void l5_init( int local_port)
{
    remote_addr = remote_port = -1;
    my_port = local_port;

    total_bytes_sent = 0;
}

/*
 * Report success for a physical link establishment.
 */
void l5_linkup( int other_address, const char* other_hostname, int other_port )
{
    fprintf( stderr, "Successfully established a physical link (plugged in a cable)\n"
                     "with host:port %s:%d.\n"
                     "We can use the address >>%d<< for that machine.\n"
                     "\n",
                     other_hostname,
                     other_port,
                     other_address );

    remote_addr = other_address;
    remote_port = other_port;
}

/* Size of this layer's headers, and the ones below. */
int l5_get_header_size() {
            return l4_get_header_size();
}

void l5_handle_keyboard( )
{
    char  buffer[1024];
    char *retval;

    /*
     * The use of gets() is discouraged because it's a security hole.
     * If you input more than buffer's size, bad things can happen.
     * It's just such a convenient little function ...
     * Feel free to make something better.
     */
    retval = fgets( buffer, sizeof(buffer), stdin );
    if( retval != 0 )
    {
        buffer[strlen(buffer)-1] = 0;

        DEBUG("The buffer contains: >>%s<<\n", buffer );

        if( strstr( buffer, "CONNECT" ) != NULL )
        {
            char hostname[1024];
            int  port;
            int  device;

            /*
             * sscanf tries to find the pattern in the buffer, and extracts
             * the parts into the given variables.
             */
            int ret = sscanf( buffer, "CONNECT %s %d", hostname, &port );
            if( ret == 2 )
            {
                /* two matches, we got our hostname and port */
                device = l4_connect( hostname, port );

                DEBUG("Physical connection to %s:%d has device number %d\n",
                    hostname, port, device );

            }
        } else if (strstr(buffer,"QUIT") != NULL) {
            DEBUG("Exiting as the user entered: >>%s<<\n", buffer);
            exit(0);
        } else if (strstr(buffer,"SEND") != NULL) {
            if (sscanf(buffer, "SEND %s", filename) == 1) {
                l5_send();
            }
        }
    }
}

int l5_recv(const char* l5buf, int sz )
{
    return slow_receiver(l5buf, sz);
}

void l5_send()
{
    FILE *file;

    char *buffer;
    int max_read, read, bytes_sent;

    // If length of filename is 0, we are finished reading the file, just
    // some stale callbacks trying to finish the job.
    if (strlen(filename) == 0)
	return;

    if (remote_port == -1) {
        fprintf(stderr, "Do a CONNECT first!\n");
	return;
    }

    max_read = MAX_FRAME_LENGTH - l5_get_header_size();

    file = fopen(filename, "r");

    if (file == NULL) {
        fprintf(stderr, "Could not open file %s\n", filename);
	return;
    }

    // (Re)start at the correct position.
    fseek(file, total_bytes_sent, SEEK_SET);

    for (;;) {
	if (feof(file)) {
	    printf("Done sending file.\n");
	    // Reset in order to send a new file.
	    total_bytes_sent = 0;
	    bzero(filename, sizeof(filename));
	}

	buffer = malloc(max_read);
	bzero(buffer, max_read);
       
	read = fread(buffer, (size_t) 1, max_read, file);

	if (read < max_read && ferror(file))
	{
	    fclose(file);
	    fprintf(stderr, "Error occured while reading %s\n", filename);
	    return;
	}
	
	bytes_sent = l4_send(remote_addr, remote_port, my_port, buffer, read);

	if (bytes_sent <= 0)
	{
	    //Sending didn't go well. Try resending it later.
	    register_timeout_cb(get_time_struct(DEFAULT_TIMEOUT), &l5_send);
	    break;
	} else
	    total_bytes_sent += bytes_sent;

	free(buffer);
    };

    fclose(file);
}
