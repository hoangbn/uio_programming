#include <sys/time.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "common.h"
#include "general.h"
#include "irq.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "l3_net.h"

static int get_previous_id(int);
static void resend_window(void);
static void l2_send_ack(int , int, int);

/*
 * The network layer needs to maintain private information about
 * the MAC address at the other end of every link.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static link_entry_t mac_to_device_map[MAX_ADDRESSES];

int local_mac;

// The sequece number the receiving side is currently waiting for.
static int waitfor;

// Position of the first and last element in the sliding buffer.
static int sb_first, sb_last;

window_entry_t slide_buffer[SL_BUFFER_SIZE];

int dest_to_src_lookup(int dest_mac_addr)
{
    int i;

    for( i=0; i<MAX_ADDRESSES; i++ )
    {
        if( mac_to_device_map[i].remote_mac_address == dest_mac_addr )
	    return i;
    }
    
    return -1;
}

/*
 * Return the first available position to store the l1buffer.
 * Returns E_FULL if there are 10 windows in the buffer.
 */
int get_new_buffer_position()
{
    if (num_windows_in_buffer() == MAX_WINDOW_SIZE)
	return E_FULL;
    else {
	BUF_INC(sb_last);
	return sb_last;
    }
}

/*
 * Returns the value of one valid sequence id before the given one.
 * If no previous ones, it returns the given id.
 */
static int get_previous_id(int id)
{
    if (sb_first <= sb_last || sb_first < SL_BUFFER_SIZE)
	return (sb_first < id) ? sb_first : id;
    else
	return (id < sb_last) ? id : sb_last;
}

/*
 * Removes all frames from position sb_first to the ack id.
 */
void receive_ack(int ackid)
{
    int i;

    DEBUG("sb_first = %d, sb_last = %d, ackid = %d, valid = %d\n", sb_first, sb_last, ackid, valid_id(ackid));

    if (!valid_id(ackid))
	return;

    for(;;)
    {
	i = get_previous_id(ackid);
	DEBUG("i = %d, ackid = %d\n", i, ackid);
	free(slide_buffer[i].buf);
	slide_buffer[i].len = 0;
	if (i != sb_last)
	    BUF_INC(sb_first);
	if (i == ackid)
	    break;
    }
}

/*
 * Resend the entire window, the frames in correct order. Very primitive flow control..
 * Set a new timer if we have resent frames.
 */

void resend_window(void)
{
    int i, count;

    count = 0;

    i = sb_first;

    for (;;)
    {
	if (slide_buffer[i].len != 0) {
	    DEBUG("Resending sequence id %d\n", i);
	    l1_send(slide_buffer[i].device, slide_buffer[i].buf, slide_buffer[i].len);
	    count++;
	}

	if (i == sb_last)
	    break;
	BUF_INC(i);
    }

    // We have sent a packet, and hence need to start a time again.
    if (count) {
	DEBUG("Had to resend %d frames, hence register a new timeout.\n", count);
	register_timeout_cb(get_time_struct(DEFAULT_TIMEOUT), &resend_window);
    } else
	DEBUG("No frames resent, good! :-)\n");
}


/* Verify that the id is in the sliding window.
 * Returns 1 if true, and 0 if false. */

int valid_id(int id)
{
    if (id < 0 || id >= SL_BUFFER_SIZE)
	return 0;
    else if (sb_last >= id && id >= sb_first)
	return (slide_buffer[id].len != 0) ? 1 : 0;
    else if (sb_last <= id && sb_first <= id)
	return (slide_buffer[id].len != 0) ? 1 : 0;
    else
	return 0;
}

/*
 * Returns the number of windows in the buffer.
 */
int num_windows_in_buffer()
{
    if (sb_last >= sb_first)
	return sb_last - sb_first;
    else
	return SL_BUFFER_SIZE - 1 + sb_last - sb_first;
}

/* Pass the connect function downwards */
int l2_connect(const char* hostname, int port)
{
    return l1_connect(hostname, port);
}

/* Size of this layers headers. */
int l2_get_header_size() {
    return sizeof(struct L2Header) + l1_get_header_size();
}

/* Pass down the mac address, as needed during the CONNECT/UP */
int l2_get_local_mac() {
    return local_mac;
}

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * In particular initialize all the data structures that you
 * need for link-layer error correction and flow control.
 */
void l2_init( int local_mac_address, int device )
{
    int mac;

    sb_first = waitfor = 0;

    // Set this one to -1, as the first time we enter l2_send(), the sequence
    // will be increased to 0.
    sb_last = -1;

    local_mac = local_mac_address;

    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        mac_to_device_map[mac].remote_mac_address = -1;
        mac_to_device_map[mac].phys_device        = -1;
    }

    mac_to_device_map[local_mac_address].remote_mac_address = -1;
    mac_to_device_map[local_mac_address].phys_device        = device;
}

/*
 * We have gotten an UP packet for a particular device from the remote host.
 * We have to remember that in our table that maps MAC addresses to devices.
 */
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address )
{
    int mac;
    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        if( mac_to_device_map[mac].phys_device == device )
        {
            mac_to_device_map[mac].remote_mac_address = other_mac_address;
            l3_linkup( other_hostname, other_port, other_mac_address );
            return;
        }
    }
    errx(-1, "Programming error in establishing a physical link\n");
}

/*
 * Called by layer 3, network, when it wants to send data to a
 * direct neighbour identified by the MAC address.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 *
 * NOTE:
 * You will need to split this function into many small helper
 * functions. In particular, you will need something that allows
 * you to perform retransmissions after a timeout.
 */
int l2_send( int dest_mac_addr, const char* buf, int length )
{
    int   device = -1;
    int   src_mac_addr = -1;
    char* l1buf;
    struct L2Header*  hdr;
    int   retval;
    int l1buflen;
    int sid;

    DEBUG("in buffer: %d\n", num_windows_in_buffer());

    if ((sid = get_new_buffer_position()) == E_FULL)
	return E_TRYAGAIN;

    DEBUG("Sender sid: %d\n", sid);

    if ((src_mac_addr = dest_to_src_lookup(dest_mac_addr)) == -1)
	err(-1,"MAC address not found in l2_send");
    device = mac_to_device_map[src_mac_addr].phys_device;

    l1buflen = length + sizeof(struct L2Header);

    l1buf = malloc(l1buflen);

    if( l1buf == NULL)
        err(-1,"Not enough memory in l2_send\n");

    memcpy( &l1buf[sizeof(struct L2Header)], buf, length );

    hdr = (struct L2Header*)l1buf;
    hdr->type = DATA;
    hdr->src_mac_address = htonl(src_mac_addr);
    hdr->dst_mac_address = htonl(dest_mac_addr);
    hdr->id = htons(sid);

    retval = l1_send(device, l1buf, l1buflen);

    slide_buffer[sid].buf = l1buf;
    slide_buffer[sid].len = l1buflen;
    slide_buffer[sid].device = device;

    register_timeout_cb(get_time_struct(2), &resend_window);

    if( retval < 0 )
        return -1;
    else
        return retval-sizeof(struct L2Header);
}

static void
l2_send_ack(int dest_mac_addr, int src_mac_addr, int id)
{
    struct L2Header* hdr;
    int device;

    hdr = malloc(sizeof(struct L2Header));

    DEBUG("Acking id %d from %d to %d\n", ntohs(id),dest_mac_addr,src_mac_addr);

    device = mac_to_device_map[src_mac_addr].phys_device;
    hdr->type = ACK;
    hdr->src_mac_address = htonl(src_mac_addr);
    hdr->dst_mac_address = htonl(dest_mac_addr);
    hdr->id = id;

    // Blindy send the ack. If the recipient doesn't receive it, it will send the
    // packet again, and we'll try acking again.
    l1_send(device, (char *)hdr, sizeof(struct L2Header));
    
    free(hdr);
}

/*
 * Called by layer 1, physical, when a frame has arrived.
 *
 * This function has no return value. It must handle all
 * problems itself because the physical layer isn't able
 * to handle errors.
 */
void l2_recv(const char* buf, int length )
{
    const struct L2Header*  hdr_pointer;
    const char*             l3buf;
    int                     src_mac_address;
    int                     dst_mac_address;
    int			    id;

    hdr_pointer = (const struct L2Header*)buf;
    src_mac_address  = ntohl(hdr_pointer->src_mac_address);
    dst_mac_address  = ntohl(hdr_pointer->dst_mac_address);
    id = ntohs(hdr_pointer->id);

    if (hdr_pointer->type == DATA)
	DEBUG("innkommende id: %d, vi venter p� %d.\n", id, waitfor);

    if (hdr_pointer->type == DATA)
    {
	// This is the frame with the sequence id we want.
	if (id == waitfor)
	{
	    l3buf = buf + sizeof(struct L2Header);


	    if (l3_recv(l3buf, length-sizeof(struct L2Header) )) {
		l2_send_ack(src_mac_address, dst_mac_address, hdr_pointer->id);
		BUF_INC(waitfor);
	    }
	} else {
	    // If this frame has a sequence id which is less than 10 less than
	    // 'waitfor', we reack the frame, as our ack might have possibly been
	    // lost.
	    if (waitfor - id < 10 || waitfor - id + SL_BUFFER_SIZE)
		l2_send_ack(src_mac_address, dst_mac_address, hdr_pointer->id);
	}

    } else if (hdr_pointer->type == ACK)
    {
	DEBUG("We received an ack! :-) (id %d)\n", id);
	receive_ack(id);
    }
}
