#include <sys/socket.h>

#include <arpa/inet.h>
#include <err.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

#include "delayed_dropping_sendto.h"
#include "general.h"
#include "l1_phys.h"
#include "l2_link.h"

static phys_conn_t my_conns[MAX_CONNS];

int my_udp_socket = -1;
int my_port;


/* Size of this layers headers. */
int l1_get_header_size() {
    return 0;
}

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * local_port is the port that for the local UDP socket that
 *   we will use for all communication.
 *
 * Here in particular: initialize all of your socket communication
 * in this function. The socket communication is meant to fake
 * physical cables, so it should be done completely before
 * continuing.
 */
void l1_init( int local_port )
{
    int                i;
    int                error;
    struct sockaddr_in addr;

    my_port = local_port;
    my_udp_socket = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP );
    if( my_udp_socket < 0 )
        errx(-1, "Failed to create local UDP socket");

    memset( &addr, 0, sizeof(struct sockaddr_in) );
    addr.sin_family      = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port        = htons(local_port);

    error = bind( my_udp_socket, (struct sockaddr*)&addr, sizeof(struct sockaddr_in) );
    if( error < 0 )
        errx(-1, "Failed to bind local UDP socket to port %d", local_port );
    else
	DEBUG("Successfully bound to port %d\n", local_port);

    for( i=0; i<MAX_CONNS; i++ )
    {
        my_conns[i].remote_hostname = 0;
        my_conns[i].remote_port     = -1;
    }
}

/*
 * Initiate establishment of a "physical connection" to the given host and
 * port. The return value of the function is the device that has been
 * allocated to this connection.
 */
int l1_connect(const char* hostname, int port)
{
    
    char message[7];
    int mac;
    struct sockaddr_in remote;
    struct hostent *he;
    socklen_t remote_len = (socklen_t) sizeof(remote);
    int device;

    if ((he=gethostbyname(hostname)) == NULL)
    {
        herror("gethostbyname");
        exit(-1);
    }

    bzero(&remote, sizeof(remote));
    remote.sin_family = AF_INET;
    remote.sin_port = htons(port);
    remote.sin_addr = *((struct in_addr *)he->h_addr);

    hostname = inet_ntoa(remote.sin_addr);

    for( device=0; device<MAX_CONNS; device++ )
    {
        if( my_conns[device].remote_hostname == 0 )
        {
            my_conns[device].remote_hostname = strdup(hostname);
            my_conns[device].remote_port     = port;
            break;
        }
    }
    if( device == MAX_CONNS )
        errx(-1, "Too many physical connections established.");

    
    sprintf(message, "C %d", l2_get_local_mac());

    if ((sendto(my_udp_socket, &message, strlen(message), 0, (struct sockaddr*)&remote, remote_len)) == -1)
        errx(-1, "Could not send CONNECT to %s:%d, aborting.\n", hostname,port);

    bzero(message, strlen(message));

    if ((recvfrom(my_udp_socket, &message, 15, 0, (struct sockaddr*)&remote, &remote_len)) == -1)
        errx(-1, "Error when retriving message from %s\n", hostname);
    
    if ((sscanf(message, "U %d", &mac)) == 1)
    {
        DEBUG("Buffer from %s was >>%s<<\n", hostname, message);
        DEBUG("Got an UP from %s, he has 'mac' %d\n", hostname,mac);
        l1_linkup(inet_ntoa(remote.sin_addr), port, mac);
    } else
        errx(-1, "Remote host (%s) didn't respond well to our CONNECT. Exiting.\n", hostname);

    return device;
}

/*
 * Called by layer 2, link, when it wants to send data over the
 * "physical connection" that is represented by device.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 */
int l1_send( int device, const char* buf, int length )
{
    /*
     * Extend this function so that you send the given data over YOUR
     * "physical connection" using the function sendto(), the
     * function delayed_sendto() and the function delayed_dropping_sendto().
     *
     * It's easiest to start with sendto(), because it doesn't delay
     * and it doesn't drop packets randomly.
     *
     * delayed_sendto() works very similar to the system function
     * sendto() but it delays packet delivery in annoying ways.
     * The same for delayed_dropping_sendto().
     */

    struct sockaddr_in remote;

    bzero(&remote, sizeof(remote));
    remote.sin_family = AF_INET;
    inet_aton(my_conns[device].remote_hostname, &remote.sin_addr);
    remote.sin_port = htons(my_conns[device].remote_port);

    return delayed_dropping_sendto(my_udp_socket, buf, length, (struct sockaddr*)&remote, (socklen_t) sizeof(remote));
}
/*
 * If a packet that has been received is an UP packet that establishes
 * a link instead of carrying data, l1_handle_event should call this
 * function.
 * The function should assign a device that is now connected in the
 * table my_conn and print an UP message onto the screen.
 */
void l1_linkup( const char* other_hostname, int other_port, int other_mac_address )
{

    int device;

    /* First, check whether this is an answer to a CONNECT packet.
     * If it is, we will find a device entry and tell the user.
     */
    for( device=0; device<MAX_CONNS; device++ )
    {
        if( my_conns[device].remote_hostname != 0 )
        {
            if( ( my_conns[device].remote_port == other_port ) &&
                ( !strcmp( my_conns[device].remote_hostname, other_hostname ) ) )
            {
                l2_linkup( device, other_hostname, other_port, other_mac_address );
                return;
            }
        }
    }

    /* Second, if it isn't try to assign a new device and tell the
     * user.
     */
    for( device=0; device<MAX_CONNS; device++ )
    {
        if( my_conns[device].remote_hostname == 0 )
        {
            my_conns[device].remote_hostname = strdup(other_hostname);
            my_conns[device].remote_port     = other_port;
            l2_linkup( device, other_hostname, other_port, other_mac_address );
            return;
        }
    }
    if( device == MAX_CONNS )
        errx(-1,"Too many physical connections established.");
}

/*
 * In interrupt occurs when data arrives. Our interrupts are simulated
 * by data-arrival events in the select loop.
 * When select notices that data for my_udp_socket has arrived, it calls
 * this function.
 */
void l1_handle_event( )
{
    /*
     * Write this function so that it can deliver data to layer 2
     * using l2_recv. You must find the right device identifier to
     * pass to l2_recv. A good way of finding it is a combination
     * the system function recvfrom for receiving from the network
     * and finding the right entry in my_conns.
     *
     * This function does not have any return values. The physical
     * layer can not handle errors, and the interrupt handler can't,
     * either. The higher level functions must be able to deal with
     * everything.
     */
    struct sockaddr_in remote;
    socklen_t addr_len = sizeof(remote);
    int device, remote_mac, remote_port, rbytes;
    char *remote_host;
    char buffer[MAX_FRAME_LENGTH];

    bzero(&buffer, MAX_FRAME_LENGTH);

    char up_message[7];

    if ((rbytes = recvfrom(my_udp_socket, buffer, MAX_FRAME_LENGTH, 0,
            (struct sockaddr *)&remote, &addr_len )) == -1) {
            perror("recvfrom");
            exit(-1);
    }

    remote_host = inet_ntoa(remote.sin_addr);
    remote_port = ntohs(remote.sin_port);
    DEBUG("Got a %d byte(s) buffer from %s:%d\n", rbytes, remote_host, remote_port);

    // A normal packet will be much longer due to the headers, hence this is
    // a CONNECT packet.
    if (rbytes < 10) {
        if ((sscanf(buffer, "C %d", &remote_mac)) == 1) {
            sprintf(up_message, "U %d", l2_get_local_mac());
            if ((sendto(my_udp_socket, &up_message, strlen(up_message), 0,
              (struct sockaddr*)&remote, sizeof(remote))) == -1)
                errx(-1, "Could not send UP to %s, aborting.\n", remote_host);
            l1_linkup(remote_host, remote_port, remote_mac);
        } else {
            err(-1,"Got a bad short message from %s\n", remote_host);
        }
    } else {
        for (device = 0; device < MAX_CONNS; device++) {
            if( ( my_conns[device].remote_port == remote_port ) &&
                ( !strcmp( inet_ntoa(my_conns[device].remote_hostname), remote_host ) ) )
            {
                l2_recv(buffer, rbytes);
                return;
            }
        }
        DEBUG("What!! Should not get here. Bogus packet from: %s\n", remote_host);
    }
}
