#ifndef IRQ_H
#define IRQ_H

void handle_events(void);

void register_timeout_cb( struct timeval tv, void (*cb)(void) );

/*
 * You will be interested to try things repeatedly after a little
 * bit of waiting time. This will happens at several layers, and
 * it will happen for several layers at the same time. This functions
 * helps you to register a function when a particular timer has
 * expired.
 */
typedef void (*TimeoutCallFunc)(void);

struct TimeoutCallback
{
    struct timeval  calltime;
    TimeoutCallFunc callback;
    struct TimeoutCallback* next;
};
typedef struct TimeoutCallback timeout_cb_t;

#endif /* IRQ_H */
