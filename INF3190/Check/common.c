#include <sys/time.h>

#include "common.h"

/*
 * Returns a 'struct timeval' which is set to a give seconds into the future.
 */
struct timeval get_time_struct(int seconds)
{
    struct timeval t;
    gettimeofday(&t, 0);

    t.tv_sec += seconds;

    return t;
}
