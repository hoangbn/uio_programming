#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "irq.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "l2_flow.h"
#include "l3_net.h"
#include "l5_app.h"


#define MAX_ADDRESSES 1024

/*
 * The network layer needs to maintain private information about
 * the MAC address at the other end of every link.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static link_entry_t mac_to_device_map[MAX_ADDRESSES];
static int local_address, received_from, connections = 0;

/*
 * window_size = window size 
 * req_num = request number
 * seq_num = sequence number
 * seq_base = sequence base
 * seq_max = sequence max
 */
static int window_size, done; 

/*naming was a little off, but it's used to tell us that we've read all bytes form file*/
void resetSend()
{
  done = 1;
}

/*reset it before sending so it's possible to send to a old sender*/
void resetRecv()
{
  received_from = -1;
}

/*used to get the mac address of this machine*/
void l2_local(int local)
{
  local_address = local;
  printf("LOCAL: %d\n", local_address);
}

/*returns the size of header from this layer*/
int l2_header_size()
{
  return sizeof(struct L2Header);
}

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * In particular initialize all the data structures that you
 * need for link-layer error correction and flow control.
 */
void l2_init( int local_mac_address, int device )
{
  static int first = 1;
  received_from = -1;  // not set

  window_size = MAX_WIND_SIZE;
  done  = 0;

  if( first )
    {
      l2_flow_init( mac_to_device_map );

      int mac, x;
      for( mac=0; mac<MAX_ADDRESSES; mac++ )
        {
	  mac_to_device_map[mac].remote_mac_address = -1;
	  mac_to_device_map[mac].phys_device        = -1;
	  for(x = 0;x < MAX_WIND_SIZE;x++){
	    mac_to_device_map[mac].windows[x].seq = -1;
	  }
        }
      first = 0;
    }
  fprintf(stderr, "localmac: %d, device: %d\n", local_mac_address, device);
  mac_to_device_map[local_mac_address].remote_mac_address = -1;
  mac_to_device_map[local_mac_address].phys_device        = device;

}

/*
 * We have gotten an UP packet for a particular device from the remote host.
 * We have to remember that in our table that maps MAC addresses to devices.
 */
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address )
{
  connections++;
  int mac;
  mac = l2_get_mac( device );
  fprintf(stderr, "mac: %d\n", mac);

  mac_to_device_map[mac].remote_mac_address = other_mac_address;
  mac_to_device_map[mac].seq_num = mac_to_device_map[mac].req_num  = mac_to_device_map[mac].seq_base = 0;
  l2_flow_linkup( device, other_hostname, other_port, other_mac_address );

  l3_linkup( other_hostname, other_port, other_mac_address );
}

/*
 * Used to get the mac address of a device.
 */
int l2_get_mac( int device )
{
  int mac;
    
  for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
      if( mac_to_device_map[mac].phys_device == device )
	break;
    }
    
  /*Didn't find and mac*/
  if( mac == MAX_ADDRESSES )
    {
      return -1;
    }
  return mac;
}

/*
 *Used to get the device corresponding to the remote address
 */
int l2_get_dest_device(int dest)
{
  int device;
  for(device = 0; device < MAX_ADDRESSES; device++){
    if(  mac_to_device_map[device].remote_mac_address == dest){
      return device;
    }
  }
  return -1;
}

/*Used to get a new position in window
 *device is used to know which window we
 *are dealing with
*/
 int get_new_position(int device)
  {
    int val = -1;
    int i;
    for(i = 0; i < MAX_WIND_SIZE;i++){
      if(mac_to_device_map[device].windows[i].seq == -1){
	val = i;
	return val;
      }
    }
    return val;
  }


 /*When a timeout runs out, we'll try to resend the frame again*/
/*
 *param is a struct Parameter with 2 ints which is used to locate
 *exactly which device window and position in window
 * but it seems like this is the reasons for SEG fauelt if not runnig with valgrind
 */
void sendAgain(void *param)
{
  int err, device, length;
  char *l1buf;
  struct Parameter *params;

  params = param;

  device = l2_get_dest_device(params->address);
  l1buf = mac_to_device_map[params->address].windows[params->slot].buffer;
  length = mac_to_device_map[params->address].windows[params->slot].len;
  
  err = l1_send( device, l1buf, length);
  
  if(err < 0){
    register_timeout_cb(get_time(1), &sendAgain, params);
  } else {
    free(params);
  }
}

/*Flood to all nodes which is connected to this node*/
int l2_flood(int dest_mac_addr, const char *buf, int length)
{
  int remote_address;
  int through_mac;
  int err;

  int mac;
  for(mac = 0; mac < MAX_ADDRESSES; mac++){
    remote_address = l2_get_mac(mac);
    through_mac = mac_to_device_map[remote_address].remote_mac_address;
    
    if(remote_address == -1){
      break;
    }   
    if(through_mac != local_address && through_mac != received_from){ 
      err = GBN_send(through_mac, buf, length); 
      if(err < 0){
	return -1;
      }
    }
  }
  return err;
}


/*
 * This function decides if a frame should be sent up to the next
 * layer above or sent out over a bridge to the other links.
 * device is the device we have received the frame from.
 *
 * NOTE, buf contains L2Header, use this to decide to bridge or send to l3_recv 
 */
int l2_frame_controller( int device, const char* buf, int length )
{

  const struct L2Header* hdr_pointer;
  const char*            l3buf;
  int                    src_mac_address;
  int                    dst_mac_address;
  int                    real_dest_address;
  int                    err;
  int                    dest;

  hdr_pointer = (const struct L2Header*)buf;
  src_mac_address  = ntohl(hdr_pointer->src_mac_address);
  dst_mac_address  = ntohl(hdr_pointer->dst_mac_address);
  real_dest_address = ntohl(hdr_pointer->real_mac_address);

  real_destination_address = real_dest_address;
  received_from = src_mac_address;

  l3buf = buf + sizeof(struct L2Header);
  
  dest = l2_get_dest_device(real_dest_address);
  err = 1;  


#ifdef DEBUG
  fprintf(stderr, "This frame is going to Destination: %d\n", real_dest_address);
#endif

  /*if it's destination is this node/machine then we'll try to receive*/
  if(this_address == real_dest_address){
    err = l3_recv(device, l3buf, length-sizeof(struct L2Header));
    /*Bridging is possible since remote address can be found locally*/
  } else if(dest > -1){
#ifdef DEBUG
    fprintf(stderr, "Frame is not for us, but we can bridge it to the right receiver\n");
#endif

    err = GBN_send(real_dest_address, l3buf, length-sizeof(struct L2Header));
  }
  /*Since it doesn't exist locally, flood to all machines in the perimeter*/
  else {
#ifdef DEBUG
    fprintf(stderr, "Frame is not for us, and we can't bridge it since there ain't no one in our perimeter with MAC: %d, try flooding\n", real_dest_address);
#endif
    /*If we have only 1 connection then we can't flood by sending the package back, so we drop the package then ack*/
    if(connections < 2){
      printf("Flooding ain't possible, drop frame and ack\n");
      return 1;
    }
    err = l2_flood(dst_mac_address, l3buf, length-sizeof(struct L2Header));
  }
  /*window is full so we dropped the frame and wait till the window got more spaces to receive more frames*/
  if(err == -2){
    printf("window is full, request a new one\n");
    return -1;
    /*Since window has still some free spaces, it's already saved so we can ack back*/
  } else if(err == -1) {
    return 1;
    /*a negative number means slow receiver didn't accept frame, a positive value means otherwise*/
  } else {
    return err;      
  }
}

/*
 *Resend all frames that are still in the window
 *input parameter is a mac_address so we can locate which 
 *frames to send from which window
 */
void resend_frames(int mac_address)
{
  struct Parameter *param;
  int retval, length;
  int check = 0;
  
  int i;
  for(i = 0; i < MAX_WIND_SIZE;i++){
    if( mac_to_device_map[mac_address].windows[i].seq != -1){
      length = mac_to_device_map[mac_address].windows[i].len;
    
      check++;
    
      param = malloc(sizeof(struct Parameter));
      param->address = mac_address;
      param->slot = i;
      printf("Resending frame %d, LENGTH %d\n",mac_to_device_map[mac_address].windows[i].seq, length);
      retval = l1_send( mac_to_device_map[mac_address].windows[i].device, mac_to_device_map[mac_address].windows[i].buffer , length);
      if(retval <= 0){
	register_timeout_cb(get_time(1), &sendAgain , param); 
      } else {
	free(param);
      }
    }
  }
  /******************************************************************************************/
  if(done){
    if(check == 0){
      printf("Nothing to send anymore\n");
      resetSend();
    }
    /******************************************************************************************/
  }
}


/*
 *Take care of all incoming ack-packets
 *It will free all spaces in the window of located by mac
 *which is lower that the requested value request
*/
  void Ack_recv(int mac, int request)
  {

    /*Only receive acks on frame we are waiting acknowledge on, dismiss all other*/
  
#ifdef DEBUG
    fprintf(stderr, "Got ACK: %d from MAC: %d\n", request-1, mac);
#endif

   int seq_max = MAX_WIND_SIZE - 1;
    if(request > mac_to_device_map[mac].seq_base){
      seq_max = seq_max + (request - mac_to_device_map[mac].seq_base);
      mac_to_device_map[mac].seq_base = request;
    
      /*free all frame thats sended and received acks on them*/
      int i;
      for(i = 0; i < MAX_WIND_SIZE;i++){
	if(mac_to_device_map[mac].windows[i].seq < request){
	   if(mac_to_device_map[mac].windows[i].seq > -1){      /*to make sure it doesn't free already freed buffer*/
	    free(mac_to_device_map[mac].windows[i].buffer);
	   } 

	  mac_to_device_map[mac].windows[i].seq = -1;
	  mac_to_device_map[mac].windows[i].len = -1;
	}
      }
    } else {
      resend_frames(mac);
    }
  }

/*When a frame is succesfully received we ack it back
 *destination: who to send to
 *source: sender
 *req: which frame 
*/
  void Ack_send(int destination, int source, int req)
  {
#ifdef DEBUG
    fprintf(stderr, "SENDING ACK: %d to MAC: %d\n", req, destination);
#endif

    struct L2Header* header;
    int device;

    header = malloc(sizeof(struct L2Header));
    device = mac_to_device_map[source].phys_device;

    header->ack = req;
    header->src_mac_address = htonl(source);
    header->dst_mac_address = htonl(destination);

    /*cast struct to char array and send it out*/
    l1_send(device, (char*) header, sizeof(struct L2Header));

    free(header);
  }

/*Determine whether a frame is an ACK- or data-frame */
  void GBN_recv( int device, const char* buf, int length )
  {
    const struct L2Header* hdr_pointer;
    int                    src_mac_address;
    int                    dst_mac_address;
    int                    whichAck;
    int                    err;
    int                    whichSeq;
     
    hdr_pointer = (const struct L2Header*)buf;
    src_mac_address  = ntohl(hdr_pointer->src_mac_address);
    dst_mac_address  = ntohl(hdr_pointer->dst_mac_address);

    
  
    whichAck = hdr_pointer->ack;
    whichSeq = hdr_pointer->seq;
    
    /*  ACK frame*/
    if (whichAck >= 0) {
      Ack_recv(src_mac_address, whichAck);
      /*Data frame*/
    } else if(whichAck < 0){
      if(mac_to_device_map[src_mac_address].req_num == whichSeq){
	err = l2_frame_controller(dst_mac_address, buf, length);
	if(err <= 0){
	  fprintf(stderr, "couldn't receive data, requesting package %d again\n",  mac_to_device_map[src_mac_address].req_num);
	  Ack_send(dst_mac_address,dst_mac_address,   mac_to_device_map[src_mac_address].req_num);
	} else {	 
	  mac_to_device_map[src_mac_address].req_num++;
	  Ack_send(dst_mac_address,dst_mac_address,   mac_to_device_map[src_mac_address].req_num);
	}
      } else {
	fprintf(stderr, "Got package %d, Requesting package %d again..\n", whichSeq,  mac_to_device_map[src_mac_address].req_num);
	Ack_send(dst_mac_address,dst_mac_address,   mac_to_device_map[src_mac_address].req_num);
      }
    }
  }

  /*
   * Called by layer 1, physical, when a frame has arrived.
   *
   * This function has no return value. It must handle all
   * problems itself because the physical layer isn't able
   * to handle errors.
   *
   */
  void l2_recv( int device, const char* buf, int length )
  {
  
    /* 
     * l2_flow_recv will handle the flow_control mechanism, before sending the frame
     * to l2_frame_controller
     */

    GBN_recv(device, buf, length);    
  }

/*Save frames in window then send the frame to dest_mac_addr
 *Returns: -1 couldn't send the frame but frame is saved in window, -2 window is full
**/
  int GBN_send( int dest_mac_addr, const char * buf, int length)
  {
    int   device = -1;
    int   src_mac_addr = -1;
    char* l1buf;
    struct L2Header*  hdr_pointer;
    int   retval;
    int   i;
    int pos_in_window;
  
    struct Parameter *param; 

    for( i=0; i<MAX_ADDRESSES; i++ )
      {
	if( mac_to_device_map[i].remote_mac_address == dest_mac_addr )
	  {
            device       = mac_to_device_map[i].phys_device;
            src_mac_addr = i;
            break;
	  }
      }
    if( i==MAX_ADDRESSES )
      {
	fprintf( stderr, "MAC address not found in l2_send" );
	return -1;
      }
  
    pos_in_window = get_new_position(dest_mac_addr);
  
    /*window is full*/
    if(pos_in_window == -1){
      return -2;
    }

    l1buf = (char*)malloc( length+sizeof(struct L2Header) );
    if( l1buf == 0 )
      {
	fprintf( stderr, "Not enough memory in l2_send\n" );
	return -1;
      }
  
    memcpy( &l1buf[sizeof(struct L2Header)], buf, length );
  
    /*header file*/
    hdr_pointer = (struct L2Header*)l1buf;
    hdr_pointer->src_mac_address = htonl(src_mac_addr);
    hdr_pointer->dst_mac_address = htonl(dest_mac_addr);
    hdr_pointer->seq = mac_to_device_map[dest_mac_addr].seq_num++;
    hdr_pointer->real_mac_address = htonl(real_destination_address);
    hdr_pointer->ack = -1; 
  
    /*save it to the corresponding dest_mac_addr window*/
    mac_to_device_map[dest_mac_addr].windows[pos_in_window].buffer = (char*)malloc( length+sizeof(struct L2Header) );
    memcpy(mac_to_device_map[dest_mac_addr].windows[pos_in_window].buffer, l1buf, length+sizeof(struct L2Header) );
    //mac_to_device_map[dest_mac_addr].windows[pos_in_window].buffer = l1buf;
    mac_to_device_map[dest_mac_addr].windows[pos_in_window].seq = hdr_pointer->seq;
    mac_to_device_map[dest_mac_addr].windows[pos_in_window].device = device;
    mac_to_device_map[dest_mac_addr].windows[pos_in_window].len = length+sizeof(struct L2Header);

    /*parameters that are used to know which window and frame to send at a later time*/
    param = malloc(sizeof(struct Parameter));
    param->address = dest_mac_addr;
    param->slot = pos_in_window;
  
    retval = l1_send( device, l1buf, length+sizeof(struct L2Header) );

    free(l1buf);
  
    if( retval < 0 )
      {
	register_timeout_cb(get_time(1), &sendAgain , param); 
	return -1;
      }
    else
      {
	free(param);
	return retval-sizeof(struct L2Header);
      } 
  }

  /*
   * Called by layer 3, network, when it wants to send data to a
   * MAC address.
   * A positive return value means the number of bytes that have been
   * sent.
   * A negative return value means that an error has occured.
   *
   * NOTE:
   * You can split this function into many small helper
   * functions. If you are not using the supplied flow control, you will need
   * to have a sliding window implementation. 
   */

/*
 *determine whether we're gonna flood or bridge
 */
  int l2_send( int dest_mac_addr, const char * buf, int length )
  {
    /* 
     * Will send the frame with the given flow control.
     * The second parameter will be the mac we will send out on if
     * we do not know the receiver.
     *
     * I.e.:
     *
     * With the network topology like the following:
     *
     *     A (mac 1) <-------> (mac 2) B (mac 3) <------> (mac 4) C
     *
     * node B will have two mac addresses and the other will just have one.
     * If A were to send a frame to node C then B will have to
     * bridge over to the link to C. The purpose of the second parameter is
     * to tell l2_flow_send that node A wants to send via node B because
     * the destination is unknown.
     */
  
    int destination;
    destination = l2_get_dest_device(dest_mac_addr);
    // If the destination address is not locally connected with this node, then we've to flood to all nodes who's connected
    if(destination == -1){
      return l2_flood(dest_mac_addr, buf, length);
    } else {
      return GBN_send(dest_mac_addr, buf, length);
      // return l2_flow_send(dest_mac_addr, dest_mac_addr, buf, length);
    }
  }

