#ifndef L2_LINK_H
#define L2_LINK_H
#define MAX_ADDRESSES 1024

#define MAX_WIND_SIZE 10
#define MAX_FRAME_SIZE 100

/* This structure keeps track of the remote MAC on the other side of the link, and what device number it can be reached.
 * We also have a flow control mechanism for each link */

struct LinkEntry
{
    int remote_mac_address;
    int phys_device;
};
typedef struct LinkEntry link_entry_t;


/* see more comments in the c file */

void l2_init( int local_mac_address, int device );
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address );

int l2_get_mac( int device );

int l2_frame_controller( int device, const char* buf, int length );

int  l2_send( int mac_address, const char* buf, int length );
void l2_recv( int device, const char* buf, int length );
int l2_header_size();
void printall();
void l2_local(int local);
void resetRecv();
#endif /* L2_LINK_H */
