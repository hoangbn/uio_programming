#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

#include "general.h"
#include "irq.h"
#include "l2_link.h"
#include "l3_net.h"
#include "l3_routing.h"
#include "l4_trans.h"

/* Pass the connect function downwards */
int l3_connect(const char* hostname, int port)
{
    return l2_connect(hostname, port);
}

/* Size of this layers headers. */
int l3_get_header_size() {
    return sizeof(struct L3Header) + l2_get_header_size();
}

/*
 * We have received a MAC address and should lookup or create a host
 * address for it (we fake the address assignment, we don't want to
 * implement ARP (address resolution protocol) in this course!).
 *
 * We have chosen MAC address = host address
 */
void l3_linkup(const char* other_hostname, int other_port, int other_mac_address)
{
    int other_host_address = other_mac_address;

    // Store a ping to the mac address. We can not do ut until l2_linkup has completed,
    // which is after l3_linkup has completed, and hence it must be done later.
    struct L3Ping *buf = malloc(sizeof(struct L3Ping));
    struct timeval tv = get_time_struct(0);
    buf->time = htonl((uint32_t) tv.tv_sec);
    l3_add_to_queue(0, other_host_address, own_ip, (char *) buf, sizeof(struct L3Ping), PING);
    l4_linkup(other_host_address, other_hostname, other_port);
}

/*
 * Called by layer 4, and when routing data.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 */
int l3_send(int dst_addr, const char* buf, int length)
{
    int mac_addr;
    char* l2buf;
    struct L3Header* hdr;
    int retval;

    if ((mac_addr = l3_get_route(dst_addr)) == E_NOHOST)
        return E_NOHOST;

    DEBUG("mac_addr = %d, dst_addr = %d\n", mac_addr, dst_addr);

    l2buf = (char*) malloc(length+sizeof(struct L3Header));
    if (l2buf == 0)
    {
        fprintf(stderr, "Not enough memory in l3_send\n" );
        return ENOMEM;
    }

    memcpy(&l2buf[sizeof(struct L3Header)], buf, length);

    hdr = (struct L3Header*)l2buf;
    hdr->dst_addr = htons((uint16_t) dst_addr);
    hdr->src_addr = htons((uint16_t) own_ip);
    hdr->type = DATA;

    retval = l2_send(mac_addr, l2buf, length+sizeof(struct L3Header));

    free(l2buf);

    if (retval < 0)
        return retval;
    else
        return retval-sizeof(struct L3Header);
}

/*
 * Called when sending ping and LSA-packets, and hence need to
 * avoid the routing, but instead send directly to the neighbour.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 */
int l3_direct_send(int dst_addr, int dst_mac_addr, int src_addr, const char* buf, int length, char type)
{
    char* l2buf;
    struct L3Header* hdr;
    int retval;

    l2buf = (char*)malloc(length+sizeof(struct L3Header));
    if (l2buf == 0)
    {
        fprintf(stderr, "Not enough memory in l3_direct_send\n" );
        return ENOMEM;
    }
    
    memcpy(&l2buf[sizeof(struct L3Header)], buf, length);

    hdr = (struct L3Header*)l2buf;
    hdr->dst_addr = htons((uint16_t) dst_addr);
    hdr->src_addr = htons((uint16_t) src_addr);
    hdr->type = type;

    //DEBUG("Sending to %d via mac %d, with type %c\n", dst_addr, dst_mac_addr, type);
    retval = l2_send(dst_mac_addr, l2buf, length+sizeof(struct L3Header));

    free(l2buf);

    if (retval < 0)
        return retval;
    else
        return retval-sizeof(struct L3Header);
}

/*
 * Called by layer 2, link, when it has received data and wants to
 * deliver it.
 * A positive return value means that all data has been delivered.
 * A zero return value means that the receiver can not receive the
 * data right now.
 * A negative return value means that an error has occured and
 * receiving failed.
 */
int l3_recv(int src_mac_addr, const char* buf, int length)
{
    const struct L3Header* hdr;
    uint16_t dst_addr,src_addr;
    size_t l3len;

    l3len = sizeof(struct L3Header);

    hdr = (const struct L3Header*)buf;
    src_addr = ntohs(hdr->src_addr);
    dst_addr = ntohs(hdr->dst_addr);

    DEBUG("src_addr = %d, dst_addr = %d, mac_addr = %d, type = %c, len = %d\n", src_addr, dst_addr, src_mac_addr, hdr->type,length);

    if (hdr->type == DATA) {
        if (dst_addr == own_ip) {
            return l4_recv(src_addr, buf + l3len, length-l3len);
        } else {
            return l3_route(src_addr,dst_addr, buf + l3len, length-l3len);
        }
    } else if (hdr->type == PING) {
        l3_send_pong(src_addr, src_mac_addr, buf + l3len, length-l3len);
        return SUCCESS; // A positive value, not important if the answer is delayed.
    } else if (hdr->type == PONG) {
        l3_recv_pong(src_addr,src_mac_addr, buf + l3len);
        return SUCCESS;
    } else if (hdr->type == LSA) {
        // Ignore LSA packets we have sent.
        if (src_addr != own_ip)
            l3_recv_lsa(src_mac_addr, buf, length);
        return SUCCESS;
    } else {
        DEBUG("type = %c, which is not supported...FAILBOAT!!\n", hdr->type);
        return -1;
    }
}
