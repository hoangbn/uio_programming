#ifndef L3_NET_H
#define L3_NET_H

#define E_NOMAC	    -2
#define E_NOHOST    -3

#define DATA	    'D'
#define LSA         'L'
#define PING        'P'
#define PONG        'O'

/*
 * This header is included in every network layer packet.
 */
struct L3Header
{
    uint16_t src_addr;
    uint16_t dst_addr;
    char type;
    char ttl;
};

int l3_connect(const char* hostname, int port);
int l3_get_header_size();
void l3_linkup(const char* other_hostname, int other_port, int other_mac_address);
int l3_send(int host_addr, const char* buf, int length);
int l3_direct_send(int dst_addr, int dst_mac_addr, int src_addr, const char* buf, int length, char type);
int l3_recv(int src_mac_addr, const char* buf, int length);

#endif /* L3_NET_H */
