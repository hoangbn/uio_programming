#include <sys/time.h>

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "general.h"
#include "irq.h"
#include "l4_trans.h"
#include "l5_app.h"
#include "slow_receiver.h"

// The udp port to listen on.
int my_port;

// Information abount each concurrent sending.
send_entry_t sendinfo[MAX_CONNS];

// Maps remote mac to remote ports
int mac_to_port[MAX_CONNS];

/*
 * Initialize however you want.
 */
void l5_init(int mac, int port)
{
    static int first = 1;
    int i;

    if (first)
    {
        for (i=0; i < MAX_CONNS; i++)
            mac_to_port[i] = UNUSED_PORT;

        my_port = port;
        first = 0;
    }

    mac_to_port[mac] = OWN_PORT;
}

/*
 * Report success for a physical link establishment.
 */
void l5_linkup( int other_address, const char* other_hostname, int other_port )
{
    printf("Successfully established a physical link (plugged in a cable)\n"
	 "with host:port %s:%d.\n"
	 "We can use the address >>%d<< for that machine.\n\n",
	 other_hostname, other_port, other_address );
}

/* Size of this layer's headers, and the ones below. */
int l5_get_header_size() {
    return l4_get_header_size();
}

/* Parses the keyboard input and delegates to appropriate functions. */
void l5_handle_keyboard( )
{
    char  buffer[1024];
    char *retval;

    int i,remote_mac;
    char filename[30];

    retval = fgets( buffer, sizeof(buffer), stdin );
    if( retval != 0 )
    {
        buffer[strlen(buffer)-1] = 0;

        DEBUG("The buffer contains: >>%s<<\n", buffer );

        if( strstr( buffer, "CONNECT" ) != NULL )
        {
            char hostname[1024];
            int  port;
            int  device;

            /*
             * sscanf tries to find the pattern in the buffer, and extracts
             * the parts into the given variables.
             */
            int ret = sscanf( buffer, "CONNECT %s %d", hostname, &port );
            if( ret == 2 )
            {
                // two matches, we got our hostname and port
                device = l4_connect( hostname, port );
                printf("Physical connection to %s:%d has device number %d\n",hostname, port, device );
            } else
		fprintf(stderr,"Invalid arguments to (%s) to CONNECT. Use \"hostname port\"\n",buffer+7);
        } else if (strstr(buffer,"QUIT") != NULL) {
	    fprintf(stderr, "OK, exiting program.\n");
            exit(0);
        } else if (strstr(buffer,"SEND") != NULL) {
            if (sscanf(buffer, "SEND %d %s", &remote_mac, filename) == 2) {
		for (i=0; i < MAX_CONNS; i++) {
		    if (sendinfo[i].filename == NULL)
			break;
		}
		
		if (i == MAX_CONNS) {
		    fprintf(stderr,"Too many concurrent sendings, try again later.\n");
    		    return;
		}
		sendinfo[i].remote_mac = remote_mac;
		sendinfo[i].filename = strdup(filename);
                l5_send((void *)&i);
	    } else
		fprintf(stderr, "Invalid arguments (%s) to SEND. Use \"mac filename\"\n",buffer+4);
        } else {
	    printf("Available commands:\n");
	    printf("CONNECT hostname port --- Connects to a host:port\n");
	    printf("SEND mac filename     --- Sends file to mac adress\n");
	    printf("QUIT                  --- Quits program\n");
	}
    }
}

/* 
 * Receives a buffer from l4 and passes it blindly up to slow_receiver.
 * Returns slow_receiver's return value.
 */
int l5_recv( int dest_pid, int src_address, int src_port, const char* l5buf, int sz )
{
    return slow_receiver(l5buf, sz);
}

/* 
 * Sends the file to the host which the user has entered. It will be called
 * by the callback system in order to finish the sending, and hence it uses
 * a global variable to be able to restart at the last position.
 * */
void l5_send(void *param)
{
    FILE *file;

    char *buffer;
    int max_read, read, ret, s_id;
    
    s_id = ((int *)param)[0];
    DEBUG("s_id = %d\n", s_id);

    // If the filename is null, we are finished reading the file, just
    // some stale callbacks trying to finish the job.
    if (sendinfo[s_id].filename == NULL)
    	return;

    max_read = MAX_FRAME_LENGTH - l5_get_header_size();

    file = fopen(sendinfo[s_id].filename, "r");

    if (file == NULL) {
        perror("Can not open file");
	return;
    }

    // (Re)start at the correct position.
    fseek(file, sendinfo[s_id].total_bytes_sent, SEEK_SET);

    for (;;) {
	if (feof(file)) {
	    printf("Done sending file %s to %d\n", sendinfo[s_id].filename, sendinfo[s_id].remote_mac);
	    goto donefile;
	}

	if ((buffer = calloc(1,max_read)) == NULL)
	    errx(-1, "Insufficient memory available, aborting.\n");
        
        DEBUG("max_read = %d\n", max_read);
	read = fread(buffer, (size_t) 1, max_read, file);

	if (read < max_read && ferror(file))
	{
	    fprintf(stderr, "Error occured while reading %s\n", sendinfo[s_id].filename);
	    goto donefile;
	}
	
	ret = l4_send(sendinfo[s_id].remote_mac, mac_to_port[sendinfo[s_id].remote_mac], my_port, buffer, read);

	free(buffer);

	if (ret == E_TRYAGAIN)
	{
	    //Sending didn't go well. Try resending it later.
	    void *p;
	    p = malloc(sizeof(int));
	    memcpy(p, &s_id, sizeof(int));
	    register_timeout_cb(get_time_struct(DEFAULT_TIMEOUT), &l5_send, p);
	    break;
	} else
	    sendinfo[s_id].total_bytes_sent += ret;
    }

    fclose(file);
    return;

// Reset in order to send a new file.
donefile:
    sendinfo[s_id].total_bytes_sent = 0;
    free(sendinfo[s_id].filename);
};
