#include <sys/time.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "general.h"
#include "irq.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "l3_net.h"

/*
 * The network layer needs to maintain private information about
 * the MAC address at the other end of every link.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static link_entry_t m_to_d[MAX_ADDRESSES];

/* 
 * Looks up the correct mac address to use for a remote destination.
 * Returns E_NOMAC if the remote mac is not found.
 */
int src_mac_lookup(int dest_mac_addr)
{
    int i;

    for( i=0; i<MAX_ADDRESSES; i++ )
    {
        if (m_to_d[i].remote_mac_addr == dest_mac_addr)
	    return i;
    }
   
    return E_NOMAC;
}

/*
 * Used to get the mac address of a device.
 */
int l2_get_mac( int device )
{
    int mac;

    if (device == E_NODEV)
	return E_NOMAC;

    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
	if( m_to_d[mac].phys_device == device )
	    return mac;
    }

    return E_NODEV;
}

/*
 * Used for setting timeouts for frames which have been sent with l2_real_send.
 */
void l2_register_send_timeout(int timeout, int dest_mac_addr, int id)
{
    int intparam[2];
    void *param;

    intparam[0] = dest_mac_addr;
    intparam[1] = id;

    param = malloc(sizeof(int) * 2);
    memcpy(param, intparam, sizeof(int) * 2);

    register_timeout_cb(get_time_struct(timeout), &resend_frame, param);
}

/*
 * Return the first available position in the ring buffer to store 
 * the l1buffer.
 * Returns E_FULL if there are MAX_WINDOW_SIZE windows in the ring buffer.
 */
int get_new_buffer_position(int mac)
{
    if (num_frames_in_window(mac) == MAX_WINDOW_SIZE)
	return E_FULL;
    else {
	BUF_INC(BUF_LAST(mac));
	return BUF_LAST(mac);
    }
}

/*
 * Removes all frames from position from the beginning of the send
 * buffer to position ackid for the recipient of the given device.
 */
void receive_ack(int device, int ackid)
{
    int id, mac;

    mac = M_REM(l2_get_mac(device));

    if (!valid_id(ackid,mac))
	return;

    for(;;)
    {
        id = BUF_FIRST(mac);
	if (BUF_LEN(mac,id) > 0) {
	    free(m_to_d[mac].s_buf[id].buf);
	    BUF_LEN(mac,id) = 0;
	}

	if (id != BUF_LAST(mac)) {
	    BUF_INC(BUF_FIRST(mac));
        }
	if (id == ackid)
	    break;
    }
}

/*
 * Resend a frame after a timeout occured.
 * Set a new timer on it, if it has to be resent.
 */
void resend_frame(void *param)
{
    int *intparam;
    int id, mac;

    intparam = malloc(sizeof(int) * 2);
    memcpy(intparam, param, sizeof(int) * 2);
    mac = intparam[0];
    id = intparam[1];

    // Resend only if the frame has not already been acked, and hence its
    // length is zero.
    if (BUF_LEN(mac,id) > 0)
    {
	l1_send(m_to_d[mac].s_buf[id].device, m_to_d[mac].s_buf[id].buf, BUF_LEN(mac,id));
	l2_register_send_timeout(DEFAULT_TIMEOUT, mac, id);
    }
}

/* 
 * Check if the id is in the sliding window.
 * Returns 1 if true, and 0 if false.
 */ 
int valid_id(int id, int mac)
{
    if (id < 0 || id >= SL_BUFFER_SIZE)
	return 0;
    else if (BUF_LAST(mac) >= id && id >= BUF_FIRST(mac))
	return (BUF_LEN(mac,id) > 0) ? 1 : 0;
    else if (BUF_LAST(mac) <= id && BUF_FIRST(mac) <= id)
	return (BUF_LEN(mac,id) > 0) ? 1 : 0;
    else
	return 0;
}

/*
 * Returns the number of frames in the window.
 */
int num_frames_in_window(int mac)
{
    if (BUF_LAST(mac) >= BUF_FIRST(mac))
	return BUF_LAST(mac) - BUF_FIRST(mac) + 1;
    else
	return SL_BUFFER_SIZE + BUF_LAST(mac) - BUF_FIRST(mac);
}

/* Pass the connect function downwards */
int l2_connect(const char* hostname, int port)
{
    return l1_connect(hostname, port);
}

/* Return our layer's header size added to the one below. */
int l2_get_header_size() {
    return sizeof(struct L2Header) + l1_get_header_size();
}

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * In particular initialize all the data structures that you
 * need for link-layer error correction and flow control.
 */
void l2_init( int local_mac_addr, int device )
{
    int mac;

    static int first = 1;

    if (first)
    {
	for( mac=0; mac<MAX_ADDRESSES; mac++ )
	{
	    m_to_d[mac].remote_mac_addr = E_NOMAC;
	    m_to_d[mac].phys_device = E_NODEV;
	    // Set BUF_LAST(mac) one to -1, as the first time we enter
	    // l2_real_send(), the sequence id will be increased to 0.
	    BUF_LAST(mac) = -1;
	}

	first = 0;
    }


    m_to_d[local_mac_addr].remote_mac_addr = E_OWN;
    m_to_d[local_mac_addr].own = 1;
    m_to_d[local_mac_addr].phys_device = device;
}

/*
 * We have gotten an UP packet for a particular device from the remote host.
 * We have to remember that in our table that maps MAC addresses to devices.
 */
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_addr )
{
    int mac;
    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        if( m_to_d[mac].phys_device == device )
        {
            m_to_d[mac].remote_mac_addr = other_mac_addr;
            l3_linkup( other_hostname, other_port, other_mac_addr );
            return;
        }
    }
    errx(-1, "Programming error in establishing a physical link\n");
}

/*
 * Called by layer 3, network, when it wants to send data to a
 * node identified by the MAC address.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 */
int l2_send(int dest_mac_addr, char* buf, int length )
{
    int   device = -1;
    char* l1buf;
    struct L2Header*  hdr;
    int   retval;
    int l1buflen;
    int id;
    int src_mac_addr;

    src_mac_addr = src_mac_lookup(dest_mac_addr);

    if ((id = get_new_buffer_position(dest_mac_addr)) == E_FULL)
    {
	return E_TRYAGAIN;
    }


    device = m_to_d[src_mac_addr].phys_device;

    l1buflen = length + sizeof(struct L2Header);

    l1buf = malloc(l1buflen);

    if( l1buf == NULL)
        err(-1,"Not enough memory in l2_send.\n");

    memcpy( &l1buf[sizeof(struct L2Header)], buf, length );

    hdr = (struct L2Header*)l1buf;
    hdr->type = DATA;
    hdr->src_mac_addr = htonl(src_mac_addr);
    hdr->dest_mac_addr = htonl(dest_mac_addr);
    hdr->id = htons((uint16_t) id);

    retval = l1_send(device, l1buf, l1buflen);

    m_to_d[dest_mac_addr].s_buf[id].buf = l1buf;
    m_to_d[dest_mac_addr].s_buf[id].len = l1buflen;
    m_to_d[dest_mac_addr].s_buf[id].device = device;

    l2_register_send_timeout(DEFAULT_TIMEOUT, dest_mac_addr, id);

    if( retval < 0 )
        return -1;
    else
	return retval-sizeof(struct L2Header);
}

/* Send ack for frame given with id to the device connected to the given device. */
void l2_send_ack(int device, int id)
{
    struct L2Header* hdr;
    int src_mac_addr;

    src_mac_addr = l2_get_mac(device);

    hdr = malloc(sizeof(struct L2Header));

    device = m_to_d[src_mac_addr].phys_device;
    hdr->type = ACK;
    hdr->src_mac_addr = htonl(src_mac_addr);
    hdr->dest_mac_addr = htonl(M_REM(src_mac_addr));
    hdr->id = htons((uint16_t) id);

    // Blindy send the ack. If the recipient doesn't receive it, it will send the
    // packet again, and we'll try acking again.
    l1_send(device, (char *)hdr, sizeof(struct L2Header));
    
    free(hdr);
}

/*
 * Called by layer 1, physical, when a frame has arrived.
 * If the packet is addressed to this host, then it will be sent upwards
 * to layer 3, if not it will either be forwarded if the recipient
 * is known, or flooded out.
 * This function has no return value.
 */
void l2_recv( int device, char* buf, int length )
{
    const struct L2Header* hdr;
    char* l3buf;
    int	id,l3len;
    int dest_mac_addr, src_mac_addr; // To and from from the l2 header
    int from, to; //The real to and from addresses

    hdr = (struct L2Header*)buf;
    src_mac_addr  = ntohl(hdr->src_mac_addr);
    dest_mac_addr  = ntohl(hdr->dest_mac_addr);
    id = (int) ntohs(hdr->id);
    l3buf = buf + sizeof(struct L2Header);
    l3len = length - sizeof(struct L2Header);

    to = l2_get_mac(device);
    from = M_REM(to);
    if (hdr->type == DATA)
    {
	// This is the frame with the sequence id we want.
	if (id == m_to_d[from].waitfor)
	{
	    // If l3_recv returns false, we don't ack the frame and hence the
	    // remote site will have to resend the frame.
            if (l3_recv(src_mac_addr, l3buf, l3len)) {
                l2_send_ack(device, id);
                BUF_INC(m_to_d[from].waitfor);
            }
	} else {
	    // If the frame has an id which is less than MAX_WINDOW_SIZE away from
	    // 'waitfor', we reack the frame, as our ack has been lost, but
	    // ignore the content of the frame.
	    int diff = m_to_d[from].waitfor - id;
	    if ((diff >= 0 && diff < MAX_WINDOW_SIZE) || diff + SL_BUFFER_SIZE < MAX_WINDOW_SIZE) {
		l2_send_ack(device, id);
	    }
	}
    } else if (hdr->type == ACK)
    {
	receive_ack(device, id);
    }
}
