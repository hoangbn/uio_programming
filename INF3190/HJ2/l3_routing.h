#ifndef L3_ROUTING_H
#define L3_ROUTING_H

#include "general.h"

#define PING_INTERVAL       15
#define LSA_INTERVAL        15
#define QUEUE_INTERVAL      4

/*
 * This variable contains the host name of this machine.
 * It must be initialized at startup time.
 */
int own_ip;

struct L3Ping {
    uint32_t time;
};

struct L3LSAneighbour {
    uint16_t ip;
    uint16_t delay;
};

struct L3LSA {
    uint32_t generation; // Increases by one for each sending
    uint8_t neighbours;  // Numbers of neighbours in the LSA.
};

struct node {
    uint32_t generation; // Last seen generation of LSA
    uint16_t delay;
};

struct queue_t {
    int dst_addr;
    int dst_mac_addr;
    int src_addr;
    char *buf;
    int length;
    char type;
    struct queue_t *next;
    struct queue_t *previous;
};

void l3_init(int self);
void l3_send_ping(int dst_addr);
void l3_send_pong(int dst_addr, int src_mac_addr, const char *buf, int length);
void l3_recv_pong(int src_addr, int src_mac_addr, const char *buf);
void l3_send_lsa(void);
void l3_recv_lsa(int src_mac_addr, const char *buf, int length);
void l3_ping_all(void);
void l3_run_queue(void);
void l3_add_to_queue(int dst_addr, int dst_mac_addr, int src_addr, char *buf, int length, char type);
int l3_route(int src_addr, int dst_addr, const char* buf, int length);
int l3_get_route(int dst_addr);
void shortest_path(int s, int t);
void dijkstra(void);
#endif /* L3_ROUTING_H */
