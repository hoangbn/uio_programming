#ifndef L2_LINK_H
#define L2_LINK_H

#include <sys/types.h>

#define ACK		    'A'
#define DATA		    'D'
#define NACK		    'N'

#define E_FULL		    -1
#define E_NOMAC		    -2
#define E_OWN		    -3
#define E_NODEV		    -4

#define MAX_WINDOW_SIZE	    10
#define	MAX_FLOODQUEUE_SIZE  10000 // Should be sufficient..
#define SL_BUFFER_SIZE	    MAX_WINDOW_SIZE * 3

#define BUF_INC(id)	    if (1) { id++; if (id == SL_BUFFER_SIZE) id = 0; }
#define BUF_FIRST(mac)	    m_to_d[mac].sb_first
#define BUF_LAST(mac)	    m_to_d[mac].sb_last
#define BUF_LEN(mac,id)	    m_to_d[mac].s_buf[id].len

#define M_REM(mac)	    m_to_d[mac].remote_mac_addr
#define M_OWN(mac)	    m_to_d[mac].own

#define	M_PDEV(mac)	    m_to_d[mac].phys_device

/*
 * Struct to hold the content and length of a buffer in the window.
 */
struct WindowEntry
{
    char* buf;
    int	len;
    int device;
};
typedef struct WindowEntry window_entry_t;

/*
 * This struct is meant to keep information about the local
 * physical layer device that must be used to reach a device
 * with a given remote MAC address.
 * The local device does have an own MAC address as well, of
 * course, even though we don't ever check whether a frame
 * has actually been sent to it or to another MAC address.
 */
struct LinkEntry
{
    int remote_mac_addr;
    int phys_device;
    int waitfor;    /* The sequece number the receiving side is currently waiting for. */
    int sb_first;   /* Position of the first in the sliding buffer. */
    int sb_last;    /* Position of the last in the sliding buffer. */
    int own;	    /* If the mac belong to this node. */
    window_entry_t s_buf[SL_BUFFER_SIZE]; /* A sliding buffer for each link */
};
typedef struct LinkEntry link_entry_t;


/*
 * The MAC header. It is included in every frame.
 */
struct L2Header
{
        int src_mac_addr;
        int dest_mac_addr;
        uint16_t id;
        char type;
};

/* For comments about each function see l2_link.c */

int src_mac_lookup(int dest_mac_addr);
int get_new_buffer_position(int mac);
struct timeval get_time_struct(int sec);
int num_frames_in_window(int mac);
void receive_ack(int mac, int id);
void resend_frame(void *param);
int valid_id(int id, int mac);
int l2_connect( const char* hostname, int port );
int l2_get_header_size();
int l2_get_mac( int device );
void l2_init( int local_mac_addr, int device );
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_addr );
int  l2_send( int mac_addr, char* buf, int length );
void l2_send_ack(int device, int id);
void l2_recv( int device, char* buf, int length );
void l2_register_send_timeout(int timeout, int dest_mac_addr, int id);

#endif /* L2_LINK_H */
