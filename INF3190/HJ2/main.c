#include <err.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "irq.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "l3_routing.h"
#include "l4_trans.h"
#include "l5_app.h"

int main( int argc, char* argv[] )
{
    int          udp_socket_port;
    int          mac;
    int          phys_device;
    int		 i;

    if( argc < 3 )
    {
        fprintf( stderr, "Usage: %s <port> <id> <id>...\n"
                         "       <port> is the UDP port used on this machine\n"
                         "       <id> is the fake MAC address of this machine, one machine can have several\n",
                         argv[0] );
        exit( -1 );
    }

    /*
     * Read information from the command line. This is very primitive.
     * Refine as you see fit.
     */
    udp_socket_port       = atoi(argv[1]);

    if (udp_socket_port < 1024 )
        errx(-1, "Can not bind to port %d, as it is less than 1024", udp_socket_port);

    phys_device        = 0;

    /*
     * Initialize all layers. This can include setting up all the
     * network connections, but it doesn't have to. It is also OK
     * to send connect-requests and handle the responses later, in
     * the handle_events loop. Your choice.
     */
    l1_init( udp_socket_port );
    /* Initialize layers 2, 3 and 5 for each unique MAC on the command line */
    mac = atoi(argv[2]);

    for(i=2; i < argc; i++)
    {
	mac = atoi(argv[i]);
	l2_init( mac, phys_device++ );
        l3_init( mac );
	l5_init( mac, udp_socket_port );
    }
    l4_init( );

    /*
     * An endless loop for processing everything that happens on this
     * machine.
     */
    handle_events( );

    return 1;
}

