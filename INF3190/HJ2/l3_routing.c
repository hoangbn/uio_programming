#include <sys/time.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <arpa/inet.h>

#include "general.h"
#include "irq.h"
#include "l3_net.h"
#include "l3_routing.h"

// Used in the Dijkstra algorithm from the book.
#define dist(i,j)           nodes[i][j].delay
#define INFINITY            1000000000 // Bogus infinity value.

// Stores all the import info about each node, that is the delay and the
// generation.
struct node nodes[MAX_ADDRESSES][MAX_ADDRESSES];

// Our routing table. Specifies which directly connected node to use.
int r_table[MAX_ADDRESSES];

// The l3 routing tail-head-queue.
struct queue_t *q_head = NULL;
struct queue_t *q_tail = NULL;

// current LSA generation.
uint32_t LSAgen = 1;

/*
 * The network layer needs to maintain private information about
 * the link that leads to a host address. This map is only used for
 * direct mappings, i.e. it is not affected by the routing.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static int host_to_mac_map[MAX_ADDRESSES];

/*
 * Call at the start of the program. Sets the own host address and initialzies
 * the host to mac table.
 */
void
l3_init(int ip)
{
    static int first = 1;

    if (first) {

        int i,j;

        own_ip= ip;

        for (i=0; i<MAX_ADDRESSES; i++)
            host_to_mac_map[i] = E_NOMAC;

        for (i=0; i < MAX_ADDRESSES; i++) {
            for (j=0; j < MAX_ADDRESSES; j++) {
                nodes[i][j].generation = 0;
                dist(i,j) = 0;
            }
        }

        // Start the pinger, queue runner and the LSA broadcasting timers.
        register_timeout_cb(get_time_struct(5), &l3_ping_all, NULL);
        register_timeout_cb(get_time_struct(5), &l3_run_queue, NULL);
        register_timeout_cb(get_time_struct(5), &l3_send_lsa, NULL);
        first = 0;
    };

    host_to_mac_map[ip] = own_ip;
}

/*
 * Find and return the associated host for a mac address.
 */
int
l3_mac_to_host(int mac)
{
    int i;

    for (i=0; i < MAX_ADDRESSES; i++) {
        if (host_to_mac_map[i] == mac)
            return i;
    }

    return E_NOHOST;
}

/*
 * Returns the mac address for use when routing an address.
 * Returns E_NOHOST on error.
 */
int
l3_get_route(int dst_addr)
{
    return (r_table[dst_addr] > 0) ? r_table[dst_addr] : E_NOHOST;
}

/*
 * Send a ping to the destination given.
 */
void
l3_send_ping(int dst_addr)
{
    struct L3Ping *buf = malloc(sizeof(struct L3Ping));
    struct timeval tv = get_time_struct(0);

    buf->time = htonl((uint32_t)tv.tv_sec);
    //DEBUG("dst_addr  = %d, dst_mac_addr = %d, own_ip = %d\n", dst_addr,host_to_mac_map[dst_addr],own_ip);
    
    // Put it in the sending queue on failure.
    if ((l3_direct_send(dst_addr, host_to_mac_map[dst_addr], own_ip,(const char *) buf,
        sizeof(struct L3Ping), PING)) == E_TRYAGAIN) {
        l3_add_to_queue(dst_addr, host_to_mac_map[dst_addr], own_ip,(char *) buf, sizeof(struct L3Ping), PING);
    } else {
        free(buf);
    }
   
}

/*
 * Answer a ping, that is send a pong.
 */
void
l3_send_pong(int dst_addr, int dst_mac_addr, const char *buf, int length)
{
    char *bufcopy;

    bufcopy = malloc(length);
    memcpy(bufcopy, buf, length);

    //DEBUG("Ponging %d via %d, length = %d\n", dst_addr, dst_mac_addr, length);
    // Put it in the sending queue on failure.
    if ((l3_direct_send(dst_addr, dst_mac_addr, own_ip, bufcopy, length, PONG)) == E_TRYAGAIN) {
        l3_add_to_queue(dst_addr, dst_mac_addr, own_ip, bufcopy, length, PONG);
    } else {
        free(bufcopy);
    }
}

/*
 * Receives and updates the corresponding node with the new delay.
 */
void
l3_recv_pong(int src_addr, int mac_addr, const char *buf)
{
    struct L3Ping *p = (struct L3Ping*) buf;
    struct timeval tv = get_time_struct(0);
    uint16_t rtt;
    
    // Use the pong packets to map "ip" to mac, this is neccessary, because
    // when we first meet the new node in l3_linkup, we only have the mac address
    // and hence we send a ping to get the ip in a pong.
    if (host_to_mac_map[src_addr] == E_NOMAC) {
        host_to_mac_map[src_addr] = mac_addr;
    }

    // This cast will only be a problem if the delay is more than 18 hours..
    rtt = (uint16_t) ((uint32_t)tv.tv_sec) - ntohl(p->time);

    //DEBUG("RTT from %d to %d is %d s.\n", src_addr, own_ip, rtt);
    nodes[own_ip][src_addr].delay = rtt;
    
    // Ensure that the direct link is properly configured, else it is void
    // to run dijkstra.
    if (dist(src_addr,own_ip) > 0)
        dijkstra();
}

/*
 * Pings a directly connected nodes.
 */
void
l3_ping_all(void)
{
    int i;

    for (i=0; i < MAX_ADDRESSES; i++) {
        if (host_to_mac_map[i] != E_NOMAC && host_to_mac_map[i] != own_ip) {
            l3_send_ping(i);
        }
    }

    register_timeout_cb(get_time_struct(PING_INTERVAL), &l3_ping_all, NULL);
}


void
l3_recv_lsa(int src_mac_addr, const char* buf, int length)
{
    char *bufcopy;
    int i,l2len;
    uint16_t dst_addr,src_addr,ip;
    uint32_t generation;
    struct L3Header *hdr = (struct L3Header*) buf;
    dst_addr = ntohs(hdr->dst_addr);
    src_addr = ntohs(hdr->src_addr);

    struct L3LSA *lsa = (struct L3LSA*) (buf + sizeof(struct L3Header));
    generation = ntohl(lsa->generation);
    struct L3LSAneighbour *neighbour = (struct L3LSAneighbour*) (((char *)lsa) + sizeof(struct L3LSA));

    l2len = length - sizeof(struct L3Header);
    DEBUG("Recieved LSA from IP %d via %d, with length %d, containing %d nodes.\n", src_addr, src_mac_addr, length, lsa->neighbours);
    
    for (i=0; i < lsa->neighbours; i++) {
        //DEBUG("Lsa bor p� %p, og neighbour p� %p\n", &lsa,&neighbour);
        ip = ntohs(neighbour->ip);
        DEBUG("Trying to update for (%d,%d)\n", src_addr, ip); 
        // Not a valid generation, hence the node hasn't been activated yet.
        if (nodes[src_addr][ip].generation == 0) {
            // Don't announce our own interfaces.
            if (host_to_mac_map[ip] != own_ip)
                printf("Discovered new node: %d\n", ip);
            if (host_to_mac_map[src_addr] != own_ip)
                printf("Discovered new node: %d\n", src_addr);

            nodes[src_addr][ip].generation = generation;
            // Indicates the next one we are waiting for.
            nodes[src_addr][ip].generation++;
            nodes[src_addr][ip].delay = ntohs(neighbour->delay);
            // Only update the info if it contains the generation we are waiting for.
        } else if (nodes[src_addr][ip].generation == generation) {
            DEBUG("Generation OK, for (%d,%d). updating delay.\n", src_addr,ip);
            nodes[src_addr][ip].delay = ntohs(neighbour->delay);
            nodes[src_addr][ip].generation++;
        } else {
            // Don't bother to broadcast LSA packets we already have seen.
            DEBUG("Returning...\n");
            return;
        }
             
        neighbour = neighbour + 1;
    }

    // Forward the buffer to the directly connected hosts, but avoid
    // pushing it back to the one we got it from.
    for (i=0; i < MAX_ADDRESSES; i++) {
        if (host_to_mac_map[i] != E_NOMAC && host_to_mac_map[i] != own_ip &&
                host_to_mac_map[i] != src_mac_addr) {
            DEBUG("Redirecting LSA to host %d with mac %d\n", i, host_to_mac_map[i]);
            bufcopy = malloc(l2len);
            memcpy(bufcopy, buf+sizeof(struct L3Header), l2len);
            // Put it in the sending queue on failure.
            if ((l3_direct_send(dst_addr, host_to_mac_map[i], src_addr,(const char *) bufcopy,l2len, LSA)) == E_TRYAGAIN)
                l3_add_to_queue(dst_addr, host_to_mac_map[i], src_addr,bufcopy, l2len, LSA);
        }
    }
    // Rerun dikjstra after receiving new information.
    dijkstra();
}

/* 
 * Broadcast all our directly connected nodes to all our directly connected.
 * The packet sent contains the generation, number of neighbours and the same
 * number of structs containg each edge and delay.
 */
void
l3_send_lsa(void)
{
    int i, numneigh;
    char *buf, *bufcopy;

    numneigh = 0;

    for (i=0; i < MAX_ADDRESSES; i++) {
        if (dist(own_ip,i))
            numneigh++;
    }

    // Don't need to send out LSA packet unless we are connected. 
    if (numneigh) {

        // First create the LSA packet
        uint16_t LSAsize = sizeof(struct L3LSA) + (sizeof(struct L3LSAneighbour) * numneigh);
        buf = malloc(LSAsize);
        struct L3LSA *lsa = (struct L3LSA*) buf;
        lsa->generation = htonl(LSAgen);
        lsa->neighbours = (uint8_t) numneigh;
        struct L3LSAneighbour *neighbour = (struct L3LSAneighbour*) (buf + sizeof(struct L3LSA));

        // Add all nodes which are verified.
        for (i=0; i < MAX_ADDRESSES; i++) {
            if (dist(own_ip,i)) {
                //DEBUG("own_ip = %d, i = %d\n", own_ip,i);
                neighbour->ip = htons((uint16_t) i);
                neighbour->delay = htons(nodes[own_ip][i].delay);
                //DEBUG("neighbour->delay = %d\n", ntohs(neighbour->delay));
                neighbour = neighbour + 1;
            }
        }
	
        // Then distribute to all directly connected hosts.
        for (i=0; i < MAX_ADDRESSES; i++) {
            if (host_to_mac_map[i] != E_NOMAC && host_to_mac_map[i] != own_ip) {
                bufcopy = malloc(LSAsize);
                memcpy(bufcopy, buf, LSAsize);
                // Put it in the sending queue on failure.
                if ((l3_direct_send(i, host_to_mac_map[i], own_ip, (const char *) buf,LSAsize, LSA)) == E_TRYAGAIN)
                    l3_add_to_queue(i, host_to_mac_map[i], own_ip, buf, LSAsize, LSA);
            }
        }

        free(buf);
        LSAgen++;
    }

    register_timeout_cb(get_time_struct(LSA_INTERVAL), &l3_send_lsa, NULL);
}

int
l3_route(int src_addr, int dst_addr, const char* buf, int length)
{
    // Reject packets which we can not route.
    if (r_table[dst_addr] == 0)
        return -1;
    
    char *bufcopy;
    bufcopy = malloc(length);
    memcpy(bufcopy, buf, length);
    // Put it in the sending queue on failure.
    if ((l3_direct_send(dst_addr, r_table[dst_addr], src_addr, (const char *) bufcopy,length, DATA)) == E_TRYAGAIN)
        l3_add_to_queue(dst_addr, r_table[dst_addr], src_addr, bufcopy, length, DATA);

    return SUCCESS;
}

/* 
 * Add a new element to the l3 routing queue, which later will be processed
 * by l3_run_queue().
 */
void
l3_add_to_queue(int dst_addr, int dst_mac_addr, int src_addr, char *buf, int length, char type)
{
    //DEBUG("adding addr %d with type %c to the queue\n", dst_addr, type);
    struct queue_t *q;
    
    q = malloc(sizeof(*q));

    q->dst_addr = dst_addr;
    q->dst_mac_addr = dst_mac_addr;
    q->src_addr = src_addr;
    q->buf = buf;
    q->length = length;
    q->type = type;

    // Queue is empty.
    if (q_tail == NULL) {
        q_tail = q;
        q_head = q;
        q->previous = NULL;
        q->next = NULL;
    } else {
        q_tail->next = q;
        q->previous = q_tail;
        q_tail = q;
        q->next = NULL;
    }
}

/* Removes and frees element q from the l3 routing queue. */
void
l3_remove_from_queue(struct queue_t *q)
{
    if (q->previous == NULL) {
        q_head = q->next;
    } else
        q->previous->next = q->next;

    if (q->next == NULL)
        q_tail = q->previous;

    free(q->buf);
    free(q);
}

/*
 * Loops through the entire queue, and tries to push down each element to l2.
 * Runs periodically with QUEUE_INTERVAL seconds intertval.
 */
void
l3_run_queue(void)
{

    struct queue_t *q = q_head;
    struct queue_t *n = NULL; // The next element
    while (q != NULL)
    {   
        n = q->next;
        DEBUG("Trying to send to %d via mac %d, with length %d and with type %c.\n", q->dst_addr, q->dst_mac_addr, q->length,q->type);
        // Remove from the queue if successfully pushed down to l2.
        if ((l3_direct_send(q->dst_addr, q->dst_mac_addr, q->src_addr,q->buf, q->length, q->type)) == q->length)
        {
            l3_remove_from_queue(q);
        }
        q = n;
    }
    // Reschedule another queue run.
    register_timeout_cb(get_time_struct(QUEUE_INTERVAL), &l3_run_queue, NULL);
}


/* 
 * Wrapper around the Dijkstra implementation in shortest_path(), in
 * which we run it for each valid node.
 */
void
dijkstra(void)
{
    int i,j;

    for (i=0; i < 20; i++) {
        for (j=0; j < 20; j++) {
            if (dist(i,j))
                DEBUG("dist(%d,%d) = %d\n", i,j,dist(i,j));
        }
    }

    for (i=0; i < MAX_ADDRESSES; i++) {
        if (host_to_mac_map[i] != E_NOMAC && host_to_mac_map[i] != own_ip) {
            DEBUG("Starter dijkstra for node %d\n", i); 
            shortest_path(own_ip, i);
            DEBUG("r_table[%d] = %d\n", i, r_table[i]); 
        }
    }
}

/* 
 * Dijkstra from the book on page 356.
 * s is the source, t is the target
 */
void
shortest_path(int s, int t)
{
    struct state {
        int predecessor;
        int length;
        enum {permanent, tentative} label;
    } state[MAX_ADDRESSES];

    int i,k,min;
    struct state *p;

    for (p =&state[0]; p < &state[MAX_ADDRESSES]; p++) { /* initialize state */
        p->predecessor = -1;
        p->length = INFINITY;
        p->label = tentative;
    }

    state[t].length = 0; state[t].label = permanent;
    k = t;                                  /* k is the initial working node */
    do {                                    /* Is there a better path from k? */
        for (i=0; i < MAX_ADDRESSES; i++) { /* this graph has MAX_ADDRESSES nodes */

            /* Abort if nodes have not been pinged in both directions,
             * otherwise we will have an eternal loop*/
            if (!(dist(k,i) && dist(i,k))) {
                DEBUG("Blame (%d,%d) and (%d,%d)\n", i,k,k,i);
                return;
            }
            
            if (dist(k,i) != 0 && state[i].label == tentative) {
                DEBUG("L�per inni med dist(%d,%d) = %d\n", k,i, dist(k,i));
                if (state[k].length + dist(k,i) < state[i].length) {
                    state[i].predecessor = k;
                    state[i].length = state[k].length + dist(k,i);
                }
            }
        }
        /* Find the tentatively labeled node with the smallest label. */
        k = 0; min = INFINITY;
        for (i=0; i < MAX_ADDRESSES; i++) {
            if (state[i].label == tentative && state[i].length < min) {
                min = state[i].length;
                k=i;
            }
        }
        state[k].label = permanent;
        DEBUG("k=%d, s = %d, min = %d, i = %d\n", k,s,min,i);
    } while (k != s);

    /* Copy the path into the routing table */
    k = s;
    r_table[t] = host_to_mac_map[state[k].predecessor];
}
