#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "l1_phys.h"
#include "l2_link.h"
#include "l3_net.h"
#include "l5_app.h"
#include "irq.h"

#define MAX_ADDRESSES 1024


/*
 * The MAC header. It is included in every frame.
 * You must extend it for your needs.
 */

int local_mac_addr;
int ackUsed;
int timeID;

void sendACK(int destination, int source, int which);


/*A = ACK, D = Data and N = NACK*/
struct L2Header
{
  int src_mac_address;
  int dst_mac_address;
  char type;
  int ack_id;
};

struct timeval get_time(int s)
{
    struct timeval time;
    gettimeofday(&time, 0);

    time.tv_sec += s;

    return time;
}

struct Param 
{
  int src_mac_address;
  int dst_mac_address;
}params;

/*
 * The network layer needs to maintain private information about
 * the MAC address at the other end of every link.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static link_entry_t mac_to_device_map[MAX_ADDRESSES];

/* int validACK(int which) { */
 

 
int getL2HeaderSize()
{
  return sizeof(struct L2Header);
}

/* } */
/*******************************************************************************
* Function name:    void resetACK()
* Parameters:       No Parameters
*  
* returns:          No returns                                 
* Description:      reset ACKs that has been used
************'******************************************************************/
void resetACK()
{
  ackUsed = 0;
}

/*******************************************************************************
* Function name:    void receiveACK(int which)
* Parameters:       int which
*  
* returns:          1 on success
*                   0 on failure                                
* Description:      receive ACK and determine if this is valid or not
************'******************************************************************/
int receiveACK(int which)
{

  return 1;

}

void sendACKAgain(int ack)
{
  fprintf(stderr, "Sending previous ACK: %d\n", ack);
  sendACK(params.src_mac_address, params.dst_mac_address, ack); 
}



/*******************************************************************************
* Function name:    void sendACK(int destination, int source, int which)
* Parameters:       int destination, int source, int which
*  
* returns:          No returns
*                                         
* Description:      send an ACK to tell that it has received the package successfully
************'******************************************************************/
void sendACK(int destination, int source, int which){
  struct L2Header* header;
  int device;

  header = malloc(sizeof(struct L2Header));
  device = mac_to_device_map[source].phys_device;
  header->type = 'A';
  header->ack_id = which;
  header->src_mac_address = htonl(source);
  header->dst_mac_address = htonl(destination);

  params.src_mac_address = htonl(source);
  params.dst_mac_address = htonl(destination);
  // params->ack_id = which;

  /*cast struct to char array and send it out*/
  l1_send(device, (char*) header, sizeof(struct L2Header));
/*   timeID = register_timeout_cb(get_time(1), &sendACKAgain ,&which); */
  free(header);
}


/*******************************************************************************
* Function name:    void l2_init( int local_mac_address, int device )
* Parameters:       int local_mac_address, int device
*  
* returns:          No returns
*                                         
* Description:     Call at the start of the program. Initialize data structures
*                  like an operating system would do at boot time.
*******************************************************************************/
void l2_init( int local_mac_address, int device )
{
    int mac;
    
    ackUsed = 0;
    local_mac_addr = local_mac_address;
    
    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        mac_to_device_map[mac].remote_mac_address = -1;
        mac_to_device_map[mac].phys_device        = -1;
    }

    mac_to_device_map[local_mac_address].remote_mac_address = -1;
    mac_to_device_map[local_mac_address].phys_device        = device;
}

/*******************************************************************************
* Function name:    int getMacAddress()
* Parameters:       No parameters
*  
* returns:          returns a positive value
*                                         
* Description:      get MAC address
*******************************************************************************/
int getMacAddress()
{
  return local_mac_addr;
}


/*******************************************************************************
* Function name:    void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address )
* Parameters:       int device, const char* other_hostname, int other_port, int other_mac_address
*  
* returns:          returns a positive value
* error:            exit on errors                                        
* Description:      maps MAC addresses to devices 
*******************************************************************************/
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address )
{

    int mac;
    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        if( mac_to_device_map[mac].phys_device == device )
        {
            mac_to_device_map[mac].remote_mac_address = other_mac_address;
            l3_linkup( other_hostname, other_port, other_mac_address );
            return;
        }
    }
    fprintf( stderr, "Programming error in establishing a physical link\n" );
    exit( -1 );
}

void sendAgain(int id)
{
  gotNack();
}


 /*
 * NOTE:
 * You will need to split this function into many small helper
 * functions. In particular, you will need something that allows
 * you to perform retransmissions after a timeout.
 */


/*******************************************************************************
* Function name:    int l2_send( int dest_mac_addr, const char* buf, int length )
* Parameters:       int dest_mac_addr, const char* buf, int length
*  
* returns:          positive value on success
*                   -1 on failure
* error:            exit on errors                                        
* Description:      send data to a direct neighbour identified by the MAC address.
*******************************************************************************/
int l2_send( int dest_mac_addr, const char* buf, int length )
{
    int   device = -1;
    int   src_mac_addr = -1;
    char* l1buf;
    struct L2Header*  hdr_pointer;
    int   retval;
    int   i;

    for( i=0; i<MAX_ADDRESSES; i++ )
    {
        if( mac_to_device_map[i].remote_mac_address == dest_mac_addr )
        {
            device       = mac_to_device_map[i].phys_device;
            src_mac_addr = i;
            break;
        }
    }
    if( i==MAX_ADDRESSES )
    {
        fprintf( stderr, "MAC address not found in l2_send" );
        return -1;
    }

    l1buf = (char*)malloc( length+sizeof(struct L2Header) );
    if( l1buf == 0 )
    {
        fprintf( stderr, "Not enough memory in l2_send\n" );
        return -1;
    }

    memcpy( &l1buf[sizeof(struct L2Header)], buf, length );

    hdr_pointer = (struct L2Header*)l1buf;
    hdr_pointer->src_mac_address = htonl(src_mac_addr);
    hdr_pointer->dst_mac_address = htonl(dest_mac_addr);
    hdr_pointer->type = 'D';
    hdr_pointer->ack_id = ackUsed; 
    
    printf("Ack: %d sended\n", hdr_pointer->ack_id);
    
    retval = l1_send( device, l1buf, length+sizeof(struct L2Header) );
    free(l1buf);
    
    int param = 0;
    

    timeID = register_timeout_cb(get_time(1), &sendAgain ,&param);

    if( retval < 0 )
    {
        return -1;
    }
    else
    {

      return retval-sizeof(struct L2Header);
    }
}

/*
 * Called by layer 1, physical, when a frame has arrived.
 *
 * This function has no return value. It must handle all
 * problems itself because the physical layer isn't able
 * to handle errors.
 *
 * NOTE:
 * Link layer error correction and flow control must be considered
 * here. You will certainly need several helper functions because
 * you will need to perform retransmissions after a timeout.
 */


/*******************************************************************************
* Function name:    void l2_recv( int device, const char* buf, int length )
* Parameters:       int device, const char* buf, int length
*  
* returns:          positive value on success
*                   -1 on failure
* error:            exit on errors                                        
* Description:      send data to a direct neighbour identified by the MAC address.
*******************************************************************************/
void l2_recv( int device, const char* buf, int length )
{
    const struct L2Header* hdr_pointer;
    const char*            l3buf;
    int                    src_mac_address;
    int                    dst_mac_address;
/*     int                    waitAck; */
    int                    whichAck;
    int                    err;

    hdr_pointer = (const struct L2Header*)buf;
    src_mac_address  = ntohl(hdr_pointer->src_mac_address);
    dst_mac_address  = ntohl(hdr_pointer->dst_mac_address);
    whichAck = hdr_pointer->ack_id;

    /*ACK frame*/
    if (hdr_pointer->type == 'A') {
      if(whichAck == -1){
	printf("Finished receiving\n");
	remove_timeout( timeID );
	resetACK();
      }
      if(ackUsed == whichAck){
	ackUsed++;
	printf("Got ack, ACK %d\n", whichAck);

	if(done){
	  printf("Finished sending\n");
	  remove_timeout( timeID );
	  doneSending();
	} else {
	  remove_timeout( timeID );
	  gotAck();
	}
      } else {
	printf("Gotten this ack before, ACK %d\n", whichAck);
      }
      /*Data frame*/
    } else if(hdr_pointer->type == 'D'){
      fprintf(stderr, "ACK: %d, whichACK: %d\n", ackUsed, whichAck);
      if(ackUsed == whichAck){
	ackUsed++;
      
	l3buf = buf + sizeof(struct L2Header);
	err = l3_recv( dst_mac_address, l3buf, length-sizeof(struct L2Header) );
	if(err <= 0){
	  ackUsed--;
	  fprintf(stderr, "couldn't receive data\n");
	} else {
	  printf("Sending Ack: %d\n", whichAck);
	  sendACK(dst_mac_address,dst_mac_address, whichAck);
	}
      } else {
	fprintf(stderr, "Got package %d before!, sending ACK %d again..\n", whichAck, whichAck);
	sendACK(dst_mac_address,dst_mac_address, whichAck);
      }
    }
}
