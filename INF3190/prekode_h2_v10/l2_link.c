#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "irq.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "l2_flow.h"
#include "l3_net.h"

#define MAX_ADDRESSES 1024

/*
 * The network layer needs to maintain private information about
 * the MAC address at the other end of every link.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static link_entry_t mac_to_device_map[MAX_ADDRESSES];

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * In particular initialize all the data structures that you
 * need for link-layer error correction and flow control.
 */
void l2_init( int local_mac_address, int device )
{
    static int first = 1;

    if( first )
    {
        l2_flow_init( mac_to_device_map );

        int mac;
        for( mac=0; mac<MAX_ADDRESSES; mac++ )
        {
            mac_to_device_map[mac].remote_mac_address = -1;
            mac_to_device_map[mac].phys_device        = -1;
        }
        first = 0;
    }

    mac_to_device_map[local_mac_address].remote_mac_address = -1;
    mac_to_device_map[local_mac_address].phys_device        = device;

}

/*
 * We have gotten an UP packet for a particular device from the remote host.
 * We have to remember that in our table that maps MAC addresses to devices.
 */
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address )
{
    int mac;
    mac = l2_get_mac( device );

    mac_to_device_map[mac].remote_mac_address = other_mac_address;
    l2_flow_linkup( device, other_hostname, other_port, other_mac_address );

    l3_linkup( other_hostname, other_port, other_mac_address );
}

/*
 * Used to get the mac address of a device.
 */
int l2_get_mac( int device )
{
    int mac;

    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        if( mac_to_device_map[mac].phys_device == device )
            break;
    }

    if( mac == MAX_ADDRESSES )
    {
        fprintf(stderr, "Programming error lookup of device\n" );
        exit( -1 );
    }

    return mac;
}

/*
 * This function decides if a frame should be sent up to the next layer, or be dropped. 
 * It works in the same way as in the previous assignment, apart from there is no bridging here
 */
int l2_frame_controller( int device, const char* buf, int length )
{
    const struct L2Header *hdr_pointer;
    const char* l3buf;
    int destination;
    int source;

    /* Pick out the L2Header if you want, could be an idea to check destination */
    hdr_pointer = (struct L2Header*) buf;
    destination = ntohl(hdr_pointer->dst_mac_address);
    source = ntohl(hdr_pointer->src_mac_address);
    
    l3buf = buf + sizeof(struct L2Header);
    return l3_recv(destination, l3buf, length-sizeof(struct L2Header));
}

/*
 * Called by layer 3, network, when it wants to send data to a
 * MAC address.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 * 
 */

int l2_send( int dest_mac_addr, const char* buf, int length )
{
    /* 
     * Will send the frame with the given flow control.
     *
     * The destination will be set by the network layer. 
     */
    return l2_flow_send(dest_mac_addr, buf, length);
}

/*
 * Called by layer 1, physical, when a frame has arrived.
 *
 * This function has no return value. It must handle all
 * problems itself because the physical layer isn't able
 * to handle errors.
 *
 */
void l2_recv( int device, const char* buf, int length )
{
    /* 
     * l2_flow_recv will handle the flow_control mechanism, before sending the frame
     * to l2_frame_controller
     */
    l2_flow_recv(device, buf, length);
}
