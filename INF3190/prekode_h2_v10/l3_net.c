#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "l2_link.h"
#include "l3_net.h"
#include "l4_trans.h"

#define MAX_ADDRESSES 1024

/*
 * This header is included in every network layer packet.
 *
 * Since you implement routing in this assignment, you should keep
 * in mind that routing protocols co-exist with data-carrying network
 * layer protocols. You must be able to distinguish these two types
 * of network layer packet.
 * It would also be better to implement the link state routing
 * algorithm in a separate file.
 */
struct L3Header
{
    int src_address;
    int dst_address;
};

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time. Set the own
 * host address.
 */
void l3_init( int self )
{
    /* ... */
}

/*
 * This function is called when we have received the MAC address of
 * a direct neighbour. This information is needed because routing
 * determines the next host on the path to a destination. For sending
 * to that one, we need its MAC address. So you must keep that
 * association somewhere.
 */
void l3_linkup( const char* other_hostname, int other_port, int other_mac_address )
{
    /* ... */
}

/*
 * Called by layer 4, transport, when it wants to send data to the
 * host identified by host_address.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 */
int l3_send( int dest_address, const char* buf, int length )
{
    /* ... */
    return -1;
}

/*
 * Called by layer 2, link, when it has received data and wants to
 * deliver it.
 * A positive return value means that all data has been delivered.
 * A zero return value means that the receiver can not receive the
 * data right now.
 * A negative return value means that an error has occured and
 * receiving failed.
 */
int l3_recv( int mac_address, const char* buf, int length )
{
    /* ... */
    return -1;
}

