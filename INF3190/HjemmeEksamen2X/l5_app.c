#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

#include "slow_receiver.h"
#include "l5_app.h"
#include "l4_trans.h"
#include "l3_net.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "irq.h"

int remote_address, remote_port, this_address;
int total_sent;
 char filename[1024];
/*
 * Initialize however you want.
 */
void l5_init(int local)
{
  remote_port = -1;
  remote_address = -1;
  this_address = local;
}

/*
 * Report success for a physical link establishment.
 */
void l5_linkup( int other_address, const char* other_hostname, int other_port )
{
    fprintf( stderr, "Successfully established a physical link (plugged in a cable)\n"
                     "with host:port %s:%d.\n"
                     "We can use the address >>%d<< for that machine.\n"
                     "\n",
                     other_hostname,
                     other_port,
                     other_address );
}

void l5_handle_keyboard( )
{
    char  buffer[1024];
    char *retval;

    retval = fgets( buffer, sizeof(buffer), stdin );
    if( retval != 0 )
    {
        buffer[strlen(buffer)-1] = 0;

        /* debug */
        fprintf( stderr, "The buffer contains: >>%s<<\n", buffer );

        if( strstr( buffer, "CONNECT" ) != NULL )
	  {
            char hostname[1024];
            int  port;
            int  device;

            /*
             * sscanf tries to find the pattern in the buffer, and extracts
             * the parts into the given variables.
             */
            int ret = sscanf( buffer, "CONNECT %s %d", hostname, &port );
            if( ret == 2 )
            {
                /* two matches, we got our hostname and port */
                device = l1_connect( hostname, port );

                fprintf( stderr,
                         "Physical connection to host:port %s:%d has device number %d\n",
                         hostname, port, device );

            }
        }
	if( strstr( buffer, "QUIT" ) != NULL )
	  {
	    lastFree();
	    printf("Exiting...\n");
	    exit(0);
	  }
	if( strstr( buffer, "SEND" ) != NULL)
	  {	    
	   
	    
	    int ret = sscanf( buffer, "SEND %d %s", &remote_address, filename );
	    if(ret == 2){
	      fprintf( stderr, "OK mac_address: %d, filename:%s\n", remote_address, filename);
	      total_sent = 0;
	      resetRecv();
	      l5_send(NULL);
	    }
	  }
	if( strstr( buffer, "print" ) != NULL)
	  {
	    printall();
	  }

        /* Your keyboard processing here
         * should handle SEND <mac> <file> and QUIT
         */
    }
}

int l5_header_size()
{
  return l4_header_size();
}


int l5_recv( int dest_pid, int src_address, int src_port, const char* l5buf, int sz )
{
    return slow_receiver( l5buf, sz );
}

/*using to set a timeout*/
struct timeval get_time(int s)
{
    struct timeval time;
    gettimeofday(&time, 0);

    time.tv_sec += s;

    return time;
}

int timerID;

/*
 *Send packages down the layers, and since the window size is
 *10, we'll have to send the rest by using timeout on when the return value
 *is -1 then there has been an error, could be sendto or buffer is full, so we'll set a timeout to send again
 *at a later time, by using a global variable
 *total_sent, we can find the place were we left.
 */


void l5_send(void *param)
{
  FILE *file_to_send;
  char *buffer;
  int bytes_read,  bytes_send, headers, bytes_to_read, read;


  /* if(remote_port == -1){ */
/*     fprintf(stderr, "CONNECT before sending\n"); */
/*     return; */
/*   } */

  headers = l5_header_size();
  
  
  bytes_send = 0;
  bytes_read = 0;
  bytes_to_read = FRAME_LENGTH - headers;
 
  file_to_send = fopen(filename, "r"); 
  if(file_to_send == NULL){
    fprintf(stderr, "Could not open file %s\n", filename);
    return;
  } 

 

  while(1){
    if(feof(file_to_send)){
      printf("Done reading file\n");
      fclose(file_to_send);
       /*Make sure it's empty*/
      bzero(filename, sizeof(filename));
      return;
    }
    /*set the file position indicator to total_send*/
    fseek(file_to_send, total_sent, SEEK_SET);
    buffer = malloc(FRAME_LENGTH);
    bzero(buffer, FRAME_LENGTH);

    /*read and stores to buffer, fread returns number of bytes read*/
    read = fread(buffer, 1, bytes_to_read, file_to_send);

 
    
    bytes_send = l4_send(remote_address, 0, this_address, buffer, read);
    
    // printf("bytes_sent: %d\n", bytes_send);
    //  printf("total_sent: %d\n", total_sent);

    free(buffer);
    if(ferror(file_to_send)){
      fclose(file_to_send);
      fprintf(stderr, "Reading error\n");
      return;
    }
    
    /*Got a negative value, error occurred during sending, could be buffer full or package got dropped*/
    if(bytes_send < 0){
      if(bytes_send == -2) {
	fprintf(stderr, "Address doesn't exist in the network\n");
	fclose(file_to_send);
	return;
      }
      timerID = register_timeout_cb(get_time(1), &l5_send, NULL);
      fclose(file_to_send);
      return;
    } else {
      total_sent = bytes_send + total_sent;
    }
  }
}
