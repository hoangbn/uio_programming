#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "irq.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "l2_flow.h"
#include "l3_net.h"
#include "l5_app.h"


#define MAX_ADDRESSES 1024

/*
 * The network layer needs to maintain private information about
 * the MAC address at the other end of every link.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static link_entry_t mac_to_device_map[MAX_ADDRESSES];

int counters = 0;
int local_address_id[MAX_ADDRESSES];

static int local_address, received_from, connections = 0;

void printall()
{
  int mac;
  for( mac=0; mac<MAX_ADDRESSES; mac++ ){
    if(mac_to_device_map[mac].remote_mac_address != -1){
      /* printf("mac: %d, device: %d\n",   mac_to_device_map[mac].remote_mac_address,  mac_to_device_map[mac].phys_device ); */
    }

  }

}

void resetRecv()
{
  received_from = -1;
}

void l2_local(int local)
{
  local_address = local;
  /* printf("LOCAL: %d\n", local_address); */
}

/*returns the size of header from this layer*/
int l2_header_size()
{
  return sizeof(struct L2Header);
}


/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * In particular initialize all the data structures that you
 * need for link-layer error correction and flow control.
 */
void l2_init( int local_mac_address, int device )
{
    static int first = 1;
    received_from = -1;  // not set


    if( first )
    {
        l2_flow_init( mac_to_device_map );

        int mac;
        for( mac=0; mac<MAX_ADDRESSES; mac++ )
        {
            mac_to_device_map[mac].remote_mac_address = -1;
            mac_to_device_map[mac].phys_device        = -1;
        }
        first = 0;
    }
    /* fprintf(stderr, "localmac: %d, device: %d\n", local_mac_address, device); */
    mac_to_device_map[local_mac_address].remote_mac_address = -1;
    mac_to_device_map[local_mac_address].phys_device        = device;
    local_address_id[counters++] = local_mac_address;

}

/*
 * We have gotten an UP packet for a particular device from the remote host.
 * We have to remember that in our table that maps MAC addresses to devices.
 */
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_address )
{
  connections++;
    int mac;
    mac = l2_get_mac( device );
    /* fprintf(stderr, "mac: %d\n", mac); */

    mac_to_device_map[mac].remote_mac_address = other_mac_address;
    l2_flow_linkup( device, other_hostname, other_port, other_mac_address );

    l3_linkup( other_hostname, other_port, other_mac_address );
}

/*
 * Used to get the mac address of a device.
 */
int l2_get_mac( int device )
{
    int mac;
    
    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        if( mac_to_device_map[mac].phys_device == device )
            break;
    }
    
    /*Didn't find and mac*/
    if( mac == MAX_ADDRESSES )
    {
      return -1;
    }

    return mac;
}

/*
 *Used to get remote mac addresses
 */
int l2_get_dest_mac(int dest)
{
  int mac;
  for(mac = 0; mac < MAX_ADDRESSES; mac++){
    if(  mac_to_device_map[mac].remote_mac_address == dest){
      return mac;
    }
  }
  
  return -1;

}

struct Parameter 
{
  int dest;
  int through;
  const char *buffer;
  int len;
};

int l2_flood(int dest_mac_addr, const char *buf, int length)
{
  int remote_address;
  int through_mac;
  int err;
  int mac;

  for(mac = 0; mac < MAX_ADDRESSES; mac++){
    remote_address = l2_get_mac(mac);
    through_mac = mac_to_device_map[remote_address].remote_mac_address;
    
    if(remote_address == -1){
      break;
    }
    
    if(through_mac != local_address && through_mac != received_from){ 
      err = l2_flow_send(dest_mac_addr, through_mac, buf, length);
      if(err < 0){
	return -1;
      }

    }
  }
  return err;
}


/*
 * This function decides if a frame should be sent up to the next
 * layer above or sent out over a bridge to the other links.
 * device is the device we have received the frame from.
 *
 * NOTE, buf contains L2Header, use this to decide to bridge or send to l3_recv 
 */
int l2_frame_controller( int device, const char* buf, int length )
{
  // printf("Recv: %d, buf: %s, Length: %d\n", device, buf, length);

  const struct L2Header* hdr_pointer;
  const char*            l3buf;
  int                    src_mac_address;
  int                    dst_mac_address;
  int                    err;
  int                    dest;

  hdr_pointer = (const struct L2Header*)buf;
  src_mac_address  = ntohl(hdr_pointer->src_mac_address);
  dst_mac_address  = ntohl(hdr_pointer->dst_mac_address);
  
  received_from = src_mac_address;
  l3buf = buf + sizeof(struct L2Header);
  dest = l2_get_dest_mac(dst_mac_address);
  err = 1;  

  // printf("This_address: %d, Destination: %d\n", this_address, dst_mac_address);
  
 /*  /\*There ain't no other machines connected to this node locally so can just drop it and send ack back*\/ */
/*   if(dest == ){ */
/*     printf("Package is not for us, and there ain't no other in the perimeter to send to, dropping package and send ack back...\n"); */
/*     return err; */
/*   } */
  
  err = l3_recv(device, l3buf, length-sizeof(struct L2Header));

  /* /\*if it's destination is this node/machine then we'll try to receive*\/ */
  /* if(this_address == dst_mac_address){ */
  /*   err = l3_recv(device, l3buf, length-sizeof(struct L2Header)); */
  /*   printf("err:%d\n", err); */
  /*   /\*Bridging is possible since remote address can be found locally*\/ */
  /* } else if(dest != -1){ */
  /*   bridge(device, l3buf, length-sizeof(struct L2Header)); */
  /*   err = l2_flow_send(dst_mac_address, dst_mac_address, l3buf, length-sizeof(struct L2Header)); */
  /* } */
  /* /\*Since it doesn't exist locally, flood to all machines in the perimeter*\/ */
  /* else { */
  /*   printf("didn't get bridge \n"); */

  /*   /\*If we have only 1 connection then we can't flood by sending the package back, so we drop the package then ack*\/ */
  /*   if(connections < 2){ */
  /*     printf("This package is not for us and there is no other machines in the perimeter, dropping and sending ack\n"); */
  /*     return 1; */
  /*   } */

  /*   err = l2_flood(dst_mac_address, l3buf, length-sizeof(struct L2Header)); */
  /* } */
  
  
  /* Implement bridging or l3_recv here! */
  return err;
}





/*
 * Called by layer 3, network, when it wants to send data to a
 * MAC address.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 *
 * NOTE:
 * You can split this function into many small helper
 * functions. If you are not using the supplied flow control, you will need
 * to have a sliding window implementation. 
 */

int l2_send( int dest_mac_addr, const char * buf, int length )
{
    /* 
     * Will send the frame with the given flow control.
     * The second parameter will be the mac we will send out on if
     * we do not know the receiver.
     *
     * I.e.:
     *
     * With the network topology like the following:
     *
     *     A (mac 1) <-------> (mac 2) B (mac 3) <------> (mac 4) C
     *
     * node B will have two mac addresses and the other will just have one.
     * If A were to send a frame to node C then B will have to
     * bridge over to the link to C. The purpose of the second parameter is
     * to tell l2_flow_send that node A wants to send via node B because
     * the destination is unknown.
     */

  // fprintf(stderr, "mac: %d, buffer: %s, length: %d\n", dest_mac_addr, buf, length);
  
  int destination;

  destination = l2_get_dest_mac(dest_mac_addr);
  // If the destination address is not locally connected with this node, then we've to flood to all nodes who's connected
   if(destination == -1){
     return l2_flood(dest_mac_addr, buf, length);
   } else {
     return l2_flow_send(dest_mac_addr, dest_mac_addr, buf, length);
   }
}

/*
 * Called by layer 1, physical, when a frame has arrived.
 *
 * This function has no return value. It must handle all
 * problems itself because the physical layer isn't able
 * to handle errors.
 *
 */
void l2_recv( int device, const char* buf, int length )
{
  
    /* 
     * l2_flow_recv will handle the flow_control mechanism, before sending the frame
     * to l2_frame_controller
     */
    l2_flow_recv(device, buf, length);
}
