#ifndef L3_NET_H
#define L3_NET_H

/* see comments in the c file */

/**
 *Flags
 *E = Echo packet
 *L = LSP -> link state packet
 *D = Data packet
 *R = reply ECHO packet
 */

struct EDGE
{
  //  int edge_gen;
  int cost;
};

struct ECHO
{
  int round_trip_time;
};

struct NODE
{
  int address;
  int cost;
  //  struct NODE *next;
};

struct LSP
{
  int source;
  int seq;
  int nodes;
  // struct NODE *first;
};

struct LIST
{
  int destination, length;
  char *buffer;
  char flag;
  struct LIST *next/*, *prev*/;
  // struct QUEUE *previous;
};


void l3_init( int self );
void l3_linkup( const char* other_hostname, int other_port, int other_mac_address );

int  l3_send( int host_address, const char* buf, int length );
int  l3_recv( int mac_address, const char* buf, int length );
int l3_header_size();
void l3_add_to_list(int dest, const char* buffer, int length, char flag);
void l3_recv_echo(int source, const char* buffer, int length);
void l3_reply_echo(int source, const char* buf, int length);
void l3_remove_from_list(struct LIST *rm);
void l3_send_echo(void);
void l3_send_list(void);
void l3_send_lsp(void);
void l3_recv_lsp(int source_address, const char *buf, int length);
int l3_get_mac(int i);
void shortest_path(int from, int to);
void l3_update_routing_table(void);
#endif /* L3_NET_H */

