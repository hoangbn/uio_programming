#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "l1_phys.h"
#include "l2_link.h"

#include "delayed_dropping_sendto.h"

#define MAX_CONNS 1024

static phys_conn_t my_conns[MAX_CONNS];
static phys_conn_t *l1_linkup( phys_conn_t *conn, const char* other_hostname, int other_port, int other_address );

int my_udp_socket = -1;


/*******************************************************************************
 * Function name:    static phys_conn_t *get_phys_conn( struct sockaddr_in *addr )
 * Parameters:       struct sockaddr_in *addr
 *                   
 * Returns:          phys_conn_t conn on success
 *                   NULL on failure
 * Description:      Finds the connection associated with the given sockaddr
 *******************************************************************************/
static phys_conn_t *get_phys_conn( struct sockaddr_in *addr ) {
  phys_conn_t *conn = NULL;

  int i;
  for( i=0; i<MAX_CONNS && my_conns[i].remote_hostname != 0; ++i )
    {
      if ( my_conns[i].addr.sin_addr.s_addr == addr->sin_addr.s_addr 
	   && my_conns[i].addr.sin_port == addr->sin_port )
        {
	  /* Match */
	  conn = &my_conns[i];
	  /*    return conn; */
	  break;
        }
    }
  return conn;
}


/*******************************************************************************
 * Function name:    static phys_conn_t *create_phys_conn( const char *hostname, unsigned short port )
 * Parameters:       const char *hostname, unsigned short port
 *                   
 * Returns:          phys_conn_t conn on success
 *                   exit on failure
 * Description:      Create an entry in the table of physical connection
 *******************************************************************************/
static phys_conn_t *create_phys_conn( const char *hostname, unsigned short port )
{
  phys_conn_t *conn = NULL;
  struct hostent *he;
    
  /* Find an available device id */
  int device;
  for( device=0; device<MAX_CONNS; device++ )
    {
      if( my_conns[device].remote_hostname == 0 )
        {
	  conn = &my_conns[device];
	  conn->device = device;
	  break;
        }
    }
  if( !conn  )
    {
      fprintf( stderr, "Too many physical connections established.\n" );
      exit( -1 );
    }

  if((he = gethostbyname(hostname)) == NULL) {
    fprintf(stderr, "Could not get host\n");
    exit(-1);
  }

  conn->remote_hostname = strdup(hostname);
  conn->remote_port = port;
  conn->state = UNASSIGNED;
  conn->addr.sin_family = AF_INET;     
  memcpy(&conn->addr.sin_addr, (struct in_addr *) he->h_addr, sizeof(struct in_addr));
  conn->addr.sin_port = htons(port);
    
  return conn;
}

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * local_port is the port that for the local UDP socket that
 *   we will use for all communication.
 *
 * hosts is a pointer to an array of remote hostnames and ports
 *   that we want to establish connections to. Each entry in
 *   hosts represents one network device. The index of the entry
 *   is the device number.
 *
 * numhosts is the number of hosts in the array.
 *
 * Here in particular: initialize all of your socket communication
 * in this function. The socket communication is meant to fake
 * physical cables, so it should be done completely before
 * continuing.
 */


/*******************************************************************************
 * Function name:    void l1_init( int local_port )
 * Parameters:       int local_port
 *                   
 * error:            exit on failure
 * Description:      initiate socket
 ************'******************************************************************/
void l1_init( int local_port )
{
  int                err;
  struct sockaddr_in addr;

  my_udp_socket = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP );
  if( my_udp_socket < 0 )
    {
      fprintf( stderr, "Failed to create local UDP socket" );
      exit( -1 );
    }

  memset( &addr, 0, sizeof(struct sockaddr_in) );
  addr.sin_family      = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_port        = htons(local_port);

  err = bind( my_udp_socket, (struct sockaddr*)&addr, sizeof(struct sockaddr_in) );
  if( err < 0 )
    {
      fprintf( stderr, "Failed to bind local UDP socket to port %d", local_port );
      exit( -1 );
    }
    
  memset( my_conns, 0, sizeof(my_conns) );
}



/*******************************************************************************
 * Function name:    int l1_connect( const char* hostname, int port )
 * Parameters:       const char* hostname, int port
 *  
 * returns:          value of the device allocated to this connection
 *                   -1 on failure                 
 * error:            exit on error
 * Description:      Initiate establishment of a "physical connection" to the given host and port.
 ************'******************************************************************/
int l1_connect( const char* hostname, int port )
{
  char msg[11];
  
  phys_conn_t *conn = create_phys_conn( hostname, port );

  if (!conn) {
    fprintf( stderr, "Could not create physical connection to %s\n", hostname );
    return -1;
  }
  
  sprintf(msg, "CONNECT %d", getMacAddress());
  
  if(sendto(my_udp_socket, &msg, strlen(msg), 0, (struct sockaddr*)&conn->addr, (socklen_t) sizeof(conn->addr)) == -1){
    fprintf(stderr, "Could not send CONNECT to %s in port %d.... Aborting...\n", hostname, port);
  }
  
  bzero(msg, strlen(msg));

  return conn->device;
}



/*******************************************************************************
 * Function name:    int l1_send( int device, const char* buf, int length )
 * Parameters:       int device, const char* buf, int length 
 *  
 * returns:          positive value of bytes sent
 *                   negative value on failure                 
 * error:            exit on error
 * Description:      send bytes over the connection
 ************'******************************************************************/
int l1_send( int device, const char* buf, int length )
{    
  int bytes_send;

  phys_conn_t *conn = &my_conns[device];
  /*   printf("The remote hostname: %s\n",conn->remote_hostname); */
 
  if((bytes_send = sendto(my_udp_socket, buf, length, 0, (struct sockaddr*)&conn->addr, (socklen_t) sizeof(struct sockaddr_in))) < 0){
    fprintf(stderr, "Couldn't send data from the physical layer\n");
  }
  
  return bytes_send;
  
  /*
   * Extend this function so that you send the given data over YOUR
   * "physical connection" using the function sendto(), the
   * function delayed_sendto() and the function delayed_dropping_sendto().
   *
   * It's easiest to start with sendto(), because it doesn't delay
   * and it doesn't drop packets randomly.
   *
   * delayed_sendto() works very similar to the system function
   * sendto() but it delays packet delivery in annoying ways.
   * The same for delayed_dropping_sendto().
   */
}


/*******************************************************************************
 * Function name:    static phys_conn_t *l1_linkup( phys_conn_t *conn, const char* other_hostname, int other_port, int other_address )
 * Parameters:       phys_conn_t *conn, const char* other_hostname, int other_port, int other_address
 *  
 * returns:          return phys_conn_t *conn                                  
 * Description:      assign a device that is now connected in the table my_conn and print an UP message onto the screen.
 ************'******************************************************************/
static phys_conn_t *l1_linkup( phys_conn_t *conn, const char* other_hostname, int other_port, int other_address )
{
  if (!conn) {
    /* If the conn parameter was NULL, we need to assign a new device.  */
    conn = create_phys_conn(other_hostname, other_port);
  }
  
  conn->state = ESTABLISHED;
  l2_linkup(conn->device, other_hostname, other_port, other_address);
  return conn;
}

/*
 * In interrupt occurs when data arrives. Our interrupts are simulated
 * by data-arrival events in the select loop.
 * When select notices that data for my_udp_socket has arrived, it calls
 * this function.
 *
 * A positive return value means that all data has been delivered.
 * A zero return value means that the receiver can not receive the
 * data right now.
 * A negative return value means that an error has occured and
 * receiving failed.
 *
 * NOTE:
 * Link layer error correction and flow control must be considered
 * here. You will certainly need several helper functions because
 * you will need to perform retransmissions after a timeout.
 */


/*******************************************************************************
 * Function name:    void l1_handle_event( )
 * Parameters:       
 *                                 
 * Description:      Called by layer 5 when an interupt occur, handles the event accordingly to what it is
 ************'******************************************************************/
void l1_handle_event( )
{
 
  struct sockaddr_in remote_host;
  phys_conn_t *conn; 

  int remote_MAC_address, remote_port, bytes_recv, length;
  char *remote_name, buf[100];
  socklen_t len = sizeof(remote_host);

  /*have to make sure buf is clean*/
  bzero(&buf, 100);

  char msg[7];
  
  if((bytes_recv = recvfrom(my_udp_socket, buf, 100, 0,(struct sockaddr *)&remote_host, &len)) < 0) {
    fprintf(stderr, "Something went wrong while receiving data from remote\n");
    exit(-1);
  } 
  
  remote_name = inet_ntoa(remote_host.sin_addr);
  remote_port = ntohs(remote_host.sin_port);
  fprintf(stderr, "remote_name = %s, remote_port = %d\n", remote_name, remote_port);

  fprintf(stderr, "Buf: %s, Bytes received: %d, host: %s with port: %d\n", buf, bytes_recv, remote_name, remote_port);
  
  /*Compare if it's a CONNECT packet*/
  if(strncmp(buf, "CONNECT", 7) == 0){
    if((sscanf(buf, "CONNECT %d", &remote_MAC_address)) == 1){
      sprintf(msg, "UP %d", getMacAddress());
      length = strlen(msg);
      if((sendto(my_udp_socket, &msg, length, 0, (struct sockaddr*)&remote_host, (socklen_t)sizeof(remote_host))) < 0){
	fprintf(stderr, "Couldn't send UP..\n");
      } else {
	l1_linkup(get_phys_conn(&remote_host), remote_name, remote_port, remote_MAC_address); 
	return;
      }		       
    } else {
      fprintf(stderr, "Bad message, impossible to interpret\n");
    } 
    /* Compare if it's a UP packet*/
  } else if(strncmp(buf, "UP", 2) == 0){
    if((sscanf(buf, "UP %d", &remote_MAC_address)) == 1){
      conn = get_phys_conn(&remote_host);
      l1_linkup(conn, remote_name, remote_port, remote_MAC_address);  
      return;
    }
    /*It's a data packet then*/
  } else {
    conn = get_phys_conn(&remote_host);
    l2_recv(conn->device, buf, bytes_recv);
    return;
  }
}

