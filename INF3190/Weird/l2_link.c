#include <sys/time.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "general.h"
#include "irq.h"
#include "l1_phys.h"
#include "l2_link.h"
#include "l3_net.h"

/*
 * The network layer needs to maintain private information about
 * the MAC address at the other end of every link.
 * The map is static because it is inappropriate for other layers
 * to see or change it.
 */
static link_entry_t m_to_d[MAX_ADDRESSES];


// Array to store pointers to the buffers which are waiting to be pushed
// down to l2_bridge.
char *floodqueue[MAX_FLOODQUEUE_SIZE];

/* 
 * Looks up the correct mac address to use for a remote destination.
 * Returns E_NOMAC if the remote mac is not found.
 */
int src_mac_lookup(int dest_mac_addr)
{
    int i;

    for( i=0; i<MAX_ADDRESSES; i++ )
    {
        if (m_to_d[i].remote_mac_addr == dest_mac_addr)
	    return i;
    }
   
    return E_NOMAC;
}

/*
 * Used to get the mac address of a device.
 */
int l2_get_mac( int device )
{
    int mac;

    if (device == E_NODEV)
	return E_NOMAC;

    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
	if( m_to_d[mac].phys_device == device )
	    return mac;
    }

    return E_NODEV;
}

/*
 * Used for setting timeouts for frames which have been sent with l2_real_send.
 */
void l2_register_send_timeout(int timeout, int dest_mac_addr, int id)
{
    int intparam[2];
    void *param;

    DEBUG("Registers send timeout for dest_mac = %d and seq id = %d\n", dest_mac_addr, id);

    intparam[0] = dest_mac_addr;
    intparam[1] = id;

    param = malloc(sizeof(int) * 2);
    memcpy(param, intparam, sizeof(int) * 2);

    register_timeout_cb(get_time_struct(timeout), &resend_frame, param);
}

/*
 * Used for setting timeouts for frames which should be sent with l2_bridge,
 * but are unable to do so and hence are waiting in a "flood queue".
 */
void l2_register_flood_timeout(int timeout, int id, int device, int length)
{
    int intparam[3];
    void *param;

    DEBUG("Registers flood timeout for device = %d and queue id = %d\n", device, id);

    intparam[0] = id;
    intparam[1] = device;
    intparam[2] = length;

    param = malloc(sizeof(int) * 3);
    memcpy(param, intparam, sizeof(int) * 3);

    register_timeout_cb(get_time_struct(timeout), &resend_flood_frame, param);
}

/*
 * Return the first available position in the ring buffer to store 
 * the l1buffer.
 * Returns E_FULL if there are MAX_WINDOW_SIZE windows in the ring buffer.
 */
int get_new_buffer_position(int mac)
{
    if (num_frames_in_window(mac) == MAX_WINDOW_SIZE)
	return E_FULL;
    else {
	BUF_INC(BUF_LAST(mac));
	return BUF_LAST(mac);
    }
}

/*
 * Returns a vacant position in the flood queue..
 */
int get_flood_position(void)
{
    int i;

    for (i=0; i < MAX_FLOODQUEUE_SIZE; i++)
    {
	if (!floodqueue[i])
	    return i;
    }

    fprintf(stderr,"Flood queue is full.. can't handre the frame. Exiting.. \n");
    exit(1);
}

/*
 * Returns the value of one valid sequence id before the given one.
 * If no previous ones, it returns the given id.
 */
int get_previous_id(int id, int mac)
{
    if (BUF_FIRST(mac) <= BUF_LAST(mac) || BUF_FIRST(mac) < SL_BUFFER_SIZE)
	return (BUF_FIRST(mac) < id) ? BUF_FIRST(mac) : id;
    else
	return (id < BUF_LAST(mac)) ? id : BUF_LAST(mac);
}

/*
 * Removes all frames from position from the beginning of the send
 * buffer to position ackid for the recipient of the given device.
 */
void receive_ack(int device, int ackid)
{
    int id, mac;

    mac = M_REM(l2_get_mac(device));

    DEBUG("mac = %d, sb_first = %d, sb_last = %d, ackid = %d, valid = %d\n", 
	    mac,BUF_FIRST(mac), BUF_LAST(mac), ackid, valid_id(ackid,mac));

    if (!valid_id(ackid,mac))
	return;

    for(;;)
    {
	id = get_previous_id(ackid,mac);
	if (BUF_LEN(mac,id) > 0) {
	    free(m_to_d[mac].s_buf[id].buf);
	    BUF_LEN(mac,id) = 0;
	}
	if (ackid != BUF_LAST(mac))
	    BUF_INC(BUF_FIRST(mac));
	if (id == ackid)
	    break;
    }
}

/*
 * Tries to resend a flood frame after a timeout occured.
 * Set a new timer on it if it failed.
 */
void resend_flood_frame(void *param)
{
    int *intparam;
    int device,id,length;

    intparam = malloc(sizeof(int) * 3);
    memcpy(intparam, param, sizeof(int) * 3);
    id = intparam[0];
    device = intparam[1];
    length = intparam[2];

    // Try pushing it down to l2_bridge, or reregister a timeout for the frame.
    if (l2_bridge(device, floodqueue[id], length) == E_TRYAGAIN) {
	DEBUG("Requeing flood id %d for device %d\n", id, device);
	l2_register_flood_timeout(DEFAULT_TIMEOUT,id,device,length);
    } else {
	// Release it from floodqueue. receive_ack will free the resource.
	floodqueue[id] = NULL;
    }

}

/*
 * Resend a frame after a timeout occured.
 * Set a new timer on it, if it has to be resent.
 */
void resend_frame(void *param)
{
    int *intparam;
    int id, mac;

    intparam = malloc(sizeof(int) * 2);
    memcpy(intparam, param, sizeof(int) * 2);
    mac = intparam[0];
    id = intparam[1];

    //DEBUG("BUF_LEN(%d,%d) = %d\n", mac,id,BUF_LEN(mac,id));

    // Resend only if the frame has not already been acked, and hence its
    // length is zero.
    if (BUF_LEN(mac,id) > 0)
    {
	DEBUG("Resending sequence id %d to %d\n", id,mac);
	l1_send(m_to_d[mac].s_buf[id].device, m_to_d[mac].s_buf[id].buf, BUF_LEN(mac,id));
	l2_register_send_timeout(DEFAULT_TIMEOUT, mac, id);
    } else
	DEBUG("Frame %d has already been acked, hence not resending it..\n", id);
}

/* 
 * Check if the id is in the sliding window.
 * Returns 1 if true, and 0 if false.
 */ 

int valid_id(int id, int mac)
{
    if (id < 0 || id >= SL_BUFFER_SIZE)
	return 0;
    else if (BUF_LAST(mac) >= id && id >= BUF_FIRST(mac))
	return (BUF_LEN(mac,id) > 0) ? 1 : 0;
    else if (BUF_LAST(mac) <= id && BUF_FIRST(mac) <= id)
	return (BUF_LEN(mac,id) > 0) ? 1 : 0;
    else
	return 0;
}

/*
 * Returns the number of frames in the window.
 */
int num_frames_in_window(int mac)
{
    if (BUF_LAST(mac) >= BUF_FIRST(mac))
	return BUF_LAST(mac) - BUF_FIRST(mac) + 1;
    else
	return SL_BUFFER_SIZE + BUF_LAST(mac) - BUF_FIRST(mac);
}

/* Pass the connect function downwards */
int l2_connect(const char* hostname, int port)
{
    return l1_connect(hostname, port);
}

/* Return our layer's header size added to the one below. */
int l2_get_header_size() {
    return sizeof(struct L2Header) + l1_get_header_size();
}

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * In particular initialize all the data structures that you
 * need for link-layer error correction and flow control.
 */
void l2_init( int local_mac_addr, int device )
{
    int mac;

    static int first = 1;

    if (first)
    {
	for( mac=0; mac<MAX_ADDRESSES; mac++ )
	{
	    m_to_d[mac].remote_mac_addr = E_NOMAC;
	    m_to_d[mac].phys_device = E_NODEV;
	    // Set BUF_LAST(mac) one to -1, as the first time we enter
	    // l2_real_send(), the sequence id will be increased to 0.
	    BUF_LAST(mac) = -1;
	}

	first = 0;
    }


    m_to_d[local_mac_addr].remote_mac_addr = E_OWN;
    m_to_d[local_mac_addr].own = 1;
    m_to_d[local_mac_addr].phys_device = device;
}

/*
 * We have gotten an UP packet for a particular device from the remote host.
 * We have to remember that in our table that maps MAC addresses to devices.
 */
void l2_linkup( int device, const char* other_hostname, int other_port, int other_mac_addr )
{
    int mac;
    for( mac=0; mac<MAX_ADDRESSES; mac++ )
    {
        if( m_to_d[mac].phys_device == device )
        {
            m_to_d[mac].remote_mac_addr = other_mac_addr;
            l3_linkup( other_hostname, other_port, other_mac_addr );
            return;
        }
    }
    errx(-1, "Programming error in establishing a physical link\n");
}

/* 
 * Called by layer 3, network, when it wants to send data to a
 * node identified by the MAC address.
 * Small function to decide of we are going to flood all interfaces
 * or send it properly. This is used to ensure that l2_real_send always
 * gets a valid dest_mac_addr.
 */
int l2_send(int dest_mac_addr, char* buf, int length)
{
    struct L2Header *hdr;
    char *l1buf;
    int src_mac_addr;

    src_mac_addr = src_mac_lookup(dest_mac_addr);

    DEBUG("src_mac_addr = %d, dest_mac_addr = %d\n", src_mac_addr, dest_mac_addr);
    
    if (src_mac_addr == E_NOMAC) {
	DEBUG("Flooding to all interfaces, as %d is not localy connected\n", dest_mac_addr);
	l1buf = malloc(length+sizeof(struct L2Header));

	if( l1buf == NULL)
	    err(-1,"Not enough memory in l2_send.\n");

	memcpy(&l1buf[sizeof(struct L2Header)], buf, length );

	hdr = (struct L2Header*)l1buf;
	hdr->type = DATA;
	// Find first lokal mac
	for (src_mac_addr=0; src_mac_addr < MAX_ADDRESSES; src_mac_addr++)
	{
	    if (M_OWN(src_mac_addr))
		break;
	}
	hdr->src_mac_addr = htonl(src_mac_addr);
	hdr->dest_mac_addr = htonl(dest_mac_addr);

	l2_send_flood(E_NODEV, dest_mac_addr, l1buf, length+sizeof(struct L2Header));
	// As l2_send_flood will queue if not able to send with l2_real_send
	// it is ok to just return the length.
	return length;
    } else {
	return l2_real_send(dest_mac_addr, src_mac_addr, buf, length);
    }
}

/*
 * Called l2_send when it wants to send data to a direct neighbour identified
 * by the MAC address.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 */
int l2_real_send(int dest_mac_addr, int src_mac_addr, char* buf, int length )
{
    int   device = -1;
    char* l1buf;
    struct L2Header*  hdr;
    int   retval;
    int l1buflen;
    int id;

    if ((id = get_new_buffer_position(dest_mac_addr)) == E_FULL)
    {
	DEBUG("Buffer is full, %d entries. Returning..\n", num_frames_in_window(dest_mac_addr));
	return E_TRYAGAIN;
    }

    DEBUG("Sender id %d for mac %d\n", id, dest_mac_addr);

    device = m_to_d[src_mac_addr].phys_device;

    l1buflen = length + sizeof(struct L2Header);

    l1buf = malloc(l1buflen);

    if( l1buf == NULL)
        err(-1,"Not enough memory in l2_send.\n");

    memcpy( &l1buf[sizeof(struct L2Header)], buf, length );

    hdr = (struct L2Header*)l1buf;
    hdr->type = DATA;
    hdr->src_mac_addr = htonl(src_mac_addr);
    hdr->dest_mac_addr = htonl(dest_mac_addr);
    hdr->id = htons((uint16_t) id);

    retval = l1_send(device, l1buf, l1buflen);

    m_to_d[dest_mac_addr].s_buf[id].buf = l1buf;
    m_to_d[dest_mac_addr].s_buf[id].len = l1buflen;
    m_to_d[dest_mac_addr].s_buf[id].device = device;

    l2_register_send_timeout(DEFAULT_TIMEOUT, dest_mac_addr, id);

    if( retval < 0 )
        return -1;
    else
	return retval-sizeof(struct L2Header);
}

/* Send ack for frame given with id to the device connected to the given device. */
void l2_send_ack(int device, int id)
{
    struct L2Header* hdr;
    int src_mac_addr;

    src_mac_addr = l2_get_mac(device);

    hdr = malloc(sizeof(struct L2Header));

    DEBUG("Acking id %d from %d to %d\n", id,M_REM(src_mac_addr),src_mac_addr);

    device = m_to_d[src_mac_addr].phys_device;
    hdr->type = ACK;
    hdr->src_mac_addr = htonl(src_mac_addr);
    hdr->dest_mac_addr = htonl(M_REM(src_mac_addr));
    hdr->id = htons((uint16_t) id);

    // Blindy send the ack. If the recipient doesn't receive it, it will send the
    // packet again, and we'll try acking again.
    l1_send(device, (char *)hdr, sizeof(struct L2Header));
    
    free(hdr);
}

/*
 * Will send the the frame out on all devices, unless the device argument
 * contains a device, in that case we will skip that device.
 * The second parameter will is the destination mac.
 *
 * I.e.:
 *
 * With the network topology like the following:
 *
 *     A (mac 1) <-------> (mac 2) B (mac 3) <------> (mac 4) C
 *
 * node B will have two mac addresses and the other will just have one.
 * If A were to send a frame to node C then B will have to
 * bridge over to the link to C. The purpose of the second parameter is
 * to tell that node A wants to send via node B because the destination
 * is unknown.
 */
void l2_send_flood(int device, int dest_mac_addr, char* buf, int length)
{
    int i, skip;
    int src;
    char *bufcopy;

    // If device is E_NODEV we want to skip flooding to the same interface
    // we received it,
    if (device == E_NODEV)
	skip = -1000; // Some bugus value.
    else
	skip = device;

    DEBUG("device = %d, dest_mac_addr = %d\n", device, dest_mac_addr);

    // Iterate over all possible devices for flooding.
    for (i=0; i < MAX_ADDRESSES; i++)
    {
	src = l2_get_mac(i);
	if ((M_OWN(src)) && (M_REM(src) != E_OWN) && (i != skip))
	{
	    // Need a copy for each of the sendings.
	    bufcopy = malloc(length);
	    memcpy(bufcopy,buf,length);
	    if (l2_bridge(i, bufcopy, length) == E_TRYAGAIN)
		l2_add_to_flood_queue(i, bufcopy, length);
	}
    }
}

/*
 * Adds a buffer to queue the flood queue for later attemps to
 * push it down to l2_bridge.
 */
void l2_add_to_flood_queue(int device, char *buf, int length)
{
    char *bufcopy;
    int id;

    bufcopy = malloc(length);
    memcpy(bufcopy, buf, length);

    id = get_flood_position();
    floodqueue[id] = bufcopy;
    l2_register_flood_timeout(DEFAULT_TIMEOUT, id, device, length);
}

/*
 * Bridge a buffer on the given device.
 * Returns numbers of bytes sent, and E_TRYAGAIN if the
 * buffer is full.
 */
int l2_bridge(int device, char *buf, int length)
{
    struct L2Header *hdr;
    int dest_mac_addr;
    int id;
    int sent;

    dest_mac_addr = M_REM(l2_get_mac(device));
    
    if ((id = get_new_buffer_position(dest_mac_addr)) == E_FULL)
    {
	DEBUG("Buffer is full, %d entries. Returning..\n", num_frames_in_window(dest_mac_addr));
	return E_TRYAGAIN;
    }

    // Insert the unique sequence number for each frame.
    hdr = (struct L2Header*) buf;
    hdr->id = htons((uint16_t) id);
    DEBUG("id = %d\n", (int) ntohs(hdr->id));

    DEBUG("sending id %d via l2_bridge on device %d\n", id, device);

    sent = l1_send(device, buf, length);
    
    // Store everything we need, in case we have to resend it.
    m_to_d[dest_mac_addr].s_buf[id].buf = buf;
    m_to_d[dest_mac_addr].s_buf[id].len = length;
    m_to_d[dest_mac_addr].s_buf[id].device = device;

    l2_register_send_timeout(DEFAULT_TIMEOUT,dest_mac_addr, id);
    return sent;
}

/*
 * Called by layer 1, physical, when a frame has arrived.
 * If the packet is addressed to this host, then it will be sent upwards
 * to layer 3, if not it will either be forwarded if the recipient
 * is known, or flooded out.
 * This function has no return value.
 */
void l2_recv( int device, char* buf, int length )
{
    const struct L2Header* hdr;
    char* l3buf;
    char* bufcopy;
    int	id,l3len,src_lookup;
    int dest_mac_addr, src_mac_addr; // To and from from the l2 header
    int from, to; //The real to and from addresses

    hdr = (struct L2Header*)buf;
    src_mac_addr  = ntohl(hdr->src_mac_addr);
    dest_mac_addr  = ntohl(hdr->dest_mac_addr);
    id = (int) ntohs(hdr->id);
    l3buf = buf + sizeof(struct L2Header);
    l3len = length - sizeof(struct L2Header);

    to = l2_get_mac(device);
    from = M_REM(to);

    if (hdr->type == DATA)
    {
	// This is the frame with the sequence id we want.
	if (id == m_to_d[from].waitfor)
	{
	    src_lookup = src_mac_lookup(dest_mac_addr);
	    DEBUG("src_mac_lookup(%d) = %d\n", dest_mac_addr, src_lookup);
	    // Try writing receive packets which are to our macs.
	    // If l3_recv returns false, we don't ack the frame and hence the
	    // remote site will have to resend the frame.
	    if (M_OWN(dest_mac_addr)) {
		if (l3_recv(dest_mac_addr, l3buf, l3len)) {
		    DEBUG("ack 1\n");
		    l2_send_ack(device, id);
		    BUF_INC(m_to_d[from].waitfor);
		}
	    // Forward it to the correct recipient
	    } else if (src_lookup > 0) {
		// Need a copy, as l2_send_ack will remove the buffer.
		bufcopy = malloc(length);
		memcpy(bufcopy,buf,length);
		// Try bridging it, else put it on the flood queue.
		if (l2_bridge(m_to_d[src_lookup].phys_device, bufcopy, length) == E_TRYAGAIN)
		    l2_add_to_flood_queue(m_to_d[src_lookup].phys_device, bufcopy, length);
		DEBUG("ack 2\n");
		l2_send_ack(device, id);
		BUF_INC(m_to_d[from].waitfor);
	    // This frame is addressed to a node which is not this one, and
	    // we don't know where to send it, so it must be flooded out on
	    // all devices except for this one.
	    } else if (src_lookup == E_NOMAC) {
		l2_send_flood(device, dest_mac_addr, buf, length);
		DEBUG("ack 3\n");
		l2_send_ack(device, id);
		BUF_INC(m_to_d[from].waitfor);
	    } else
		DEBUG("This should not happen. src = %d, dst = %d.\n", src_mac_addr, dest_mac_addr);
	} else {
	    // If the frame has an id which is less than MAX_WINDOW_SIZE away from
	    // 'waitfor', we reack the frame, as our ack has been lost, but
	    // ignore the content of the frame.
	    int diff = m_to_d[from].waitfor - id;
	    if ((diff >= 0 && diff < MAX_WINDOW_SIZE) || diff + SL_BUFFER_SIZE < MAX_WINDOW_SIZE) {
		l2_send_ack(device, id);
	    }
	}
    } else if (hdr->type == ACK)
    {
	receive_ack(device, id);
    }
}
