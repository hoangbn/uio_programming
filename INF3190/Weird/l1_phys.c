#include <sys/socket.h>

#include <err.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "general.h"
#include "l1_phys.h"
#include "l2_link.h"

#include "delayed_dropping_sendto.h"

static phys_conn_t my_conns[MAX_CONNS];

int my_udp_socket = -1;

/* Size of this layers headers. */
int l1_get_header_size() {
    return 0;
}

/* Finds the connection associated with the given sockaddr */
static phys_conn_t *get_phys_conn( struct sockaddr_in *addr, unsigned short port ) {
    phys_conn_t *conn = NULL;

    int i;
    for( i=0; i<MAX_CONNS && my_conns[i].remote_hostname != 0; ++i )
    {
        if ( my_conns[i].addr.sin_addr.s_addr == addr->sin_addr.s_addr && my_conns[i].addr.sin_port == port )
        {
            /* Match */
            conn = &my_conns[i];
            break;
        }
    }

    return conn;
}

/* Create an entry in the table of physical connection */
static phys_conn_t *create_phys_conn( const char *hostname, unsigned short port )
{
    phys_conn_t *conn = NULL;

    /* Find an available device id */
    int device;
    for( device=0; device<MAX_CONNS; device++ )
    {
        if( my_conns[device].remote_hostname == 0 )
        {
            conn = &my_conns[device];
            conn->device = device;
            break;
        }
    }

    if( !conn  )
    {
        fprintf( stderr, "Too many physical connections established.\n" );
        exit( -1 );
    }

    conn->remote_hostname = strdup(hostname);
    conn->remote_port = port;
    conn->state = UNASSIGNED;

    /* Get address */
    struct hostent *he;
    if( !(he = gethostbyname(hostname)) )
    {
        herror("gethostbyname");
        return NULL;
    }

    /* Create the sockadd_in we will use for sending and identifying the connection */
    conn->addr.sin_family = AF_INET;
    conn->addr.sin_port = htons(port);
    memcpy(&conn->addr.sin_addr, he->h_addr, he->h_length);
    return conn;
}

/*
 * Call at the start of the program. Initialize data structures
 * like an operating system would do at boot time.
 *
 * local_port is the port that for the local UDP socket that
 *   we will use for all communication.
 *
 * hosts is a pointer to an array of remote hostnames and ports
 *   that we want to establish connections to. Each entry in
 *   hosts represents one network device. The index of the entry
 *   is the device number.
 *
 * numhosts is the number of hosts in the array.
 *
 * Here in particular: initialize all of your socket communication
 * in this function. The socket communication is meant to fake
 * physical cables, so it should be done completely before
 * continuing.
 */
void l1_init( int local_port )
{
    int                err;
    struct sockaddr_in addr;

    my_udp_socket = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP );
    if( my_udp_socket < 0 )
        errx(-1, "Failed to create local UDP socket. Exiting.");

    memset( &addr, 0, sizeof(struct sockaddr_in) );
    addr.sin_family      = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port        = htons(local_port);

    err = bind( my_udp_socket, (struct sockaddr*)&addr, sizeof(struct sockaddr_in) );
    if (err == -1)
        errx(-1,"Failed to bind local UDP socket to port %d. Exiting.", local_port );
    else
	DEBUG("Successfully bound to port %d\n", local_port);

    memset( my_conns, 0, sizeof(my_conns) );
}

/*
 * Initiate establishment of a "physical connection" to the given host and
 * port. The return value of the function is the device that has been
 * allocated to this connection.
 */
int l1_connect( const char* hostname, int port )
{
    phys_conn_t *conn = create_phys_conn( hostname, port );

    if (!conn) {
        fprintf( stderr, "Could not create physical connection to %s\n", hostname );
        return -1;
    }

    /* Send a connect message */
    char msg[20];
    /* Bit ugly, but gets the mac address from a broken physical layer */
    int local_mac = l2_get_mac( conn->device );
    snprintf( msg, sizeof(msg), "CONNECT %d", local_mac );
    conn->state = CONNECTING;

    ssize_t num = sendto( my_udp_socket, msg, strlen(msg) + 1, 0, (struct sockaddr*) &conn->addr, sizeof(struct sockaddr_in) );

    if (num == -1 )
    {
        perror( "sendto" );
        return -1;
    }

    return conn->device;
}

/*
 * Called by layer 2, link, when it wants to send data over the
 * "physical connection" that is represented by device.
 * A positive return value means the number of bytes that have been
 * sent.
 * A negative return value means that an error has occured.
 */
int l1_send( int device, const char* buf, int length )
{
    phys_conn_t *conn = &my_conns[device];

    if( !conn )
    {
        fprintf(stderr, "Device not found\n");
        return -1;
    }
    ssize_t num = delayed_dropping_sendto( my_udp_socket, buf, length, 0, (struct sockaddr*) &conn->addr, sizeof(struct sockaddr_in) );

    return num;
}

/*
 * If a packet that has been received is an UP packet that establishes
 * a link instead of carrying data, l1_handle_event should call this
 * function.
 * The function should assign a device that is now connected in the
 * table my_conn and print an UP message onto the screen.
 */
static phys_conn_t *l1_linkup( phys_conn_t *conn, const char* other_hostname, int other_port, int other_address )
{
    DEBUG("l1_linkup( %s, %d, %x )\n", other_hostname, other_port, other_address);

    if ( !conn) {
        conn = create_phys_conn( other_hostname, other_port );

        if ( !conn ) {
            fprintf( stderr, "Could not create physical connection to %s\n", other_hostname );
            return 0;
        }
    }

    DEBUG("Calling l2_linkup( %d, ... )\n", conn->device );
    conn->state = ESTABLISHED;
    l2_linkup( conn->device, other_hostname, other_port, other_address );

    DEBUG("Message UP received from %s\n", other_hostname );

    return conn;
}

/*
 * In interrupt occurs when data arrives. Our interrupts are simulated
 * by data-arrival events in the select loop.
 * When select notices that data for my_udp_socket has arrived, it calls
 * this function.
 *
 * A positive return value means that all data has been delivered.
 * A zero return value means that the receiver can not receive the
 * data right now.
 * A negative return value means that an error has occured and
 * receiving failed.
 *
 * NOTE:
 * Link layer error correction and flow control must be considered
 * here. You will certainly need several helper functions because
 * you will need to perform retransmissions after a timeout.
 */
void l1_handle_event( )
{
    char buf[4096];
    struct sockaddr_in from;
    socklen_t fromlen = sizeof(from);

    ssize_t num = recvfrom( my_udp_socket, buf, sizeof(buf), 0, (struct sockaddr*) &from, &fromlen );

    phys_conn_t *conn = get_phys_conn( &from, from.sin_port );


    if( !conn ) {
        /* Connection not found */

        if( strstr( buf, "CONNECT" ) ) {
            /* Create connection */

            /* Get the remote machine's hostname */
            struct hostent *he;
            he = gethostbyaddr( &from.sin_addr, sizeof(struct in_addr), AF_INET );

            int remote_mac;
            sscanf( buf, "%*s %d", &remote_mac );

            conn = l1_linkup( conn, he->h_name, ntohs(from.sin_port), remote_mac );

            /* Send an UP message back */
            char msg[20];
            int local_mac = l2_get_mac( conn->device );
            snprintf( msg, sizeof(msg), "UP %d", local_mac );

            ssize_t num = sendto( my_udp_socket, msg, strlen(msg) + 1, 0, (struct sockaddr*) &conn->addr, sizeof(struct sockaddr_in) );

            if (num == -1 )
                perror( "sendto" );

            return;
        }
        else
        {
            DEBUG("Received %d bytes of invalid data\n", (int) num );
            return;
        }
    }

    if( conn->state == CONNECTING )
    {
        if( strstr( buf, "UP" ) ) {
            int remote_mac;
            sscanf( buf, "%*s %d", &remote_mac );

            /* Connection established */
            l1_linkup( conn, conn->remote_hostname, conn->remote_port, remote_mac );

            fprintf(stderr, "Connection established\n" );
            return;
        }
        else
        {
            DEBUG("Received %d bytes of invalid data\n", (int) num );
            return;
        }
    }

    /* An established connection. Send to layer 2 */
    l2_recv( conn->device, buf, num );
}

