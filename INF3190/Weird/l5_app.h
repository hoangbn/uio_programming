#ifndef L5_APP_H
#define L5_APP_H

#define UNUSED_PORT -1
#define OWN_PORT    -2

/* see comments in the c file */

int l5_get_header_size();
void l5_init( );
void l5_linkup( int other_address, const char* other_hostname, int other_port );

void l5_handle_keyboard( );
int l5_recv( int dest_pid, int src_address, int src_port, const char* l5buf, int sz );
void l5_send(void *param);

#endif /* L5_APP_H */

