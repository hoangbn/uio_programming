#include <sys/time.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "general.h"
#include "irq.h"
#include "l1_phys.h"
#include "l4_trans.h"
#include "l5_app.h"
#include "slow_receiver.h"

// Can only handle one sending of files simultaneously
char filename[30];
static int total_bytes_sent;
static int remote_mac;

// The udp port to listen on.
int my_port;

// Maps remote mac to remote ports
int mac_to_port[MAX_CONNS];

/*
 * Initialize however you want.
 */
void l5_init(int mac, int port)
{
    static int first = 1;
    int i;

    if (first)
    {
	for (i=0; i < MAX_CONNS; i++)
	    mac_to_port[i] = UNUSED_PORT;

	my_port = port;
	first = 0;
    }

    mac_to_port[mac] = OWN_PORT;
}

/*
 * Report success for a physical link establishment.
 */
void l5_linkup( int other_address, const char* other_hostname, int other_port )
{
    fprintf( stderr, "Successfully established a physical link (plugged in a cable)\n"
                     "with host:port %s:%d.\n"
                     "We can use the address >>%d<< for that machine.\n"
                     "\n",
                     other_hostname,
                     other_port,
                     other_address );

    mac_to_port[other_address] = other_port;
}

/* Size of this layer's headers, and the ones below. */
int l5_get_header_size() {
    return l4_get_header_size();
}

/* Parses the keyboard input and delegates to appropriate functions. */
void l5_handle_keyboard( )
{
    char  buffer[1024];
    char *retval;

    /*
     * The use of gets() is discouraged because it's a security hole.
     * If you input more than buffer's size, bad things can happen.
     * It's just such a convenient little function ...
     * Feel free to make something better.
     */
    retval = fgets( buffer, sizeof(buffer), stdin );
    if( retval != 0 )
    {
        buffer[strlen(buffer)-1] = 0;

        DEBUG("The buffer contains: >>%s<<\n", buffer );

        if( strstr( buffer, "CONNECT" ) != NULL )
        {
            char hostname[1024];
            int  port;
            int  device;

            /*
             * sscanf tries to find the pattern in the buffer, and extracts
             * the parts into the given variables.
             */
            int ret = sscanf( buffer, "CONNECT %s %d", hostname, &port );
            if( ret == 2 )
            {
                // two matches, we got our hostname and port
                device = l4_connect( hostname, port );
                DEBUG("Physical connection to %s:%d has device number %d\n",hostname, port, device );
            } else
		fprintf(stderr,"Invalid arguments to (%s) to CONNECT. Use \"hostname port\"\n",buffer+7);
        } else if (strstr(buffer,"QUIT") != NULL) {
            DEBUG("Exiting as the user entered: >>%s<<\n", buffer);
	    fprintf(stderr, "OK, exiting program.\n");
            exit(0);
        } else if (strstr(buffer,"SEND") != NULL) {
            if (sscanf(buffer, "SEND %d %s", &remote_mac, filename) == 2)
                l5_send(NULL);
            else
		fprintf(stderr, "Invalid arguments (%s) to SEND. Use \"mac filename\"\n",buffer+4);
        } else {
	    fprintf(stderr,"Available commands:\n");
	    fprintf(stderr,"CONNECT hostname port --- Connects to a host:port\n");
	    fprintf(stderr,"SEND mac filename     --- Sends file to mac adress\n");
	    fprintf(stderr,"QUIT                  --- Quits program\n");
	}
    }
}

/* 
 * Receives a buffer from l4 and passes it blindly up to slow_receiver.
 * Returns slow_receiver's return value.
 */
int l5_recv( int dest_pid, int src_address, int src_port, const char* l5buf, int sz )
{
    return slow_receiver(l5buf, sz);
}

/* 
 * Sends the file to the host which the user has entered. It will be called
 * by the callback system in order to finish the sending, and hence it uses
 * a global variable to be able to restart at the last position.
 * */
void l5_send(void *param)
{
    FILE *file;

    char *buffer;
    int max_read, read, ret;

    // If length of filename is 0, we are finished reading the file, just
    // some stale callbacks trying to finish the job.
    if (strlen(filename) == 0)
	return;

    if (mac_to_port[remote_mac] == OWN_PORT) {
	fprintf(stderr, "Can't send to our own mac!\n");
	return;
    }

    max_read = MAX_FRAME_LENGTH - l5_get_header_size();

    file = fopen(filename, "r");

    if (file == NULL) {
        fprintf(stderr, "Could not open file %s\n", filename);
	return;
    }

    // (Re)start at the correct position.
    fseek(file, total_bytes_sent, SEEK_SET);

    for (;;) {
	if (feof(file)) {
	    printf("Done sending file.\n");
	    // Reset in order to send a new file.
	    total_bytes_sent = 0;
	    bzero(filename, sizeof(filename));
	    break;
	}

	if ((buffer = calloc(1,max_read)) == NULL)
	    errx(-1, "Insufficient memory available, aborting.\n");

	read = fread(buffer, (size_t) 1, max_read, file);

	if (read < max_read && ferror(file))
	{
	    fclose(file);
	    total_bytes_sent = 0;
	    bzero(filename, sizeof(filename));
	    fprintf(stderr, "Error occured while reading %s\n", filename);
	    return;
	}
	
	ret = l4_send(remote_mac, mac_to_port[remote_mac], my_port, buffer, read);

	free(buffer);

	if (ret == E_TRYAGAIN)
	{
	    //Sending didn't go well. Try resending it later.
	    register_timeout_cb(get_time_struct(DEFAULT_TIMEOUT), &l5_send, NULL);
	    break;
	} else
	    total_bytes_sent += ret;

    };

    fclose(file);
}
