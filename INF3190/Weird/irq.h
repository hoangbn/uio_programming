#ifndef IRQ_H
#define IRQ_H

void handle_events( );

void register_timeout_cb( struct timeval tv, void (*cb)(), void* param );

#endif /* IRQ_H */
