       .globl sprinter
       .data
       var:   .byte 37        #% in decimal is 37
       char:  .byte 99        #c in decimal is 99
       hex:   .byte 120       #x in decimal is 120
       dec:   .byte 100	     #d in decimal is 100
       space: .byte 32       #white space in decimal
       int2: .byte 50        #byte fpr 2
       int4: .byte 52        #byte for 4
       int0: .byte 48        #byte for 0
       int9: .byte 57        #byte for 9
       string: .byte 115     #byte for s
       arg1: .long 16        #tredje argument
       arg_temp:	.long 16
       number:	.long 0
       count:   .long 0
       num_space: .long 0
       args_len: .long 0
       space_len: .long 0
        added_space:	.long 0
       negate:	 .long 0
	
sprinter:
  pushl %ebp                #setter opp stakken
  movl  %esp, %ebp          #lagrer esp registeret i ebp registeret
  xor %eax, %eax             #initialisere lengden til 0
	
  movl 8(%ebp), %ecx         #lager en kopi i ecx
  movl 8(%ebp), %ebx         #lager en ekstra kopi for å telle bokstaver
  movl 12(%ebp), %edx        #starten til bufferet
  movl $16, arg1             #initiliserer 16 på arg1

str_cpy:
  movb (%edx), %al         #flytter bokstaven fra strengen til registeret al
  incl %edx                #flytter pekeren framover med 1

  cmpb var, %al            #if the char is a % 
  je compare_char          #jump to compare_char
 
  movb %al, (%ecx)         #flytter bokstaven fra al til ecx som er bufferet vårt
  incl %ecx                #flytter pekeren framover med 1
  cmpb $0, %al             #hvis al er forskjellig fra 0
  jz  str_len              #looper videre
  jmp str_cpy

compare_char:
  movb (%edx), %al         #move the byte next to % to al 
  cmpb var, %al            #check if it's a %
  je percent               #since it a % char jump to percent method
  cmpb char, %al           #sammenligner om byten i al er c
  je add_char              #hopp til add_char
  cmpb string, %al         #sammenligner om al er s
  je make_arg_str          #string som argument
  cmpb hex, %al            #sammenligner om al er x
  je make_arg_hex               #hopp til make_arg_hex
  cmpb dec, %al            #sammenligner om al er d
  je make_arg_int         #hopp til make_arg_cpy2

  cmpb $111, %al            #sammenligner om al er d
  je make_arg_oct         #hopp til make_arg_cpy2

  cmpb int0, %al           #sammenligner på verdien på byten
  jae check_high            #hopp hvis verdien er større enn 0
  cmpb int9, %al           #sammenligner verdien
  jbe check_low             #hopp hvis verdien er mindre enn 9
	
  jmp str_cpy

check_high:
  cmpb int9, %al           #sammenligner verdien
  jbe save_reg             #hopp hvis mindre eller lik
  jmp str_cpy              #fortsett å lese strengen

check_low:
  cmpb int0, %al           #sammenligner på verdien på byten
  jae save_reg             #hopp hvis høyere eller lik
  jmp str_cpy              #fortsett å lese strengen

save_reg:
	pushl %eax
	movl $0, %edi
	movl $0, count		#nullstiller count
	jmp my_atoi

my_atoi:
	cmpb char, %al
        je calculate
	cmpb string, %al          
	je calculate              
	cmpb hex, %al               
	je calculate            
	cmpb dec, %al             
	je calculate

	 movsx %al, %eax      #kopierer samt konvertere 8- og 16-bit til 16- og 32-bit
	 subl $48, %eax       #subtraherer decimal verdien av 0, konvertering til integer
	 imul $10, %edi       #multipliserer summen med 10
	 addl %edi, %eax      #multipliserer eax med edi samt lagre det i eax
	 movl %eax, number    #kopierer summen av eax til number
       	 movl %eax, num_space  
	 movl %eax, %edi      #kopierer summen av eax til edi
	
	 incl %edx            #flytter peker framover
	 movb (%edx), %al     #kopierer byteneste char til al
	
	jmp my_atoi
	
calculate:	
	#sammenligner til vi kommer til et format, da er vi sikke p� at alle sifrene er tatt med
	
	movl $0, added_space
	cmpb char, %al
        je make_space_char
#	cmpb string, %al
#	je calculate_string_len
	cmpb dec, %al
	je make_space_int
#	cmpb hex, %al
#	je make_space_int
	jmp temp

//Denne metoden funker bare p� test 7 siden argument 3 hadde ingen tegn.
calculate_string_len:
 	cmpb $0, number
 	jbe temp
 	movb $32, (%ecx)
 	incl %ecx
	incl added_space
 	subl $1, number
 	jmp calculate_string_len

make_space_int:
	cmpl $0, number
	jbe load_reg
	movb $32, (%ecx)
	incl %ecx
	subl $1, number
	jmp make_space_int
	
make_space_char:
	cmpl $1, number
	je load_reg
	movb space, %al
	movb %al, (%ecx)
	incl %ecx
	incl num_space
	subl $1, number
	jmp make_space_char
	
load_reg:
        popl %eax
	
	jmp compare_char           #lag mellomrom/-ene

temp:
	popl %eax
	jmp compare_char

add_char:
  movl %ebp, %edi          #lager en kopi i edi
  addl arg1, %edi          #peker til en ny addresse i edi
  addl $4, arg1            #øker med 4 slik at vi kan hente neste argument

  movb (%edi), %al         #kopierer byten til al
  movb %al, (%ecx)         #kopierer byten i al til ecx som er bufferet vår
  incl %ecx                #flytter peker framover
  incl %edx                #flytter peker framover

  jmp str_cpy              #hopper til str_cpy

make_arg_str:
	incl %edx
	movl %ebp, %edi          #lager en kopi i edi
	addl arg1, %edi          #peker til en ny addresse i edi
	movl (%edi), %edi
	addl $4, arg1            #øker med 4 slik at vi kan hente neste argument
	jmp add_str              #hopp til add_str

add_str:
        movb (%edi), %al	#flytter byten som er pekt p� i edi til al
	incl %edi		#flytter pekeren framover
	cmpb $0, %al		#sammenligner 0 byte
	jz str_cpy		#hop til str_cpy
	movb %al, (%ecx)	#flytter byten i al til ecx
	incl %ecx		#flytter pekeren i ecx framover
	jmp add_str		#fortsett � loope

make_arg_hex:
	incl %edx
	pushl %edx
	pushl %eax                #lagrer eax i stakken
	pushl %esi
	
	movl %ebp, %eax          #lager en kopi i edx
	addl arg1, %eax          #peker til en ny addresse i edx
	movl (%eax), %eax
	addl $4, arg1            #øker med 4 slik at vi kan hente neste argument

	movl $0, count           #nullstiller count
	movl $16, %esi
	cmpl $0, %eax
	je zero_number	
	jmp hex_loop	

#tar hver eneste siffer i tallet og pusher dem i stakken
hex_loop:
	cmpl $0, %eax
	jz add_hex
	
	movl $0, %edx           #setter edx lik 0
	divl %esi              #svar i eax og rest i edx, da har vi fjernet den f�rste sifferet lengst til h�yre og lagret den i eax
	pushl %edx               #pusher charen i stakken
	addl $1, count           #�ker count med 1
	incl number
	jmp hex_loop

#konverterer 0-9 p� samme m�te som integer og 10-16 adderer man p� 87 istedenfor 48, for � f� hex representasjonen av tallet
add_hex:
	cmpl $0, count		#if count == 0
	je load_stack		#jump to load_stack
	popl %edx		#pop stakk and save it in edx
	cmpl $10, %edx		# 10 <= edx
	jae add_alfa		#jump to add_alfa
	
	addl $48, %edx	 	#add 48 on edx
	movb %dl, (%ecx)	#move byte in dl to ecx
	incl %ecx		#moves the pointer forward
	
	subl $1, count		#substract 1 from count
	jmp add_hex		#continue the loop

make_arg_oct:
	incl %edx
	pushl %edx
	pushl %eax                #lagrer eax i stakken
	pushl %esi
	
	movl %ebp, %eax          #lager en kopi i edx
	addl arg1, %eax          #peker til en ny addresse i edx
	movl (%eax), %eax
	addl $4, arg1            #øker med 4 slik at vi kan hente neste argument

	movl $0, count           #nullstiller count
	movl $8, %esi
	cmpl $0, %eax
	je zero_number	
	jmp oct_loop	

#tar hver eneste siffer i tallet og pusher dem i stakken
oct_loop:
	cmpl $0, %eax
	jz add_oct
	
	movl $0, %edx           #setter edx lik 0
	divl %esi              #svar i eax og rest i edx, da har vi fjernet den f�rste sifferet lengst til h�yre og lagret den i eax
	pushl %edx               #pusher charen i stakken
	addl $1, count           #�ker count med 1
	incl number
	jmp oct_loop

#konverterer 0-9 p� samme m�te som integer og 10-16 adderer man p� 87 istedenfor 48, for � f� hex representasjonen av tallet
add_oct:
	cmpl $0, count		#if count == 0
	je load_stack		#jump to load_stack
	popl %edx		#pop stakk and save it in edx
	
	addl $48, %edx	 	#add 48 on edx
	movb %dl, (%ecx)	#move byte in dl to ecx
	incl %ecx		#moves the pointer forward
	
	subl $1, count		#substract 1 from count
	jmp add_oct		#continue the loop,
	
#konvertering av tall fra 10-16 til hex verdi
add_alfa:
	addl $87, %edx		#add 87 on edx
	movb %dl, (%ecx)	#move the byte in dl to ecx
	incl %ecx		#moves pointer forward
	decl count		#substract count by one

	jmp add_hex		#continue loop
	
#pusher alle registere som m� brukes i integer divisjon inn i stakken og lagrer argumentet i et av registrene
make_arg_int:	
	incl %edx		#dytter pekeren framover
	pushl %edx		#pusher edx i stakken
	pushl %eax              #lagrer eax i stakken
	pushl %esi		#pusher esi i stakken
	
	movl %ebp, %eax         #lager en kopi i edx
	addl arg1, %eax         #peker til en ny addresse i edx
	movl (%eax), %eax	#addressen skal starte p� argumentet vi er ute etter
	addl $4, arg1           #�ker med 4 slik at vi kan hente neste argument

	movl $0, number		#nullstiller
	movl $0, count          #nullstiller count
	movl $10, %esi		#esi = 10
	cmpl $0, %eax		#if eax == 0
	je zero_number		#jump to zero_number 
	
	cmpl $0, %eax		#if eax == 0
	js neg_number		#jump on negative integer
	jmp pushloop            #hopp til pushloop

#kommer hit hvis vi f�r et argument som er 0
zero_number:
	movb $48, (%ecx)
	incl %ecx
	jmp str_cpy

#kommer hit hvis vi f�r negative integer
neg_number:
	neg %eax
	
	incl negate		#to know we got a negative number
	incl number
	jmp pushloop

#kommer hit slik at vi kan sette minus tegn f�r char representasjonene av integerene blir skrevet
negates:
	movb $45, (%ecx)
	incl %ecx
	decl negate
	jmp add_dec

#push all number in to stakk
pushloop:
	cmpl $0, %eax
	jz remove_space
	
	movl $0, %edx           #setter edx lik 0
	idivl %esi              #svar i eax og rest i edx, da har vi fjernet den f�rste sifferet lengst til h�yre og lagret den i eax
	pushl %edx               #pusher charen i stakken
	incl count           #�ker count med 1

	incl number
	jmp pushloop

#remove spaces that we've already made by checking how many bytes the arguments uses
remove_space:
	cmpl $0, num_space
	jbe add_dec
	cmpl $0, number
	jbe add_dec
	jz add_dec
	decl %ecx
	decl number
	
	jmp remove_space

#konvert integer to char and writes it
add_dec:
	cmpl $1, negate		#if negate == 1
	je negates		#that means we've a negative number
	cmpl $0, count
	je load_stack
	popl %edx		#get the next number
	addl $48, %edx
	
	
	movb %dl, (%ecx)
	incl %ecx
	subl $1, count

	jmp add_dec

#since we've done using the registers, we want the old data before, so popping it to their original registers is easiest
load_stack:
	popl %esi
	popl %eax
	popl %edx

	movl $0, num_space
	movl $0, number
	jmp str_cpy

#We got a % sign so we adds it and moves the pointers forward
percent:
  movb %al, (%ecx)         #move % to eax
  incl %ecx                #flytter peker framover
  incl %edx                #flytter peker framover
  jmp str_cpy	

#calculates our strings length
str_len:
   cmpb $0, (%ebx)         #sammenligner om byte er 0
   je exit                 #hvis lik hopp til exit
   movb (%ebx), %dl        #flytter charen som ebx peker på til dl
   incl %eax               #øker antall chars
   incl %ebx               #flytter peker framover
   jmp str_len             #loop om vi er fortsatt ikke på slutten av strenen

exit:
  movl %ebp, %esp          #gjør esp til den opprinnelig tilstanden
  popl  %ebp               #gjør ebp til den opprinnelige tilstanden
  ret
