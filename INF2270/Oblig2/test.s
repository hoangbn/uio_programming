	.file	"test.c"
# GNU C version 4.1.2 20080704 (Red Hat 4.1.2-52) (x86_64-redhat-linux)
#	compiled by GNU C version 4.1.2 20080704 (Red Hat 4.1.2-52).
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -mtune=generic -auxbase -fverbose-asm
# options enabled:  -falign-loops -fargument-alias
# -fasynchronous-unwind-tables -fbranch-count-reg -fcommon -fearly-inlining
# -feliminate-unused-debug-types -ffunction-cse -fgcse-lm -fident
# -finline-functions-called-once -fivopts -fkeep-static-consts
# -fleading-underscore -floop-optimize2 -fmath-errno -fpeephole
# -freg-struct-return -fsched-interblock -fsched-spec
# -fsched-stalled-insns-dep -fshow-column -fsplit-ivs-in-unroller
# -ftrapping-math -ftree-loop-im -ftree-loop-ivcanon -ftree-loop-optimize
# -ftree-vect-loop-version -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -maccumulate-outgoing-args -malign-stringops -mfancy-math-387
# -mfp-ret-in-387 -mfused-madd -mieee-fp -mmmx -mpush-args -mred-zone -msse
# -msse2 -mtls-direct-seg-refs

# Compiler executable checksum: 45e01b3cedcaae60fe06dacaced4f8fb

.globl a
	.bss
	.align 4
	.type	a, @object
	.size	a, 4
a:
	.zero	4
	.text
.globl main
	.type	main, @function
main:
.LFB5:
	pushq	%rbp	#
.LCFI0:
	movq	%rsp, %rbp	#,
.LCFI1:
	jmp	.L2	#
.L3:
	movl	a(%rip), %eax	# a, a.0
	addl	$1, %eax	#, D.2781
	movl	%eax, a(%rip)	# D.2781, a
.L2:
	movl	a(%rip), %eax	# a, a.1
	cmpl	$10, %eax	#, a.1
	jne	.L3	#,
	leave
	ret
.LFE5:
	.size	main, .-main
	.section	.eh_frame,"a",@progbits
.Lframe1:
	.long	.LECIE1-.LSCIE1
.LSCIE1:
	.long	0x0
	.byte	0x1
	.string	"zR"
	.uleb128 0x1
	.sleb128 -8
	.byte	0x10
	.uleb128 0x1
	.byte	0x3
	.byte	0xc
	.uleb128 0x7
	.uleb128 0x8
	.byte	0x90
	.uleb128 0x1
	.align 8
.LECIE1:
.LSFDE1:
	.long	.LEFDE1-.LASFDE1
.LASFDE1:
	.long	.LASFDE1-.Lframe1
	.long	.LFB5
	.long	.LFE5-.LFB5
	.uleb128 0x0
	.byte	0x4
	.long	.LCFI0-.LFB5
	.byte	0xe
	.uleb128 0x10
	.byte	0x86
	.uleb128 0x2
	.byte	0x4
	.long	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0x6
	.align 8
.LEFDE1:
	.ident	"GCC: (GNU) 4.1.2 20080704 (Red Hat 4.1.2-52)"
	.section	.note.GNU-stack,"",@progbits
