#include <stdio.h>
#include <stdarg.h>

int sprinter( char *res, char *format, ...)
{
  va_list arguments;
  
  va_start (arguments, format);
  
  return vsprintf(res, format, arguments);
}
