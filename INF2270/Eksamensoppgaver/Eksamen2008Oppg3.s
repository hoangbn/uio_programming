
	.globl rle

	.data
char:	.byte 0			#random char
int:	.long 0 		#random int
	
rle:
	pushl %ebp		#legger opp stakken

	movl %esp, %ebp		#lagrer esp i stakken
	pushl %ebx
	
	movl 8(%ebp), %eax	#legger f�rste argument i eax
	movl 12(%ebp), %ebx	#legger andre argument i ebx

	movl $0, %ecx
	movl $0, %edx
	
	jmp while_loop

while_loop:
	cmpb $0, (%ebx)
	je end
	
	movb (%ebx), %dl
	movb %dl, char		#c = det from peker p�
	incl %ebx
	movl $1, %ecx		#int = 1
	jmp check_int
	
	
while_loop_continue:
	cmpl $1, %ecx		#n == 1
	je add_char		#hopp add_char
	jmp add_stuff

add_char:
	movb char, %dl
	movb %dl, (%eax)		# t = c
	incl %eax		# t++
	jmp while_loop		#fortsett loopen

add_stuff:
	
	movb $'#', (%eax)
	incl %eax
	movb $'0', %dl 
	addl %ecx, %edx
	movb %dl, (%eax)
	incl %eax
	
	movb char, %dl
	movb %dl, (%eax)
	incl %eax
	jmp while_loop
	
check_int:
	cmpl $9, %ecx
	jl check_char
	jmp while_loop_continue
	
check_char:
	movb char, %dl
	cmpb %dl, (%ebx)
	je inner_while
	jmp while_loop_continue


	
inner_while:
	incl %ecx	
	incl %ebx
	
	jmp check_int

end:
	movb $0, (%eax)
	movl %ebp, %esp
#	popl %ebx
	popl %ebp
	ret
	


