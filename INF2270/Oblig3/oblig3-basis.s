	.extern	fread, fwrite

	.text 

#Parameters
	.data
		buffer:		.long 		#Buffer			
		status:		.byte 0		#status		
	.globl	readbyte
 # Navn:	readbyte
 # Synopsis:	Leser en byte fra en binærfil.
 # C-signatur: 	int readbyte (FILE *f)
 # Registre:
	
readbyte:
	pushl	%ebp		# Standard funksjonsstart
	movl	%esp,%ebp	#

# int readbyte (FILE *f)
#	{
#		int status;
#		char c;
#		status = fread(&c, 1, 1, f);
#		i_f (status <= 0) return -1;
#		return (int)c;
#	}

	movl	8(%ebp), %eax
	pushl	%eax
	pushl	$1
	pushl	$1
	leal	buffer, %edx
	pushl	%edx
	
#	movl	$0, %eax
#	leal	status, %eax
	
#	pushl	buffer
#	pushl	$1
#	pushl	$1
#	leal	8(%ebp), %eax	#Kopier pekeren og push til stakken
#	pushl	%eax
#	pushl	readbyte
	call	fread
	
	
	cmpl	$0, %eax	#Tester om returverdien er 0
	jbe	error

#	movl	buffer, %eax
	jmp	rb_x	
	
	
error:	dec	%eax		#returner -1 for EOF
	
rb_x:	movl	%ebp, %esp	#
	popl	%ebp		# Standard
	ret			# retur.

	.globl	readutf8char
 # Navn:	readutf8char
 # Synopsis:	Leser et Unicode-tegn fra en binærfil.
 # C-signatur: 	long readutf8char (FILE *f)
 # Registre:
	
readutf8char:
	pushl	%ebp		# Standard funksjonsstart
	movl	%esp,%ebp	#

	popl	%ebp		# Standard
	ret			# retur.

############################## Oppgave 1 ##############################
.globl	writebyte
 # Navn:	writebyte
 # Synopsis:	Skriver en byte til en binærfil.
 # C-signatur: 	void writebyte (FILE *f, unsigned char b)
 # Registre:
	
writebyte:
	pushl	%ebp		# Standard funksjonsstart
	movl	%esp,%ebp	#

#void writebyte (FILE *f, byte b)
#{
#	fwrite(&b, 1, 1, f);
#}
#Ideen er å videreføre argumentene/pekerne og kalle på fwrite

#Legg parametrene på stakken i omvendt rekkefølge.
	pushl	8(%ebp)		#Push char b
	pushl	$1
	pushl	$1
	leal	12(%ebp), %eax	#Kopier pekeren og push til stakken
	pushl	%eax

	call fwrite		#Kaller på fwrite fra C bibliotek

	movl	%ebp, %esp	
	popl	%ebp		# Standard
	ret			# retur.

#######################################################################

############################## Oppgave 2 ##############################
	.globl	writeutf8char
 # Navn:	writeutf8char
 # Synopsis:	Skriver et tegn kodet som UTF-8 til en binærfil.
 # C-signatur: 	void writeutf8char (FILE *f, unsigned long u)
 # Registre:
	
writeutf8char:
	pushl	%ebp		# Standard funksjonsstart
	movl	%esp,%ebp	#


#Side 89 i Kompendiet står det informasjon om UTF-8 representasjoner av
#Unicode characters.
#Vi kan identifisere hvor mange bytes som trenges for å dekode en
#karakter ved å sammenligne verdiene. Som vi vet fra komp. så finnes det en
#maks verdi på 1-byte, 2-byte, 3-byte og 4-bytes karakterer (som er 0x7F, 0x7FF
#0xFFFF og 0x10FFFF).

	cmpl	$0x7F, 8(%ebp)		#Tester om den er mellom 0-maks verdi av 1 byte karakter
	jbe	wu8_1byte		
	
	cmpl	$0x7FF, 8(%ebp)		#Tester om den er mellom 0-maks verdi av 1 byte karakter
	jbe	wu8_2bytes		

	cmpl	$0xFFFF, 8(%ebp)	#Tester om den er mellom 0-maks verdi av 1 byte karakter
	jbe	wu8_3bytes

	cmpl	$0x10FFFF, 8(%ebp)	#Tester om den er mellom 0-maks verdi av 1 byte karakter
	jbe	wu8_4bytes		
	

	
	

	
wu8_1byte:
	#Legg parametrene på stakken i omvendt rekkefølge.
	pushl	8(%ebp)		#Push char b
	pushl	$1
	pushl	$1
	leal	12(%ebp), %eax	#Kopier pekeren og push til stakken
	pushl	%eax
	
	call fwrite		#Kaller på fwrite fra C bibliotek

	jmp 	wu8_x
		
wu8_2bytes:
	

	
wu8_3bytes:

wu8_4bytes:	
	
	
	
wu8_x:	movl	%ebp, %esp
	popl	%ebp		# Standard
	ret			# retur.

#######################################################################

