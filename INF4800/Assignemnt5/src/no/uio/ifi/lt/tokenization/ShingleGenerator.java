package no.uio.ifi.lt.tokenization;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.uio.ifi.lt.utils.ArrayIterator;

/**
 * A simple {@link ITokenizer} implementation that generates overlapping
 * shingles. Useful for approximate matching purposes.
 * 
 * @author aleks
 */
public class ShingleGenerator implements ITokenizer {
	
	/**
	 * The shingle size, i.e., the width of our sliding window
	 * over the text buffer,
	 */
	private int width;

	/**
	 * Constructor.
	 * 
	 * @param width the shingle size
	 */
	public ShingleGenerator(int width) {
		this.width = width;
	}

	/**
	 * Implements the {@link ITokenizer} interface.
	 */
	// Override
	public Iterator<IToken> iterator(String text) {
		return new ArrayIterator<IToken>(this.toArray(text));
	}
	
	/**
	 * Implements the {@link ITokenizer} interface.
	 */
	// Override
	public IToken[] toArray(String text) {
		List<IToken> result = new ArrayList<IToken>();
		//A collection of words extracted from the text
		String[] words = text.split(" ");
		
		String shingle;
		int WordIndex = 0;
		
		System.out.println("------------- " + text + " -------------");
		for(int j = 0; j < words.length; j++){
			//The reason for + 1 is to include the index of spaces	
			for(int i = 0; i < words[j].length() - this.width + 1 ;i++){
				shingle = words[j].substring(i, i + this.width);
//				System.out.println(shingle);
//				System.out.println(j);
//				System.out.println(WordIndex);
				result.add(new Token(shingle, j, WordIndex));
			}
			WordIndex += words[j].length() + 1;
		}
		/*
		 * Return a k-gram array of "shingles" from the text. the 'k' is defined by 'this.width'.
		 * 
		 * A k-gram is simply a window over n characters in a string. For example, the set of 3-grams 
		 * from the string backgammon would be the set {bac, ack, ckg, kga, gam, amm, mmo, mon}. 
		 * 
		 */
		
		return (IToken[]) result.toArray();
		
		//throw new RuntimeException("COMPLETE THIS METHOD!");
		

	}

}
