package no.uio.ifi.lt.utils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import no.uio.ifi.lt.storage.IDocumentStore;
import no.uio.ifi.lt.tokenization.IToken;
import no.uio.ifi.lt.tokenization.ITokenizer;


/**
 * Simple class for representing a suffix array over
 * the keys in a {@link IDocumentStore} object.
 *
 * @author aleks
 */
public class SuffixArray {


    /**
     * The dictionary whose keys this suffix array is for.
     */
    private final IDocumentStore dictionary;

    /*
     * TODO:
     * You should decide the data structure of the tokens in the document yourself, and use a class accordingly.
     * This data structure should for all token in the document:
     * 1. Store the document id
     * 2. Store the index where the token starts
     * Well, since it is described to NOT use Object I'll change it to a new data structure Suffix, it is safe to assume that a suffix array
     * has suffixes in them :)
     */
    private final Suffix[] suffixArray;

    /**
     * Constructor.
     *
     * @param dictionary the dictionary whose keys this suffix array is for
     * @param tokenizer  the tokenizer that will determine where the suffixes start
     */
    public SuffixArray(IDocumentStore dictionary, ITokenizer tokenizer) {
        this.dictionary = dictionary;
        this.suffixArray = buildSuffixArray(this.dictionary, tokenizer);
    }
    
    
    /**
     * Data structure
     * 
     * @param documentId the id of the document 
     * @param index tells us where the token or word starts in a dictionary entry, the offset in otther word
     * @param dictionary the dictionary whose keys this suffix array is for
     * 
     * @author hoangbn
     *
     */
    static class Suffix {
    	int documentId;
    	int index;
    	IDocumentStore dictionary;
    	
    	/*Constructor*/
		public Suffix(int docID, int i, IDocumentStore dict) {
			documentId = docID;
    		index = i;
    		dictionary = dict;
		}
    }

    public class MyComparator implements Comparator<Suffix>{

		@Override
		public int compare(Suffix s1, Suffix s2) {
			// TODO Auto-generated method stub
			return 0;
		}
    }
    
    
    /**
     * Builds a suffix array up from the keys in the given dictionary.
     *
     * @param dictionary the dictionary whose keys we want to generate a suffix array for
     * @param tokenizer  the tokenizer that will determine where the suffixes start
     * @return the generated suffix array
     */
    private static Suffix[] buildSuffixArray(IDocumentStore dictionary, ITokenizer tokenizer) {
        
    	//TODO: Choose a data structure, do NOT use Object :) you need to store document id and position index, and write a
    	// comparator accordingly
    	
    	
        // First, count how many expanded items there are. That's easy:
    	//Not sure what they mean with *expanded items*, but this step is probably necessary to count how many suffixes we have to allocate enough
    	//spaces
        int expandedCount = 0;
        
        for (int i = 0; i < dictionary.size(); ++i) {
            String key = dictionary.getDocument(i).getOriginalData();
            Iterator<IToken> iterator = tokenizer.iterator(key);
            while (iterator.hasNext()) {
            	/*token is never read, to loop through it is only necessary with iterator.next()*/
            	IToken token = iterator.next();
                ++expandedCount;
            }
        }
        
        // Allocate memory.
        Suffix[] suffixArray = new Suffix[expandedCount];
        
        // Set the data.
        //TODO: Return a sorted suffix array!
        
        //We're are basically doing the same thing as over, but instead of just counting we'll start to store informations about each 
        //token/word from each document in their respective suffix arrays 
        //the number a tells us which document we are in, and 
        int count = 0;
        Suffix s;
        for( int a = 0; a < dictionary.size(); a++){
        	String key = dictionary.getDocument(a).getOriginalData();
        	Iterator<IToken> iterate = tokenizer.iterator(key);
        	while (iterate.hasNext()){
        		//token can be used here to find the index/offset of a word
        		IToken token = iterate.next();
        		/*We've to store the info about the word in a new data structure which we called Suffix*/
        		s = new Suffix(a, token.getStartIndex(), dictionary);
        		//then add to the array
        		suffixArray[count++] = s;
        	}
        }
        
        // HINT: Make a custom comparator MyComparator, and sort the suffix list with:
        // need it?
        //We can make a new datastructure which implements comparable, and since the data structure is 
        //comparable we can use Arrays.sort on the array, but lets try their way
        //Arrays.sort(suffixArray);
        
        
        throw new RuntimeException("Your task is to complete this method");
        
        //return suffixArray;
        
    }
    
    /**
     * Returns the number of suffixes.
     *
     * @return the number of suffixes
     */
    public int size() {
        return this.suffixArray.length;
    }
    
    /**
     * Returns the index of the dictionary entry that the given suffix index is for.
     *
     * @param index a suffix index
     * @return a dictionary entry index
     */
    public int getEntry(int index) {
    	//TODO: IMPLEMENT THIS!
    	throw new RuntimeException("Complete this method!");
        
    }
    
    /**
     * Returns the offset into the dictionary entry that the given suffix index is for.
     *
     * @param index a suffix index
     * @return an offset into a dictionary entry
     */
    public int getOffset(int index) {
    	
    	//TODO: IMPLEMENT THIS!
    	throw new RuntimeException("Complete this method!");
        
    }
    
    /**
     * Looks up the given key by performing a binary search. A binary search
     * allows prefix matching as well as exact matching.
     * <p/>
     * The caller must ensure that the probe key has the expected case.
     *
     * @param key the key we want to look up in the suffix array
     * @return the suffix index of the key, if found, or the insertion point
     */
    public int lookup(String key) {
        
        
    	
    	
    	//TODO: HINT: Make sure the comparator is working properly, and let Arrays.binarySearch do the
    	// whole job!
    	
        
    	
    	throw new RuntimeException("Fix the binary search call according to your data structure!");
    	
        
        
    }

}
