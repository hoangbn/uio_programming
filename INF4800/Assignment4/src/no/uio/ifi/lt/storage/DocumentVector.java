package no.uio.ifi.lt.storage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import no.uio.ifi.lt.indexing.IInvertedIndex;
import no.uio.ifi.lt.indexing.ILexicon;
import no.uio.ifi.lt.indexing.PostingList;
import no.uio.ifi.lt.ranking.TfIdfRanker;
import no.uio.ifi.lt.tokenization.IToken;

public class DocumentVector implements IDocumentVector {

	IToken[] documentTerms;
	ILexicon lexicon;
	IInvertedIndex invertedIndex;
	TfIdfRanker ranker = new TfIdfRanker(null);
	double vector = 0;
	HashMap<String, Double> tfIdfTerms = new HashMap<String, Double>(); //HashMap for storing term occurrences weighted with tf-idf
	
	
	
	/*
	 * YOU MAY IMPLEMENT THIS CLASS AS YOU WANT, THIS INCLUDES THE INTERFACE AS WELL!
	 * 
	 * THE INTERFACE REFLECTS THE METHODS USED IN THE SOLUTION.
	 * 
	 * 
	 */
	
	public DocumentVector(IToken[] documentTerms, ILexicon lexicon, IInvertedIndex invertedIndex) {
		this.documentTerms = documentTerms;
		this.lexicon = lexicon;
		this.invertedIndex = invertedIndex;
		
		calculateVector();
	}
	
	private void calculateVector() {
		
		//Counting occurrences of words in document
		HashMap<String, Integer> occurrences = new HashMap<String, Integer>();
		for(int i = 0; i<documentTerms.length; i++) {
			if(!occurrences.containsKey(documentTerms[i].getValue())) {
				occurrences.put(documentTerms[i].getValue(), 1);
			} else {
				occurrences.put(documentTerms[i].getValue(), occurrences.get(documentTerms[i].getValue())+1);
			}
		}
		
		// Generate tf-idf values for the occurences
		Iterator<String> tfIdfIt = occurrences.keySet().iterator();
		int tokenId = 0;
		while(tfIdfIt.hasNext()) {
			String word = tfIdfIt.next();
			double tfIdfValue = get(lexicon.lookup(word), tokenId++);
			tfIdfTerms.put(word, tfIdfValue);
		}
		
		/**for(int i = 0; i<documentTerms.length; i++) {
			double tfIdfValue = 0;
			PostingList pl = invertedIndex.getPostingList(lexicon.lookup(documentTerms[i].getValue()));
			for(int j = 0; j<pl.size(); j++) {
				ranker.update(documentTerms[i], pl.getPosting(j), pl);
			}
			tfIdfTerms.put(documentTerms[i].getValue(), ranker.evaluate(null, null));
		}
		
		Iterator<Entry<String, Integer>> tfIdfIt = occurrences.entrySet().iterator();
		ranker.reset();
		int count = 0;
		while(tfIdfIt.hasNext()) {
			PostingList pl = invertedIndex.getPostingList(lexicon.lookup(tfIdfIt.next().getKey()));
			ranker.update(documentTerms[1], pl.getPosting(count++), pl);
		}*/
		
		//Calculates the Euclidean normalization of the tf-idf values
		Iterator<Integer> normalizationIt = occurrences.values().iterator();
		double tmp = 0;
		
		while(normalizationIt.hasNext()) {
			tmp += Math.pow(normalizationIt.next(), 2);
		}
		
		vector = Math.sqrt(tmp);

		//Calculates the Euclidean normalized tf-idf values
		Iterator<String> occurrencesIt = occurrences.keySet().iterator();
		Iterator<Entry<String, Integer>> occurrencesIt2 = occurrences.entrySet().iterator();
		while(occurrencesIt.hasNext()) {
			tfIdfTerms.put(occurrencesIt.next(), occurrencesIt2.next().getValue()/vector);
		}

	}

	// Override
	public double get(int lexiconId, int tokenId) {
		ranker.reset();
		PostingList p = invertedIndex.getPostingList(lexiconId);
		
		for(int i = 0; i<p.size(); i++) {
			ranker.update(documentTerms[tokenId], p.getPosting(i), p); //<-- documentTerms[i] is wrong!
		}
		
		return ranker.evaluate(null, null);
	}

	// Override
	public double getCosineSimilarity(IDocumentVector docVector) {
		double cosineSimilarity = 0;
		Iterator<Double> foreignIt = docVector.getTfIdfScoreIterator();
		Iterator<Double> localIt = this.getTfIdfScoreIterator();

		while(foreignIt.hasNext() && localIt.hasNext()) {
			cosineSimilarity += (foreignIt.next()*localIt.next());
		}
		return cosineSimilarity;
		
	}

	// Override
	public Iterator<Double> getTfIdfScoreIterator() {
		return tfIdfTerms.values().iterator();
	}

	// Override
	public int size() {
		/**
		 * SOMETHING GOTTA BE DONE HERE
		 */
		return (int)vector;
	}
	
	/*
	 * YOU MAY IMPLEMENT THIS CLASS AS YOU WANT, THIS INCLUDES THE INTERFACE AS WELL!
	 * 
	 * THE INTERFACE REFLECTS THE METHODS USED IN THE SOLUTION.
	 * 
	 * 
	 */
	
//	public DocumentVector(IToken[] documentTerms, ILexicon lexicon, IInvertedIndex invertedIndex) {
//		
//		//TODO: IMPLEMENT THIS CLASS!
//		throw new RuntimeException("ASSIGNMENT D, (B): Complete the constructor!");	
//	}
//	
//	
//	// Override
//	public double get(int lexiconId) {
//		throw new RuntimeException("ASSIGNMENT D, (B): Complete this method!");
//	}
//
//	// Override
//	public double getCosineSimilarity(IDocumentVector docVector) {
//		throw new RuntimeException("ASSIGNMENT D, (B): Complete this method!");
//	}
//
//	// Override
//	public Iterator<Double> getTfIdfScoreIterator() {
//		throw new RuntimeException("ASSIGNMENT D, (B): Complete this method!");
//	}
//
//	// Override
//	public int size() {
//		throw new RuntimeException("ASSIGNMENT D, (B): Complete this method!");
//	}

}
