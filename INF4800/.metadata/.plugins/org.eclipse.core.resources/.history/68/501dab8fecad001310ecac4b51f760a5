package no.uio.ifi.lt.search;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;




import no.uio.ifi.lt.indexing.IInvertedIndex;
import no.uio.ifi.lt.indexing.ILexicon;
import no.uio.ifi.lt.indexing.PostingList;
import no.uio.ifi.lt.preprocessing.INormalizer;
import no.uio.ifi.lt.ranking.IRanker;
import no.uio.ifi.lt.storage.IDocumentStore;
import no.uio.ifi.lt.tokenization.IToken;
import no.uio.ifi.lt.tokenization.ITokenizer;
import no.uio.ifi.lt.utils.HeapItem;
import no.uio.ifi.lt.utils.Sieve;

/**
 * Implements the query evaluation logic in a search engine.
 * 
 * @author aleks
 */
public class QueryEvaluator implements IQueryEvaluator {

	/**
	 * Defines the evaluation parameters.
	 */
	private QueryEvaluatorSettings settings;	

	/**
	 * Where we emit messages, if at all.
	 */
	private Logger logger;

	/**
	 * Constructor.
	 * 
	 * @param settings defines the evaluation parameters
	 * @param logger defines where to emit log messages, if at all
	 */
	public QueryEvaluator(QueryEvaluatorSettings settings, Logger logger) {
		this.settings = settings;
		this.logger = logger;
	}

	/**
	 * Implements the {@link IQueryEvaluator} interface.
	 */
	// Override
	public IResultSet evaluate(IQuery query, IInvertedIndex invertedIndex, IRanker ranker) {

		// Paranoia.
		if (query.getNormalizedLength() == 0) {
			return new ResultSet(query, 0);
		}


		// Spam the logs?
		boolean debug =  this.settings.debug && (this.logger != null) && this.logger.isLoggable(Level.FINEST);

		// Should the ranker spam the logs as well?
		ranker.debug(debug);

		// Synchronize query processing with document processing.
		INormalizer normalizer = invertedIndex.getNormalizer();
		ITokenizer tokenizer = invertedIndex.getTokenizer();
		ILexicon lexicon = invertedIndex.getLexicon();
		IDocumentStore documentStore = invertedIndex.getDocumentStore();

		// Process a normalized version, not the raw value.
		String normalizedQuery = normalizer.normalize(query.getOriginalQuery());

		// Split the query string up into terms.

		IToken[] queryTerms = tokenizer.toArray(normalizedQuery);
		/*
        TODO: 

        Now you have all the queries, and an inverted index. Your task is to retrieve ranks for each document 
        according to the recall of the document, and make sure you keep track of the highest scoring documents!

        After doing this, you can return the most relevant documents
        as a ResultSet, containing no more than 10 results. 
        (you should use the value in this.settings.candidates for the size of the ResultSet). 

        Do program efficiently! Do not traverse unnecessary, or keep things in memory if
        it is not called for. Optimizing will be rewarded!

        There is pre-code (i.e. Sieve) you may find useful, or you may program everything yourself!


		THE LAST TWO LINES COULD LOOK LIKE THIS:

       results.sortByRelevance();
       return results;
	 */
		/*Creating a ResultSet to return it, as prescribed, we can use the this.settings.candidates for the size.*/
		ResultSet results = new ResultSet(query, this.settings.candidates);
		
		//Result result = new Result(document, relevance)
		
		//System.out.println(normalizedQuery);
		//System.out.println(results.size());
		/*Need to add stuff in results*/
		
		//Need of PostingLists?
		/*We'll have to first find out where each of the terms in the query occur in which documents*/
		
		/*Make an array of PostingList with the size of the number of terms in the query*/
		PostingList plArray[] = new PostingList[queryTerms.length];
		HashMap<Integer, Double> ranking = new HashMap<Integer, Double>();
		
		/*Populate the PostingList array*/
		int key = 0;
		Double value = (double) 0;
		for(int i = 0; i < plArray.length;i++){
			//System.out.println(plArray.length);
			/*Assuming that our inverted index has that particular PostingList, we can find its lexicon id by lookup in lexicon*/
			plArray[i] = invertedIndex.getPostingList(lexicon.lookup(queryTerms[i].getValue()));
			for(int j = 0; j < plArray[i].size(); j ++){
				key = plArray[i].getPosting(j).getDocumentId();
				value = ranking.get(key);
				
				/*If there is a key in the map then increment its value. Else make a new key and add 1*/
				if(ranking.containsKey(key)){
					ranking.put(key, value + 1);
				} else {
					ranking.put(key, (double) 1);
				}
			}
		}
		//System.out.println(ranking.size());
		
//		for (Map.Entry<Integer, Integer> entry : ranking.entrySet()) {
//			System.out.println("Key : " + entry.getKey() + " Value : "
//				+ entry.getValue());
//		}
		
		//Count how many documents has those query.
//		for(int i = 0 ; i < queryTerms.length; i++){
//			System.out.println(queryTerms[i].getValue());
//		}
		
		/*Use this to add in the different results we got and its ranking, ranking is baded on how many occurences the query has on a document*/
		/*Initially an Integer but it seems like in Result it wanted a Double as its input, and since we'll have to divide numbers it will probably be best
		 * to use Double instead of casting Integer to Double everywhere*/
		Sieve<Result, Double> sieve = new Sieve<Result, Double>(this.settings.candidates);
		Result result = null;
		
		System.out.println(sieve.size());
		for (Entry<Integer, Double> entry : ranking.entrySet()) {
			result = new Result(documentStore.getDocument(entry.getKey()), entry.getValue());
			/*If the result has a lower relevance score than the threshold in the settings then we will ignore it*/
			if((result.getRelevance()/queryTerms.length) > this.settings.rankThreshold){
				/*result.getRelevance and entry.getValue() are basically the same*/
				sieve.sift(result, result.getRelevance());
			}
		}
		System.out.println(sieve.size());
		Iterator<HeapItem<Result, Double>> hItem = sieve.iterator();
		
		while(hItem.hasNext()) results.appendResult(hItem.next().data);
		/*The ResultSet only has Result, so we've to extract the results from Sieve by using HeapItem, they've made it for us to easily extract 
		 * the data or the ranking*/
		//HeapItem<Result, Integer> hiep
	
		//Recall is defined as the number of relevant documents retrieved by a search divided by the total number of existing relevant documents
		
		// results.appendResult(result)
		results.sortByRelevance();
		//System.out.println(results.size());
		
		return results;
	// throw new RuntimeException("Your task is to complete this method!");	
	}
}
