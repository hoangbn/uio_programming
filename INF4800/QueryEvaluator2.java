package no.uio.ifi.lt.search;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.uio.ifi.lt.indexing.IInvertedIndex;
import no.uio.ifi.lt.indexing.ILexicon;
import no.uio.ifi.lt.indexing.PostingList;
import no.uio.ifi.lt.preprocessing.INormalizer;
import no.uio.ifi.lt.ranking.IRanker;
import no.uio.ifi.lt.storage.IDocumentStore;
import no.uio.ifi.lt.tokenization.IToken;
import no.uio.ifi.lt.tokenization.ITokenizer;
import no.uio.ifi.lt.utils.HeapItem;
import no.uio.ifi.lt.utils.Sieve;

/**
 * Implements the query evaluation logic in a search engine.
 * 
 * @author aleks
 */
public class QueryEvaluator implements IQueryEvaluator {

	/**
	 * Defines the evaluation parameters.
	 */
	private QueryEvaluatorSettings settings;	
	
	/**
	 * Where we emit messages, if at all.
	 */
	private Logger logger;
	
    /**
     * Constructor.
     * 
     * @param settings defines the evaluation parameters
     * @param logger defines where to emit log messages, if at all
     */
    public QueryEvaluator(QueryEvaluatorSettings settings, Logger logger) {
    	this.settings = settings;
    	this.logger = logger;
    }
    
	/**
	 * Implements the {@link IQueryEvaluator} interface.
	 */
	// Override
	public IResultSet evaluate(IQuery query, IInvertedIndex invertedIndex, IRanker ranker) {
		
		// Paranoia.
        if (query.getNormalizedLength() == 0) {
        	return new ResultSet(query, 0);
        }
       
        // Spam the logs?
        boolean debug =  this.settings.debug && (this.logger != null) && this.logger.isLoggable(Level.FINEST);

        // Should the ranker spam the logs as well?
        ranker.debug(debug);
        
        // Synchronize query processing with document processing.
        INormalizer normalizer = invertedIndex.getNormalizer();
        ITokenizer tokenizer = invertedIndex.getTokenizer();
        ILexicon lexicon = invertedIndex.getLexicon();
        IDocumentStore documentStore = invertedIndex.getDocumentStore();

        // Process a normalized version, not the raw value.
        String normalizedQuery = normalizer.normalize(query.getOriginalQuery());

        
        // Split the query string up into terms.
        IToken[] queryTerms = tokenizer.toArray(normalizedQuery);
        
        //Declares a new array for each posting list
        PostingList docIdList[] = new PostingList[queryTerms.length];
        
        //Declares a new resultSet
        ResultSet results = new ResultSet(query, settings.candidates);
        
        //Makes an array of the posting lists, and finds the highest documentID
        int max = 0;
        for(int i = 0; i < queryTerms.length; i++) {
        	int tmp = lexicon.lookup(queryTerms[i].getValue());
            docIdList[i]  = invertedIndex.getPostingList(tmp);
            
            if(docIdList[i].getLastPosting().getDocumentId() > max)
            	max = docIdList[i].getLastPosting().getDocumentId();
        }
        
        //Print of the posting lists
        /**
        for(int i = 0; i < docIdList.length; i++) {
        	System.out.print("docIdList["+i+"]: ");
        	for(int j = 0; j < docIdList[i].size(); j++) {
        		System.out.print(docIdList[i].getPosting(j).getDocumentId() + ", ");
        	}
        	System.out.println("");
        }*/

        //Declares a new array for counting documentID occurrences.
        int[] occurrences = new int[max+1];
        for(int i = 0; i < docIdList.length; i++) {
        	for(int j = 0; j < docIdList[i].size(); j++) {
        		occurrences[docIdList[i].getPosting(j).getDocumentId()]++;
        	}
        }
        
        //Print of the occurrences array
        /**
        for(int i = 0; i < occurrences.length; i++) {
        	if(occurrences[i] != 0) {
        		System.out.println("occurrences[" + i + "]: " + occurrences[i]);
        	}
        }*/

        //Adds the top results to a sieve
        Sieve<Result, Double> s = new Sieve<Result, Double>(settings.candidates);
        for(int i = 0; i < occurrences.length; i++) {
        	Result r = new Result(documentStore.getDocument(i), (double)occurrences[i]);
        	if((double)(r.getRelevance()/queryTerms.length) > settings.rankThreshold) {
        		s.sift(r, r.getRelevance());
        	}
        }

        //Iterates the sieve and adds the sieve elements to the result set
        Iterator<HeapItem<Result, Double>> itr = s.iterator();
        while(itr.hasNext()) {
        	results.appendResult(itr.next().data);
        }
        
        results.sortByRelevance();
        	
        return results;
	}
	
	
}
