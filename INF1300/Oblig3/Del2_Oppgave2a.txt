Oppgave 2a)
-----------------------------------------------------------------------------------------------------------------------------
select count(f.filmid) as "Number of films directed",  min(f.prodyear), max(f.prodyear), p.lastname, p.firstname
from filmparticipation x, person p, film f
where
p.lastname = 'Breien' and
p.firstname = 'Anja' and
x.personid = p.personid and
x.filmid = f.filmid and
x.parttype = 'director' group by p.lastname, p.firstname;

 Number of films directed | min  | max  | lastname | firstname 
--------------------------+------+------+----------+-----------
                       14 | 1969 | 2005 | Breien   | Anja
(1 row)
-----------------------------------------------------------------------------------------------------------------------------

Fra vedlegget over ser vi at Anja Breien har regissert 14 filmer fra
1969 til 2005.

Oppgave 2b)

select f.title, f.prodyear, p.lastname, p.firstname
from film f, filmitem i, filmparticipation x, person p
where x.personid = p.personid and 
x.filmid = f.filmid and 
x.parttype = 'director' and
i.filmid = f.filmid and	   
i.filmtype = 'TV' and
prodyear = (select min(prodyear)
from film f, filmitem i, filmparticipation x
where	 
x.filmid = f.filmid and 
x.parttype = 'director' and
i.filmid = f.filmid and	   
i.filmtype = 'TV');

         title          | prodyear | lastname | firstname 
------------------------+----------+----------+-----------
 Queen's Messenger, The |     1928 | Stewart  | Mortimer


indre select setning for � finne den f�rste �ret TV-filmer ble
registrert i databasen.
Siden det fins bare et tv-film i det �ret kom det bare et resultat.


Oppgave 2c)

select genre as Genre, count(f.filmid), (count(f.filmid)*100.0)/((select count(*)
from filmitem fi left outer join filmgenre fg
on fi.filmid = fg.filmid
where
filmtype = 'C'
)) as Prosent		
from filmitem f left outer join filmgenre g
on g.filmid = f.filmid
where
f.filmtype = 'C' group by genre order by Prosent desc;
  genre    | count  | prosent 
-------------+--------+---------
             | 253724 |   34.25
 Short       | 127233 |   17.18
 Drama       |  89019 |   12.02
 Comedy      |  70088 |    9.46
 Documentary |  43626 |    5.89
 Animation   |  17681 |    2.39
 Romance     |  17287 |    2.33
 Action      |  15487 |    2.09
 Thriller    |  12709 |    1.72
 Crime       |  12397 |    1.67
 Family      |   9721 |    1.31
 Western     |   9435 |    1.27
 Adventure   |   9052 |    1.22
 Horror      |   8681 |    1.17
 Adult       |   7271 |    0.98
 Musical     |   7221 |    0.97
 Fantasy     |   5667 |    0.76
 Mystery     |   5488 |    0.74
 War         |   4893 |    0.66
 Sci-Fi      |   4662 |    0.63
 Music       |   2800 |    0.38
 Biography   |   2048 |    0.28
 History     |   2063 |    0.28
 Sport       |   1686 |    0.23
 Film-Noir   |    439 |    0.06
 News        |    393 |    0.05
 Game-Show   |      8 |    0.00
 Talk-Show   |      5 |    0.00
 Reality-TV  |     17 |    0.00

er ikke sikker p� hvorfor man f�r totalsummen av all kinofilmer(de
uten sjanger ogs�) bare ved � bruke left outer join. kinofilmene uten
sjanger har blank felt pga. de hadde ingen tegn i genre plassen
dems(ikke null). er ikke helt sikker p� hvorfor 'blank' genre ble tatt
i betraking n�r man brukte left outer join og n�r man ikke bruke join
blir det ikke med i tabellen...


Oppgave 2d)

select firstname || ' ' || lastname as Navn, count(fi.filmid)
from person p, filmitem fi, filmparticipation ff	  
where 
50 <= (
select count(*) 
from filmitem f, filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'director' and exists(
select distinct f.filmid
from filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'cast' and exists(
select distinct f.filmid
from filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'producer'))) 

and

ff.filmid = fi.filmid and
ff.personid = p.personid and
ff.parttype = 'director' and exists(
select distinct fi.filmid
from filmparticipation ff
where
ff.filmid = fi.filmid and
ff.personid = p.personid and
ff.parttype = 'cast' and exists(
select distinct fi.filmid
from filmparticipation ff
where
ff.filmid = fi.filmid and
ff.personid = p.personid and
ff.parttype = 'producer'))
group by firstname, lastname; 

               navn                 | count 
-------------------------------------+-------
 Gilbert M. 'Broncho Billy' Anderson |   187
 Kevin Murphy                        |    51
 Ed Powers                           |    63
 Michael Landon                      |    85
 Kenny Hotz                          |    59


Optimaliserte SQL sp�rring gikk mye raskere, men en liten forskjell p�
Kenny Hotz, hvorfor er det forskjellig p� bare p� en?

select lastname || ' ' || firstname as name, count(fi.filmid)
from person p, filmitem fi, filmparticipation a, filmparticipation b, filmparticipation c
where
a.personid = p.personid and
b.personid = p.personid and
c.personid = p.personid and
a.filmid = fi.filmid and
b.filmid = fi.filmid and
c.filmid = fi.filmid and
a.parttype = 'director' and
b.parttype = 'cast' and
c.parttype = 'producer'

and

50 <= (
select count(*)
from filmitem fil, filmparticipation d, filmparticipation e, filmparticipation f
where
d.personid = p.personid and
e.personid = p.personid and
f.personid = p.personid and
d.filmid = fil.filmid and
e.filmid = fil.filmid and
f.filmid = fil.filmid and
d.parttype = 'director' and
e.parttype = 'cast' and
f.parttype = 'producer') group by lastname, firstname;

                name                 | count 
-------------------------------------+-------
 Murphy Kevin                        |    51
 Powers Ed                           |    63
 Landon Michael                      |    85
 Hotz Kenny                          |   117
 Anderson Gilbert M. 'Broncho Billy' |   187


oppgave 2e)
  
select lastname || ' ' || firstname as Navn, count(fa.filmid)
from person p, filmitem fa, filmparticipation fb	  
where 
p.gender = 'M' and

20 <= (
select count(*) 
from filmitem f, filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'director' and exists(
select distinct f.filmid
from filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'cast')) 

and

(select count(*)
from filmitem f, filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'director') 
= 
(select count(*) 
from filmitem f, filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'director' and exists(
select distinct f.filmid
from filmparticipation fp
where
fp.filmid = f.filmid and
fp.personid = p.personid and
fp.parttype = 'cast')) 

and

fb.filmid = fa.filmid and
fb.personid = p.personid and
fb.parttype = 'director' and exists(
select distinct fa.filmid
from filmparticipation fb
where
fb.filmid = fa.filmid and
fb.personid = p.personid and
fb.parttype = 'cast')
group by firstname, lastname
order by lastname;

      navn          | count 
-----------------------+-------
 Barril Joan           |   124
 Bay Shaun             |    25
 Beaulieu Trace        |    22
 Buenafuente Andreu    |   220
 Chaplin Charles       |    75
 Costello Maurice      |    79
 Day Dillon            |    32
 Evans Fred            |    71
 Hart Neal             |    23
 Hart William S.       |    53
 Hermida Jes�s         |    22
 Holmes Steve          |    22
 Joan Joel             |    41
 Johnson Arthur V.     |    30
 Manzano Emilio        |    25
 Martin Tony           |    24
 McCulloch Christopher |    28
 Molloy Mick           |    34
 Monegal Ferran        |   113
 Murphy Kevin          |    55
 Myers Harry           |    42
 Nykj�r Elith Nulle    |    28
 N�rgaard Kjeld        |    53
 Pellicer Ramon        |    48
 Petersen S�ren Ryge   |    95
 Reid Wallace          |    56
 Ribas Tony            |    26
 Ryder E.Z.            |    28
 Schmidt Wolf          |    46
 Smith Steve           |    23
 Soler Toni            |    48
 St. John Al           |    22
 Villatoro Vicen�      |    62
 Walsh Darren          |    26
 Willis Dave           |    44


