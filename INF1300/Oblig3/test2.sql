create view role as
select count(f.filmid), p.lastname, p.firstname
from film f, filmparticipation x, person p
where
x.filmid = f.filmid and
x.personid = p.personid and
x.parttype = 'cast' group by p.lastname, p.firstname;


