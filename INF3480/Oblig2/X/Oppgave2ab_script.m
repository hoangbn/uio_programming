syms L1 L2 L3 L4 A1 A2 A3 t1 t2 t3

A2 = [cos(t2) -sin(t2) 0 L2*cos(t2); sin(t2) cos(t2) 0 L2*sin(t2); 0 0 1 0; 0 0 0 1]
A3 = [cos(t3) -sin(t3) 0 (L3 + L4)*cos(t3); sin(t3) cos(t3) 0 (L3 + L4)*sin(t3); 0 0 1 0; 0 0 0 1]
A1 = [cos(t1 + (pi/2)) 0 sin(t1 + (pi/2)) 0; sin(t1 + (pi/2)) 0 -cos(t1 + (pi/2)) 0; 0 1 0 L1; 0 0 0 1]
H_Btot = A1*A2*A3
H_Btot = simple(A1*A2*A3)
H_TtoB = [0 1 0 750 ; -1 0 0 250 ; 0 0 1 -100 ; 0 0 0 1]

pt = simple(H_TtoB * (H_Btot * [0 ; 0 ; 0 ; 1]))

H_3to0 = simple(A1*A2*A3)
H_2to0 = simple(A1*A2)
H_1to0 = simple(A1)

O3 = simple(H_3to0(1:3 , end))
O2 = simple(H_2to0(1:3 , end))
O1 = simple(H_1to0(1:3 , end))
O0 = [0 ; 0 ; 0]

Z0 = [0 ; 0 ; 1]
Z1 = H_2to0(1:3 , 3)
Z2 = H_3to0(1:3 , 3)

Jb = simple([cross(Z0, (O3-O0)) (cross(Z1, (O3-O1))) (cross(Z2, (O3-O2))); Z0 Z1 Z2])

det_J = simple(det(Jb(1:3 , 1:3)))