syms joint_velocities joint_angles

joint_angles = [0 ; 150 ; -45];
joint_velocities = [0.1 ; 0.05 ; 0.05];

cart_velocities = jacobian(joint_angles, joint_velocities);