%Forward kinematics

syms L1 A1 theta1 Velocity Acceleration

A1 = [cos(theta1) -sin(theta1)*cos(0) sin(theta1)*sin(0) L1*cos(theta1) ; sin(theta1) cos(theta1)*cos(0) -cos(theta1)*sin(0) L1*sin(theta1) ; 0 sin(0) cos(0) 0 ; 0 0 0 1]

Acceleration = diff(A1)

Velocity = length(Accelaration)