function cart_cord = forward(joint_angles)
    
L1 = 281
L2 = 239
L3 = 231
L4 = 50
t1 = joint_angles(1, :)
t2 = joint_angles(2, :)
t3 = joint_angles(3, :)

 x = cos(t1)*(L3*cos(t2 + t3) + L4*cos(t2 + t3) + L2*cos(t2)) + 750
 y = 250 - cos(pi/2 + t1)*(L3*cos(t2 + t3) + L4*cos(t2 + t3) + L2*cos(t2))
 z = L1 + sin(t2 + t3)*(L3 + L4) + L2*sin(t2) - 100

cart_cord = [x ; y ; z ; 1]