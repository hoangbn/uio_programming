function resetJoints(robot)

fprintf(robot.joint1,'r\n');
fprintf(robot.joint2,'r\n');
fprintf(robot.joint3,'r\n');

s1 = fscanf(robot.joint1);
s2 = fscanf(robot.joint2);
s3 = fscanf(robot.joint3);

disp(strcat(s1));
disp(strcat(s2));
disp(strcat(s3));