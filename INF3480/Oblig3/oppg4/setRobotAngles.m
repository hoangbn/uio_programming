function setRobotAngles(robot, theta1, theta2, theta3)
% theta in deg

t1 = (theta1*1440/90) + 1440;
t2 = (theta2*1440/90) + 1440;
t3 = (theta3*1440/90) + 1440;

fprintf(robot.joint1,'a%04u',t1);
fprintf(robot.joint2,'a%04u',t2);
fprintf(robot.joint3,'a%04u',t3);

s1 = fscanf(robot.joint1);
s2 = fscanf(robot.joint2);
s3 = fscanf(robot.joint3);

disp(strcat(s1));
disp(strcat(s2));
disp(strcat(s3));