function motorToggle(robot)

fprintf(robot.joint1, 'm\n');
fprintf(robot.joint2, 'm\n');
fprintf(robot.joint3, 'm\n');

s1 = fscanf(robot.joint1);
s2 = fscanf(robot.joint2);
s3 = fscanf(robot.joint3);

disp(strcat(s1));
disp(strcat(s2));
disp(strcat(s3));