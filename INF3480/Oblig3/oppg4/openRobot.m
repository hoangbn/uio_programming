function robot = openRobot(com_j1, com_j2, com_j3)

joint1 = serial(com_j1);
joint2 = serial(com_j2);
joint3 = serial(com_j3);

set(joint1,'BaudRate',115200);
set(joint2,'BaudRate',115200);
set(joint3,'BaudRate',115200);

fopen(joint1);
fopen(joint2);
fopen(joint3);

robot.joint1 = joint1;
robot.joint2 = joint2;
robot.joint3 = joint3;

pause(5);

resetJoints(robot);
