function closeRobot(robot)

fclose(robot.joint1);
fclose(robot.joint2);
fclose(robot.joint3);

delete(robot.joint1);
delete(robot.joint2);
delete(robot.joint3);
