function setRobotAngles(theta)
%Input: three robot angles (unit: degrees)

global ax1 ax2 ax3

theta(1) = theta(1)*10000/360-1;
theta(2) = theta(2)*10000/360-1;
theta(3) = theta(3)*10000/360-1;

    %NB! Animation must be activated first
    fwrite(ax1,sprintf('a%04d', int16(theta(1))))
    fwrite(ax2,sprintf('a%04d', int16(theta(2))))
    fwrite(ax3,sprintf('a%04d', int16(theta(3))))
     
end
