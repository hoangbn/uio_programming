%
% Script for setting up the udp connections and activating 
% communication with the animation
%

global ax1 ax2 ax3 send receive

% Setting up udp ports for communicating with the Arduinos
ax1 = udp('127.0.0.1',7701);
ax2 = udp('127.0.0.1',7702);
ax3 = udp('127.0.0.1',7703);

% Opening the ports
fopen(ax1);
fopen(ax2);
fopen(ax3);

% Addition to receive feedback from Max
send = udp();
send.RemotePort = 5679;
receive = udp('10.10.10.1',9091);
receive.LocalPort = 5677;
%receive.RemotePort = 9091;
%receive.RemoteHost = '127.0.0.1';
%receive.LocalHost = '127.0.0.1';

fopen(send);
fopen(receive);