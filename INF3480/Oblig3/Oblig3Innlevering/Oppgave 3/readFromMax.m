function position = readFromMax()

global send receive;

fwrite(send,'1')
position = str2num(fscanf(receive));

end