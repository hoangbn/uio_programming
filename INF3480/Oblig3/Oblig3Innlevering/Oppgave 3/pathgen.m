function answer = pathgen( radius, origo_cord)
    setRobotAngles(inverse([origo_cord]));
    pause(5)
    delayTime = 0.5
    thetaO = 0;
    degrees = pi/2;
    points = 36;
    inc = ((2*pi)/points)
    n = 0
    draw = [0 ; 0 ; 0]
    while n <= points
         x0 = 0
         y0 = radius * sin(thetaO)
         z0 = radius * cos(thetaO)
         x = x0*cos(degrees) + z0 * sin(degrees)
         y = y0
         z = x0 * (-sin(degrees)) + z0*cos(degrees)
         
         draw = [x + 450; y + 0; z + 200]
         setRobotAngles(inverse(draw));
         pause(delayTime)
         thetaO = thetaO + inc
         n = n + 1
end

