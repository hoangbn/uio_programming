function joint_angles = inverse(cart_cord)

L1 = 281
L2 = 238.93
L3 = 231
L4 = 50

theta1 = atan2(-cart_cord(1, 1), cart_cord(2, 1))

r = sqrt((cart_cord(1, 1))^2 + (cart_cord(2, 1))^2)
s = cart_cord(3, 1) - L1

D = (r^2 + s^2 - L2^2 - (L3+L4)^2) / (2*L2*(L3+L4))

if D > 1
    D = 1
elseif D < -1
    D = -1
end

theta3 = (atan2(sqrt(1-D^2), D))

theta2 = atan2(s, r) - atan2((L3+L4)*sin(theta3), L2 + (L3+L4)*cos(theta3))
joint_angles = [radtodeg(theta1 + pi/2) ; radtodeg(-theta2) ; radtodeg(-theta3)]

