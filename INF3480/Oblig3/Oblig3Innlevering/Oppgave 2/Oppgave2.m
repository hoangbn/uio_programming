%Oppgave 2a
syms th1 th2 th3 th1_dt th2_dt th3_dt L1 L2 L3 L4
syms m1 m2 m3 r1 J1

%For � finne m1 s� m� vi finne DH parameteren dens, det er tilstrekkelig � 
%ha bare den 1. linken
%DH-parameteren blir da:
%link  ai alfai   di    thetai
%  1    0   0    L1/2    th1
% di er L1/2 er fordi i f�lge teksten skulle massen, m1, v�re fordelt
% jevnt ut slik at for at basen til sylinderen er i midten

C1=[cos(th1) -sin(th1) 0 0; sin(th1) cos(th1) 0 0; 0 0 1 (L1/2); 0 0 0 1];

%Man finner zi parameterne i den tredje kolonnen i C1 som er translasjonen
%fra senteret til sylinderen til basen.
z1=C1(1:3,3);

z0=z1;

%Man finner oi parameterne i den fjerde kolonnen i C1

o1=C1(1:3,4);

o0=[0;0;0];

%Vi vet at Jv er oppgitt av den �vre delen av jacobian og Jw den nedre
%slik at vi f�r da
Jac1=[cross(z0,(o1-o0)) ; z0];

Jv1=Jac1(1:3, 1);
Jw1=Jac1(4:6,1);

v1=Jv1*th1_dt;
w1=Jw1*th1_dt;

%Er ikke helt sikker p� hva de mener med at hvis man roterer rundt z-aksen
%f�r man J1 som:
J1_zz=((m1*(r1)^2) / 2);
%eller er resten av J1 lik 0
K1 = (1/2)*m1*transpose(v1)*v1 + (1/2)*transpose(w1)*J1*w1
%skulle vi ha brukt J1_zz her istedenfor?

%DH parameteren til m2 blir da
% link   ai    alfa1   di    thetai
%   1    0       pi    L1       O*
%   2    L2       0     0       O*

A1=[0 0 0 0; 0 -1 0 0; 0 0 -1 L1; 0 0 0 1];
A2=[1 0 0 L2; 0 -1 0 0; 0 0 1 0; 0 0 0 1];

T_m2to0=A1*A2;

z2=T_m2to0(1:3,3);
z1=A1(1:3,3);
z0=[0;0;0];

o2=T_m2to0(1:3,4);
o1=A1(1:3,4);
o0=[0;0;0];

Jac2=[cross(z0, (o2-o0)) cross(z1, (o2-o1)); z0 z1];
Jv2=Jac2(1:3,1:2);
Jw2=Jac2(4:6,1:2);

v2=Jv2*[th1_dt ; th2_dt];
w2=Jw2*[th1_dt ; th2_dt];

%Fra oppgave teksten f�r vi vite at inertia tensor J2 er 0 siden
%den er en point mass -> peker masse? Antar de mener J2 = 0

J2=0;

K2=((1/2)*m2*transpose(v2)*v2) + ((1/2)*transpose(w2)*J2*w2)

%Da var svaret som jeg fikk 0... noe skan tyde p� regnefeil ellers  s� har 
% den bare ikke kinetisk energi.

%Oppgave 2b
%siden vi har 'l�st' a) s� kan man omforme formelen fra a) for � finne m
m1=((2*K1)-(transpose(w1)*J1*w1)) / (transpose(v1)*v1);
m2=((2*K2)-(transpose(w2)*J2*w2)) / (transpose(v2)*v2);

%h�yden til Link 1 en lengden p� den
h1 = L1;

%for h2 kan man finne den ut ved � bruke pytagoras setning
%det medf�lger en liten bilde som kan gi deg en litt mer oversikt
%H2 er lik L1 + a, og for � finnebruker vi L2 * sin(th2) da er h2:
h2=L1 + (L2 * sin(th2));

%g er oppgitt som 9.81 som er gravitasjonskraften
g = 9.81;
P1=m1*g*h1
P2=m2*g*h2


%Oppgave 2c. forst�r fortsatt ikke helt hvordan man g�r fremover med
%Euler lagrange

%Oppgave 2d)
%Det st�r at man kan finne jacobian ved � bruke equation 18 fra oppgaven
J_00 = -sin(th1)*(L2*cos(th2)+L3*cos(th2+th3));
J_01 = -cos(th1)*(L2*sin(th2)+L3*sin(th2+th3));
J_02 = -cos(th1)*(L3*sin(th2 + th3));

J_10 = cos(th1)*(L2*cos(th2)+L3*cos(th2+th3));
J_11 = -sin(th1)*(L2*sin(th2)+L3*sin(th2+th3));
J_12 = -sin(th1)*(L3*sin(th2 + th3));

J_20 = 0;
J_21 = L2*sin(th2)+L3*sin(th2+th3);
J_22 = L3*sin(th2 + th3);

J_30 = 0;
J_31 = sin(th1);
J_32 = sin(th1);

J_40 = 0;
J_41 = -cos(th1);
J_42 = -cos(th1);

J_50 = 1;
J_51 = 0;
J_52 = 0;

Jv3 = [J_00 J_01 J_02; J_10 J_11 J_12; J_20 J_21 J_22];
Jw3 = [J_30 J_31 J_32; J_40 J_41 J_42; J_50 J_51 J_52];

v3 = Jv3* [th1_dt ; th2_dt ; th3_dt];
w3 = Jw3* [th1_dt ; th2_dt ; th3_dt];

%Siden fra teksten sto det at joint 3 er modellert i samme m�te som Joint 2
%s� antar jeg at Inertia tensor J3 = J2 = 0
J3 = J2;
K3=((1/2)*m3*transpose(v3)*v3) + ((1/2)*transpose(w3)*J3*w3)
m3=((2*K3)-(transpose(w3)*J3*w3)) / (transpose(v3)*v3);

%h3 fnner man ganske lett ved � addere h�yden til 1 og 2 joint og L3 og L4
%siden L3 st�r loddrett og L4 er en extension av L3
h3 = h1+h2+L3+L4;

%potensiell energi blir da
P3 = m3*g*h3

%Oppgave 2e) Fortsatt euler lagrange tr�bbel

    

