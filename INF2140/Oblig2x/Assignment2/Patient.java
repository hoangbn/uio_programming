package Assignment2;

public class Patient extends Thread {
	int pat_nmb, chair_pos_x, chair_pos_y;
	boolean payed, waiting, entered, open_mouth, got_anesthesia, pulled_tooth;
	
	Patient(int number){
		pat_nmb = number;
		payed = false;
		waiting = false;
		entered = false;
		open_mouth = false;
		got_anesthesia = false;
		pulled_tooth = false;
	}
	
	public boolean doneTreatment(){
		if(pay() && waiting() && entered() && open_mouth && got_anesthesia && pulled_tooth){
			return true;		
		} else {
			return false;
		}
	}
	
	public void satChair(int x, int y){
		chair_pos_x = x;
		chair_pos_y = y;
	}
	
	public int getNum(){
		return pat_nmb;
	}
	
	public boolean pay(){
		payed = true;
		return payed;
	}
	
	public boolean waiting(){
		return waiting;
	}
	
	public boolean entered(){
		return entered;
	}
	
	public boolean open_mouth(){
		return open_mouth;
	}
	public boolean got_anesthesia(){
		return got_anesthesia;
	}
	
	public boolean pulled_tooth(){
		return pulled_tooth;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
