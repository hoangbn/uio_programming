package Assignment2;

public class Chair {
	Patient id;
	Secretary s;
	boolean occupied;
	int chair_id, pos_x, pos_y;
	
	Chair(int number, Secretary sc){
		this.s = sc;
		chair_id = number;
		occupied = false;
	}
	
	public void position(int x, int y){
		pos_x = x;
		pos_y = y;
	}
	
	public boolean status(){
		return occupied;
	}
	
	public void sit(){
		occupied = true;
	}
	
	public void free(){
		occupied = false;
		id = null;
		s.changeChairsLeft(1);
	}
}
