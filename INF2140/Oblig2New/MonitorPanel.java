package Assignment2;

import java.awt.GridLayout;
import java.awt.Label;
import javax.swing.JPanel;


@SuppressWarnings("serial")
class MonitorPanel extends JPanel {	
	Secretary s;
	Thread th;
	final static int chairs = 4;
	final static int dentists = 2;
	final static int customers = 20;
	
	int count = 0;
	int dentist_count = 0;
	int chair_count = 0;
	String newLine;
	Label[][] labels;
	Patient[] p;
	Dentist[] d;
	Chair[] c;
	int highest;
	
	MonitorPanel () {
		s = new Secretary(customers, dentists, chairs);
		p = new Patient[customers];
		d = new Dentist[dentists];
		c = new Chair[chairs];
		highest = customers;
		if(highest < dentists) highest = dentists;
		if(highest < chairs) highest = chairs;
		for(int i = 0; i < highest; i++){
			if(i < customers){
				p[i] = new Patient(i+1, s);
			}
			if(i < dentists){
				d[i] = new Dentist(i+1, s);
			}
			if(i < chairs){
				c[i] = new Chair(i+1, s);
			}
		}
		
		newLine = System.getProperty("line.separator");
		setLayout ( new GridLayout (dentists, chairs));
		labels = new Label[dentists][chairs];
		for(int i = 0; i < dentists; i++){
			for(int j = 0; j < chairs; j++){
				if(chairs - 1 == j){
					labels[i][j] = new Label("Dentist " + (i+1) + newLine + " Available");
					add(labels[i][j]);
					d[dentist_count].position(i, j);
					dentist_count++;
				//	add(new Label("Dentist " + (i+1) + newLine + " Not Occupied"));
					break;
				}
				if(count == chairs)	{
					labels[i][j] = new Label(" ");
					add(labels[i][j]);
					//add(new Label(" "));
				}
				else {
					count++;	
					labels[i][j] = new Label("Chair " + count + newLine + " Available");
					add(labels[i][j]);
					c[chair_count].position(i, j);
					chair_count++;
					//add(new Label("Chair " + count + newLine + " Not Occupied"));
				}
			}
		}	
		
	}
	
	public Patient getPatient(int id){
		Patient pa = null;
		for(int i = 0; i < p.length;i++){
			pa = p[i];
			if(pa.pat_nmb == id){
				return pa;
			}
		}
		return pa;
	}
	
	public void updateApp(int i, int j, String what){
		labels[i][j].setText(what);
	}
	
	public void start(){

		System.out.println("Dental Clinic is now open");
		for(int a = 0; a < customers;a++){
			th = new Thread(p[a]);
			th.start();
		}
	
	}
	
	public void stop(){
	}

	public Secretary getSecretary() {
		return s;
	}
}