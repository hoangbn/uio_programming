package Assignment2;

public class Patient implements Runnable {
	int pat_nmb, chair_pos_x, chair_pos_y;
	boolean payed, waiting, entered, open_mouth, got_anesthesia, pulled_tooth, visit;
	Secretary s;
	
	Patient(int number, Secretary sc){
		this.s = sc;
	//	System.out.printf("Number: %d, same: %d, %d, %d\n", number, s.customers_left, s.chairs_left, s.dentists_left);
		pat_nmb = number;
		payed = false;
		waiting = false;
		entered = false;
		open_mouth = false;
		got_anesthesia = false;
		pulled_tooth = false;
		visit = false;
	}
	
	public void run(){
		try {
			//s.test();
			s.monitor(getNum());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}
	
	
	public boolean doneTreatment(){
		if(pay() && waiting() && entered() && open_mouth && got_anesthesia && pulled_tooth){
			return true;		
		} else {
			return false;
		}
	}
	
	public void satChair(int x, int y){
		chair_pos_x = x;
		chair_pos_y = y;
	}
	
	public int getNum(){
		return pat_nmb;
	}
	
	public boolean pay(){
		payed = true;
		return payed;
	}
	
	public boolean waiting(){
		return waiting;
	}
	
	public boolean entered(){
		return entered;
	}
	
	public boolean open_mouth(){
		return open_mouth;
	}
	public boolean got_anesthesia(){
		return got_anesthesia;
	}
	
	public boolean pulled_tooth(){
		return pulled_tooth;
	}	
}
