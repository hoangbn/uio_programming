package Assignment2;

//Monitorcontroller
public class Secretary{
	MonitorPanel mp;
	Dentist[] d;
	Chair[] c;
	int customers_left;
	int chairs_left;
	int dentists_left;
	
	int highest;
	
	Secretary(int customers, int dentists, int chairs){
		
		customers_left = customers;
		chairs_left = chairs;
		dentists_left = dentists;
	}
	
	public synchronized void changeDensLeft(int what){
		if(what == 1){
			dentists_left++;
		} else {
			dentists_left--;
		}
	}
	
	public synchronized void changeChairsLeft(int what){
		if(what == 1){	
			chairs_left++;
		} else {
			chairs_left--;
		}
	}

	public void test(){
		System.out.println("Worked");
	}
	
	/*References classes with each other so it is possible to communicate*/
	public void setMonitor(MonitorPanel m){
		this.mp = m;
	}
	
	public int sendToDentist(Patient pt) throws InterruptedException{
		//while(getD_left() == 0) wait();
		
		Dentist dt;
		for(int i = 0;i < mp.d.length;i++){
			dt = mp.d[i];
			if(!dt.gotPatient()){
				changeDensLeft(0);
				dt.newPatient(pt);	
				return 1;
			}
		}
		return 0;
	}
	
	public int occupyChair(Patient pn){
		Chair ch;
		for(int i = 0;i < mp.c.length;i++){
			ch = mp.c[i];
			if(!ch.occupied){
				ch.occupied = true;
				ch.id = pn;
				pn.satChair(ch.pos_x, ch.pos_y);
				pn.entered = true;
				pn.waiting = true;
				String s = "Chair " + ch.chair_id + " Occupied by Patient " + pn.pat_nmb;
				update(ch.pos_x, ch.pos_y, s);
				changeChairsLeft(0);
				
				return 1;
			}
		}
		//System.out.println("No chairs left");
		return 0;
	}
	
	public synchronized void leftChair(Patient pn){
		Chair ch;
		for(int i = 0;i < mp.c.length;i++){
			ch = mp.c[i];
			if(ch.id == pn){
				ch.free();
				String s = "Chair " + ch.chair_id + " Available";
				update(ch.pos_x, ch.pos_y, s);
				break;
			}
		}
		notifyAll();
	}
	
	public synchronized void getPayment(){
		for(int i = 0;i < mp.p.length;i++){
			if(mp.p[i].pulled_tooth){
				mp.p[i].pay();	
				customers_left--;
			}
		}
	}
	
	public void monitor(int id) throws InterruptedException{	
		Patient pa = null;
		
		//while(customers_left != 0) {

			pa = mp.getPatient(id);
			
			if(pa == null){
				System.out.printf("Patient %d doesn't exist\n", id);
				return;
			} else {
				
				//if(!pa.entered()){
					//System.out.printf("Chair left %d\n", chairs_left);	
					
					while(occupyChair(pa) == 0){	
						synchronized(this){
							wait(); 
						}
					}
					
				//}
				
				//if(pa.waiting()){
					//System.out.printf("dentists %d, chairs %d\n", dentists_left, chairs_left);	
					while(sendToDentist(pa) == 0){
						synchronized(this){
							wait(); 
						}
					} 
						
				//} 
				
				getPayment();
				System.out.printf("Patient %d done treatment!!!\n", id);
				//	update();
			}
		//}
	}
	
	public synchronized void notifySecretary(){
		notifyAll();
	}
	
	public synchronized void update(int i, int j, String s){
		
		//System.out.println(i);
		//System.out.println(j);
		System.out.println(s);
		mp.labels[i][j].setText(s);
		
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		//mp.updateApp(i, j, s);
	}

}

