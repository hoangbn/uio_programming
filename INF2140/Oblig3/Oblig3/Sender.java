
public class Sender implements Runnable {
	UnreliableChannel<Integer> channel_data, channel_ack ;
	int bit = 0;

	public Sender(UnreliableChannel<Integer> ci, UnreliableChannel<Integer> ca){
		this.channel_data = ci;
		this.channel_ack = ca;
	}

	
	/*flipper bitet*/
	public void flipbit(){
		if(bit == 1){
			bit = 0;
		} else {
			bit = 1;
		}
	}

	public void run() {
		try {
			Thread.sleep(100);


			int start = 0;
			int ack = -1;
			while(true) {


				if(channel_data.isEmpty()){
					System.out.printf("sending msg: %d\n", start);
					channel_data.send(start);
				}

				if(ack == start && !channel_ack.isEmpty()){
					ack = channel_ack.receive();

					System.out.printf("ack received: %d\n", ack);
					start = (start + 1)%10;
				}	
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}



