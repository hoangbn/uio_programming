
public class Receiver implements Runnable {
	UnreliableChannel<Integer> channel_data, channel_ack ;
	int bit = 1;
	
	
	public Receiver(UnreliableChannel<Integer> ci, UnreliableChannel<Integer> ca){
		this.channel_data = ci;
		this.channel_ack = ca;
	}

	
	/*flipper bitet*/
	public void flipbit(){
		if(bit == 1){
			bit = 0;
		} else {
			bit = 1;
		}
	}
		
	
	public void run() {
		try {
			Thread.sleep(100);


			int wait = 0;
			int msg = -1;

			while(true) {

				if(!channel_data.isEmpty()){
					System.out.printf("received msg: %d\n", msg);
					msg = channel_data.receive();
				}

				if(msg == wait && channel_ack.isEmpty()){
					channel_ack.send(msg);

					System.out.printf("sending ack: %d\n", wait);
					wait = (wait + 1)%10;
				}	
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}