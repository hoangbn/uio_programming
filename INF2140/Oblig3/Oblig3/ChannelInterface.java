

public interface ChannelInterface<T> {
    public void send(T v) throws InterruptedException;
    public T receive() throws InterruptedException;
}
