public class Assgnment3 {
	public static void main(String [] args){
		Channel<Integer> chan1 = new Channel<Integer>();
		Channel<Integer> chan2 = new Channel<Integer>();
		Channel<Integer> chan3 = new Channel<Integer>();
		Channel<Integer> chan4 = new Channel<Integer>();
		
		UnreliableChannel<Integer> CHANNEL_K = new UnreliableChannel<Integer>(chan1, chan2);
		UnreliableChannel<Integer> CHANNEL_L = new UnreliableChannel<Integer>(chan3, chan4);
		
		Receiver r = new Receiver(CHANNEL_K, CHANNEL_L);
		Sender s = new Sender(CHANNEL_K, CHANNEL_L);
		
		
		Thread thread = new Thread(r);
		Thread thread2 = new Thread(s);
		thread.start();
		thread2.start();
	}
}
