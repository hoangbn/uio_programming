package Assignment2;


public class Dentist {
	int dentist_num, pos_x, pos_y;
	boolean occupied;
	
	Patient id;
    Secretary s;
	
	Dentist(int number){
		dentist_num = number;
		occupied = false;
		id = null;
	}
	
	public boolean gotPatient(){
		return occupied;
	}
	
	public synchronized void newPatient(Patient num) throws InterruptedException{
		id = num;
		occupied = true;
		s.leftChair(num);
		notifyAll();
		beginTreatment();
	}
	
	public void patientDone(){
		id = null;
		occupied = false;
		s.dentists_left++;
	}
	
	public synchronized void beginTreatment() throws InterruptedException{
		while(id == null){
			wait();
		}
		id.open_mouth = true;
		id.got_anesthesia = true;
		id.pulled_tooth = true;
		occupied = false;
		notifyAll();
	}
	
}
