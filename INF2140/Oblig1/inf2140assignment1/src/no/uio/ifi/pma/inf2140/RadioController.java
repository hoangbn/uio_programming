package no.uio.ifi.pma.inf2140;



class RadioController implements IRadioController {
	IRadioDisplay disp;
	IRotator rot;
	boolean ON = false;
	boolean OFF = true;
	boolean locked = false;
	
	
    RadioController(IRadioDisplay display, IRotator rotator) {
    	this.disp = display;
    	this.rot = rotator;
    }

    public void eventLock() {
    	if(ON){
    		if(locked) {
    			rot.startScanning();
    			locked = false;
    		}
    		else {
    			rot.stopScanning();
    			locked = true;
    		}
    	} else {	  		
    		return;
    	}
    }

    public void eventScan() {
    	if(ON){
    		if(!locked) rot.startScanning();
    		return;
    	} else {
    		return;
    	}
    }
    
    public void eventOn() {
    	if(ON) return;
    	else {
    		ON = true;
    		OFF = false;
    		disp.setText("ON");
    	}
    }
    
    public void eventOff() {
    	if(OFF) return;
    	else {
    		ON = false;
    		OFF = true;
    		disp.setText("OFF");
    		rot.stopScanning();
    		disp.setAngle(0);   
    		locked = false;
    	}
    }
    
    public void eventReset() {
    		if(ON){
    			disp.setAngle(0);
    		}
    }   
}


/****************************************************************************/
