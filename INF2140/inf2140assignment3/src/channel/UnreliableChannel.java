package channel;
import java.util.Random;

public class UnreliableChannel<T> implements Runnable, ChannelInterface<T>{
	 final static int maxError = 5; // The channel can not lose or duplicate more than maxError times continuously
	 private Channel<T> syn1, syn2; // Channel syn1 is to receive messages from a sender, channel syn2 is to deliver messages to a receiver
	 protected T msg;
	 protected T tmp;
	 int numberOfRepetitionAndLost = 0;
	 public UnreliableChannel(Channel<T> l, Channel<T> r){
		 syn1 = l;
		 syn2 = r;
	 }
	 public void send(T v){
		 try{
			 syn1.send(v);
		 }catch (InterruptedException e){}
	 }
	 public T receive(){
		 try{
			 tmp = syn2.receive();
		 }catch (InterruptedException e){} 
		 return tmp;
		
	 }
	 public void run() {
		    try {
		    	while (true){
			    	msg = syn1.receive();
			        	// After maxError times losing or duplicating a message, the channel must deliver the message
			        	// This guarantees that a message will eventually be delivered to a receiver
			        	if (numberOfRepetitionAndLost == maxError){ 
			        		syn2.send(msg);
			        		System.out.println("The message " + msg+ " is sent properly");
			        		numberOfRepetitionAndLost = 0; // After sending properly, 
			        								       // variable numberOfRepetitionAndLost needs to be reset for following messages
			        	}else{
			        		Random random = new Random();
			        		int randomInt = random.nextInt(3);	// randomInt gets the values 0, 1 and 2 which 
			        											// correspond to losing, duplicating or properly sending messages	
			        		 // Lose the message, i.e, the message is not delivered		
			        		if (randomInt == 0){	       
			        			System.out.println("The message " + msg+"  is lost");
			        			++ numberOfRepetitionAndLost;
			        		}		 
			        		// Duplicate messages		
			        		else if (randomInt == 1){ 
			        			System.out.println("The message " + msg+" is duplicated");
			        			syn2.send(msg);
			        			syn2.send(msg);
			        			++ numberOfRepetitionAndLost;
			        		}
			        		// Sending messages properly
			        		else if (randomInt == 2){  
			        			syn2.send(msg);
			        			System.out.println("The message " + msg+ " is sent properly");			    	
				        		numberOfRepetitionAndLost = 0; // After sending properly, 
														       // this variable needs to be reset for following messages
			        		}
			          }		
		    	}
		    } catch (InterruptedException e){}
	 }
}
