package Assignment2;


public class Dentist extends Thread {
	int dentist_num, pos_x, pos_y;
	boolean occupied;
	
	Patient id;
    Secretary s;
	
	Dentist(int number){
		dentist_num = number;
		occupied = false;
		id = null;
	}
	
	public void position(int x, int y){
		pos_x = x;
		pos_y = y;
	}
	
	public boolean gotPatient(){
		return occupied;
	}
	
	public synchronized void newPatient(Patient num) throws InterruptedException{
		id = num;
		occupied = true;
		s.leftChair(num);
		String text = "Dentist " + dentist_num + " Occupied by Patient " + num.pat_nmb;
		s.update(pos_x, pos_y, text);
		notifyAll();
		beginTreatment();
	}
	
	public void patientDone(){
		id = null;
		occupied = false;
		String text = "Dentist " + dentist_num + "Not Occupied by Patient";
		s.update(pos_x, pos_y, text);
		s.dentists_left++;
	}
	
	public synchronized void beginTreatment() throws InterruptedException{
		while(id == null){
			wait();
		}
		id.open_mouth = true;
		id.got_anesthesia = true;
		id.pulled_tooth = true;
		occupied = false;
		patientDone();
		notifyAll();
	}
	
}
