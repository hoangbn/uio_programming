package Assignment2;

import java.awt.GridLayout;
import java.awt.Label;
import javax.swing.JPanel;


@SuppressWarnings("serial")
class MonitorPanel extends JPanel {	
	Secretary s;
	
	final static int chairs = 4;
	final static int dentists = 2;
	final static int customers = 10;
	
	int count = 0;
	int dentist_count = 0;
	int chair_count = 0;
	String newLine;
	Label[][] labels;
	
	MonitorPanel () {
		s = new Secretary(customers, dentists, chairs);
	
		newLine = System.getProperty("line.separator");
		setLayout ( new GridLayout (dentists, chairs));
		labels = new Label[dentists][chairs];
		for(int i = 0; i < dentists; i++){
			for(int j = 0; j < chairs; j++){
				if(chairs - 1 == j){
					labels[i][j] = new Label("Dentist " + (i+1) + newLine + " Not Occupied");
					add(labels[i][j]);
					s.d[dentist_count].position(i, j);
					dentist_count++;
				//	add(new Label("Dentist " + (i+1) + newLine + " Not Occupied"));
					break;
				}
				if(count == chairs)	{
					labels[i][j] = new Label(" ");
					add(labels[i][j]);
					//add(new Label(" "));
				}
				else {
					count++;	
					labels[i][j] = new Label("Chair " + count + newLine + " Not Occupied");
					add(labels[i][j]);
					s.c[chair_count].position(i, j);
					chair_count++;
					//add(new Label("Chair " + count + newLine + " Not Occupied"));
				}
			}
		}	
		
	}
	
	public void updateApp(int i, int j, String what){
		labels[i][j].setText(what);
	}
	
	public void start() {
	}
	
	public void stop(){
	}
}