package Assignment2;

//Monitorcontroller
public class Secretary {
	MonitorPanel mp;
	Patient[] p;
	Dentist[] d;
	Chair[] c;
	int customers_left;
	int chairs_left;
	int dentists_left;
	
	int highest;
	Secretary(int customers, int dentists, int chairs){
		customers_left = customers;
		chairs_left = chairs;
		dentists_left = dentists;
		p = new Patient[customers];
		d = new Dentist[dentists];
		c = new Chair[chairs];
		highest = customers;
		if(highest < dentists) highest = dentists;
		if(highest < chairs) highest = chairs;
		for(int i = 0; i < highest; i++){
			if(i < customers){
				p[i] = new Patient(i+1);
			}
			if(i < dentists){
				d[i] = new Dentist(i+1);
			}
			if(i < chairs){
				c[i] = new Chair(i+1);
			}
		}
	}
	
	public synchronized void sendToDentist(Patient pt) throws InterruptedException{
		while(dentists_left == 0) wait();
		
		Dentist dt;
		for(int i = 0;i < d.length;i++){
			dt = d[i];
			if(!dt.gotPatient()){
				dt.newPatient(pt);
				
			}
			dentists_left--;
		}
	}
	
	public synchronized void occupyChair(Patient pn){
		Chair ch;
		for(int i = 0;i < c.length;i++){
			ch = c[i];
			if(!ch.occupied){
				ch.occupied = true;
				ch.id = pn;
				pn.satChair(ch.pos_x, ch.pos_y);
				pn.entered = true;
				pn.waiting = true;
				String s = "Chair " + ch.chair_id + " Occupied by Patient " + pn.pat_nmb;
				update(ch.pos_x, ch.pos_y, s);
				chairs_left--;
				return;
			}
		}
	}
	
	public synchronized void leftChair(Patient pn){
		Chair ch;
		for(int i = 0;i < c.length;i++){
			ch = c[i];
			if(ch.id == pn){
				ch.free();
				String s = "Chair " + ch.chair_id + "Not Occupied";
				update(ch.pos_x, ch.pos_y, s);
				break;
			}
		}
		notifyAll();
	}
	
	public synchronized Patient getPatient(){
		Patient cust = null;
		
		for(int i = 0;i < p.length;i++){
			cust = p[i];
			if(cust.payed);
			else {
				return cust;
			}
		}
		return cust;
		
	}
	
	public synchronized void getPayment(){
		for(int i = 0;i < p.length;i++){
			if(p[i].pulled_tooth){
				p[i].pay();	
				customers_left--;
			}
		}
	}
	
	public boolean monitor(Patient patient) throws InterruptedException{	
		
		if(chairs_left == 0) {
			return false;
		}
		
		Patient pa;
		
		while(customers_left != 0) {
			
			pa = getPatient();
			if(pa == null){
				return false;
			} else {
				if(pa.waiting()){
					while(dentists_left == 0) wait();
					sendToDentist(pa);
					
				} else {
					while(chairs_left == 0) wait();
					occupyChair(pa);
				}
			}
			getPayment();
		//	update();
		}
		return false;
	}
	
	public synchronized void update(int i, int j, String s){
		System.out.println(i);
		System.out.println(j);
		System.out.println(s);
		mp.labels[i][j].setText(s);
		//mp.updateApp(i, j, s);
	}
}

