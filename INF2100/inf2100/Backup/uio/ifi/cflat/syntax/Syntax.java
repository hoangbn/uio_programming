package no.uio.ifi.cflat.syntax;

/*
 * module Syntax
 */

import no.uio.ifi.cflat.cflat.Cflat;
import no.uio.ifi.cflat.code.Code;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.log.Log;
import no.uio.ifi.cflat.scanner.Scanner;
import no.uio.ifi.cflat.scanner.Token;
import static no.uio.ifi.cflat.scanner.Token.*;
import no.uio.ifi.cflat.types.*;

/*
 * Creates a syntax tree by parsing; 
 * prints the parse tree (if requested);
 * checks it;
 * generates executable code. 
 */
public class Syntax {
    static DeclList library;
    static Program program;
    
    /*Had to make some changes since we aren't running through the scanner which means no tokens are read*/
    public static void init() {
	/*We have to atleast run the readNext() 3 times to actually identify a current token*/
	Scanner.readNext();
	Scanner.readNext();
	Scanner.readNext();
    }

    public static void finish() {
	//-- Must be changed in part 1:
    }

    public static void checkProgram() {
	program.check(library);
    }

    public static void genCode() {
	program.genCode(null);
    }

    public static void parseProgram() {
	program = new Program();
	program.parse();
    }

    public static void printProgram() {
	program.printTree();
    }

    static void error(SyntaxUnit use, String message) {
	Error.error(use.lineNum, message);
    }
}


/*
 * Master class for all syntactic units.
 * (This class is not mentioned in the syntax diagrams.)
 */
abstract class SyntaxUnit {
    int lineNum;

    SyntaxUnit() {
	lineNum = Scanner.curLine;
    }

    abstract void check(DeclList curDecls);
    abstract void genCode(FuncDecl curFunc);
    abstract void parse();
    abstract void printTree();
}


/*
 * A <program>
 */
class Program extends SyntaxUnit {
    DeclList progDecls = new GlobalDeclList();
	
    @Override void check(DeclList curDecls) {
	progDecls.check(curDecls);

	if (! Cflat.noLink) {
	    // Check that 'main' has been declared properly:
	    //-- Must be changed in part 2:
	}
    }
		
    @Override void genCode(FuncDecl curFunc) {
	progDecls.genCode(null);
    }

    @Override void parse() {
	Log.enterParser("<program>");

	progDecls.parse();
	if (Scanner.curToken != eofToken) {
	    System.out.println(Scanner.curName);
	    System.out.println(Scanner.nextToken);
	    Error.expected("A declaration");
	}
	Log.leaveParser("</program>");
    }

    @Override void printTree() {
	progDecls.printTree();
    }
}


/*
 * A declaration list.
 * (This class is not mentioned in the syntax diagrams.)
 */

abstract class DeclList extends SyntaxUnit {
    Declaration firstDecl = null;
    DeclList outerScope;

    DeclList () {
	//outerScope = new GlobalDeclList();
    }

    @Override void check(DeclList curDecls) {
	outerScope = curDecls;

	Declaration dx = firstDecl;
	while (dx != null) {
	    dx.check(this);  dx = dx.nextDecl;
	}
    }

    @Override void printTree() {
	Declaration temp = firstDecl;
	while(temp != null){
	    temp.printTree();
	    temp = temp.nextDecl;
	}
    }
    
    /*Compare if the declaration is already declared before if not add d last in list*/
    void addDecl(Declaration d) {
	Declaration temp = firstDecl;
	//if the list is empty
	if(temp == null){
	    firstDecl = d;
	} else {	 
	    while(temp.nextDecl != null){
		/*test if we've already added the declaration before*/
		if(temp.name.compareTo(d.name) == 0) Error.error(d.lineNum, d.name + " the declaration is already in the list");
		temp = temp.nextDecl;
	    }
	    //put d in the last place in list
	    temp.nextDecl = d;
	}
    }

    int dataSize() {
	Declaration dx = firstDecl;
	int res = 0;

	while (dx != null) {
	    res += dx.declSize();  dx = dx.nextDecl;
	}
	return res;
    }

    Declaration findDecl(String name, SyntaxUnit usedIn) {
	//-- Must be changed in part 2:
	return null;
    }
    
       

}


/*
 * A list of global declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 */
class GlobalDeclList extends DeclList {
    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	
	while (Scanner.curToken == intToken || Scanner.curToken == doubleToken) {
	    if (Scanner.nextToken == nameToken) {
		if (Scanner.nextNextToken == leftParToken) {
		    FuncDecl fd = new FuncDecl(Scanner.nextName);
		    fd.parse();
		    addDecl(fd);
		} else if (Scanner.nextNextToken == leftBracketToken) {
		    GlobalArrayDecl gad = new GlobalArrayDecl(Scanner.nextName);
		    gad.parse();
		    addDecl(gad);
		} else {
		    //if it is not a globalarray, then it has to be  varDecl 
		    GlobalSimpleVarDecl gsvd = new GlobalSimpleVarDecl(Scanner.nextName);
		    gsvd.parse();
		    addDecl(gsvd);
		}
	    } else {
		Error.expected("A declaration");
	    }
	}
    }
}



/*
 * A list of local declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 * LocalDeclList has all declarations of ints.
 */
class LocalDeclList extends DeclList {
    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    /*We will continue to parse if the current token is a int token*/
    /*When the declaration is identified as an array or a simple int then*/
    /*make a new class of the corresponding declaration then parse it and add it to a list*/
    @Override void parse() {
	while(Scanner.curToken == intToken || Scanner.curToken == doubleToken){
	    /*Check if it's either an int array or not, taken from GlobalDeclList method*/
	    if(Scanner.nextNextToken == leftBracketToken) {
		LocalArrayDecl lad = new LocalArrayDecl(Scanner.nextName);
		lad.parse();
		addDecl(lad);
	    } else {
		LocalSimpleVarDecl lsvd = new LocalSimpleVarDecl(Scanner.nextName);
		lsvd.parse();
		addDecl(lsvd);
	    }
	}
    }
}


/*
 * A list of parameter declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 */
class ParamDeclList extends DeclList {
    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    /*We have to find how many parameters there are in a method then store it */
    @Override void parse() {
	//int placement = 0;
	/*Continue till we've found all parameters inside (  )*/
	while(Scanner.curToken != rightParToken){	  
	    ParamDecl pd = new ParamDecl(Scanner.nextName);
	    pd.parse();
	    pd.paramNum++;
	    addDecl(pd);
	    if(Scanner.curToken != rightParToken) Scanner.skip(commaToken);
	}
	Scanner.skip(rightParToken);
    }

    @Override void printTree() {
	Declaration temp = firstDecl;
	
	String comma = ", "; 
	while(temp != null){
	    temp.printTree();
	    temp = temp.nextDecl;
	    if(temp != null) Log.wTree(comma);
	}
    }

}


/*
 * Any kind of declaration.
 * (This class is not mentioned in the syntax diagrams.)
 */
abstract class Declaration extends SyntaxUnit {
    String name, assemblerName;
    Type type;
    boolean visible = false;
    Declaration nextDecl = null;

    Declaration(String n) {
	name = n;
    }

    abstract int declSize();

    /**
     * checkWhetherArray: Utility method to check whether this Declaration is
     * really an array. The compiler must check that a name is used properly;
     * for instance, using an array name a in "a()" or in "x=a;" is illegal.
     * This is handled in the following way:
     * <ul>
     * <li> When a name a is found in a setting which implies that should be an
     *      array (i.e., in a construct like "a["), the parser will first 
     *      search for a's declaration d.
     * <li> The parser will call d.checkWhetherArray(this).
     * <li> Every sub-class of Declaration will implement a checkWhetherArray.
     *      If the declaration is indeed an array, checkWhetherArray will do
     *      nothing, but if it is not, the method will give an error message.
     * </ul>
     * Examples
     * <dl>
     *  <dt>GlobalArrayDecl.checkWhetherArray(...)</dt>
     *  <dd>will do nothing, as everything is all right.</dd>
     *  <dt>FuncDecl.checkWhetherArray(...)</dt>
     *  <dd>will give an error message.</dd>
     * </dl>
     */
    abstract void checkWhetherArray(SyntaxUnit use);

    /**
     * checkWhetherFunction: Utility method to check whether this Declaration
     * is really a function.
     * 
     * @param nParamsUsed Number of parameters used in the actual call.
     *                    (The method will give an error message if the
     *                    function was used with too many or too few parameters.)
     * @param use From where is the check performed?
     * @see   checkWhetherArray
     */
    abstract void checkWhetherFunction(int nParamsUsed, SyntaxUnit use);

    /**
     * checkWhetherSimpleVar: Utility method to check whether this
     * Declaration is really a simple variable.
     *
     * @see   checkWhetherArray
     */
    abstract void checkWhetherSimpleVar(SyntaxUnit use);
}


/*
 * A <var decl>
 */
abstract class VarDecl extends Declaration {
    VarDecl(String n) {
	super(n);
    }

    @Override int declSize() {
	return type.size();
    }

    @Override void checkWhetherFunction(int nParamsUsed, SyntaxUnit use) {
	Syntax.error(use, name + " is a variable and no function!");
    }
    
    @Override void printTree() {
	 Log.wTree(type.typeName() + " " + name);
	 Log.wTreeLn(";");
    }

    //-- Must be changed in part 1+2:
}


/*
 * A global array declaration
 */
class GlobalArrayDecl extends VarDecl {
    GlobalArrayDecl(String n) {
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;
    }

    @Override void check(DeclList curDecls) {
	visible = true;
	if (((ArrayType)type).nElems < 0)
	    Syntax.error(this, "Arrays cannot have negative size!");
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	/* OK */
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	Syntax.error(use, name + " is an array and no simple variable!");
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }
    
    //test and check syntax of the program
    @Override void parse() {
	Log.enterParser("<var decl>");
	Type t = Types.getType(Scanner.curToken);
	Scanner.readNext();
	Scanner.skip(nameToken);
	Scanner.skip(leftBracketToken);
	type = new ArrayType(Scanner.curNum, t);
	Scanner.skip(numberToken);
	Scanner.skip(rightBracketToken);
	Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }

    @Override void printTree() {
	String s = "int " + name + "[" + ((ArrayType)type).nElems + "];";
	Log.wTreeLn(s);
    }
}

/*
 * A global simple variable declaration
 */
class GlobalSimpleVarDecl extends VarDecl {
    GlobalSimpleVarDecl(String n) {
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;
    }

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	/* OK */
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    /*a global variabel*/
    @Override void parse() {
	Log.enterParser("<var decl>");
	type = Types.getType(Scanner.curToken);
	Scanner.skip(Scanner.curToken);
	Scanner.skip(nameToken);
	if(Scanner.curToken == semicolonToken) Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }
}


/*
 * A local array declaration
 */
class LocalArrayDecl extends VarDecl {
    LocalArrayDecl(String n) {
	super(n); 
    }

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<var decl>");
	Type t = Types.getType(Scanner.curToken);
	Scanner.readNext();
	Scanner.skip(nameToken);
	Scanner.skip(leftBracketToken);
	type = new ArrayType(Scanner.curNum, t);
	Scanner.skip(numberToken);
	Scanner.skip(rightBracketToken);
	Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }

    @Override void printTree() {
	String s = "int " + name + "[" + ((ArrayType)type).nElems + "];";
	Log.wTreeLn(s);
    }

}


/*
 * A local simple variable declaration
 */
class LocalSimpleVarDecl extends VarDecl {
    LocalSimpleVarDecl(String n) {
	super(n); 
    }

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<var decl>");
	type = Types.getType(Scanner.curToken);
	Scanner.skip(Scanner.curToken);
	Scanner.skip(nameToken);
	if(Scanner.curToken == semicolonToken) Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }
}


/*
 * A <param decl>
 */
class ParamDecl extends VarDecl {
    int paramNum = 0;

    ParamDecl(String n) {
	super(n);
    }

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }
    
    /*Skip parameter declarations and comma tokens, "int random,"*/
    @Override void parse() {
	Log.enterParser("<param decl>");
	
	type = Types.getType(Scanner.curToken);
	Scanner.readNext();
	Scanner.skip(nameToken);
	Log.leaveParser("</param decl>");
    }
    
    @Override void printTree() {
    	Log.wTree(type.typeName() + " " + name);
    }
}


/*
 * A <func decl>
 */
class FuncDecl extends Declaration {
    ParamDeclList param_decl;
    FuncBody body;

    FuncDecl(String n) {
	// Used for user functions:
	
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;

    }

    @Override int declSize() {
	return 0;
    }

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void checkWhetherFunction(int nParamsUsed, SyntaxUnit use) {
	//-- Must be changed in part 2:
    }
	
    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	Code.genInstr("", ".globl", assemblerName, "");
	Code.genInstr(assemblerName, "pushl", "%ebp", "Start function "+name);
	Code.genInstr("", "movl", "%esp,%ebp", "");
	//-- Must be changed in part 2:
    }
    
    /*Skip the datatype, int string etc. and go through various test and store the declarations we're after*/
    /*skip method from Scanner test if the input is the same as current token if it is continue else Error*/
    @Override void parse() {
	/*We just want the parameters inside the method so skip till we find a leftParToken*/
	Log.enterParser("<func decl>");
	Scanner.readNext();
	Scanner.skip(nameToken);
	Scanner.skip(leftParToken);
	/*Now we can begin parsing the parameters*/
	param_decl = new ParamDeclList();
	param_decl.parse();
	Scanner.skip(leftCurlToken);
	body = new FuncBody();
	body.parse();
	Scanner.skip(rightCurlToken);
	Log.leaveParser("</func decl>");
    }

    @Override void printTree() {
	Log.wTree("int " + name + " (");
	param_decl.printTree();
	Log.wTreeLn(")");
	body.printTree();	
    }
}

class FuncBody extends SyntaxUnit {
    LocalDeclList local_decl;
    StatmList statm_list;
    

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

     @Override void genCode(FuncDecl curFunc) {
	// Code.genInstr("", ".globl", assemblerName, "");
	// Code.genInstr(assemblerName, "pushl", "%ebp", "Start function "+name);
	// Code.genInstr("", "movl", "%esp,%ebp", "");
	//-- Must be changed in part 2:
    }
    

    /*Skip the datatype, int string etc. and go through various test and store the declarations we're after*/
    /*skip method from Scanner test if the input is the same as current token if it is continue else Error*/
    @Override void parse() {
	/*We just want the parameters inside the method so skip till we find a leftParToken*/
	Log.enterParser("<func body>");
	local_decl = new LocalDeclList();
	local_decl.parse();
	statm_list = new StatmList();
	statm_list.parse();
	Log.leaveParser("</func body>");
    }

    @Override void printTree() {
	Log.wTreeLn("{");
	Log.indentTree();
	local_decl.printTree();
	statm_list.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
    }
}


/*
 * A <statm list>.
 */
class StatmList extends SyntaxUnit {
    Statement firstStatm = null;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<statm list>");
	Log.enterParser("<statement>");
	Statement lastStatm = null;
	while (Scanner.curToken != rightCurlToken) {
	    Statement s = Statement.makeNewStatement();	  
	    s.parse();

	    if(lastStatm == null) firstStatm = lastStatm = s;
	    else lastStatm.nextStatm = lastStatm = s;	   
	}
	Log.leaveParser("</statement>");
	Log.leaveParser("</statm list>");
    }

    
    @Override void printTree() {
	/*Make enough spaces in the tree*/
	Statement temp = firstStatm;
	while(temp != null){
	    temp.printTree();
	    temp = temp.nextStatm;
	}
    }
}


/*
 * A <statement>.
 */
abstract class Statement extends SyntaxUnit {
    Statement nextStatm = null;

    static Statement makeNewStatement() {
	if (Scanner.curToken==nameToken && 
	    Scanner.nextToken==leftParToken) {
	    return new CallStatm();
	} else if (Scanner.curToken == nameToken) {
	    return new AssignStatm();
	} else if (Scanner.curToken == forToken) {
	    return new ForStatm();
	} else if (Scanner.curToken == ifToken) {
	    return new IfStatm();
	} else if (Scanner.curToken == returnToken) {
	    return new ReturnStatm();
	} else if (Scanner.curToken == whileToken) {
	    return new WhileStatm();
	} else if (Scanner.curToken == semicolonToken) {
	    return new EmptyStatm();
	} else {
	    Error.expected("A statement");
	}
	return null;  // Just to keep the Java compiler happy. :-)
    }
}




/*
 * An <empty statm>.
 */
class EmptyStatm extends Statement {

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	//Log.enterParser("<emtpy-statm>");
	Scanner.skip(semicolonToken);
	//Log.leaveParser("</emtpy-statm>");
    }

    @Override void printTree() {
	Log.wTreeLn(";");

    }
}

/*
 *A <call-statm>
 */
class CallStatm extends Statement {
    //A call statements usually result in call a function, so we need to initiate a FunctionCall class
    FunctionCall func;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<call-statm>");
	func = new FunctionCall();
	func.parse();
	Scanner.skip(semicolonToken);
	Log.leaveParser("</call-statm>");
    }

    @Override void printTree() {
	func.printTree();
	Log.wTreeLn(";");
    }
}


class AssignStatm extends Statement {
    Assignment assign;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<assign-statm>");
	assign = new Assignment();
	assign.parse();
	Scanner.skip(semicolonToken);
	Log.leaveParser("</assign-statm>");
    }

    @Override void printTree() {
	assign.printTree();
	Log.wTreeLn(";");
    }
}

/*
 *A <assign-statm>
 */	
class Assignment extends SyntaxUnit {
    //When we assign something it's usually means we've a variable and an expression
    Variable var; 
    Expression exp;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	var = new Variable();
	var.parse();
	Scanner.skip(assignToken);
	exp = new Expression();
	exp.parse();
    }

    @Override void printTree() {
	var.printTree();
	Log.wTree("=");
	exp.printTree();
    }
}

/*
 * A <for-statm>.
 */
class ForStatm extends Statement {
    ForControl forc;
    StatmList sl;
 
    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<for-statm>");
	Scanner.skip(forToken);
	Scanner.skip(leftParToken);
	forc = new ForControl();
	forc.parse();
	Scanner.skip(rightParToken);
	Scanner.skip(leftCurlToken);
	sl = new StatmList();
	sl.parse();
	Scanner.skip(rightCurlToken);
	Log.leaveParser("</for-statm>");
    }

    @Override void printTree() {
	Log.wTree("for(");
	forc.printTree();
	Log.wTreeLn("){");
	Log.indentTree();
	sl.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
    }
}

class ForControl extends SyntaxUnit {
    Assignment start;
    Expression exp;
    Assignment end;

    @Override void check(DeclList curDecls) {

    }

     @Override void genCode(FuncDecl curFunc) {

    }
  
    @Override void parse() {
	start = new Assignment();
	start.parse();
	Scanner.skip(semicolonToken);
	exp = new Expression();
	exp.parse();
	Scanner.skip(semicolonToken);
	end = new Assignment();
	end.parse();
    }

    @Override void printTree() {
	start.printTree();
	Log.wTree("; ");
	exp.printTree();
	Log.wTree("; ");
	end.printTree();
    }


}


/*
 * An <if-statm>.
 */
class IfStatm extends Statement {
    Expression test;
    StatmList if_body;
    ElseStatm else_body = null;
 

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<if-statm>");
	Scanner.skip(ifToken);
	Scanner.skip(leftParToken);
	test = new Expression();
	test.parse();
	Scanner.skip(rightParToken);
	Scanner.skip(leftCurlToken);
	if_body = new StatmList();
	if_body.parse();
	Scanner.skip(rightCurlToken);
	if(Scanner.curToken == elseToken) {
	    else_body = new ElseStatm();
	    else_body.parse();
	}
	Log.leaveParser("</if-statm>");
    }

    @Override void printTree() {
	Log.wTree("if(");
	test.printTree();
	Log.wTreeLn("){");
	Log.indentTree();
	if_body.printTree();
	Log.outdentTree();
	if(else_body != null){
	    Log.wTree("}");
	    else_body.printTree();
	} else Log.wTreeLn("}");
    }
}

class ElseStatm extends SyntaxUnit {
    StatmList else_body;
     @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	    Scanner.skip(elseToken);
	    Scanner.skip(leftCurlToken);
	    else_body = new StatmList();
	    else_body.parse();
	    Scanner.skip(rightCurlToken);
    }

    @Override void printTree() {
	 Log.wTreeLn(" else {");
	 Log.indentTree();
	 else_body.printTree();
	 Log.outdentTree();
	 Log.wTreeLn("}");
    }


}

/*
 * A <return-statm>.
 */
class ReturnStatm extends Statement {
    Expression test;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	//Log.enterParser("<return-statm>");
	Scanner.skip(returnToken);
	test = new Expression();
	test.parse();
	Scanner.skip(semicolonToken);
	//Log.leaveParser("</return-statm>");
    }

    @Override void printTree() {
	Log.wTree("return ");
	test.printTree();
	Log.wTreeLn(";");
    }
}


/*
 * A <while-statm>.
 */
class WhileStatm extends Statement {
    Expression test;
    StatmList body;

    @Override void check(DeclList curDecls) {
	test.check(curDecls);
	body.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
	String testLabel = Code.getLocalLabel(), 
	       endLabel  = Code.getLocalLabel();

	Code.genInstr(testLabel, "", "", "Start while-statement");
	test.genCode(curFunc);
	test.valType.genJumpIfZero(endLabel);
	body.genCode(curFunc);
	Code.genInstr("", "jmp", testLabel, "");
	Code.genInstr(endLabel, "", "", "End while-statement");
    }

    @Override void parse() {
	Log.enterParser("<while-statm>");
	Scanner.skip(whileToken);
	Scanner.skip(leftParToken);
	test = new Expression();
	test.parse();
	Scanner.skip(rightParToken);
	Scanner.skip(leftCurlToken);
	body = new StatmList();
	body.parse();
	Scanner.skip(rightCurlToken);
	Log.leaveParser("</while-statm>");
    }

    @Override void printTree() {
	Log.wTree("while("); 
	test.printTree();  
	Log.wTreeLn("){");
	Log.indentTree();
	body.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
    }
}

/*
 * An <expression list>.
 */

class ExprList extends SyntaxUnit {
    Expression firstExpr = null;
    Expression temp; 

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Expression lastExpr = null;

	Log.enterParser("<expr list>");
	while(Scanner.curToken != rightParToken){   
	    temp = new Expression();
	    temp.parse();
	    add_to_list(temp);
	    if(Scanner.curToken != rightParToken){              //Since temp has been parsed, we've to check if there is any other expression left
	     	Scanner.skip(commaToken);  
	    }
	}
	Log.leaveParser("</expr list>");
    }

    @Override void printTree() {
	temp = firstExpr;
	while(temp != null){
	    temp.printTree();
	    if(temp.nextExpr != null){
		Log.wTree(", ");
	    }
	    temp = temp.nextExpr;
	}
    }

    void add_to_list(Expression in){
	/*check if list is empty*/
	if(firstExpr == null) firstExpr = in;
	else {
	    temp = firstExpr.nextExpr;
	    while(temp != null) temp = temp.nextExpr;      //loop till we find the end of list
	    temp = in;                                     //add the new expression end of list                     
	}
    }
  
}


/*
 * An <expression>
 */
class Expression extends Operand {
    Expression nextExpr = null;
    Term firstTerm = new Term(), secondTerm = null;
    Operator relOp = null;
    boolean innerExpr = false;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<expression>");
	if(innerExpr) Scanner.skip(leftParToken);
	firstTerm.parse();
	if (Token.isRelOperator(Scanner.curToken)) {
	    relOp = new RelOperator(); 
	    relOp.parse();
	    secondTerm = new Term();
	    secondTerm.parse();
	}
	if(innerExpr) Scanner.skip(rightParToken);
	Log.leaveParser("</expression>");
    }

    @Override void printTree() {
	if(innerExpr) Log.wTree("(");
	firstTerm.printTree();
	if(relOp != null){
	    relOp.printTree();
	    secondTerm.printTree();
	}
	if(innerExpr) Log.wTree(")");
    }
}

/*
 * A <term>
 */
class Term extends SyntaxUnit {
    Factor f, temp;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	temp = f = new Factor();
	while(true){
	    temp.parse();
	    if(Token.isTermOperator(Scanner.curToken)){
		temp.term = new TermOperator();
		temp.term.parse();
		temp = temp.next = new Factor();
	    } else {
		return;
	    }
	}
    }

    @Override void printTree() {
	temp = f;
	while(temp != null){
	    temp.printTree();
	    if(temp.term != null){
		temp.term.printTree();
		temp = temp.next;
	    } else temp = temp.next;
	    
	}
    }
}

class Factor extends SyntaxUnit {
    Operand operand, temp;
    TermOperator term = null;
    Factor next = null;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	temp = operand = Operand.newOperand();
	while(true){	  
	    temp.parse(); 
	    if(Token.isFactorOperator(Scanner.curToken)){
		temp.fonext = new FactorOperator();
		temp.fonext.parse();
		temp = temp.nextOperand = Operand.newOperand();
	    } else {
		break;
	    }
	}	
    }

    @Override void printTree() {
	temp = operand;
	while(temp != null){	  
	    temp.printTree();
        	    
	    if(temp.fonext != null){
		temp.fonext.printTree();
		temp = temp.nextOperand;
	    } else {
		temp = temp.nextOperand;
	    }
	}
    }
}

/*
 * An <operator>
 */
abstract class Operator extends SyntaxUnit {
    Operand nextOp = null;
    Type opType;
    static Token opToken;
    String operator;

    @Override void check(DeclList curDecls) {
	nextOp.check(curDecls);
    }
    
    public static Operator newOperator(){
	if(Token.isTermOperator(opToken)){
	    return new TermOperator();
	} else if(Token.isFactorOperator(opToken)){
	    return new FactorOperator();
	} else {
	    return new RelOperator();
	}
    }

    public boolean nextOperand() {
	return Token.isOperand(Scanner.curToken);
    }
    
}


class TermOperator extends Operator {
    Factor fnext = null;
    Operator next = null;

    @Override void genCode(FuncDecl curFunc) {
	
    }
    
    @Override void parse() {
	//Log.enterParser("<term operator>");
	opToken = Scanner.curToken;
	if(opToken == addToken) {
	    Scanner.skip(addToken);
	    operator= "+";
	} else {
	    Scanner.skip(subtractToken);
	    operator = "-";
	}
	//	Log.leaveParser("</term operator>");
    }
    
    @Override void printTree() {
	Log.wTree("" + operator);
    }
}

class FactorOperator extends Operator {
    Operand next;
    Factor fnext;

    @Override void genCode(FuncDecl curFunc) {
	
    }
    
    @Override void parse() {
	//	Log.enterParser("<factor operator>");
	opToken = Scanner.curToken;
	if(opToken == multiplyToken){
	    Scanner.skip(multiplyToken);
	    operator = "*";
	} else {
	    Scanner.skip(divideToken);
	    operator = "/";
	}
	//	Log.leaveParser("</factor operator>");
    }
    
    @Override void printTree() {
	Log.wTree(operator + "");
    }
}


/*
 * A relational operator (==, !=, <, <=, > or >=).
 */

class RelOperator extends Operator {
    @Override void genCode(FuncDecl curFunc) {
	if (opType == Types.doubleType) {
	    Code.genInstr("", "fldl", "(%esp)", "");
	    Code.genInstr("", "addl", "$8,%esp", "");
	    Code.genInstr("", "fsubp", "", "");
	    Code.genInstr("", "fstps", Code.tmpLabel, "");
	    Code.genInstr("", "cmpl", "$0,"+Code.tmpLabel, "");
	} else {
	    Code.genInstr("", "popl", "%ecx", "");
	    Code.genInstr("", "cmpl", "%eax,%ecx", "");
	}
	Code.genInstr("", "movl", "$0,%eax", "");
	switch (opToken) {
	case equalToken:        
	    Code.genInstr("", "sete", "%al", "Test ==");  break;
	case notEqualToken:
	    Code.genInstr("", "setne", "%al", "Test !=");  break;
	case lessToken:
	    Code.genInstr("", "setl", "%al", "Test <");  break;
	case lessEqualToken:
	    Code.genInstr("", "setle", "%al", "Test <=");  break;
	case greaterToken:
	    Code.genInstr("", "setg", "%al", "Test >");  break;
	case greaterEqualToken:
	    Code.genInstr("", "setge", "%al", "Test >=");  break;
	}
    }

    @Override void parse() {
	Log.enterParser("<rel operator>");
	opToken = Scanner.curToken;
	switch (opToken) {
	case equalToken:        
	    Scanner.skip(equalToken);
	    operator = "==";  break;
	case notEqualToken:    
	    Scanner.skip(notEqualToken);
	    operator = "!=";  break;
	case lessToken:        
	    Scanner.skip(lessToken);
	    operator = "<";   break;
	case lessEqualToken: 
	    Scanner.skip(lessEqualToken);
	    operator = "<=";  break;
	case greaterToken:     
	    Scanner.skip(greaterToken);	    
	    operator = ">";   break;
	case greaterEqualToken:
	    Scanner.skip(greaterEqualToken);
	    operator = ">=";  break;
	}
	Log.leaveParser("</rel operator>");
    }

    @Override void printTree() {
	Log.wTree(" " + operator + " ");
    }
}




/*
 * An <operand>
 */
abstract class Operand extends SyntaxUnit {
    FactorOperator fonext;
    Operand nextOperand = null;
    Type valType;
    
    @Override void check(DeclList curDecls) {
	nextOperand.check(curDecls);
    }
    
    public static Operand newOperand(){
	if(Scanner.curToken == nameToken){
	    if(Scanner.nextToken == leftParToken) return new FunctionCall();
	    else return new Variable();
	} else if(Scanner.curToken == leftParToken){
	    Expression e = new Expression();
	    e.innerExpr = true;
	    return e;
	}
	else return new Number();
    }
}



/*
 * A <function call>.
 */
class FunctionCall extends Operand {
    ExprList parameters;
    String funcName;
    Operator nextOperator = null;

    @Override void check(DeclList curDecls) {
	//-- Must be changed in part 2:
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<function call>");
	funcName = Scanner.curName;
        Scanner.skip(nameToken);
        Scanner.skip(leftParToken);
	parameters = new ExprList();
        parameters.parse();
        Scanner.skip(rightParToken);
        Log.leaveParser("</function call>");
    }

    @Override void printTree() {
	Log.wTree(funcName + "(");
        parameters.printTree();
        Log.wTree(")");
    }
    //-- Must be changed in part 1+2:
}


/*
 * A <number>.
 */
class Number extends Operand {
    int numVal = Scanner.curNum;
    Operator nextOperator = null;

   @Override void check(DeclList curDecls) {
       //-- Must be changed in part 2:
    }
	
    @Override void genCode(FuncDecl curFunc) {
	Code.genInstr("", "movl", "$"+numVal+",%eax", ""+numVal); 
    }

    @Override void parse() {
	Log.enterParser("<number>");
	Scanner.skip(numberToken);
        Log.leaveParser("</number>");
    }

     @Override void printTree() {
	 Log.wTree(Integer.toString(numVal));
    }
}


/*
 * A <variable>.
 */

class Variable extends Operand {
    String varName = Scanner.curName;
    VarDecl declRef = null;
    Expression index = null;
    Operator opr = null;
    
    @Override void check(DeclList curDecls) {
	Declaration d = curDecls.findDecl(varName,this);
	if (index == null) {
	    d.checkWhetherSimpleVar(this);
	    valType = d.type;
	} else {
	    d.checkWhetherArray(this);
	    index.check(curDecls);
	    index.valType.checkType(lineNum, Types.intType, "Array index");
	    valType = ((ArrayType)d.type).elemType;
	}
	declRef = (VarDecl)d;

	if(nextOperand != null) nextOperand.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- Must be changed in part 2:
    }

    @Override void parse() {
	Log.enterParser("<variable>");
	Scanner.skip(nameToken);
        if(Scanner.curToken == leftBracketToken) {
            Scanner.skip(leftBracketToken);
            index = new Expression();
            index.parse();
            Scanner.skip(rightBracketToken);
        }
        Log.leaveParser("</variable>");
    }

    @Override void printTree() {
	Log.wTree(varName);
	if(index != null){
	    Log.wTree("[");
	    index.printTree();
	    Log.wTree("]");
	}
	if(opr != null) opr.printTree();
    }
}
