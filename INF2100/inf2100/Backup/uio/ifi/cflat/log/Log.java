package no.uio.ifi.cflat.log;

/*
 * module Log
 */

import java.io.*;
import no.uio.ifi.cflat.cflat.Cflat;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.scanner.Scanner;
import static no.uio.ifi.cflat.scanner.Token.*;

/*
 * Produce logging information.
 */
public class Log {
    public static boolean doLogBinding = false, doLogParser = false, 
	doLogScanner = false, doLogTree = false;
	
    private static String logName, curTreeLine = "";
    private static int nLogLines = 0, parseLevel = 0, treeLevel = 0;
	
    public static void init() {
	logName = Cflat.sourceBaseName + ".log";
    }
	
    public static void finish() {
	//-- Must be changed in part 0:
    }

    private static void writeLogLine(String data) {
	try {
	    PrintWriter log = (nLogLines==0 ? new PrintWriter(logName) :
		new PrintWriter(new FileOutputStream(logName,true)));
	    log.println(data);  ++nLogLines;
	    log.close();
	} catch (FileNotFoundException e) {
	    Error.error("Cannot open log file " + logName + "!");
	}
    }

    /*
     * Make a note in the log file that an error has occured.
     *
     * @param message  The error message
     */
    public static void noteError(String message) {
	if (nLogLines > 0) 
	    writeLogLine(message);
    }

    /**
     *Method:  enterParse
     *Purpose: Initialize a string and make enough space 
     *         between each line then write it to log
     *         spaces becomes wider for each line
     */
    public static void enterParser(String symbol) {
	int i = 0;
	String parse = "Parser: ";
	if (doLogParser){
	    while(i < parseLevel){
		parse += "   ";
		i++;
	    }
	    parse += symbol;
	    parseLevel++;
	    writeLogLine(parse);
	} 
	return;
    }
    
    /**
     *Method: leaveParse
     *Purpose: Initialize a string and make enough space 
     *         between each line then write it to log
     *         spaces becomes smaller for each line
     */
    public static void leaveParser(String symbol) {
	int i = 0;
	String parse = "Parser: ";
	if (doLogParser){
	    while(i < parseLevel-1){
		parse += "   ";
		i++;
	    }
	    parse += symbol;
	    parseLevel--;
	    writeLogLine(parse);
	}
	return;
    }

    /**
     * Make a note in the log file that another source line has been read.
     * This note is only made if the user has requested it.
     *
     * @param lineNum  The line number
     * @param line     The actual line
     */
    public static void noteSourceLine(int lineNum, String line) {
	if (! doLogParser && ! doLogScanner) return;
	String s = "   " + lineNum + ": " + line;
	writeLogLine(s);
    }

    public static String getName() {
	return Scanner.nextNextName;
    }
    
    public static int getNumber() {
	return Scanner.nextNextNum;
    }
    
    /*
    public static Token getToken() {
	return Scanner.nextNextToken;
    }
    */

    /**
     * Make a note in the log file that another token has been read 
     * by the Scanner module into Scanner.nextNextToken.
     * This note will only be made if the user has requested it.
     */
    public static void noteToken() {
	if (! doLogScanner) return;
	/*Convert the token which resided in nextNextToken to a string*/
	String s = Scanner.nextNextToken.toString();
	/*if the token is a name token then include the name in string s*/
	if(Scanner.nextNextToken == nameToken){
	    s = s + " " + getName();
	    writeLogLine("Scanner :" + s);
       /*if the token is a number token then we include the number in string s*/
	} else
	if(Scanner.nextNextToken == numberToken){
	    s = s + " " + getNumber(); 
	    writeLogLine("Scanner :" + s);
	    /*If it's neither of them then just send the string to writeLogLine*/
	} else {
	    writeLogLine("Scanner :" + s);
	}
	
    }

    public static void noteBinding(String name, int lineNum, int useLineNum) {
	if (! doLogBinding) return;
	//-- Must be changed in part 2:
    }


    public static void wTree(String s) {
	if (curTreeLine.length() == 0) {
	    for (int i = 1;  i <= treeLevel;  ++i) curTreeLine += "  ";
	}
	curTreeLine += s;
    }

    public static void wTreeLn() {
	writeLogLine("Tree:     " + curTreeLine);
	curTreeLine = "";
    }

    public static void wTreeLn(String s) {
	wTree(s);  wTreeLn();
    }
    
    /**
     *Method:  indentTree()
     *Purpose: Increment the value of treeLevel
     */
    public static void indentTree() {
	treeLevel++;
    }
    
    /**
     *Method:  outdentTree()
     *Purpose: Deccrement the value of treeLevel
     */
    public static void outdentTree() {
	treeLevel--;
    }
}
