        .data
.tmp:   .fill   4                       # Temporary storage
        .text
        .globl  main                    
main:   pushl   %ebp                    # Start function main
        movl    %esp,%ebp               
        movl    $120,%eax               # 120
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
.exit$main:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function main
