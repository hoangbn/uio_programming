package no.uio.ifi.cflat.scanner;

/*
 * class Token
 */

/*
 * The different kinds of tokens read by Scanner.
 */
public enum Token { 
    addToken, assignToken, 
	commaToken, 
	divideToken, doubleToken,
	elseToken, eofToken, equalToken, 
	forToken, 
	greaterEqualToken, greaterToken, 
	ifToken, intToken, 
	leftBracketToken, leftCurlToken, leftParToken, lessEqualToken, lessToken, 
	multiplyToken, 
	nameToken, notEqualToken, numberToken, 
	rightBracketToken, rightCurlToken, rightParToken, returnToken, 
	semicolonToken, subtractToken, 
	whileToken;

  
    /*  multiply  divide
     *
     **/
     
    public static boolean isFactorOperator(Token t) {
	if(t == divideToken || t == multiplyToken) 
	    return true;
	else 
	    return false;
    }

    /*+  -
     *
     **/
    public static boolean isTermOperator(Token t) {	
	if(t == addToken || t == subtractToken)
	    return true;
	else 
	    return false;
	
    }
    
    /* == < <= > >= !=
     *
     *
     *
     **/
    public static boolean isRelOperator(Token t) {
	if(t == equalToken || t == notEqualToken || t == lessToken || t == lessEqualToken || t == greaterToken || t == greaterEqualToken)
	    return true;
	else 
	    return false;
 
    }

    public static boolean isOperand(Token t) {
	if(t == numberToken || t == nameToken || t == rightCurlToken || t == leftCurlToken || t == leftParToken || t == rightParToken || t == commaToken)
	    return true;
	else
	    return false;
    }

    public static boolean isTypeName(Token t) {
	if(t == nameToken) return true;
	else return false;
    }
}
