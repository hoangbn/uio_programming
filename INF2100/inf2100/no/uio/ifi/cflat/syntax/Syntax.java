package no.uio.ifi.cflat.syntax;

/*
 * module Syntax
 */

import no.uio.ifi.cflat.cflat.Cflat;
import no.uio.ifi.cflat.code.Code;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.log.Log;
import no.uio.ifi.cflat.scanner.Scanner;
import no.uio.ifi.cflat.scanner.Token;
import static no.uio.ifi.cflat.scanner.Token.*;
import no.uio.ifi.cflat.types.*;

/*
 * Creates a syntax tree by parsing; 
 * prints the parse tree (if requested);
 * checks it;
 * generates executable code. 
 */
public class Syntax {
    static DeclList library;
    static Program program;
    
    /*Had to make some changes since we aren't running through the scanner which means no tokens are read*/
    public static void init() {
	/*We have to atleast run the readNext() 3 times to actually identify a current token*/
	Scanner.readNext();
	Scanner.readNext();
	Scanner.readNext();
	addLibrary();
    }

    public static void finish() {
	//-- Must be changed in part 1:
    }

    public static void checkProgram() {
	program.check(library);
    }

    public static void genCode() {
	program.genCode(null);
    }

    public static void parseProgram() {
	program = new Program();
	program.parse();
    }

    public static void printProgram() {
	program.printTree();
    }
    
    static void error(SyntaxUnit use, String message) {
	Error.error(use.lineNum, message);
    }
    
    static void addLibrary() {	
    	library = new GlobalDeclList();
    	library.addDecl(new LibraryFunctions("exit", 1, 0));
    	library.addDecl(new LibraryFunctions("getchar", 0, 0));
    	library.addDecl(new LibraryFunctions("getdouble", 0, 1));
    	library.addDecl(new LibraryFunctions("getint", 0, 0));
    	library.addDecl(new LibraryFunctions("putchar", 1, 0));	
    	library.addDecl(new LibraryFunctions("putdouble", 1, 1));
    	library.addDecl(new LibraryFunctions("putint", 1, 0));
    }

}

class LibraryFunctions extends FuncDecl {
    /*Funcname is the name of the functions, and params tells us if the functions accept parameteres*/
    LibraryFunctions(String funcName, int params, int whichType){
	super(funcName);
	visible = true;
	if(whichType == 0) type = Types.intType;
	else type = Types.doubleType;
	if(params != 0){
	    /*param_decl is the list which reside in the super class funcdecl*/
	    param_decl = new ParamDeclList();
	    ParamDecl p = new ParamDecl("random");
	    p.type = type;
	    param_decl.addDecl(p);
	}
    }

    
    @Override void genCode(FuncDecl curFunc) {
        super.genCode(curFunc);
    }
}


/*
 * Master class for all syntactic units.
 * (This class is not mentioned in the syntax diagrams.)
 */
abstract class SyntaxUnit {
    int lineNum;

    SyntaxUnit() {
	lineNum = Scanner.curLine;
    }

    abstract void check(DeclList curDecls);
    abstract void genCode(FuncDecl curFunc);
    abstract void parse();
    abstract void printTree();
}


/*
 * A <program>
 */
class Program extends SyntaxUnit {
    DeclList progDecls = new GlobalDeclList();
    Declaration temp;

    @Override void check(DeclList curDecls) {
	progDecls.check(curDecls);

	if (! Cflat.noLink) {
	    // Check that 'main' has been declared properly:	    
	    temp = progDecls.firstDecl;
	    while(temp != null){
		if(temp.name.compareTo("main") == 0) {
		    temp.checkWhetherFunction(0, this);
		    if(temp.type != Types.intType) Error.error(lineNum, "'main' should be an int function!");
		    Log.noteBinding("main", 0, temp.lineNum);
		    return;
		}
		temp = temp.nextDecl;
	    } 
	    Error.error("Name main is unknown");
	}
    }
		
    @Override void genCode(FuncDecl curFunc) {
	progDecls.genCode(null);
    }

    @Override void parse() {
	Log.enterParser("<program>");

	progDecls.parse();
	if (Scanner.curToken != eofToken) {
	    System.out.println(Scanner.curName);
	    System.out.println(Scanner.nextToken);
	    Error.expected("A declaration");
	}
	Log.leaveParser("</program>");
    }

    @Override void printTree() {
	progDecls.printTree();
    }
}

/*
 * A declaration list.
 * (This class is not mentioned in the syntax diagrams.)
 */

abstract class DeclList extends SyntaxUnit {
    Declaration firstDecl = null;
    DeclList outerScope;
    
    DeclList () {
	//outerScope = new GlobalDeclList();
    }

    @Override void check(DeclList curDecls) {
	outerScope = curDecls;

	Declaration dx = firstDecl;
	while (dx != null) {
	    dx.check(this);  dx = dx.nextDecl;
	}
    }

    @Override void printTree() {
	Declaration temp = firstDecl;
	while(temp != null){
	    temp.printTree();
	    temp = temp.nextDecl;
	}
    }
    
    /*Compare if the declaration is already declared before if not add d last in list*/
    void addDecl(Declaration d) {
	Declaration temp = firstDecl;
	//if the list is empty
	if(temp == null){
	    firstDecl = d;
	} else {	 
	    if(temp.name.compareTo(d.name) == 0) Error.error(d.lineNum, "Name " + d.name + " already declared!");
	    while(temp.nextDecl != null){
		/*test if we've already added the declaration before*/
		if(temp.nextDecl.name.compareTo(d.name) == 0) Error.error(d.lineNum, "Name " + d.name + " already declared!");
		temp = temp.nextDecl;
	    }
	    //put d in the last place in list
	    temp.nextDecl = d;
	}
    }
    
    int size(){
	Declaration dx = firstDecl;
	int res = 0;

	while (dx != null) {
	    res++;  dx = dx.nextDecl;
	}
	return res;
    }

    int dataSize() {
	Declaration dx = firstDecl;
	int res = 0;

	while (dx != null) {
	    res += dx.declSize();  dx = dx.nextDecl;
	}
	return res;
    }

    Declaration findDecl(String name, SyntaxUnit usedIn) {
	DeclList temp = this;
	while(temp != null){
	    Declaration temp2 = temp.firstDecl;
	    while(temp2 != null){
		if(temp2.name.compareTo(name) == 0 && temp2.visible){	  
		    if(temp2 instanceof LibraryFunctions) Log.noteBinding(name, 0, usedIn.lineNum);
		    else  Log.noteBinding(name, temp2.lineNum, usedIn.lineNum);
		    return temp2;
		}
		temp2 = temp2.nextDecl;
	    }
	    temp = temp.outerScope;
	}
	
	//if we're here then it means there is something not declared properly.
	Error.error(usedIn.lineNum, "Name " + name + " is unknown!");
	return null;
    }
}


/*
 * A list of global declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 */
class GlobalDeclList extends DeclList {
    @Override void genCode(FuncDecl curFunc) {
	Declaration temp = firstDecl;
	while(temp != null){
	    temp.genCode(null);
	    temp = temp.nextDecl;
	}
    }

    @Override void parse() {
	while (Scanner.curToken == intToken || Scanner.curToken == doubleToken) {
	    if (Scanner.nextToken == nameToken) {
		if (Scanner.nextNextToken == leftParToken) {
		    FuncDecl fd = new FuncDecl(Scanner.nextName);
		    fd.parse();
		    addDecl(fd);
		} else if (Scanner.nextNextToken == leftBracketToken) {
		    GlobalArrayDecl gad = new GlobalArrayDecl(Scanner.nextName);
		    gad.parse();
		    addDecl(gad);
		} else {
		    //if it is not a globalarray, then it has to be  varDecl 
		    GlobalSimpleVarDecl gsvd = new GlobalSimpleVarDecl(Scanner.nextName);
		    gsvd.parse();
		    addDecl(gsvd);
		}
	    } else {
		Error.expected("A declaration");
	    }
	}
    }
}



/*
 * A list of local declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 * LocalDeclList has all declarations of ints.
 */
class LocalDeclList extends DeclList {    

     @Override void genCode(FuncDecl curFunc) {
	int offset = 0;
	Declaration temp = firstDecl;
	while(temp != null){
	     offset += temp.declSize();
	     temp.offset = offset;
	     temp.assemblerName = "-" + offset + "(%ebp)";
	     temp = temp.nextDecl;
	}
	
	if(offset > 0) Code.genInstr("", "subl", "$" + offset + ",%esp", "Get " + offset + " bytes local data space");	
    }

    /*We will continue to parse if the current token is a int token*/
    /*When the declaration is identified as an array or a simple int then*/
    /*make a new class of the corresponding declaration then parse it and add it to a list*/
    @Override void parse() {
	while(Scanner.curToken == intToken || Scanner.curToken == doubleToken){
	    /*Check if it's either an int array or not, taken from GlobalDeclList method*/
	    if(Scanner.nextNextToken == leftBracketToken) {
		LocalArrayDecl lad = new LocalArrayDecl(Scanner.nextName);
		lad.parse();
		addDecl(lad);
	    } else {
		LocalSimpleVarDecl lsvd = new LocalSimpleVarDecl(Scanner.nextName);
		lsvd.parse();
		addDecl(lsvd);
	    }
	}
    }
}


/*
 * A list of parameter declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 */
class ParamDeclList extends DeclList {

    @Override void genCode(FuncDecl curFunc) {
	if(firstDecl == null) return;  /*Nothing to do here if there aren't any parameters*/
	else {
	    Declaration temp = firstDecl;
	    int offset = 8;
	  
	    while(temp != null){
		temp.assemblerName = offset + "(%ebp)";
		offset = offset + temp.type.size();
		temp = temp.nextDecl;
	    }	    
	}
    }

    /*We have to find how many parameters there are in a method then store it */
    @Override void parse() {
	int parameters = 0;
	/*Continue till we've found all parameters inside (  )*/
	while(Scanner.curToken != rightParToken){	  
	    ParamDecl pd = new ParamDecl(Scanner.nextName);
	    pd.parse();
	    pd.paramNum = parameters++;
	    addDecl(pd);
	    if(Scanner.curToken != rightParToken) Scanner.skip(commaToken);
	}
	Scanner.skip(rightParToken);
    }

    @Override void printTree() {
	Declaration temp = firstDecl;
	
	String comma = ", "; 
	while(temp != null){
	    temp.printTree();
	    temp = temp.nextDecl;
	    if(temp != null) Log.wTree(comma);
	}
    }

}


/*
 * Any kind of declaration.
 * (This class is not mentioned in the syntax diagrams.)
 */
abstract class Declaration extends SyntaxUnit {
    String name, assemblerName;
    int offset;
    Type type;
    boolean visible = false;
    Declaration nextDecl = null;
    

    Declaration(String n) {
	name = n;
    }

    abstract int declSize();

    /**
     * checkWhetherArray: Utility method to check whether this Declaration is
     * really an array. The compiler must check that a name is used properly;
     * for instance, using an array name a in "a()" or in "x=a;" is illegal.
     * This is handled in the following way:
     * <ul>
     * <li> When a name a is found in a setting which implies that should be an
     *      array (i.e., in a construct like "a["), the parser will first 
     *      search for a's declaration d.
     * <li> The parser will call d.checkWhetherArray(this).
     * <li> Every sub-class of Declaration will implement a checkWhetherArray.
     *      If the declaration is indeed an array, checkWhetherArray will do
     *      nothing, but if it is not, the method will give an error message.
     * </ul>
     * Examples
     * <dl>
     *  <dt>GlobalArrayDecl.checkWhetherArray(...)</dt>
     *  <dd>will do nothing, as everything is all right.</dd>
     *  <dt>FuncDecl.checkWhetherArray(...)</dt>
     *  <dd>will give an error message.</dd>
     * </dl>
     */
    abstract void checkWhetherArray(SyntaxUnit use);

    /**
     * checkWhetherFunction: Utility method to check whether this Declaration
     * is really a function.
     * 
     * @param nParamsUsed Number of parameters used in the actual call.
     *                    (The method will give an error message if the
     *                    function was used with too many or too few parameters.)
     * @param use From where is the check performed?
     * @see   checkWhetherArray
     */
    abstract void checkWhetherFunction(int nParamsUsed, SyntaxUnit use);

    /**
     * checkWhetherSimpleVar: Utility method to check whether this
     * Declaration is really a simple variable.
     *
     * @see   checkWhetherArray
     */
    abstract void checkWhetherSimpleVar(SyntaxUnit use);
}


/*
 * A <var decl>
 */
abstract class VarDecl extends Declaration {
    VarDecl(String n) {
	super(n);
    }

    @Override int declSize() {
	if(type == null){
	    System.out.println(name);
	    Error.panic("Da Heck");
	}
	return type.size();
    }

    @Override void checkWhetherFunction(int nParamsUsed, SyntaxUnit use) {
	Syntax.error(use, name + " is a variable and no function!");
    }
    
    @Override void printTree() {
	 Log.wTree(type.typeName() + " " + name);
	 Log.wTreeLn(";");
    }
    
    void genStore(Type valType) {
	if(type == Types.doubleType && valType == Types.doubleType){
	    Code.genInstr("", "fstpl", assemblerName, name + " =");
	} else if(type == Types.doubleType && valType == Types.intType){
	    Code.genInstr("", "movl", "%eax," + Code.tmpLabel, "");
	    Code.genInstr("", "fildl", Code.tmpLabel, "  (double)");
	    Code.genInstr("", "fstpl", assemblerName, name + " =");
	} else if(type == Types.intType && valType == Types.doubleType){
	    Code.genInstr("", "fistpl", assemblerName, name + " = (int)");
	} else if(type == Types.intType && valType == Types.intType){
	    Code.genInstr("", "movl", "%eax," + assemblerName, name + " =");
	} else {
	    Error.panic("Declaration.genStore");
	}
    }
    
    void genStoreArray(Type valType) {
	if(((ArrayType)type).elemType == Types.doubleType && valType == Types.doubleType){   
	    Code.genInstr("", "leal", assemblerName + ",%edx", "");
	    Code.genInstr("", "popl", "%ecx", "");
	    Code.genInstr("", "fstpl", "(%edx,%ecx,8)", name + "[...] =");
	} else if(((ArrayType)type).elemType == Types.doubleType && valType == Types.intType){	   
	    Code.genInstr("", "leal", assemblerName + ",%edx", "");
	    Code.genInstr("", "popl", "%ecx", "");
	    Code.genInstr("", "movl", "%eax," + Code.tmpLabel, "");
	    Code.genInstr("", "fildl", Code.tmpLabel, "  (double)");
	    Code.genInstr("", "fstpl", "(%edx,%ecx,8)", name + "[...] =");  	  	    
	} else if(((ArrayType)type).elemType == Types.intType && valType == Types.doubleType){
	    Code.genInstr("", "leal", assemblerName + ",%edx", "");
	    Code.genInstr("", "popl", "%ecx", "");
	    Code.genInstr("", "fistpl", "(%edx,%ecx,4)", name + "[...] =");	  
	} else if(((ArrayType)type).elemType == Types.intType && valType == Types.intType){
	    Code.genInstr("", "leal", assemblerName + ",%edx", "");
	    Code.genInstr("", "popl", "%ecx", "");
	    Code.genInstr("", "movl", "%eax,(%edx,%ecx,4)", name + "[...] =");	    
	} else {
	    Error.panic("Declaration.genStoreArray");
	}
    }
    abstract void genGetVar();   
}


/*
 * A global array declaration
 */
class GlobalArrayDecl extends VarDecl {
    GlobalArrayDecl(String n) {
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;
    }

    @Override void check(DeclList curDecls) {
	visible = true;
	if (((ArrayType)type).nElems < 0)
	    Syntax.error(this, "Arrays cannot have negative size!");
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	/* OK */
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	Syntax.error(use, name + " is an array and no simple variable!");
    }

    @Override void genCode(FuncDecl curFunc) {
	Code.genVar(assemblerName, true, ((ArrayType)type).size(), "int " + name + "[" + ((ArrayType)type).nElems + "];");
    }
    
    //test and check syntax of the program
    @Override void parse() {
	Log.enterParser("<var decl>");
	Type t = Types.getType(Scanner.curToken);
	Scanner.readNext();
	Scanner.skip(nameToken);
	Scanner.skip(leftBracketToken);
	type = new ArrayType(Scanner.curNum, t);
	Scanner.skip(numberToken);
	Scanner.skip(rightBracketToken);
	Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }

    @Override void printTree() {
	String s = "int " + name + "[" + ((ArrayType)type).nElems + "];";
	Log.wTreeLn(s);
    }

    void genGetVar() {
	Code.genInstr("", "leal", assemblerName + ",%edx", name + "[...]");
	if(((ArrayType)type).elemType == Types.intType){	   
	    Code.genInstr("", "movl", "(%edx,%eax,4),%eax", "");
	} else {
	    Code.genInstr("", "fldl", "(%edx,%eax,8)", "");
	}
    }
}

/*
 * A global simple variable declaration
 */
class GlobalSimpleVarDecl extends VarDecl {
    GlobalSimpleVarDecl(String n) {
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;
    }

    @Override void check(DeclList curDecls) {
	visible = true;
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	Syntax.error(use, name + " is a variable and not an array");
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	/* OK */
    }

    @Override void genCode(FuncDecl curFunc) {
	Code.genVar(assemblerName, true, declSize(), type.typeName() + " " + name + ";");     
    }

    /*a global variabel*/
    @Override void parse() {
	Log.enterParser("<var decl>");
	type = Types.getType(Scanner.curToken);
	Scanner.skip(Scanner.curToken);
	Scanner.skip(nameToken);
	if(Scanner.curToken == semicolonToken) Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }
    
     void genGetVar() {
	 if(type == Types.intType){
	     Code.genInstr("", "movl", assemblerName + ",%eax", name);    
	 } else {
	     Code.genInstr("", "fldl", assemblerName, name);
	 }	   
     }
}

class LocalArrayDecl extends VarDecl {
    LocalArrayDecl(String n) {
	super(n); 
    }

    @Override void check(DeclList curDecls) {
	visible = true;
	if(((ArrayType)type).nElems < 0) Syntax.error(this, "Arrays can't have a negative size");
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	/* OK */
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	Syntax.error(use, name + " is an array and no simple variable");
    }

    @Override void genCode(FuncDecl curFunc) {
	//genstorearray and getvar
    }

    @Override void parse() {
	Log.enterParser("<var decl>");
	Type t = Types.getType(Scanner.curToken);
	Scanner.readNext();
	Scanner.skip(nameToken);
	Scanner.skip(leftBracketToken);
	type = new ArrayType(Scanner.curNum, t);
	Scanner.skip(numberToken);
	Scanner.skip(rightBracketToken);
	Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }

    @Override void printTree() {
	String s = "int " + name + "[" + ((ArrayType)type).nElems + "];";
	Log.wTreeLn(s);
    }

    void genGetVar() {	 
	Code.genInstr("", "leal", assemblerName + ",%edx", name + "[...]");
	if(((ArrayType)type).elemType == Types.intType){	   
	    Code.genInstr("", "movl", "(%edx,%eax,4),%eax", "");
	} else {
	    Code.genInstr("", "fldl", "(%edx,%eax,8)", "");
	}
     }
}


/*
 * A local simple variable declaration
 */
class LocalSimpleVarDecl extends VarDecl {
    LocalSimpleVarDecl(String n) {
	super(n); 
    }

    @Override void check(DeclList curDecls) {
	visible = true;
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	Syntax.error(use, name + " is a variable and not an array");
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	/* OK */
    }

    @Override void genCode(FuncDecl curFunc) {
	
	
    }

    @Override void parse() {
	Log.enterParser("<var decl>");
	type = Types.getType(Scanner.curToken);
	Scanner.skip(Scanner.curToken);
	Scanner.skip(nameToken);
	if(Scanner.curToken == semicolonToken) Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }

    void genGetVar() {
	if(type == Types.intType){
	    Code.genInstr("", "movl", assemblerName + ",%eax", name);   	    
	} else {
	    Code.genInstr("", "fldl", assemblerName, name);
	}	   
    }
}



/*
 * A <param decl>
 */
class ParamDecl extends VarDecl {
    int paramNum = 0;

    ParamDecl(String n) {
	super(n);
    }

    @Override void check(DeclList curDecls) {
	visible = true;
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	Syntax.error(use, name + " is a variable and not an array");
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
         /* OK */
    }

    @Override void genCode(FuncDecl curFunc) {
	//no need yet
    }
    
    /*Skip parameter declarations and comma tokens, "int random,"*/
    @Override void parse() {
	Log.enterParser("<param decl>");
	
	type = Types.getType(Scanner.curToken);
	Scanner.readNext();
	Scanner.skip(nameToken);
	Log.leaveParser("</param decl>");
    }
    
    @Override void printTree() {
    	Log.wTree(type.typeName() + " " + name);
    }
    
     @Override void genGetVar() {
	 if(type == Types.intType){
	     Code.genInstr("", "movl", assemblerName + ",%eax", name);    	
	 } else {
	     Code.genInstr("", "fldl", assemblerName, name);
	 }	 
     }
}


/*
 * A <func decl>
 */
class FuncDecl extends Declaration {
    ParamDeclList param_decl;
    FuncBody body;

    FuncDecl(String n) {
	// Used for user functions:
	
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;

    }

    @Override int declSize() {
	return 0;
    }

    @Override void check(DeclList curDecls) {
	visible = true;
	param_decl.check(curDecls);
	body.check(param_decl);
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	Syntax.error(use, name + " is a function and no array!");
    }

    @Override void checkWhetherFunction(int nParamsUsed, SyntaxUnit use) {
	if(param_decl == null && nParamsUsed == 0) return; 
	if(nParamsUsed != param_decl.size()){
	    if(name.compareTo("main") == 0) Error.error(use.lineNum, "Function 'main' should have no parameters!");
	    else Error.error(use.lineNum, "Calls to " + name + " should have " + param_decl.size() + " parameters, not " + nParamsUsed + "!");
	}
    }
	
    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	Syntax.error(use, name + " is a function and not a simple variable");
    }

    @Override void genCode(FuncDecl curFunc) {
	Code.genInstr("", ".globl", assemblerName, "");
	Code.genInstr(assemblerName, "pushl", "%ebp", "Start function "+name);
	Code.genInstr("", "movl", "%esp,%ebp", "");
	param_decl.genCode(curFunc);
	body.genCode(this);
	Code.genInstr(".exit$" + assemblerName, "", "", "");
	Code.genInstr("", "movl", "%ebp,%esp", "");
	Code.genInstr("", "popl", "%ebp", "");
	Code.genInstr("", "ret", "", "End function " + assemblerName);
    }
    
    /*Skip the datatype, int string etc. and go through various test and store the declarations we're after*/
    /*skip method from Scanner test if the input is the same as current token if it is continue else Error*/
    @Override void parse() {
	/*We just want the parameters inside the method so skip till we find a leftParToken*/
	Log.enterParser("<func decl>");
	type = Types.getType(Scanner.curToken);
	Scanner.readNext();
	Scanner.skip(nameToken);
	Scanner.skip(leftParToken);
	/*Now we can begin parsing the parameters*/
	param_decl = new ParamDeclList();
	param_decl.parse();
	Scanner.skip(leftCurlToken);
	body = new FuncBody();
	body.parse();
	Scanner.skip(rightCurlToken);
	Log.leaveParser("</func decl>");
    }

    @Override void printTree() {
	Log.wTree("int " + name + " (");
	param_decl.printTree();
	Log.wTreeLn(")");
	body.printTree();	
    }
}

class FuncBody extends SyntaxUnit {
    LocalDeclList local_decl;
    StatmList statm_list;

    @Override void check(DeclList curDecls) {
	local_decl.check(curDecls);
	statm_list.check(local_decl);
    }

     @Override void genCode(FuncDecl curFunc) {
	 local_decl.genCode(curFunc);
	 statm_list.genCode(curFunc);
    }
    

    /*Skip the datatype, int string etc. and go through various test and store the declarations we're after*/
    /*skip method from Scanner test if the input is the same as current token if it is continue else Error*/
    @Override void parse() {
	/*We just want the parameters inside the method so skip till we find a leftParToken*/
	Log.enterParser("<func body>");
	local_decl = new LocalDeclList();
	local_decl.parse();
	statm_list = new StatmList();
	statm_list.parse();
	Log.leaveParser("</func body>");
    }

    @Override void printTree() {
	Log.wTreeLn("{");
	Log.indentTree();
	local_decl.printTree();
	statm_list.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
    }
}


/*
 * A <statm list>.
 */
class StatmList extends SyntaxUnit {
    Statement firstStatm = null;

    @Override void check(DeclList curDecls) {
	Statement temp = firstStatm;
	while(temp != null){
	    temp.check(curDecls);
	    temp = temp.nextStatm;
	}
    }

    @Override void genCode(FuncDecl curFunc) {
        Statement temp = firstStatm;
        while(temp != null){
	    temp.genCode(curFunc);
	    temp = temp.nextStatm;
	}
    }

    @Override void parse() {
	Log.enterParser("<statm list>");
	Log.enterParser("<statement>");
	Statement lastStatm = null;
	while (Scanner.curToken != rightCurlToken) {
	    Statement s = Statement.makeNewStatement();	  
	    s.parse();

	    if(lastStatm == null) firstStatm = lastStatm = s;
	    else lastStatm.nextStatm = lastStatm = s;	   
	}
	Log.leaveParser("</statement>");
	Log.leaveParser("</statm list>");
    }

    
    @Override void printTree() {
	/*Make enough spaces in the tree*/
	Statement temp = firstStatm;
	while(temp != null){
	    temp.printTree();
	    temp = temp.nextStatm;
	}
    }
}


/*
 * A <statement>.
 */
abstract class Statement extends SyntaxUnit {
    Statement nextStatm = null;

    static Statement makeNewStatement() {
	if (Scanner.curToken==nameToken && 
	    Scanner.nextToken==leftParToken) {
	    return new CallStatm();
	} else if (Scanner.curToken == nameToken) {
	    return new AssignStatm();
	} else if (Scanner.curToken == forToken) {
	    return new ForStatm();
	} else if (Scanner.curToken == ifToken) {
	    return new IfStatm();
	} else if (Scanner.curToken == returnToken) {
	    return new ReturnStatm();
	} else if (Scanner.curToken == whileToken) {
	    return new WhileStatm();
	} else if (Scanner.curToken == semicolonToken) {
	    return new EmptyStatm();
	} else {
	    Error.expected("A statement");
	}
	return null;  // Just to keep the Java compiler happy. :-)
    }
}




/*
 * An <empty statm>.
 */
class EmptyStatm extends Statement {

    @Override void check(DeclList curDecls) {
	//--No need to check an empty statement
    }

    @Override void genCode(FuncDecl curFunc) {
	//-- No need to generate code for empty statement
    }

    @Override void parse() {
	//Log.enterParser("<emtpy-statm>");
	Scanner.skip(semicolonToken);
	//Log.leaveParser("</emtpy-statm>");
    }

    @Override void printTree() {
	Log.wTreeLn(";");

    }
}

/*
 *A <call-statm>
 */
class CallStatm extends Statement {
    //A call statements usually result in call a function, so we need to initiate a FunctionCall class
    FunctionCall func;

    @Override void check(DeclList curDecls) {
	func.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
        func.genCode(curFunc);
    }

    @Override void parse() {
	Log.enterParser("<call-statm>");
	func = new FunctionCall();
	func.parse();
	Scanner.skip(semicolonToken);
	Log.leaveParser("</call-statm>");
    }

    @Override void printTree() {
	func.printTree();
	Log.wTree(";");
    }
}


class AssignStatm extends Statement {
    Assignment assign;

    @Override void check(DeclList curDecls) {
	assign.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
	assign.genCode(curFunc);
    }

    @Override void parse() {
	Log.enterParser("<assign-statm>");
	assign = new Assignment();
	assign.parse();
	Scanner.skip(semicolonToken);
	Log.leaveParser("</assign-statm>");
    }

    @Override void printTree() {
	assign.printTree();
	Log.wTreeLn(";");
    }
}

/*
 *A <assign-statm>
 */	
class Assignment extends SyntaxUnit {
    //When we assign something it's usually means we've a variable and an expression
    Variable var; 
    Expression exp;
    boolean forloop = false;

    @Override void check(DeclList curDecls) {
	var.check(curDecls);
	exp.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
	if(var.index != null){
	    var.index.genCode(curFunc);
	    Code.genInstr("", "pushl", "%eax", "");
	    exp.genCode(curFunc);
	    var.declRef.genStoreArray(exp.valType);
	} else {
	    exp.genCode(curFunc);	 
	    var.declRef.genStore(exp.valType);
	}
    }

    @Override void parse() {
	var = new Variable();
	var.parse();
	Scanner.skip(assignToken);
	exp = new Expression();
	exp.parse();
    }

    @Override void printTree() {
	var.printTree();
	Log.wTree("=");
	exp.printTree();
    }
}

/*
 * A <for-statm>.
 */
class ForStatm extends Statement {
    ForControl forc;
    StatmList sl;
 
    @Override void check(DeclList curDecls) {
	forc.check(curDecls);
	sl.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
      	String startLabel = Code.getLocalLabel();
	String endLabel = Code.getLocalLabel();
	forc.start.genCode(curFunc);
	Code.genInstr(startLabel,"","","Start for-statement");
	forc.exp.genCode(curFunc);
	forc.exp.valType.genJumpIfZero(endLabel);
	sl.genCode(curFunc);
	forc.end.genCode(curFunc);
	Code.genInstr("","jmp",startLabel,"");
	Code.genInstr(endLabel,"","","End for-statement");
    }

    @Override void parse() {
	Log.enterParser("<for-statm>");
	Scanner.skip(forToken);
	Scanner.skip(leftParToken);
	forc = new ForControl();
	forc.parse();
	Scanner.skip(rightParToken);
	Scanner.skip(leftCurlToken);
	sl = new StatmList();
	sl.parse();
	Scanner.skip(rightCurlToken);
	Log.leaveParser("</for-statm>");
    }

    @Override void printTree() {
	Log.wTree("for(");
	forc.printTree();
	Log.wTreeLn("){");
	Log.indentTree();
	sl.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
    }
}

class ForControl extends SyntaxUnit {
    Assignment start;
    Expression exp;
    Assignment end;

    @Override void check(DeclList curDecls) {
	start.check(curDecls);
	exp.check(curDecls);
	end.check(curDecls);
    }
    
    @Override void genCode(FuncDecl func) {
	//nothing to do here
    }
    
    @Override void parse() {
	start = new Assignment();
	start.parse();
	Scanner.skip(semicolonToken);
	exp = new Expression();
	exp.parse();
	Scanner.skip(semicolonToken);
	end = new Assignment();
	end.parse();
    }

    @Override void printTree() {
	start.printTree();
	Log.wTree("; ");
	exp.printTree();
	Log.wTree("; ");
	end.printTree();
    }


}


/*
 * An <if-statm>.
 */
class IfStatm extends Statement {
    Expression test;
    StatmList if_body;
    ElseStatm else_body = null;
 

    @Override void check(DeclList curDecls) {
	test.check(curDecls);
	if_body.check(curDecls);
	if(else_body != null) else_body.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
	if(else_body != null) {
	    String ifLabel = Code.getLocalLabel(), 
		elseLabel  = Code.getLocalLabel();

	    Code.genInstr("", "", "", "Start if-statement");
	    test.genCode(curFunc);	
	    test.valType.genJumpIfZero(elseLabel);
	    if_body.genCode(curFunc);
	    Code.genInstr("", "jmp", ifLabel, "");	
	    Code.genInstr(elseLabel, "", "", "  else-part");
	    else_body.genCode(curFunc);
	    Code.genInstr(ifLabel, "", "", "End if-statement");
	} else {
	    String ifLabel = Code.getLocalLabel();

	    Code.genInstr("", "", "", "Start if-statement");
	    test.genCode(curFunc);
	    test.valType.genJumpIfZero(ifLabel);
	    if_body.genCode(curFunc);
	    //Code.genInstr("", "jmp", ifLabel, "");	
	    Code.genInstr(ifLabel, "", "", "End if-statement");
	}	
    }

    @Override void parse() {
	Log.enterParser("<if-statm>");
	Scanner.skip(ifToken);
	Scanner.skip(leftParToken);
	test = new Expression();
	test.parse();
	Scanner.skip(rightParToken);
	Scanner.skip(leftCurlToken);
	if_body = new StatmList();
	if_body.parse();
	Scanner.skip(rightCurlToken);
	if(Scanner.curToken == elseToken) {
	    else_body = new ElseStatm();
	    else_body.parse();
	}
	Log.leaveParser("</if-statm>");
    }

    @Override void printTree() {
	Log.wTree("if(");
	test.printTree();
	Log.wTreeLn("){");
	Log.indentTree();
	if_body.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
	if(else_body != null) else_body.printTree();
    }
}

class ElseStatm extends SyntaxUnit {
    StatmList else_body;
    
     @Override void check(DeclList curDecls) {
	 else_body.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
        else_body.genCode(curFunc);
    }

    @Override void parse() {
	    Scanner.skip(elseToken);
	    Scanner.skip(leftCurlToken);
	    else_body = new StatmList();
	    else_body.parse();
	    Scanner.skip(rightCurlToken);
    }

    @Override void printTree() {
	 Log.wTree(" else {");
	 Log.indentTree();
	 else_body.printTree();
	 Log.outdentTree();
	 Log.wTreeLn("}");
    }


}

/*
 * A <return-statm>.
 */
class ReturnStatm extends Statement {
    Expression test;
    FuncDecl declRef = null;

    @Override void check(DeclList curDecls) {
        test.check(curDecls);	
    }

    @Override void genCode(FuncDecl curFunc) {
	test.genCode(curFunc);
	Code.genInstr("", "jmp", ".exit$" + curFunc.assemblerName, "Return-statement");
	if(test.valType == Types.intType){}
	else Code.genInstr("", "fldz", "", "");
    }

    @Override void parse() {
	//Log.enterParser("<return-statm>");
	Scanner.skip(returnToken);
	test = new Expression();
	test.parse();
	Scanner.skip(semicolonToken);
	//Log.leaveParser("</return-statm>");
    }

    @Override void printTree() {
	Log.wTree("return ");
	test.printTree();
	Log.wTreeLn(";");
    }
}


/*
 * A <while-statm>.
 */
class WhileStatm extends Statement {
    Expression test;
    StatmList body;

    @Override void check(DeclList curDecls) {
	test.check(curDecls);
	body.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
	String testLabel = Code.getLocalLabel(), 
	       endLabel  = Code.getLocalLabel();

	Code.genInstr(testLabel, "", "", "Start while-statement");
	test.genCode(curFunc);
	test.valType.genJumpIfZero(endLabel);
	body.genCode(curFunc);
	Code.genInstr("", "jmp", testLabel, "");
	Code.genInstr(endLabel, "", "", "End while-statement");
    }

    @Override void parse() {
	Log.enterParser("<while-statm>");
	Scanner.skip(whileToken);
	Scanner.skip(leftParToken);
	test = new Expression();
	test.parse();
	Scanner.skip(rightParToken);
	Scanner.skip(leftCurlToken);
	body = new StatmList();
	body.parse();
	Scanner.skip(rightCurlToken);
	Log.leaveParser("</while-statm>");
    }

    @Override void printTree() {
	Log.wTree("while("); 
	test.printTree();  
	Log.wTreeLn("){");
	Log.indentTree();
	body.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
    }
}

/*
 * An <expression list>.
 */

class ExprList extends SyntaxUnit {
    Expression firstExpr = null;
    Expression temp; 

    @Override void check(DeclList curDecls) {
	Expression current = firstExpr;
	//type = current.valType;
	while(current != null){
	    current.check(curDecls);
	    current = current.nextExpr;
	}
    }

    @Override void genCode(FuncDecl curFunc) {
	if(firstExpr != null) firstExpr.pushParams(1);
    }

    @Override void parse() {
	Expression lastExpr = null;

	Log.enterParser("<expr list>");
	while(Scanner.curToken != rightParToken){   
	    temp = new Expression();
	    temp.parse();
	    add_to_list(temp);
	    if(Scanner.curToken != rightParToken){              //Since temp has been parsed, we've to check if there is any other expression left
	     	Scanner.skip(commaToken);  
	    }
	}
	Log.leaveParser("</expr list>");
    }

    @Override void printTree() {
	temp = firstExpr;
	while(temp != null){
	    temp.printTree();
	    if(temp.nextExpr != null){
		Log.wTree(", ");
	    }
	    temp = temp.nextExpr;
	}
    }

    int size(){
    	int i = 0;
    	temp = firstExpr;
    	while(temp != null){
    	    i++;
    	    temp = temp.nextExpr;
    	}
    	return i;
    }

    void add_to_list(Expression in){
	/*check if list is empty*/
	if(firstExpr == null) firstExpr = in;
	else {
	    temp = firstExpr;
	    while(temp.nextExpr != null) temp = temp.nextExpr;      //loop till we find the end of list
	    temp.nextExpr = in;                                     //add the new expression end of list                     
	}
	//type = firstExpr.valType;
    }
    
    void popParams(){
	if(firstExpr != null) firstExpr.popParams(1);
    }
    
    void removeParams(int nm){	
	Code.genInstr("", "addl", "$" + nm + ",%esp", "Remove parameters");
    }
}


/*
 * An <expression>
 */
class Expression extends Operand {
    Expression nextExpr = null;
    Term firstTerm = new Term(), secondTerm = null;
    Operator relOp = null;
    boolean innerExpr = false;
    int count = 0;
    

    @Override void check(DeclList curDecls) {
	firstTerm.check(curDecls);
	
	if(secondTerm != null){
	    secondTerm.check(curDecls);
	    
	    if(secondTerm.type.typeName().compareTo(firstTerm.type.typeName()) == 0){
		valType = firstTerm.type;
		relOp.opType = firstTerm.type;
		this.valType = Types.intType;
	    } else {	
		//secondTerm.type.checkSameType(secondTerm.lineNum, firstTerm.type, "What"); 
		System.out.println("in expression: firstTerm.type = " + firstTerm.type.typeName());	
	       	System.out.println("in expression: SecondTerm.type = " + secondTerm.type.typeName());
		Error.panic("Wrong Arithmetic type"); 
	    }
	} else {
	    valType = firstTerm.type;
	}
    }
    
    @Override void genCode(FuncDecl curFunc) {
	firstTerm.genCode(curFunc);
	if(relOp != null){
	    if(relOp.opType == Types.intType) Code.genInstr("", "pushl", "%eax", "");
	    else if(relOp.opType == Types.doubleType){
		Code.genInstr("", "subl", "$8,%esp", "");
		Code.genInstr("", "fstpl", "(%esp)", "");
	    } else {
		Error.panic("Expression type not set");
	    }
	    secondTerm.genCode(curFunc);
	    relOp.genCode(curFunc);
	}
    }

    @Override void parse() {
	Log.enterParser("<expression>");
	if(innerExpr) Scanner.skip(leftParToken);
	firstTerm.parse();
	if (Token.isRelOperator(Scanner.curToken)) {
	    relOp = new RelOperator(); 
	    relOp.parse();
	    secondTerm = new Term();
	    secondTerm.parse();
	}
	if(innerExpr) Scanner.skip(rightParToken);
	Log.leaveParser("</expression>");
    }

    @Override void printTree() {
	if(innerExpr) Log.wTree("(");
	firstTerm.printTree();
	if(relOp != null){
	    relOp.printTree();
	    secondTerm.printTree();
	}
	if(innerExpr) Log.wTree(")");
    }
    
    void popParams(int nm){
	Code.genInstr("", "popl", "%ecx", "Pop parameter #" + nm);
	if(nextExpr != null) nextExpr.popParams(nm + 1);
    }

    void pushParams(int nm){
	if(nextExpr != null) nextExpr.pushParams(nm + 1);
	genCode(null);
	if(valType == Types.intType){
	    Code.genInstr("", "pushl", "%eax", "Push parameter #" + nm);
	} else {
	    Code.genInstr("", "subl", "$8,%esp", "");
	    Code.genInstr("", "fstpl", "(%esp)", "Push parameter #" + nm);
	}
    }
}

/*
 * A <term>
 */
class Term extends SyntaxUnit {
    //  TermOperator term = null;
    Factor f, temp;
    Type type;
    boolean called = false;
    
    @Override void check(DeclList curDecls) {
	
	Factor current = f;
	Type prev = null;
	if(current != null){
	    current.check(curDecls);
	    type = current.type;
	    current = current.next;
	}

	while(current != null){
	    current.check(curDecls);
	    prev = current.type;
	    
	    prev.checkSameType(lineNum, type, "Type Error in term"); 
	    current = current.next;
	}
	
	current = f;
	while(current != null){
	    if(current.term != null){
		current.term.opType = type;
	    }
	    current = current.next;
	}
    }

    @Override void genCode(FuncDecl curFunc) {
	Factor current = f;
	while(current != null){
	    if(!called) current.genCode(curFunc);
	    called = true;
	    if(current.next != null) {  
		if(type == Types.doubleType){
		    Code.genInstr("", "subl", "$8,%esp", "");
		    Code.genInstr("", "fstpl", "(%esp)", "");		  
		} else if(type == Types.intType) {
		    Code.genInstr("", "pushl", "%eax", "");
		} else {
		    Error.panic("Type not set yet");
		}
	
		current.next.genCode(curFunc);
		current.term.genCode(curFunc);
		current = current.next;	
        	
	    } else {   
		return;
	    }
	}
    }


    @Override void parse() {
	temp = f = new Factor();
	while(true){
	    temp.parse();
	    if(Token.isTermOperator(Scanner.curToken)){
		temp.term = new TermOperator();
		temp.term.parse();
		temp = temp.next = new Factor();
	    } else {
		return;
	    }
	}
    }

    @Override void printTree() {
	temp = f;
	while(temp != null){
	    temp.printTree();
	    if(temp.term != null){
		temp.term.printTree();
		temp = temp.next;
	    } else temp = temp.next;
	    
	}
    }
}

class Factor extends SyntaxUnit {
    Operand operand, temp;
    //FactorOperator fo = null;;
    TermOperator term = null;
    Factor next = null;
    Type type;
    boolean called = false;

    @Override void check(DeclList curDecls) {
	Operand temp = operand;
	Type prev = null;
	if(temp != null){
	    temp.check(curDecls);
	    type = temp.valType;
	    temp = temp.nextOperand;
	}

	while(temp != null){
	    temp.check(curDecls);
	    prev = temp.valType;
	    
	    prev.checkSameType(lineNum, type, "Operands"); 
	    temp = temp.nextOperand;
	}
	
	temp = operand;
	while(temp != null){
	    if(temp.fonext != null) temp.fonext.opType = type;
	    
	    temp = temp.nextOperand;
	}



	// temp = operand;
	// while(temp != null){	  
	//     if(!called2) temp.check(curDecls);
	//     if(!called2) type = temp.valType;
	//     called2 = true;
	    
	//     if(temp.nextOperand != null){
	// 	temp.nextOperand.check(curDecls);
	// 	temp.fonext.check(curDecls);
	// 	temp.fonext.opType = type;
	// 	temp = temp.nextOperand;
	//     } else return;
	  
	// }
    }

    @Override void genCode(FuncDecl curFunc) {
	temp = operand;
	while(temp != null){
	    if(!called)temp.genCode(curFunc);
	    called = true;
	    if(temp.nextOperand != null) {
		if(type == Types.doubleType){
		    Code.genInstr("", "subl", "$8,%esp", "");
		    Code.genInstr("", "fstpl", "(%esp)", "");		  
		} else if(type == Types.intType) {
		    Code.genInstr("", "pushl", "%eax", "");
		} else {
		    Error.panic("Type not set yet");
		}
	
		temp.nextOperand.genCode(curFunc);
		temp.fonext.genCode(curFunc);
		temp = temp.nextOperand;		
	    } else {	    
		return;
	    }
	}
    }

    @Override void parse() {
	temp = operand = Operand.newOperand();
	while(true){	  
	    temp.parse(); 
	    if(Token.isFactorOperator(Scanner.curToken)){
		temp.fonext = new FactorOperator();
		temp.fonext.parse();
		temp = temp.nextOperand = Operand.newOperand();
	    } else {
		break;
	    }
	}	
    }

    @Override void printTree() {
	temp = operand;
	while(temp != null){	  
	    temp.printTree();
        	    
	    if(temp.fonext != null){
		temp.fonext.printTree();
		temp = temp.nextOperand;
	    } else {
		temp = temp.nextOperand;
	    }
	}
    }
}

/*
 * An <operator>
 */
abstract class Operator extends SyntaxUnit {
    Operand nextOp = null;
    Type opType;
    static Token opToken;
    String operator;
  
    @Override void check(DeclList curDecls) { }
    
    public static Operator newOperator(){
	if(Token.isTermOperator(opToken)){
	    return new TermOperator();
	} else if(Token.isFactorOperator(opToken)){
	    return new FactorOperator();
	} else {
	    return new RelOperator();
	}
    }

    public boolean nextOperand() {
	return Token.isOperand(Scanner.curToken);
    }
    
}


class TermOperator extends Operator {
    Factor fnext = null;
    Operator next = null;
    
    @Override void genCode(FuncDecl curFunc) {
	if(opType == null)Error.panic("TERM");
	if (opType == Types.doubleType) {
	    Code.genInstr("", "fldl", "(%esp)", "");
	    if(operator == "+"){
		Code.genInstr("", "addl", "$8,%esp", "");
		Code.genInstr("", "faddp", "", "Compute +");		
	    } else {
		Code.genInstr("", "addl", "$8,%esp", "");
		Code.genInstr("", "fsubp", "", "Compute -"); 
	    }
	} else {
	    if(operator == "+"){
		Code.genInstr("", "movl", "%eax,%ecx", "");
		Code.genInstr("", "popl", "%eax", "");
		Code.genInstr("", "addl", "%ecx,%eax", "Compute +");
	    } else {
		Code.genInstr("", "movl", "%eax,%ecx", "");
		Code.genInstr("", "popl", "%eax", "");
		Code.genInstr("", "subl", "%ecx,%eax", "Compute -");
	    }
	}
    }
    
    @Override void parse() {
	//Log.enterParser("<term operator>");
	opToken = Scanner.curToken;
	if(opToken == addToken) {
	    Scanner.skip(addToken);
	    operator= "+";
	} else {
	    Scanner.skip(subtractToken);
	    operator = "-";
	}
	//	Log.leaveParser("</term operator>");
    }
    
    @Override void printTree() {
	Log.wTree("" + operator);
    }

}

class FactorOperator extends Operator {
    Operand next;
    Factor fnext;

    @Override void genCode(FuncDecl curFunc) {
	if(opType == null)Error.panic("FACTOR");
	if (opType == Types.doubleType) {
	    Code.genInstr("", "fldl", "(%esp)", "");
	    if(operator == "*"){
		Code.genInstr("", "addl", "$8,%esp", "");
		Code.genInstr("", "fmulp", "", "Compute *");			
	    } else {
		Code.genInstr("", "addl", "$8,%esp", "");
		Code.genInstr("", "fdivp", "", "Compute /");
	    }
	} else {
	    if(operator == "*"){
		Code.genInstr("", "movl", "%eax,%ecx", "");
		Code.genInstr("", "popl", "%eax", "");
		Code.genInstr("", "imull", "%ecx,%eax", "Compute *");
	    } else {
		Code.genInstr("", "movl", "%eax,%ecx", "");
		Code.genInstr("", "popl", "%eax", "");
		Code.genInstr("", "cdq", "", "");
		Code.genInstr("", "idivl", "%ecx", "Compute /");
	    }
	}
	//Code.genInstr("", "popl", "%eax", "");
    }
    
    @Override void parse() {
	//	Log.enterParser("<factor operator>");
	opToken = Scanner.curToken;
	if(opToken == multiplyToken){
	    Scanner.skip(multiplyToken);
	    operator = "*";
	} else {
	    Scanner.skip(divideToken);
	    operator = "/";
	}
	//	Log.leaveParser("</factor operator>");
    }
    
    @Override void printTree() {
	Log.wTree(operator + "");
    }
}


/*
 * A relational operator (==, !=, <, <=, > or >=).
 */

class RelOperator extends Operator {
    @Override void genCode(FuncDecl curFunc) {
	if (opType == Types.doubleType) {
	    Code.genInstr("", "fldl", "(%esp)", "");
	    Code.genInstr("", "addl", "$8,%esp", "");
	    Code.genInstr("", "fsubp", "", "");
	    Code.genInstr("", "fstps", Code.tmpLabel, "");
	    Code.genInstr("", "cmpl", "$0,"+Code.tmpLabel, "");
	} else {
	    Code.genInstr("", "popl", "%ecx", "");
	    Code.genInstr("", "cmpl", "%eax,%ecx", "");
	}
	Code.genInstr("", "movl", "$0,%eax", "");
	/*Dunno why opToken didn't work, didn't set it properly*/
	switch (operator) {
	case "==":        
	    Code.genInstr("", "sete", "%al", "Test ==");  break;
	case "!=":
	    Code.genInstr("", "setne", "%al", "Test !=");  break;
	case "<":
	    Code.genInstr("", "setl", "%al", "Test <");  break;
	case "<=":
	    Code.genInstr("", "setle", "%al", "Test <=");  break;
	case ">":
	    Code.genInstr("", "setg", "%al", "Test >");  break;
	case ">=":
	    Code.genInstr("", "setge", "%al", "Test >=");  break;
	}
    }

    @Override void parse() {
	//opType = Types.intType;
	Log.enterParser("<rel operator>");
	opToken = Scanner.curToken;
	switch (opToken) {
	case equalToken:        
	    Scanner.skip(equalToken);
	    operator = "==";  break;
	case notEqualToken:    
	    Scanner.skip(notEqualToken);
	    operator = "!=";  break;
	case lessToken:        
	    Scanner.skip(lessToken);
	    operator = "<";   break;
	case lessEqualToken: 
	    Scanner.skip(lessEqualToken);
	    operator = "<=";  break;
	case greaterToken:     
	    Scanner.skip(greaterToken);	    
	    operator = ">";   break;
	case greaterEqualToken:
	    Scanner.skip(greaterEqualToken);
	    operator = ">=";  break;
	}
	Log.leaveParser("</rel operator>");
    }

    @Override void printTree() {
	Log.wTree(" " + operator + " ");
    }
}




/*
 * An <operand>
 */
abstract class Operand extends SyntaxUnit {
    FactorOperator fonext;
    Operand nextOperand = null;
    Type valType;
    String name;
    
     @Override void check(DeclList curDecls) {
    // 	// nextOperand.check(curDecls);
     }
    
    public static Operand newOperand(){
	if(Scanner.curToken == nameToken && Scanner.nextToken == leftParToken) return new FunctionCall();
	else if(Scanner.curToken == nameToken) return new Variable();
	else if(Scanner.curToken == leftParToken){
	    Expression e = new Expression();
	    e.innerExpr = true;
	    return e;
	} else if(Scanner.curToken == numberToken) return new Number();
	else {
	    Error.panic(Scanner.curName);
	    return null;
	}
	
    }
    
    
	
    public boolean nextOperator() {
	return Token.isTermOperator(Scanner.curToken) ||  Token.isFactorOperator(Scanner.curToken) ||  Token.isRelOperator(Scanner.curToken);
    }
    
    public boolean arithmeticOperator(){
	return Token.isTermOperator(Scanner.curToken) ||  Token.isFactorOperator(Scanner.curToken);
    }

}



/*
 * A <function call>.
 */
class FunctionCall extends Operand {
    ExprList parameters;
    String funcName;
    Operator nextOperator = null;
    FuncDecl declRef = null;

    @Override void check(DeclList curDecls) {
	parameters.check(curDecls);
	Declaration dec = curDecls.findDecl(funcName, this);
	dec.checkWhetherFunction(parameters.size(), this);	
	valType = dec.type;
	declRef = (FuncDecl)dec;
	dec.type.checkType(lineNum, declRef.type, "Return value");
    }

    @Override void genCode(FuncDecl curFunc) {
        parameters.genCode(curFunc);
        Code.genInstr("", "call", funcName, "Call " + funcName);

	if(declRef.param_decl != null && declRef.param_decl.dataSize() > 0) parameters.removeParams(declRef.param_decl.dataSize());	
	if(declRef.name.compareTo("putdouble") == 0) Code.genInstr("", "fstps", Code.tmpLabel, "Remove return value");
    }

    @Override void parse() {
	Log.enterParser("<function call>");
	funcName = Scanner.curName;
        Scanner.skip(nameToken);
        Scanner.skip(leftParToken);
	parameters = new ExprList();
        parameters.parse();
        Scanner.skip(rightParToken);
        Log.leaveParser("</function call>");
    }

    @Override void printTree() {
	Log.wTree(funcName + "(");
        parameters.printTree();
        Log.wTree(")");
	//if(nextOperator != null) nextOperator.printTree();
    }
}


/*
 * A <number>.
 */
class Number extends Operand {
    int numVal = Scanner.curNum;
    //Operator nextOperator = null;

   @Override void check(DeclList curDecls) {
       valType = Types.intType;
       name = "number";
       // if(nextOperand != null) nextOperand.check(curDecls);
    }
	
    @Override void genCode(FuncDecl curFunc) {
	Code.genInstr("", "movl", "$"+numVal+",%eax", ""+numVal); 
	// if(nextOperand != null) nextOperand.genCode(curFunc);
    }

    @Override void parse() {
	Log.enterParser("<number>");
        if(Scanner.curToken == commaToken) Scanner.skip(commaToken);
        if(Scanner.curToken == numberToken) Scanner.skip(numberToken);
	// if(Scanner.curToken == nameToken){
	//     nextOperand = newOperand();
	//     nextOperand.parse();
	// }
        Log.leaveParser("</number>");
    }

     @Override void printTree() {
	 Log.wTree(Integer.toString(numVal));
	 // if(nextOperand != null) nextOperand.printTree();
    }
}


/*
 * A <variable>.
 */
class Variable extends Operand {
    String varName = Scanner.curName;
    VarDecl declRef = null;
    Expression index = null;
    Operator opr = null;

    @Override void check(DeclList curDecls) {
	Declaration d = curDecls.findDecl(varName,this);
	name = varName;
	if (index == null) {
	    d.checkWhetherSimpleVar(this);
	    valType = d.type;
	} else {
	    d.checkWhetherArray(this);
	    index.check(curDecls);
	    index.valType.checkType(lineNum, Types.intType, "Array index");
	    valType = ((ArrayType)d.type).elemType;
	}
	declRef = (VarDecl)d;

	//	if(nextOperand != null) nextOperand.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
        if(declRef == null) {
	    
	   //Error.panic("VARIABLE");
	   
	    System.out.print(varName);
	} else {
	    if(index != null){
		index.genCode(curFunc);
		declRef.genGetVar();
	    } else {	  
		declRef.genGetVar();	    
	    }
	}
    }

    @Override void parse() {
	Log.enterParser("<variable>");
	
	Scanner.skip(nameToken);
        if(Scanner.curToken == leftBracketToken) {
            Scanner.skip(leftBracketToken);
            index = new Expression();
            index.parse();
            Scanner.skip(rightBracketToken);
        }
        Log.leaveParser("</variable>");
    }

    @Override void printTree() {
	Log.wTree(varName);
	if(index != null){
	    Log.wTree("[");
	    index.printTree();
	    Log.wTree("]");
	}
	if(opr != null) opr.printTree();
    }
}
