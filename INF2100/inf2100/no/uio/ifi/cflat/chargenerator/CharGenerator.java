package no.uio.ifi.cflat.chargenerator;

/*
 * module CharGenerator
 */

import java.io.*;
import no.uio.ifi.cflat.cflat.Cflat;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.log.Log;

/*
 * Module for reading single characters.
 */
public class CharGenerator {
    public static char curC, nextC, character;
	
    private static LineNumberReader sourceFile = null;
    private static String sourceLine;
    private static int sourcePos;
    private static boolean EOF = false, EOL = false, comment = false;
    
    public static void init() {
	try {
	    sourceFile = new LineNumberReader(new FileReader(Cflat.sourceName));
	} catch (FileNotFoundException e) {
	    Error.error("Cannot read " + Cflat.sourceName + "!");
	}
	sourceLine = "";  sourcePos = 0;  curC = nextC = ' ';
	readNext();  readNext();
    }
	
    public static void finish() {
	if (sourceFile != null) {
	    try {
		sourceFile.close();
	    } catch (IOException e) {
		Error.error("Could not close source file!");
	    }
	}
    }
	
    
    /*Method: isMoreToRead()
     *returns true if if there aren't anything more to read 
     *else returns false
     **/
    public static boolean isMoreToRead() {
	if(EOF == false) return true;
	else return false;
    }
    
    /*
    *returns the current line number
    **/
    public static int curLineNum() {
	return (sourceFile == null ? 0 : sourceFile.getLineNumber());
    }
    
    public static void comment(){
	comment = true;
    }

    /*
     *read the next char
     *
    **/
    public static void readNext() {
	curC = nextC;
	
	/*check if there is more to read*/
	if (! isMoreToRead()) return;

	/*test if a line has been read, and if it is check if we're done reading that line*/
	if(sourceLine != null && sourceLine.length() == sourcePos){
	    EOL = true;
	    readNextLine();
	    readNext();
	} else {
	    /*If by some reason this is null then we've done something very wrong somewhere*/
	    if(sourceLine == null){
		Error.error("Either we're done reading or something seriously went wrong during readNext() in CharGenerator");
	    }
	    character = sourceLine.charAt(sourcePos++);
      
	    /*test if the character is a #, if it is then we assume that the rest of that line is just comments*/
	    if(character == '#'){
		comment();
		readNextLine();
		readNext();
	    } 
	    nextC = character;
	}
    }
    

    /*read the next line*/
    public static void readNextLine() {
	try {		   
	    /*A line have to be saved in sourceLine before we can write to log*/
	    if(curLineNum() > 0){
		/*We're write info to log only when a line is done reading, so # will be ignored*/
		if(EOL || comment){
		    Log.noteSourceLine(curLineNum(), sourceLine);
		    EOL = false;
		    comment = false;
		}
	    }
	    
	    /*update & reset variables*/
	    sourceLine = sourceFile.readLine();
	    sourcePos = 0;
	    
	    /*if sourceLine is null, then it means we've done reading the FILE*/
	    if(sourceLine == null){
		EOF = true;
	    }
	} catch(IOException e){
	    Error.error("Something went wrong when using readNextLine() in CharGenerator");
	}
    }
}
