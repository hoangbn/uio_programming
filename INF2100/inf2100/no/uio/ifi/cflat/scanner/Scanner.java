package no.uio.ifi.cflat.scanner;

/*
 * module Scanner
 */

import no.uio.ifi.cflat.chargenerator.CharGenerator;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.log.Log;
import static no.uio.ifi.cflat.scanner.Token.*;

/*
 * Module for forming characters into tokens.
 */
public class Scanner {
    public static Token curToken, nextToken, nextNextToken;
    public static String curName, nextName, nextNextName, add = "'";
    public static int curNum, nextNum, nextNextNum;
    public static int curLine, nextLine, nextNextLine, last = 0;;
    private static char current, next; 
    private static boolean negative = false;    
    

    public static void init() {
	//-- Must be changed in part 0:
    }
	
    public static void finish() {
	//-- Must be changed in part 0:
    }
	
    /*
      isDigit(c) og isLetter kan bruker for å minske ned kodelinjer


     */

    public static void readNext() {
	curToken = nextToken;  nextToken = nextNextToken;
	curName = nextName;  nextName = nextNextName;
	curNum = nextNum;  nextNum = nextNextNum;
	curLine = nextLine;  nextLine = nextNextLine;


	nextNextToken = null;
	while (nextNextToken == null) {
	    nextNextLine = CharGenerator.curLineNum();
	    current = getCc();
	    next = getNc();
	    if (! CharGenerator.isMoreToRead()) last++;
	    /*Have to run one extra time to print to log the last token before eofToken, else the eofToken will overwrite the last rightBrackettoken */
	    if(last < 2){	    	
		/*check if current line is a comment line*/
		if(current == '/' && next == '*') skipLine();
		else if(current == add.charAt(0)) addNumber();
		else if(Character.isSpace(getCc())) update();	
		else if(whatToken(current)) update();
		else if(whatTokens(current)) update();
		else if(isNumber(current)) whatNumber();
		else if(isLetterAZ(current)) whatWord(); 
		/*If run properly we won't be coming here*/
		else update(); 
		
	    } else nextNextToken = eofToken;	       		
	}
	Log.noteToken();
    }

    /*We can assume that we usually comment a whole line*/
    private static void skipLine(){	
	while(!(getCc() == '*' && getNc() == '/')){
	    if(!CharGenerator.isMoreToRead()) Error.error(nextNextLine, "EOF reached before finding the end of comment");
	    update();
	}
	/*Skip the * and * chars*/
	update(); update();
    }
    
    private static void addNumber() {
	/*Since we know current char is ' we want to get the char thats in the middle of ' '*/
	update();
	nextNextToken = numberToken;
	nextNextNum = (int)(getCc());
	/*we want to skip the last ' too*/
	update();
	update();
    }

    private static char getCc(){
	return CharGenerator.curC;
    }

    private static char getNc(){
	return CharGenerator.nextC;
    }
    
    /* Testing both small and big characters
     *test if a character is a letter
     */
    private static boolean isLetterAZ(char c) {	
	if(c >= 'a' || c <= 'z') {
	    return true;
	} else if(c >= 'A' || c <= 'Z') {
	    return true;
	} else if(c == ' ') {
	    return true;
	} else {
	    return false;
	}
    }

    private static boolean isNumber(char c){
	/*Checks if c is a -, if it is then set negative to true*/
	if(c == '-') {
	    negative = true;
	    update();
	    c = getCc();
	}
	
	if(c >= '0' && c <= '9') 
	    return true;
	else
	    return false;
    }
    
    /*check for two symbols, including the update in this method we're doing two updates if it's a token with 2 symbols, so we don't retest chars*/
    private static boolean whatTokens(char c){
	if(c == '!' && getNc() == '='){
	    nextNextToken = notEqualToken;
	    update();
	    return true;
	} else if(c == '<'){
	    if(getNc() == '='){
		nextNextToken = lessEqualToken;
		update();
	    }
	    else nextNextToken = lessToken;	  
	    return true;
	} else if(c == '='){
	    if(getNc() == '=') nextNextToken = equalToken;
	    else nextNextToken = assignToken;
	    update();
	    return true;
	} else if(c == '>'){
	    if(getNc() == '='){
		nextNextToken = greaterEqualToken;
		update();
	    }  else nextNextToken = greaterToken;	  
	    return true;
	} else return false;
    }

    /*identify which symbol it is*/
    private static boolean whatToken(char c){	
	if(c == '(') {
	    nextNextToken = leftParToken;
	    return true;
	} else if(c == ')') {
	    nextNextToken = rightParToken;
	    return true;
	} else if(c == '{'){
	    nextNextToken = leftCurlToken;
	    return true;
	} else if(c == '}'){
	    nextNextToken = rightCurlToken;
	    return true;
	} else if(c == ';'){
	    nextNextToken = semicolonToken;
	    return true;
	} else if(c == ','){
	    nextNextToken = commaToken;
	    return true;
	} else if(c == '-'){
	    nextNextToken = subtractToken;
	    if(isNumber(getNc())){
		return false;
	    }
	    return true;
	} else if(c == '+'){
	    nextNextToken = addToken;
	    return true;
	} else if(c == '*'){
	    nextNextToken = multiplyToken;
	    return true;
	} else if(c == '/'){
	    nextNextToken = divideToken;
	    return true;
	} else if(c == '['){
	    nextNextToken = leftBracketToken;
	    return true;
	} else if(c == ']'){
	    nextNextToken = rightBracketToken;
	    return true;
	} else return false;
    }

    private static void whatWord() {
	String word = "";
	/*Should be able to atleast run one time, since we've already tested before that this char is a letter*/
	/*Added another statement, since a name in a software could include _*/
	while(Character.isLetter(getCc()) || Character.isDigit(getCc()) || getCc() == '_'){
	    word += Character.toString(getCc());
	    update();
	}

	if(!isLetterAZ(getCc()) || !isLetterAZ(getNc())){	    
	    String error = "Illegal Symbol: ";
	    if(!isLetterAZ(getCc())) error += Character.toString(getCc());
	    else error += Character.toString(getNc());
	    Error.error(nextNextLine, error);
	}

	      
	/*test on empty, int, for, if, else, return and while strings*/
	if(word.equals("")) return;
	else if(word.equals("int"))     nextNextToken = intToken;
	else if(word.equals("double"))  nextNextToken = doubleToken;
	else if(word.equals("for"))     nextNextToken = forToken;
	else if(word.equals("if"))      nextNextToken = ifToken;
	else if(word.equals("else"))    nextNextToken = elseToken;
	else if(word.equals("return"))  nextNextToken = returnToken;
	else if(word.equals("while"))   nextNextToken = whileToken;
	/*if all the test were negative, then it must be some function call name*/
	else {
	    nextNextToken = nameToken;
	    nextNextName = word;
	}

    }
    
    private static void whatNumber(){
	/*Initialize the string*/
	String number = "";
	
	while(Character.isDigit(getCc())){
	    number = number + Character.toString(getCc());
	    update();
	}

	nextNextToken = numberToken;
	nextNextNum = Integer.parseInt(number);

	/*If it's a negative number then multiply it with -1 to make it into a negative*/
	if(negative){
	    nextNextNum *= -1;
	}
	/*reset it*/
	negative = false;
    }

    public static void update() {
	CharGenerator.readNext();
    }

    public static void check(Token t) {
	if (curToken != t)
	    Error.expected("A " + t);
    }
	
    public static void check(Token t1, Token t2) {
	if (curToken != t1 && curToken != t2)
	    Error.expected("A " + t1 + " or a " + t2);
    }
	
    public static void skip(Token t) {
	check(t);  readNext();
    }

    public static void skip(Token t1, Token t2) {
	check(t1,t2);  readNext();
    }
}
