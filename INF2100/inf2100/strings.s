        .data
.tmp:   .fill   4                       # Temporary storage
        .globl  s
s:      .fill   804                     # int s[201];
        .globl  true
true:   .fill   4                       # int true;
        .globl  false
false:  .fill   4                       # int false;
        .globl  LF
LF:     .fill   4                       # int LF;
        .text
        .globl  getstring               
getstring:
        pushl   %ebp                    # Start function getstring
        movl    %esp,%ebp               
        subl    $8,%esp                 # Get 8 bytes local data space
        movl    $0,%eax                 # 0
        movl    %eax,-4(%ebp)           # i =
.L0001:                                 # Start while-statement
        movl    true,%eax               # true
        cmpl    $0,%eax                 
        je      .L0002                  
        call    getchar                 # Call getchar
        movl    %eax,-8(%ebp)           # c =
                                        # Start if-statement
        movl    -8(%ebp),%eax           # c
        pushl   %eax                    
        movl    LF,%eax                 # LF
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        sete    %al                     # Test ==
        cmpl    $0,%eax                 
        je      .L0003                  
        movl    -4(%ebp),%eax           # i
        pushl   %eax                    
        movl    $0,%eax                 # 0
        leal    s,%edx                  
        popl    %ecx                    
        movl    %eax,(%edx,%ecx,4)      # s[...] =
        movl    $0,%eax                 # 0
        jmp     .exit$getstring         # Return-statement
.L0003:                                 # End if-statement
        movl    -4(%ebp),%eax           # i
        pushl   %eax                    
        movl    -8(%ebp),%eax           # c
        leal    s,%edx                  
        popl    %ecx                    
        movl    %eax,(%edx,%ecx,4)      # s[...] =
        movl    -4(%ebp),%eax           # i
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,-4(%ebp)           # i =
        jmp     .L0001                  
.L0002:                                 # End while-statement
.exit$getstring:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function getstring
        .globl  p1                      
p1:     pushl   %ebp                    # Start function p1
        movl    %esp,%ebp               
        movl    8(%ebp),%eax            # c1
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
.exit$p1:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function p1
        .globl  p2                      
p2:     pushl   %ebp                    # Start function p2
        movl    %esp,%ebp               
        movl    8(%ebp),%eax            # c1
        pushl   %eax                    # Push parameter #1
        call    p1                      # Call p1
        addl    $4,%esp                 # Remove parameters
        movl    12(%ebp),%eax           # c2
        pushl   %eax                    # Push parameter #1
        call    p1                      # Call p1
        addl    $4,%esp                 # Remove parameters
.exit$p2:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function p2
        .globl  p3                      
p3:     pushl   %ebp                    # Start function p3
        movl    %esp,%ebp               
        movl    12(%ebp),%eax           # c2
        pushl   %eax                    # Push parameter #2
        movl    8(%ebp),%eax            # c1
        pushl   %eax                    # Push parameter #1
        call    p2                      # Call p2
        addl    $8,%esp                 # Remove parameters
        movl    16(%ebp),%eax           # c3
        pushl   %eax                    # Push parameter #1
        call    p1                      # Call p1
        addl    $4,%esp                 # Remove parameters
.exit$p3:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function p3
        .globl  p4                      
p4:     pushl   %ebp                    # Start function p4
        movl    %esp,%ebp               
        movl    16(%ebp),%eax           # c3
        pushl   %eax                    # Push parameter #3
        movl    12(%ebp),%eax           # c2
        pushl   %eax                    # Push parameter #2
        movl    8(%ebp),%eax            # c1
        pushl   %eax                    # Push parameter #1
        call    p3                      # Call p3
        addl    $12,%esp                # Remove parameters
        movl    20(%ebp),%eax           # c4
        pushl   %eax                    # Push parameter #1
        call    p1                      # Call p1
        addl    $4,%esp                 # Remove parameters
.exit$p4:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function p4
        .globl  p12                     
p12:    pushl   %ebp                    # Start function p12
        movl    %esp,%ebp               
        movl    20(%ebp),%eax           # c4
        pushl   %eax                    # Push parameter #4
        movl    16(%ebp),%eax           # c3
        pushl   %eax                    # Push parameter #3
        movl    12(%ebp),%eax           # c2
        pushl   %eax                    # Push parameter #2
        movl    8(%ebp),%eax            # c1
        pushl   %eax                    # Push parameter #1
        call    p4                      # Call p4
        addl    $16,%esp                # Remove parameters
        movl    36(%ebp),%eax           # c8
        pushl   %eax                    # Push parameter #4
        movl    32(%ebp),%eax           # c7
        pushl   %eax                    # Push parameter #3
        movl    28(%ebp),%eax           # c6
        pushl   %eax                    # Push parameter #2
        movl    24(%ebp),%eax           # c5
        pushl   %eax                    # Push parameter #1
        call    p4                      # Call p4
        addl    $16,%esp                # Remove parameters
        movl    52(%ebp),%eax           # c12
        pushl   %eax                    # Push parameter #4
        movl    48(%ebp),%eax           # c11
        pushl   %eax                    # Push parameter #3
        movl    44(%ebp),%eax           # c10
        pushl   %eax                    # Push parameter #2
        movl    40(%ebp),%eax           # c9
        pushl   %eax                    # Push parameter #1
        call    p4                      # Call p4
        addl    $16,%esp                # Remove parameters
.exit$p12:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function p12
        .globl  putstring               
putstring:
        pushl   %ebp                    # Start function putstring
        movl    %esp,%ebp               
        subl    $8,%esp                 # Get 8 bytes local data space
        movl    $0,%eax                 # 0
        movl    %eax,-4(%ebp)           # i =
.L0004:                                 # Start while-statement
        movl    -4(%ebp),%eax           # i
        leal    s,%edx                  # s[...]
        movl    (%edx,%eax,4),%eax      
        cmpl    $0,%eax                 
        je      .L0005                  
        movl    -4(%ebp),%eax           # i
        leal    s,%edx                  # s[...]
        movl    (%edx,%eax,4),%eax      
        movl    %eax,-8(%ebp)           # c =
        movl    -4(%ebp),%eax           # i
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,-4(%ebp)           # i =
        movl    -8(%ebp),%eax           # c
        pushl   %eax                    # Push parameter #1
        call    p1                      # Call p1
        addl    $4,%esp                 # Remove parameters
        jmp     .L0004                  
.L0005:                                 # End while-statement
.exit$putstring:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function putstring
        .globl  strlen                  
strlen: pushl   %ebp                    # Start function strlen
        movl    %esp,%ebp               
        subl    $4,%esp                 # Get 4 bytes local data space
        movl    $0,%eax                 # 0
        movl    %eax,-4(%ebp)           # i =
.L0006:                                 # Start while-statement
        movl    -4(%ebp),%eax           # i
        leal    s,%edx                  # s[...]
        movl    (%edx,%eax,4),%eax      
        cmpl    $0,%eax                 
        je      .L0007                  
        movl    -4(%ebp),%eax           # i
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,-4(%ebp)           # i =
        jmp     .L0006                  
.L0007:                                 # End while-statement
        movl    -4(%ebp),%eax           # i
        jmp     .exit$strlen            # Return-statement
.exit$strlen:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function strlen
        .globl  is_palindrome           
is_palindrome:
        pushl   %ebp                    # Start function is_palindrome
        movl    %esp,%ebp               
        subl    $8,%esp                 # Get 8 bytes local data space
        movl    $0,%eax                 # 0
        movl    %eax,-4(%ebp)           # i1 =
        call    strlen                  # Call strlen
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        movl    %eax,-8(%ebp)           # i2 =
.L0008:                                 # Start while-statement
        movl    -4(%ebp),%eax           # i1
        pushl   %eax                    
        movl    -8(%ebp),%eax           # i2
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setl    %al                     # Test <
        cmpl    $0,%eax                 
        je      .L0009                  
                                        # Start if-statement
        movl    -4(%ebp),%eax           # i1
        leal    s,%edx                  # s[...]
        movl    (%edx,%eax,4),%eax      
        pushl   %eax                    
        movl    -8(%ebp),%eax           # i2
        leal    s,%edx                  # s[...]
        movl    (%edx,%eax,4),%eax      
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setne   %al                     # Test !=
        cmpl    $0,%eax                 
        je      .L0010                  
        movl    false,%eax              # false
        jmp     .exit$is_palindrome     # Return-statement
.L0010:                                 # End if-statement
        movl    -4(%ebp),%eax           # i1
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,-4(%ebp)           # i1 =
        movl    -8(%ebp),%eax           # i2
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        movl    %eax,-8(%ebp)           # i2 =
        jmp     .L0008                  
.L0009:                                 # End while-statement
        movl    true,%eax               # true
        jmp     .exit$is_palindrome     # Return-statement
.exit$is_palindrome:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function is_palindrome
        .globl  main                    
main:   pushl   %ebp                    # Start function main
        movl    %esp,%ebp               
        subl    $4,%esp                 # Get 4 bytes local data space
        movl    $0,%eax                 # 0
        movl    %eax,false              # false =
        movl    $1,%eax                 # 1
        movl    %eax,true               # true =
        movl    $10,%eax                # 10
        movl    %eax,LF                 # LF =
.L0011:                                 # Start while-statement
        movl    true,%eax               # true
        cmpl    $0,%eax                 
        je      .L0012                  
        movl    $32,%eax                # 32
        pushl   %eax                    # Push parameter #2
        movl    $63,%eax                # 63
        pushl   %eax                    # Push parameter #1
        call    p2                      # Call p2
        addl    $8,%esp                 # Remove parameters
        call    getstring               # Call getstring
                                        # Start if-statement
        call    strlen                  # Call strlen
        pushl   %eax                    
        movl    $0,%eax                 # 0
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        sete    %al                     # Test ==
        cmpl    $0,%eax                 
        je      .L0013                  
        movl    $0,%eax                 # 0
        pushl   %eax                    # Push parameter #1
        call    exit                    # Call exit
        addl    $4,%esp                 # Remove parameters
.L0013:                                 # End if-statement
        movl    $34,%eax                # 34
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
        call    putstring               # Call putstring
        movl    $34,%eax                # 34
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
        movl    $32,%eax                # 32
        pushl   %eax                    # Push parameter #4
        movl    $115,%eax               # 115
        pushl   %eax                    # Push parameter #3
        movl    $105,%eax               # 105
        pushl   %eax                    # Push parameter #2
        movl    $32,%eax                # 32
        pushl   %eax                    # Push parameter #1
        call    p4                      # Call p4
        addl    $16,%esp                # Remove parameters
        call    is_palindrome           # Call is_palindrome
        pushl   %eax                    
        movl    $0,%eax                 # 0
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        sete    %al                     # Test ==
        movl    %eax,-4(%ebp)           # no_p =
                                        # Start if-statement
        movl    -4(%ebp),%eax           # no_p
        cmpl    $0,%eax                 
        je      .L0014                  
        movl    $32,%eax                # 32
        pushl   %eax                    # Push parameter #3
        movl    $111,%eax               # 111
        pushl   %eax                    # Push parameter #2
        movl    $110,%eax               # 110
        pushl   %eax                    # Push parameter #1
        call    p3                      # Call p3
        addl    $12,%esp                # Remove parameters
.L0014:                                 # End if-statement
        movl    LF,%eax                 # LF
        pushl   %eax                    # Push parameter #12
        movl    $46,%eax                # 46
        pushl   %eax                    # Push parameter #11
        movl    $101,%eax               # 101
        pushl   %eax                    # Push parameter #10
        movl    $109,%eax               # 109
        pushl   %eax                    # Push parameter #9
        movl    $111,%eax               # 111
        pushl   %eax                    # Push parameter #8
        movl    $114,%eax               # 114
        pushl   %eax                    # Push parameter #7
        movl    $100,%eax               # 100
        pushl   %eax                    # Push parameter #6
        movl    $110,%eax               # 110
        pushl   %eax                    # Push parameter #5
        movl    $105,%eax               # 105
        pushl   %eax                    # Push parameter #4
        movl    $108,%eax               # 108
        pushl   %eax                    # Push parameter #3
        movl    $97,%eax                # 97
        pushl   %eax                    # Push parameter #2
        movl    $112,%eax               # 112
        pushl   %eax                    # Push parameter #1
        call    p12                     # Call p12
        addl    $48,%esp                # Remove parameters
        jmp     .L0011                  
.L0012:                                 # End while-statement
.exit$main:                                
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function main
