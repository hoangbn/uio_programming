        .data
.tmp:   .fill   4                       # Temporary storage
        .globl  ten
ten:    .fill   8                       # double ten;
        .globl  two
two:    .fill   8                       # double two;
        .globl  one
one:    .fill   8                       # double one;
        .globl  tenth
tenth:  .fill   8                       # double tenth;
        .text
        .globl  mod                     
mod:    pushl   %ebp                    # Start function mod
        movl    %esp,%ebp               
        movl    8(%ebp),%eax            # a
        pushl   %eax                    
        movl    8(%ebp),%eax            # a
        pushl   %eax                    
        movl    12(%ebp),%eax           # b
        movl    %eax,%ecx               
        popl    %eax                    
        cdq                             
        idivl   %ecx                    # Compute /
        pushl   %eax                    
        movl    12(%ebp),%eax           # b
        movl    %eax,%ecx               
        popl    %eax                    
        imull   %ecx,%eax               # Compute *
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        jmp     .exit$mod               # Return-statement
.exit$mod:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function mod
        .globl  rough_log               
rough_log:
        pushl   %ebp                    # Start function rough_log
        movl    %esp,%ebp               
                                        # Start if-statement
        fldl    8(%ebp)                 # x
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    ten                     # ten
        fldl    (%esp)                  
        addl    $8,%esp                 
        fsubp                           
        fstps   .tmp                    
        cmpl    $0,.tmp                 
        movl    $0,%eax                 
        setg    %al                     # Test >
        cmpl    $0,%eax                 
        je      .L0001                  
        fldl    8(%ebp)                 # x
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    ten                     # ten
        fldl    (%esp)                  
        addl    $8,%esp                 
        fdivp                           # Compute /
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    rough_log               # Call rough_log
        addl    $8,%esp                 # Remove parameters
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        jmp     .exit$rough_log         # Return-statement
.L0001:                                 # End if-statement
                                        # Start if-statement
        fldl    8(%ebp)                 # x
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    tenth                   # tenth
        fldl    (%esp)                  
        addl    $8,%esp                 
        fsubp                           
        fstps   .tmp                    
        cmpl    $0,.tmp                 
        movl    $0,%eax                 
        setl    %al                     # Test <
        cmpl    $0,%eax                 
        je      .L0002                  
        fldl    8(%ebp)                 # x
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    ten                     # ten
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    rough_log               # Call rough_log
        addl    $8,%esp                 # Remove parameters
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        jmp     .exit$rough_log         # Return-statement
.L0002:                                 # End if-statement
        movl    $1,%eax                 # 1
        jmp     .exit$rough_log         # Return-statement
.exit$rough_log:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function rough_log
        .globl  power                   
power:  pushl   %ebp                    # Start function power
        movl    %esp,%ebp               
        subl    $8,%esp                 # Get 8 bytes local data space
        movl    $1,%eax                 # 1
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   -8(%ebp)                # p =
.L0003:                                 # Start while-statement
        movl    16(%ebp),%eax           # n
        pushl   %eax                    
        movl    $0,%eax                 # 0
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setg    %al                     # Test >
        cmpl    $0,%eax                 
        je      .L0004                  
        fldl    -8(%ebp)                # p
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # x
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        fstpl   -8(%ebp)                # p =
        movl    16(%ebp),%eax           # n
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        movl    %eax,16(%ebp)           # n =
        jmp     .L0003                  
.L0004:                                 # End while-statement
.L0005:                                 # Start while-statement
        movl    16(%ebp),%eax           # n
        pushl   %eax                    
        movl    $0,%eax                 # 0
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setl    %al                     # Test <
        cmpl    $0,%eax                 
        je      .L0006                  
        fldl    -8(%ebp)                # p
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # x
        fldl    (%esp)                  
        addl    $8,%esp                 
        fdivp                           # Compute /
        fstpl   -8(%ebp)                # p =
        movl    16(%ebp),%eax           # n
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,16(%ebp)           # n =
        jmp     .L0005                  
.L0006:                                 # End while-statement
        fldl    -8(%ebp)                # p
        jmp     .exit$power             # Return-statement
        fldz                            
.exit$power:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function power
        .globl  sqrt                    
sqrt:   pushl   %ebp                    # Start function sqrt
        movl    %esp,%ebp               
        subl    $104,%esp               # Get 104 bytes local data space
        fldl    8(%ebp)                 # v
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    rough_log               # Call rough_log
        addl    $8,%esp                 # Remove parameters
        movl    %eax,-4(%ebp)           # d =
                                        # Start if-statement
        movl    $2,%eax                 # 2
        pushl   %eax                    # Push parameter #2
        movl    -4(%ebp),%eax           # d
        pushl   %eax                    # Push parameter #1
        call    mod                     # Call mod
        addl    $8,%esp                 # Remove parameters
        pushl   %eax                    
        movl    $1,%eax                 # 1
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        sete    %al                     # Test ==
        cmpl    $0,%eax                 
        je      .L0008                  
        movl    $0,%eax                 # 0
        pushl   %eax                    
        movl    $2,%eax                 # 2
        leal    -104(%ebp),%edx         
        popl    %ecx                    
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   (%edx,%ecx,8)           # x[...] =
        movl    $0,%eax                 # 0
        pushl   %eax                    
        movl    $0,%eax                 # 0
        leal    -104(%ebp),%edx         # x[...]
        fldl    (%edx,%eax,8)           
        subl    $8,%esp                 
        fstpl   (%esp)                  
        movl    -4(%ebp),%eax           # d
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        pushl   %eax                    
        movl    $2,%eax                 # 2
        movl    %eax,%ecx               
        popl    %eax                    
        cdq                             
        idivl   %ecx                    # Compute /
        pushl   %eax                    # Push parameter #2
        fldl    ten                     # ten
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    power                   # Call power
        addl    $12,%esp                # Remove parameters
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        leal    -104(%ebp),%edx         
        popl    %ecx                    
        fstpl   (%edx,%ecx,8)           # x[...] =
        jmp     .L0007                  
.L0008:                                 #   else-part
        movl    $0,%eax                 # 0
        pushl   %eax                    
        movl    $6,%eax                 # 6
        leal    -104(%ebp),%edx         
        popl    %ecx                    
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   (%edx,%ecx,8)           # x[...] =
        movl    $0,%eax                 # 0
        pushl   %eax                    
        movl    $0,%eax                 # 0
        leal    -104(%ebp),%edx         # x[...]
        fldl    (%edx,%eax,8)           
        subl    $8,%esp                 
        fstpl   (%esp)                  
        movl    -4(%ebp),%eax           # d
        pushl   %eax                    
        movl    $2,%eax                 # 2
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        pushl   %eax                    
        movl    $2,%eax                 # 2
        movl    %eax,%ecx               
        popl    %eax                    
        cdq                             
        idivl   %ecx                    # Compute /
        pushl   %eax                    # Push parameter #2
        fldl    ten                     # ten
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    power                   # Call power
        addl    $12,%esp                # Remove parameters
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        leal    -104(%ebp),%edx         
        popl    %ecx                    
        fstpl   (%edx,%ecx,8)           # x[...] =
.L0007:                                 # End if-statement
        movl    $1,%eax                 # 1
        movl    %eax,-8(%ebp)           # ix =
.L0009:                                 # Start for-statement
        movl    -8(%ebp),%eax           # ix
        pushl   %eax                    
        movl    $12,%eax                # 12
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setl    %al                     # Test <
        cmpl    $0,%eax                 
        je      .L0010                  
        movl    -8(%ebp),%eax           # ix
        pushl   %eax                    
        movl    -8(%ebp),%eax           # ix
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        leal    -104(%ebp),%edx         # x[...]
        fldl    (%edx,%eax,8)           
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # v
        subl    $8,%esp                 
        fstpl   (%esp)                  
        movl    -8(%ebp),%eax           # ix
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        leal    -104(%ebp),%edx         # x[...]
        fldl    (%edx,%eax,8)           
        fldl    (%esp)                  
        addl    $8,%esp                 
        fdivp                           # Compute /
        fldl    (%esp)                  
        addl    $8,%esp                 
        faddp                           # Compute +
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    two                     # two
        fldl    (%esp)                  
        addl    $8,%esp                 
        fdivp                           # Compute /
        leal    -104(%ebp),%edx         
        popl    %ecx                    
        fstpl   (%edx,%ecx,8)           # x[...] =
        movl    -8(%ebp),%eax           # ix
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,-8(%ebp)           # ix =
        jmp     .L0009                  
.L0010:                                 # End for-statement
        movl    $11,%eax                # 11
        leal    -104(%ebp),%edx         # x[...]
        fldl    (%edx,%eax,8)           
        jmp     .exit$sqrt              # Return-statement
        fldz                            
.exit$sqrt:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function sqrt
        .globl  main                    
main:   pushl   %ebp                    # Start function main
        movl    %esp,%ebp               
        movl    $10,%eax                # 10
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   ten                     # ten =
        movl    $2,%eax                 # 2
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   two                     # two =
        movl    $1,%eax                 # 1
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   one                     # one =
        fldl    one                     # one
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    ten                     # ten
        fldl    (%esp)                  
        addl    $8,%esp                 
        fdivp                           # Compute /
        fstpl   tenth                   # tenth =
        movl    $63,%eax                # 63
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
        call    getdouble               # Call getdouble
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    sqrt                    # Call sqrt
        addl    $8,%esp                 # Remove parameters
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    putdouble               # Call putdouble
        addl    $8,%esp                 # Remove parameters
        fstps   .tmp                    # Remove return value.
        movl    $10,%eax                # 10
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
        movl    $0,%eax                 # 0
        pushl   %eax                    # Push parameter #1
        call    exit                    # Call exit
        addl    $4,%esp                 # Remove parameters
.exit$main:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function main
