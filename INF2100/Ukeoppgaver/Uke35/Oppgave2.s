        .data
.tmp:   .fill   4                       # Temporary storage
        .text
        .globl  main                    
main:   pushl   %ebp                    # Start function main
        movl    %esp,%ebp               
        subl    $12,%esp                # Get 12 bytes local data space
        movl    $0,%eax                 # 0
        movl    %eax,-12(%ebp)          # i =
.L0001:                                 # Start for-statement
        movl    -12(%ebp),%eax          # i
        pushl   %eax                    
        movl    $9,%eax                 # 9
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setl    %al                     # Test <
        cmpl    $0,%eax                 
        je      .L0002                  
                                        # Start if-statement
        movl    -12(%ebp),%eax          # i
        pushl   %eax                    
        movl    $0,%eax                 # 0
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        sete    %al                     # Test ==
        cmpl    $0,%eax                 
        je      .L0003                  
        call    getint                  # Call getint
        movl    %eax,-4(%ebp)           # max =
.L0003:                                 # End if-statement
        call    getint                  # Call getint
        movl    %eax,-8(%ebp)           # number =
                                        # Start if-statement
        movl    -8(%ebp),%eax           # number
        pushl   %eax                    
        movl    -4(%ebp),%eax           # max
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setg    %al                     # Test >
        cmpl    $0,%eax                 
        je      .L0004                  
        movl    -8(%ebp),%eax           # number
        movl    %eax,-4(%ebp)           # max =
.L0004:                                 # End if-statement
        movl    -12(%ebp),%eax          # i
        pushl   %eax                    
        movl    $1,%eax                 # 1
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,-12(%ebp)          # i =
        jmp     .L0001                  
.L0002:                                 # End for-statement
        movl    -4(%ebp),%eax           # max
        pushl   %eax                    # Push parameter #1
        call    putint                  # Call putint
        addl    $4,%esp                 # Remove parameters
.exit$main:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function main
