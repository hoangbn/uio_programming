        .data
.tmp:   .fill   4                       # Temporary storage
        .text
        .globl  lowercase               
lowercase:
        pushl   %ebp                    # Start function lowercase
        movl    %esp,%ebp               
                                        # Start if-statement
        movl    8(%ebp),%eax            # c
        pushl   %eax                    
        movl    $90,%eax                # 90
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setle   %al                     # Test <=
        cmpl    $0,%eax                 
        je      .L0001                  
        movl    8(%ebp),%eax            # c
        pushl   %eax                    
        movl    $32,%eax                # 32
        movl    %eax,%ecx               
        popl    %eax                    
        addl    %ecx,%eax               # Compute +
        movl    %eax,8(%ebp)            # c =
.L0001:                                 # End if-statement
        movl    8(%ebp),%eax            # c
        jmp     .exit$lowercase         # Return-statement
.exit$lowercase:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function lowercase
        .globl  uppercase               
uppercase:
        pushl   %ebp                    # Start function uppercase
        movl    %esp,%ebp               
                                        # Start if-statement
        movl    8(%ebp),%eax            # c
        pushl   %eax                    
        movl    $97,%eax                # 97
        popl    %ecx                    
        cmpl    %eax,%ecx               
        movl    $0,%eax                 
        setge   %al                     # Test >=
        cmpl    $0,%eax                 
        je      .L0002                  
        movl    8(%ebp),%eax            # c
        pushl   %eax                    
        movl    $32,%eax                # 32
        movl    %eax,%ecx               
        popl    %eax                    
        subl    %ecx,%eax               # Compute -
        movl    %eax,8(%ebp)            # c =
.L0002:                                 # End if-statement
        movl    8(%ebp),%eax            # c
        jmp     .exit$uppercase         # Return-statement
.exit$uppercase:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function uppercase
        .globl  main                    
main:   pushl   %ebp                    # Start function main
        movl    %esp,%ebp               
        subl    $12,%esp                # Get 12 bytes local data space
        call    getchar                 # Call getchar
        movl    %eax,-4(%ebp)           # c =
        movl    -4(%ebp),%eax           # c
        pushl   %eax                    # Push parameter #1
        call    lowercase               # Call lowercase
        addl    $4,%esp                 # Remove parameters
        movl    %eax,-8(%ebp)           # cl =
        movl    -4(%ebp),%eax           # c
        pushl   %eax                    # Push parameter #1
        call    uppercase               # Call uppercase
        addl    $4,%esp                 # Remove parameters
        movl    %eax,-12(%ebp)          # cu =
        movl    -8(%ebp),%eax           # cl
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
        movl    -12(%ebp),%eax          # cu
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
.exit$main:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function main
