        .data
.tmp:   .fill   4                       # Temporary storage
        .globl  zero
zero:   .fill   8                       # double zero;
        .text
        .globl  abs                     
abs:    pushl   %ebp                    # Start function abs
        movl    %esp,%ebp               
                                        # Start if-statement
        fldl    8(%ebp)                 # x
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    zero                    # zero
        fldl    (%esp)                  
        addl    $8,%esp                 
        fsubp                           
        fstps   .tmp                    
        cmpl    $0,.tmp                 
        movl    $0,%eax                 
        setl    %al                     # Test <
        cmpl    $0,%eax                 
        je      .L0001                  
        fldl    8(%ebp)                 # x
        jmp     .exit$abs               # Return-statement
.L0001:                                 # End if-statement
        fldl    8(%ebp)                 # x
        jmp     .exit$abs               # Return-statement
        fldz                            
.exit$abs:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function abs
        .globl  main                    
main:   pushl   %ebp                    # Start function main
        movl    %esp,%ebp               
        subl    $8,%esp                 # Get 8 bytes local data space
        movl    $314,%eax               # 314
        pushl   %eax                    
        movl    $100,%eax               # 100
        movl    %eax,%ecx               
        popl    %eax                    
        cdq                             
        idivl   %ecx                    # Compute /
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   zero                    # zero =
        call    getdouble               # Call getdouble
        fstpl   -8(%ebp)                # v =
        fldl    -8(%ebp)                # v
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    abs                     # Call abs
        addl    $8,%esp                 # Remove parameters
        fstpl   -8(%ebp)                # v =
        fldl    -8(%ebp)                # v
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    putdouble               # Call putdouble
        addl    $8,%esp                 # Remove parameters
        fstps   .tmp                    # Remove return value.
.exit$main:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function main
