        .data
.tmp:   .fill   4                       # Temporary storage
        .globl  pi
pi:     .fill   8                       # double pi;
        .globl  four
four:   .fill   8                       # double four;
        .globl  tre
tre:    .fill   8                       # double tre;
        .text
        .globl  surface                 
surface:
        pushl   %ebp                    # Start function surface
        movl    %esp,%ebp               
        fldl    four                    # four
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    pi                      # pi
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # r
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # r
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        jmp     .exit$surface           # Return-statement
        fldz                            
.exit$surface:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function surface
        .globl  volume                  
volume: pushl   %ebp                    # Start function volume
        movl    %esp,%ebp               
        fldl    four                    # four
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    tre                     # tre
        fldl    (%esp)                  
        addl    $8,%esp                 
        fdivp                           # Compute /
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    pi                      # pi
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # r
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # r
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        subl    $8,%esp                 
        fstpl   (%esp)                  
        fldl    8(%ebp)                 # r
        fldl    (%esp)                  
        addl    $8,%esp                 
        fmulp                           # Compute *
        jmp     .exit$volume            # Return-statement
        fldz                            
.exit$volume:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function volume
        .globl  main                    
main:   pushl   %ebp                    # Start function main
        movl    %esp,%ebp               
        subl    $24,%esp                # Get 24 bytes local data space
        movl    $114,%eax               # 114
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
        movl    $63,%eax                # 63
        pushl   %eax                    # Push parameter #1
        call    putchar                 # Call putchar
        addl    $4,%esp                 # Remove parameters
        call    getdouble               # Call getdouble
        fstpl   -8(%ebp)                # r =
        movl    $314,%eax               # 314
        pushl   %eax                    
        movl    $100,%eax               # 100
        movl    %eax,%ecx               
        popl    %eax                    
        cdq                             
        idivl   %ecx                    # Compute /
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   pi                      # pi =
        movl    $4,%eax                 # 4
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   four                    # four =
        movl    $3,%eax                 # 3
        movl    %eax,.tmp               
        fildl   .tmp                    #   (double)
        fstpl   tre                     # tre =
        fldl    -8(%ebp)                # r
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    surface                 # Call surface
        addl    $8,%esp                 # Remove parameters
        fstpl   -16(%ebp)               # surfac =
        fldl    -8(%ebp)                # r
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    volume                  # Call volume
        addl    $8,%esp                 # Remove parameters
        fstpl   -24(%ebp)               # volum =
        fldl    -16(%ebp)               # surfac
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    putdouble               # Call putdouble
        addl    $8,%esp                 # Remove parameters
        fstps   .tmp                    # Remove return value.
        fldl    -24(%ebp)               # volum
        subl    $8,%esp                 
        fstpl   (%esp)                  # Push parameter #1
        call    putdouble               # Call putdouble
        addl    $8,%esp                 # Remove parameters
        fstps   .tmp                    # Remove return value.
.exit$main:
        movl    %ebp,%esp               
        popl    %ebp                    
        ret                             # End function main
