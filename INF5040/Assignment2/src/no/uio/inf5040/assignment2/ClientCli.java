package no.uio.inf5040.assignment2;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class ClientCli {
	private BankClient bc;
	private Scanner sc;
	private String filename;
	public ClientCli(BankClient client) {
		// TODO Auto-generated constructor stub
		this.bc = client;
	}
	public ClientCli(BankClient client, String filename){
		this.bc = client;
		this.filename = filename;
	}
	
	//Start commandoline etc.
	public void init(){
		sc = new Scanner(System.in);
		this.startInterpreter();
	}
	
	public void startInterpreter(){
		String input = "";
		//Sleep until a value is true.
		//bc.waitForUsers();
		while(true){
			try{
				input = sc.nextLine();
			}catch(NoSuchElementException e){
				e.printStackTrace();
			}
			System.out.println("Input from keyboard: " + input);
			input = input.toLowerCase();//lazyfix
			if(input.equalsIgnoreCase("exit"))
				break;
			else if(input.startsWith("sleep")){
				String tmp[] = input.split(" ");
				if(tmp.length == 1)
					System.out.println("Usage: Sleep <duration>");
				else{
					System.out.println("sleep for " + tmp[1]);
					try {
						Thread.sleep(Integer.parseInt(tmp[1]));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			else if(bc.getInitLock()){
				System.out.println("Still waiting for clients");
			}
			else{
				System.out.println("Sending to bankClient:" + input);
				bc.sendMessage(input);
			}
			
			//System.out.println();
		}
	}//Interpreter
	
	/*Read file and start it*/
	public void startFile(){
		
	}
}
