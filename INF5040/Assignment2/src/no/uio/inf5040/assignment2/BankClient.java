package no.uio.inf5040.assignment2;

import java.io.InterruptedIOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

import spread.*;

public class BankClient {

	public SpreadConnection connection;
	private String connName;
	private int activeUsers; // Count users
	private int maxUsers;
	private int balance; // Should use float/double because of decimal numbers. to lazy?

	private boolean initLock;


	private String add;
	private int port;
	private String group;

	private State state;
	public BankClient(String add, int port, String gr, int max) {
		this.add = add;
		this.port = port;
		this.group = gr;

		this.state = State.INIT;

		// reset all numbers
		this.activeUsers = 0;
		this.maxUsers = max;
		this.initLock = true;
		this.balance = 0;
		// TODO Auto-generated constructor stub
	}

	public void init() {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(100);
		connName = "Cl-" + randomInt;

		connection = new SpreadConnection();
		try {
			connection.connect(InetAddress.getByName("localhost"), 4803,
					connName, false, true);
			connection.add(new Listener());

			SpreadGroup spreadGroup = new SpreadGroup();
			spreadGroup.join(connection, group);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SpreadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*Should handle lock here
	 * e.g:
	 *  INIT:message
	 * 	USER:message
	 * 	BANK:message
	 * */
	public void sendMessage(String text) {
		System.out.println("BankClient sendMessage: " + text);
		
		//Construct text before adding state
		if(text.equalsIgnoreCase("balance")){
			text = text + ":" + getBalance();
		}
		else if(text.startsWith("deposit")){
			String tmp[] = text.split(" ");
			if(tmp.length == 1){
				System.out.println("Fail use of deposit");
				return;
			}
			//deposit:amount
			text = tmp[0] + ":" + tmp[1];
		}
		else if(text.startsWith("withdraw")){
			String tmp[] = text.split(" ");
			if(tmp.length == 1){
				System.out.println("Fail use of withdraw");
				return;
			}
			//withdraw:amount
			text = tmp[0] + ":" + tmp[1];
		}
		else if(text.startsWith("addinterest")){
			String tmp[] = text.split(" ");
			if(tmp.length == 1){
				System.out.println("Fail use of addinterest");
				return;
			}
			if(Integer.parseInt(tmp[1]) > 100){
				System.out.println("Percent can only be 1-100");
				return;
			}
			//addinterest:amount
			text = tmp[0] + ":" + tmp[1];
		}
		
		if (!initLock) {
			SpreadMessage message = new SpreadMessage();
			text = state+":"+text;
			System.out.println("BankClient sendMessage: " + text);

			message.setData(text.getBytes());
			message.addGroup(group);
			message.setSafe();

			try {
				connection.multicast(message);
			} catch (SpreadException e) {
				e.printStackTrace();
			}
		}
	}
	public void printConnectedClients(){
		System.out.println(connection.getPrivateGroup());
	}
	public State getState(){
		return state;
	}

	public void setState(State state){
		this.state = state;
	}
	public synchronized int getActiveUsers() {
		return this.activeUsers;
	}

	public synchronized void incActiveUsers() {
		this.activeUsers++;
	}

	public synchronized void setActiveUsers(int i) {
		this.activeUsers = i;
	}

	public boolean getInitLock(){
		return this.initLock;
	}

	public void shutdown() throws SpreadException {
		connection.disconnect();
	}
	
	public void handleBalance(String t){
		System.out.println("Got balance" + t);
	
	}

	public void setBalance(int t){
		this.balance = t;
	}
	
	public int getBalance(){
		return this.balance;
	}
	class Listener implements AdvancedMessageListener {
		@Override
		public void regularMessageReceived(SpreadMessage message) {
			// System.out.println("sup");
			// TODO Auto-generated method stub
			
				if (message.isRegular()) {
					String sender = message.getSender().toString();
					String data = new String(message.getData());
					String sp[] = data.split(":");
					System.out.println("New message from " + sender + " " + data);
					
					if(sp.length < 2)
						return;
					
					//System already running. Get balance before run command
					if(sp[0].equalsIgnoreCase("UP") && state == State.INIT){
						//send balance
						//lock until balance is set.
						System.out.println("GIVE ME LOGIC PLES");
					}

					//Logic to handle different balance
					
					data = sp[1];
					if(data.length() <= 3)
						return;
					
					if("exit".equalsIgnoreCase(data.substring(0,4))){
						System.out.println("exit");
					}
					else if("sleep".equalsIgnoreCase(data.substring(0,5))){
						System.out.println("Sleep");
					}
					else if("balance".equalsIgnoreCase(data.substring(0, 7))){
						System.out.println("Current balance: " + balance);
						if(balance != Integer.parseInt(sp[2]))
							System.out.println("WTF YOUR BALANCE IS DIFFERENT FROM MINE!");
					}	
					else if("deposit".equalsIgnoreCase(data.substring(0, 7))){
						System.out.println("Deposit -  Current balance" + balance);
						System.out.println("Adding " + sp[2] + " to account");
						balance += Integer.parseInt(sp[2]);	
					}
					else if("withdraw".equalsIgnoreCase(data.substring(0,8))){
						System.out.println("Withdraw - Current balance" + balance);
						System.out.println("Subtracting " + sp[2] + " to account");
						balance -= Integer.parseInt(sp[2]);
					}
					else if("addinterest".equalsIgnoreCase(data.substring(0,11))){
						System.out.println("Add Interest - current balance" + balance);
						System.out.println("Adding " + sp[2] + "%" + " to account");
						double percent = Double.parseDouble(sp[2]);
						
						balance = (int) ((double)balance * (1.0 + (percent/100.0)));
						System.out.println("New balance: " + balance);
					}
					else 
						System.out.println("Got unknown message.");


					//SpreadGroup groups[] = message.getGroups();
					//System.out.println("Group length: " + groups.length);
					//printConnectedClients();
					//System.out.println(message.getMembershipInfo().getGroup());
					// Send message back.? Create a intepreter on server site
			}//
		}// regularMessageReceived

		//
		// Cannot start sending until activeUsers == maxUsers?
		// Cases:
		// Client init
		// Client fail and just started again
		// More then max users.
		//Fail if active users are les then maxUsers, Scenaro: max = 3, started 3 clients. 2 fail, one join. current will be 2. That why initlock is there?
		//Scenaro failed because the activeusers from spread is init to 0 and set to 2. 
		public void membershipMessageReceived(SpreadMessage message) {
			// TODO Auto-generated method stub
			//incActiveUsers();
			setActiveUsers(message.getMembershipInfo().getMembers().length);
			if(activeUsers >= maxUsers){
				initLock = false;
				state = State.UP;
				System.out.println("System is ready to start");
			}
			System.out.println("New membership message from "
					+ message.getMembershipInfo().getMembers().length);
			System.out.println("Current active users: " + activeUsers);
			//System.out.println("Joined group: "+  message.getMembershipInfo().getGroup());

		}
	}
}
