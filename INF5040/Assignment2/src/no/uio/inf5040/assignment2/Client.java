package no.uio.inf5040.assignment2;

public class Client {
	/*Should provide parameters when running*/
	public static void main(String[] args) {
		//Parse arguments
		parseArguments(args);
		
		String address = "localhost", 
				group = "testGroup", 
				filename = "";
		int port = 4809, max = 5;
		BankClient client;
		ClientCli clientCli;
		
		System.out.println("Starting Client...");
		if (args.length == 0){
			System.out.println("Running with default settings");
			client = new BankClient(address, port, group,  max);
			clientCli = new ClientCli(client);
		}
		else if(args.length == 1){
			System.out.println("Running with default settings and loading file: " + args[0]);
			client = new BankClient(address, port, group,  max);
			clientCli = new ClientCli(client, args[0]);
		}
		else{
			System.out.println("Parameters: " + args[0] + ":" + args[1] + " " + args[2] + " " + args[3]);
			address = args[0];
			port = Integer.parseInt(args[1]);
			group = args[2];
			max = Integer.parseInt(args[3]);
			
			client = new BankClient(address, port, group,  max);
			clientCli = new ClientCli(client);
			System.out.println(client.getState());

			client.init();
			clientCli.init();
			
		}
		System.out.println("Shutting down");
		
	}
	
	
	public static void parseArguments(String[] args){
		if (args.length == 0){
			System.out.println("Running with default settings");
		}
		else if(args.length == 1){
			System.out.println("Running with default settings and loading file: " + args[0]);
		}
		else if(args.length < 3 || args.length > 5){
			printUsage();
			System.exit(1);
		}
	}
	/*Lage optional parameters? */
	public static void printUsage(){
		System.out.println("java Client <server address> <account name> <number of replicas> [filename](Optional)");
		System.out.println("java Client = Running client with standard ip, standard replicas(5), and standard group(testGroup)");
		System.out.println("java Client [filename] = Same as above, but running actions from file. ");
	}
}
