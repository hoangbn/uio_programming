set terminal png enhanced
set output 'StarInDegree.png'

set title "In-degree distribution"
set xlabel "in-degree"
set ylabel "number of nodes"
set key right top
plot 'StarShuffle30Degree.txt' title 'Basic Shuffle c = 30' with histeps, \
	"Random30InDegree.txt" title 'Random Graph c = 30' with histeps, \
	"StarShuffle50Degree.txt" title 'Basic Shuffle c = 50' with histeps, \
	"Random50InDegree.txt" title 'Random Graph c = 50' with histeps
	