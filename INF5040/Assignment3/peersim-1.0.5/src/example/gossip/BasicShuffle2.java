package example.gossip;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;


/**
 * @author Lucas Provensi
 * 
 * Basic Shuffling protocol template
 * 
 * The basic shuffling algorithm, introduced by Stavrou et al in the paper: 
 * "A Lightweight, Robust P2P System to Handle Flash Crowds", is a simple 
 * peer-to-peer communication model. It forms an overlay and keeps it 
 * connected by means of an epidemic algorithm. The protocol is extremely 
 * simple: each peer knows a small, continuously changing set of other peers, 
 * called its neighbors, and occasionally contacts a random one to exchange 
 * some of their neighbors.
 * 
 * This class is a template with instructions of how to implement the shuffling
 * algorithm in PeerSim.
 * Should make use of the classes Entry and GossipMessage:
 *    Entry - Is an entry in the cache, contains a reference to a neighbor node
 *  		  and a reference to the last node this entry was sent to.
 *    GossipMessage - The message used by the protocol. It can be a shuffle
 *    		  request, reply or reject message. It contains the originating
 *    		  node and the shuffle list.
 *
 */
public class BasicShuffle2  implements Linkable, EDProtocol, CDProtocol{
	
	private static final String PAR_CACHE = "cacheSize";
	private static final String PAR_L = "shuffleLength";
	private static final String PAR_TRANSPORT = "transport";

	private final int tid;

	// The list of neighbors known by this node, or the cache.
	private List<Entry> cache;
	
	// The maximum size of the cache;
	private final int size;
	
	// The maximum length of the shuffle exchange;
	private final int l;
	
	private boolean waiting = false;
	/**
	 * Constructor that initializes the relevant simulation parameters and
	 * other class variables.
	 * 
	 * @param n simulation parameters
	 */
	public BasicShuffle2(String n)
	{
		this.size = Configuration.getInt(n + "." + PAR_CACHE);
		this.l = Configuration.getInt(n + "." + PAR_L);
		this.tid = Configuration.getPid(n + "." + PAR_TRANSPORT);
		
		cache = new ArrayList<Entry>(size);
	}

	/* START YOUR IMPLEMENTATION FROM HERE
	 * 
	 * The simulator engine calls the method nextCycle once every cycle 
	 * (specified in time units in the simulation script) for all the nodes.
	 * 
	 * You can assume that a node initiates a shuffling operation every cycle.
	 * 
	 * @see peersim.cdsim.CDProtocol#nextCycle(peersim.core.Node, int)
	 */
	@Override
	public void nextCycle(Node node, int protocolID) {
		// Implement the shuffling protocol using the following steps (or
		// you can design a similar algorithm):
		// Let's name this node as P

		// 1. If P is waiting for a response from a shuffling operation initiated in a previous cycle, return;
		// 2. If P's cache is empty, return;		
		// 3. Select a random neighbor (named Q) from P's cache to initiate the shuffling;
		//	  - You should use the simulator's common random source to produce a random number: CommonState.r.nextInt(cache.size())
		// 4. If P's cache is full, remove Q from the cache;
		// 5. Select a subset of other l - 1 random neighbors from P's cache;
		//	  - l is the length of the shuffle exchange
		//    - Do not add Q to this subset	
		// 6. Add P to the subset;
		// 7. Send a shuffle request to Q containing the subset;
		//	  - Keep track of the nodes sent to Q
		//	  - Example code for sending a message:
		//
		// GossipMessage message = new GossipMessage(node, subset);
		// message.setType(MessageType.SHUFFLE_REQUEST);
		// Transport tr = (Transport) node.getProtocol(tid);
		// tr.send(node, Q.getNode(), message, protocolID);
		//
		// 8. From this point on P is waiting for Q's response and will not initiate a new shuffle operation;
		//
		// The response from Q will be handled by the method processEvent.

		/*Initialize variables*/
		List<Entry> subset = new ArrayList<Entry>(l); 
		Entry ent;

		/*Check if it is waiting for a shuffling request*/
		if(waiting){
			//System.out.println("Node " + node.getID() + " is waiting for a shuffling reply , cache size " + degree());
			return;
		}
		
		/*Check if the cache is empty*/
		if(cache.isEmpty()){
			System.out.println("Node " + node.getID() + " has no neighbor");
			return;				
		}
		
		/*Using the random number, pick a random neighbor*/
		Entry Q = new Entry(getNeighbor(getRandom()));
		/*If the cache is full, then remove Q from the list could test if Q is removed from list, no need to test remove since Q was taken from the list*/
		if(degree() == size) cache.remove(Q);

		/*
		 * Fill the subset with node until the shuffle length are met
		 * or if the cache size is smaller than l fill it up with nodes until the cache size of neighbors are met
		 */
		int stop = degree() < l - 1 ? degree() : l - 1;
		while(subset.size() < stop){
			ent = new Entry(getNeighbor(getRandom()));
			/*Make sure the new entry doesn't exist in the subset and it isn't entry Q(the random node Q)*/
			if(!subset.contains(ent)){
				subset.add(ent);
			}
		}
		if(subset.contains(Q)){
			subset.remove(Q);
		//	System.out.println("Contains Q size " + subset.size());
		//	System.exit(1);
		}
		/*add node to the subset*/
		subset.add(new Entry(node));

		/*Keep track of which Node's are being send to*/
		/*Not being used, maybe remove it?*/
		for(int i=0;i<subset.size();i++) subset.get(i).setSentTo(Q.getNode());

		// 7. Send a shuffle request to Q containing the subset;
		//	  - Keep track of the nodes sent to Q
		//	  - Example code for sending a message:
		//
		// GossipMessage message = new GossipMessage(node, subset);
		// message.setType(MessageType.SHUFFLE_REQUEST);
		// Transport tr = (Transport) node.getProtocol(tid);
		// tr.send(node, Q.getNode(), message, protocolID);
		//
		// 8. From this point on P is waiting for Q's response and will not initiate a new shuffle operation;
		//
		// The response from Q will be handled by the method processEvent.

		GossipMessage gossipMsg = new GossipMessage(node, subset);
		gossipMsg.setType(MessageType.SHUFFLE_REQUEST);
		Transport tr = (Transport) node.getProtocol(tid);
		tr.send(node, Q.getNode(), gossipMsg, protocolID);

		/*set boolean to true, thus saying this node is waiting for a shuffling request*/
		waiting = true;
	}

	/* The simulator engine calls the method processEvent at the specific time unit that an event occurs in the simulation.
	 * It is not called periodically as the nextCycle method.
	 * 
	 * You should implement the handling of the messages received by this node in this method.
	 * 
	 * @see peersim.edsim.EDProtocol#processEvent(peersim.core.Node, int, java.lang.Object)
	 */
	@Override
	public void processEvent(Node node, int pid, Object event) {

		/*Initialize variables*/
		List<Entry> subset = new ArrayList<Entry>(l), tmp;
		subset.clear();
		Entry ent/*, intern, extern*/;
		// Let's name this node as Q;
		// Q receives a message from P;
		//	  - Cast the event object to a message:
		GossipMessage message = (GossipMessage) event;

		if(degree() == 0 && node.getID() != 0){
			System.err.println("Inside 2 " + node.getID() + " Another node ? " + message.getShuffleList().get(0).getNode().getID());
			System.exit(1);
		}
		
		switch (message.getType()) {
		// If the message is a shuffle request:
		case SHUFFLE_REQUEST:
			//	  1. If Q is waiting for a response from a shuffling initiated in a previous cycle, send back to P a message rejecting the shuffle request; 
			//	  2. Q selects a random subset of size l of its own neighbors; 
			//	  3. Q reply P's shuffle request by sending back its own subset;
			//	  4. Q updates its cache to include the neighbors sent by P:
			//		 - No neighbor appears twice in the cache
			//		 - Use empty cache slots to add the new entries
			//		 - If the cache is full, you can replace entries among the ones sent to P with the new ones

			if(waiting) {
				/*If the node is waiting for a shuffling request initiated before, then it will send back nothing but a message SHUFFLE_REJECTED*/
				GossipMessage gossipMsg = new GossipMessage(message.getNode(), null);
				gossipMsg.setType(MessageType.SHUFFLE_REJECTED);
				Transport tr = (Transport) node.getProtocol(tid);
				tr.send(node, message.getNode(), gossipMsg, pid);
				return;
			} else {
				/*
				 * Fill the subset with node until the shuffle length are met
				 * or if the cache size is smaller than l fill it up with nodes until the cache size of neighbors are met
				 */
				int stop = degree() < l ? degree() : l;
				while(subset.size() < stop){
					ent = new Entry(getNeighbor(getRandom()));
					/*Make sure the new entry doesn't exist in the subset*/
					if(!subset.contains(ent)){
						subset.add(ent);
					}
				}
			}
			
			/*Subset of its neighbors has been made, send it back to the one who requested the shuffle*/
			GossipMessage gossipMsg = new GossipMessage(message.getNode(), subset);
			gossipMsg.setType(MessageType.SHUFFLE_REPLY);
			Transport tr = (Transport) node.getProtocol(tid);
			tr.send(node, message.getNode(), gossipMsg, pid);
	
//			  4. Q updates its cache to include the neighbors sent by P:
			//		 - No neighbor appears twice in the cache
			//		 - Use empty cache slots to add the new entries
			//		 - If the cache is full, you can replace entries among the ones sent to P with the new ones
			
			/*Update our cache with the nodes we received from the one who sended the shuffle request*/
			tmp = message.getShuffleList();
			int counter = 0;
			while(counter < tmp.size()){
				/*If the cache already contains the node, ignore it*/
				if(contains(tmp.get(counter).getNode())){
					counter++;
				} else {
					/*If there are still space left in the cache, add the new node to it*/
					if(degree() < size) {
						if(!addNeighbor(tmp.get(counter).getNode())) System.out.println("Something wrong in SHUFFLE_REQUEST 1");
					}
					else {
						for(int i = 0; i < tmp.size(); i++){
							if(contains(tmp.get(i).getNode())){
								cache.remove(tmp.get(i));
								if(!addNeighbor(tmp.get(counter).getNode())) System.out.println("Something wrong in SHUFFLE_REQUEST");
								break;
							}
						}
					}
					counter++;
				}
			}	
			break;
		
		// If the message is a shuffle reply:
		case SHUFFLE_REPLY:
		//	  1. In this case Q initiated a shuffle with P and is receiving a response containing a subset of P's neighbors
		//	  2. Q updates its cache to include the neighbors sent by P:
		//		 - No neighbor appears twice in the cache
		//		 - Use empty cache slots to add new entries
		//		 - If the cache is full, you can replace entries among the ones originally sent to P with the new ones
		//	  3. Q is no longer waiting for a shuffle reply;	 
			/*Got a reply from a peer*/

			tmp = message.getShuffleList();
			counter = 0;
			while(counter < tmp.size()){
				/*If the cache already contains the node, ignore it*/
				if(contains(tmp.get(counter).getNode())){
					counter++;
				} else {
					/*If there are still space left in the cache, add the new node to it*/
					if(degree() < size){
						if(!addNeighbor(tmp.get(counter).getNode())) System.out.println("Something wrong in SHUFFLE_REPLY 1");			
					}
					else {
						for(int i = 0; i < tmp.size(); i++){
							if(contains(tmp.get(i).getNode())){
								cache.remove(tmp.get(i));
								if(!addNeighbor(tmp.get(counter).getNode())) System.out.println("Something wrong in SHUFFLE_REPLY");
								break;
							}
						}
					}
					counter++;
				}
			}
			/*This node is no longer waiting for a shuffle reply*/
			waiting = false;
			break;
		
		// If the message is a shuffle rejection:
		case SHUFFLE_REJECTED:
			/*We shouldn't have to worry about cache being full since we removed P from Q's cache, so it will 
			 * always have 1 free space left when we come here
			 * */
			if(!contains(node)) if(!addNeighbor(node)) System.out.println("SOMETHING WENT WRONG IN SHUFLE_REJECT");

			/*No longer waiting for a shuffle reply*/
			waiting = false;
			break;
		default:
			break;
		}
	}
	
/* The following methods are used only by the simulator and don't need to be changed */
	
	/*Return a random number*/
	private int getRandom(){
		return CommonState.r.nextInt(degree());
	}
		
	@Override
	public int degree() {
		return cache.size();
	}

	@Override
	public Node getNeighbor(int i) {
		return cache.get(i).getNode();
	}

	@Override
	public boolean addNeighbor(Node neighbour) {
		if (contains(neighbour))
			return false;

		if (cache.size() >= size)
			return false;

		Entry entry = new Entry(neighbour);
		cache.add(entry);

		return true;
	}

	@Override
	public boolean contains(Node neighbor) {
		return cache.contains(new Entry(neighbor));
	}

	public Object clone()
	{
		BasicShuffle2 gossip = null;
		try { 
			gossip = (BasicShuffle2) super.clone(); 
		} catch( CloneNotSupportedException e ) {
			
		} 
		gossip.cache = new ArrayList<Entry>();

		return gossip;
	}

	@Override
	public void onKill() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void pack() {
		// TODO Auto-generated method stub	
	}
}
