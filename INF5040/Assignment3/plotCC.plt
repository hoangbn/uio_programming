# You can uncomment the following lines to produce a png figure
set terminal png enhanced
set output 'StarAverageClustering.png'

set title "Average Clustering Coefficient"
set xlabel "cycles"
set ylabel "clustering coefficient (log)"
set key right top
set logscale y 
plot "Random30Cluster.txt" title 'Random Graph c = 30' with lines, \
	"StarShuffle30Clust.txt" title 'Shuffle c = 30' with lines, \
	"Random50Cluster.txt" title 'Random Graph c = 50' with lines, \
	"StarShuffle50Clust.txt" title 'Shuffle c = 50' with lines