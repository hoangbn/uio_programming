package example.gossip;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;


/**
 * @author Lucas Provensi
 * 
 * Basic Shuffling protocol template
 * 
 * The basic shuffling algorithm, introduced by Stavrou et al in the paper: 
 * "A Lightweight, Robust P2P System to Handle Flash Crowds", is a simple 
 * peer-to-peer communication model. It forms an overlay and keeps it 
 * connected by means of an epidemic algorithm. The protocol is extremely 
 * simple: each peer knows a small, continuously changing set of other peers, 
 * called its neighbors, and occasionally contacts a random one to exchange 
 * some of their neighbors.
 * 
 * This class is a template with instructions of how to implement the shuffling
 * algorithm in PeerSim.
 * Should make use of the classes Entry and GossipMessage:
 *    Entry - Is an entry in the cache, contains a reference to a neighbor node
 *  		  and a reference to the last node this entry was sent to.
 *    GossipMessage - The message used by the protocol. It can be a shuffle
 *    		  request, reply or reject message. It contains the originating
 *    		  node and the shuffle list.
 *
 */
public class BasicShuffle  implements Linkable, EDProtocol, CDProtocol{
	
	private static final String PAR_CACHE = "cacheSize";
	private static final String PAR_L = "shuffleLength";
	private static final String PAR_TRANSPORT = "transport";

	private final int tid;

	// The list of neighbors known by this node, or the cache.
	private List<Entry> cache;
	
	// The maximum size of the cache;
	private final int size;
	
	// The maximum length of the shuffle exchange;
	private final int l;
	
	private boolean waiting = false;
	/**
	 * Constructor that initializes the relevant simulation parameters and
	 * other class variables.
	 * 
	 * @param n simulation parameters
	 */
	public BasicShuffle(String n)
	{
		this.size = Configuration.getInt(n + "." + PAR_CACHE);
		this.l = Configuration.getInt(n + "." + PAR_L);
		this.tid = Configuration.getPid(n + "." + PAR_TRANSPORT);
		
		cache = new ArrayList<Entry>(size);
	}

	/* START YOUR IMPLEMENTATION FROM HERE
	 * 
	 * The simulator engine calls the method nextCycle once every cycle 
	 * (specified in time units in the simulation script) for all the nodes.
	 * 
	 * You can assume that a node initiates a shuffling operation every cycle.
	 * 
	 * @see peersim.cdsim.CDProtocol#nextCycle(peersim.core.Node, int)
	 */
	@Override
	public void nextCycle(Node node, int protocolID) {


		// Implement the shuffling protocol using the following steps (or
		// you can design a similar algorithm):
		// Let's name this node as P

		// 1. If P is waiting for a response from a shuffling operation initiated in a previous cycle, return;
		// 2. If P's cache is empty, return;		
		// 3. Select a random neighbor (named Q) from P's cache to initiate the shuffling;
		//	  - You should use the simulator's common random source to produce a random number: CommonState.r.nextInt(cache.size())
		// 4. If P's cache is full, remove Q from the cache;
		// 5. Select a subset of other l - 1 random neighbors from P's cache;
		//	  - l is the length of the shuffle exchange
		//    - Do not add Q to this subset	
		// 6. Add P to the subset;
		// 7. Send a shuffle request to Q containing the subset;
		//	  - Keep track of the nodes sent to Q
		//	  - Example code for sending a message:
		//
		// GossipMessage message = new GossipMessage(node, subset);
		// message.setType(MessageType.SHUFFLE_REQUEST);
		// Transport tr = (Transport) node.getProtocol(tid);
		// tr.send(node, Q.getNode(), message, protocolID);
		//
		// 8. From this point on P is waiting for Q's response and will not initiate a new shuffle operation;
		//
		// The response from Q will be handled by the method processEvent.

		/*Initialize variables*/
		List<Entry> subset = new ArrayList<Entry>(l); 
		Entry ent;
//		if(node.getID() == 0) {
//		System.out.println("Got 0 ");
//		System.exit(1);
//		}
		/*Check the state of the node, if it is 2 which is DOWN then the node is not accessible and probably waiting for a response*/
		//if(node.getFailState() == 2) return;

		/*Check if it is waiting for a shuffling request*/
		if(waiting){
			//System.out.println("Node " + node.getID() + " is waiting for a shuffling reply , cache size " + degree());
			return;
		}
		/*Check if the cache is empty*/
		if(cache.isEmpty()){
			System.out.println("Node " + node.getID() + " has no neighbor");
			return;				
		}
		if(contains(node)) {
			System.out.println("Why does this node has itself inside1?");
			System.exit(1);
		}
		
		
		/*Using the random number, pick a random neighbor*/
		Entry Q = new Entry(getNeighbor(getRandom()));
		/*If the cache is full, then remove Q from the list could test if Q is removed from list, no need to test remove since Q was taken from the list*/
		if(degree() == size) cache.remove(Q);
		//System.out.println("Node this: " + node.getID() + " Node random: " + Q.getNode().getID());

		/*
		 * Fill the subset with node until the shuffle length are met
		 * or if the cache size is smaller than l fill it up with nodes until the cache size of neighbors are met
		 */
		int stop = degree() < l-1 ? degree(): l - 1;
		/*If the cache size is smaller than l-1 and if it contains Q then the subset will be smaller by 1 since we wouldn't want duplicates in our subset*/
		if(degree() <= l-1 && cache.contains(Q))stop--;
		//if(contains(node))stop--;
		while(subset.size() < stop){
		//	System.out.println("HERE " + degree() + " l " + l + " stop " + stop + " subset " + subset.size());
		//	System.out.println("cache size " + degree());
			ent = new Entry(getNeighbor(getRandom()));
			/*Make sure the new entry doesn't exist in the subset and it isn't entry Q(the random node Q)*/
			//System.out.println("stop " + stop + " subset " + subset.size());
			if(!subset.contains(ent) && !ent.equals(Q)){
				subset.add(ent);
			}
		}
		/*Remove Q from subset and the cache if it exist*/
		//if(subset.contains(Q)) subset.remove(Q);
		//if(contains(Q.getNode())) cache.remove(Q);
		/*add node to the subset*/
		subset.add(new Entry(node));
		
		int index = 0;
		/*Keep track of which Node's are being sent to*/
		for(int i=0;i<subset.size();i++) {
			ent = subset.get(i);
			if((index = cache.indexOf(ent)) >= 0){
				cache.get(index).setSentTo(Q.getNode());
				//System.out.println("Sent to " + cache.get(index).getSentTo().getID());
			}
		}
//		if(i == 1){
//		if(subset.get(i).equals(subset.get(i-1))){
//		System.out.println("Duplicate");
//		for(int a = 0; a < subset.size();a++) System.out.println("Node " + subset.get(i).getNode().getID());
//		System.exit(1);
//		}
//		}


		// 7. Send a shuffle request to Q containing the subset;
		//	  - Keep track of the nodes sent to Q
		//	  - Example code for sending a message:
		//
		// GossipMessage message = new GossipMessage(node, subset);
		// message.setType(MessageType.SHUFFLE_REQUEST);
		// Transport tr = (Transport) node.getProtocol(tid);
		// tr.send(node, Q.getNode(), message, protocolID);
		//
		// 8. From this point on P is waiting for Q's response and will not initiate a new shuffle operation;
		//
		// The response from Q will be handled by the method processEvent.

		GossipMessage gossipMsg = new GossipMessage(node, subset);
		gossipMsg.setType(MessageType.SHUFFLE_REQUEST);
		Transport tr = (Transport) node.getProtocol(tid);
		tr.send(node, Q.getNode(), gossipMsg, protocolID);
		//System.out.println("subset size " + subset.size());

		/*set boolean to true, thus saying this node is waiting for a shuffling request*/
		waiting = true;
	}

	/* The simulator engine calls the method processEvent at the specific time unit that an event occurs in the simulation.
	 * It is not called periodically as the nextCycle method.
	 * 
	 * You should implement the handling of the messages received by this node in this method.
	 * 
	 * @see peersim.edsim.EDProtocol#processEvent(peersim.core.Node, int, java.lang.Object)
	 */
	@Override
	public void processEvent(Node node, int pid, Object event) {

		/*Initialize variables*/
		List<Entry> subset = new ArrayList<Entry>(l), tmp;
		subset.clear();
		Entry ent/*, intern, extern*/;
		// Let's name this node as Q;
		// Q receives a message from P;
		//	  - Cast the event object to a message:
		GossipMessage message = (GossipMessage) event;

		//	System.out.println("Node " + node.getID() + "Message Node: " + message.getNode().getID());
//		if(message.getShuffleList().size() < 2) {
//		System.out.println("Subset receive size: " + message.getShuffleList().size());
//		System.exit(1);
//		}
		if(degree() == 0 && node.getID() != 0){
			System.err.println("Inside 2 " + node.getID() + " Another node ? " + message.getShuffleList().get(0).getNode().getID());
			System.exit(1);
		}
		
		switch (message.getType()) {
		// If the message is a shuffle request:
		case SHUFFLE_REQUEST:
			//	  1. If Q is waiting for a response from a shuffling initiated in a previous cycle, send back to P a message rejecting the shuffle request; 
			//	  2. Q selects a random subset of size l of its own neighbors; 
			//	  3. Q reply P's shuffle request by sending back its own subset;
			//	  4. Q updates its cache to include the neighbors sent by P:
			//		 - No neighbor appears twice in the cache
			//		 - Use empty cache slots to add the new entries
			//		 - If the cache is full, you can replace entries among the ones sent to P with the new ones

			if(waiting) {
				//System.out.println("SHUFFLE");
				/*If the node is waiting for a shuffling request initiated before, then it will send back nothing but a message SHUFFLE_REJECTED*/
				GossipMessage gossipMsg = new GossipMessage(node, null);
				gossipMsg.setType(MessageType.SHUFFLE_REJECTED);
				Transport tr = (Transport) node.getProtocol(tid);
				tr.send(node, message.getNode(), gossipMsg, pid);
				return;
			} else {
				/*
				 * Fill the subset with node until the shuffle length are met
				 * or if the cache size is smaller than l fill it up with nodes until the cache size of neighbors are met
				 */
				int stop = degree() < l ? degree() : l;
				if(degree() <= l && contains(message.getNode())) stop--;
				if(contains(node)) {
					System.out.println("Why does this node has itself inside?");
					System.exit(1);
					stop--;
				}
				
				while(subset.size() < stop){
					//System.out.println(subset.size());
					ent = new Entry(getNeighbor(getRandom()));
					/*Make sure the new entry doesn't exist in the subset*/
					if(!subset.contains(ent) && !ent.getNode().equals(message.getNode())){
						subset.add(ent);
					}
				}
				//System.out.println("subset size " + subset.size());
			}
			//System.out.println(subset.size());
			int index = 0;
			/*Keep track of which Node's are being sent to*/
			for(int i=0;i<subset.size();i++) {
				ent = subset.get(i);
				if((index = cache.indexOf(ent)) >= 0){
					cache.get(index).setSentTo(message.getNode());
					//System.out.println("Sent to " + cache.get(index).getSentTo().getID());
				}
			}
			
			/*Subset of its neighbors has been made, send it back to the one who requested the shuffle*/
			GossipMessage gossipMsg = new GossipMessage(node, subset);
			gossipMsg.setType(MessageType.SHUFFLE_REPLY);
			Transport tr = (Transport) node.getProtocol(tid);
			tr.send(node, message.getNode(), gossipMsg, pid);

			
//			  4. Q updates its cache to include the neighbors sent by P:
			//		 - No neighbor appears twice in the cache
			//		 - Use empty cache slots to add the new entries
			//		 - If the cache is full, you can replace entries among the ones sent to P with the new ones
			
			/*Update our cache with the nodes we received from the one who sended the shuffle request*/
			tmp = message.getShuffleList();
			int counter = 0;
			while(counter < tmp.size()){
				/*If the cache already contains the node or is the same node as this node, ignore it*/
				if(contains(tmp.get(counter).getNode()) || node.getID() == tmp.get(counter).getNode().getID()){
					counter++;
				} else {
					/*If there are still space left in the cache, add the new node to it*/
					if(degree() < size) {
						cache.add(tmp.get(counter));
					}
					else {
						/*Cache is full, remove nodes that was sended and add new fresh ones*/
						for(int i = 0; i < cache.size(); i++){
							//System.out.println("cache size " + degree() + "node " + cache.get(i).get);
							if(cache.get(i).getSentTo() != null && cache.get(i).getSentTo().equals(message.getNode())){
								cache.remove(i);
								cache.add(tmp.get(counter));
								break;
							}
						}
					}
					counter++;
				}
			}
			
			
			
//			while(0 < tmp.size()){
//				/*Try and add a new neighbor, if it is successfull then remove the node from the temporary list*/
//				if(addNeighbor(tmp.get(tmp.size()-1).getNode())){
//					tmp.remove(tmp.size()-1);
//				} else {
//					/*If we get here then there are either one of the 2 reasons or both of them, it already contains the node, the cache is full*/
//					//If the cache already contains the node then remove it from the list
//					if(contains(tmp.get(tmp.size()-1).getNode())) tmp.remove(tmp.get(tmp.size()-1));
//					else {
//						//The cache is full, make some space
//						for(int i = 0; i < degree();i++){
//							intern = new Entry(getNeighbor(i));
//							for(int y = 0; y < subset.size(); y++){
//								extern = new Entry(subset.get(y).getNode());
//								if(intern.equals(extern)){
//									cache.remove(intern);
//									break;
//								}
//							}
//						}
//						if(addNeighbor(tmp.get(tmp.size()-1).getNode())) tmp.remove(tmp.size()-1);
//						else {
//							System.out.println("IN: SHUFFLE_REQUEST Should never come to this!! Write some debug to see the cache, node etc");
//							System.exit(1);
//						}
//					}
//				}
//			}
			
//			if(waiting || cache.size() == 0){
//				System.out.println("waiting " + waiting);
//				System.out.println("cache " + cache.size());
//				System.out.println("empty!! " + message.getShuffleList().size());
//				System.out.println("Sometimes!!!!!!!");
//				System.exit(1);
//				GossipMessage gossipMsg = new GossipMessage(node, subset);
//				gossipMsg.setType(MessageType.SHUFFLE_REJECTED);
//				Transport tr = (Transport) node.getProtocol(tid);
//				tr.send(node, message.getNode(), gossipMsg, pid);
//				return;
//			}
//			/*counting till we've filled up the subset*/
//			count = 0; 
//			while(l > count){
//				/*Don't have enough to fill up the whole subset*/
//				if(cache.size() == count) break;
//				ent = new Entry(cache.get(CommonState.r.nextInt(cache.size())).getNode());
//				if(subset.isEmpty()) subset.add(ent);
//				else {
//					if(subset.contains(ent)){
//						while(subset.contains(ent)){
//							ent = new Entry(cache.get(CommonState.r.nextInt(cache.size())).getNode());
//							System.err.println("Something went wrong2! " + count + " size " + cache.size() + " subset s " + subset.size() + " random " + CommonState.r.nextInt(cache.size()));
//							//if(random == 1) System.exit(1);
//							
//							if(CommonState.r.nextInt(cache.size()) == 1){
//								System.out.println("Subset " + subset.get(0).getNode().getID());
//								//System.out.println("Subset sent to " + subset.get(0).getSentTo().getID());
//								for(int i = 0;i < cache.size();i++){
//									System.out.println("Node " + cache.get(i).getNode().getID() + " i " + i);
//								}
//								System.exit(1);
//							}
//						}
//					}
//					subset.add(ent);
//				}
//				count++;
//			}
//			
//			for(int i=0;i<subset.size();i++){
//				subset.get(i).setSentTo(message.getNode());
//				if(i > 0){
//					if(subset.get(i).equals(subset.get(i-1))){
//						System.out.println("Duplicate");
//						System.exit(1);
//					}
//				}
//			}
//			
//			GossipMessage gossipMsg = new GossipMessage(node, subset);
//			gossipMsg.setType(MessageType.SHUFFLE_REPLY);
//			Transport tr = (Transport) node.getProtocol(tid);
//			tr.send(node, message.getNode(), gossipMsg, pid);
//		
//			if(gossipMsg.getShuffleList().size() == 0 || gossipMsg.getShuffleList().size() == 1) 
//			{
//				System.out.println("waiting " + waiting);
//				System.out.println("count " + count);
//				System.out.println("cache " + cache.size());
//				System.out.println("empty!! " + gossipMsg.getShuffleList().size());
//				System.exit(1);
//			}
//				
			break;
		
		// If the message is a shuffle reply:
		case SHUFFLE_REPLY:
		//	  1. In this case Q initiated a shuffle with P and is receiving a response containing a subset of P's neighbors
		//	  2. Q updates its cache to include the neighbors sent by P:
		//		 - No neighbor appears twice in the cache
		//		 - Use empty cache slots to add new entries
		//		 - If the cache is full, you can replace entries among the ones originally sent to P with the new ones
		//	  3. Q is no longer waiting for a shuffle reply;	 
			/*Got a reply from a peer*/
			//System.out.println("SHUFFLE REPLY" + message.getShuffleList().size());

			tmp = message.getShuffleList();
			counter = 0;
			while(counter < tmp.size()){
				/*If the cache already contains the node or is the same node as this node, ignore it*/
				if(contains(tmp.get(counter).getNode()) || node.getID() == tmp.get(counter).getNode().getID()){
					counter++;
				} else {
					/*If there are still space left in the cache, add the new node to it*/
					if(degree() < size){
						//if(!addNeighbor(tmp.get(counter).getNode())) System.out.println("Something wrong in SHUFFLE_REPLY 1");		
						cache.add(tmp.get(counter));
					}
					else {
						/*Cache is full, remove nodes that was sended and add new fresh ones*/
						for(int i = 0; i < cache.size(); i++){
							if(cache.get(i).getSentTo() != null && cache.get(i).getSentTo().equals(message.getNode())){
								cache.remove(i);
								//if(!addNeighbor(tmp.get(counter).getNode())) System.out.println("Something wrong in SHUFFLE_REQUEST");
								cache.add(tmp.get(counter));
								break;
							}
						}
					}
					counter++;
				}
			}
//			for(int i = 0;i < degree(); i++){
//				if(tmp.contains(cache.get(i))){
//					System.out.println("YA, WE REMOVED SOMETHING i " + i + " size " + tmp.size());
//					cache.remove(i);
//				}
//			}
			
			
//			while(0 < tmp.size()){
//				/*Try and add a new neighbor, if it is successfull then remove the node from the temporary list*/
//				if(addNeighbor(tmp.get(tmp.size()-1).getNode())){
//					tmp.remove(tmp.size()-1);
//				} else {
//					/*If we get here then there are either one of the 2 reasons or both of them, it already contains the node, the cache is full*/
//					//If the cache already contains the node then remove it from the list
//					if(contains(tmp.get(tmp.size()-1).getNode())) tmp.remove(tmp.get(tmp.size()-1));
//					else {
//						//The cache is full, make some space
//
//						for(int i = 0; i < degree();i++){
//							intern = new Entry(getNeighbor(i));
//							for(int y = 0; y < subset.size(); y++){
//								extern = new Entry(subset.get(y).getNode());
//								if(intern.equals(extern)){
//									System.out.println("I CAN REMOVE!!!!!!!!!!!");
//									cache.remove(intern);
//									break;
//								}
//							}
//						}
//
//						if(addNeighbor(tmp.get(tmp.size()-1).getNode())) tmp.remove(tmp.size()-1);
//						else {
//							System.out.println("IN: SHUFFLE_REPLY Should never come to this!! Write some debug to see the cache, node etc");
//							System.out.println("tmp size " + tmp.size() + " cache " + cache.size());
//							System.exit(1);
//						}
//					}
//				}
//			}	
			/*This node is no longer waiting for a shuffle reply*/
			waiting = false;
			break;
		
		// If the message is a shuffle rejection:
		case SHUFFLE_REJECTED:
		//	  1. If P was originally removed from Q's cache, add it again to the cache.
		//	  2. Q is no longer waiting for a shuffle reply;
			//System.out.println("SHUFFLE REJECTED");
			/*We shouldn't have to worry about cache being full since we removed P from Q's cache, so it will 
			 * always have 1 free space left when we come here
			 * */
			
			if(!contains(message.getNode())) cache.add(new Entry(message.getNode())); //if(!addNeighbor(message.getNode())) System.out.println("SOMETHING WENT WRONG IN SHUFLE_REJECT");

			/*No longer waiting for a shuffle reply*/
			waiting = false;
			break;
		default:
			break;
		}
		
	}
	
/* The following methods are used only by the simulator and don't need to be changed */
	
	/*Return a random number*/
	private int getRandom(){
		return CommonState.r.nextInt(degree());
	}
		
	@Override
	public int degree() {
		return cache.size();
	}

	@Override
	public Node getNeighbor(int i) {
		return cache.get(i).getNode();
	}

	@Override
	public boolean addNeighbor(Node neighbour) {
		if (contains(neighbour))
			return false;

		if (cache.size() >= size)
			return false;

		Entry entry = new Entry(neighbour);
		cache.add(entry);

		return true;
	}

	@Override
	public boolean contains(Node neighbor) {
		return cache.contains(new Entry(neighbor));
	}

	public Object clone()
	{
		BasicShuffle gossip = null;
		try { 
			gossip = (BasicShuffle) super.clone(); 
		} catch( CloneNotSupportedException e ) {
			
		} 
		gossip.cache = new ArrayList<Entry>();

		return gossip;
	}

	@Override
	public void onKill() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void pack() {
		// TODO Auto-generated method stub	
	}
}
