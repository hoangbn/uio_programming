package Client;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import Quiz.Alternative;
import Quiz.AlternativeHelper;
import Quiz.CompleteQuestion;
import Quiz.CompleteQuestionHelper;
import Quiz.Question;
import Quiz.QuestionHelper;
import Quiz.QuizServer;
import Quiz.QuizServerHelper;
import Quiz.QuizServerOperations;
import Quiz.QuizServerPOA;

public class Client {
	
	static QuizServer quiz;
	
	public static void main(String[] args) {
		try{
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);
		
			// get the root naming context
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			// Use NamingContextExt instead of NamingContext. This is 
			// part of the Interoperable naming Service.  
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			
			String name = "Quiz";
			quiz = QuizServerHelper.narrow(ncRef.resolve_str(name));
			
		} catch (Exception e){
			System.out.println("Error: " + e);
			e.printStackTrace(System.out);
		}
	}
}

