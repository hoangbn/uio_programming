#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
  /* page directory/table entry bits (PMSA p.235 and p.240) */
  PE_P = 1 << 0, /* present */
  PE_RW = 1 << 1,  /* read/write */
  PE_US = 1 << 2,  /* user/supervisor */
  PE_PWT = 1 << 3, /* page write-through */
  PE_PCD = 1 << 4, /* page cache disable */
  PE_A = 1 << 5, /* accessed */
  PE_D = 1 << 6, /* dirty */
};

int test_bit(int code, int bit)
{
  int test = code & bit;
  if(test == bit){
    return 1;
  } else {
    return 0;
  }
}

void set_bit(int *vaddr, uint which_bit)
{
  *vaddr |= which_bit;
  // *vaddr = set;
}


int main()
{ 
  printf("********Bit setting*********\n");
  int present = 0;
  set_bit(&present, PE_P);
  if(test_bit(present, PE_P)){
    printf("Success\n");
  } else {
    printf("Failed\n");
  }
    return;
}
