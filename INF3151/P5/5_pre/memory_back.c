/*
 * memory.c
 * Note: 
 * There is no separate swap area. When a data page is swapped out, 
 * it is stored in the location it was loaded from in the process' 
 * image. This means it's impossible to start two processes from the 
 * same image without screwing up the running. It also means the 
 * disk image is read once. And that we cannot use the program disk.
 *
 * Best viewed with tabs set to 4 spaces.
 */

#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "memory.h"
#include "thread.h"
#include "util.h"
#include "interrupt.h"
#include "tlb.h"
#include "usb/scsi.h"

struct page_map_t {
  pcb_t *who;
  uint32_t vaddr;
  uint32_t *paddr;
  bool_t is_pinned;
};

static uint32_t mem_ptr;
static uint32_t *kernel_page_directory = NULL;
static uint32_t *kernel_page_table = NULL;
static lock_t mem_lock;
static int count = 0;
static struct page_map_t page_map[PAGEABLE_PAGES];

/* Use virtual address to get index in page directory.  */
inline uint32_t get_directory_index(uint32_t vaddr)
{
  return (vaddr & PAGE_DIRECTORY_MASK) >> PAGE_DIRECTORY_BITS;
}

/*
 * Use virtual address to get index in a page table.  The bits are
 * masked, so we essentially get a modulo 1024 index.  The selection
 * of which page table to index into is done with
 * get_directory_index().
 */
inline uint32_t get_table_index(uint32_t vaddr)
{
  return (vaddr & PAGE_TABLE_MASK) >> PAGE_TABLE_BITS;
}

/*
 * init_memory()
 *   
 * called once by _start() in kernel.c
 * You need to set up the virtual memory map for the kernel here.
 */
void init_memory(void)
{
  mem_ptr = MEM_START;
  lock_init(&mem_lock);
  srand((uint32_t) get_timer());
  
  int i;
  for(i = 0; i < PAGEABLE_PAGES; i++){
    page_map[i].paddr = alloc_memory();
    page_map[i].is_pinned = FALSE;
  }

  kernel_page_directory = page_map[alloc_page(TRUE)].paddr;
  
   make_common_map(kernel_page_directory, FALSE);
}

/*
 * Sets up a page directory and page table for a new process or thread. 
 */
void setup_page_table(pcb_t * p)
{
  lock_acquire(&mem_lock);
  if(p->is_thread){
    p->page_directory = kernel_page_directory;
    lock_release(&mem_lock);
  } else {
    uint32_t *process_directory, *process_stack_table, stack_table_entry1, stack_table_entry2, *process_table, max_proc_addr; 
    int pagenm;

    /*Allocate a process page directory and pin it*/   
    pagenm = alloc_page(TRUE);
    process_directory = page_map[pagenm].paddr;
    page_map[pagenm].who = p;

    /* /\* Zero out the directory entries and set the bits*\/ */
    /* int i; */
    /* for(i = 0; i < PAGE_N_ENTRIES; i++) process_directory[i] = 0 | PE_RW | PE_US; */

    /*Map kernel and video memory*/
    
    //make_common_map(process_directory, TRUE);

    /* int offset; */
    /* for(offset = 0; offset < p->swap_size * 512; offset += PAGE_SIZE) */
    /* { */
    /*     paddr = p->start_pc + offset; */
    /*     vaddr = PROCESS_START + offset; */
    /*     table_map_present(process_stack_table, vaddr, paddr, 1); */
    /* } */
    /* int i; */
    /* for(i = 0; i < p->swap_size * SECTOR_SIZE;i += PAGE_SIZE){ */
      
    /* } */

    /*allocate stack table insert it in the directory*/   
    process_stack_table = page_map[pagenm].paddr;
    page_map[pagenm].who = p;
    
  

    /*allocate 2 stack pages and insert then in the table and pin them all*/ 
    pagenm = alloc_page(TRUE);
    stack_table_entry1 = (uint32_t)page_map[pagenm].paddr;
    table_map_present(process_stack_table, PROCESS_STACK, stack_table_entry1, 1);

    pagenm = alloc_page(TRUE);
    stack_table_entry2 = (uint32_t)page_map[pagenm].paddr;  
    table_map_present(process_stack_table, PROCESS_STACK, stack_table_entry2, 1);
    
    //put stack table in directory
    directory_insert_table(process_directory, PROCESS_STACK, process_stack_table, 1);
    
    //put kernel table in directory
    directory_insert_table(process_directory, (uint32_t)kernel_page_table, kernel_page_table, 1);

    //setup process page table
    pagenm = alloc_page(TRUE);
    process_table = page_map[pagenm].paddr;
    page_map[pagenm].who = p;

    max_proc_addr = p->user_stack + p->swap_size * SECTOR_SIZE;
    //map pages in table
    uint32_t addr;
    for(addr = p->user_stack; addr < max_proc_addr; addr += PAGE_SIZE){
      table_map_present(process_table, addr, 0, 1);
    }

    directory_insert_table(process_directory, p->user_stack, process_table, 1);

    /*Set directory in the pcb*/
    p->page_directory = process_directory;

    lock_release(&mem_lock);
  }
}




 
int calculate_location(pcb_t *p)
{
  int location = 0;
  uint32_t temp = p->start_pc;
  while(temp < p->fault_addr){
    location++;
    temp += PAGE_SIZE;
  } 
  return location;
}

int calculate_size(int size, int index)
{
  if(index < (size/SECTORS_PER_PAGE)) return SECTORS_PER_PAGE;
  else return size % SECTORS_PER_PAGE;
}

/*
 * called by exception_14 in interrupt.c (the faulting address is in
 * current_running->fault_addr)
 *
 * Interrupts are on when calling this function.
 */
void page_fault_handler(void)
{
  lock_acquire(&mem_lock);
   
 /* PE_P = 1 << 0, /\* present *\/ */
 /*  PE_RW = 1 << 1,  /\* read/write *\/ */
 /*  PE_US = 1 << 2,  /\* user/supervisor *\/ */
 /*  PE_PWT = 1 << 3, /\* page write-through *\/ */ 
  /*PE_PWT will be used as a replacement for testing on RSVD bit which reside at the same place in error code format*/
  if(test_bit(PE_P)) rsprintf("1");
  else  rsprintf("0"); 
    
  
  if(test_bit(PE_RW)) rsprintf("1");
  else  rsprintf("0");
  if(test_bit(PE_US))    rsprintf("1");
  else rsprintf("0");
  if(test_bit(PE_PWT))  rsprintf("1");
  else rsprintf("0\n");

  /*If bit 0 is clear it means the page is not present in memory*/
  uint32_t *page_table, dir_index, tab_index, *convert;
  int page;
  
  current_running->page_fault_count++;
  
  dir_index = get_directory_index(current_running->fault_addr);
  tab_index = get_table_index(current_running->fault_addr);
  rsprintf("dir index %d, Index from page fault %d\n", dir_index,  tab_index);
  page = alloc_page(FALSE);
  
  page_table = (uint32_t *)current_running->page_directory[dir_index] & PE_BASE_ADDR_MASK;
  table_map_present(page_table, current_running->fault_addr, (uint32_t) page_map[page].paddr, 1);
  
  swap_in(page);  
  
  page_map[page].who = current_running;
  convert = (uint32_t*) dir_index;
  *convert &= ~PE_P;
  
  
  flush_tlb_entry(current_running->fault_addr);
  lock_release(&mem_lock);
  return;
}

uint32_t *alloc_memory()
{
  lock_acquire(&mem_lock);
  uint32_t *page = (uint32_t *)mem_ptr;
  mem_ptr += PAGE_SIZE;
  
  if ((mem_ptr & 0xfff) != 0) {
    /* align next_free_mem to page boundary */
    mem_ptr= (mem_ptr & 0xfffff000) + 0x1000;
  }
  
  int i;
  for(i = 0; i < 1024; page[i++] = 0);

  lock_release(&mem_lock);
  return page;
}

int alloc_page(int bool)
{
  int pagenm;
  if(count < PAGEABLE_PAGES){
    pagenm = count;
    count++;
  } else {
    pagenm = page_replacement_policy();
  }
  if(bool) page_map[pagenm].is_pinned = TRUE;
  return pagenm;
}

void clear_page()
{
  

}

/*Read from USB to memory
 *
 *
 *
 */
void swap_in(int pagenm)
{
  int placement = calculate_location(current_running);
  int location = current_running->swap_loc + (placement * SECTORS_PER_PAGE);
  int size = calculate_size(current_running->swap_size, placement);

  scsi_read(location, size, (char *) page_map[pagenm].paddr);
  page_map[pagenm].vaddr = current_running->fault_addr;
  set_bit(&page_map[pagenm].vaddr, PE_P);
}

/*Read from memory and store it on USB
 *
 *
 *
 */
void swap_out(int pagenm)
{
  int placement = calculate_location(page_map[pagenm].who);
  int location = current_running->swap_loc + (placement * SECTORS_PER_PAGE);
  int size = calculate_size(current_running->swap_size, placement);

  scsi_write(location, size, (char *) page_map[pagenm].paddr);
  page_map[pagenm].vaddr = current_running->fault_addr;
  set_bit(&page_map[pagenm].vaddr, PE_P);
}



int page_replacement_policy(void)
{
  uint32_t *dir, *tab, *page;
  int random;
  while(TRUE){
    random = rand() % PAGEABLE_PAGES;
    /*Find a page which is not pinned to swap out or overwrite*/
    if(!page_map[random].is_pinned){
      /*If the pages has been modified then make a backup by storing it in USB*/
      if(test_bit(PE_D)) swap_out(random);
      dir = page_map[random].who->page_directory;
      tab = (uint32_t *)dir[get_directory_index(page_map[random].vaddr)];
      page = (uint32_t *)dir[get_table_index(page_map[random].vaddr)];
      *page &= ~PE_P;
      
      return random;
    }
  }
}


int test_bit(uint32_t which_bit)
{ 
  uint32_t test = current_running->error_code;
  test &= which_bit;
  if(test) return TRUE;
  else return FALSE;
}

void set_bit(uint32_t *vaddr, uint32_t which_bit)
{
  *vaddr |= which_bit;
}

/*Auxiliery methods taken from P4 it some modification*/
inline void directory_insert_table(uint32_t * directory, uint32_t vaddr,
      	   uint32_t * table, int user)
{
  int access = PE_RW | PE_P, index = get_directory_index(vaddr);
  if(user) access |= PE_US;
  rsprintf("Index from dir %d\n", index);
  directory[index] = ((uint32_t) table & PE_BASE_ADDR_MASK) | access;
}

inline void table_map_present(uint32_t * table, uint32_t vaddr,
            uint32_t paddr, int user)
{
  int access = PE_P | PE_RW, index = get_table_index(vaddr);
  if (user) access |= PE_US;
  //rsprintf("Index from table %d\n", index);
  table[index] = (paddr & PE_BASE_ADDR_MASK) | access;
}

void make_common_map(uint32_t *page_directory, int user)
{
  uint32_t  addr;
  int page_table = alloc_page(TRUE);
  
  
  for (addr = 0; addr < MEM_START; addr += PAGE_SIZE)
    table_map_present(page_map[page_table].paddr, addr, addr, 0);
  
  /* Identity map the video memory, from 0xb8000-0xb8fff. */
  table_map_present(page_map[page_table].paddr, (uint32_t) SCREEN_ADDR,
		    (uint32_t) SCREEN_ADDR, user);
  
  /*
   * Identity map in the rest of the physical memory so the
   * kernel can access everything in memory directly.
   */
    for (addr = MEM_START; addr < MAX_PHYSICAL_MEMORY; addr += PAGE_SIZE) 
      table_map_present(page_map[page_table].paddr, addr, addr, TRUE);  
  
  /*
   * Insert in page_directory an entry for virtual address 0
   * that points to physical address of page_table.
   */
  directory_insert_table(page_directory, 0, page_map[page_table].paddr, TRUE);
  kernel_page_table = page_map[page_table].paddr;
}


