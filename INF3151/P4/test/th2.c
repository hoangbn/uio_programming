#include "common.h"
#include "th.h"
#include "thread.h"
#include "util.h"
#include "scheduler.h"

#define RUNNING 0
#define OK 1
#define FAILED 2

static void print_trace(int thread, int status);

static int shared_var = 0;
static int exit_count = 0;
static int init = FALSE;
static lock_t l;
static condition_t c0mod3, cnot0mod3;

void thread2(void)
{
    int tmp, i;

    lock_init(&l);
    condition_init(&c0mod3);
    condition_init(&cnot0mod3);
    init = TRUE;
    for (i = 0; i < 200; i++) {
	lock_acquire(&l);
	while (shared_var % 3 == 0) {
	    condition_wait(&l, &cnot0mod3);
	}
	ASSERT(shared_var % 3 != 0);
	tmp = shared_var;
	if (i % 13 == 0) {
	    yield();
	}
	shared_var = tmp + 1;
	if (shared_var % 3 == 0) {
	    if (shared_var < 20) {
		condition_signal(&c0mod3);
	    } else {
		condition_broadcast(&c0mod3);
	    }
	}
	print_trace(2, RUNNING);
	lock_release(&l);
    }
    lock_acquire(&l);
    exit_count++;
    if (exit_count == 2) {
	print_trace(2, (shared_var == 300) ? OK : FAILED);
    }
    lock_release(&l);
    exit();
}

void thread3(void)
{
    int tmp, i;

    while (!init) {
	yield();
    }
    for (i = 0; i < 100; i++) {
	lock_acquire(&l);
	while (shared_var % 3 != 0) {
	    condition_wait(&l, &c0mod3);
	}
	ASSERT(shared_var % 3 == 0);
	tmp = shared_var;
	if (i % 17 == 0) {
	    yield();
	}
	shared_var = tmp + 1;
	if (shared_var % 3 != 0) {
	    if (shared_var < 20) {
		condition_signal(&cnot0mod3);
	    } else {
		condition_broadcast(&cnot0mod3);
	    }
	}
	print_trace(3, RUNNING);
	lock_release(&l);
    }
    lock_acquire(&l);
    exit_count++;
    if (exit_count == 2) {
	print_trace(3, (shared_var == 300) ? OK : FAILED);
    }
    lock_release(&l);
    exit();
}

#define LINE 10

static void print_trace(int thread, int status)
{
    print_str(LINE - 1, 0, "Thread 2");
    print_str(LINE - 1, 10, "Thread 3");
    print_str(LINE - 1, 20, "Result");
    if (status == RUNNING) {
	if (thread == 2) {
	    print_int(LINE, 0, shared_var);
	} else {
	    print_int(LINE, 10, shared_var);
	}
	print_str(LINE, 20, "Running");
    } else if (status == OK) {
	print_str(LINE, 20, "Passed ");
    } else {
	print_str(LINE, 20, "Failed ");
    }
    delay(2000000);
}
