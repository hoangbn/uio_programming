#include "common.h"
#include "util.h"
#include "thread.h"
#include "scheduler.h"

#define LOCKED 1
#define UNLOCKED 0

/*
 * spinlock functions
 */

static int test_and_set(int *addr)
{
    int val = LOCKED;

    asm volatile ("xchg (%%eax), %%ebx":"=b"
		  (val): "a"(addr),"0"(val));

    return val;
}

void spinlock_init(int *s)
{
    *s = UNLOCKED;
}

void spinlock_acquire(int *s)
{
    while (test_and_set(s) == LOCKED) {
	yield();
    }
}

void spinlock_release(int *s)
{
    ASSERT2(*s == LOCKED, "spinlock should be locked");
    *s = UNLOCKED;
}

/*
 * lock functions
 */

void lock_init(lock_t *l)
{
    spinlock_init(&l->spinlock);
    l->status = UNLOCKED;
    l->waiting = NULL;
}

void lock_acquire(lock_t *l)
{
    spinlock_acquire(&l->spinlock);
    if (l->status == UNLOCKED) {
	l->status = LOCKED;
	spinlock_release(&l->spinlock);
	return;
    } else {
	block(&l->waiting, &l->spinlock);
    }
}

void lock_release(lock_t *l)
{
    spinlock_acquire(&l->spinlock);
    if (l->waiting == NULL) {
	l->status = UNLOCKED;
    } else {
	unblock(&l->waiting);
    }
    spinlock_release(&l->spinlock);
}

/*
 * condition functions
 */

void condition_init(condition_t *c)
{
    spinlock_init(&c->spinlock);
    c->waiting = NULL;
}

void condition_wait(lock_t *m, condition_t *c)
{
    spinlock_acquire(&c->spinlock);
    lock_release(m);
    block(&c->waiting, &c->spinlock);
    lock_acquire(m);
}

void condition_signal(condition_t *c)
{
    spinlock_acquire(&c->spinlock);
    if (c->waiting != NULL) {
	unblock(&c->waiting);
    }
    spinlock_release(&c->spinlock);
}

void condition_broadcast(condition_t *c)
{
    spinlock_acquire(&c->spinlock);
    while (c->waiting != NULL) {
	unblock(&c->waiting);
    }
    spinlock_release(&c->spinlock);
}
