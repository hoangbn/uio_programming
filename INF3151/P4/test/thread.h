#ifndef THREAD_INCLUDED
#define THREAD_INCLUDED

void spinlock_init(int *s);
void spinlock_acquire(int *s);
void spinlock_release(int *s);

typedef struct {
    int spinlock;
    struct pcb_t *waiting;
    int status;
} lock_t;

void lock_init(lock_t *);
void lock_acquire(lock_t *);
void lock_release(lock_t *);

typedef struct {
    int spinlock;
    struct pcb_t *waiting;
} condition_t;

void condition_init(condition_t *c);
void condition_wait(lock_t *m, condition_t *c);
void condition_signal(condition_t *c);
void condition_broadcast(condition_t *c);

#endif
