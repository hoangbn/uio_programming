#ifndef KEYBOARD_INCLUDED
#define KEYBOARD_INCLUDED

#define KEYBUFFER	16
#define	RIGHT_SHIFT	1
#define	LEFT_SHIFT	2
#define CAPS_SHIFT      4
#define	CONTROL		8
#define	ALT		16

struct ascii {
    unsigned char no_shift;
    unsigned char shift;
    unsigned char control;
    void (*handler) (unsigned char);
};

struct character {
    unsigned char character;
    unsigned char attribute;
    unsigned char scancode;
};

void keyboard_init(void);
void keyboard_interrupt(void);
int getchar(int *c);

#endif
