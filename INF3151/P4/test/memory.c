#include "common.h"
#include "floppy.h"
#include "kernel.h"
#include "scheduler.h"
#include "memory.h"
#include "thread.h"
#include "util.h"


/* physical page facts */
#define	PAGE_SIZE		4096
#define PAGE_N_ENTRIES		(PAGE_SIZE / sizeof(uint_t))
#define SECTORS_PER_PAGE	(PAGE_SIZE / SECTOR_SIZE)

/* page directory/table entry bits */
#define PE_P			1	/* present */
#define PE_RW			2	/* read/write */
#define PE_US			4	/* user/supervisor */
#define PE_PWT			8	/* page write-through */
#define PE_PCD			16	/* page cache disable */
#define PE_A			32	/* accessed */
#define PE_D			64	/* dirty */
#define PE_BASE_ADDR_MASK	0xfffff000	/* extracts the base address */

/* facts about a linear address */
#define	VADDR_PDI_BITS	22		/* position of page dir index */
#define VADDR_PTI_BITS	12		/* position of page table index */
#define	VADDR_PDI_MASK	0xffc00000	/* extracts page dir index */
#define VADDR_PTI_MASK	0x003ff000	/* extracts page table index */
#define VADDR_PDI(a)	(((a) & VADDR_PDI_MASK) >> VADDR_PDI_BITS)
#define VADDR_PTI(a)	(((a) & VADDR_PTI_MASK) >> VADDR_PTI_BITS)

/* constants to simulate a very small physical memory */
#define MEM_START		0x100000
#define PAGEABLE_PAGES		30
#define MAX_PHYSICAL_MEMORY	(MEM_START + PAGEABLE_PAGES * PAGE_SIZE)
#define N_KERNEL_PTS		1

/* structure of an entry in the page map */
struct page_map_entry_t {
    struct pcb_t *owner;        /* process that owns this page */
    uint_t vaddr;           /* page-aligned virtual address of
                     * this page */
    uint_t *entry;          /* entry that points to this page */
    bool_t pinned;          /* is this page pinned? */
};

/*
 * local functions
 */

/* page_alloc allocates a page.  If necessary, it swaps a page out.
 * On success, it returns the index of the page in the page map.  On
 * failure, it aborts. */
static int page_alloc(void);

/* page_replacement_policy returns the index in the page map of a page
 * to be swapped out */
static int page_replacement_policy(void);

/* swap the i-th page in */
static void page_swap_in(int pageno);

/* swap the i-th page out */
static void page_swap_out(int pageno);

/*
 * local variables
 */

/* the page map */
static struct page_map_entry_t page_map[PAGEABLE_PAGES];

/* address of the kernel page directory (shared by all kernel threads) */
static uint_t *kernel_pdir;


/*
 * exported functions
 */

/* init_memory is called once by _start() in kernel.c */
void init_memory(void)
{
/* Add your code here */
}


/* Called by create_process() and create_thread() in kernel.c */
void setup_page_table(struct pcb_t *p)
{
/*  Add your code here */
}


/* called by exception_14 in interrupt.c (the faulting address is in
 * current_running->fault_addr) */
void page_fault_handler(void)
{
/*  Add your code here */
}


/*
 * implementation of local auxiliary functions
 */

static int page_alloc(void)
{
/* Add your code here */
}

static void page_swap_in(int pageno)
{
/* Add your code here */
}

static void page_swap_out(int pageno)
{
    struct page_map_entry_t *page = &page_map[pageno];


   /*  Whenever the contents of a page table entry is altered, the
     * program must explicitly instruct the TLBs to discard the
     * affected page table entry from the TLBs.  This is accomplished
     * by execution of the INVLPG (invalidate page) instruction. */

    asm volatile ("invlpg %0"::"m" (page->vaddr));

/*   Add your code here   */

}

static int page_replacement_policy(void)
{
/* Add your code here */
}

/* Add other auxiliary functions here */

