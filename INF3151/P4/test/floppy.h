#ifndef FLOPPY_INCLUDED
#define FLOPPY_INCLUDED

#include "common.h"

void floppy_init(void);
void floppy_start_motor(void);
void floppy_stop_motor(void);
int floppy_read(int block, ulong_t address);
int floppy_write(int block, ulong_t address);

extern volatile int floppy_irq;

#endif
