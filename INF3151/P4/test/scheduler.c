#include "interrupt.h"
#include "kernel.h"
#include "scheduler.h"
#include "thread.h"
#include "util.h"

#define INIT_EFLAGS ((3 << 12) | (1 << 9))

static int eflags = INIT_EFLAGS;

static void scheduler(void);

static uint_t eax, ebx, ecx, edx, edi, esi, ebp;

void yield(void)
{
    CRITICAL_SECTION_BEGIN;
    current_running->yield_count++;
    scheduler();
    CRITICAL_SECTION_END;
}

static void scheduler(void)
{
    /* Save the context of the thread that will be scheduled out  */
    asm volatile ("movl %%eax, %0":"=m" (eax));
    asm volatile ("movl %%ebx, %0":"=m" (ebx));
    asm volatile ("movl %%ecx, %0":"=m" (ecx));
    asm volatile ("movl %%edx, %0":"=m" (edx));
    asm volatile ("movl %%edi, %0":"=m" (edi));
    asm volatile ("movl %%esi, %0":"=m" (esi));
    asm volatile ("movl %%ebp, %0":"=m" (ebp));

    ASSERT(current_running->disable_count != 0);

    current_running->eax = eax;
    current_running->ebx = ebx;
    current_running->ecx = ecx;
    current_running->edx = edx;
    current_running->edi = edi;
    current_running->esi = esi;
    current_running->ebp = ebp;

    SAVE_STACK(current_running->kernel_stack);
    SAVE_MASK;

    /* Pick which process/thread we wish to run next */
    current_running = current_running->next;

    /* .. and run it */
    dispatch();
}

void setup_current_running(void)
{
    RESTORE_MASK;
    SETUP_PAGE_TABLE;
    RESET_TIMER;

    /* Setup the kernel stack */
    tss.esp_0 = (uint32_t) current_running->base_kernel_stack;
    tss.ss_0 = KERNEL_DS;
}

void dispatch(void)
{
    setup_current_running();

    if (current_running->first_time) {
	/* Special case for the first time we run a process/thread.
	 * Basically the first time we simply jump to the process */
	current_running->first_time = FALSE;

	RESTORE_STACK(current_running->kernel_stack);
	if (!current_running->in_kernel) {
	    PUSH(current_running->ds);	/* SS */
	    PUSH(current_running->user_stack);
	}
	PUSH(eflags);
	PUSH(current_running->cs);
	PUSH(current_running->start_pc);
	CRITICAL_SECTION_END;
	load_data_segments(current_running->ds);
	RETURN_FROM_INTERRUPT;
    }
    /* Restore the current thread */
    RESTORE_STACK(current_running->kernel_stack);

    ASSERT(current_running->disable_count != 0);
    eax = current_running->eax;
    ebx = current_running->ebx;
    ecx = current_running->ecx;
    edx = current_running->edx;
    esi = current_running->esi;
    edi = current_running->edi;
    ebp = current_running->ebp;

    asm volatile ("movl %0, %%eax"::"m" (eax));
    asm volatile ("movl %0, %%ebx"::"m" (ebx));
    asm volatile ("movl %0, %%ecx"::"m" (ecx));
    asm volatile ("movl %0, %%edx"::"m" (edx));
    asm volatile ("movl %0, %%esi"::"m" (esi));
    asm volatile ("movl %0, %%edi"::"m" (edi));
    asm volatile ("movl %0, %%ebp"::"m" (ebp));
}

void exit(void)
{
    /* Remove the current_running process from the linked list so it
     * will not be scheduled in the future */

    CRITICAL_SECTION_BEGIN;

    current_running->status = EXITED;

    /* If no more processes left to run then loop forever */
    if (current_running->next == current_running) {
	HALT("No more process to run!");
    }
    current_running->previous->next = current_running->next;
    current_running->next->previous = current_running->previous;

    current_running = current_running->next;
    dispatch();
}

void block(struct pcb_t **q, int *spinlock)
{
    struct pcb_t *tmp;

    CRITICAL_SECTION_BEGIN;

    spinlock_release(spinlock);

    current_running->status = BLOCKED;

    /* If no more processes left to run then loop forever */
    if (current_running->next == current_running)
	while (1);

    current_running->previous->next = current_running->next;
    current_running->next->previous = current_running->previous;

    tmp = *q;
    (*q) = current_running;
    current_running->next_blocked = tmp;

    scheduler();

    CRITICAL_SECTION_END;
}

void unblock(struct pcb_t **q)
{
    struct pcb_t *new, *tmp;

    CRITICAL_SECTION_BEGIN;
    ASSERT((*q) != NULL);

    if ((*q)->next_blocked == NULL) {
	new = (*q);
	(*q) = NULL;
    } else {
	for (tmp = *q;
	    tmp->next_blocked->next_blocked != NULL;
	    tmp = tmp->next_blocked);
	new = tmp->next_blocked;
	tmp->next_blocked = NULL;
    }

    new->status = RUNNING;
    /* Add the process to active process queue */
    new->next = current_running->next;
    new->previous = current_running;
    current_running->next->previous = new;
    current_running->next = new;
    CRITICAL_SECTION_END;
}

int getpid(void)
{
    return current_running->pid;
}

int getpriority(void)
{
    return current_running->priority;
}

void setpriority(int p)
{
    current_running->priority = p;
}
