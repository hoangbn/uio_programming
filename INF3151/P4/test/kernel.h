#ifndef KERNEL_INCLUDED
#define KERNEL_INCLUDED

#include "common.h"

#define KERNEL_CODE	1
#define KERNEL_DATA     2
#define PROCESS_CODE    3
#define PROCESS_DATA    4
#define TSS_INDEX       5

#define PROCESS_STACK  	0xfffffff0

#define	KERNEL_CS       (KERNEL_CODE << 3)
#define	KERNEL_DS       (KERNEL_DATA << 3)
#define	PROCESS_CS      (PROCESS_CODE << 3)
#define	PROCESS_DS      (PROCESS_DATA << 3)
#define	KERNEL_TSS      (TSS_INDEX << 3)

#define CODE_SEGMENT	0x0a
#define DATA_SEGMENT	0x02
#define TSS_SEGMENT    	0x09
#define MEMORY 		1
#define SYSTEM 		0
#define TSS_SIZE	103

#define PREEMPT_TICKS   11932		/* 100 Hz */

#define RESET_TIMER \
    do { \
	outb(0x40, (uchar_t) PREEMPT_TICKS); \
	outb(0x40, (uchar_t) (PREEMPT_TICKS >> 8)); \
    } while (0)

struct pcb_t {
    unsigned int pid;
    unsigned int in_kernel;
    unsigned int user_stack;
    unsigned int kernel_stack;
    unsigned int base_kernel_stack;
    unsigned int stack_mark;
    unsigned int nested_count;
    unsigned int start_pc;
    unsigned int ds;
    unsigned int cs;
    unsigned int fault_addr;
    unsigned int swap_loc;
    unsigned int swap_size;
    unsigned int first_time;
    unsigned int priority;
    unsigned int status;
    unsigned int disable_count;
    unsigned int preempt_count;
    unsigned int page_fault_count;
    unsigned int yield_count;
    unsigned int int_controller_mask;
    struct pcb_t *next_blocked;
    uint32_t *root_page_table;
    struct pcb_t *next;
    struct pcb_t *previous;
    unsigned int eax, ebx, ecx, edx, edi, esi, ebp;
    unsigned int error_code;
};

struct gate_t {
    uint16_t offset_low;
    uint16_t selector;
    uint8_t count;
    uint8_t access;
    uint16_t offset_high;
} __attribute__((packed));

struct segment_t {
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t base_mid;
    uint8_t access;
    uint8_t limit_high;
    uint8_t base_high;
} __attribute((packed));

struct point_t {
    uint16_t limit;
    uint32_t base;
} __attribute__((packed));

struct tss_t {
    uint32_t backlink;
    uint32_t esp_0;
    uint16_t ss_0;
    uint16_t pad0;
    uint32_t esp_1;
    uint16_t ss_1;
    uint16_t pad1;
    uint32_t esp_2;
    uint16_t ss_2;
    uint16_t pad2;
    uint32_t reserved;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint16_t es;
    uint16_t pad3;
    uint16_t cs;
    uint16_t pad4;
    uint16_t ss;
    uint16_t pad5;
    uint16_t ds;
    uint16_t pad6;
    uint16_t fs;
    uint16_t pad7;
    uint16_t gs;
    uint16_t pad8;
    uint16_t ldt_selector;
    uint16_t pad9;
    uint16_t debug_trap;
    uint16_t iomap_base;
} __attribute((packed));

typedef int (*syscall_t) ();

void print_status(int time);

extern syscall_t syscall[SYSCALL_COUNT];
extern struct pcb_t pcb[];
extern struct pcb_t *current_running;
extern struct tss_t tss;

#endif
