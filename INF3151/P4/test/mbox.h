#ifndef MSG_INCLUDED
#define MSG_INCLUDED

void mbox_init(void);
int mbox_open(int key);
int mbox_close(int q);
int mbox_stat(int q, int *count, int *space);
int mbox_recv(int q, msg_t *m);
int mbox_send(int q, msg_t *m);
void print_mbox_status(void);

#endif
