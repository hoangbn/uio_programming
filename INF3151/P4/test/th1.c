#include "kernel.h"
#include "scheduler.h"
#include "th.h"
#include "mbox.h"
#include "util.h"

void loader_thread(void)
{
    unsigned char buf[SECTOR_SIZE];
    struct directory_t *dir = (struct directory_t *) buf;

    readdir(buf);
    if (dir->location != 0) {
	loadproc(dir->location, dir->size);
    }
    exit();
}

void clock_thread(void)
{
    unsigned int time;
    unsigned long long int ticks;

    while (1) {
	ticks = get_timer() >> 20;
	time = ((int) ticks) / 166;
	print_status(time);
	// print_mbox_status();
	yield();
    }
}
