#ifndef COMMON_INCLUDED
#define COMMON_INCLUDED

#ifndef NULL
#define NULL 0
#endif

#define SECTOR_SIZE 512

#define SCREEN_ADDR ((short *) 0xb8000)

enum {
    SYSCALL_YIELD,
    SYSCALL_EXIT,
    SYSCALL_GETPID,
    SYSCALL_GETPRIORITY,
    SYSCALL_SETPRIORITY,
    SYSCALL_MBOX_OPEN,
    SYSCALL_MBOX_CLOSE,
    SYSCALL_MBOX_STAT,
    SYSCALL_MBOX_RECV,
    SYSCALL_MBOX_SEND,
    SYSCALL_GETCHAR,
    SYSCALL_READDIR,
    SYSCALL_LOADPROC,
    SYSCALL_COUNT
};

#define ASSERT2(p, s) \
    do { \
	if (!(p)) { \
	    print_str(0, 0, "Assertion failure: "); \
	    print_str(0, 19, s); \
	    print_str(1, 0, "file: "); \
	    print_str(1, 6, __FILE__); \
	    print_str(2, 0, "line: "); \
	    print_int(2, 6, __LINE__); \
	    asm volatile ("cli"); \
	    while (1); \
	} \
    } while(0)

#define ASSERT(p) ASSERT2(p, #p)

#define HALT(s) ASSERT2(FALSE, s)

typedef enum {
    FALSE, TRUE
} bool_t;

typedef signed char int8_t;
typedef short int int16_t;
typedef int int32_t;
typedef long long int int64_t;

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long int uint64_t;

typedef unsigned char uchar_t;
typedef unsigned int uint_t;
typedef unsigned long ulong_t;

typedef struct {
    int size;
    char body[0];
} msg_t;

struct directory_t {
    int location;
    int size;
};

#endif
