#include "common.h"
#include "syslib.h"
#include "util.h"

#define QUEUE 2
#define FLOW_OUT 3
#define FLOW_IN 4
#define MAX_MSG_SIZE 256
#define ACTUAL_MSG_SIZE(n) ((n) + sizeof(int))

static char space[ACTUAL_MSG_SIZE(MAX_MSG_SIZE)];
static msg_t *m;
static msg_t token;

#define LINE 4

void _start(void)
{
    int q, fi, fo, size, c, i, j, wait = FALSE;

    if ((q = mbox_open(QUEUE)) < 0) {
	exit();
    }
    if ((fi = mbox_open(FLOW_IN)) < 0) {
	exit();
    }
    if ((fo = mbox_open(FLOW_OUT)) < 0) {
	exit();
    }
    m = (msg_t *) &space;
    for (i = 0; i < 1000000; i++) {
	print_str(LINE, 15, "Process 4");
	print_int(LINE + 1, 15, i);
	size = 128 + i % 128;
	c = 'a' + i % 26;

	if (i < 5000) {
	    if (wait) {
		mbox_recv(fi, &token);
		ASSERT2(token.size == 0, "Invalid control mesg in process 4");
		wait = FALSE;
	    } else {
		int count, space;

		mbox_stat(q, &count, &space);
		if (count == 0) {
		    token.size = 0;
		    mbox_send(fo, &token);
		    wait = TRUE;
		}
	    }
	} else if (i == 5000) {
	    mbox_send(fo, &token);
	}
	mbox_recv(q, m);

	ASSERT2(m->size == size, "Invalid size in process 4");
	for (j = 0; j < size; j++) {
	    ASSERT2(m->body[j] == c, "Invalid data in process 4");
	}
    }

    print_str(LINE, 15, "Process 4");
    print_str(LINE + 1, 15, "Done");
    mbox_close(q);
    mbox_close(fi);
    mbox_close(fo);
    exit();
}
