#include "common.h"
#include "syslib.h"
#include "util.h"

#define COMMAND_MBOX 1
#define MINX 30
#define SIZEX 50
#define MINY 0
#define SIZEY 7
#define MAXX (MINX + SIZEX)
#define MAXY (MINY + SIZEY)
#define RETURN 13

static void shell_print_char(int y, int x, int c);
static void read_line(int max);
static void write_int(int n);
static void write_char(int c);
static void write_str(char *s);
static void parse_line(void);
static void usage(char *s);

static int cursor_x = MINX, cursor_y = MINY;
static char line[SIZEX + 1];
static char *argv[SIZEX];
static int argc;

void _start(void)
{
    int q, p, n;
    unsigned char buf[SECTOR_SIZE];
    struct directory_t *dir;

    write_str("COS 318 Shell Version 0.000001");
    write_char(RETURN);
    write_char(RETURN);

    q = mbox_open(COMMAND_MBOX);

    while (1) {
	write_str("# ");
	read_line(SIZEX - 2);
	parse_line();
	if (argc == 0) {
	    continue;
	}

	if (same_string("clear", argv[0])) {
	    if (argc == 1) {
		clear_screen(MINX, MINY, MAXX, MAXY);
		cursor_x = MINX;
		cursor_y = MINY;
	    } else {
		usage("");
	    }
	} else if (same_string("fire", argv[0])) {
	    if (argc == 1) {
		if (q >= 0) {
		    msg_t m;

		    m.size = 0;
		    mbox_send(q, &m);
		} else {
		    write_str("Mailboxes not available");
		    write_char(RETURN);
		}
	    } else {
		usage("");
	    }
	} else if (same_string("exit", argv[0])) {
	    if (argc == 1) {
		write_str("Goodbye");
		exit();
	    } else {
		usage("");
	    }
	} else if (same_string("ls", argv[0])) {
	    readdir(buf);
	    dir = (struct directory_t *) buf;

	    p = 0;
	    while (dir->location != 0) {
		write_str("process ");
		write_int(p++);
		write_str(" - location: ");
		write_int(dir->location);
		write_str(", size: ");
		write_int(dir->size);
		write_char(RETURN);
		dir++;
	    }

	    if (p == 0) {
		write_str("Process not found.");
	    } else {
		write_int(p);
		write_str(" process(es).");
	    }
	    write_char(RETURN);
	} else if (same_string("load", argv[0])) {
	    readdir(buf);
	    dir = (struct directory_t *) buf;
	    n = atoi(argv[1]);
	    for (p = 0; p < n && dir->location != 0; p++, dir++);

	    if (dir->location != 0) {
		loadproc(dir->location, dir->size);
		write_str("Done.");
	    } else {
		write_str("File not found.");
	    }
	    write_char(RETURN);
	} else if (same_string("ps", argv[0])) {
	    write_str(argv[0]);
	    write_str(" : Command not implemented.");
	    write_char(RETURN);
	} else if (same_string("kill", argv[0])) {
	    write_str(argv[0]);
	    write_str(" : Command not implemented.");
	    write_char(RETURN);
	} else if (same_string("suspend", argv[0])) {
	    write_str(argv[0]);
	    write_str(" : Command not implemented.");
	    write_char(RETURN);
	} else if (same_string("resume", argv[0])) {
	    write_str(argv[0]);
	    write_str(" : Command not implemented.");
	    write_char(RETURN);
	} else {
	    write_str(argv[0]);
	    write_str(" : Command not found.");
	    write_char(RETURN);
	}
    }
}

static void usage(char *s)
{
    write_str("Usage : ");
    write_str(argv[0]);
    write_str(s);
    write_char(RETURN);
}

static void parse_line(void)
{
    char *s = line;

    argc = 0;
    while (1) {
	while (*s == ' ') {
	    *s = 0;
	    s++;
	}
	if (*s == 0) {
	    return;
	} else {
	    argv[argc++] = s;
	    while ((*s != ' ') && (*s != 0))
		s++;
	}
    }
}

static void write_int(int n)
{
    char str[10];

    itoa(n, str);
    write_str(str);
}

static void write_char(int c)
{
    if (c == RETURN) {
	cursor_x = MINX;
	cursor_y++;
    } else {
	shell_print_char(cursor_y, cursor_x, c);
	cursor_x++;
    }

    if (cursor_y >= MAXY) {
	scroll(MINX, MINY, MAXX, MAXY);
	cursor_y--;
    }
}

static void write_str(char *s)
{
    while (*s != 0) {
	write_char(*s);
	s++;
    }
}

static void read_line(int max)
{
    int i = 0, c;

    ASSERT2(max <= SIZEX, "Request too long");
    do {
	getchar(&c);
	if ((i < max) && (c != RETURN)) {
	    line[i++] = c;
	    write_char(c);
	}
    } while (c != RETURN);
    line[i] = 0;
    write_char(RETURN);
}

static void shell_print_char(int y, int x, int c)
{
    if (((x) >= MINX) && ((x) < MAXX) && ((y) >= MINY) && ((y) < MAXY)) {
	print_char(y, x, c);
    }
}
