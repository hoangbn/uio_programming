#include "common.h"
#include "floppy.h"
#include "interrupt.h"
#include "kernel.h"
#include "keyboard.h"
#include "mbox.h"
#include "memory.h"
#include "scheduler.h"
#include "th.h"
#include "util.h"

#define NUM_THREADS 4

#define PCB_TABLE_SIZE 128

#define STACK_MIN	0x20000
#define STACK_MAX	0x40000
#define STACK_OFFSET	0x0ffc
#define STACK_SIZE	0x1000

#define GDT_SIZE        7
#define IDT_SIZE        49
#define IRQ_START       32
#define INTERRUPT_GATE  0x0e
#define IDT_SYSCALL_POS	48

/* paging is enabled by setting CR0[31] to one */
#define ENABLE_PAGING \
    asm volatile("movl	%cr0,%eax		\n\t" \
		 "orl	$0x80000000,%eax	\n\t" \
		 "movl	%eax,%cr0		\n\t")

#define DISABLE_INTERRUPTS	asm volatile ("cli")
#define ENABLE_INTERRUPTS	asm volatile ("sti")

typedef void (*handler_t) (void);

static void create_gate(struct gate_t *entry, uint32_t offset,
    uint16_t selector, char type, char privilege, char count);
static void create_segment(struct segment_t *entry, uint32_t base,
    uint32_t limit, char type, char privilege, char seg_type);
static void init_syscall(int i, syscall_t call);
static void init_idt(void);
static void init_gdt(void);
static void init_tss(void);
static void init_pcb_table(void);
static int create_thread(int i);
static int create_process(uint32_t location, uint32_t size);
static struct pcb_t *alloc_pcb();
static void free_pcb(struct pcb_t *p);
static void insert_pcb(struct pcb_t *p);

syscall_t syscall[SYSCALL_COUNT];

struct pcb_t pcb[PCB_TABLE_SIZE];
struct pcb_t *current_running;

static struct gate_t idt[IDT_SIZE];
static struct segment_t gdt[GDT_SIZE];
struct tss_t tss;

static struct pcb_t *next_free_pcb;
static int next_pid = 0;
static int next_stack = STACK_MIN;

static unsigned int start_addr[NUM_THREADS] =
{
    (unsigned int) loader_thread,
    (unsigned int) clock_thread,
    (unsigned int) thread2,
    (unsigned int) thread3
};

static handler_t exception_handler[NUM_EXCEPTIONS] =
{
    exception_0, exception_1, exception_2, exception_3,
    exception_4, exception_5, exception_6, exception_7,
    exception_8, exception_9, exception_10, exception_11,
    exception_12, exception_13, exception_14
};

void _start(void)
{
    int i;

    clear_screen(0, 0, 80, 25);

    init_syscall(SYSCALL_YIELD, (syscall_t) yield);
    init_syscall(SYSCALL_EXIT, (syscall_t) exit);
    init_syscall(SYSCALL_GETPID, (syscall_t) getpid);
    init_syscall(SYSCALL_GETPRIORITY, (syscall_t) getpriority);
    init_syscall(SYSCALL_SETPRIORITY, (syscall_t) setpriority);
    init_syscall(SYSCALL_MBOX_OPEN, (syscall_t) mbox_open);
    init_syscall(SYSCALL_MBOX_CLOSE, (syscall_t) mbox_close);
    init_syscall(SYSCALL_MBOX_STAT, (syscall_t) mbox_stat);
    init_syscall(SYSCALL_MBOX_RECV, (syscall_t) mbox_recv);
    init_syscall(SYSCALL_MBOX_SEND, (syscall_t) mbox_send);
    init_syscall(SYSCALL_GETCHAR, (syscall_t) getchar);
    init_syscall(SYSCALL_READDIR, (syscall_t) readdir);
    init_syscall(SYSCALL_LOADPROC, (syscall_t) loadproc);

    init_idt();
    init_gdt();
    init_tss();
    init_pcb_table();

    init_memory();
    floppy_init();
    mbox_init();
    keyboard_init();

    for (i = 0; i < NUM_THREADS; i++) {
	create_thread(i);
    }

    SETUP_PAGE_TABLE;
    ENABLE_PAGING;

    dispatch();

    while (1);				/*NOTREACHED */
}

static void create_gate(struct gate_t *entry, uint32_t offset,
    uint16_t selector, char type, char privilege, char count)
{
    /* General function make a gate entry.  Refer to chapter 12, page
     * 203, in PMSA for a description of interrupt gates */
    entry->offset_low = (uint16_t) offset;
    entry->selector = (uint16_t) selector;
    entry->count = (uint8_t) count & 0x1f;
    entry->access = type | privilege << 5 | 1 << 7;
    entry->offset_high = (uint16_t) (offset >> 16);
}

static void create_segment(struct segment_t *entry, uint32_t base,
    uint32_t limit, char type, char privilege, char seg_type)
{
    entry->limit_low = (uint16_t) limit;
    entry->limit_high = (uint8_t) (limit >> 16);
    entry->base_low = (uint16_t) base;
    entry->base_mid = (uint8_t) (base >> 16);
    entry->base_high = (uint8_t) (base >> 24);
    entry->access = type | seg_type << 4 | privilege << 5 | 1 << 7;
    entry->limit_high |= 1 << 7 | 1 << 6;
}

static void init_syscall(int i, syscall_t call)
{
    ASSERT((i >= 0) && (i < SYSCALL_COUNT));
    ASSERT(call != NULL);
    syscall[i] = call;
}

static void init_idt(void)
{
    int i;
    struct point_t idt_p;

    /* Rearrange irq vector mapping so that we have no overlap with
     * exception vectors. Refer to chapter 17 Undocumented PC. */
    outb(0x20, 0x11);
    outb(0xa0, 0x11);
    outb(0x21, 0x20);
    outb(0xa1, 0x28);
    outb(0x21, 0x04);
    outb(0xa1, 0x02);
    outb(0x21, 0x01);
    outb(0xa1, 0x01);
    outb(0xa1, 0xff);
    outb(0x21, 0xfb);

    /* Set timer frequency */
    RESET_TIMER;

    /* Create default handlers for interrupts/exceptions */
    for (i = 0; i < IDT_SIZE; i++) {
	create_gate(&(idt[i]), (uint32_t) bogus_interrupt, KERNEL_CS,
	    INTERRUPT_GATE, 0, 0);
    }

    /* Create handlers for some exceptions */
    for (i = 0; i < NUM_EXCEPTIONS; i++) {
	create_gate(&(idt[i]), (uint32_t) exception_handler[i], KERNEL_CS,
	    INTERRUPT_GATE, 0, 0);
    }

    /* Create gate for int 39  */
    create_gate(&(idt[IRQ_START + 7]), (uint32_t) fake_irq7, KERNEL_CS,
	INTERRUPT_GATE, 0, 0);

    /* Create gate for the timer interrupt */
    create_gate(&(idt[IRQ_START]), (uint32_t) irq0, KERNEL_CS,
	INTERRUPT_GATE, 0, 0);

    /* Create gate for the keyboard interrupt */
    create_gate(&(idt[IRQ_START + 1]), (uint32_t) irq1, KERNEL_CS,
	INTERRUPT_GATE, 0, 0);

    /* Create gate for the floppy interrupt */
    create_gate(&(idt[IRQ_START + 6]), (uint32_t) irq6, KERNEL_CS,
	INTERRUPT_GATE, 0, 0);

    /* Create gate for system calls */
    create_gate(&(idt[IDT_SYSCALL_POS]), (uint32_t) system_call_entry,
	KERNEL_CS, INTERRUPT_GATE, 3, 0);

    /* Load the idtr with a pointer to our idt */
    idt_p.limit = (IDT_SIZE * 8) - 1;
    idt_p.base = (uint32_t) idt;

  asm("lidt %0": :"m"(idt_p));
}

static void init_gdt(void)
{
    struct point_t gdt_p;

    create_segment(gdt + KERNEL_CODE, 0, 0xfffff, CODE_SEGMENT, 0, MEMORY);
    create_segment(gdt + KERNEL_DATA, 0, 0xfffff, DATA_SEGMENT, 0, MEMORY);
    create_segment(gdt + PROCESS_CODE, 0, 0xfffff, CODE_SEGMENT, 3, MEMORY);
    create_segment(gdt + PROCESS_DATA, 0, 0xfffff, DATA_SEGMENT, 3, MEMORY);
    create_segment(gdt + TSS_INDEX, (uint32_t) &tss, TSS_SIZE, TSS_SEGMENT, 0,
	SYSTEM);

    /* Load the GDTR register with a pointer to the gdt */
    gdt_p.limit = (GDT_SIZE * 8) - 1;
    gdt_p.base = (uint32_t) gdt;

    asm volatile ("lgdt %0"::"m" (gdt_p));

    /* Reload the Segment registers to refresh the hidden portions */
    asm volatile ("pushl %ds");
    asm volatile ("popl %ds");
    asm volatile ("pushl %es");
    asm volatile ("popl %es");
    asm volatile ("pushl %ss");
    asm volatile ("popl %ss");
}

static void init_tss(void)
{
    uint16_t tss_p = KERNEL_TSS;

    tss.esp_0 = 0;
    tss.ss_0 = 0;
    tss.ldt_selector = 0;
    tss.backlink = KERNEL_TSS;
    tss.iomap_base = sizeof(struct tss_t);

    asm volatile ("ltr %0"::"m" (tss_p));
}

static void init_pcb_table()
{
    int i;

    /* link all the pcbs together in the next_free_pcb list */
    for (i = 0; i < PCB_TABLE_SIZE - 1; i++) {
	pcb[i].next = &pcb[i + 1];
    }
    pcb[PCB_TABLE_SIZE - 1].next = NULL;

    next_free_pcb = pcb;
    /* current_running also serves as the ready queue pointer */
    current_running = NULL;
}

static int create_thread(int i)
{
    struct pcb_t *p = alloc_pcb();

    p->pid = next_pid++;
    p->in_kernel = TRUE;

    ASSERT2(next_stack < STACK_MAX, "Out of stack space");
    p->kernel_stack = p->base_kernel_stack = next_stack + STACK_OFFSET;
    next_stack += STACK_SIZE;

    p->stack_mark = 0;
    p->first_time = TRUE;
    p->priority = 10;
    p->status = RUNNING;
    p->nested_count = 0;
    p->disable_count = 1;
    p->preempt_count = 0;
    p->page_fault_count = 0;
    p->yield_count = 0;
    p->int_controller_mask = 0xbc;	/* Enable keyboard & timer */

    p->user_stack = 0;
    p->start_pc = start_addr[i];
    p->cs = KERNEL_CS;
    p->ds = KERNEL_DS;

    p->swap_loc = 0;
    p->swap_size = 0;
    setup_page_table(p);
    insert_pcb(p);
    return 0;
}

static int create_process(uint32_t location, uint32_t size)
{
    struct pcb_t *p = alloc_pcb();

    p->pid = next_pid++;
    p->in_kernel = FALSE;

    ASSERT2(next_stack < STACK_MAX, "Out of stack space");
    p->kernel_stack = p->base_kernel_stack = next_stack + STACK_OFFSET;
    next_stack += STACK_SIZE;

    p->stack_mark = 0;
    p->first_time = TRUE;
    p->priority = 10;
    p->status = RUNNING;
    p->nested_count = 0;
    p->disable_count = 1;
    p->preempt_count = 0;
    p->page_fault_count = 0;
    p->yield_count = 0;
    p->int_controller_mask = 0xbc;	/* Enable keyboard & timer */

    p->user_stack = PROCESS_STACK;
    p->start_pc = PROCESS_START;
    p->cs = PROCESS_CS | 3;
    p->ds = PROCESS_DS | 3;

    p->swap_loc = location;
    p->swap_size = size;
    setup_page_table(p);
    insert_pcb(p);
    return 0;
}

static struct pcb_t *alloc_pcb()
{
    struct pcb_t *p;

    ASSERT2(next_free_pcb, "Running out of free PCB!");

    DISABLE_INTERRUPTS;		/* can't use CRITICAL_SECTION_BEGIN
				 * because current_running could be *
				 * undefined */
    p = next_free_pcb;
    next_free_pcb = p->next;
    ENABLE_INTERRUPTS;
    return p;
}

static void insert_pcb(struct pcb_t *p)
{
    DISABLE_INTERRUPTS;		/* can't use CRITICAL_SECTION_BEGIN
				 * because current_running could be *
				 * undefined */
    if (current_running == NULL) {
	current_running = p;
	p->next = p;
	p->previous = p;
    } else {
	p->next = current_running;
	p->previous = current_running->previous;
	p->next->previous = p;
	p->previous->next = p;
    }
    ENABLE_INTERRUPTS;
}

static void free_pcb(struct pcb_t *p)
{
    /* put the pcb back into the free list */
    /*  CRITICAL_SECTION_BEGIN; */
    p->next = next_free_pcb;
    next_free_pcb = p;
    /*  CRITICAL_SECTION_END; */
}

void print_status(int time)
{
    static char *status[] =
    {"Running", "Blocked", "Exited "};
    int i, base;
    struct pcb_t *p;

    base = 17;
    clear_screen(0, 17, 80, 22);

    print_str(base - 5, 12, "P R O C E S S    S T A T U S   after");
    print_int(base - 5, 50, time);
    print_str(base - 5, 55, "seconds");
    print_str(base - 3, 0, "Pid");
    print_str(base - 3, 5, "Type");
    print_str(base - 3, 15, "Status");
    print_str(base - 3, 25, "Disable");
    print_str(base - 2, 25, "count");
    print_str(base - 3, 35, "Preempt");
    print_str(base - 2, 35, "count");
    print_str(base - 3, 45, "Yield");
    print_str(base - 2, 45, "count");
    print_str(base - 3, 55, "Page");
    print_str(base - 2, 55, "faults");
    print_str(base - 3, 65, "Kernel");
    print_str(base - 2, 65, "stack");
    p = current_running;
    i = 0;
    do {
	print_int(base + i, 0, p->pid);
	print_str(base + i, 5, p->in_kernel ? "Thread" : "Process");
	print_str(base + i, 15, status[p->status]);
	print_int(base + i, 25, p->disable_count);
	print_int(base + i, 35, p->preempt_count);
	print_int(base + i, 45, p->yield_count);
	print_int(base + i, 55, p->page_fault_count);
	print_hex(base + i, 65, p->kernel_stack);
	p = p->next;
	i++;
    } while (p != current_running);
}

void readdir(unsigned char *buf)
{
    unsigned char internal_buf[SECTOR_SIZE];
    uint16_t os_size;

#define OS_SIZE_LOC 2
    /* read the boot block */
    floppy_start_motor();
    print_str(23, 0, "reading bootblock");
    floppy_read(0, (uint32_t) internal_buf);
    print_str(23, 0, "                 ");

    os_size = *((uint16_t *) (internal_buf + OS_SIZE_LOC));

    print_str(23, 0, "reading directory");
    /* now skip the kernel, and read the directory */
    floppy_read(os_size + 1, (uint32_t) internal_buf);
    print_str(23, 0, "                 ");
    floppy_stop_motor();
    /* we are done! */
    bcopy(internal_buf, buf, SECTOR_SIZE);
}

void loadproc(int location, int size)
{
    create_process(location, size);
}
