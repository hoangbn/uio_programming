#include "common.h"
#include "syslib.h"
#include "util.h"

#define QUEUE 2
#define FLOW_IN 3
#define FLOW_OUT 4
#define MAX_MSG_SIZE 256
#define ACTUAL_MSG_SIZE(n) ((n) + sizeof(int))

static char space[ACTUAL_MSG_SIZE(MAX_MSG_SIZE)];
static msg_t *m;
static msg_t token;

#define LINE 4

void _start()
{
    int q, fi, fo, size, c, i, j, wait = TRUE;

    if ((q = mbox_open(QUEUE)) < 0) {
	exit();
    }
    if ((fi = mbox_open(FLOW_IN)) < 0) {
	exit();
    }
    if ((fo = mbox_open(FLOW_OUT)) < 0) {
	exit();
    }
    m = (msg_t *) &space;
    for (i = 0; i < 1000000; i++) {
	print_str(LINE, 0, "Process 3");
	print_int(LINE + 1, 0, i);
	size = 128 + i % 128;
	c = 'a' + i % 26;

	m->size = size;
	for (j = 0; j < size; j++) {
	    m->body[j] = c;
	}

	if (i < 5000) {
	    if (wait) {
		mbox_recv(fi, &token);
		ASSERT2(token.size == 0, "Invalid control mesg in process 4");
		wait = FALSE;
	    } else {
		int count, space;

		mbox_stat(q, &count, &space);
		if (space < ACTUAL_MSG_SIZE(m->size)) {
		    token.size = 0;
		    mbox_send(fo, &token);
		    wait = TRUE;
		}
	    }
	} else if (i == 5000) {
	    mbox_send(fo, &token);
	}
	mbox_send(q, m);
    }

    print_str(LINE, 0, "Process 3");
    print_str(LINE + 1, 0, "Done");
    mbox_close(q);
    mbox_close(fi);
    mbox_close(fo);
    exit();
}
