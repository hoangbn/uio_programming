#ifndef MEMORY_INCLUDED
#define MEMORY_INCLUDED

void init_memory(void);
void setup_page_table(struct pcb_t *p);
void page_fault_handler(void);

#endif
