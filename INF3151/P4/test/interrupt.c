#include "common.h"
#include "floppy.h"
#include "interrupt.h"
#include "kernel.h"
#include "keyboard.h"
#include "memory.h"
#include "scheduler.h"
#include "util.h"

#define STACK_CONTENT(n) (*((int *) ((current_running->stack_mark) + (n))))
			
#define CALL_OFFSET	(7 * sizeof(long))
#define ARG1_OFFSET	(6 * sizeof(long))
#define ARG2_OFFSET	(5 * sizeof(long))
#define ARG3_OFFSET	(4 * sizeof(long))
#define RET_OFFSET	(7 * sizeof(long))

#define FN	STACK_CONTENT(CALL_OFFSET)
#define ARG1	STACK_CONTENT(ARG1_OFFSET)
#define ARG2	STACK_CONTENT(ARG2_OFFSET)
#define ARG3	STACK_CONTENT(ARG3_OFFSET)
#define RET	STACK_CONTENT(RET_OFFSET)

void load_data_segments(int seg)
{
    asm volatile ("movw %%ax, %%ds \n\t"
		  "movw %% ax, %%es " 
		  ::"a"(seg));
}

static void system_call_entry_helper(void)
{
    if (FN >= SYSCALL_COUNT) {
	FN = SYSCALL_EXIT;
    }

    /*
     * Since in C's calling convention, caller is responsible for
     * cleaning up the stack therefore we don't really need to
     * distinguish between different argument numbers. Just pass all 3
     * arguments and it will work
     */
    RET = syscall[FN](ARG1, ARG2, ARG3);
}

void system_call_entry(void)
{
    /* Save the context of the process */
    PUSH_ALL;
    SWITCH_DS;
    CRITICAL_SECTION_BEGIN;
    SAVE_STACK(current_running->stack_mark);
    ASSERT2(current_running->nested_count == 0, "Wrong nest count at A");
    current_running->nested_count++;
    CRITICAL_SECTION_END;

    system_call_entry_helper();

    /* Restore the user stack */
    CRITICAL_SECTION_BEGIN;
    current_running->nested_count--;
    ASSERT2(current_running->nested_count == 0, "Wrong nest count at B");
    CRITICAL_SECTION_END;
    RESTORE_DS;
    POP_ALL;
    RETURN_FROM_INTERRUPT;
}

#define HARDWARE_INTERRUPT_PRE(irq) \
    do { \
	PUSH_ALL; \
	SWITCH_DS; \
	CRITICAL_SECTION_BEGIN; \
	MASK_HARDWARE_INTERRUPT(irq); \
	current_running->nested_count++; \
	current_running->preempt_count++; \
	SEND_END_OF_INTERRUPT; \
	CRITICAL_SECTION_END; \
    } while (0)

#define HARDWARE_INTERRUPT_POST(irq) \
    do { \
	CRITICAL_SECTION_BEGIN; \
	current_running->nested_count--; \
	UNMASK_HARDWARE_INTERRUPT(irq); \
	DELAYED_CRITICAL_SECTION_END; \
	RESTORE_DS; \
	POP_ALL; \
	RETURN_FROM_INTERRUPT; \
    } while (0)

void irq0(void)
{
    HARDWARE_INTERRUPT_PRE(0);
    yield();
    HARDWARE_INTERRUPT_POST(0);
}

void irq1(void)
{
    HARDWARE_INTERRUPT_PRE(1);
    keyboard_interrupt();
    HARDWARE_INTERRUPT_POST(1);
}

void irq6_helper(void)
{
    floppy_irq = TRUE;
}

void irq6(void)
{
    HARDWARE_INTERRUPT_PRE(6);
    irq6_helper();
    HARDWARE_INTERRUPT_POST(6);
}

/* See Page 1007, Chapter 17, Warnings section in The Undocumented PC */

void fake_irq7(void)
{
#ifdef UNCOMMENT_CHECK
    static int fake_irq7_count = 0;
    static int iis_flag;

    /* Read the interrupt in service flags */
    PUSH_ALL;
    outb(0x20, 0x0b);
    iis_flag = inb(0x20);
    ASSERT2((iis_flag & (1 << 7)) == 0, "Real irq7 !");
    print_str(2, 0, "Fake irq7 count : ");
    print_int(2, 20, ++fake_irq7_count);
    POP_ALL;
#endif
    RETURN_FROM_INTERRUPT;
}

void exception_14(void)
{
    /* Save the context of the process */
    PUSH_ALL;
    SWITCH_DS;
    CRITICAL_SECTION_BEGIN;
    current_running->nested_count++;
    asm volatile ("movl %%cr2, %0":"=q" (current_running->fault_addr));

    CRITICAL_SECTION_END;

    page_fault_handler();

    /* Restore the user stack */
    CRITICAL_SECTION_BEGIN;
    current_running->nested_count--;
    CRITICAL_SECTION_END;
    RESTORE_DS;
    POP_ALL;
    POP_DISCARD;
    RETURN_FROM_INTERRUPT;
}

/* Exception handlers, currently they are all dummies. */

#define START_COL 40

#define PRINT_INFO(l, s, f, v) \
    do { \
	print_str(l, START_COL, "                                        "); \
	print_str(l, START_COL, s); \
	f(l, START_COL + 15, v); \
    } while (0)

#define INTERRUPT_HANDLER(name, str, error_code) \
void name(void) \
{ \
    int s[1]; \
    PUSH_ALL; \
    SWITCH_DS; \
    PRINT_INFO(0, "Error", print_str, (str)); \
    PRINT_INFO(1, "PID", print_int, current_running->pid); \
    PRINT_INFO(2, "Stack", print_hex, ((int) s)); \
    PRINT_INFO(3, "Preemptions", print_int, current_running->preempt_count); \
    PRINT_INFO(4, "Yields", print_int, current_running->yield_count); \
    PRINT_INFO(5, "IP", print_hex, error_code ? s[2] : s[1]); \
    PRINT_INFO(6, "CS", print_hex, error_code ? s[3] : s[2]); \
    PRINT_INFO(7, "nested count", print_int, current_running->nested_count); \
    PRINT_INFO(8, "Error code", print_hex, error_code ? s[1] : 0); \
    PRINT_INFO(9, "Hardware mask", print_hex, \
	current_running->int_controller_mask); \
    print_status(-1); \
    while (1); \
}

INTERRUPT_HANDLER(bogus_interrupt, "In bogus_interrupt", FALSE);
INTERRUPT_HANDLER(exception_0, "In exception 0", FALSE);
INTERRUPT_HANDLER(exception_1, "In exception 1", FALSE);
INTERRUPT_HANDLER(exception_2, "In exception 2", FALSE);
INTERRUPT_HANDLER(exception_3, "In exception 3", FALSE);
INTERRUPT_HANDLER(exception_4, "In exception 4", FALSE);
INTERRUPT_HANDLER(exception_5, "In exception 5", FALSE);
INTERRUPT_HANDLER(exception_6, "In exception 6", FALSE);
INTERRUPT_HANDLER(exception_7, "In exception 7", FALSE);
INTERRUPT_HANDLER(exception_8, "In exception 8", TRUE);
INTERRUPT_HANDLER(exception_9, "In exception 9", FALSE);
INTERRUPT_HANDLER(exception_10, "In exception 10", TRUE);
INTERRUPT_HANDLER(exception_11, "In exception 11", TRUE);
INTERRUPT_HANDLER(exception_12, "In exception 12", TRUE);
INTERRUPT_HANDLER(exception_13, "In exception 13", TRUE);
