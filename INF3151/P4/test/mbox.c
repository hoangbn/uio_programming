#include "common.h"
#include "thread.h"
#include "mbox.h"
#include "util.h"

#define MAX_Q 5
#define BUFFER_SIZE 1024

typedef struct {
    int used;
    lock_t l;
    condition_t more_space, more_data;
    int count;
    int head;
    int tail;
    char buffer[BUFFER_SIZE];
} msg_queue_t;

static msg_queue_t Q[MAX_Q];

static int space_available(msg_queue_t *q)
{
    if ((q->tail == q->head) && (q->count != 0)) {
	return 0;
    } else {
	if (q->tail > q->head) {
	    return q->tail - q->head;
	} else {
	    return q->tail + BUFFER_SIZE - q->head;
	}
    }
}

static int msg_size(msg_t *m)
{
    return sizeof(int) + m->size;
}

/* Interrupts have to be enabled before calling these functions */
void mbox_init(void)
{
    int i;

    for (i = 0; i < MAX_Q; i++) {
	Q[i].used = 0;
	lock_init(&Q[i].l);
	condition_init(&Q[i].more_space);
	condition_init(&Q[i].more_data);
    }
}

int mbox_open(int key)
{
    if (key < MAX_Q) {
	lock_acquire(&Q[key].l);
	if (Q[key].used == 0) {
	    Q[key].head = Q[key].tail = 0;
	}
	Q[key].used++;
	lock_release(&Q[key].l);
	return key;
    } else {
	return -1;
    }
}

int mbox_close(int q)
{
    lock_acquire(&Q[q].l);
    Q[q].used--;
    lock_release(&Q[q].l);
    return 1;
}

int mbox_stat(int q, int *count, int *space)
{
    lock_acquire(&Q[q].l);
    *count = Q[q].count;
    *space = space_available(&Q[q]);
    lock_release(&Q[q].l);
    return 1;
}

static void print_trace(char *s, int q, int msgsize)
{
#ifdef UNCOMMENT
    static int count = 0;

    count++;
    print_str(count % 10, 40, "                                          ");
    print_int(count % 10, 40, count / 10);
    print_int(count % 10, 45, q);
    print_str(count % 10, 50, s);
    print_int(count % 10, 60, space_available(&Q[q]));
    if (msgsize < 0) {
	print_str(count % 10, 70, "-    ");
    } else {
	print_int(count % 10, 70, msgsize);
    }
#endif
}

int mbox_recv(int q, msg_t *m)
{
    int i, msgsize;
    char *p;

    lock_acquire(&Q[q].l);
    print_trace("Recv", q, -1);
    while (Q[q].count == 0) {
	condition_wait(&Q[q].l, &Q[q].more_data);
    }
    p = (char *) &m->size;
    for (i = 0; i < sizeof(int); i++) {
	p[i] = (Q[q].buffer[(Q[q].tail + i) % BUFFER_SIZE]);
    }
    msgsize = msg_size(m);
    for (i = 0; i < msgsize; i++) {
	((char *) m)[i] = Q[q].buffer[Q[q].tail];
	Q[q].tail = (Q[q].tail + 1) % BUFFER_SIZE;
    }
    condition_broadcast(&Q[q].more_space);
    Q[q].count--;
    lock_release(&Q[q].l);
    return 1;
}

int mbox_send(int q, msg_t *m)
{
    int i, msgsize = msg_size(m);

    lock_acquire(&Q[q].l);
    print_trace("Send", q, msgsize);
    while (space_available(&Q[q]) < msgsize) {
	condition_wait(&Q[q].l, &Q[q].more_space);
    }
    for (i = 0; i < msgsize; i++) {
	Q[q].buffer[Q[q].head] = ((char *) m)[i];
	Q[q].head = (Q[q].head + 1) % BUFFER_SIZE;
    }
    condition_broadcast(&Q[q].more_data);
    Q[q].count++;
    lock_release(&Q[q].l);
    return 1;
}

#define LINE 1
#define COL 40

void print_mbox_status(void)
{
    int i;

    print_str(LINE - 1, COL, "Mbox");
    print_str(LINE - 1, COL + 8, "Used");
    print_str(LINE - 1, COL + 16, "Head");
    print_str(LINE - 1, COL + 24, "Tail");
    print_str(LINE - 1, COL + 32, "Count");

    for (i = 0; i < MAX_Q; i++) {
	print_str(LINE + i, COL, "                                          ");
	print_int(LINE + i, COL, i);
	print_int(LINE + i, COL + 8, Q[i].used);
	print_int(LINE + i, COL + 16, Q[i].head);
	print_int(LINE + i, COL + 24, Q[i].tail);
	print_int(LINE + i, COL + 32, Q[i].count);
    }
}
