#ifndef SCHEDULER_INCLUDED
#define SCHEDULER_INCLUDED

#include "common.h"
#include "kernel.h"

#ifdef DEBUG 
#define TRACE(s) \
    do { \
	print_int(critical_count % 10 + 2, 0, critical_count / 10); \
	print_str(critical_count % 10 + 2, 10, (s)); \
	print_str(critical_count % 10 + 2, 20, __FILE__); \
	print_int(critical_count % 10 + 2, 35, __LINE__); \
	print_int(critical_count % 10 + 2, 45, \
	    current_running->disable_count); \
	print_int(critical_count % 10 + 2, 50, current_running->pid); \
	print_int(critical_count % 10 + 2, 55, \
	    current_running->nested_count); \
	critical_count++; \
    } while (0)
#else
#define TRACE(s)
#endif

#define RUNNING 0
#define BLOCKED 1
#define EXITED  2

#define SAVE_STACK(s) \
    asm volatile ("movl  %%esp, %0":"=q" (s))

#define RESTORE_STACK(s) \
    asm volatile ("movl %0, %%esp"::"q" (s))

#define JUMP(a) \
    asm volatile ("jmp %0"::"q" (a))

#define RETURN_FROM_INTERRUPT \
    asm volatile ("iret")

#define SEND_END_OF_INTERRUPT \
    outb(0x20, 0x20)

#define SAVE_MASK \
    (current_running->int_controller_mask = inb(0x21))

#define RESTORE_MASK \
    outb(0x21, current_running->int_controller_mask)

#define MASK_HARDWARE_INTERRUPT(i) \
    do { \
	int mask; \
	mask = inb(0x21); \
	mask |= (1 << i); \
	outb(0x21, mask); \
    } while (0)

#define UNMASK_HARDWARE_INTERRUPT(i) \
    do { \
	int mask; \
	mask = inb(0x21); \
	mask &= ~(1 << i); \
	outb(0x21, mask); \
    } while (0)

#define PUSH_ALL \
    do { \
	asm volatile ("pushl %eax"); \
	asm volatile ("pushl %ebx"); \
	asm volatile ("pushl %ecx"); \
	asm volatile ("pushl %edx"); \
	asm volatile ("pushl %edi"); \
	asm volatile ("pushl %esi"); \
	asm volatile ("pushl %ebp"); \
    } while (0)

#define POP_ALL \
    do { \
	asm volatile ("popl %ebp"); \
	asm volatile ("popl %esi"); \
	asm volatile ("popl %edi"); \
	asm volatile ("popl %edx"); \
	asm volatile ("popl %ecx"); \
	asm volatile ("popl %ebx"); \
	asm volatile ("popl %eax"); \
    } while (0)

#define PUSH(x) \
    asm volatile ("pushl %0"::"q" (x))

#define POP(x) \
    asm volatile ("popl %0":"=q" (x))

#define POP_DISCARD \
    asm volatile ("addl $0x4, %esp")

#define SWITCH_DS \
    do { \
	asm volatile ("pushl %ds"); \
	load_data_segments(KERNEL_DS); \
    } while (0)

#define RESTORE_DS \
    asm volatile ("popl %ds")

#define SETUP_PAGE_TABLE \
    asm volatile ("movl %%eax, %%cr3 "::"a" (current_running->root_page_table))

#define CRITICAL_SECTION_BEGIN \
    do { \
	asm volatile ("cli"); \
	++current_running->disable_count; \
	TRACE("Disable"); \
    } while (0)

#define CRITICAL_SECTION_END  \
    do { \
	--current_running->disable_count; \
	TRACE("Enable "); \
	if (current_running->disable_count == 0) { \
	    asm volatile ("sti"); \
	} \
    } while (0)

#define DELAYED_CRITICAL_SECTION_END \
    do { \
	--current_running->disable_count; \
	TRACE("Enable "); \
	ASSERT(current_running->disable_count == 0); \
    } while (0)

void dispatch(void);

void yield(void);
void exit(void);
int getpid(void);
int getpriority(void);
void setpriority(int);
void block(struct pcb_t **, int *spinlock);
void unblock(struct pcb_t **);
void readdir(unsigned char *buf);
void loadproc(int location, int size);

#endif
