#include "common.h"
#include "syslib.h"
#include "util.h"

#define COMMAND_MBOX 1

#define ROWS 4
#define COLUMNS 18
static char picture[ROWS][COLUMNS + 1] =
{
    "     ___       _  ",
    " | __\\_\\_o____/_| ",
    " <[___\\_\\_-----<  ",
    " |  o'            "
};

static void draw_plane(int loc_x, int loc_y, int plane);
static void draw_bullet(int loc_x, int loc_y, int plane);

void _start(void)
{
    int loc_x = 80, loc_y = 7, q, fired = FALSE, bullet_x = -1, bullet_y = -1;
    msg_t m;

    q = mbox_open(COMMAND_MBOX);
    while (1) {
	draw_plane(loc_x, loc_y, FALSE);
	loc_x -= 1;
	if (loc_x < 10) {
	    loc_x = 80;
	}
	draw_plane(loc_x, loc_y, TRUE);

	if ((q >= 0) && (!fired)) {
	    int count, space;

	    mbox_stat(q, &count, &space);
	    if (count > 0) {
		mbox_recv(q, &m);
		ASSERT2(m.size == 0, "Invalid mesg size");

		fired = TRUE;
		if (loc_x < 30) {
		    bullet_x = 80;
		} else {
		    bullet_x = loc_x - 2;
		}
		bullet_y = loc_y + 2;
	    }
	}
	if (fired) {
	    int c;

	    draw_bullet(bullet_x, bullet_y, FALSE);
	    if ((bullet_x - 1 >= 0) &&
		((c = peek_screen(bullet_x - 1, bullet_y)) != 0) &&
		(c != ' ')) {
		draw_bullet(bullet_x - 1, bullet_y, FALSE);
		fired = FALSE;
	    } else if ((bullet_x - 2 >= 0) &&
		    ((c = peek_screen(bullet_x - 2, bullet_y)) != 0) &&
		(c != ' ')) {
		draw_bullet(bullet_x - 2, bullet_y, FALSE);
		fired = FALSE;
	    } else {
		bullet_x -= 2;
		if (bullet_x < 0) {
		    fired = FALSE;
		} else {
		    draw_bullet(bullet_x, bullet_y, TRUE);
		}
	    }
	}
	delay(2000000);
    }
    if (q >= 0) {
	mbox_close(q);
    }
}

static void draw_plane(int loc_x, int loc_y, int plane)
{
    int i, j;

    for (i = 0; i < COLUMNS; i++) {
	for (j = 0; j < ROWS; j++) {
	    if (loc_x + i >= 30) {
		if (plane == TRUE) {
		    print_char(loc_y + j, loc_x + i, picture[j][i]);
		} else {
		    print_char(loc_y + j, loc_x + i, ' ');
		}
	    }
	}
    }
}

static void draw_bullet(int loc_x, int loc_y, int bullet)
{
    if (bullet) {
	print_str(loc_y, loc_x, "<=");
    } else {
	print_str(loc_y, loc_x, "  ");
    }
}
