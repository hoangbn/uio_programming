#include "common.h"
#include "floppy.h"
#include "kernel.h"
#include "scheduler.h"
#include "thread.h"
#include "util.h"

/* I/O Ports used by floppy disk */
#define DOR            0x3F2    /* motor drive control bits */
#define FDC_STATUS     0x3F4    /* floppy disk controller status register */
#define FDC_DATA       0x3F5    /* floppy disk controller data register */
#define FDC_RATE       0x3F7    /* transfer rate register */
#define DMA_ADDR       0x004    /* port for low 16 bits of DMA address */
#define DMA_TOP        0x081    /* port for top 4 bits of 20-bit DMA addr */
#define DMA_COUNT      0x005    /* port for DMA count (count =  bytes - 1) */
#define DMA_FLIPFLOP   0x00C    /* DMA byte pointer flip-flop */
#define DMA_MODE       0x00B    /* DMA mode port */
#define DMA_INIT       0x00A    /* DMA init port */
#define DMA_RESET_VAL   0x06
	
/* Status registers returned as result of operation. */
#define ST0             0x00    /* status register 0 */
#define ST1             0x01    /* status register 1 */
#define ST2             0x02    /* status register 2 */
#define ST3             0x00    /* status register 3 (return by DRIVE_SENSE) */
#define ST_CYL          0x03    /* slot where controller reports cylinder */
#define ST_HEAD         0x04    /* slot where controller reports head */
#define ST_SEC          0x05    /* slot where controller reports sector */

/* Fields within the I/O ports. */

/* Main status register. */
#define CTL_BUSY        0x10    /* bit is set when read or write in progress */
#define DIRECTION       0x40    /* bit is set when reading data reg is valid */
#define MASTER          0x80    /* bit is set when data reg can be accessed */
/* Digital output port (DOR). */
#define MOTOR_SHIFT        4    /* high 4 bits control the motors in DOR */
#define ENABLE_INT      0x0C    /* used for setting DOR port */
/* ST0. */
#define ST0_BITS        0xF8    /* check top 5 bits of seek status */
#define TRANS_ST0       0x00    /* top 5 bits of ST0 for READ/WRITE */
#define SEEK_ST0        0x20    /* top 5 bits of ST0 for SEEK */
/* ST1. */
#define WRITE_PROTECT   0x02    /* bit is set if diskette is write protected */

/* Floppy disk controller command bytes. */
#define FDC_SEEK        0x0F    /* command the drive to seek */
#define FDC_READ        0xE6    /* command the drive to read */
#define FDC_WRITE       0xC5    /* command the drive to write */
#define FDC_SENSE       0x08    /* command the controller to tell its status */
#define FDC_SPECIFY     0x03    /* command the drive to accept params */

/* DMA channel commands. */
#define DMA_READ        0x46    /* DMA read opcode */
#define DMA_WRITE       0x4A    /* DMA write opcode */
	
/* Parameters for the disk drive. */
#define SPEC2           0x02    /* second parameter to SPECIFY */
#define DTL             0xFF    /* determines data length (sector size) */

/* Maximum retun values from the controller */
#define MAX_RESULTS     7

/* Miscellaneous. */
#define SECTOR_SIZE_CODE   2    /* code to say "512" to the controller */
#define DEV_WRITE 1
#define DEV_READ  2

#ifdef DEBUG
#define PRINT_STR print_str
#else
#define PRINT_STR(a,b,c)
#endif

/* The global status variable */
volatile int floppy_irq = FALSE;
uchar_t f_results[MAX_RESULTS];

static int floppy_lock;
static bool_t motor_started = FALSE;

static void fdc_out(int val)
{
    while ((inb(FDC_STATUS) & (MASTER | DIRECTION)) !=
	(uchar_t) (MASTER | 0)) {
	/* Wait */
    }
    outb(FDC_DATA, val);
}

void floppy_init(void)
{
    /* Set the stepping rate */
    fdc_out(FDC_SPECIFY);
    fdc_out(0xDF);
    fdc_out(SPEC2);
    spinlock_init(&floppy_lock);
}

void floppy_start_motor(void)
{
    spinlock_acquire(&floppy_lock);
    /* Ask drive 0 to turn on motor */
    outb(DOR, (1 << MOTOR_SHIFT) | ENABLE_INT);
    motor_started = TRUE;
}

void floppy_stop_motor(void)
{
    ASSERT(motor_started);
    /* Ask drive 0 to turn off motor */
    outb(DOR, ENABLE_INT);
    motor_started = FALSE;
    spinlock_release(&floppy_lock);
}

static int fdc_results(void)
{
    int result_nr = 0;
    uchar_t status;
    int count = 0;

    do {
	status = inb(FDC_STATUS) & (MASTER | DIRECTION | CTL_BUSY);
	if (status == (MASTER | DIRECTION | CTL_BUSY)) {
	    if (result_nr >= MAX_RESULTS) {
		break;			/* too many results */
	    }
	    f_results[result_nr++] = inb(FDC_DATA);
	    continue;
	}
	if (status == MASTER) {
	    return 0;
	}
    } while (count++ < 1000000);

    return -1;
}

static int seek_cylinder(int head, int cylinder)
{
    int r;

    floppy_irq = FALSE;

    /* Seek the right cylinder */
    fdc_out(FDC_SEEK);
    fdc_out(head << 2);
    fdc_out(cylinder);

    PRINT_STR(2, 0, "Waiting for seek");

    /* Wait for the floppy interrupt */
    while (!floppy_irq);

    fdc_out(FDC_SENSE);			/* probe FDC to make it return status */
    r = fdc_results();
    /* get controller status bytes */
    if (r != 0) {
	HALT("Could not get results");
    }
    if ((f_results[ST0] & ST0_BITS) != SEEK_ST0) {
	HALT("No seek");
    }
    if (f_results[ST1] != cylinder) {
	HALT("Incorrect cylinder");
    }
    if (r != 0 || (f_results[ST0] & ST0_BITS) != SEEK_ST0 || f_results[ST1] !=
	cylinder) {
	/* seek failed, may need a recalibrate */
	return -1;
    }
    PRINT_STR(2, 0, "Floppy is done  ");
    return 0;
}

static void dma_setup(int opcode, uint32_t address, unsigned short count)
{
    outb(DMA_INIT, DMA_RESET_VAL);	/* reset the dma controller */
    outb(DMA_FLIPFLOP, 0);		/* write anything to reset it */
    outb(DMA_MODE, opcode == DEV_WRITE ? DMA_WRITE : DMA_READ);
    outb(DMA_ADDR, (uchar_t) (address >> 0));
    outb(DMA_ADDR, (uchar_t) (address >> 8));
    outb(DMA_TOP, (uchar_t) (address >> 16));
    outb(DMA_COUNT, (count - 1) >> 0);
    outb(DMA_COUNT, (count - 1) >> 8);
    outb(DMA_INIT, 2);			/* some sort of enable */
}

static int actual_transfer(int opcode, int sector, int head, int cylinder)
{
    int s, r;

    floppy_irq = FALSE;

    /* The drive is now on the proper cylinder.  Read or write 1 block. */
    /* The command is issued by outputting several bytes to the controller. */
    fdc_out(opcode == DEV_WRITE ? FDC_WRITE : FDC_READ);
    fdc_out(head << 2);
    fdc_out(cylinder);
    fdc_out(head);
    fdc_out(sector);
    fdc_out(SECTOR_SIZE_CODE);
    fdc_out(18);
    fdc_out(0x1b);			/* sector gap */
    fdc_out(DTL);			/* data length */

    /* Wait for the floppy interrupt */
    while (!floppy_irq) {
	yield();
    }
    PRINT_STR(2, 0, "dma done       ");

    r = fdc_results();
    if (r != 0) {
	return (r);
    }

    if (f_results[ST1] & WRITE_PROTECT) {
	HALT("Diskette is write protected");
    }
    if ((f_results[ST0] & ST0_BITS) != TRANS_ST0) {
	HALT("Transfer error");
    }
    if (f_results[ST1] | f_results[ST2]) {
	HALT("Some error");
    }
    /* Compare actual numbers of sectors transferred with expected number: 1 */
    s = (f_results[ST_CYL] - cylinder) * 2 * 18;
    s += (f_results[ST_HEAD] - head) * 18;
    s += (f_results[ST_SEC] - sector);
    if (s != 1) {
	return -1;
    }
    return 0;
}

static int floppy_read_write(int what, int block, ulong_t address)
{
    int r;
    int sector, head, cylinder;

    int i;

    /* for ( i = 0; i < 0x100000; i++); */

    ASSERT2(block < 2880, "Block numer too high");

    cylinder = block / (2 * 18);
    head = (block % (2 * 18)) / 18;
    sector = 1 + (block % 18);

    /* First we perform a seek to get to the right cylinder. */
    if (seek_cylinder(head, cylinder) == 0) {

	/* Let's setup the dma transfer */
	dma_setup(what, address, SECTOR_SIZE);
	PRINT_STR(2, 0, "seek is done  ");
	for (i = 0; i < 0x100000; i++) {
	    /* delay */
	}
	/* Now we actually do the transfer */
	r = actual_transfer(what, sector, head, cylinder);
	if (r != 0) {
	    HALT("Problem with actual transfer");
	}
    } else {
	HALT("Seek failed");
    }
    PRINT_STR(2, 0, "transfer done   ");
    return 0;
}

int floppy_read(int block, ulong_t address)
{
    ASSERT(motor_started);
    return floppy_read_write(DEV_READ, block, address);
}

int floppy_write(int block, ulong_t address)
{
    ASSERT(motor_started);
    return floppy_read_write(DEV_WRITE, block, address);
}
