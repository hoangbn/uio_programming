#ifndef INTERRUPT_INCLUDED
#define INTERRUPT_INCLUDED

void load_data_segments(int seg);
void system_call_entry(void);
void irq0(void);
void irq1(void);
void irq6(void);
void fake_irq7(void);

void bogus_interrupt(void);
void exception_0(void);
void exception_1(void);
void exception_2(void);
void exception_3(void);
void exception_4(void);
void exception_5(void);
void exception_6(void);
void exception_7(void);
void exception_8(void);
void exception_9(void);
void exception_10(void);
void exception_11(void);
void exception_12(void);
void exception_13(void);
void exception_14(void);

#define NUM_EXCEPTIONS 15

#endif
