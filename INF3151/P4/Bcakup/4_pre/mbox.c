/*
 * Implementation of the mailbox.
 * Implementation notes: 
 *
 * The mailbox is protected with a lock to make sure that only 
 * one process is within the queue at any time. 
 *
 * It also uses condition variables to signal that more space or
 * more messages are available. 
 * In other words, this code can be seen as an example of implementing a
 * producer-consumer problem with a monitor and condition variables. 
 *
 * Note that this implementation only allows keys from 0 to 4 
 * (key >= 0 and key < MAX_Q). 
 *
 * The buffer is a circular array. 
*/

#include "common.h"
#include "thread.h"
#include "mbox.h"
#include "util.h"

mbox_t Q[MAX_MBOX];


/*
 * Returns the number of bytes available in the queue
 * Note: Mailboxes with count=0 messages should have head=tail, which
 * means that we return BUFFER_SIZE bytes.
 */
static int space_available(mbox_t * q)
{
  if ((q->tail == q->head) && (q->count != 0)) {
    /* Message in the queue, but no space  */
    return 0;
  }

  if (q->tail > q->head) {
    /* Head has wrapped around  */
    return q->tail - q->head;
  }
  /* Head has a higher index than tail  */
  return q->tail + BUFFER_SIZE - q->head;
}

/* Initialize mailbox system, called by kernel on startup  */
void mbox_init(void)
{
  int i;
  for(i = 0;i < MAX_MBOX;i++){
    lock_init(&Q[i].l);
    condition_init(&Q[i].moreSpace);
    condition_init(&Q[i].moreData);
    Q[i].used = 0;
    Q[i].count = 0; 
    Q[i].head = 0;
    Q[i].tail = 0;
  }
}

/*
 * Open a mailbox with the key 'key'. Returns a mailbox handle which
 * must be used to identify this mailbox in the following functions
 * (parameter q).
 */
int mbox_open(int key)
{
  if(key < 0 || key >= MAX_MBOX) return FAILED;
  else {
    lock_acquire(&Q[key].l);
    Q[key].used++;
    lock_release(&Q[key].l);
    return key;
  }
}

/* Close the mailbox with handle q  */
int mbox_close(int q)
{
  ASSERT2(Q[q].used == 0, "Used should be no be negative");
  lock_acquire(&Q[q].l); 
  Q[q].used--;
  lock_release(&Q[q].l);
  return SUCCESS;
}


/*
 * Get number of bytes available in the
 * mailbox buffer (space). Note that the buffer is also used for
 * storing the message headers, which means that a message will take
 * MSG_T_HEADER + m->size bytes in the buffer. (MSG_T_HEADER =
 * sizeof(msg_t header))
 */
int mbox_stat(int q, int *count, int *space)
{
  if(q < 0 || q >= MAX_MBOX) return FAILED;
  lock_acquire(&Q[q].l);    	
  *count = Q[q].count;
  *space = space_available(&Q[q]);
  lock_release(&Q[q].l);
  return SUCCESS;
}

/* Fetch a message from queue 'q' and store it in 'm'  */
int mbox_recv(int q, msg_t * m)
{  
  enter_critical();
  lock_acquire(&Q[q].l);
  if(q < 0 || q >= MAX_MBOX) return FAILED;
  else {
    while(Q[q].count == 0) {     
      condition_wait(&Q[q].l, &Q[q].moreData);
    }
  
    int *tail = (int * )( Q[q].buffer + Q[q].tail);
    int size = MSG_SIZE(m);
    bcopy((char *)tail, (char *)m, size);
    Q[q].tail += size;
    condition_broadcast(&Q[q].moreSpace);
    Q[q].count--;
    lock_release(&Q[q].l);
  }

  leave_critical();
  return SUCCESS;
}

/* Insert 'm' into the mailbox 'q'  */
int mbox_send(int q, msg_t * m)
{ 
  enter_critical();
  lock_acquire(&Q[q].l);
  if(q < 0 || q >= MAX_MBOX) return FAILED;
  else {
    while(m->size > space_available(&Q[q])) {
      if(q == 0){
	lock_release(&Q[q].l);
	return FAILED;
      }
      condition_wait(&Q[q].l, &Q[q].moreSpace);
    }
    
    int *head = (int * )( Q[q].buffer + Q[q].head);
    int size = MSG_SIZE(m);
    bcopy((char *) m, (char *) head, size);
    Q[q].head += size;
    
    condition_broadcast(&Q[q].moreData);
    Q[q].count++;
    lock_release(&Q[q].l);
  }
  leave_critical();
  return SUCCESS;
  
}

