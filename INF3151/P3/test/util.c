#include "util.h"

#define SCREEN_ADDR ((short *) 0xb8000)

void clear_screen()
{
    static short *screen = SCREEN_ADDR;
    int i;

    for (i = 0; i < 2000; i++) {
	screen[i] = 0x0700;
    }
}

void delay(int n)
{
    int i, j;

    for (i = 0; i < n; i++) {
	j = i * 11;
    }
}

unsigned long long int get_timer(void)
{
    unsigned long long int x;

    asm volatile (".byte 0x0f, 0x31":"=A" (x));

    return x;
}

/* K&R page 64 */
void itoa(unsigned int n, char *s)
{
    int i;

    i = 0;
    do {
	s[i++] = n % 10 + '0';
    } while ((n /= 10) > 0);
    s[i++] = 0;
    reverse(s);
}

void itohex(unsigned int n, char *s)
{
    int i, d;

    i = 0;
    do {
	d = n % 16;
	if (d < 10) {
	    s[i++] = d + '0';
	} else {
	    s[i++] = d - 10 + 'a';
	}
    } while ((n /= 16) > 0);
    s[i++] = 0;
    reverse(s);
}

void print_char(int line, int col, char c)
{
    static short *screen = SCREEN_ADDR;

    if ((line < 0) || (line > 24)) {
	return;
    }
    if ((col < 0) || (col > 79)) {
	return;
    }
    screen[line * 80 + col] = 0x07 << 8 | c;
}

void print_int(int line, int col, unsigned int num)
{
    int i, n;
    char buf[12];

    itoa(num, buf);
    n = strlen(buf);
    for (i = 0; i < n; i++) {
	print_char(line, col + i, buf[i]);
    }
}

void print_hex(int line, int col, unsigned int num)
{
    int i, n;
    char buf[12];

    itohex(num, buf);
    n = strlen(buf);
    for (i = 0; i < n; i++) {
	print_char(line, col + i, buf[i]);
    }
}

void print_str(int line, int col, char *str)
{
    int i, n;

    n = strlen(str);
    for (i = 0; i < n; i++) {
	print_char(line, col + i, str[i]);
    }
}

/* K&R page 62 */
void reverse(char *s)
{
    int c, i, j;

    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
	c = s[i];
	s[i] = s[j];
	s[j] = c;
    }
}

/* K&R page 99 */
int strlen(char *s)
{
    int n;

    for (n = 0; *s != '\0'; s++) {
	n++;
    }
    return n;
}

/* read byte from I/O address space */
unsigned char inb(int port)
{
    int ret;

    iodelay();
    asm volatile ("xorl %eax,%eax");
    asm volatile ("inb %%dx,%%al":"=a" (ret):"d"(port));

    return ret;
}

/* write byte to I/O address space */
void outb(int port, unsigned char data)
{
    iodelay();
    asm volatile ("outb %%al,%%dx"::"a" (data), "d"(port));
}

/* This is the delay needed between each access to the I/O address
 * space.  The delay must be tuned according to processor speed (the
 * number we use should be safe within the 486 family). */
void iodelay(void)
{
    int i;

    for (i = 0; i < 10000; i++);
}
