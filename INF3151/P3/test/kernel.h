#ifndef KERNEL_INCLUDED
#define KERNEL_INCLUDED

enum {
    SYSCALL_YIELD,
    SYSCALL_EXIT,
    SYSCALL_GETPID,
    SYSCALL_GETPRIORITY,
    SYSCALL_SETPRIORITY
};

enum {
    STATUS_FIRST_TIME,
    STATUS_READY,
    STATUS_BLOCKED,
    STATUS_EXITED
};

#define CRITICAL_SECTION_BEGIN \
    do { \
	asm volatile ("cli"); \
	asm volatile ("incl %0" :"=m" (disable_count)); \
    } while (0)

#define CRITICAL_SECTION_END \
    do { \
	asm volatile ("decl %0" :"=m" (disable_count)); \
	if (disable_count == 0) { \
	    asm volatile ("sti"); \
	} \
    } while (0)

typedef struct {
    unsigned int eax, ebx, ecx, edx, esi, edi, ebp, esp, eflags;
} regs_t;

/* process control block */
typedef struct pcb_s {
    unsigned int pid;
    unsigned int in_kernel;
    unsigned int kernel_stack;
    unsigned int user_stack;
    unsigned int start_addr;
    unsigned int priority;
    unsigned int status;
    regs_t regs;
    struct pcb_s *next;
    struct pcb_s *prev;
} pcb_t;

void yield(void);
void exit(void);
int getpid(void);
int getpriority(void);
void setpriority(int);
void block(pcb_t **);
void unblock(pcb_t **);

void print_status(void);

extern pcb_t *current_running;
extern int disable_count;
extern int preempt_count;
extern int yield_count;

#endif
