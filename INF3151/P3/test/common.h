#ifndef COMMON_INCLUDED
#define COMMON_INCLUDED

#define NULL 0

typedef enum {
    FALSE, TRUE
} bool_t;

typedef signed char int8_t;
typedef short int int16_t;
typedef int int32_t;
typedef long long int int64_t;

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long int uint64_t;

typedef unsigned char uchar_t;
typedef unsigned int uint_t;
typedef unsigned long ulong_t;

#define ASSERT(p) \
    do { \
	if (!(p)) { \
	    print_str(0, 0, "Assertion failure: "); \
	    print_str(0, 19, # p); \
	    print_str(1, 0, "file: "); \
	    print_str(1, 6, __FILE__); \
	    print_str(2, 0, "line: "); \
	    print_int(2, 6, __LINE__); \
	    asm volatile ("cli"); \
	    while (1); \
	} \
    } while(0)

#endif
