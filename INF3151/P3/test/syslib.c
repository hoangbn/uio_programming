#include "kernel.h"
#include "syslib.h"

void yield(void)
{
    asm volatile ("int $48"::"a" (SYSCALL_YIELD));
}

void exit(void)
{
    asm volatile ("int $48"::"a" (SYSCALL_EXIT));
}

int getpid(void)
{
    int ret;

    asm volatile ("int $48":"=a" (ret):"0"(SYSCALL_GETPID));

    return ret;
}

int getpriority(void)
{
    int ret;

    asm volatile ("int $48":"=a" (ret):"0"(SYSCALL_GETPRIORITY));

    return ret;
}

void setpriority(int p)
{
    asm volatile ("pushl %0"::"m" (p));
    asm volatile ("int $48"::"a" (SYSCALL_SETPRIORITY));
    asm volatile ("addl $4,%esp");
}
