#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "util.h"

static void scheduler(void);
static void _yield(void);
static void remove_job(pcb_t *job);

static regs_t regs;

#define SAVE_GEN_REGS \
    do { \
	asm volatile ("# save general-purpose registers"); \
	asm volatile ("movl %%eax,%0":"=m" (regs.eax)); \
	asm volatile ("movl %%ebx,%0":"=m" (regs.ebx)); \
	asm volatile ("movl %%ecx,%0":"=m" (regs.ecx)); \
	asm volatile ("movl %%edx,%0":"=m" (regs.edx)); \
	asm volatile ("movl %%esi,%0":"=m" (regs.esi)); \
	asm volatile ("movl %%edi,%0":"=m" (regs.edi)); \
	asm volatile ("movl %%ebp,%0":"=m" (regs.ebp)); \
	asm volatile ("movl %%esp,%0":"=m" (regs.esp)); \
	current_running->regs.eax = regs.eax; \
	current_running->regs.ebx = regs.ebx; \
	current_running->regs.ecx = regs.ecx; \
	current_running->regs.edx = regs.edx; \
	current_running->regs.esi = regs.esi; \
	current_running->regs.edi = regs.edi; \
	current_running->regs.ebp = regs.ebp; \
	current_running->regs.esp = regs.esp; \
    } while (0)

#define RESTORE_GEN_REGS \
    do { \
	asm volatile ("# restore general-purpose registers"); \
	regs.eax = current_running->regs.eax; \
	regs.ebx = current_running->regs.ebx; \
	regs.ecx = current_running->regs.ecx; \
	regs.edx = current_running->regs.edx; \
	regs.esi = current_running->regs.esi; \
	regs.edi = current_running->regs.edi; \
	regs.ebp = current_running->regs.ebp; \
	regs.esp = current_running->regs.esp; \
	asm volatile ("movl %0,%%eax"::"m" (regs.eax)); \
	asm volatile ("movl %0,%%ebx"::"m" (regs.ebx)); \
	asm volatile ("movl %0,%%ecx"::"m" (regs.ecx)); \
	asm volatile ("movl %0,%%edx"::"m" (regs.edx)); \
	asm volatile ("movl %0,%%esi"::"m" (regs.esi)); \
	asm volatile ("movl %0,%%edi"::"m" (regs.edi)); \
	asm volatile ("movl %0,%%ebp"::"m" (regs.ebp)); \
	asm volatile ("movl %0,%%esp"::"m" (regs.esp)); \
    } while (0)

#define SAVE_EFLAGS \
    do { \
	asm volatile ("# save eflags"); \
	asm volatile ("pushfl"); \
	asm volatile ("popl %0":"=q" (current_running->regs.eflags)); \
    } while (0)

#define RESTORE_EFLAGS \
    do { \
	asm volatile ("# restore eflags"); \
	asm volatile ("pushl %0"::"q" (current_running->regs.eflags)); \
	asm volatile ("popfl"); \
    } while (0)

#define SWITCH_TO_KERNEL_STACK \
    do { \
	asm volatile ("# switch to kernel stack"); \
	asm volatile ("movl %0,%%esp"::"q" (current_running->kernel_stack)); \
    } while (0)

#define SAVE_KERNEL_STACK \
    do { \
	asm volatile ("# save kernel stack"); \
	asm volatile ("movl %%esp,%0":"=q" (current_running->kernel_stack)); \
    } while (0)

#define SEND_EOI outb(0x20, 0x20)

/* %eax has the system call number.  Any arguments are found on the
 * stack.  Any result value is put in %eax. */
void system_call_entry(void)
{
    SAVE_GEN_REGS;
    SWITCH_TO_KERNEL_STACK;

    /* call the desired function */
    switch (regs.eax) {
    case SYSCALL_YIELD:
	_yield();
	break;
    case SYSCALL_EXIT:
	exit();
	break;
    case SYSCALL_GETPID:
	getpid();
	break;
    case SYSCALL_GETPRIORITY:
	getpriority();
	break;
    case SYSCALL_SETPRIORITY:
	/* syscall stack: eip, cs, eflags, arg1 */
	asm volatile ("movl %0,%%eax"::"q" (current_running->regs.esp));
	asm volatile ("pushl 12(%eax)");
	asm volatile ("call setpriority");
	asm volatile ("addl $4,%esp");

	break;
    default:
	/* kill the process if it makes an illegal system call */
	exit();
	break;
    }

    /* save the return value */
    asm volatile ("movl %%eax, %0":"=q" (current_running->regs.eax));

    SAVE_KERNEL_STACK;
    RESTORE_GEN_REGS;
    asm volatile ("iret");
}

void yield(void)
{
    /* current_running is a thread: processes call _yield() */
    SAVE_GEN_REGS;
    SAVE_EFLAGS;

    yield_count++;
    scheduler();

    RESTORE_EFLAGS;
    RESTORE_GEN_REGS;
}

static void _yield(void)
{
    /* current_running is a process: context was saved in system_call_entry */
    yield_count++;
    scheduler();
}

static void scheduler(void)
{
    SAVE_KERNEL_STACK;
    ASSERT(disable_count != 0);

    switch (current_running->status) {
    case STATUS_READY:
	current_running = current_running->next;
	break;
    case STATUS_BLOCKED:
    case STATUS_EXITED:
	if (current_running->next == current_running) {
	    print_str(0, 0, "No more jobs.");
	    asm volatile ("cli");

	    while (1);
	}
	/* remove the job from the job list */
	current_running = current_running->next;
	remove_job(current_running->prev);
	break;
    default:
	print_str(0, 0, "Invalid job status.");
	asm volatile ("cli");

	while (1);
	break;
    }
    dispatch();
}

void dispatch(void)
{
    ASSERT(disable_count != 0);
    /* if this is the first time, just jump to the start address */
    if (current_running->status == STATUS_FIRST_TIME) {
	current_running->status = STATUS_READY;
	if (current_running->in_kernel) {
	    asm volatile ("movl %0, %%esp"::
		"q" (current_running->kernel_stack));
	} else {
	    asm volatile ("movl %0, %%esp"::
		"q" (current_running->user_stack));
	}
	asm volatile ("jmp *%0"::"q" (current_running->start_addr));
    }
    SWITCH_TO_KERNEL_STACK;
}

void exit(void)
{
    current_running->status = STATUS_EXITED;
    scheduler();
    ASSERT(0);				/* not reached */
}

void block(pcb_t **q)
{
    pcb_t *p;

    ASSERT(disable_count != 0);

    /* mark the job as blocked */
    current_running->status = STATUS_BLOCKED;

    /* put the job on the waiting queue */
    if (*q == NULL) {
	*q = current_running;
    } else {
	p = *q;
	while (p->next != NULL) {
	    p = p->next;
	}
	p->next = current_running;
    }
    SAVE_GEN_REGS;
    SAVE_EFLAGS;
    scheduler();
    RESTORE_EFLAGS;
    RESTORE_GEN_REGS;
}

void unblock(pcb_t **q)
{
    pcb_t *job;

    ASSERT(disable_count != 0);

    /* remove the job from the waiting queue */
    job = *q;
    (*q) = (*q)->next;

    /* put the job back on the job list */
    job->status = STATUS_READY;
    job->next = current_running->next;
    job->prev = current_running;
    current_running->next->prev = job;
    current_running->next = job;
}

void irq0(void)
{
}

int getpid(void)
{
    return current_running->pid;
}

int getpriority(void)
{
    return current_running->priority;
}

void setpriority(int p)
{
    current_running->priority = p;
}

static void remove_job(pcb_t *job)
{
    job->prev->next = job->next;
    job->next->prev = job->prev;
    job->next = NULL;
    job->prev = NULL;
}
