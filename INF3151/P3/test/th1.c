#include "kernel.h"
#include "th.h"
#include "util.h"

void clock_thread(void)
{
    unsigned int time;
    unsigned long long int ticks;

    while (1) {
	ticks = get_timer() >> 20;
	time = ((int) ticks) / 166;
	print_str(16, 49, "S c h e d u l e r   S t a t s");
	print_str(19, 49, "Disable Count      : ");
	print_int(19, 70, disable_count);
	print_str(20, 49, "No. of Yields      : ");
	print_int(20, 70, yield_count);
	print_str(21, 49, "No. of Preemptions : ");
	print_int(21, 70, preempt_count);
	print_str(24, 49, "Time (in seconds)  : ");
	print_int(24, 70, time);
	print_status();
	yield();
    }
}
