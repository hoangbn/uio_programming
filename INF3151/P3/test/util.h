#ifndef UTIL_INCLUDED
#define UTIL_INCLUDED

void clear_screen(void);
void delay(int n);
unsigned long long int get_timer(void);

void itoa(unsigned int n, char *s);
void itohex(unsigned int n, char *s);

void print_char(int line, int col, char c);
void print_int(int line, int col, unsigned int num);
void print_hex(int line, int col, unsigned int num);
void print_str(int line, int col, char *str);

void reverse(char *s);
int strlen(char *s);

unsigned char inb(int port);
void outb(int port, unsigned char data);
void iodelay(void);

#endif
