#ifndef SCHEDULER_INCLUDED
#define SCHEDULER_INCLUDED

void system_call_entry(void);
void dispatch(void);
void irq0(void);

#endif
