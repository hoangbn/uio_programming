#ifndef THREAD_INCLUDED
#define THREAD_INCLUDED

#include "kernel.h"

typedef struct {
    int status;
    pcb_t *waiting;
} lock_t;

void lock_init(lock_t *);
void lock_acquire(lock_t *);
void lock_release(lock_t *);

typedef struct {
    pcb_t *waiting;
} condition_t;

void condition_init(condition_t *c);
void condition_wait(lock_t *m, condition_t *c);
void condition_signal(condition_t *c);
void condition_broadcast(condition_t *c);

#endif
