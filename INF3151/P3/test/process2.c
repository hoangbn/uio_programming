#include "syslib.h"
#include "util.h"

static int rec(int n);

void _start(void)
{
    int i, res;

    for (i = 0; i <= 100; i++) {
	res = rec(i);
	print_str(10, 0, "Did you know that 1 + ... + ");
	print_int(10, 28, i);
	print_str(10, 31, " = ");
	print_int(10, 34, res);
	delay(3000000);
	yield();
    }
    exit();
}

static int rec(int n)
{
    if (n % 37 == 0) {
	yield();
    }
    if (n == 0) {
	return 0;
    } else {
	return n + rec(n - 1);
    }
}
