#include "common.h"
#include "syslib.h"
#include "util.h"

#define ROWS 4
#define COLUMNS 18

static char picture[ROWS][COLUMNS + 1] =
{
    "     ___       _  ",
    " | __\\_\\_o____/_| ",
    " <[___\\_\\_-----<  ",
    " |  o'            "
};

static void draw(int loc_x, int loc_y, int plane);

void _start(void)
{
    int count = 0, loc_x = 80, loc_y = 1, pri, pid;

    while (1) {
	draw(loc_x, loc_y, FALSE);
	loc_x -= 1;
	if (loc_x < -20) {
	    loc_x = 80;
	}
	draw(loc_x, loc_y, TRUE);
	if (count++ % 100) {
	    pid = getpid();
	    pri = getpriority();
	    print_str(8, 0, "Process ");
	    print_int(8, 8, pid);
	    print_str(8, 10, "priority ");
	    print_int(8, 20, pri);
	    if (pri < 64) {
		setpriority(pri + 1);
	    } else {
		setpriority(10);
	    }
	}
	delay(2000000);
    }
}

static void draw(int loc_x, int loc_y, int plane)
{
    int i, j;

    for (i = 0; i < COLUMNS; i++) {
	for (j = 0; j < ROWS; j++) {
	    if (plane == TRUE) {
		print_char(loc_y + j, loc_x + i, picture[j][i]);
	    } else {
		print_char(loc_y + j, loc_x + i, ' ');
	    }
	}
    }
}
