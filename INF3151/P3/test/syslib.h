#ifndef SYSLIB_INCLUDED
#define SYSLIB_INCLUDED

void yield(void);
void exit(void);
int getpid(void);
int getpriority(void);
void setpriority(int);

#endif
