#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "th.h"
#include "util.h"

#define NUM_THREADS 3
#define NUM_PROCS 2
#define NUM_TOTAL (NUM_PROCS + NUM_THREADS)

static unsigned int start_addr[NUM_TOTAL] =
{
    (unsigned int) clock_thread,
    (unsigned int) thread2,
    (unsigned int) thread3,
    PROC1_ADDR,
    PROC2_ADDR
};

static pcb_t pcb[NUM_TOTAL];
pcb_t *current_running;
int disable_count = 1;
int preempt_count = 0;
int yield_count = 0;

/* 100 Hz */
#define PREEMPT_TICKS 11932

#define STACK_MIN 0x10000
#define STACK_MAX 0x20000
#define STACK_OFFSET 0x0ffc
#define STACK_SIZE 0x1000

#define	KERNEL_CS (1 << 3)
#define IDT_SIZE 49
#define IRQ_START 32
#define INTERRUPT_GATE 0x0e
#define IDT_SYSCALL_POS	48

struct gate_t {
    uint16_t offset_low;
    uint16_t selector;
    uint8_t count;
    uint8_t access;
    uint16_t offset_high;
} __attribute__((packed));

struct point_t {
    uint16_t limit;
    uint32_t base;
} __attribute__((packed));

static struct gate_t idt[IDT_SIZE];

static void create_gate(
    struct gate_t *entry,
    uint32_t offset,
    uint16_t selector,
    char type,
    char privilege,
    char count);
static void init_idt(void);
static void bogus_interrupt(void);
static void fake_irq7(void);

void _start(void)
{
    int i;
    int next_stack = STACK_MIN;

    clear_screen();
    for (i = 0; i < NUM_TOTAL; i++) {
	pcb[i].pid = i;
	pcb[i].in_kernel = (i < NUM_THREADS) ? TRUE : FALSE;

	ASSERT(next_stack < STACK_MAX);
	pcb[i].kernel_stack = next_stack + STACK_OFFSET;
	next_stack += STACK_SIZE;

	if (!pcb[i].in_kernel) {
	    ASSERT(next_stack < STACK_MAX);
	    pcb[i].user_stack = next_stack + STACK_OFFSET;
	    next_stack += STACK_SIZE;
	}
	pcb[i].start_addr = start_addr[i];
	pcb[i].priority = 10;
	pcb[i].status = STATUS_FIRST_TIME;
	pcb[i].next = &pcb[(i + 1) % NUM_TOTAL];
	pcb[i].prev = &pcb[(i - 1 + NUM_TOTAL) % NUM_TOTAL];
    }
    init_idt();

    /* Enable the timer interrupt.  The interrupt flag will be set
     * once we start the first process, thus starting scheduling.
     * Refer to the IF flag in the EFLAGS register. */
    outb(0x21, 0xfe);

    current_running = &pcb[0];
    dispatch();
    ASSERT(0);				/* not reached */
}

void init_idt(void)
{
    int i;
    struct point_t idt_p;

    /* Rearrange irq vector mapping so that we have no overlap with
     * exception vectors. Refer to chapter 17 Undocumented PC. */
    outb(0x20, 0x11);
    outb(0xa0, 0x11);
    outb(0x21, 0x20);
    outb(0xa1, 0x28);
    outb(0x21, 0x04);
    outb(0xa1, 0x02);
    outb(0x21, 0x01);
    outb(0xa1, 0x01);
    outb(0xa1, 0xff);
    outb(0x21, 0xfb);

    /* set timer 0 frequency */
    outb(0x40, (unsigned char) PREEMPT_TICKS);
    outb(0x40, PREEMPT_TICKS >> 8);

    /* create default handlers for interrupts/exceptions */
    for (i = 0; i < IDT_SIZE; i++) {
	create_gate(&(idt[i]),
	    (uint32_t) bogus_interrupt,
	    KERNEL_CS,
	    INTERRUPT_GATE,
	    0,
	    0);
    }

    /* create gate for the fake interrupt generated on IRQ line 7 when
     * the timer is working at a high frequency */
    create_gate(&(idt[IRQ_START + 7]),
	(uint32_t) fake_irq7,
	KERNEL_CS,
	INTERRUPT_GATE,
	0,
	0);

    /* create gate for the timer interrupt */
    create_gate(&(idt[IRQ_START]),
	(uint32_t) irq0,
	KERNEL_CS,
	INTERRUPT_GATE,
	0,
	0);

    /* create gate for system calls */
    create_gate(&(idt[IDT_SYSCALL_POS]),
	(uint32_t) system_call_entry,
	KERNEL_CS,
	INTERRUPT_GATE,
	0,
	0);

    /* load the idtr with a pointer to our idt */
    idt_p.limit = (IDT_SIZE * 8) - 1;
    idt_p.base = (uint32_t) idt;

  asm("lidt %0": :"m"(idt_p));
}

void create_gate(
    struct gate_t *entry,
    uint32_t offset,
    uint16_t selector,
    char type,
    char privilege,
    char count)
{
    /* General function make a gate entry.  Refer to chapter 12, page
     * 203, in PMSA for a description of interrupt gates. */
    entry->offset_low = (uint16_t) offset;
    entry->selector = (uint16_t) selector;
    entry->count = (uint8_t) count & 0x1f;
    entry->access = type | privilege << 5 | 1 << 7;
    entry->offset_high = (uint16_t) (offset >> 16);
}

void bogus_interrupt(void)
{
    /* hang */
    print_str(2, 0, "In bogus_interrupts");
    while (1);
}

void fake_irq7(void)
{
    asm volatile ("iret");
}

void print_status(void)
{
    static char *status[] =
    {"First  ", "Running", "Blocked", "Exited "};
    int i, base;

    base = 20;
    print_str(base - 4, 5, "P R O C E S S    S T A T U S");
    print_str(base - 2, 0, "Pid");
    print_str(base - 2, 10, "Type");
    print_str(base - 2, 20, "Priority");
    print_str(base - 2, 30, "Status");
    for (i = 0; i < NUM_TOTAL; i++) {
	print_int(base + i, 0, pcb[i].pid);
	print_str(base + i, 10, pcb[i].in_kernel ? "Thread" : "Process");
	print_int(base + i, 20, pcb[i].priority);
	print_str(base + i, 30, status[pcb[i].status]);
    }
}
