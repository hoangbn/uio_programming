#include "common.h"
#include "kernel.h"
#include "thread.h"

#define LOCKED 1
#define UNLOCKED 0

void lock_init(lock_t *l)
{
    l->status = UNLOCKED;
    l->waiting = NULL;
}

void lock_acquire(lock_t *l)
{
    if (l->status == UNLOCKED) {
	l->status = LOCKED;
    } else {
	block(&l->waiting);
	yield();
    }
}

void lock_release(lock_t *l)
{
    if (l->waiting == NULL) {
	l->status = UNLOCKED;
    } else {
	unblock(&l->waiting);
    }
}

void condition_init(condition_t *c)
{
}

void condition_wait(lock_t *m, condition_t *c)
{
}

void condition_signal(condition_t *c)
{
}

void condition_broadcast(condition_t *c)
{
}
