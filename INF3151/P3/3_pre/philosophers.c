#include "thread.h"
#include "util.h"
#include "scheduler.h"
#include "screen.h"

/* Dining philosphers threads. */

enum {
  THINK_TIME = 9999,
  EAT_TIME = THINK_TIME,
};

volatile int forks_initialized = 0;
semaphore_t fork[3];
int num_eating = 0;
int scroll_eating = 0;
int caps_eating = 0;
/* Set to true if status should be printed to screen */
int print_to_screen;

/* Odd philospher */
void num(void)
{
  volatile int foo;
  int i, n;

  print_to_screen = 1;

  /* Initialize semaphores */
  semaphore_init(&fork[0], 1);
  semaphore_init(&fork[1], 1);
  semaphore_init(&fork[2], 1);
  forks_initialized = 1;
  if (print_to_screen) {
    scrprintf(PHIL_LINE, PHIL_COL, "Phil.");
    scrprintf(PHIL_LINE + 1, PHIL_COL, "Running");
  }

  while (1) {
    /* Think for a random time */
    n = rand() % THINK_TIME;
    for (i = 0; i < n; i++)
      if (foo % 2 == 0)
      	foo++;

    /* Grab left fork... */
    semaphore_down(&fork[0]);
    /* ... and grab right fork */
    semaphore_down(&fork[1]);

    num_eating = 1;
    /* With three forks only one philosopher at a time can eat */
    ASSERT(scroll_eating + caps_eating == 0);

    if (print_to_screen) {
      scrprintf(PHIL_LINE, PHIL_COL, "Phil.");
      scrprintf(PHIL_LINE + 1, PHIL_COL, "Num    ");
    }

    /* Eat for a random time */
    n = rand() % EAT_TIME;
    for (i = 0; i < n; i++)
      if (foo % 2 == 0)
      	foo++;

    num_eating = 0;

    /* Release forks */
    semaphore_up(&fork[0]);
    semaphore_up(&fork[1]);
  }
}

void caps(void)
{
  volatile int foo;
  int i, n;

  /* Wait until num hasd initialized forks */
  while (forks_initialized == 0)
    yield();

  while (1) {
    /* Think for a random time */
    n = rand() % THINK_TIME;
    for (i = 0; i < n; i++)
      if (foo % 2 == 0)
      	foo++;

    /* Grab right fork... */
    semaphore_down(&fork[2]);
    /* ... and grab left fork */
    semaphore_down(&fork[1]);

    caps_eating = 1;
    /* With three forks only one philosopher at a time can eat */
    ASSERT(scroll_eating + num_eating == 0);

    if (print_to_screen) {
      scrprintf(PHIL_LINE, PHIL_COL, "Phil.");
      scrprintf(PHIL_LINE + 1, PHIL_COL, "Caps   ");
    }

    /* Eat for a random time */
    n = rand() % EAT_TIME;
    for (i = 0; i < n; i++)
      if (foo % 2 == 0)
      	foo++;

    caps_eating = 0;

    /* Release forks */
    semaphore_up(&fork[2]);
    semaphore_up(&fork[1]);
  }
}

void scroll_th(void)
{
  volatile int foo;
  int i, n;

  /* Wait until num hasd initialized forks */
  while (forks_initialized == 0)
    yield();

  while (1) {
    /* Think for a random time */
    n = rand() % THINK_TIME;
    for (i = 0; i < n; i++)
      if (foo % 2 == 0)
      	foo++;

    /* Grab right fork... */
    semaphore_down(&fork[0]);
    /* ... and grab left fork */
    semaphore_down(&fork[2]);

    scroll_eating = 1;
    /* With three forks only one philosopher at a time can eat */
    ASSERT(caps_eating + num_eating == 0);

    if (print_to_screen) {
      scrprintf(PHIL_LINE, PHIL_COL, "Phil.");
      scrprintf(PHIL_LINE + 1, PHIL_COL, "Scroll ");
    }

    /* Eat for a random time */
    n = rand() % EAT_TIME;
    for (i = 0; i < n; i++)
      if (foo % 2 == 0)
      	foo++;

    scroll_eating = 0;

    /* Release forks */
    semaphore_up(&fork[0]);
    semaphore_up(&fork[2]);
  }
}
