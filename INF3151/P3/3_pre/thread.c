/*
 * Implementation of locks and condition variables
 */

#include "common.h"
#include "util.h"
#include "thread.h"
#include "scheduler.h"
#include "interrupt.h"



void lock_init(lock_t * l)
{
  /*
   * no need for critical section, it is callers responsibility to
   * make sure that locks are initialized only once
   */
  l->status = UNLOCKED;
  l->waiting = NULL;
}

/* Acquire lock whitout critical section (called within critical section) */
static void lock_acquire_helper(lock_t * l)
{
  if (l->status == UNLOCKED) l->status = LOCKED;
  else {
    block(&l->waiting);
  }
}


void lock_acquire(lock_t * l)
{
  static int lock = LOCKED;
  asm volatile ("xchg (%%eax), %%ebx":"=b"(lock):"a"(&l->status), "0"(LOCKED));
  if (lock == LOCKED) {
    enter_critical();
    block(&l->waiting);
    leave_critical();
  }	
}

void lock_release(lock_t * l)
{ 
  if (l->waiting != NULL) {   
    enter_critical();
    unblock(&l->waiting); 
    leave_critical();
  }
  else {
    l->status = UNLOCKED;
  }

}

/* condition functions */
void condition_init(condition_t * c)
{
  c->waiting = NULL;
}

/*
 * unlock m and block the thread (enqued on c), when unblocked acquire
 * lock m
 */
void condition_wait(lock_t * m, condition_t * c)
{
  enter_critical();
  lock_release(m);
  block(&c->waiting);
  lock_acquire_helper(m);
  leave_critical();
}

/* unblock first thread enqued on c */
void condition_signal(condition_t * c)
{
  enter_critical();
  if (c->waiting != NULL) {   
    unblock(&c->waiting); 
  }
  leave_critical();
}

/* unblock all threads enqued on c */
void condition_broadcast(condition_t * c)
{
  enter_critical();  
  while(c->waiting != NULL){
    unblock(&c->waiting);
  }
  leave_critical();
}

/* Semaphore functions. */
void semaphore_init(semaphore_t * s, int value)
{
  s->resource = value;
}

void semaphore_up(semaphore_t * s)
{
  enter_critical();
  s->resource++;                                     /*current_running increments it*/
  if(s->resource <= 0){                              /*if even after incrementing and the resource is 0 or negative, then it implies that a task were blocked before*/
    unblock(&s->waiting);    
  }
  leave_critical();
}

void semaphore_down(semaphore_t * s)
{
  enter_critical();
  s->resource--;                                     /*current_running decrements it*/
  if(s->resource < 0){                               /*if resource it a negative, it means that we didn't have resource to execute this task so we'll block it*/
    block(&s->waiting);
  }
  leave_critical();
}

/*
 * Barrier functions
 * Note that to test these functions you must set NUM_THREADS
 * (kernel.h) to 9 and uncomment the start_addr lines in kernel.c.
 */

/* n = number of threads that waits at the barrier */
void barrier_init(barrier_t * b, int n)
{
  condition_init(&b->cv);
  lock_init(&b->l);
  b->total = n;
  b->count = 0;
}

/* Wait at barrier until all n threads reach it */
void barrier_wait(barrier_t * b)
{
  lock_acquire(&b->l);                /*Make sure that a thread that recently left the barrier won't be coming*/
                                     /*in the same barrier to fast, since the lock is still in use*/
  b->count++;                        /*we increments the incoming threads before testing*/
  if(b->total == b->count){          /*We could just block all incoming threads and then increment it and test the result*/
    condition_broadcast(&b->cv);      /*But why block the last threads?, if the result of the waiting threads*/    
    b->count = 0;                                 /* and current_running is total then let them go through*/
  } else {
    condition_wait(&b->l, &b->cv);
  }
  lock_release(&b->l);                /*release the lock*/
}
