/*
 * Simple counter program, to check the functionality of yield().
 * Print time in seconds.
 */

#include "scheduler.h"
#include "th.h"
#include "util.h"
#include "screen.h"

#define MHZ 2000 /* CPU clock rate */

/* 
 * This thread runs indefinitely, which means that the 
 * scheduler should never run out of processes. 
 */
void clock_thread(void)
{
  unsigned int time;
  unsigned long long int ticks;
  unsigned int start_time;

  /* To show time since last boot, remove all references to start_time */
  ticks = get_timer() >> 20; /* divide on 2^20 = 10^6 (1048576) */
  /* divide by CPU clock frequency in megahertz */
  start_time = ((int) ticks) / MHZ; 
  while (1) {
    /* divide by 2^20 = 10^6 (1048576) */
    ticks = get_timer() >> 20; 
    /* divide by CPU clock frequency in megahertz */
    time = ((int) ticks) / MHZ; 
    scrprintf(CLOCK_LINE, CLOCK_STR, "Time (in seconds): %d", time - start_time);
    print_status();
    yield();
  }
}
