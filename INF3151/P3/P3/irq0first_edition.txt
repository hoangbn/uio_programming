 irq0_entry:
  # You must add code here to handle the timer interrupt.
  call	enter_critical
  call	switch_to_kernel_stack 		#wont switch if it shouldnt (has its own checks)
  pushl	%eax
  movl	(current_running), %eax
  cmpl	$1, PCB_IS_THREAD(%eax)
  je	is_thread
  cmpl	$2, PCB_NESTED_COUNT(%eax)
  je	is_thread 					#already interrupted process in kernel mode
  incl	PCB_NESTED_COUNT(%eax)
is_thread:
  popl	%eax
  call	scheduler_entry
  pushl	%eax
  movl	(current_running), %eax
  cmpl	$1, PCB_IS_THREAD(%eax)
  je	is_thread_2
  decl	PCB_NESTED_COUNT(%eax)
  call	switch_to_user_stack		#wont switch if it shouldnt (has its own checks)
is_thread_2:
  pushl %edx
  movb	$0x20, %al
  movb	$0x20, %dx
  outb	%al, %dx #EOI
  popl	%edx
  popl	%eax
  call	leave_critical_delayed #because iret pops old flags (which will enable interrupts)
  iret
_______________________________________________________________
 
  call 	enter_critical
  pushl	%eax
  movl	(preempt_count), %eax
  incl	%eax
  movl	%eax, (preempt_count)
  movl	current_running, %eax
  cmpl	$0, PCB_IS_THREAD(%eax)
  jne	irq0_continue
  movl	current_running, %eax
  cmpl	$2, PCB_NESTED_COUNT(%eax)
  je	irq0_continue
  movl	current_running, %eax
  cmpl	$1, PCB_NESTED_COUNT(%eax)
  je	increase_nested_count
  popl	%eax
  call	switch_to_kernel_stack
  pushl	%eax
increase_nested_count:
  movl	current_running, %eax
  incl	PCB_NESTED_COUNT(%eax)
irq0_continue:
  popl	%eax
  call	scheduler_entry
  pushl	%eax
  movl	current_running, %eax
  cmpl	$0, PCB_IS_THREAD(%eax)
  je	stay_kernel_mode #threads
  movl	current_running, %eax
  decl	PCB_NESTED_COUNT(%eax)
  movl	current_running, %eax
  cmpl	$1, PCB_NESTED_COUNT(%eax)
  je	stay_kernel_mode
  popl	%eax
  call	switch_to_user_stack
  pushl %eax
stay_kernel_mode:
  pushl %edx
  movb	$0x20, %al
  movw	$0x20, %dx
  outb	%al, %dx #EOI
  popl	%edx
  popl	%eax
