#include "fs.h"

#ifdef LINUX_SIM
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#endif /* LINUX_SIM */

#include "common.h"
#include "block.h"
#include "util.h"
#include "thread.h"
#include "inode.h"
#include "superblock.h"
#include "kernel.h"
#include "fs_error.h"

#define BITMAP_ENTRIES 256

#define INODE_TABLE_ENTRIES 20

static struct mem_superblock mem_super;
static struct disk_superblock disk_super;

static struct mem_inode inode_table[INODE_TABLE_ENTRIES];
static struct disk_inode dinode_table[INODE_TABLE_ENTRIES];

static struct fd_entry fd_table[MAX_OPEN_FILES];

static unsigned char inode_bmap[BITMAP_ENTRIES];
static unsigned char dblk_bmap[BITMAP_ENTRIES];

//static mem_inode[INODE_TABLE_ENTRIES];

static int get_free_entry(unsigned char *bitmap);
static int free_bitmap_entry(int entry, unsigned char *bitmap);
static inode_t name2inode(char *name);
static blknum_t ino2blk(inode_t ino);
static blknum_t idx2blk(int index);


/*
 * This function is called by the "loader_thread" after USB
 * subsystem has been initialized. 
 *
 * It should check whether there is a filesystem on the
 * disk and perform the necessary operations to prepare it
 * for usage. 
 */
void fs_init(void)
{
  
  block_init();
}

/*
 * Make a new file system. 
 *
 * The kernel_size is passed to _start(..) in kernel.c by
 * the bootloader.
 */
void fs_mkfs(void)
{
}

int fs_open(const char *filename, int mode)
{
  int fd;
  for(fd = 1; fd < MAX_OPEN_FILES; fd++){
    if(fd_table[fd].mode == MODE_UNUSED) break;
  }

  if(fd == MAX_OPEN_FILES) return -1;
  
  /*TODO make dirent and inode if the file does not exist*/
  
  /*******************************************************/
  
  fd_table[fd].mode = mode;
  /* fd_table[fd].idx = /*the inode number we got*/

  return fd;
}

int fs_close(int fd)
{
  fd_table[fd].mode = MODE_UNUSED;
  int inodenm = fd_table[fd].idx;
  
  if(inodenm >= INODE_TABLE_ENTRIES) return -1;

  dinode_table[inodenm].nlinks--;
  if(dinode_table[inodenm].nlinks <= 0){
    int i, blocknm;
    for(i = 0; i < INODE_NDIRECT;i++){
      blocknm = dinode_table[inodenm].direct[i];
      if(blocknm != -1){
	free_bitmap_entry(blocknm, dblk_bmap);
      }
    }
    /*need to reset?*/
    free_bitmap_entry(inodenm, inode_bmap);
    /*check if linkz are 0 if it is then remove the directory or files, dirent etc*/
  }

  return 0;
}

int fs_read(int fd, char *buffer, int size)
{
  /*calculate which block to start reading from fseek mb, copy it to buffer*/
  return -1;
}

int fs_write(int fd, char *buffer, int size)
{
  /*calulate which block to start writing to, use bcopy using buffer as 1 argument then sending it to block*/
  int fileSize = 0;
 /*If the size is bigger than filesize then we've to allocate more blocks*/
  if(size > fileSize){
    int sum = size - fileSize, block_size = BLOCK_SIZE, blocks = 1;
    
    while(block_size < sum){
      blocks++;
      block_size += BLOCK_SIZE;
    }
    
    int i, blk;
    for(i = 0; i < block_size;i++){
      blk = get_free_block(fd);
      if(blk == -1) return -1;
    }

  } else {


  }

  return -1;
}

/*
 * fs_lseek:
 * This function is really incorrectly named, since neither its offset
 * argument or its return value are longs (or off_t's). Also, it will
 * cause blocks to allocated if it extends the file (holes are not
 * supported in this simple filesystem).
 */
int fs_lseek(int fd, int offset, int whence)
{
  struct mem_inode temp = inode_table[fd_table[fd].idx];
  int size = 0, fileSize = temp.d_inode.size;
  
  switch(whence){
  case SEEK_SET: size = 0 + offset;
  case SEEK_CUR: size = temp.pos + offset;
  case SEEK_END: size = fileSize + size;
  default: return -1;
  }
  
  return size;
}

int filename_exist(char *dirname)
{
  /* directory_t temp_dir = current_dir.file.next_dir; */
  /* file_t temp_file = current_dir.file.next_file; */
  /* int length = strlen(dirname); */
  
  /* /\*Check all directories*\/ */
  /* while(temp_dir != NULL){ */
  /*   if(strncmp(dirname, temp_dir.file.this.name, length) == 0){ */
  /*     return -1; */
  /*   } */
  /*   temp_dir = temp_dir.file.next_dir; */
  /* } */

  /* while(temp != NULL){ */


  /* } */
  return -1;
}

int fs_mkdir(char *dirname)
{
  
  
  return -1;
}

int fs_chdir(char *path)
{
  return -1;
}

int fs_rmfile(char *filename)
{

  return -1;
}

int fs_rmdir(char *path)
{
  // char name[MAX_FILENAME_LEN];
  return -1;
}

int fs_link(char *linkname, char *filename)
{
  return -1;
}

int fs_unlink(char *linkname)
{
  
  return -1;
}


int fs_stat(int fd, char *buffer)
{
  if(fd_table[fd].mode == MODE_UNUSED) return -1;
  
  char cast;
  struct disk_inode temp = dinode_table[fd_table[fd].idx];
  cast = (char) temp.type;
  bcopy(&cast, &buffer[0], 1);

  cast = (char) temp.nlinks;
  bcopy(&cast, &buffer[1], 1);

  bcopy((char *)&temp.size, &buffer[2], 4);
  return 0;
}

/* 
 * Helper functions for the system calls
 */


int get_free_block(int fd)
{
  struct disk_inode temp = dinode_table[fd_table[fd].idx];
  int i;
  for(i = 0; i < INODE_NDIRECT; i++){
    if(temp.direct[i] == -1) return i;
  }
  
  return -1;
}

static inode_t get_free_inode(void) 
{
  int i = get_free_entry((unsigned char *)inode_bmap);
        if (i >= INODE_TABLE_ENTRIES) return (inode_t) -1;
        return (inode_t) i;
}
 
static blknum_t get_free_dblock(void)
{
  int b = get_free_entry((unsigned char *)dblk_bmap);
        if (b >= BLOCK_SIZE) return (blknum_t) -1;
        return (blknum_t) b;
}

/*
 * get_free_entry:
 * 
 * Search the given bitmap for the first zero bit.  If an entry is
 * found it is set to one and the entry number is returned.  Returns
 * -1 if all entrys in the bitmap are set.
 */
static int get_free_entry(unsigned char *bitmap)
{
  int i;

  /* Seach for a free entry */
  for (i = 0; i < BITMAP_ENTRIES / 8; i++) {
    if (bitmap[i] == 0xff)  /* All taken */
      continue;
    if ((bitmap[i] & 0x80) == 0) {  /* msb */
      bitmap[i] |= 0x80;
      return i * 8;
    } else if ((bitmap[i] & 0x40) == 0) {
      bitmap[i] |= 0x40;
      return i * 8 + 1;
    } else if ((bitmap[i] & 0x20) == 0) {
      bitmap[i] |= 0x20;
      return i * 8 + 2;
    } else if ((bitmap[i] & 0x10) == 0) {
      bitmap[i] |= 0x10;
      return i * 8 + 3;
    } else if ((bitmap[i] & 0x08) == 0) {
      bitmap[i] |= 0x08;
      return i * 8 + 4;
    } else if ((bitmap[i] & 0x04) == 0) {
      bitmap[i] |= 0x04;
      return i * 8 + 5;
    } else if ((bitmap[i] & 0x02) == 0) {
      bitmap[i] |= 0x02;
      return i * 8 + 6;
    } else if ((bitmap[i] & 0x01) == 0) {  /* lsb */
      bitmap[i] |= 0x01;
      return i * 8 + 7;
    }
  }
  return -1;
}

/*
 * free_bitmap_entry:
 *
 * Free a bitmap entry, if the entry is not found -1 is returned, otherwise zero. 
 * Note that this function does not check if the bitmap entry was used (freeing
 * an unused entry has no effect).
 */
static int free_bitmap_entry(int entry, unsigned char *bitmap)
{
  unsigned char *bme;

  if (entry >= BITMAP_ENTRIES)
    return -1;

  bme = &bitmap[entry / 8];

  switch (entry % 8) {
  case 0:
    *bme &= ~0x80;
    break;
  case 1:
    *bme &= ~0x40;
    break;
  case 2:
    *bme &= ~0x20;
    break;
  case 3:
    *bme &= ~0x10;
    break;
  case 4:
    *bme &= ~0x08;
    break;
  case 5:
    *bme &= ~0x04;
    break;
  case 6:
    *bme &= ~0x02;
    break;
  case 7:
    *bme &= ~0x01;
    break;
  }

  return 0;
}



/*
 * ino2blk:
 * Returns the filesystem block (block number relative to the super
 * block) corresponding to the inode number passed.
 */
static blknum_t ino2blk(inode_t ino)
{
  return (blknum_t)-1;
}

/*
 * idx2blk:
 * Returns the filesystem block (block number relative to the super
 * block) corresponding to the data block index passed.
 */
static blknum_t idx2blk(int index)
{
  return (blknum_t)-1;
}

/*
 * name2inode:
 * Parses a file name and returns the corresponding inode number. If
 * the file cannot be found, -1 is returned.
 */
static inode_t name2inode(char *name)
{
  return (inode_t)-1;
}
