#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct h{
  int a;
  char name[4];
};

int main()
{
  char buffer[(2*sizeof(struct h))];
  
  struct h *s = (struct h *) buffer;
  s->a = 6;
  s->name[0] = 'h'; 
  s->name[1] = 'e'; 
  s->name[2] = 'i';
  s->name[3] = 0;
  
  s = (struct h *) (buffer + sizeof(struct h));
  s->a = 5;
  s->name[0] = 'y'; 
  s->name[1] = 'o'; 
  s->name[2] = 'u'; 
  s->name[3] = 0;


  struct h *b = (struct h*) buffer;
  printf("%d, %s\n", b->a, b->name);
  b = (struct h*) (buffer + sizeof(struct h));
  printf("%d, %s\n", b->a, b->name);
}
