# 23 "entry.S"
# Save all general purpose registers on the kernel stack
# 33 "entry.S"
# Restore all general purpose registers from the kernel stack
# 43 "entry.S"
# Save eflags on the kernel stack


# Save eflags on the kernel stack


# We need the kernel stack offset in the PCB when saving and restoring the stack in
# scheduler() and dispatch_saved()


.data
        .align 4

.text
.code32
        .align 4

# Make symbols visible for ld
.globl system_call_entry
.globl scheduler
.globl dispatch_saved
.globl irq0 # Timer
.globl irq1 # Keyboard
.globl irq6 # Floppy
.globl fake_irq7
.globl exception_14 # Page fault

# In syslib.c we put systemcall number in eax, arg1 in eb, arg2 in ecx
# and arg3 in edx. The return value is returned in eax.

# Before entering the processor has switched to the kernel stack
# (PMSA p. 209, Privilege level switch whitout error code)

system_call_entry:
        # Save all general purpose registers except eax where the return value
        # is placed
        pushl %ebx
        pushl %ecx
        pushl %edx
        pushl %edi
        pushl %esi
        pushl %ebp

        # Save user stack segment. The data segment is switched to the kernel
        # data segment in interrupt.c (code and stack segment switched by HW)
        pushl %ds

        pushl %edx
        pushl %ecx
        pushl %ebx
        pushl %eax
        call system_call_entry_helper
        addl $16, %esp

        # Switch ds to user data segment
        popl %ds

        # Restore all general purpose registers saved previously
        popl %ebp
        popl %esi
        popl %edi
        popl %edx
        popl %ecx
        popl %ebx

        # Return value is already in eax

        # Switch to user stack segment and restore user stack
        iret

irq0:
        pushl %eax; pushl %ebx; pushl %ecx; pushl %edx; pushl %edi; pushl %esi; pushl %ebp

        # Save user stack segment. The data segment is switched to the kernel
        # data segment in interrupt.c (code and stack segment switched by HW)
        pushl %ds

        call irq0_helper

        # Restore user data segment. The processor will restore the user
        # stack segment and user code segment (if current running is a
        # process)
        popl %ds
        popl %ebp; popl %esi; popl %edi; popl %edx; popl %ecx; popl %ebx; popl %eax

        # The processor will restore the user stack (if current running is a
        # process)
        iret

irq1:
        pushl %eax; pushl %ebx; pushl %ecx; pushl %edx; pushl %edi; pushl %esi; pushl %ebp

        # Save user stack segment. The data segment is switched to the kernel
        # data segment in interrupt.c (code and stack segment switched by HW)
        pushl %ds

        call irq1_helper

        # Restore user data segment. The processor will restore the user
        # stack segment and user code segment (if current running is a
        # process)
        popl %ds
        popl %ebp; popl %esi; popl %edi; popl %edx; popl %ecx; popl %ebx; popl %eax

        # The processor will restore the user stack (if current running is a
        # process)
        iret

irq6:
        pushl %eax; pushl %ebx; pushl %ecx; pushl %edx; pushl %edi; pushl %esi; pushl %ebp

        # Save user stack segment. The data segment is switched to the kernel
        # data segment in interrupt.c (code and stack segment switched by HW)
        pushl %ds

        call irq6_helper

        # Restore user data segment. The processor will restore the user
        # stack segment and user code segment (if current running is a
        # process)
        popl %ds
        popl %ebp; popl %esi; popl %edi; popl %edx; popl %ecx; popl %ebx; popl %eax

        # The processor will restore the user stack (if current running is a
        # process)
        iret

fake_irq7:
        pushl %eax; pushl %ebx; pushl %ecx; pushl %edx; pushl %edi; pushl %esi; pushl %ebp

        # Save user stack segment. The data segment is switched to the kernel
        # data segment in interrupt.c (code and stack segment switched by HW)
        pushl %ds

        call fake_irq7_helper

        # Restore user data segment. The processor will restore the user
        # stack segment and user code segment (if current running is a
        # process)
        popl %ds
        popl %ebp; popl %esi; popl %edi; popl %edx; popl %ecx; popl %ebx; popl %eax

        # The processor will restore the user stack (if current running is a
        # process)
        iret

# Stack contents when entering from lower privilege level:
# Error Code, EIP, CS, Eflags, ESP, SS
# Stack contents when entering from same privilege level:
# Error Code, EIP, CS, Eflags
exception_14:
        pushl %eax; pushl %ebx; pushl %ecx; pushl %edx; pushl %edi; pushl %esi; pushl %ebp

        # Save user stack segment. The data segment is switched to the kernel
        # data segment in interrupt.c (code and stack segment switched by HW)
        pushl %ds

        call exception_14_helper

        # Restore user data segment. The processor will restore the user
        # stack segment and user code segment (if current running is a
        # process)
        popl %ds
        popl %ebp; popl %esi; popl %edi; popl %edx; popl %ecx; popl %ebx; popl %eax

        # Pop error code
        addl $4, %esp

        # The processor will restore the user stack (if current running is a
        # process)
        iret

# Must be called within a critcal section.
# Save context and call scheduler_helper to picks the next job to run
scheduler:

        # Save the context of the thread that will be scheduled out, in the
        # kernel stack. The context will be restored in dispatch().
        pushl %eax; pushl %ebx; pushl %ecx; pushl %edx; pushl %edi; pushl %esi; pushl %ebp
        pushfl

        # Save kernel stack pointer (restored in dispatch)
        movl current_running, %eax
        movl %esp, 12(%eax) # Save esp into cr->kernel_stack

        call scheduler_helper

        # We should never return from the call
        cli
        hlt

# Dispatch a thread whose context was saved in scheduler()
dispatch_saved:
        # Restore kernel stack
        movl current_running, %eax
        movl 12(%eax), %esp # move cr->kernel_stack into esp

        popfl
        popl %ebp; popl %esi; popl %edi; popl %edx; popl %ecx; popl %ebx; popl %eax

        ret
        # The processor pops the return address from the stack and returns to
        # the function that called scheduler()
