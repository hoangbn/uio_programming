#include "fs.h"

#ifdef LINUX_SIM
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#endif LINUX_SIM 

#include "common.h"
#include "block.h"
#include "util.h"
#include "thread.h"
#include "inode.h"
#include "superblock.h"
#include "kernel.h"
#include "fs_error.h"

#define BITMAP_ENTRIES 256

#define INODE_TABLE_ENTRIES 20

/* fd assigned to internal read/writes for directory files. 
 * we keep this available so that we can still traverse the 
 * file system, even if all available fd's are used up by files.
 */
#define FD_DIR 0

static struct mem_superblock mem_super;
static struct disk_superblock disk_super;

static struct mem_inode inode_table[INODE_TABLE_ENTRIES];
static struct disk_inode dinode_table[INODE_TABLE_ENTRIES];

static inode_t root, cwd;

static struct fd_entry fd_table[MAX_OPEN_FILES];

static char inode_bmap[BITMAP_ENTRIES];
static char dblk_bmap[BITMAP_ENTRIES];

static int get_free_entry(unsigned char *bitmap);
static int free_bitmap_entry(int entry, unsigned char *bitmap);
static inode_t name2inode(char *name);
static blknum_t ino2blk(inode_t ino);
static blknum_t idx2blk(int index);
lock_t map_lock;
lock_t ino_lock;
lock_t fdt_lock;


/*
 * This function is called by the "loader_thread" after USB
 * subsystem has been initialized. 
 *
 * It should check whether there is a filesystem on the
 * disk and perform the necessary operations to prepare it
 * for usage. 
 */
void fs_init(void)
{
	lock_init(&map_lock);
	lock_init(&ino_lock);
	lock_init(&fdt_lock);
	block_init();
	fs_mkfs();
}

/*
 * Make a new file system. 
 *
 * The kernel_size is passed to _start(..) in kernel.c by
 * the bootloader.
 */
void fs_mkfs(void)
{
	lock_acquire(&map_lock);
	lock_acquire(&ino_lock);
	lock_acquire(&fdt_lock);
	disk_super.magic_number = MAGIC_NUMBER;
	disk_super.ninodes = INODE_TABLE_ENTRIES;
	disk_super.ndata_blks = BLOCKS;
	int i;
	for (i=0; i<BITMAP_ENTRIES; i++) {
		inode_bmap[i] = 0;
		dblk_bmap[i] = 0;
	}
	root = cwd = get_free_inode();
	disk_super.root_inode = root;
	disk_super.root_inode_block = ino2blk(disk_super.root_inode);
	disk_super.max_filesize = (BLOCK_SIZE * INODE_NDIRECT);
	mem_super.d_super = disk_super;
	mem_super.ibmap = inode_bmap;
	mem_super.dbmap = dblk_bmap;
	mem_super.dirty = 0;
	for (i=0; i<INODE_TABLE_ENTRIES; i++) {
		dinode_table[i].type = 0;
		dinode_table[i].size = 0;
		dinode_table[i].nlinks = 0;
		int j;
		for (j=0; j<INODE_NDIRECT;j++) {
			dinode_table[i].direct[j] = UNUSED_BLOCK;
		}
		inode_table[i].d_inode = &dinode_table[i];
		inode_table[i].open_count = 0;
		inode_table[i].pos = 0;
		inode_table[i].inode_num = (inode_t) i;
		inode_table[i].dirty = 0;
	}
	for (i=0; i<MAX_OPEN_FILES; i++) {
		fd_table[i].mode = MODE_UNUSED;
	}
	fd_table[0].mode = MODE_RDWR; /* read/write for internal FD for directory files */
	dinode_table[disk_super.root_inode].type = INTYPE_DIR;
	dinode_table[disk_super.root_inode].direct[0] = get_free_dblock();
	dinode_table[disk_super.root_inode].nlinks++;

	struct dirent std_entries[2];
	std_entries[0].inode = root;
	std_entries[1].inode = root;
	char *first = ".";
	char *second = "..";
	strncpy(std_entries[0].name, first, 2);
	strncpy(std_entries[1].name, second, 3);
	fd_table[FD_DIR].idx = root;
	lock_release(&map_lock);
	lock_release(&ino_lock);
	lock_release(&fdt_lock);
	fs_write(FD_DIR, (char *)std_entries, (2 * sizeof(struct dirent)));
}

int walk_path(char *path){
  int count = 0;
  cwd = root;
  char name[MAX_FILENAME_LEN];

  if(++path == '\0') {
    cwd = name2inode(".");
    return cwd;
  } else {
    while(*path != '\0'){
      if(*path == '/'){
	name[count] = '\0';
	count = 0;
	cwd = name2inode(name);
	if(cwd == -1) return -1;
      } else {
	name[count++] = *path;
      }
      path++;
    }
    return cwd;
  }
  return -1;
}


int fs_open(const char *filename, int mode)
{
  int file_inode, tmp_cwd = cwd;
  // char *argv[MAX_FILENAME_LEN];
  
  char path[MAX_PATH_LEN];
  strcpy(path, (char *)filename);
  if(path[0] == '/'){
    if (path[1] == '\0') {
      file_inode = name2inode(".");
    } else {
      file_inode = walk_path(path);
      cwd = tmp_cwd;
      if(file_inode < 0) return -1;
      
      /* for(i = 0; i < counter; i++){ */
      /*   printf("Inside %s\n", argv[i]); */
      /*   if((file_inode = name2inode((char*)argv[i])) == -1){ */
      /*     cwd = tmp_cwd; */
      /*     break; */
      /*   } */
      
      /*   cwd = file_inode; */
      /* } */
    
    }
    lock_acquire(&fdt_lock);
    int fd = get_free_fd();
    if (fd == -1) {
      lock_release(&fdt_lock);
      return fd;
    }
    fd_table[fd].mode = mode;
    fd_table[fd].idx = file_inode;
    lock_release(&fdt_lock);
    lock_acquire(&ino_lock);
    dinode_table[file_inode].nlinks++;
    lock_release(&ino_lock);
    inode_table[fd_table[fd].idx].open_count++;
    return fd;
  } else {
    file_inode = name2inode((char *)filename);
    if (file_inode >= INODE_TABLE_ENTRIES) return -1;
    if (file_inode < 0) {
      //create file
      //	printf("\n\n%sAAAAAAAAA\n\n", filename);
      lock_acquire(&map_lock);
      lock_acquire(&ino_lock);
      file_inode = get_free_inode();
      dinode_table[file_inode].nlinks++;
      dinode_table[file_inode].type = INTYPE_FILE;
      lock_release(&map_lock);
      lock_release(&ino_lock);
      char d[sizeof(struct dirent)];
      struct dirent *dir_ent;
      dir_ent = (struct dirent*)d;
		dir_ent->inode = file_inode;
		strncpy(dir_ent->name, filename, strlen(filename)+1);
		fd_table[FD_DIR].idx = cwd;
		fs_write(FD_DIR, (char *)dir_ent, sizeof(struct dirent));
    }
    //open file
    lock_acquire(&fdt_lock);
    int fd = get_free_fd();
    if (fd == -1) {
      lock_release(&fdt_lock);
      return fd;
    }
    fd_table[fd].mode = mode;
    fd_table[fd].idx = file_inode;
    lock_release(&fdt_lock);
    lock_acquire(&ino_lock);
    dinode_table[file_inode].nlinks++;
    lock_release(&ino_lock);
    inode_table[fd_table[fd].idx].open_count++;
    return fd;
  }
}

int fs_close(int fd)
{
	lock_acquire(&fdt_lock);
	fd_table[fd].mode = MODE_UNUSED;
	dinode_table[fd_table[fd].idx].nlinks--;
	inode_table[fd_table[fd].idx].pos = 0;
	if (dinode_table[fd_table[fd].idx].nlinks == 0) { 
		lock_acquire(&ino_lock);
		clear_inode(fd_table[fd].idx);
		lock_release(&ino_lock);
	} else if (dinode_table[fd_table[fd].idx].nlinks < 0) {	
		dinode_table[fd_table[fd].idx].nlinks = 0;
		lock_release(&fdt_lock);
		return -1;
	}
	lock_release(&fdt_lock);
	return 0;
}

int fs_read(int fd, char *buffer, int size)
{
  if (fd < 0 || fd > MAX_OPEN_FILES) return -1;
  int start = inode_table[fd_table[fd].idx].pos;
  int stop = start + size;
  int blocks_to_read = ((stop/BLOCK_SIZE) - (start/BLOCK_SIZE)) +1;
  char buf[blocks_to_read*BLOCK_SIZE];
  
  //printf("\n dinode read %d \n", fd_table[fd].idx  );

  int i;
  for (i=0; i<blocks_to_read; i++) {
    if (dinode_table[fd_table[fd].idx].direct[(start/BLOCK_SIZE)+i] != -1)
      block_read(idx2blk(dinode_table[fd_table[fd].idx].direct[(start/BLOCK_SIZE)+i]), &buf[i*BLOCK_SIZE]);
  }
  int j = 0;
  for (i=start; i<stop; i++) {
    if (i >= dinode_table[fd_table[fd].idx].size) break;
    ((char *)buffer)[j] = ((char *)buf)[i];
    j++;
  }
  inode_table[fd_table[fd].idx].pos += i-start;
  
  return i-start;
}

int fs_write(int fd, char *buffer, int size)
{
	if (fd < 0 || fd > MAX_OPEN_FILES) return -1;
	lock_acquire(&ino_lock);
	lock_acquire(&fdt_lock);
	lock_acquire(&map_lock);
	if (dinode_table[fd_table[fd].idx].direct[0] == UNUSED_BLOCK) {
		dinode_table[fd_table[fd].idx].direct[0] = get_free_dblock();
	}
	int declared_space_left = BLOCK_SIZE - (dinode_table[fd_table[fd].idx].size % BLOCK_SIZE);
	int space_to_allocate = size - declared_space_left;
	if (space_to_allocate > 0) {
		int blocks_to_allocate = (space_to_allocate / BLOCK_SIZE) +1;
		int i;
		for (i=0; i<blocks_to_allocate; i++) {
			allocate_dblock(fd_table[fd].idx);
		}
	}
	int end = dinode_table[fd_table[fd].idx].size + size;
	int i = dinode_table[fd_table[fd].idx].size;
	
	while (i < end && i < disk_super.max_filesize) {
		if (declared_space_left > 0) {
			if (size < declared_space_left) {
				block_modify(idx2blk(dinode_table[fd_table[fd].idx].direct[i/BLOCK_SIZE]), BLOCK_SIZE-declared_space_left, buffer, size);
				i += size;
			} else {
				block_modify(idx2blk(dinode_table[fd_table[fd].idx].direct[i/BLOCK_SIZE]), BLOCK_SIZE-declared_space_left, buffer, declared_space_left);
				i += declared_space_left;
			}
			declared_space_left = 0;
		}
		else if ((end - i) >= BLOCK_SIZE) {
			block_write(idx2blk(dinode_table[fd_table[fd].idx].direct[i/BLOCK_SIZE]), buffer);
			i += BLOCK_SIZE;
		} else {
			block_modify(idx2blk(dinode_table[fd_table[fd].idx].direct[i/BLOCK_SIZE]), 0, buffer, end - i);
			i = end;
		}
	}
	//	printf("\inode: %d, norig size: %d, +size: %d, new size: %d\n", fd_table[fd].idx, dinode_table[fd_table[fd].idx].size, size, dinode_table[fd_table[fd].idx].size + size);
	dinode_table[fd_table[fd].idx].size += size - (end - i);
	write_inodes_to_disk();
	lock_release(&ino_lock);
	lock_release(&fdt_lock);
	lock_release(&map_lock);
	return size - (end - i);
}

/*
 * fs_lseek:
 * This function is really incorrectly named, since neither its offset
 * argument or its return value are longs (or off_t's). Also, it will
 * cause blocks to allocated if it extends the file (holes are not
 * supported in this simple filesystem).
 */
int fs_lseek(int fd, int offset, int whence)
{
	return -1;
}

int fs_mkdir(char *dirname)
{
	inode_t inode = name2inode(dirname);
	if (inode != -1) return -1;
	lock_acquire(&map_lock);
	lock_acquire(&ino_lock);
	inode = get_free_inode();
	lock_release(&map_lock);
	if (inode == -1) {
		lock_release(&ino_lock);
		return -1;
	}
	char d[sizeof(struct dirent)];
	struct dirent *temp;
	temp = (struct dirent *)d;
	temp->inode = inode;
	strncpy(temp->name, dirname, strlen(dirname)+1);
	dinode_table[inode].type = INTYPE_DIR;
	dinode_table[inode].nlinks++;
	fd_table[FD_DIR].idx = cwd;
	lock_release(&ino_lock);
	fs_write(FD_DIR, (char *)temp, sizeof(struct dirent));
	lock_acquire(&ino_lock);
	struct dirent std_entries[2];
	std_entries[0].inode = inode;
	std_entries[1].inode = cwd;
	char *first = ".";
	char *second = "..";
	strncpy(std_entries[0].name, first, 2);
	strncpy(std_entries[1].name, second, 3);
	fd_table[FD_DIR].idx = inode;
	lock_release(&ino_lock);
	fs_write(FD_DIR, (char *)std_entries, (2 * sizeof(struct dirent)));
	return 1;
}


int fs_parse_path(char *path, char *argv[MAX_FILENAME_LEN])
{ 
  int count = 0;

  argv[count++] = path;  

  while(*path != '\0'){
    if (*path == '/') {
      *path = '\0';
      argv[count++] = (path + 1);
    }
    path++;
  }
  return count;
}


int fs_chdir(char *path)
{
  /*   test / . ..  */
  int inode, counter, tmp_cwd = cwd, path_found = TRUE;
  if(strlen(path) >= MAX_PATH_LEN) return -1;
  char *argv[MAX_FILENAME_LEN];
  
  counter = fs_parse_path(path, argv);

  int i;
  for(i = 0; i < counter; i++){
    if((inode = name2inode(argv[i])) == -1) path_found = FALSE;
    cwd = inode;
  }
  
  if(!path_found){
    cwd = tmp_cwd;
    return -1;
  }
  
  /* int j; */
  /* for(j = 0; j < len; j++){ */
  /*   if(buf[j] == '/') buf[j] = '\0'; */
  /* } */
 
  /* for(i = 0; i < counter; i++){ */
  /*   printf("%s\n", argv[i]); */
  /* } */

  return 0;

}

int fs_rmdir(char *path)
{
  lock_acquire(&fdt_lock);
  int dir_inode;
  if (strncmp(path, "/", 2) == 0) dir_inode = name2inode(".");
  else dir_inode = name2inode(path);
  if (dir_inode >= INODE_TABLE_ENTRIES) return -1;
  
  if(dinode_table[dir_inode].type != INTYPE_DIR) return -1;
  else if(dinode_table[dir_inode].size > 40) return -1;
  else {
    lock_acquire(&ino_lock);
    remove_dirent(dir_inode);
    clear_inode(dir_inode); 
    lock_release(&ino_lock);
  }
  lock_release(&fdt_lock);
  return 0;
}

int fs_link(char *linkname, char *filename)
{
  
	return -1;
}

int fs_unlink(char *linkname)
{
  
	return -1;
}


int fs_stat(int fd, char *buffer)
{
   if(fd_table[fd].mode == MODE_UNUSED) return -1;
  
  char cast;
  struct disk_inode temp = dinode_table[fd_table[fd].idx];
  cast = (char) temp.type;
  bcopy(&cast, &buffer[0], 1);

  cast = (char) temp.nlinks;
  bcopy(&cast, &buffer[1], 1);

  bcopy((char *)&temp.size, &buffer[2], 4);
  return 0;
}

/* 
 * Helper functions for the system calls
 */
 
blknum_t allocate_dblock(inode_t in)
{
	int i;
	for (i=0; i<INODE_NDIRECT; i++) {
		if (dinode_table[in].direct[i] == UNUSED_BLOCK) {
			dinode_table[in].direct[i] = get_free_dblock();
			return dinode_table[in].direct[i];
		}
	}
	return -1;
}
 
void clear_inode(inode_t i) 
{
	if (i >= INODE_TABLE_ENTRIES) return;
	inode_table[i].dirty = 1;
	dinode_table[i].type = 0;
	dinode_table[i].size = 0;
	int j;
	for (j=0; j<INODE_NDIRECT; j++) {
		free_bitmap_block(dinode_table[i].direct[j]);
		dinode_table[i].direct[j] = UNUSED_BLOCK;
	}
	inode_table[i].open_count = 0;
	inode_table[i].pos = 0;
	free_bitmap_inode(i);
	write_inodes_to_disk();
}
 
inode_t get_free_inode(void) 
{
	int i = get_free_entry((unsigned char *)inode_bmap);
	if (i >= INODE_TABLE_ENTRIES) return (inode_t) -1;
	return (inode_t) i;
}
 
blknum_t get_free_dblock(void)
{
	int b = get_free_entry((unsigned char *)dblk_bmap);
	if (b >= BLOCKS) return (blknum_t) -1;
	return (blknum_t) b;
}

int get_free_fd(void)
{
	int i;
	for (i=1; i<MAX_OPEN_FILES; i++) {
		if (fd_table[i].mode == MODE_UNUSED) return i;
	}
	return -1;
}

int free_bitmap_inode(inode_t i)
{
	if (i < 0 || i >= INODE_TABLE_ENTRIES) return -1;
	int ret = free_bitmap_entry(i, (unsigned char *)inode_bmap);
	return ret;
}

int free_bitmap_block(blknum_t b)
{
	if (b < 0 || b >= BLOCKS) return -1;
	int ret = free_bitmap_entry(b, (unsigned char *)dblk_bmap);
	return ret;
}

void write_inodes_to_disk(void)
{
	block_write(ino2blk(0), dinode_table);
	int i;
	for (i=0; i<INODE_TABLE_ENTRIES; i++) {
		inode_table[i].dirty = 0;
	}
}

int remove_dirent(inode_t in)
{
  char buf[dinode_table[cwd].size], buf2[dinode_table[cwd].size-sizeof(struct dirent)];
  
  struct dirent *temp;
  
  fd_table[FD_DIR].idx = cwd;
	
  inode_table[cwd].pos = 0;
  fs_read(FD_DIR, buf, dinode_table[cwd].size);
  inode_table[cwd].pos = 0;

	int ret = -1;
	int i, j = 0;
	for (i=0; i<dinode_table[cwd].size; i+=sizeof(struct dirent)) {
	  temp = (struct dirent *) &buf[i];
	 
	  if (in != temp->inode){	    	   
	    bcopy(&buf[i], &buf2[j], sizeof(struct dirent));
	    j += sizeof(struct dirent);
	  } else { 	
	    ret = 1; //found dirent with inode marked for deletion.
	  }
	}
	if (ret == 1) {
	  inode_table[cwd].dirty = 1;
	  int size = dinode_table[cwd].size - sizeof(struct dirent);
	  dinode_table[fd_table[FD_DIR].idx].size = 0;
	  fs_write(FD_DIR, buf2, size);
	  inode_table[cwd].dirty = 0;
	}
	return ret;
}

/*
 * get_free_entry:
 * 
 * Search the given bitmap for the first zero bit.  If an entry is
 * found it is set to one and the entry number is returned.  Returns
 * -1 if all entrys in the bitmap are set.
 */
static int get_free_entry(unsigned char *bitmap)
{
  int i;

  /* Seach for a free entry */
  for (i = 0; i < BITMAP_ENTRIES / 8; i++) {
    if (bitmap[i] == 0xff)  /* All taken */
      continue;
    if ((bitmap[i] & 0x80) == 0) {  /* msb */
      bitmap[i] |= 0x80;
      return i * 8;
    } else if ((bitmap[i] & 0x40) == 0) {
      bitmap[i] |= 0x40;
      return i * 8 + 1;
    } else if ((bitmap[i] & 0x20) == 0) {
      bitmap[i] |= 0x20;
      return i * 8 + 2;
    } else if ((bitmap[i] & 0x10) == 0) {
      bitmap[i] |= 0x10;
      return i * 8 + 3;
    } else if ((bitmap[i] & 0x08) == 0) {
      bitmap[i] |= 0x08;
      return i * 8 + 4;
    } else if ((bitmap[i] & 0x04) == 0) {
      bitmap[i] |= 0x04;
      return i * 8 + 5;
    } else if ((bitmap[i] & 0x02) == 0) {
      bitmap[i] |= 0x02;
      return i * 8 + 6;
    } else if ((bitmap[i] & 0x01) == 0) {  /* lsb */
      bitmap[i] |= 0x01;
      return i * 8 + 7;
    }
  }
  return -1;
}

/*
 * free_bitmap_entry:
 *
 * Free a bitmap entry, if the entry is not found -1 is returned, otherwise zero. 
 * Note that this function does not check if the bitmap entry was used (freeing
 * an unused entry has no effect).
 */
static int free_bitmap_entry(int entry, unsigned char *bitmap)
{
  unsigned char *bme;

  if (entry >= BITMAP_ENTRIES)
    return -1;

  bme = &bitmap[entry / 8];

  switch (entry % 8) {
  case 0:
    *bme &= ~0x80;
    break;
  case 1:
    *bme &= ~0x40;
    break;
  case 2:
    *bme &= ~0x20;
    break;
  case 3:
    *bme &= ~0x10;
    break;
  case 4:
    *bme &= ~0x08;
    break;
  case 5:
    *bme &= ~0x04;
    break;
  case 6:
    *bme &= ~0x02;
    break;
  case 7:
    *bme &= ~0x01;
    break;
  }

  return 0;
}


/*
 * ino2blk:
 * Returns the filesystem block (block number relative to the super
 * block) corresponding to the inode number passed.
 */
static blknum_t ino2blk(inode_t ino)
{
	return (blknum_t) ((ino / INODES_PER_BLOCK) + 2); 
}

/*
 * idx2blk:
 * Returns the filesystem block (block number relative to the super
 * block) corresponding to the data block index passed.
 */
static blknum_t idx2blk(int index)
{
	return (blknum_t) (index + 3);
}

/*
 * name2inode:
 * Parses a file name and returns the corresponding inode number. If
 * the file cannot be found, -1 is returned.
 */
static inode_t name2inode(char *name)
{
	char buf[dinode_table[cwd].size];
	char *buff;
	buff = (char *)buf;
	fd_table[FD_DIR].idx = cwd;
	inode_table[cwd].pos = 0;
	//	printf(" size %d\n", dinode_table[cwd].size);
	fs_read(FD_DIR, buff, dinode_table[cwd].size);
	inode_table[cwd].pos = 0;
	int i;
	for (i=0; i<dinode_table[cwd].size; i+=sizeof(struct dirent)) {
		struct dirent *temp;
		temp = (struct dirent*)buff;
		if (strncmp(name, temp->name, MAX_FILENAME_LEN) == 0) {
		  // printf("\ninode  %d \n", temp->inode);
			return temp->inode;
		}
		buff += sizeof(struct dirent);
	}
	return -1;
}


