/*
 * memory.c
 * Note: 
 * There is no separate swap area. When a data page is swapped out, 
 * it is stored in the location it was loaded from in the process' 
 * image. This means it's impossible to start two processes from the 
 * same image without screwing up the running. It also means the 
 * disk image is read once. And that we cannot use the program disk.
 *
 * Best viewed with tabs set to 4 spaces.
 */

#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "memory.h"
#include "thread.h"
#include "util.h"
#include "interrupt.h"
#include "usb/scsi.h"

/*******************************************************************************************
 * Global variables.
 * - page_frames: array of phys_page structs, representing the physical page frames we can use.
 * - pf_index: counts up to PAGEABLE_PAGES to allocate currently unused page_frames, once it
 * reaches max, we start evicting pages to reallocate them.
 * - kernel_dir: kernel page directory, to be used by kernel and all threads.
 * - common_mapping: pointer to base address of the page table that contains the identity mapping
 * of kernel and video memory.
 * - next_free_memory: physical address of the next free memory area.
 * - mem_lock: lock used for synchronization.
 */

phys_page page_frames[PAGEABLE_PAGES]; //page frames
int pf_index = 0;
uint32_t *kernel_dir; //page directory
uint32_t *common_mapping; //page table
uint32_t next_free_memory = MEM_START;
lock_t mem_lock;

/* Use virtual address to get index in page directory.  */
inline uint32_t get_directory_index(uint32_t vaddr)
{
  return (vaddr & PAGE_DIRECTORY_MASK) >> PAGE_DIRECTORY_BITS;
}

/*
 * Use virtual address to get index in a page table.  The bits are
 * masked, so we essentially get a modulo 1024 index.  The selection
 * of which page table to index into is done with
 * get_directory_index().
 */
inline uint32_t get_table_index(uint32_t vaddr)
{
  return (vaddr & PAGE_TABLE_MASK) >> PAGE_TABLE_BITS;
}

/* Use the virtual address to invalidate a page in the TLB. */
inline void invalidate_page(uint32_t * vaddr)
{
      asm volatile("invlpg %0": :"m"(vaddr));
}

/*******************************************************************************************
 * init_memory()
 *   
 * called once by _start() in kernel.c
 * You need to set up the virtual memory map for the kernel here.
 */
void init_memory(void)
{
        srand((uint32_t) get_timer());
        lock_init(&mem_lock);
        init_page_frames();
        kernel_dir = (uint32_t *)page_frames[alloc_page_frame(TRUE)].phys_addr;
        set_kernel_mapping();
}

/* initiates the PAGEABLE_PAGES amount of page frames.
 */
void init_page_frames(void)
{
        int i;
        for (i = 0; i < PAGEABLE_PAGES; i++) {
                page_frames[i].phys_addr = alloc_memory();
        }
}

/* alloc_memory is just used to allocate physical memory space
 * for the PAGEABLE_PAGES number of page frames we are allowed
 * to allocate.
 */
uint32_t alloc_memory(void)
{
        static uint32_t ptr;
        ptr = next_free_memory;
        next_free_memory += PAGE_SIZE;
        if ((next_free_memory & 0xfff) != 0x0) {
                next_free_memory &= 0xfffff000; next_free_memory += 0x1000;
        }
        return ptr;
}

/* alloc_page_frame is used to allocate one of the previously allocated
 * physical page frames, to a directory, table, or page.
 */
int alloc_page_frame(bool_t pinned)
{
        int index;
        if (pf_index < PAGEABLE_PAGES) {
                index = pf_index++;
        } else {
                index = evict_page_frame();
        }
        bzero((void *)page_frames[index].phys_addr, PAGE_SIZE);
        page_frames[index].pinned = pinned;
        return index;
}

/* called by alloc_page_frame and randomly evicts a non-pinned page frame.
 * if page was dirty, it is written to disk (in this case usb).
 * returns index to the page_frame evicted, so it can be cleared in
 * alloc_page_frame before index is returned by its caller.
 */
int evict_page_frame()
{
        uint32_t *dir, *tab;
        int random;
        while (TRUE) {
                random = rand() % PAGEABLE_PAGES;
                if (!page_frames[random].pinned) {
                        uint32_t vaddr = page_frames[random].virt_addr;
                        int tab_i = get_table_index(vaddr);
                        int dir_i = get_directory_index(vaddr);
                        int flush = 0;
                        if (current_running->pid == page_frames[random].cur_holder->pid) flush = 1;
                        dir = (uint32_t *)page_frames[random].cur_holder->page_directory;
                        tab = (uint32_t *)(dir[dir_i] & PE_BASE_ADDR_MASK);
                        
                        tab[tab_i] &= ~PE_P;
                        if (flush) invalidate_page((uint32_t *)vaddr);
                        
                        if (test_bit(tab[tab_i], PE_D)){
                                 swap_out(random);
                        }
                        return random;
                }
        }
}

/* identity maps the kernel and video memory for the kernel directory.
 * pins the mapped table to memory, and refrences it with a pointer,
 * so that the other processes can easily map that table to their own
 * directories.
 */
void set_kernel_mapping(void)
{
        common_mapping = (uint32_t *)page_frames[ alloc_page_frame(TRUE) ].phys_addr;
        uint32_t addr;
        
        /* identity maps everything between address 0x0, and 0x100000 + 33 * 0x1000.
         */
        for (addr = 0x0; addr < MAX_PHYSICAL_MEMORY; addr += PAGE_SIZE) {
                map_table_entry(common_mapping, addr, addr, 0);
        }
        
        /* maps video memory area 1:1 (identity mapping).
         */
        map_table_entry(common_mapping, SCREEN_ADDR, SCREEN_ADDR, 1);
        
        /* maps the common_mapping table to kernel_dir.
         */
        map_directory_entry(kernel_dir, common_mapping, KERN_MEM_START, 1);
}

/* maps a page table to a page directory entry, and sets the proper bits.
 * sets present bit, read/write bit, and user bit if userlevel.
 */
void map_directory_entry(uint32_t *directory, uint32_t *table, uint32_t virt_addr, int userlevel)
{
        int index = get_directory_index(virt_addr);
        int bits = 0 | PE_P | PE_RW;
        if (userlevel) bits |= PE_US;
        directory[index] = ((uint32_t) table & PE_BASE_ADDR_MASK) | bits;
}

/* maps a page to a page table entry, and sets the proper bits.
 * sets present bit, read/write bit, and user bit if userlevel.
 */
void map_table_entry(uint32_t *table, uint32_t virt_addr, uint32_t phys_addr, int userlevel)
{
        int index = get_table_index(virt_addr);
        int bits = 0 | PE_P | PE_RW;
        if (userlevel) bits |= PE_US;
        table[index] = ((uint32_t) phys_addr & PE_BASE_ADDR_MASK) | bits;
}

/*******************************************************************************************
 * Sets up a page directory and page table for a new process or thread. 
 */
void setup_page_table(pcb_t * p)
{
        lock_acquire(&mem_lock);
        
        /* if the caller is a thread, it simply shares the memory area of the kernel.
         */
        if (p->is_thread) {
                p->page_directory = kernel_dir;
        } else {
                /* if the caller is a process, it must setup and initiate its own memory area.
                 * each process will need to pin the following five page frames:
                 * a page directory.
                 * a page table for the process memory (one will be enough in our case).
                 * a page table for the stack area.
                 * and two pages for the stack.
                 */
                uint32_t *process_dir;
                uint32_t *process_table;
                uint32_t *process_stack_table;
                uint32_t stack_entry1;
                uint32_t stack_entry2;
                
                /* requests all the needed page frames, and pins all of them as shown by the TRUE parameter.
                 * everything that is pinned will only need the physical address, and has no use for access
                 * to the full struct set up for each page frame.
                 */
                process_dir = (uint32_t *)page_frames[alloc_page_frame(TRUE)].phys_addr;
                process_table = (uint32_t *)page_frames[alloc_page_frame(TRUE)].phys_addr;
                process_stack_table = (uint32_t *)page_frames[alloc_page_frame(TRUE)].phys_addr;
                stack_entry1 = (uint32_t)page_frames[alloc_page_frame(TRUE)].phys_addr;
                stack_entry2 = (uint32_t)page_frames[alloc_page_frame(TRUE)].phys_addr;
                
                /* maps each of the two new tables to the new directory, as well as the common_mapping table.
                 * then maps the two stack pages to the stack page table.
                 */
                map_directory_entry(process_dir, common_mapping, KERN_MEM_START, 1);
                map_directory_entry(process_dir, process_table, p->start_pc, 1);
                map_directory_entry(process_dir, process_stack_table, p->user_stack, 1);
                map_table_entry(process_stack_table, p->user_stack, stack_entry1, 1);
                map_table_entry(process_stack_table, p->user_stack-0x1000, stack_entry2, 1);
                
                /* sets every physical address of the given table to 0 with user level and read/write bits
                 * set, present to zero.
                 */
                map_table_entries_not_present(process_table);
                
                /* sets the pcb's page directory to the newly made directory.
                 */
                p->page_directory = process_dir;
        }
        
        lock_release(&mem_lock);
}

/* maps all the table entries to not present, but sets other bits correctly.
 *
 */
void map_table_entries_not_present(uint32_t *table)
{
        int i;
        for (i = 0; i < 1024; i++) {
                table[i] = (PE_RW | PE_US) & ~PE_P;
        } 
}


/*******************************************************************************************
 * called by exception_14 in interrupt.c (the faulting address is in
 * current_running->fault_addr)
 *
 * Interrupts are on when calling this function.
 */
void page_fault_handler(void)
{
        lock_acquire(&mem_lock);
        
        /* declares variables.
         */
        uint32_t *cur_dir;
        uint32_t *cur_tab;
        uint32_t error_code;
        uint32_t fault_addr;
        int frame_index;
        int dir_index;
        int tab_index;
        
        /* sets error_code from current_running.
         * sets fault_addr from current_running.
         */
        error_code = current_running->error_code;
        fault_addr = current_running->fault_addr;
        
        /* asserts that address causing page fault, is not present in memory.
         */
                                
        current_running->page_fault_count++;
        
        frame_index = alloc_page_frame(FALSE);
        
        dir_index = get_directory_index(fault_addr);
        tab_index = get_table_index(fault_addr);
        
        cur_dir = current_running->page_directory;
        cur_tab = (uint32_t *)(cur_dir[dir_index] & PE_BASE_ADDR_MASK);
       
        /* Asserts that page is not in memory during page fault (in which case it might 
                 * be a protection violation).
                 */     
                int protection_violation = 1;
                if (test_bit(error_code, PE_P)) protection_violation = 0;
        ASSERT(protection_violation);
        
        swap_in(frame_index);
        
        map_table_entry(cur_tab, fault_addr, page_frames[frame_index].phys_addr, 1);
        
        lock_release(&mem_lock);
}


/* takes error_code and bit input, and tests if the error_code contains that bit.
 */
int test_bit(uint32_t error_code, uint32_t bit)
{
        return error_code & bit;
}

/* pagenumber represents which of the 8-sector sized pages that the program is split
 * onto, will be swapped in/out.
 */
void swap_in(int frame_index)
{
        int location;
        int size;
                
                // finds the page index of the virtual pages the program consists of.
        location = calculate_location(current_running, current_running->fault_addr);
        // finds the page size.
        size = calculate_size(current_running, location);
        
        scsi_read(location, size, (char *) page_frames[frame_index].phys_addr);
        
                page_frames[frame_index].virt_addr = current_running->fault_addr & PE_BASE_ADDR_MASK;
        page_frames[frame_index].cur_holder = current_running;
}
void swap_out(int frame_index)
{
        int location;
        int size;
        pcb_t *p = page_frames[frame_index].cur_holder;
                
                // finds the page index of the virtual pages the program consists of.
        location = calculate_location(p, page_frames[frame_index].virt_addr);
         // finds the page size.
        size = calculate_size(p, location);

        scsi_write(location, size, (char *) page_frames[frame_index].phys_addr);
        
        page_frames[frame_index].virt_addr = 0;
        page_frames[frame_index].cur_holder = 0;
}

/* gets sector location of faulting page on disk.
 */
int calculate_location(pcb_t *p, uint32_t vaddr)
{
        int location = 0;
        uint32_t temp = p->start_pc;
        uint32_t page_loc = vaddr & PE_BASE_ADDR_MASK;
        while (temp < page_loc) {
                location ++;
                temp += PAGE_SIZE;
        }
        return (location*8) + p->swap_loc;
}

int calculate_size(pcb_t *p, int location)
{
        int size = p->swap_size - (location - p->swap_loc);
        if (size >= 8) return SECTORS_PER_PAGE;
        return size;
}











