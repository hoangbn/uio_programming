/* kernel.c
 * Best viewed with tabs set to 4 spaces.
 */
#include "common.h"
#include "kernel.h"
#include "th.h"
#include "util.h"
#include "scheduler.h"

/* helper function declarations */
void make_struct(pcb_t *cur, uint32_t u_s, uint32_t k_s, void *s_e_p, int bool);
uint32_t get_next_stack();
void first_entry();
void init_tasks();

/* Statically allocate some storage for the pcb's */
pcb_t    pcb[NUM_TOTAL];
/* Ready queue and pointer to currently running process */
pcb_t    *current_running;


/* This is the entry point for the kernel.
 *  - Initialize pcb entries
 * - set up stacks
 * - start first thread/process
 */
uint32_t next_stack = 0x10000;

FUNCTION_ALIAS(kernel_start, _start);
void _start(void) {
  int    i;

  /* Declare entry_point as pointer to pointer to function returning void 
   * ENTRY_POINT is defined in kernel h as (void(**)()0xf00)
   */
  void (**entry_point) ()    = ENTRY_POINT;
  
  /* load address of kernel_entry into memory location 0xf00 */
  *entry_point = kernel_entry;
  
  clear_screen(0, 0, 80, 25);

	/* RS232 serial print example
	 * Using provided bochs configuration you will find the printout 
	 * in the serial.out file 
	 */
	rsprintf("Starting up kernel\n");
  
	/* /\* initialization of processes and threads *\/ */
	/* make_struct(&pcb[0], get_next_stack(), get_next_stack(), ((void (*)) 0x10000), TRUE); */
	/* make_struct(&pcb[1], get_next_stack(), get_next_stack(), ((void (*)) 0x12000), TRUE); */
	/* make_struct(&pcb[2], 0x0, next_stack, &clock_thread, FALSE); */
	/* make_struct(&pcb[3], 0x0, get_next_stack(), &thread2, FALSE); */
	/* make_struct(&pcb[4], 0x0, get_next_stack(), &thread3, FALSE); */
	/* make_struct(&pcb[5], 0x0, get_next_stack(), &mcpi_thread0, FALSE); */
	/* make_struct(&pcb[6], 0x0, get_next_stack(), &mcpi_thread1, FALSE); */
	/* make_struct(&pcb[7], 0x0, get_next_stack(), &mcpi_thread2, FALSE); */
	/* make_struct(&pcb[8], 0x0, get_next_stack(), &mcpi_thread3, FALSE); */

	
	/* creates a circular loop of PCB's for the scheduler to run through */
	
	init_tasks();
	
	i = 0;
	while (i < (NUM_TOTAL - 1)) {
    	pcb[i].next = &pcb[i+1];
    	i++;
		pcb[i].previous = &pcb[i-1];
	}
	pcb[i].next = &pcb[0];
	pcb[2].previous = &pcb[i];
	current_running = &pcb[0]; /* initialize a current running to start of with */
  
	scheduler();
  
	while (1);
}

// Stack constants
//    STACK_MIN    = 0x10000,
//    STACK_MAX    = 0x30000,
//    STACK_OFFSET = 0x0ffc,
//    STACK_SIZE   = 0x2000
uint32_t get_next_stack() 
{
  if (next_stack == 0x10000) {
    next_stack += 0x01ffc;
  } else {
    next_stack += 0x02000;
  }
  return next_stack;
}

void init_tasks()
{
 
  int stack_init = STACK_MIN;
  pcb_t *tmp;
  
  int i;
  for(i = 0;i < NUM_TOTAL;i++){
    tmp = &pcb[i];
    switch(i) {
    case 0: 
      tmp->startup_entry_point = 0x10000;
      tmp->is_process = TRUE;
    case 1: 
      tmp->startup_entry_point = 0x12000;
      tmp->is_process = TRUE;
    case 2: 
      tmp->startup_entry_point = (uint32_t)&clock_thread;
      tmp->is_process = FALSE;
    case 3:
      tmp->startup_entry_point = (uint32_t)&thread2;
      tmp->is_process = FALSE;
    case 4:
      tmp->startup_entry_point = (uint32_t)&thread3;
      tmp->is_process = FALSE;
    case 5:
      tmp->startup_entry_point = (uint32_t)&mcpi_thread0;
      tmp->is_process = FALSE;
    case 6:
      tmp->startup_entry_point = (uint32_t)&mcpi_thread1; 
      tmp->is_process = FALSE;
    case 7:
      tmp->startup_entry_point = (uint32_t)&mcpi_thread2;
      tmp->is_process = FALSE;
    case 8:
      tmp->startup_entry_point = (uint32_t)&mcpi_thread3;
      tmp->is_process = FALSE;
      //  default: ASSERT(FALSE); /*should never come here*/ 
    }
    
    switch(tmp->is_process){
    case TRUE:
      tmp->user_stack = (uint32_t*) stack_init+STACK_SIZE;
      stack_init += STACK_SIZE;
    case FALSE:
      tmp->kern_stack = (uint32_t*) stack_init+STACK_SIZE;
      stack_init += STACK_SIZE;
    }

    *--tmp->kern_stack = (uint32_t) &first_entry;
  }
}

void first_entry()
{
  uint32_t *stack, entry_point;
  if(current_running->is_process == TRUE){
    stack = current_running->user_stack;
  } else {
    stack = current_running->kern_stack;
  }
  entry_point = current_running->startup_entry_point;
  asm volatile("movl %0, %%esp;""jmp %1"
	       ::"r"(stack),"r"(entry_point));

  //  ASSERT(FALSE);
}

/* void make_struct(pcb_t *cur, uint32_t u_s, uint32_t k_s, void *s_e_p, int bool) { */
/* 	cur->user_stack = u_s; */
/* 	cur->kern_stack = k_s; */
/* 	cur->startup_entry_point = s_e_p; */
/* 	cur->status = STATUS_FIRST_TIME; */
/* 	cur->is_process = bool; */
/* } */

/* Helper function for kernel_entry, in entry.S. Does the actual work
 * of executing the specified syscall.
 */
void kernel_entry_helper(int fn) {
}

void store_kern_stack(uint32_t* user_stack) {
	current_running->user_stack = user_stack;
	return;
}

void store_user_stack(uint32_t* kern_stack) {
	current_running->kern_stack = kern_stack;
	return;
}

uint32_t get_kern_stack() {
	return &current_running->kern_stack;
}

uint32_t get_user_stack() {
	return &current_running->user_stack;
}
















