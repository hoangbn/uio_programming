/* lock.h
 * Best viewed with tabs set to 4 spaces.
 */

#ifndef LOCK_H
  #define LOCK_H

/* Includes */
  #include  "kernel.h"

/* Constants */
enum {
  LOCKED    = 1,
  UNLOCKED  = 0
};
  
/* Typedefs */
typedef struct {
	int locked;
	pcb_t *block_q;
} lock_t;

/* Prototypes */
  void lock_init(lock_t *);
  void lock_acquire(lock_t *);
  void lock_release(lock_t *);

#endif
