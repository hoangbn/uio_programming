/*  scheduler.c
  Best viewed with tabs set to 4 spaces.
*/
#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "util.h"


/* Call scheduler to run the 'next' process */
void yield(void) {
	//:TODO: GET TIMER! 
	scheduler_entry();
}


/* The scheduler picks the next job to run, and removes blocked and exited
 * processes from the ready queue, before it calls dispatch to start the
 * picked process.
 */
void scheduler(void) {
	current_running = current_running->next;
	dispatch();
}

/* dispatch() does not restore gpr's it just pops down the kernel_stack,
 * and returns to whatever called scheduler (which happens to be
 * scheduler_entry, in entry.S).
 */
void dispatch(void) {
	if(current_running->status == STATUS_FIRST_TIME){
		/* inline asm set stack */
		current_running->status = STATUS_READY;
		if(current_running->is_process == TRUE){
			asm volatile(	"movl %0, %%esp;"
 	                 		"jmp  *%1"
 	                		::"m"(current_running->user_stack), "m"(current_running->startup_entry_point));
		} else {
			asm volatile(	"movl %0, %%esp;"
 	                		"jmp  *%1"
 	                		::"m"(current_running->kern_stack), "m"(current_running->startup_entry_point));
		}
	} else {
 		return;
	}  
}

/* Remove the current_running process from the linked list so it
 * will not be scheduled in the future
 */
void exit(void) {
	current_running->status = STATUS_EXITED;
	current_running->previous->next = current_running->next;
	current_running->next->previous = current_running->previous;
	
	yield();
}


/* 'q' is a pointer to the waiting list where current_running should be 
 * inserted
 */
void block(pcb_t **q) {
	current_running->status = STATUS_BLOCKED;
	current_running->previous->next = current_running->next;
	current_running->next->previous = current_running->previous;
	current_running->q_next = NULL;
	if (*q != NULL) {
		pcb_t *temp = *q;
		while (temp->q_next != NULL) {
   			temp = temp->q_next;
		}
		temp->q_next = current_running;
	} else {
		*q = current_running;
	}
	yield();
}

/* Must be called within a critical section. 
 * Unblocks the first process in the waiting queue (q), (*q) points to the 
 * last process.
 */
void unblock(pcb_t **q) {
	pcb_t *temp = *q;
	temp->status = STATUS_READY;
	temp->previous = current_running;
	temp->next = current_running->next;
	current_running->next = temp;
	temp->next->previous = temp;
	if (temp->q_next != NULL) {
		temp = temp->q_next;
		*q = temp;
	} else {
		*q = NULL;
	}
	current_running->next->q_next = NULL;
}


