/* kernel.c
 * Best viewed with tabs set to 4 spaces.
 */
#include "common.h"
#include "kernel.h"
#include "th.h"
#include "util.h"
#include "scheduler.h"

/* helper function declarations */
void make_struct(pcb_t *cur, uint32_t u_s, uint32_t k_s, uint32_t s_e_p, int bool, int pid);
uint32_t get_next_stack();

/* Statically allocate some storage for the pcb's */
pcb_t    pcb[NUM_TOTAL];
/* Ready queue and pointer to currently running process */
pcb_t    *current_running;


/* This is the entry point for the kernel.
 *  - Initialize pcb entries
 * - set up stacks
 * - start first thread/process
 */
uint32_t next_stack = STACK_MIN;
FUNCTION_ALIAS(kernel_start, _start);
void _start(void) {
  int    i;

  /* Declare entry_point as pointer to pointer to function returning void 
   * ENTRY_POINT is defined in kernel h as (void(**)()0xf00)
   */
  void (**entry_point) ()    = ENTRY_POINT;
  
  /* load address of kernel_entry into memory location 0xf00 */
  *entry_point = kernel_entry;
  
  clear_screen(0, 0, 80, 25);

	/* RS232 serial print example
	 * Using provided bochs configuration you will find the printout 
	 * in the serial.out file 
	 */
	rsprintf("Starting up kernel\n");
  
	/* initialization of processes and threads */
	make_struct(&pcb[0], get_next_stack(), get_next_stack(), (uint32_t) 0x10000, TRUE, 0);
	make_struct(&pcb[1], get_next_stack(), get_next_stack(), (uint32_t) 0x12000, TRUE, 1);
	make_struct(&pcb[2], 0x0, get_next_stack(), (uint32_t) &clock_thread, FALSE, 2);
	make_struct(&pcb[3], 0x0, get_next_stack(), (uint32_t) &thread2, FALSE, 3);
	make_struct(&pcb[4], 0x0, get_next_stack(), (uint32_t) &thread3, FALSE, 4);
	make_struct(&pcb[5], 0x0, get_next_stack(), (uint32_t) &mcpi_thread0, FALSE, 5);
	make_struct(&pcb[6], 0x0, get_next_stack(), (uint32_t) &mcpi_thread1, FALSE, 6);
	make_struct(&pcb[7], 0x0, get_next_stack(), (uint32_t) &mcpi_thread2, FALSE, 7);
	make_struct(&pcb[8], 0x0, get_next_stack(), (uint32_t) &mcpi_thread3, FALSE, 8);

	
	/* creates a circular loop of PCB's for the scheduler to run through */
	i = 0;
	while (i < (NUM_TOTAL - 1)) {
    	pcb[i].next = &pcb[i+1];
    	i++;
		pcb[i].previous = &pcb[i-1];
	}
	pcb[i].next = &pcb[0];
	pcb[0].previous = &pcb[i];
	current_running = &pcb[0]; /* initialize a current running to start of with */
  
	scheduler_entry();
  
	while (1);
}

// Stack constants
//    STACK_MIN    = 0x10000,
//    STACK_MAX    = 0x30000,
//    STACK_OFFSET = 0x0ffc,
//    STACK_SIZE   = 0x2000
uint32_t get_next_stack() {
	if (next_stack == 0x10000) {
		next_stack += 0x01ffc;
	} else {
		next_stack += 0x02000;
	}
	return next_stack;
}
void make_struct(pcb_t *cur, uint32_t u_s, uint32_t k_s, uint32_t s_e_p, int bool, int pid) {
	cur->user_stack = u_s;
	cur->kern_stack = k_s;
	cur->startup_entry_point = s_e_p;
	cur->status = STATUS_FIRST_TIME;
	cur->is_process = bool;
	cur->pid = pid;
}

/* Helper function for kernel_entry, in entry.S. Does the actual work
 * of executing the specified syscall.
 */
void kernel_entry_helper() {
	yield();
}

void store_user_stack(uint32_t user_stack) {
	current_running->user_stack = user_stack;
	return;
}

void store_kern_stack(uint32_t kern_stack) {
	current_running->kern_stack = kern_stack;
	return;
}

uint32_t get_kern_stack() {
	return current_running->kern_stack;
}

uint32_t get_user_stack() {
	return current_running->user_stack;
}
















