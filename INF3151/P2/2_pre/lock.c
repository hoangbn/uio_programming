/*  lock.c

  Implementation of locks.
  Best viewed with tabs set to 4 spaces.
*/

#include "common.h"
#include "lock.h"
#include "scheduler.h"

void lock_init(lock_t *l) {
	l->locked = UNLOCKED;
	l->block_q = NULL;
}

void lock_acquire(lock_t *l) {
	if (l->locked == UNLOCKED) l->locked = LOCKED;
	else {
		block(l->block_q);
	}
}

void lock_release(lock_t *l) {
	if (l->block_q != NULL) unblock(l->block_q);
	else {
		l->locked = UNLOCKED;
	}
}
