/*  scheduler.c
  Best viewed with tabs set to 4 spaces.
*/
#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "util.h"


/* Call scheduler to run the 'next' process */
void yield(void) {
  scheduler();
}


/* The scheduler picks the next job to run, and removes blocked and exited
 * processes from the ready queue, before it calls dispatch to start the
 * picked process.
 */
void scheduler(void) {
	current_running = current_running->next;	     
	dispatch();
}

/* dispatch() does not restore gpr's it just pops down the kernel_stack,
 * and returns to whatever called scheduler (which happens to be
 * scheduler_entry, in entry.S).
 */
void dispatch(void) {
  //TODO
  /*inline assembly jump*/
  
  if(current_running->status == STATUS_FIRST_TIME){
    /* inline asm set stack */
    if(current_running->is_process == TRUE){
      asm volatile("call get_user_stack           \n"
		   "movl %eax, %esp               \n"
		   "movl %esp, %ebp               \n");
      current_running->startup_entry_point(); 
    } else {
      asm volatile("call get_kern_stack           \n"
		   "movl %eax, %esp               \n"
		   "movl %esp, %ebp               \n");
      current_running->startup_entry_point();
    }
  } else {
    return;
  }  
}

/* Remove the current_running process from the linked list so it
 * will not be scheduled in the future
 */
void exit(void) {
	current_running->status = STATUS_EXITED;
	current_running->previous->next = current_running->next;
	current_running->next->previous = current_running->previous;
	
	scheduler();
}


/* 'q' is a pointer to the waiting list where current_running should be 
 * inserted
 */
void block(pcb_t *q) {                         //<--------------   **q betyr q er en peker til mange pekere, vi har bare en liste som best�r av en peker, og via den kan vi finne de
  current_running->status = STATUS_BLOCKED;                  //      andre, slik at vi "har" ikke oversikten av de andre pekere og vi trenger ikke det heller siden f�rste peker 
  if (q == NULL) {                                     //            tar seg av det.
		q = current_running;
	} else {
	  /*lager en temporary block que*/
	  pcb_t *temp = q;
	  while (temp->next != NULL) {
	    temp = temp->next;
	  }
	  temp->next = current_running;
	}
	current_running->previous->next = current_running->next;
	current_running->next->previous = current_running->previous;
	current_running->next = NULL;
	
	scheduler();
}

/* Must be called within a critical section. 
 * Unblocks the first process in the waiting queue (q), (*q) points to the 
 * last process.
 */
void unblock(pcb_t *q) {
	pcb_t *temp;
	if (q->next == NULL) {
		temp = q;
		q = NULL;
	} else {
		temp = q;
		q = q->next;
	}
	temp->status = STATUS_READY;
	temp->previous = current_running;
	temp->next = current_running->next;
	current_running->next = temp;
	temp->next->previous = temp;
	
	scheduler();
}


