# bootblock.s

# .equ symbol, expression. 
# This directive sets the value of symbol to expression
	.equ	BOOT_SEGMENT,0x07c0		#used for bootblocks data segment (os_size)
	.equ	DISPLAY_SEGMENT,0xb800
	.equ	STACK_SEGMENT,0x0900  		
	.equ  	STACK_POINTER,0xfffe
	.equ	KERNEL_TARGET,0x0800
	.equ	KERNEL_OFFSET,0x0000

.text                  # Code segment
.globl	_start	       # The entry point must be global
.code16                # Real mode

#
# The first instruction to execute in a program is called the entry
# point. The linker expects to find the entry point in the "symbol" _start
# (with underscore).
#
_start:

#
# Do not add any instructions before or in os_size.
#
	
	jmp	over
os_size:
	# area reserved for createimage to write the OS size
	.word	0
	.word	0
over:

# Sets Data Segment, and the target location in RAM for INT13-2
	movw	$BOOT_SEGMENT, %ax
	movw	%ax, %ds
	movw	$KERNEL_TARGET, %ax
	movw	%ax, %es
	movw	$KERNEL_OFFSET, %bx

# Sets all values needed by INT13-2
	movb	(os_size), %al
	movb	$0x2, %ah
	movb	$0x0, %ch
	movb	$0x2, %cl
	movb	$0x0, %dh
	movb	$0x80, %dl
	
# Runs INT13-2 and checks for error.
	int	$0x13
	cmpb	$0x0, %ah
	jne	error

# Sets the stack, and sets DS to zero.
	movw	$STACK_SEGMENT, %ax
	movw	%ax, %ss
	movw	$STACK_POINTER, %ax
	movw	%ax, %sp
	movw	$0x0, %ax
	movw	%ax, %ds

# Long jump to kernel (0x08000)
	ljmp	$KERNEL_TARGET,$KERNEL_OFFSET
	
error:
	movw	$DISPLAY_SEGMENT,%bx	
	movw	%bx,%es			
	movw	$0x074b,%es:(0x0)	# Write 'K' in the upper left corner

forever:
	jmp		forever

# Sets bootable
. = 510
	.byte 0x55, 0xaa
