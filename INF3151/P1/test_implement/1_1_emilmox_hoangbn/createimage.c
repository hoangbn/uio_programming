#include <assert.h>
#include <elf.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IMAGE_FILE "image"
#define ARGS "[--extended] [--vm] <bootblock> <executable-file> ..."

#define SECTOR_SIZE 512       /* USB sector size in bytes */
#define OS_SIZE_LOC 2         /* OS position within bootblock */  
#define BOOT_MEM_LOC 0x7c00   /* bootblock memory location */
#define OS_MEM_LOC 0x8000     /* kernel memory location */

FILE *image, *file;
char *boot_buffer, *kernel_buffer, *image_buffer;
int bytes_write, start, stop, bytes_used, file_size, kernel_size, padding, p_pos, check;

void free_memory()
{
  free(boot_buffer);
  free(kernel_buffer);
}


void writeToImage()
{
  image = fopen(IMAGE_FILE, "w+");

  if(image == NULL){
    return;  
  }
  
  /*allocate memory for buffers*/
  kernel_size = file_size / SECTOR_SIZE;
  /*checks if all the file's data can be fitted in the sectors, if not increment it*/
  if((kernel_size * SECTOR_SIZE) < file_size) kernel_size++;
  
  file_size = kernel_size * SECTOR_SIZE;
  kernel_buffer = realloc(kernel_buffer, sizeof(char) * SECTOR_SIZE * kernel_size);
  
  *(boot_buffer + 2) = kernel_size;
  
  /*Write to file*/
  fwrite(boot_buffer,  1,  SECTOR_SIZE, image);
  fwrite(kernel_buffer, 1, file_size, image);
  fclose(image);
}

void kernel_padding(int offset, int size)
{ 
  /*calculate how far the pointer is supposed be forwarded*/
  int temp = p_pos;
  p_pos = offset - temp;
  check = check + p_pos + size;
  
  bytes_used = fread((kernel_buffer + p_pos), 1, size, file);
  /*We didn't read enough bytes*/
  if(bytes_used < size){
    perror("Something went wrong when reading\n");
    exit(1);
  }
}

void prep_kernel(char *object)
{
  char *buffer;                        //remember to free
  Elf32_Ehdr *header;
  Elf32_Phdr *list;
  
  int ret, entry = 0, total = 0;
  padding = 0;
  p_pos = 0;
  check = 0;
  
  /*open the file as a binary file, if */
  file = fopen(object, "rb");    
  if(file == NULL){
    fprintf(stdout, "Couldn't open file %s\n", object);
    exit(1);
  }
  
  /*Upon succecfull fseek it returns 0, check man fseek for meaning of error message*/
  if((fseek(file, 0, SEEK_END)) < 0){                         //set the stream pointer to the end of file
    if(errno == EBADF) perror("fseek, EBADF");
    else perror("fseek, EINVAL");
    exit(1);
  }
 
  file_size = ftell(file);                                    //returns the offset of bytes pointed by stream
 
  if((fseek(file, 0, SEEK_SET)) < 0){                         //set stream pointer at the beginning  
    if(errno == EBADF) perror("fseek, EBADF");
    else perror("fseek, EINVAL");
    exit(1);
  }
  
  /*allocate 1 MB*/
  kernel_buffer = calloc(1000000 ,sizeof(char));
  buffer = malloc(sizeof(char) * file_size);
  
  /*copy the file to a buffer and make a pointer to buffer casted to a ELF header*/
  ret = fread(buffer, 1, file_size, file);
  header = (Elf32_Ehdr *) buffer;
  
  /*if we haven't read enough bytes*/
  if(ret < file_size){
    fprintf(stdout, "Something went wrong with fread\n");
    exit(1);
  }
  
  list = (Elf32_Phdr *) (buffer + header->e_phoff);
  int last_addr;
  int last_filesz;
  while(entry < header->e_phnum){   
    if(entry == 0){
      fseek(file, list->p_offset, SEEK_SET);
      bytes_used = fread(kernel_buffer, 1, list->p_filesz, file);
      last_addr = list->p_vaddr;
      last_filesz = list->p_filesz;
      total += last_filesz;
    } else {
      fseek(file, list->p_offset, SEEK_SET);
      int tmp_offset = list->p_vaddr - last_addr;
      total += tmp_offset - last_filesz;
      file_size = total;
      bytes_used = fread(kernel_buffer + tmp_offset, 1, list->p_filesz, file);
    }

    entry++;
    if(entry < header->e_phnum){ 
      /*Get the next entry*/
      list = (Elf32_Phdr *) (buffer + header->e_phoff + (entry * header->e_phentsize));      
    } else {
      /*checked all entries then jump out of loop*/
      break;
    }
  }
 
  /*reset*/
  bytes_used = 0;
  writeToImage();

  free(buffer);
  fclose(file);
}

void boot_padding()
{
  //The last 2 bytes are reserved for 0x55 and 0xAA, since BIOS identifies those bytes as a bootloader
  //char a = 0xAA;
  //char b = 0x55;
  
  /*pad the rest with null bytes*/
  while(bytes_used < SECTOR_SIZE-2){
   *(boot_buffer+bytes_used++) = '\0';
  }
  /*Adding two-byte boot signature*/
  *(boot_buffer+bytes_used++) = 0x55;
  *(boot_buffer+bytes_used++) = 0xAA;
  
  /*reset to 0*/
  bytes_used = 0;
}

void prep_bootblock(char *object)
{
  char *buffer;                        //remember to free
  Elf32_Ehdr *header;
  Elf32_Phdr *list;
  int ret, entry, total = 0;
  
  /*open the file as a binary file, if */
  file = fopen(object, "rb");    
  if(file == NULL){
    fprintf(stdout, "Couldn't open file %s\n", object);
    exit(1);
  }
  
  fseek(file, 0, SEEK_END);         //set the stream pointer to the end of file
  file_size = ftell(file);          //returns the offset of bytes pointed by stream
  rewind(file);                     //set stream pointer at the beginning
  
  /*allocate memory for buffers*/
  boot_buffer = malloc(sizeof(char) * 512);
  buffer = calloc( file_size,sizeof(char));
  
  /*copy the file to a buffer and make a pointer to buffer casted to a ELF header*/
  ret = fread(buffer, 1, file_size, file);
  header = (Elf32_Ehdr *) buffer;
  
  /*if we haven't read enough bytes*/
  if(ret < file_size){
    fprintf(stdout, "Something went wrong with fread\n");
    exit(1);
  }

  list = (Elf32_Phdr *) (buffer + header->e_phoff);
  entry = 0;
  while(entry < header->e_phnum){
      fseek(file, list->p_offset, SEEK_SET);
      bytes_used = fread(boot_buffer, 1, list->p_filesz, file);
      total = total + bytes_used;
      entry++;
    /*check if we've included all our entries from the table*/
    if(entry < header->e_phnum){
      /*Get the next entry*/
      list = (Elf32_Phdr *) (buffer + header->e_phoff + (entry * header->e_phentsize));; 
    } else {
      /*checked all entries then jump out of loop*/
      break;
    }
  }
  
  /*if the boot program us larger than 510 bytes, then I assume that the necessary padding and two-byte signature is included, else do it manually*/
  if(bytes_used < 510){
    boot_padding();
  }
  
  /*reset*/
  bytes_used = 0;
  
  free(buffer);
  fclose(file);
}


int main(int argc, char **argv)
{
  bytes_used = 0;
  if(argc == 4){
    prep_bootblock(argv[2]);
    prep_kernel(argv[3]);
  }  else if (argc == 3) {
       prep_bootblock(argv[1]);
    prep_kernel(argv[2]);
  } else {
    printf("Not enough files! Please try again\n");
  }
  free_memory();
  return 0;
}

