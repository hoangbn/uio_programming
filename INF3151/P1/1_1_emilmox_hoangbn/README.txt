BOOTBLOCK:
Changed SS from 0x9000 to 0x0900, (just set wrong number originally). Not sure if 90000 is in an offlimits area of the memory, but the kernel crashed during testing because of it.
Set DS twice, did not do that in the design, forgot the need to set it for kernel, after I set it for bootblock.

CREATEIMAGE:
- Found out how to determine the correct offset by using p_vaddr in Program header table to calculate.
- Instead of writing then pad with zeroes, I just used calloc to not worry about padding and write data right on the offset
- At start we malloced the kernel to 1 Mb to make sure everything fits in, and when all data is written we recalculate to the kernel size
